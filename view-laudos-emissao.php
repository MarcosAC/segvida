<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-laudos-emissao.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 4/10/2017 14:31:57
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<!-- Datatables -->
	<link rel="stylesheet" href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css">
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 550px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	
	/*##################
	  # confirm-dialog #
	  ################## */
	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
	
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	.input-group-addon {
	     min-width:280px;
	         width:280px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Ficha
	.ficha-table {
		with:100%;
	}
	
	.ficha-table th {
		width: 100px;
		text-align: right;
		vertical-align: top;
		padding: 2px;
		color:black;
		white-space: nowrap;
	}
	.ficha-table td {
		width: *;
		text-align: left;
		vertical-align: top;
		padding: 2px;
		color:#0000ff;
	}
	.datepicker {
			z-index:1051 !important;
		}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		####
		# Carrega dados do modulo
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## Inicia modulo sem limitacoes de plano
			$o_LIMITE_REGS = -1;//Sem limites de registros
			
			
			## Carrega lista de registros
			
			
			
			####
			# INPUT-SELECT-LIST :: CLIENTE
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idcliente` as value, upper(AA.`nome_interno`) as txt,
              AA.`ld_bio` as ld_bio,
              AA.`ld_calor` as ld_calor,
              AA.`ld_eletr` as ld_eletr,
              AA.`ld_exp` as ld_exp,
              AA.`ld_infl` as ld_infl,
              AA.`ld_part` as ld_part,
              AA.`ld_poei` as ld_poei,
              AA.`ld_rad` as ld_rad,
              AA.`ld_ris` as ld_ris,
              AA.`ld_ruido` as ld_ruido,
              AA.`ld_vap` as ld_vap,
              AA.`ld_vibr_vci` as ld_vibr_vci,
              AA.`ld_vibr_vmb` as ld_vibr_vmb
         FROM `CLIENTE` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_CLIENTE, $o_INPUTSELECTLIST_TXT_CLIENTE, 
				$o_LD_BIO, $o_LD_CALOR, $o_LD_ELETR, $o_LD_EXPL, $o_LD_INFL, $o_LD_PART, $o_LD_POEI, $o_LD_RAD, $o_LD_RIS, $o_LD_RUI, 
				$o_LD_VAP, $o_LD_VBR_VCI, $o_LD_VBR_VMB);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_CLIENTE = "";
				}
				else
				{
					$INPUT_SELECT_LIST_CLIENTE = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_CLIENTE .= "<option value='".$o_INPUTSELECTLIST_VALUE_CLIENTE."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_bio='".$o_LD_BIO."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_calor='".$o_LD_CALOR."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_eletr='".$o_LD_ELETR."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_expl='".$o_LD_EXPL."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_infl='".$o_LD_INFL."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_part='".$o_LD_PART."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_poei='".$o_LD_POEI."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_rad='".$o_LD_RAD."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_ris='".$o_LD_RIS."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_rui='".$o_LD_RUI."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_vap='".$o_LD_VAP."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_vbrvci='".$o_LD_VBR_VCI."'";
						$INPUT_SELECT_LIST_CLIENTE .= " data-ld_vbrvmb='".$o_LD_VBR_VMB."'";
						$INPUT_SELECT_LIST_CLIENTE .= ">".$o_INPUTSELECTLIST_TXT_CLIENTE."</option>";
					}
					
				}
				
			}
			$INPUT_SELECT_LIST_CLIENTE = "<option value=''></option>".$INPUT_SELECT_LIST_CLIENTE."<option value=''></option>";
			
			/*
			####
			# INPUT-SELECT-LIST :: LD_BIO_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `BIOLOGICO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_BIO_PLAN, $o_INPUTSELECTLIST_TXT_LD_BIO_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_BIO_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_BIO_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_BIO_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_BIO_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_BIO_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_BIO_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_BIO_MOD, $o_INPUTSELECTLIST_TXT_LD_BIO_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_BIO_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_BIO_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_BIO_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_BIO_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_BIO_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_CALOR_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `CALOR` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_CALOR_PLAN, $o_INPUTSELECTLIST_TXT_LD_CALOR_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_CALOR_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_CALOR_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_CALOR_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_CALOR_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_CALOR_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_CALOR_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_CALOR_MOD, $o_INPUTSELECTLIST_TXT_LD_CALOR_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_CALOR_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_CALOR_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_CALOR_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_CALOR_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_CALOR_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_ELETR_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `ELETRICIDADE` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_ELETR_PLAN, $o_INPUTSELECTLIST_TXT_LD_ELETR_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_ELETR_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_ELETR_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_ELETR_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_ELETR_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_ELETR_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_ELETR_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_ELETR_MOD, $o_INPUTSELECTLIST_TXT_LD_ELETR_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_ELETR_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_ELETR_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_ELETR_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_ELETR_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_ELETR_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_EXPL_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `EXPLOSIVO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_EXPL_PLAN, $o_INPUTSELECTLIST_TXT_LD_EXPL_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_EXPL_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_EXPL_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_EXPL_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_EXPL_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_EXPL_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_EXPL_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_EXPL_MOD, $o_INPUTSELECTLIST_TXT_LD_EXPL_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_EXPL_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_EXPL_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_EXPL_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_EXPL_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_EXPL_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_INFL_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `INFLAMAVEL` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_INFL_PLAN, $o_INPUTSELECTLIST_TXT_LD_INFL_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_INFL_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_INFL_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_INFL_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_INFL_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_INFL_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_INFL_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_INFL_MOD, $o_INPUTSELECTLIST_TXT_LD_INFL_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_INFL_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_INFL_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_INFL_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_INFL_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_INFL_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_PART_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `PARTICULADO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_PART_PLAN, $o_INPUTSELECTLIST_TXT_LD_PART_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_PART_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_PART_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_PART_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_PART_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_PART_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_PART_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_PART_MOD, $o_INPUTSELECTLIST_TXT_LD_PART_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_PART_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_PART_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_PART_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_PART_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_PART_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_POEI_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `POEIRA` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_POEI_PLAN, $o_INPUTSELECTLIST_TXT_LD_POEI_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_POEI_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_POEI_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_POEI_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_POEI_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_POEI_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_POEI_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_POEI_MOD, $o_INPUTSELECTLIST_TXT_LD_POEI_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_POEI_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_POEI_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_POEI_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_POEI_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_POEI_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_RAD_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `RADIACAO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_RAD_PLAN, $o_INPUTSELECTLIST_TXT_LD_RAD_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_RAD_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_RAD_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_RAD_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_RAD_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_RAD_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_RAD_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_RAD_MOD, $o_INPUTSELECTLIST_TXT_LD_RAD_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_RAD_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_RAD_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_RAD_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_RAD_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_RAD_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_RUI_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `RUIDO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_RUI_PLAN, $o_INPUTSELECTLIST_TXT_LD_RUI_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_RUI_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_RUI_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_RUI_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_RUI_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_RUI_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_RUI_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_RUI_MOD, $o_INPUTSELECTLIST_TXT_LD_RUI_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_RUI_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_RUI_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_RUI_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_RUI_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_RUI_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VAP_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `VAPOR` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VAP_PLAN, $o_INPUTSELECTLIST_TXT_LD_VAP_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VAP_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VAP_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VAP_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VAP_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_VAP_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VAP_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VAP_MOD, $o_INPUTSELECTLIST_TXT_LD_VAP_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VAP_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VAP_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VAP_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VAP_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_VAP_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VBRVCI_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `VIBR_VCI` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VBRVCI_PLAN, $o_INPUTSELECTLIST_TXT_LD_VBRVCI_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VBRVCI_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VBRVCI_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VBRVCI_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VBRVCI_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_VBRVCI_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VBRVCI_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VBRVCI_MOD, $o_INPUTSELECTLIST_TXT_LD_VBRVCI_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VBRVCI_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VBRVCI_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VBRVCI_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VBRVCI_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_VBRVCI_MOD."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VBRVMB_PLAN
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
         FROM `VIBR_VMB` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VBRVMB_PLAN, $o_INPUTSELECTLIST_TXT_LD_VBRVMB_PLAN);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VBRVMB_PLAN = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VBRVMB_PLAN = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VBRVMB_PLAN .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VBRVMB_PLAN."'>".$o_INPUTSELECTLIST_TXT_LD_VBRVMB_PLAN."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: LD_VBRVMB_MOD
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idplanilha_modelo` as value, AA.`ano` as txt
         FROM `PLANILHA_MODELO` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LD_VBRVMB_MOD, $o_INPUTSELECTLIST_TXT_LD_VBRVMB_MOD);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LD_VBRVMB_MOD = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LD_VBRVMB_MOD = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LD_VBRVMB_MOD .= "<option value='".$o_INPUTSELECTLIST_VALUE_LD_VBRVMB_MOD."'>".$o_INPUTSELECTLIST_TXT_LD_VBRVMB_MOD."</option>";
					}
					
				}
				
			}
			*/
			
		}# /carrega dados #
		
		#<li id="search_button_link"><a href="#" onclick="javascript:$(this).trigger('f_show_advsearch');" role="button" aria-expanded="false"><i class="fa fa-lg fa-search btn_color_blue2"></i></a></li>
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_LAUDOS_EMISSAO_LAUDOS; ?></a></li>
								<li class="active"><span><?php echo TXT_LAUDOS_EMISSAO_EMISSAO; ?></span></li>
							</ol></h2>
						<ul class="nav navbar-right panel_toolbox">
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					<!-- /main_tabe -->
					<div class="col-xs-12 col-sm-9 col-centered">
						<h3 style="color:#434a54;"><span class="fa fa-file" aria-hidden="true"></span> Emissão de Laudo Final</h3>
						<div class="clearfix"></div>
						<p align="justify" style="color:#434a54; font-size: 120%;">Preencha o formulário abaixo e clique em GERAR.</p>
						<p align="justify" style="color:#434a54; font-size: 120%;">Obs.: Campos com um <span style="color:#ff0000;">*</span> são obrigatórios</p>
						
						<!-- Begin | Register Form -->
						<form id="reg-form">
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_ANO; ?>:</b>
								</span>
								<input id="reg_ano" type="text" maxlength="4" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_MES; ?>:</b>
								</span>
								<select id="reg_mes" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<option value=""></option><option value="1"><?php echo TXT_LAUDOS_EMISSAO_JAN; ?></option><option value="2"><?php echo TXT_LAUDOS_EMISSAO_FEV; ?></option><option value="3"><?php echo TXT_LAUDOS_EMISSAO_MAR; ?></option><option value="4"><?php echo TXT_LAUDOS_EMISSAO_ABR; ?></option><option value="5"><?php echo TXT_LAUDOS_EMISSAO_MAI; ?></option><option value="6"><?php echo TXT_LAUDOS_EMISSAO_JUN; ?></option><option value="7"><?php echo TXT_LAUDOS_EMISSAO_JUL; ?></option><option value="8"><?php echo TXT_LAUDOS_EMISSAO_AGO; ?></option><option value="9"><?php echo TXT_LAUDOS_EMISSAO_SET; ?></option><option value="10"><?php echo TXT_LAUDOS_EMISSAO_OUT; ?></option><option value="11"><?php echo TXT_LAUDOS_EMISSAO_NOV; ?></option><option value="12"><?php echo TXT_LAUDOS_EMISSAO_DEZ; ?></option>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_CLIENTE; ?>:</b>
								</span>
								<select id="reg_cliente" data-live-search="true" class="form-control selectpicker" style="margin-top: 0px; margin-bottom: 0px;">
									<?php echo $INPUT_SELECT_LIST_CLIENTE; ?>
								</select>
							</div>
							<div class="input-group" id="painel_bio_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_bio_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_BIO_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_bio_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_bio_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_BIO_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_calor_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_CALOR_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_calor_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_CALOR_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_calor_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_CALOR_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_calor_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_CALOR_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_eletr_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_eletr_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_ELETR_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_eletr_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_eletr_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_ELETR_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_expl_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_expl_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_EXPL_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_expl_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_expl_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_EXPL_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_infl_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_infl_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_INFL_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_infl_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_infl_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_INFL_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_part_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_part_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_PART_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_part_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_part_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_PART_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_poei_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_poei_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_POEI_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_poei_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_poei_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_POEI_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_rad_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_rad_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RAD_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_rad_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_rad_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RAD_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_ris_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RISCO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_ris_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RIS_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_ris_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RISCO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_ris_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RIS_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_rui_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_rui_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RUI_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_rui_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_rui_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RUI_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vap_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vap_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VAP_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vap_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vap_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VAP_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vbrvci_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vbrvci_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVCI_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vbrvci_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vbrvci_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVCI_MOD; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vbrvmb_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vbrvmb_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVMB_PLAN; ?>
								</select>
							</div>
							<div class="input-group" id="painel_vbrvmb_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vbrvmb_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVMB_MOD; ?>
								</select>
							</div>
							<div>
								<button id="bt_gerar" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_GENERATE; ?></button>
							</div>
						</form>
						<!-- End | cad Form -->
						
					</div><!--/.col-xs-12.col-sm-9-->
					
					
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->



<!-- BEGIN # MODAL CAD -->
<div class="modal fade" id="cad-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<span style="line-height: 10px; font-size: 10px;"><i class="fa fa-thumbs-o-up"></i>&nbsp;<?php echo TXT_TECLE_ESC_PARA_FECHAR; ?></span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
			</div>
			
			<!-- Begin # DIV Form -->
			<div id="div-forms">
				<!-- Begin | Form -->
				<form id="reg-form">
					<input id="reg_id" type="hidden">
					<div class="modal-body">
						<div id="div-reg-msg">
							<div id="div-label-type"><?php echo TXT_CAD_MODAL_NOVO_REGISTRO; ?></div>
							<div id="icon-reg-msg" class="glyphicon glyphicon-chevron-right"></div>
								<span id="text-reg-msg"><?php echo TXT_CAD_MODAL_CAMPOS_COM_A_SAO_OBRIGATORIOS; ?></span>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_ANO; ?>:</b>
								</span>
								<input id="reg_ano" type="text" maxlength="4" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_MES; ?>:</b>
								</span>
								<select id="reg_mes" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<option value="1"><?php echo TXT_LAUDOS_EMISSAO_JAN; ?></option><option value="2"><?php echo TXT_LAUDOS_EMISSAO_FEV; ?></option><option value="3"><?php echo TXT_LAUDOS_EMISSAO_MAR; ?></option><option value="4"><?php echo TXT_LAUDOS_EMISSAO_ABR; ?></option><option value="5"><?php echo TXT_LAUDOS_EMISSAO_MAI; ?></option><option value="6"><?php echo TXT_LAUDOS_EMISSAO_JUN; ?></option><option value="7"><?php echo TXT_LAUDOS_EMISSAO_JUL; ?></option><option value="8"><?php echo TXT_LAUDOS_EMISSAO_AGO; ?></option><option value="9"><?php echo TXT_LAUDOS_EMISSAO_SET; ?></option><option value="10"><?php echo TXT_LAUDOS_EMISSAO_OUT; ?></option><option value="11"><?php echo TXT_LAUDOS_EMISSAO_NOV; ?></option><option value="12"><?php echo TXT_LAUDOS_EMISSAO_DEZ; ?></option>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_CLIENTE; ?>:</b>
								</span>
								<select id="reg_cliente" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_CLIENTE; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_bio_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_BIO_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_bio_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_BIO_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_CALOR_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_calor_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_CALOR_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_CALOR_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_calor_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_CALOR_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_eletr_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_ELETR_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_eletr_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_ELETR_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_expl_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_EXPL_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_expl_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_EXPL_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_infl_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_INFL_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_infl_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_INFL_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_part_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_PART_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_part_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_PART_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_poei_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_POEI_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_poei_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_POEI_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_rad_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RAD_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_rad_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RAD_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_rui_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RUI_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_rui_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_RUI_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vap_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VAP_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vap_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VAP_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vbrvci_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVCI_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vbrvci_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVCI_MOD; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_N_PLANILHA; ?>:</b>
								</span>
								<select id="reg_ld_vbrvmb_plan" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVMB_PLAN; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_MODELO; ?>:</b>
								</span>
								<select id="reg_ld_vbrvmb_mod" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
									<?php echo $INPUT_SELECT_LIST_LD_VBRVMB_MOD; ?>
								</select>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<div>
							<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_REGISTER; ?></button>
							<button id="bt_save" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SAVE; ?></button>
						</div>
					</div>
				</form>
				<!-- End | Form -->
				
			</div>
			<!-- End # DIV Form -->
			
		</div>
	</div>
</div>
<!-- END # MODAL CAD -->


<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<!-- iCheck -->
		<script src="vendors/iCheck/icheck.min.js"></script>
		<!-- Datatables -->
		<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="vendors/datatables.net/js/dataTables.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/dataTables.buttons.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.colVis.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.flash.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.html5.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.print.min.js"></script>
		
		<script src="vendors/datatables.net/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
		<script src="vendors/datatables.net/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
		<script src="vendors/datatables.net/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/dataTables.responsive.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Scroller/js/dataTables.scroller.min.js"></script>
		<!-- <script src="vendors/datatables.net/extensions/Select/js/dataTables.select.min.js"></script> -->
		
		<script src="vendors/jszip/dist/jszip.min.js"></script>
		<script src="vendors/pdfmake/build/pdfmake.min.js"></script>
		<script src="vendors/pdfmake/build/vfs_fonts.js"></script>
		
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/view-laudos-emissao.js" charset="UTF-8"></script>
		

<?php
	} 
?>
