<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: gera_options_laudos.php
##  Funcao: MODEL - Gera <option>...</option> com a lista de numeros de planilha ou modelos de planilha para o id de cliente informado, com selected no registro indicado
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 05/10/2017 17:41:00
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once "geral/language.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_ID = "";
	$IN_BIO_FB = $IN_BIO_FBT = $IN_BIO = "";
	$IN_CALOR_FB = $IN_CALOR_FBT = $IN_CALOR = "";
	$IN_ELETR_FB = $IN_ELETR_FBT = $IN_ELETR = "";
	$IN_EXPL_FB = $IN_EXPL_FBT = $IN_EXPL = "";
	$IN_PART_FB = $IN_PART_FBT = $IN_PART = "";
	$IN_POEI_FB = $IN_POEI_FBT = $IN_POEI = "";
	$IN_RAD_FB = $IN_RAD_FBT = $IN_RAD = "";
	$IN_RIS_FB = $IN_RIS_FBT = $IN_RIS = "";
	$IN_RUI_FB = $IN_RUI_FBT = $IN_RUI = "";
	$IN_VAP_FB = $IN_VAP_FBT = $IN_VAP = "";
	$IN_VBRVCI_FB = $IN_VBRVCI_FBT = $IN_VBRVCI = "";
	$IN_VBRVMB_FB = $IN_VBRVMB_FBT = $IN_VBRVMB = "";
	
	
	//segvida.prodfy.com.br/lib-bin/gera_options_laudos.php?l=pt-br&i=1&bio=1&bio_fb=1&bio_fbt=Teste
	
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG           = test_input($_GET["l"]);
		$IN_BIO            = test_input($_GET["bio"]);
		$IN_BIO_FB         = test_input($_GET["bio_fb"]);
		$IN_BIO_FBT        = test_input($_GET["bio_fbt"]);
		$IN_CALOR          = test_input($_GET["calor"]);
		$IN_CALOR_FB       = test_input($_GET["calor_fb"]);
		$IN_CALOR_FBT      = test_input($_GET["calor_fbt"]);
		$IN_ELETR          = test_input($_GET["eletr"]);
		$IN_ELETR_FB       = test_input($_GET["eletr_fb"]);
		$IN_ELETR_FBT      = test_input($_GET["eletr_fbt"]);
		$IN_EXPL           = test_input($_GET["expl"]);
		$IN_EXPL_FB        = test_input($_GET["expl_fb"]);
		$IN_EXPL_FBT       = test_input($_GET["expl_fbt"]);
		$IN_INFL           = test_input($_GET["infl"]);
		$IN_INFL_FB        = test_input($_GET["infl_fb"]);
		$IN_INFL_FBT       = test_input($_GET["infl_fbt"]);
		$IN_PART           = test_input($_GET["part"]);
		$IN_PART_FB        = test_input($_GET["part_fb"]);
		$IN_PART_FBT       = test_input($_GET["part_fbt"]);
		$IN_POEI           = test_input($_GET["poei"]);
		$IN_POEI_FB        = test_input($_GET["poei_fb"]);
		$IN_POEI_FBT       = test_input($_GET["poei_fbt"]);
		$IN_RAD            = test_input($_GET["rad"]);
		$IN_RAD_FB         = test_input($_GET["rad_fb"]);
		$IN_RAD_FBT        = test_input($_GET["rad_fbt"]);
		$IN_RIS            = test_input($_GET["ris"]);
		$IN_RIS_FB         = test_input($_GET["ris_fb"]);
		$IN_RIS_FBT        = test_input($_GET["ris_fbt"]);
		$IN_RUI            = test_input($_GET["rui"]);
		$IN_RUI_FB         = test_input($_GET["rui_fb"]);
		$IN_RUI_FBT        = test_input($_GET["rui_fbt"]);
		$IN_VAP            = test_input($_GET["vap"]);
		$IN_VAP_FB         = test_input($_GET["vap_fb"]);
		$IN_VAP_FBT        = test_input($_GET["vap_fbt"]);
		$IN_VBRVCI         = test_input($_GET["vbrvci"]);
		$IN_VBRVCI_FB      = test_input($_GET["vbrvci_fb"]);
		$IN_VBRVCI_FBT     = test_input($_GET["vbrvci_fbt"]);
		$IN_VBRVMB         = test_input($_GET["vbrvmb"]);
		$IN_VBRVMB_FB      = test_input($_GET["vbrvmb_fb"]);
		$IN_VBRVMB_FBT     = test_input($_GET["vbrvmb_fbt"]);
		$IN_ID             = test_input($_GET["i"]);
		$IN_TARGET_ID      = test_input($_GET["ti"]);
/*
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		$IN_BIO            = test_input($_POST["bio"]);
		$IN_BIO_FB         = test_input($_POST["bio_fb"]);
		$IN_BIO_FBT        = test_input($_POST["bio_fbt"]);
		$IN_CALOR          = test_input($_POST["calor"]);
		$IN_CALOR_FB       = test_input($_POST["calor_fb"]);
		$IN_CALOR_FBT      = test_input($_POST["calor_fbt"]);
		$IN_ELETR          = test_input($_POST["eletr"]);
		$IN_ELETR_FB       = test_input($_POST["eletr_fb"]);
		$IN_ELETR_FBT      = test_input($_POST["eletr_fbt"]);
		$IN_EXPL           = test_input($_POST["expl"]);
		$IN_EXPL_FB        = test_input($_POST["expl_fb"]);
		$IN_EXPL_FBT       = test_input($_POST["expl_fbt"]);
		$IN_INFL           = test_input($_POST["infl"]);
		$IN_INFL_FB        = test_input($_POST["infl_fb"]);
		$IN_INFL_FBT       = test_input($_POST["infl_fbt"]);
		$IN_PART           = test_input($_POST["part"]);
		$IN_PART_FB        = test_input($_POST["part_fb"]);
		$IN_PART_FBT       = test_input($_POST["part_fbt"]);
		$IN_POEI           = test_input($_POST["poei"]);
		$IN_POEI_FB        = test_input($_POST["poei_fb"]);
		$IN_POEI_FBT       = test_input($_POST["poei_fbt"]);
		$IN_RAD            = test_input($_POST["rad"]);
		$IN_RAD_FB         = test_input($_POST["rad_fb"]);
		$IN_RAD_FBT        = test_input($_POST["rad_fbt"]);
		$IN_RIS            = test_input($_POST["ris"]);
		$IN_RIS_FB         = test_input($_POST["ris_fb"]);
		$IN_RIS_FBT        = test_input($_POST["ris_fbt"]);
		$IN_RUI            = test_input($_POST["rui"]);
		$IN_RUI_FB         = test_input($_POST["rui_fb"]);
		$IN_RUI_FBT        = test_input($_POST["rui_fbt"]);
		$IN_VAP            = test_input($_POST["vap"]);
		$IN_VAP_FB         = test_input($_POST["vap_fb"]);
		$IN_VAP_FBT        = test_input($_POST["vap_fbt"]);
		$IN_VBRVCI         = test_input($_POST["vbrvci"]);
		$IN_VBRVCI_FB      = test_input($_POST["vbrvci_fb"]);
		$IN_VBRVCI_FBT     = test_input($_POST["vbrvci_fbt"]);
		$IN_VBRVMB         = test_input($_POST["vbrvmb"]);
		$IN_VBRVMB_FB      = test_input($_POST["vbrvmb_fb"]);
		$IN_VBRVMB_FBT     = test_input($_POST["vbrvmb_fbt"]);
		$IN_ID             = test_input($_POST["i"]);
		$IN_TARGET_ID      = test_input($_POST["ti"]);
		*/
		if( isEmpty($IN_BIO) )   { $IN_BIO = 0; }
		if( isEmpty($IN_CALOR) ) { $IN_CALOR = 0; }
		if( isEmpty($IN_ELETR) ) { $IN_ELETR = 0; }
		if( isEmpty($IN_EXPL) )  { $IN_EXPL = 0; }
		if( isEmpty($IN_INFL) )  { $IN_INFL = 0; }
		if( isEmpty($IN_PART) )  { $IN_PART = 0; }
		if( isEmpty($IN_POEI) )  { $IN_POEI = 0; }
		if( isEmpty($IN_RAD) )   { $IN_RAD = 0; }
		if( isEmpty($IN_RIS) )   { $IN_RIS = 0; }
		if( isEmpty($IN_RUI) )   { $IN_RUI = 0; }
		if( isEmpty($IN_VAP) )   { $IN_VAP = 0; }
		if( isEmpty($IN_VBRVCI) ){ $IN_VBRVCI = 0; }
		if( isEmpty($IN_VBRVMB) ){ $IN_VBRVMB = 0; }
		
		## Carrega Idioma
		if( isEmpty($IN_LANG) ){ $IN_LANG = "pt-br"; }
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !isEmpty($IN_ID)
			) 
		{ $GO=1; } else { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##inicia sessao
			$init_sid = sec_session_start();
			
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## Valida se o usuario esta logado
			if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
			
			## Carrega ID do cliente Prodfy
			$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
			$sid_USERNAME            = $_SESSION['user_username'];
			$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
			if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
			{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
			
			## Escapes
			$sql_ID = $mysqli->escape_string($IN_ID);
			
			####
			# BIOLOGICO
			####
			if($IN_BIO == 1)
			{
				##Lista de numeros de planilha
				if ($stmt = $mysqli->prepare(
				"SELECT AA.`planilha_num` as value, AA.`planilha_num` as txt
           FROM `BIOLOGICO` AA
     INNER JOIN `SYSTEM_CLIENTE` C
             ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
          WHERE AA.`idSYSTEM_CLIENTE` = ?
            AND AA.`laudo` = 'bio'
       ORDER BY 1"
				)) 
				{
					$stmt->bind_param('s', $sql_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_PLAN_V, $o_PLAN_T);
					
					$ITEM = "";
					
					##Se nao encontrou dados, retorna
					if ($stmt->num_rows == 0) 
					{
						$ITEM = "";
					}
					else
					{
						##Corre pelas linhas encontradas
						while($stmt->fetch())
						{
							if($o_PLAN_V)
							{
								$ITEM .= "<option value='".$o_PLAN_V."'>".$o_PLAN_T."</option>";
							}
						}
					}
					
					if($IN_BIO_FB == 1){ $FIRSTBLANK = '<option value="">'.$IN_BIO_FBT.'</option>'; } else { $FIRSTBLANK=''; }
					
					## Formata lista final
					$LISTA_PLAN_BIO = $FIRSTBLANK.$ITEM;
					
				}
				else
				{
					die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
					exit;
				}
				
				##Lista de modelos de planilha
				if ($stmt = $mysqli->prepare(
				"SELECT AA.`idplanilha_modelo` as value, 
                CONCAT(CASE WHEN AA.`mes` = 1 THEN 'JAN' 
                     WHEN AA.`mes` = 2 THEN 'FEV' 
                     WHEN AA.`mes` = 3 THEN 'MAR' 
                     WHEN AA.`mes` = 4 THEN 'ABR' 
                     WHEN AA.`mes` = 5 THEN 'MAI' 
                     WHEN AA.`mes` = 6 THEN 'JUN' 
                     WHEN AA.`mes` = 7 THEN 'JUL' 
                     WHEN AA.`mes` = 8 THEN 'AGO' 
                     WHEN AA.`mes` = 9 THEN 'SET' 
                     WHEN AA.`mes` = 10 THEN 'OUT' 
                     WHEN AA.`mes` = 11 THEN 'NOV' 
                     WHEN AA.`mes` = 12 THEN 'DEZ' 
                 END,'/',
                AA.`ano`) as txt
           FROM `PLANILHA_MODELO` AA
     INNER JOIN `SYSTEM_CLIENTE` C
             ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
          WHERE AA.`idSYSTEM_CLIENTE` = ?
           AND AA.`laudo` = 'bio'
       ORDER BY AA.`ano` desc, AA.`mes` desc"
				)) 
				{
					$stmt->bind_param('s', $sql_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_MOD_V, $o_MOD_T);
					
					$ITEM = "";
					
					##Se nao encontrou dados, retorna
					if ($stmt->num_rows == 0) 
					{
						$ITEM = "";
					}
					else
					{
						##Corre pelas linhas encontradas
						while($stmt->fetch())
						{
							if($o_MOD_V)
							{
								$ITEM .= "<option value='".$o_MOD_V."'>".$o_MOD_T."</option>";
							}
						}
					}
					
					if($IN_BIO_FB == 1){ $FIRSTBLANK = '<option value="">'.$IN_BIO_FBT.'</option>'; } else { $FIRSTBLANK=''; }
					
					## Formata lista final
					$LISTA_MOD_BIO = $FIRSTBLANK.$ITEM;
					
				}
				else
				{
					die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
					exit;
				}
			}
			
			
			
			
			die('1|'.$LISTA.'|success|');
			exit;
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
