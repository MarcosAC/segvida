<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-cfg-planilha_modelo.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 6/9/2017 13:8:54
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_ANO = $IN_MES = $IN_LAUDO = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_LAUDO = test_input($_GET["laudo"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_LAUDO = test_input($_POST["laudo"]);
		
		## case convertion
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_ANO, $IN_MES, $IN_LAUDO);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_LAUDO) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_ANO, $IN_MES, $IN_LAUDO);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_LAUDO) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_ANO, $IN_MES, $IN_LAUDO);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## FILE UPLOAD
			case '5':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPLOAD($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
				
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id, AA.`ano` as ano, AA.`mes` as mes, AA.`laudo` as laudo
       FROM `PLANILHA_MODELO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
   ORDER BY 1,2,3,4"
		)) 
		{
			$stmt->bind_param('s', $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_LAUDO);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_MES == "1"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JAN; }
					if( $o_MES == "2"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_FEV; }
					if( $o_MES == "3"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_MAR; }
					if( $o_MES == "4"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_ABR; }
					if( $o_MES == "5"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_MAI; }
					if( $o_MES == "6"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JUN; }
					if( $o_MES == "7"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JUL; }
					if( $o_MES == "8"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_AGO; }
					if( $o_MES == "9"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_SET; }
					if( $o_MES == "10"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_OUT; }
					if( $o_MES == "11"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_NOV; }
					if( $o_MES == "12"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_DEZ; }
					if( $o_LAUDO == "risco"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_ANALISE_DE_RISCO; }
					if( $o_LAUDO == "bio"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_BIOLOGICO; }
					if( $o_LAUDO == "calor"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_CALOR; }
					if( $o_LAUDO == "eletr"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_ELETRICIDADE; }
					if( $o_LAUDO == "expl"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_EXPLOSIVO; }
					if( $o_LAUDO == "infl"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_INFLAMAVEL; }
					if( $o_LAUDO == "part"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_PARTICULADO; }
					if( $o_LAUDO == "poei"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_POEIRA; }
					if( $o_LAUDO == "rad"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_RADIACAO; }
					if( $o_LAUDO == "rui"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_RUIDO; }
					if( $o_LAUDO == "vap"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VAPORES; }
					if( $o_LAUDO == "vbrvci"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VIBRACAO_VCI; }
					if( $o_LAUDO == "vbrvmb"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VIBRACAO_VMB; }
				
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td>'.$o_ANO.'</td>';
					$TBODY_LIST .= '<td>'.$o_MES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_LAUDO_TXT.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_ANO, $_MES, $_LAUDO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_LAUDO = $mysqli->escape_String($_LAUDO);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id
       FROM `PLANILHA_MODELO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`laudo` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('sdds', $sql_SID_idSYSTEM_CLIENTE, $sql_ANO, $sql_MES, $sql_LAUDO);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_ANO, $_MES, $_LAUDO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_ARQUIVO_FILENAME_filename = "";
		if($_FILES["arquivo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("xls","xlsx");
			$temp        = explode(".", $_FILES["arquivo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "application/pdf") && 
					//($_FILES["file"]["type"] == "application/vnd.ms-excel") && 
					//($_FILES["file"]["type"] == "application/msword") && 
					//($_FILES["file"]["type"] == "application/vnd.ms-powerpoint") && 
					//($_FILES["file"]["type"] == "text/plain") && 
					//($_FILES["file"]["type"] == "text/richtext") && 
					//($_FILES["file"]["type"] == "application/pdf") && 
					($_FILES["arquivo_filename_file"]["size"] < 3000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["arquivo_filename_file"]["error"] > 0) 
				{
					return "0|ARQUIVO - ".$_FILES["arquivo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_ARQUIVO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_ARQUIVO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["arquivo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_ARQUIVO_FILENAME_filename);
				
			}
			else
			{
				return "0|ARQUIVO - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_LAUDO = $mysqli->escape_String($_LAUDO);
		$sql_ARQUIVO_FILENAME_filename = $mysqli->escape_String($_ARQUIVO_FILENAME_filename);
		
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id
       FROM `PLANILHA_MODELO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`laudo` = ?"
		)) 
		{
			$stmt->bind_param('sdds', $sql_SID_idSYSTEM_CLIENTE, $sql_ANO, $sql_MES, $sql_LAUDO);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		
		/*if($_DEBUG == 1)
		{
			$SQL = "INSERT INTO `PLANILHA_MODELO` 
		    SET `idSYSTEM_CLIENTE` = '$sql_SID_idSYSTEM_CLIENTE',
		        `cad_date` = now(),
		        `cad_username` = '$sql_SID_USERNAME',
		        `ano` = '$sql_ANO'
		        ,`mes` = '$sql_MES'
		        ,`laudo` = '$sql_LAUDO'
		        ,`arquivo_filename` = '$sql_ARQUIVO_FILENAME_filename'";
			error_log("mdl-cfg-planilha_modelo.php -> INSERT:\n\nSQL:\n\n".$SQL."\n\n",0);
		}
		*/
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `PLANILHA_MODELO` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `ano` = ?
		        ,`mes` = ?
		        ,`laudo` = ?
		        ,`arquivo_filename` = ?"
		)) 
		{
			$stmt->bind_param('ssddss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_LAUDO, $sql_ARQUIVO_FILENAME_filename);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_ANO, $_MES, $_LAUDO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_ARQUIVO_FILENAME_filename = "";
		if($_FILES["arquivo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("xls","xlsx");
			$temp        = explode(".", $_FILES["arquivo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "application/pdf") && 
					//($_FILES["file"]["type"] == "application/vnd.ms-excel") && 
					//($_FILES["file"]["type"] == "application/msword") && 
					//($_FILES["file"]["type"] == "application/vnd.ms-powerpoint") && 
					//($_FILES["file"]["type"] == "text/plain") && 
					//($_FILES["file"]["type"] == "text/richtext") && 
					//($_FILES["file"]["type"] == "application/pdf") && 
					($_FILES["arquivo_filename_file"]["size"] < 3000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["arquivo_filename_file"]["error"] > 0) 
				{
					return "0|ARQUIVO - ".$_FILES["arquivo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_ARQUIVO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_ARQUIVO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["arquivo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_ARQUIVO_FILENAME_filename);
				
			}
			else
			{
				return "0|ARQUIVO - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_LAUDO = $mysqli->escape_String($_LAUDO);
		$sql_ARQUIVO_FILENAME_filename = $mysqli->escape_String($_ARQUIVO_FILENAME_filename);
		
		## Verifica se o registro do ID informado existe e se é do cliente prodfy indicado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id, AA.`arquivo_filename`
       FROM `PLANILHA_MODELO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idPLANILHA_MODELO` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idPLANILHA_MODELO, $o_ARQUIVO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `PLANILHA_MODELO` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `ano` = ?
		        ,`mes` = ?
		        ,`laudo` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idPLANILHA_MODELO` = ?"
		)) 
		{
			$stmt->bind_param('sddsss', $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_LAUDO, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza ARQUIVO
				if($_ARQUIVO_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `PLANILHA_MODELO` SET 
					        `arquivo_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idPLANILHA_MODELO` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_ARQUIVO_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_ARQUIVO_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_ARQUIVO_FILENAME); }
						}
						else
						{
							return "0|ARQUIVO - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `PLANILHA_MODELO` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idPLANILHA_MODELO` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPLOAD
	####
	function executa_UPLOAD($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		#Extensoes permitidas para a foto
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (
				(($_FILES["file"]["type"] == "image/gif")   || 
				 ($_FILES["file"]["type"] == "image/jpeg")  || 
				 ($_FILES["file"]["type"] == "image/jpg")   || 
				 ($_FILES["file"]["type"] == "image/pjpeg") || 
				 ($_FILES["file"]["type"] == "image/x-png") || 
				 ($_FILES["file"]["type"] == "image/png")) && 
				 ($_FILES["file"]["size"] < 80000) &&
				 in_array($extension, $allowedExts)
			) 
		{
			if ($_FILES["file"]["error"] > 0) 
			{
				//return "Return Code: " . $_FILES["file"]["error"] . "<br>";
				return "0|".$_FILES["file"]["error"]."|error|";
				exit;
			}
			else
			{
				## Carrega nome existente da foto de perfil do usuario
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT U.`profile_pic` as foto
           FROM `SYSTEM_USER_ACCOUNT` U
          WHERE U.`idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_REG_ID = $mysqli->escape_String($_REG_ID);
					$stmt->bind_param('s', $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_NOME_FOTO_ATUAL);
					
					$stmt->fetch();
					
				}
				else
				{
					return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|";
					exit;
				}
				
				
				## Gera nome unico para o arquivo e
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(PROFILE_PICS_PATH."/" . $filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["file"]["tmp_name"],PROFILE_PICS_PATH."/" . $filename);
				
				## Atualiza base de dados com o nome novo da foto
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"UPDATE `SYSTEM_USER_ACCOUNT`
				    SET  `profile_pic` = ?
				  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_PROFILE_PIC = $mysqli->escape_String($filename);
					$sql_REG_ID      = $mysqli->escape_String($_REG_ID);
					//
					$stmt->bind_param('ss', $sql_PROFILE_PIC, $sql_REG_ID);
					
					if($stmt->execute())
					{
						## Apaga foto antiga do repositorio
						if( !isEmpty($o_NOME_FOTO_ATUAL) && 
						    ($o_NOME_FOTO_ATUAL != "no_profile_pic.jpg") && 
						    file_exists(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL)
						  )
						{ unlink(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL); }
						
						## Retorna OK com o nome da foto nova
						return "1|".TXT_MOD_PERFIL_FOTO_ATUALIZADA."|success|".$filename."|";
						exit;
					}
					else
					{
						return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
						exit;
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
			}
		}
		else
		{
			//echo "Invalid file";
			return "0|".TXT_UPLOAD_ARQUIVO_INVALIDO."|error|";
			exit;
		}
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id, AA.`ano` as ano, AA.`mes` as mes, AA.`laudo` as laudo, lower(AA.`arquivo_filename`) as arquivo_filename
       FROM `PLANILHA_MODELO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idPLANILHA_MODELO` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_LAUDO, $o_ARQUIVO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_LAUDO."|".$o_ARQUIVO_FILENAME."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPLANILHA_MODELO` as id, AA.`ano` as ano, AA.`mes` as mes, AA.`laudo` as laudo, lower(AA.`arquivo_filename`) as arquivo_filename,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `PLANILHA_MODELO` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idPLANILHA_MODELO` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_LAUDO, $o_ARQUIVO_FILENAME, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_CFG_PLANILHA_MODELO_DEZ; }
				if( $o_LAUDO == "risco"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_ANALISE_DE_RISCO; }
				if( $o_LAUDO == "bio"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_BIOLOGICO; }
				if( $o_LAUDO == "calor"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_CALOR; }
				if( $o_LAUDO == "eletr"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_ELETRICIDADE; }
				if( $o_LAUDO == "expl"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_EXPLOSIVO; }
				if( $o_LAUDO == "infl"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_INFLAMAVEL; }
				if( $o_LAUDO == "part"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_PARTICULADO; }
				if( $o_LAUDO == "poei"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_POEIRA; }
				if( $o_LAUDO == "rad"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_RADIACAO; }
				if( $o_LAUDO == "rui"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_RUIDO; }
				if( $o_LAUDO == "vap"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VAPORES; }
				if( $o_LAUDO == "vbrvci"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VIBRACAO_VCI; }
				if( $o_LAUDO == "vbrvmb"){ $o_LAUDO_TXT = TXT_CFG_PLANILHA_MODELO_VIBRACAO_VMB; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_PLANILHA_MODELO_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_PLANILHA_MODELO_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_PLANILHA_MODELO_LAUDO.':</b></th><td>'.$o_LAUDO_TXT.'</td></tr>';
				if($o_ARQUIVO_FILENAME){ $o_ARQUIVO_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_ARQUIVO_FILENAME.'" target="_blank">'.$o_ARQUIVO_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_CFG_PLANILHA_MODELO_ARQUIVO.':</b></th><td>'.$o_ARQUIVO_FILENAME_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
