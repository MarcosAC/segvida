<?php
#################################################################################
## ME DEI BEM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: existe_colab.php
## Função..........: Verifica se o username de colabroador informado já está cadastrado
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_USERNAME = $IN_CPF = $IN_SITUACAO = "";
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_USERNAME   = test_input($_GET["username"]);
		
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		$IN_USERNAME       = test_input($_POST["u"]);
		$IN_CPF            = test_input($_POST["c"]);
		$IN_SITUACAO       = test_input($_POST["s"]);
		
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		$TMP_idSYSTEM_CLIENTE_ID = $_SESSION['user_idsystem_cliente'];
		
		## Define idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		if( isEmpty($TMP_idSYSTEM_CLIENTE_ID) )
		{
			$GO=0;
			die("0|".TXT_SYS_NO_SESSION."|alert|");
			exit;
		}
		
		## Autentica dados obrigatorios
		if( !isEmpty($IN_USERNAME) &&
		    !isEmpty($IN_CPF) &&
		    !isEmpty($IN_SITUACAO)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			$tmp_idSYSTEM_CLIENTE = $_SESSION['user_idsystem_cliente'];
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT C.`username` as username
         FROM `COLABORADOR` C
        WHERE upper(U.`username`) = upper(?)
          AND C.`idSYSTEM_CLIENTE` = ?"
			)) 
			{
				$sql_USERNAME         = $mysqli->escape_string($IN_USERNAME);
				$sql_idSYSTEM_CLIENTE = $mysqli->escape_string($tmp_idSYSTEM_CLIENTE);
				//
				$stmt->bind_param('ss', $sql_USERNAME, $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_USERNAME);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					die("0|".TXT_CFG_COLABS_USUARIO_INEXISTENTE."|alert|");
					exit; 
				}
				else
				{
					die("1|".TXT_CFG_COLABS_USUARIO_EXISTE."|success|");
					exit;
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
