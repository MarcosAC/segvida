<?php
#################################################################################
## SEGVIDA - GERA LAUDO FINAL
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_laudo_final_rui.php
## Função..........: Gerar planilha excel com os dados consolidados do laudo final
##                 
#################################################################################
###########
#####
##

include_once "includes/config.php";
include_once "includes/aux_lib.php";
	
####
# GERA ARQUIVO REMESSA
####
function GeraLaudoFinalRUI($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PLAN, $_LD_MOD, $_MODELO)
{
	## Gera escapes das variaveis
	$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
	$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
	$sql_ANO = $mysqli->escape_String($_ANO);
	$sql_MES = $mysqli->escape_String($_MES);
	$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
	$sql_DDMMAAAA = '%d/%m/%Y';
	$sql_LD_PLAN    = $mysqli->escape_String($_LD_PLAN);
	$sql_LD_MOD     = $mysqli->escape_String($_LD_MOD);
	
	$_RET_MSG = $_RET_FILE = "";
	
	## Carrega dados do laudo para a planilha indicada
	if ($stmt = $mysqli->prepare(
	"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, 
          AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, 
          AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
          AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, 
          date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, 
          AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_tarefa_1` as analise_obs_tarefa_1, 
          AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, 
          AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, 
          AA.`analise_obs_tarefa_2` as analise_obs_tarefa_2, AA.`analise_amostra_3` as analise_amostra_3, 
          date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, 
          AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_tarefa_3` as analise_obs_tarefa_3, 
          AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, 
          AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, 
          AA.`analise_obs_tarefa_4` as analise_obs_tarefa_4, AA.`analise_amostra_5` as analise_amostra_5, 
          date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, 
          AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_tarefa_5` as analise_obs_tarefa_5, 
          AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, 
          AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, 
          AA.`analise_obs_tarefa_6` as analise_obs_tarefa_6, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, 
          AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, 
          AA.`mitigacao` as mitigacao, AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, 
          date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_num_serial_1` as coleta_num_serial_1, 
          AA.`coleta_num_cert_calibr_1` as coleta_num_cert_calibr_1, AA.`coleta_tempo_amostragem_1` as coleta_tempo_amostragem_1, 
          AA.`coleta_dose_1` as coleta_dose_1, AA.`coleta_lavg_1` as coleta_lavg_1, AA.`coleta_twa_1` as coleta_twa_1, 
          AA.`coleta_nr09_1` as coleta_nr09_1, AA.`coleta_nr15_1` as coleta_nr15_1, AA.`coleta_acgih_1` as coleta_acgih_1, 
          AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, 
          AA.`coleta_num_serial_2` as coleta_num_serial_2, AA.`coleta_num_cert_calibr_2` as coleta_num_cert_calibr_2, 
          AA.`coleta_tempo_amostragem_2` as coleta_tempo_amostragem_2, AA.`coleta_dose_2` as coleta_dose_2, 
          AA.`coleta_lavg_2` as coleta_lavg_2, AA.`coleta_twa_2` as coleta_twa_2, AA.`coleta_nr09_2` as coleta_nr09_2, 
          AA.`coleta_nr15_2` as coleta_nr15_2, AA.`coleta_acgih_2` as coleta_acgih_2, AA.`coleta_amostra_3` as coleta_amostra_3, 
          date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_num_serial_3` as coleta_num_serial_3, 
          AA.`coleta_num_cert_calibr_3` as coleta_num_cert_calibr_3, AA.`coleta_tempo_amostragem_3` as coleta_tempo_amostragem_3, 
          AA.`coleta_dose_3` as coleta_dose_3, AA.`coleta_lavg_3` as coleta_lavg_3, AA.`coleta_twa_3` as coleta_twa_3, 
          AA.`coleta_nr09_3` as coleta_nr09_3, AA.`coleta_nr15_3` as coleta_nr15_3, AA.`coleta_acgih_3` as coleta_acgih_3, 
          AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, 
          AA.`coleta_num_serial_4` as coleta_num_serial_4, AA.`coleta_num_cert_calibr_4` as coleta_num_cert_calibr_4, 
          AA.`coleta_tempo_amostragem_4` as coleta_tempo_amostragem_4, AA.`coleta_dose_4` as coleta_dose_4, 
          AA.`coleta_lavg_4` as coleta_lavg_4, AA.`coleta_twa_4` as coleta_twa_4, AA.`coleta_nr09_4` as coleta_nr09_4, 
          AA.`coleta_nr15_4` as coleta_nr15_4, AA.`coleta_acgih_4` as coleta_acgih_4, AA.`coleta_amostra_5` as coleta_amostra_5, 
          date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_num_serial_5` as coleta_num_serial_5, 
          AA.`coleta_num_cert_calibr_5` as coleta_num_cert_calibr_5, AA.`coleta_tempo_amostragem_5` as coleta_tempo_amostragem_5, 
          AA.`coleta_dose_5` as coleta_dose_5, AA.`coleta_lavg_5` as coleta_lavg_5, AA.`coleta_twa_5` as coleta_twa_5, 
          AA.`coleta_nr09_5` as coleta_nr09_5, AA.`coleta_nr15_5` as coleta_nr15_5, AA.`coleta_acgih_5` as coleta_acgih_5, 
          AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, 
          AA.`coleta_num_serial_6` as coleta_num_serial_6, AA.`coleta_num_cert_calibr_6` as coleta_num_cert_calibr_6, 
          AA.`coleta_tempo_amostragem_6` as coleta_tempo_amostragem_6, AA.`coleta_dose_6` as coleta_dose_6, 
          AA.`coleta_lavg_6` as coleta_lavg_6, AA.`coleta_twa_6` as coleta_twa_6, AA.`coleta_nr09_6` as coleta_nr09_6, 
          AA.`coleta_nr15_6` as coleta_nr15_6, AA.`coleta_acgih_6` as coleta_acgih_6, AA.`cert_aprov` as cert_aprov, 
          AA.`nrrsf` as nrrsf, 
          concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
          concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
          upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, 
          lower(AA.`img_ativ_filename`) as img_ativ_filename, 
          lower(AA.`logo_filename`) as logo_filename
     FROM `RUIDO` AA
INNER JOIN `CLIENTE` C
       ON C.`idCLIENTE` = AA.`idCLIENTE`
INNER JOIN `COLABORADOR` CLB1
       ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U1
       ON U1.`username` = CLB1.`username`
INNER JOIN `COLABORADOR` CLB2
       ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U2
       ON U2.`username` = CLB2.`username`
    WHERE AA.`idSYSTEM_CLIENTE` = ?
      AND AA.`idRUIDO` = ?
      LIMIT 1"
	)) 
	{
		$stmt->bind_param('sssssssssssssss', $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_PLAN);
		$stmt->execute();
		$stmt->store_result();
		
		// obtém variáveis a partir dos resultados. 
		$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_TAREFA_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_TAREFA_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_TAREFA_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_TAREFA_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_TAREFA_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_TAREFA_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_MITIGACAO, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_NUM_SERIAL_1, $o_COLETA_NUM_CERT_CALIBR_1, $o_COLETA_TEMPO_AMOSTRAGEM_1, $o_COLETA_DOSE_1, $o_COLETA_LAVG_1, $o_COLETA_TWA_1, $o_COLETA_NR09_1, $o_COLETA_NR15_1, $o_COLETA_ACGIH_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_NUM_SERIAL_2, $o_COLETA_NUM_CERT_CALIBR_2, $o_COLETA_TEMPO_AMOSTRAGEM_2, $o_COLETA_DOSE_2, $o_COLETA_LAVG_2, $o_COLETA_TWA_2, $o_COLETA_NR09_2, $o_COLETA_NR15_2, $o_COLETA_ACGIH_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_NUM_SERIAL_3, $o_COLETA_NUM_CERT_CALIBR_3, $o_COLETA_TEMPO_AMOSTRAGEM_3, $o_COLETA_DOSE_3, $o_COLETA_LAVG_3, $o_COLETA_TWA_3, $o_COLETA_NR09_3, $o_COLETA_NR15_3, $o_COLETA_ACGIH_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_NUM_SERIAL_4, $o_COLETA_NUM_CERT_CALIBR_4, $o_COLETA_TEMPO_AMOSTRAGEM_4, $o_COLETA_DOSE_4, $o_COLETA_LAVG_4, $o_COLETA_TWA_4, $o_COLETA_NR09_4, $o_COLETA_NR15_4, $o_COLETA_ACGIH_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_NUM_SERIAL_5, $o_COLETA_NUM_CERT_CALIBR_5, $o_COLETA_TEMPO_AMOSTRAGEM_5, $o_COLETA_DOSE_5, $o_COLETA_LAVG_5, $o_COLETA_TWA_5, $o_COLETA_NR09_5, $o_COLETA_NR15_5, $o_COLETA_ACGIH_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_NUM_SERIAL_6, $o_COLETA_NUM_CERT_CALIBR_6, $o_COLETA_TEMPO_AMOSTRAGEM_6, $o_COLETA_DOSE_6, $o_COLETA_LAVG_6, $o_COLETA_TWA_6, $o_COLETA_NR09_6, $o_COLETA_NR15_6, $o_COLETA_ACGIH_6, $o_CERT_APROV, $o_NRRSF, $o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
		$stmt->fetch();
		
		// Formata Unescape de Textareas
		$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
		$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
		$o_ANALISE_OBS_TAREFA_1 = unescape_string($o_ANALISE_OBS_TAREFA_1);
		$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
		$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
		$o_ANALISE_OBS_TAREFA_2 = unescape_string($o_ANALISE_OBS_TAREFA_2);
		$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
		$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
		$o_ANALISE_OBS_TAREFA_3 = unescape_string($o_ANALISE_OBS_TAREFA_3);
		$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
		$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
		$o_ANALISE_OBS_TAREFA_4 = unescape_string($o_ANALISE_OBS_TAREFA_4);
		$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
		$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
		$o_ANALISE_OBS_TAREFA_5 = unescape_string($o_ANALISE_OBS_TAREFA_5);
		$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
		$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
		$o_ANALISE_OBS_TAREFA_6 = unescape_string($o_ANALISE_OBS_TAREFA_6);
		
		// Formata Datas Nulas
		if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
		if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
		if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
		if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
		if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
		if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
		if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
		
		
		## Formata Analises
		{
			$o_ANALISES_DATA = array(
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_1,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_1,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_1,
					'proc_prod'   => $o_ANALISE_PROC_PROD_1,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_1
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_2,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_2,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_2,
					'proc_prod'   => $o_ANALISE_PROC_PROD_2,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_2
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_3,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_3,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_3,
					'proc_prod'   => $o_ANALISE_PROC_PROD_3,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_3
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_4,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_4,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_4,
					'proc_prod'   => $o_ANALISE_PROC_PROD_4,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_4
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_5,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_5,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_5,
					'proc_prod'   => $o_ANALISE_PROC_PROD_5,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_5
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_6,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_6,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_6,
					'proc_prod'   => $o_ANALISE_PROC_PROD_6,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_6
				)
			);
		}
		
		## Formata Coletas
		{
			$o_COLETAS_DATA = array(
				array(
					'amostra'          => $o_COLETA_AMOSTRA_1,
					'data'             => $o_COLETA_DATA_1,
					'num_serial'       => $o_COLETA_NUM_SERIAL_1,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_1,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_1,
					'dose'             => $o_COLETA_DOSE_1,
					'lavg'             => $o_COLETA_LAVG_1,
					'twa'              => $o_COLETA_TWA_1,
					'nr09'             => $o_COLETA_NR09_1,
					'nr15'             => $o_COLETA_NR15_1,
					'acgih'            => $o_COLETA_ACGIH_1
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_2,
					'data'             => $o_COLETA_DATA_2,
					'num_serial'       => $o_COLETA_NUM_SERIAL_2,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_2,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_2,
					'dose'             => $o_COLETA_DOSE_2,
					'lavg'             => $o_COLETA_LAVG_2,
					'twa'              => $o_COLETA_TWA_2,
					'nr09'             => $o_COLETA_NR09_2,
					'nr15'             => $o_COLETA_NR15_2,
					'acgih'            => $o_COLETA_ACGIH_2
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_3,
					'data'             => $o_COLETA_DATA_3,
					'num_serial'       => $o_COLETA_NUM_SERIAL_3,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_3,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_3,
					'dose'             => $o_COLETA_DOSE_3,
					'lavg'             => $o_COLETA_LAVG_3,
					'twa'              => $o_COLETA_TWA_3,
					'nr09'             => $o_COLETA_NR09_3,
					'nr15'             => $o_COLETA_NR15_3,
					'acgih'            => $o_COLETA_ACGIH_3
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_4,
					'data'             => $o_COLETA_DATA_4,
					'num_serial'       => $o_COLETA_NUM_SERIAL_4,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_4,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_4,
					'dose'             => $o_COLETA_DOSE_4,
					'lavg'             => $o_COLETA_LAVG_4,
					'twa'              => $o_COLETA_TWA_4,
					'nr09'             => $o_COLETA_NR09_4,
					'nr15'             => $o_COLETA_NR15_4,
					'acgih'            => $o_COLETA_ACGIH_4
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_5,
					'data'             => $o_COLETA_DATA_5,
					'num_serial'       => $o_COLETA_NUM_SERIAL_5,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_5,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_5,
					'dose'             => $o_COLETA_DOSE_5,
					'lavg'             => $o_COLETA_LAVG_5,
					'twa'              => $o_COLETA_TWA_5,
					'nr09'             => $o_COLETA_NR09_5,
					'nr15'             => $o_COLETA_NR15_5,
					'acgih'            => $o_COLETA_ACGIH_5
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_6,
					'data'             => $o_COLETA_DATA_6,
					'num_serial'       => $o_COLETA_NUM_SERIAL_6,
					'num_cert_calibr'  => $o_COLETA_NUM_CERT_CALIBR_6,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_6,
					'dose'             => $o_COLETA_DOSE_6,
					'lavg'             => $o_COLETA_LAVG_6,
					'twa'              => $o_COLETA_TWA_6,
					'nr09'             => $o_COLETA_NR09_6,
					'nr15'             => $o_COLETA_NR15_6,
					'acgih'            => $o_COLETA_ACGIH_6
				)
			);
		}
		
		##Se nao encontrou dados, retorna
		if ($stmt->num_rows == 0) 
		{
			$_RET_MSG  = "Nenhum registro encontrado!";
			$_RET_FILE = "";
		}
		else
		{
			# Gera Planilha
			/** Include PHPExcel */
			require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
			
			# Create new PHPExcel object
			//$objPHPExcel = new PHPExcel();
			//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
			$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
			
			$locale = 'pt_br';
			$validLocale = PHPExcel_Settings::setLocale($locale);
			
			# Set document properties
			$objPHPExcel->getProperties()
			            ->setCreator("SEGVIDA")
			            ->setLastModifiedBy("SEGVIDA")
			            ->setTitle("Laudo Ruído")
			            ->setSubject("")
			            ->setDescription("Laudo Ruído")
			            ->setKeywords("segvida laudo ruído")
			            ->setCategory("Laudo");
			
			## IMAGEM LOGOMARCA
			if($o_LOGO_FILENAME)
			{
				list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
				
				$img_logo_type2 = $_img_type[$img_logo_type];
				$img_logo_width_points  = $img_logo_width * 0.75;
				$img_logo_height_points = $img_logo_height * 0.75;
				
				/*
				if($_DEBUG == 1)
				{
					$img_logo_type2 = $_img_type[$img_logo_type];
					error_log("geralaudo_final_rui.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
				}
				*/
				
				switch($img_logo_type)
				{
					case '1'://gif
						$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '2'://jpg
						$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_logo');
				$objDrawing->setDescription('img_logo');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_logo_width_points);
				$objDrawing->setHeight($img_logo_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A1');
				$objDrawing->setImageResource($gd_img_logo);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			## EMPRESA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J1', $o_CLIENTE);
			
			## PLANILHA_NUM
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V1', $o_PLANILHA_NUM);
			
			## ANO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('W1', $o_ANO);
			
			## UNIDADE
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J2', $o_UNIDADE_SITE);
			
			## DATA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V2', $o_DATA_ELABORACAO);
			
			## AREA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J3', $o_AREA);
			
			## SETOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('O3', $o_SETOR);
			
			## GES
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V3', $o_GES);
			
			## CARGO_FUNCAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J4', $o_CARGO_FUNCAO);
			
			## CBO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V4', $o_CBO);
			
			## ATIV_MACRO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A6', $o_ATIV_MACRO);
			
			####
			# ANALISES
			$analises_total = count($o_ANALISES_DATA);
			for($a=0;$a<$analises_total;$a++)
			{
				$ITEM = (array) $o_ANALISES_DATA[$a];
				
				//$o_ANALISES_DATA[$a][]
				
				if($ITEM['amostra'])
				{
					$aa = 11 + $a;
					
					## ANALISE_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['amostra']);
					
					## ANALISE_DATA_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('B'.$aa, $ITEM['data']);
					
					## ANALISE_TAREFA_EXEC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['tarefa_exec']);
					
					## ANALISE_PROC_PROD
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('K'.$aa, $ITEM['proc_prod']);
					
					## ANALISE_OBS_TAREFA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('R'.$aa, $ITEM['obs_tarefa']);
				}
			}
			
			## JOR_TRAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A19', $o_JOR_TRAB);
			
			## TEMPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D19', $o_TEMPO_EXPO);
			
			## TIPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G19', $o_TIPO_EXPO);
			
			## MEIO_PROPAG
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J19', $o_MEIO_PROPAG);
			
			## FONTE_GERADORA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M19', $o_FONTE_GERADORA);
			
			## EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R19', $o_MITIGACAO);
			
			####
			# COLETAS
			$aa=0;
			$coletas_total = count($o_COLETAS_DATA);
			for($a=0;$a<$coletas_total;$a++)
			{
				$ITEM = (array) $o_COLETAS_DATA[$a];
				
				$aa = 22 + $a;
				
				if($ITEM['amostra'])
				{
					## COLETA_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['amostra']);
					
					## COLETA_DATA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('B'.$aa, $ITEM['data']);
					
					## COLETA_NUM_SERIAL
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['num_serial']);
					
					## COLETA_NUM_CERT_CALIBR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('G'.$aa, $ITEM['num_cert_calibr']);
					
					## COLETA_TEMPO_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('K'.$aa, $ITEM['tempo_amostragem']);
					
					## COLETA_DOSE
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('M'.$aa, $ITEM['dose']);
					
					## COLETA_LAVG
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('O'.$aa, $ITEM['lavg']);
					
					## COLETA_TWA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('Q'.$aa, $ITEM['twa']);
					
					## COLETA_NR09
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('S'.$aa, $ITEM['nr09']);
					
					## COLETA_NR15
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('U'.$aa, $ITEM['nr15']);
					
					## COLETA_ACGIH
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('W'.$aa, $ITEM['acgih']);
				}
			}
			
			## CERT_APROV
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('C28', $o_CERT_APROV);
			
			## NRRSF
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G28', $o_NRRSF);
			
			## RESP_CAMPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D54', $o_RESP_CAMPO);
			
			## RESP_TECNICO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P54', $o_RESP_TECNICO);
			
			## REGISTRO_RC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D55', $o_REGISTRO_RC);
			
			## REGISTRO_RT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P55', $o_REGISTRO_RT);
			
			## IMAGEM ATIVIDADE
			if($o_IMG_ATIV_FILENAME)
			{
				list($img_ativ_width, $img_ativ_height, $img_ativ_type, $img_ativ_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
				
				$img_ativ_width_points  = $img_ativ_width * 0.75;
				$img_ativ_height_points = $img_ativ_height * 0.75;
				
				switch($img_ativ_type)
				{
					case '1'://gif
						$gd_img_ativ = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '2'://jpg
						$gd_img_ativ = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_ativ');
				$objDrawing->setDescription('img_ativ');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_ativ_width_points);
				$objDrawing->setHeight($img_ativ_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A32');
				$objDrawing->setImageResource($gd_img_ativ);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Laudo Particulado');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
			
			
			// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			
			## Grava arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			try 
			{
				$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
				$_RET_MSG  = "";
				$_RET_FILE = $_FILENAME;
			} 
			catch (Exception $e) 
			{
				error_log("gera_laudo_final_rui.php:\n\nobjWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME); -> ".$e->getMessage()."\n\n",0);
			}
			
		}
	}
	else
	{
		if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
		      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
		exit;
	}
	
	//error_log("gera_laudo_final_rui.php:\n\n returning -> ".$_RET_MSG."|".$_RET_FILE."|\n\n",0);
	
	return $_RET_MSG."|".$_RET_FILE."|";
	
}


#################################################################################
###########
#####
##
?>
