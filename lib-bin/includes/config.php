<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: config.php
## Função..........: Definicao de constantes do sistema
## Funcionalidade..: Carregado inicialmente pelo main.php para definição de 
##                   variáveis constantes do sistema.
## Utilizacao......: require_once "MAIN_CGI_PATH/includes/config.php";
## Pré-requisitos..: Nenhum
##
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Configuracoes iniciais
#################################################################################

#define("CAN_REGISTER", "any");
#define("DEFAULT_ROLE", "member");
#define("SECURE", FALSE);    // ESTRITAMENTE PARA DESENVOLVIMENTO!!!!

## PATHs
define("MAIN_PATH"            ,"/home/rlgom197/public_html/segvida");
define("CGI_PATH"             ,MAIN_PATH."/lib-bin");
define("LIB_PATH"             ,MAIN_PATH."/lib-bin");
define("INCLUDES_PATH"        ,CGI_PATH."/includes");
define("IMG_PATH"             ,MAIN_PATH."/img");
define("PDF_PATH"             ,MAIN_PATH."/pdf_docs");//path para os arquivos pdf
define("PHPEXCEL_PATH"        ,INCLUDES_PATH."/PHPExcel-1.8");
define("ANEXOS_PATH"          ,MAIN_PATH."/anexos_docs");//path para os arquivos pdf dos anexos
define("LAUDO_FINAL_PATH"     ,MAIN_PATH."/laudo_final_docs");//path para os arquivos pdf
define("LANGUAGE_FOLDER_PATH" ,INCLUDES_PATH."/language");
define("PROFILE_PICS_PATH"    ,IMG_PATH."/profile_pics");
define("BOLETO_PATH"          ,MAIN_PATH."/boleto");
define("BOLETO_PICS_PATH"     ,MAIN_PATH."/boleto/imagens");
define("EMAIL_TEMPLATE_PATH"  ,INCLUDES_PATH."/email_templates");


## Caminho para o calendário do linux
#define("CALC_PATH"     ,"/usr/bin/cal";

## Endereço web do site
define("DOMAIN"      ,"segvida.prodfy.com.br");
define("WEB_ADDRESS" ,"segvida.prodfy.com.br");

##SECURITY
define("SECURE", FALSE);
define("SID_NAME", "SID-SEGVIDA");

## Segundos para expirar a sessao
## 1800 - 30 minutos
## 2700 - 45 minutos
## 3600 - 1 hora
## 7200 - 2 horas
## 14400 - 4 horas
define("SID_EXPIRE_SECONDS", 7200);

## Endereco web do site da paypal
//define("PAY_PAL_WEB_ADDRESS", "www.paypal.com");

## Endereço web do main.cgi
#define("FORM_ACTION", "/lib-bin/index.php");

## Endereco web do boleto
define("BOLETO_URL", "/boleto/boleto.php");

## Endereço web das imagens
define("IMG_CGI","/img");
define("IMG_URL",WEB_ADDRESS."/img");
define("PROFILE_PICS_URL",IMG_CGI."/profile_pics");

## Endereco web dos documentos PDF
define("PDF_CGI","/pdf_docs");
define("PDF_URL",WEB_ADDRESS."/pdf_docs");

## Endereco web dos anexos
define("ANEXOS_CGI","/anexos_docs");
define("ANEXOS_URL",WEB_ADDRESS."/anexos_docs");

## Endereco web dos laudos finais
define("LAUDO_FINAL_CGI","/anexos_docs");
define("LAUDO_FINAL_URL",WEB_ADDRESS."/laudo_final_docs");

## Endereço web dos javascripts
define("JS_CGI"                ,"/js");
define("JS_URL"                ,WEB_ADDRESS."/js");
define("JQUERY_FILENAME"       ,"jquery.min.js");
#define("AJAX_CHARSET_ENCODING" ,"utf-8");

####
# Templates de emails para envio -> Estão no language!!!
####

####
# APP CONFIG
####
# QRLogo URL
define("QRLOGO_URL","http://".WEB_ADDRESS."/lib-bin/qrlogo.php");
# Configuracao QR Code
define("QRLOGO_IMG_URL","http://".IMG_URL."/kondoo_logo_qr.png");
# Sincr URL
define("APP_SINC_URL",WEB_ADDRESS."/lib-bin/app.php");


## Definição da chave de criptografia do arquivo de sessão
## OBS.: Caso a decryptacao nao esteja ocorrendo apropriadamente, verificar o texto gravado dentro do arquivo de sessao. 
##       Provavelmente a chave usada esta gerando quebra de linha na hora de gravar o texto no arquivo.
##       solucao: altere a chave ate encontrar uma que nao cause a anomalia
#define("CRYPT_KEY", "z0n4");
#define("HAS_TO_ENCRYPT_DATA", 0);

## Caminho da pasta onde os arquivos upfile CGI serão estocados
#$TempFile::TMPDIRECTORY = '/tmp';
#$UPFILE_PATH            = '/tmp';

## Configuracao do Banco de Dados
define("DB_HOST"     ,"localhost");
define("DB_USERNAME" ,"rlgom197_cgi");
define("DB_PASSWORD" ,"4m4th3r4su");
define("DB_DATABASE" ,"rlgom197_segvida");

## Fone Empresa
#define("FONE_EMPRESA", "32988891566");

## Automacao de envio de emails do sistema
define("EMAIL_SISTEMA"              ,"system@rlgomide.com");
define("EMAIL_SISTEMA_NOME"         ,"SEGVIDA");
define("EMAIL_SISTEMA_NOREPLY"      ,"noreply@rlgomide.com");
define("EMAIL_SISTEMA_NOREPLY_NOME" ,"No-Reply");
define("EMAIL_CONTATO"              ,"kondoo@rlgomide.com");
define("EMAIL_CONTATO_NOME"         ,"contato");
define("EMAIL_CONTATO_BCC"          ,"rlgomide@gmail.com");
define("EMAIL_CONTATO_BCC_NOME"     ,"");
define("EMAIL_SUPORTE"              ,"suporte@rlgomide.com");
define("EMAIL_SUPORTE_NOME"         ,"Suporte SEGVIDA");
define("EMAIL_FINANCEIRO"           ,"fincanceiro@rlgomide.com");
define("EMAIL_FINANCEIRO_NOME"      ,"Financeiro SEGVIDA");
define("EMAIL_MARKETING"            ,"marketing@rlgomide.com");
define("EMAIL_MARKETING_NOME"       ,"Marketing SEGVIDA");
define("EMAIL_AVISO_ADMIN_REGISTRO" ,"romulo@thersistemas.com.br");
define("TXT_EMAIL_AVISO_ADMIN_REGISTRO_SUBJECT" ,"SEGVIDA - Novo Registro de Cliente");

##To Send Email
#public $SENDMAIL_SMTP = "mail.rlgomide.com";
#public $SENDMAIL_PORT = "587";
#public $SENDMAIL_USER = "system@rlgomide.com";
#public $SENDMAIL_PASS = "26101974";

##SMTP to send mail
define("SMTP_AUTH", true);
define("SMTP_HOST", "br50.hostgator.com.br");
define("SMTP_PORT", 465);//"587";
define("SMTP_USER", "system@rlgomide.com");
define("SMTP_PASS", "26101974");


#################################################################################
###########
#####
##
