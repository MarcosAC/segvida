﻿<?php
#################################################################################
## SEGVIDA - GERA LAUDO FINAL
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_laudo_final_calor.php
## Função..........: Gerar planilha excel com os dados consolidados do laudo final
##                 
#################################################################################
###########
#####
##

include_once "includes/config.php";
include_once "includes/aux_lib.php";
	
####
# GERA ARQUIVO REMESSA
####
function GeraLaudoFinalCALOR($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PLAN, $_LD_MOD, $_MODELO)
{
	## Gera escapes das variaveis
	$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
	$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
	$sql_ANO = $mysqli->escape_String($_ANO);
	$sql_MES = $mysqli->escape_String($_MES);
	$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
	$sql_DDMMAAAA = '%d/%m/%Y';
	$sql_LD_PLAN    = $mysqli->escape_String($_LD_PLAN);
	$sql_LD_MOD     = $mysqli->escape_String($_LD_MOD);
	
	$_RET_MSG = $_RET_FILE = "";
	
	## Carrega dados do laudo para a planilha indicada
	if ($stmt = $mysqli->prepare(
	"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
          AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_pert_1` as analise_obs_pert_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, AA.`analise_obs_pert_2` as analise_obs_pert_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_pert_3` as analise_obs_pert_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, AA.`analise_obs_pert_4` as analise_obs_pert_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_pert_5` as analise_obs_pert_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, AA.`analise_obs_pert_6` as analise_obs_pert_6, 
          AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, AA.`epi` as epi, AA.`disp_1` as disp_1, AA.`disp_2` as disp_2, AA.`ref_lt` as ref_lt, AA.`qdr_comp` as qdr_comp, AA.`metab` as metab, 
          AA.`coletas` as coletas, 
          AA.`coleta_passo_1` as coleta_passo_1, AA.`coleta_carga_solar_1` as coleta_carga_solar_1, AA.`coleta_tipo_ativ_1` as coleta_tipo_ativ_1, AA.`coleta_temp_passo_1` as coleta_temp_passo_1, AA.`coleta_descr_tarefa_passo_1` as coleta_descr_tarefa_passo_1, AA.`coleta_tbnm_1` as coleta_tbnm_1, AA.`coleta_tbsm_1` as coleta_tbsm_1, AA.`coleta_tgm_1` as coleta_tgm_1, AA.`coleta_ur_1` as coleta_ur_1, AA.`coleta_var_1` as coleta_var_1, AA.`coleta_m_1` as coleta_m_1, lower(AA.`coleta_img_ativ_filename_1`) as coleta_img_ativ_filename_1, 
          AA.`coleta_passo_2` as coleta_passo_2, AA.`coleta_carga_solar_2` as coleta_carga_solar_2, AA.`coleta_tipo_ativ_2` as coleta_tipo_ativ_2, AA.`coleta_temp_passo_2` as coleta_temp_passo_2, AA.`coleta_descr_tarefa_passo_2` as coleta_descr_tarefa_passo_2, AA.`coleta_tbnm_2` as coleta_tbnm_2, AA.`coleta_tbsm_2` as coleta_tbsm_2, AA.`coleta_tgm_2` as coleta_tgm_2, AA.`coleta_ur_2` as coleta_ur_2, AA.`coleta_var_2` as coleta_var_2, AA.`coleta_m_2` as coleta_m_2, lower(AA.`coleta_img_ativ_filename_2`) as coleta_img_ativ_filename_2, 
          AA.`coleta_passo_3` as coleta_passo_3, AA.`coleta_carga_solar_3` as coleta_carga_solar_3, AA.`coleta_tipo_ativ_3` as coleta_tipo_ativ_3, AA.`coleta_temp_passo_3` as coleta_temp_passo_3, AA.`coleta_descr_tarefa_passo_3` as coleta_descr_tarefa_passo_3, AA.`coleta_tbnm_3` as coleta_tbnm_3, AA.`coleta_tbsm_3` as coleta_tbsm_3, AA.`coleta_tgm_3` as coleta_tgm_3, AA.`coleta_ur_3` as coleta_ur_3, AA.`coleta_var_3` as coleta_var_3, AA.`coleta_m_3` as coleta_m_3, lower(AA.`coleta_img_ativ_filename_3`) as coleta_img_ativ_filename_3, 
          AA.`coleta_passo_4` as coleta_passo_4, AA.`coleta_carga_solar_4` as coleta_carga_solar_4, AA.`coleta_tipo_ativ_4` as coleta_tipo_ativ_4, AA.`coleta_temp_passo_4` as coleta_temp_passo_4, AA.`coleta_descr_tarefa_passo_4` as coleta_descr_tarefa_passo_4, AA.`coleta_tbnm_4` as coleta_tbnm_4, AA.`coleta_tbsm_4` as coleta_tbsm_4, AA.`coleta_tgm_4` as coleta_tgm_4, AA.`coleta_ur_4` as coleta_ur_4, AA.`coleta_var_4` as coleta_var_4, AA.`coleta_m_4` as coleta_m_4, lower(AA.`coleta_img_ativ_filename_4`) as coleta_img_ativ_filename_4, 
          AA.`coleta_passo_5` as coleta_passo_5, AA.`coleta_carga_solar_5` as coleta_carga_solar_5, AA.`coleta_tipo_ativ_5` as coleta_tipo_ativ_5, AA.`coleta_temp_passo_5` as coleta_temp_passo_5, AA.`coleta_descr_tarefa_passo_5` as coleta_descr_tarefa_passo_5, AA.`coleta_tbnm_5` as coleta_tbnm_5, AA.`coleta_tbsm_5` as coleta_tbsm_5, AA.`coleta_tgm_5` as coleta_tgm_5, AA.`coleta_ur_5` as coleta_ur_5, AA.`coleta_var_5` as coleta_var_5, AA.`coleta_m_5` as coleta_m_5, lower(AA.`coleta_img_ativ_filename_5`) as coleta_img_ativ_filename_5, 
          AA.`coleta_passo_6` as coleta_passo_6, AA.`coleta_carga_solar_6` as coleta_carga_solar_6, AA.`coleta_tipo_ativ_6` as coleta_tipo_ativ_6, AA.`coleta_temp_passo_6` as coleta_temp_passo_6, AA.`coleta_descr_tarefa_passo_6` as coleta_descr_tarefa_passo_6, AA.`coleta_tbnm_6` as coleta_tbnm_6, AA.`coleta_tbsm_6` as coleta_tbsm_6, AA.`coleta_tgm_6` as coleta_tgm_6, AA.`coleta_ur_6` as coleta_ur_6, AA.`coleta_var_6` as coleta_var_6, AA.`coleta_m_6` as coleta_m_6, lower(AA.`coleta_img_ativ_filename_6`) as coleta_img_ativ_filename_6, 
          concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
          concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
          upper(AA.`registro_rc`) as registro_rc,
          upper(AA.`registro_rt`) as registro_rt,
          lower(AA.`logo_filename`) as logo_filename
     FROM `CALOR` AA
INNER JOIN `CLIENTE` C
       ON C.`idCLIENTE` = AA.`idCLIENTE`
INNER JOIN `COLABORADOR` CLB1
       ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U1
       ON U1.`username` = CLB1.`username`
INNER JOIN `COLABORADOR` CLB2
       ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U2
       ON U2.`username` = CLB2.`username`
    WHERE AA.`idSYSTEM_CLIENTE` = ?
      AND AA.`idCALOR` = ?
      LIMIT 1"
	)) 
	{
		$stmt->bind_param('sssssssss', $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_PLAN);
		$stmt->execute();
		$stmt->store_result();
		
		// obtém variáveis a partir dos resultados. 
		$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, 
		$o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_PERT_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_PERT_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_PERT_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_PERT_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_PERT_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_PERT_6, 
		$o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_EPI, $o_DISP_1, $o_DISP_2, $o_REF_LT, $o_QDR_COMP, $o_METAB, 
		$o_COLETAS, $o_COLETA_PASSO_1, $o_COLETA_CARGA_SOLAR_1, $o_COLETA_TIPO_ATIV_1, $o_COLETA_TEMP_PASSO_1, $o_COLETA_DESCR_TAREFA_PASSO_1, $o_COLETA_TBNM_1, $o_COLETA_TBSM_1, $o_COLETA_TGM_1, $o_COLETA_UR_1, $o_COLETA_VAR_1, $o_COLETA_M_1, $o_COLETA_IMG_ATIV_FILENAME_1, $o_COLETA_PASSO_2, $o_COLETA_CARGA_SOLAR_2, $o_COLETA_TIPO_ATIV_2, $o_COLETA_TEMP_PASSO_2, $o_COLETA_DESCR_TAREFA_PASSO_2, $o_COLETA_TBNM_2, $o_COLETA_TBSM_2, $o_COLETA_TGM_2, $o_COLETA_UR_2, $o_COLETA_VAR_2, $o_COLETA_M_2, $o_COLETA_IMG_ATIV_FILENAME_2, $o_COLETA_PASSO_3, $o_COLETA_CARGA_SOLAR_3, $o_COLETA_TIPO_ATIV_3, $o_COLETA_TEMP_PASSO_3, $o_COLETA_DESCR_TAREFA_PASSO_3, $o_COLETA_TBNM_3, $o_COLETA_TBSM_3, $o_COLETA_TGM_3, $o_COLETA_UR_3, $o_COLETA_VAR_3, $o_COLETA_M_3, $o_COLETA_IMG_ATIV_FILENAME_3, $o_COLETA_PASSO_4, $o_COLETA_CARGA_SOLAR_4, $o_COLETA_TIPO_ATIV_4, $o_COLETA_TEMP_PASSO_4, $o_COLETA_DESCR_TAREFA_PASSO_4, $o_COLETA_TBNM_4, $o_COLETA_TBSM_4, $o_COLETA_TGM_4, $o_COLETA_UR_4, $o_COLETA_VAR_4, $o_COLETA_M_4, $o_COLETA_IMG_ATIV_FILENAME_4, $o_COLETA_PASSO_5, $o_COLETA_CARGA_SOLAR_5, $o_COLETA_TIPO_ATIV_5, $o_COLETA_TEMP_PASSO_5, $o_COLETA_DESCR_TAREFA_PASSO_5, $o_COLETA_TBNM_5, $o_COLETA_TBSM_5, $o_COLETA_TGM_5, $o_COLETA_UR_5, $o_COLETA_VAR_5, $o_COLETA_M_5, $o_COLETA_IMG_ATIV_FILENAME_5, $o_COLETA_PASSO_6, $o_COLETA_CARGA_SOLAR_6, $o_COLETA_TIPO_ATIV_6, $o_COLETA_TEMP_PASSO_6, $o_COLETA_DESCR_TAREFA_PASSO_6, $o_COLETA_TBNM_6, $o_COLETA_TBSM_6, $o_COLETA_TGM_6, $o_COLETA_UR_6, $o_COLETA_VAR_6, $o_COLETA_M_6, $o_COLETA_IMG_ATIV_FILENAME_6, 
		$o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_LOGO_FILENAME);
		$stmt->fetch();
		
		// Formata Unescape de Textareas
		$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
		$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
		$o_ANALISE_OBS_PERT_1 = unescape_string($o_ANALISE_OBS_PERT_1);
		$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
		$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
		$o_ANALISE_OBS_PERT_2 = unescape_string($o_ANALISE_OBS_PERT_2);
		$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
		$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
		$o_ANALISE_OBS_PERT_3 = unescape_string($o_ANALISE_OBS_PERT_3);
		$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
		$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
		$o_ANALISE_OBS_PERT_4 = unescape_string($o_ANALISE_OBS_PERT_4);
		$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
		$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
		$o_ANALISE_OBS_PERT_5 = unescape_string($o_ANALISE_OBS_PERT_5);
		$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
		$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
		$o_ANALISE_OBS_PERT_6 = unescape_string($o_ANALISE_OBS_PERT_6);
		$o_COLETA_DESCR_TAREFA_PASSO_1 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_1);
		$o_COLETA_DESCR_TAREFA_PASSO_2 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_2);
		$o_COLETA_DESCR_TAREFA_PASSO_3 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_3);
		$o_COLETA_DESCR_TAREFA_PASSO_4 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_4);
		$o_COLETA_DESCR_TAREFA_PASSO_5 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_5);
		$o_COLETA_DESCR_TAREFA_PASSO_6 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_6);
		
		// Formata Datas Nulas
		if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
		
		## Formata Analises
		{
			$o_ANALISES_DATA = array(
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_1,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_1,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_1,
					'proc_prod'   => $o_ANALISE_PROC_PROD_1,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_1
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_2,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_2,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_2,
					'proc_prod'   => $o_ANALISE_PROC_PROD_2,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_2
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_3,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_3,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_3,
					'proc_prod'   => $o_ANALISE_PROC_PROD_3,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_3
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_4,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_4,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_4,
					'proc_prod'   => $o_ANALISE_PROC_PROD_4,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_4
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_5,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_5,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_5,
					'proc_prod'   => $o_ANALISE_PROC_PROD_5,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_5
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_6,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_6,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_6,
					'proc_prod'   => $o_ANALISE_PROC_PROD_6,
					'obs_tarefa'  => $o_ANALISE_OBS_PERT_6
				)
			);
		}
		
		## Formata Coletas
		{
			$o_COLETAS_DATA = array(
				array(
					'passo'              => $o_COLETA_PASSO_1,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_1,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_1,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_1,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_1,
					'tbnm'               => $o_COLETA_TBNM_1,
					'tbsm'               => $o_COLETA_TBSM_1,
					'tgm'                => $o_COLETA_TGM_1,
					'ur'                 => $o_COLETA_UR_1,
					'var'                => $o_COLETA_VAR_1,
					'm'                  => $o_COLETA_M_1,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_1
				),
				array(
					'passo'              => $o_COLETA_PASSO_2,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_2,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_2,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_2,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_2,
					'tbnm'               => $o_COLETA_TBNM_2,
					'tbsm'               => $o_COLETA_TBSM_2,
					'tgm'                => $o_COLETA_TGM_2,
					'ur'                 => $o_COLETA_UR_2,
					'var'                => $o_COLETA_VAR_2,
					'm'                  => $o_COLETA_M_2,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_2
				),
				array(
					'passo'              => $o_COLETA_PASSO_3,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_3,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_3,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_3,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_3,
					'tbnm'               => $o_COLETA_TBNM_3,
					'tbsm'               => $o_COLETA_TBSM_3,
					'tgm'                => $o_COLETA_TGM_3,
					'ur'                 => $o_COLETA_UR_3,
					'var'                => $o_COLETA_VAR_3,
					'm'                  => $o_COLETA_M_3,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_3
				),
				array(
					'passo'              => $o_COLETA_PASSO_4,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_4,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_4,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_4,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_4,
					'tbnm'               => $o_COLETA_TBNM_4,
					'tbsm'               => $o_COLETA_TBSM_4,
					'tgm'                => $o_COLETA_TGM_4,
					'ur'                 => $o_COLETA_UR_4,
					'var'                => $o_COLETA_VAR_4,
					'm'                  => $o_COLETA_M_4,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_4
				),
				array(
					'passo'              => $o_COLETA_PASSO_5,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_5,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_5,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_5,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_5,
					'tbnm'               => $o_COLETA_TBNM_5,
					'tbsm'               => $o_COLETA_TBSM_5,
					'tgm'                => $o_COLETA_TGM_5,
					'ur'                 => $o_COLETA_UR_5,
					'var'                => $o_COLETA_VAR_5,
					'm'                  => $o_COLETA_M_5,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_5
				),
				array(
					'passo'              => $o_COLETA_PASSO_6,
					'carga_solar'        => $o_COLETA_CARGA_SOLAR_6,
					'tipo_ativ'          => $o_COLETA_TIPO_ATIV_6,
					'temp_passo'         => $o_COLETA_TEMP_PASSO_6,
					'descr_tarefa_passo' => $o_COLETA_DESCR_TAREFA_PASSO_6,
					'tbnm'               => $o_COLETA_TBNM_6,
					'tbsm'               => $o_COLETA_TBSM_6,
					'tgm'                => $o_COLETA_TGM_6,
					'ur'                 => $o_COLETA_UR_6,
					'var'                => $o_COLETA_VAR_6,
					'm'                  => $o_COLETA_M_6,
					'img_ativ'           => $o_COLETA_IMG_ATIV_FILENAME_6
				)
			);
		}
		
		##Se nao encontrou dados, retorna
		if ($stmt->num_rows == 0) 
		{
			$_RET_MSG  = "Nenhum registro encontrado!";
			$_RET_FILE = "";
		}
		else
		{
			# Gera Planilha
			/** Include PHPExcel */
			require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
			
			# Create new PHPExcel object
			//$objPHPExcel = new PHPExcel();
			//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
			$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
			
			$locale = 'pt_br';
			$validLocale = PHPExcel_Settings::setLocale($locale);
			
			# Set document properties
			$objPHPExcel->getProperties()
			            ->setCreator("SEGVIDA")
			            ->setLastModifiedBy("SEGVIDA")
			            ->setTitle("Laudo Calor")
			            ->setSubject("")
			            ->setDescription("Laudo Calor")
			            ->setKeywords("segvida laudo calor")
			            ->setCategory("Laudo");
			
			## IMAGEM LOGOMARCA
			if($o_LOGO_FILENAME)
			{
				list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
				
				$img_logo_type2 = $_img_type[$img_logo_type];
				$img_logo_width_points  = $img_logo_width * 0.75;
				$img_logo_height_points = $img_logo_height * 0.75;
				
				
				if($_DEBUG == 1)
				{
					$img_logo_type2 = $_img_type[$img_logo_type];
					error_log("geralaudo_final_calor.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
				}
				
				
				switch($img_logo_type)
				{
					case '1'://gif
						$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '2'://jpg
						$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_logo');
				$objDrawing->setDescription('img_logo');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_logo_width_points);
				$objDrawing->setHeight($img_logo_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A1');
				$objDrawing->setImageResource($gd_img_logo);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			
			
			## EMPRESA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J1', $o_CLIENTE);
			
			## PLANILHA_NUM
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V1', $o_PLANILHA_NUM);
			
			## ANO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('W1', $o_ANO);
			
			## UNIDADE
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J2', $o_UNIDADE_SITE);
			
			## DATA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V2', $o_DATA_ELABORACAO);
			
			## AREA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J3', $o_AREA);
			
			## SETOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('O3', $o_SETOR);
			
			## GES
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V3', $o_GES);
			
			## CARGO_FUNCAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J4', $o_CARGO_FUNCAO);
			
			## CBO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V4', $o_CBO);
			
			## ATIV_MACRO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A6', $o_ATIV_MACRO);
			
			####
			# ANALISES
			$analises_total = count($o_ANALISES_DATA);
			for($a=0;$a<$analises_total;$a++)
			{
				$ITEM = (array) $o_ANALISES_DATA[$a];
				
				if($ITEM['amostra'])
				{
					//$o_ANALISES_DATA[$a][]
					
					$aa = 11 + $a;
					
					## ANALISE_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['amostra']);
					
					## ANALISE_DATA_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('B'.$aa, $ITEM['data']);
					
					## ANALISE_TAREFA_EXEC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['tarefa_exec']);
					
					## ANALISE_PROC_PROD
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('K'.$aa, $ITEM['proc_prod']);
					
					## ANALISE_OBS_PERT
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('R'.$aa, $ITEM['obs_tarefa']);
				}
			}
			
			//## TAREFA_EXEC
			//$objPHPExcel->getActiveSheet()
			//            ->SetCellValue('A11', $o_TAREFA_EXEC);
			
			## JOR_TRAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A19', $o_JOR_TRAB);
			
			## TEMPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D19', $o_TEMPO_EXPO);
			
			## TIPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G19', $o_TIPO_EXPO);
			
			## MEIO_PROPAG
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J19', $o_MEIO_PROPAG);
			
			## FONTE_GERADORA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M19', $o_FONTE_GERADORA);
			
			## EPC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R19', $o_EPC);
			
			## EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D20', $o_EPI);
			
			## DISP_1
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A23', $o_DISP_1);
			
			## DISP_2
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M23', $o_DISP_2);
			
			## REF_LT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('O26', $o_REF_LT);
			
			## QDR_COMP
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R26', $o_QDR_COMP);
			
			## METAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V26', $o_METAB);
			
			####
			# COLETAS
			$aa=0;
			$coletas_total = count($o_COLETAS_DATA);
			for($a=0;$a<$coletas_total;$a++)
			{
				$ITEM = (array) $o_COLETAS_DATA[$a];
				
				if($ITEM['passo'])
				{
					if($a == 0)
					{
						$aa = 29; //Primeira posicao na planilha
					}
					elseif($a == 1)
					{
						$aa = 38; //A partir da segunda posicao na planilha
					}
					else
					{
						$aa = 30 + ($a * 9); //A partir da terceira posicao na planilha
					}
					
					## COLETA_PASSO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['passo']);
					
					## COLETA_CARGA_SOLAR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['carga_solar']);
					
					## COLETA_TIPO_ATIV
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('I'.$aa, $ITEM['tipo_ativ']);
					
					## COLETA_TEMP_PASSO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('O'.$aa, $ITEM['temp_passo']);
					
					$bb = $aa+2;
					
					//error_log("gera_laudo_final_calor.php:\n\nCOLETA #".$a." - img_ativ_".$a." -> ".$ITEM['img_ativ']."\n\n",0);
					
					## COLETA_IMG ATIV
					if($ITEM['img_ativ'])
					{
						list($img_ativ_width, $img_ativ_height, $img_ativ_type, $img_ativ_attr) = getimagesize(ANEXOS_PATH.'/'.$ITEM['img_ativ']);
						
						$img_ativ_type2 = $_img_type[$img_ativ_type];
						$img_ativ_width_points  = $img_ativ_width * 0.75;
						$img_ativ_height_points = $img_ativ_height * 0.75;
						
						switch($img_ativ_type)
						{
							case '1'://gif
								$gd_img_ativ = imagecreatefromgif(ANEXOS_PATH.'/'.$ITEM['img_ativ']);
								break;
							case '2'://jpg
								$gd_img_ativ = imagecreatefromjpeg(ANEXOS_PATH.'/'.$ITEM['img_ativ']);
								break;
							case '3'://png
							default:
								$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$ITEM['img_ativ']);
								break;
						}
						
						$ia = $a+1;
						
						$img_ativ_type2 = $img_ativ_type[$img_logo_type];
						error_log("geralaudo_final_calor.php:\n\n coleta_img_ativ_".$ia." -> ".ANEXOS_PATH.'/'.$ITEM['img_ativ']."\nimg_ativ_width -> ".$img_ativ_width."\n\nimg_ativ_height ->".$img_ativ_height."\n\nimg_ativ_type -> ".$img_ativ_type."(".$img_ativ_type2.")\n\n",0);
						
						// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
						$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
						$objDrawing->setName('coleta_img_ativ_'.$ia);
						$objDrawing->setDescription('coleta_img_ativ_'.$ia);
						$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
						$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
						$objDrawing->setResizeProportional(false);
						$objDrawing->setWidth($img_ativ_width_points);
						$objDrawing->setHeight($img_ativ_height_points);
						//$objDrawing->setOffsetX(110);
						$objDrawing->setCoordinates('A'.$bb);
						$objDrawing->setImageResource($gd_img_ativ);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					}
					
					## COLETA_DESCR_TAREFA_PASSO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('I'.$bb, $ITEM['descr_tarefa_passo']);
					
					$bb = $aa+1;
					
					## COLETA_TBNM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['tbnm']);
					
					$bb++;
					
					## COLETA_TBSM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['tbsm']);
					
					$bb++;
					
					## COLETA_TGM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['tgm']);
					
					$bb++;
					
					## COLETA_UR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['ur']);
					
					$bb++;
					
					## COLETA_VAR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['var']);
					
					$bb++;
					
					## COLETA_M
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('X'.$bb, $ITEM['m']);
				}
			}
			
			## RESP_CAMPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D96', $o_RESP_CAMPO);
			
			## RESP_TECNICO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P96', $o_RESP_TECNICO);
			
			## REGISTRO_RC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D97', $o_REGISTRO_RC);
			
			## REGISTRO_RT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P97', $o_REGISTRO_RT);
			
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Laudo Calor');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
			
			
			// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			
			## Grava arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			try 
			{
				$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
				$_RET_MSG  = "";
				$_RET_FILE = $_FILENAME;
			} 
			catch (Exception $e) 
			{
				error_log("gera_laudo_final_calor.php:\n\n"."objWriter->save(".LAUDO_FINAL_PATH."/".$_FILENAME."); -> ".$e->getMessage()."\n\n",0);
			}
			
		}
	}
	else
	{
		if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
		      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
		exit;
	}
	
	//error_log("gera_laudo_final_calor.php:\n\n returning -> ".$_RET_MSG."|".$_RET_FILE."|\n\n",0);
	
	return $_RET_MSG."|".$_RET_FILE."|";
	
}


#################################################################################
###########
#####
##
?>
