<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: registro_user.php
## Função..........: Efetua o cadastro dos usuarios que serao colaboradores ou clientes finais
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_NOME = $IN_SOBRENOME = $IN_EMAIL = $IN_CPF = $IN_CNPJ = $IN_RAZAO = $IN_FONEFIXO1 = $IN_CELULAR1 = 
	$IN_PAIS = $IN_IDIOMA = $IN_CEP = $IN_UF = $IN_CIDADE_CODIGO = $IN_BAIRRO = $IN_END = $IN_NUM = $IN_COMPL = $IN_USERNAME = 
	$IN_PASSWORD = "";
	
	//www.rlgomide.com/cgi-bin/registro.php?plano=G&nome=rodrigo&sobrenome=gomide&email=rlgomide%40gmail.com&cpf=03104925640&cnpj=123123123000145&razao=teste%20de%20razao&fonefixo1=031988891655&celular=031988891766&pais=BR&cep=36570000&uf=MG&cidade=3171303&bairro=joao%20braz&end=rua%20pedra%20do%20anta&num=65&compl=casa%20b%20fundos&username=rlgomide&password=123123
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_NOME       = test_input($_GET["nome"]);
		$IN_SOBRENOME  = test_input($_GET["sobrenome"]);
		$IN_EMAIL      = test_input($_GET["email"]);
		$IN_CPF        = test_input($_GET["cpf"]);
		$IN_CNPJ       = test_input($_GET["cnpj"]);
		$IN_RAZAO      = test_input($_GET["razao"]);
		$IN_FONEFIXO1  = test_input($_GET["fonefixo1"]);
		$IN_CELULAR1   = test_input($_GET["celular"]);
		$IN_PAIS       = test_input($_GET["pais"]);
		$IN_IDIOMA     = test_input($_GET["idioma"]);
		$IN_CEP        = test_input($_GET["cep"]);
		$IN_UF         = test_input($_GET["uf"]);
		$IN_CIDADE     = test_input($_GET["cidade"]);
		$IN_BAIRRO     = test_input($_GET["bairro"]);
		$IN_END        = test_input($_GET["end"]);
		$IN_NUM        = test_input($_GET["num"]);
		$IN_COMPL      = test_input($_GET["compl"]);
		$IN_USERNAME   = test_input($_GET["username"]);
		$IN_PASSWORD   = test_input($_GET["p"]);
		$IN_PLANO      = test_input($_GET["plano"]);
		//$IN_INDICACAO  = test_input($_GET["indicacao"]);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_NOME       = test_input($_POST["nome"]);
		$IN_SOBRENOME  = test_input($_POST["sobrenome"]);
		$IN_EMAIL      = test_input($_POST["email"]);
		$IN_CPF        = test_input($_POST["cpf"]);
		$IN_CNPJ       = test_input($_POST["cnpj"]);
		$IN_RAZAO      = test_input($_POST["razao"]);
		$IN_FONEFIXO1  = test_input($_POST["fonefixo1"]);
		$IN_CELULAR1   = test_input($_POST["celular1"]);
		$IN_PAIS       = test_input($_POST["pais"]);
		$IN_IDIOMA     = test_input($_POST["idioma"]);
		$IN_CEP        = test_input($_POST["cep"]);
		$IN_UF         = test_input($_POST["uf"]);
		$IN_CIDADE     = test_input($_POST["cidade"]);
		$IN_BAIRRO     = test_input($_POST["bairro"]);
		$IN_END        = test_input($_POST["end"]);
		$IN_NUM        = test_input($_POST["num"]);
		$IN_COMPL      = test_input($_POST["compl"]);
		$IN_USERNAME   = test_input($_POST["username"]);
		$IN_PASSWORD   = test_input($_POST["p"]);
		//$IN_INDICACAO  = test_input($_POST["indicacao"]);
		
		
		## Define idioma
		if(empty($IN_LANG))   { $IN_LANG   = "pt-br"; }
		if(empty($IN_IDIOMA)) { $IN_IDIOMA = "pt-br"; }
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_NOME) &&
				!empty($IN_SOBRENOME) &&
				!empty($IN_EMAIL) &&
				!empty($IN_CPF) &&
				//!empty($IN_CNPJ) &&
				//!empty($IN_RAZAO) &&
				!empty($IN_FONEFIXO1) &&
				//!empty($IN_CELULAR1) &&
				!empty($IN_PAIS) &&
				!empty($IN_IDIOMA) &&
				!empty($IN_CEP) &&
				!empty($IN_UF) &&
				!empty($IN_CIDADE) &&
				!empty($IN_BAIRRO) &&
				!empty($IN_END) &&
				!empty($IN_NUM) &&
				//!empty($IN_COMPL) &&
				!empty($IN_USERNAME) &&
				!empty($IN_PASSWORD) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## GERA TOKEN
			$TOKEN_TMP = strtoupper(gera_token());
			
			## Gera SALT
			//$RANDOM_SALT = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
			$RANDOM_SALT = hash('sha512', uniqid(generateRandomString(), TRUE));
			
			## Cria uma senha com salt 
			$PASSWORD = hash('sha512', $IN_PASSWORD . $RANDOM_SALT);
			
			## uppercase
			mb_strtolower($IN_USERNAME,"UTF-8");
			mb_strtolower($IN_EMAIL,"UTF-8");
			mb_strtoupper($IN_NOME,"UTF-8");
			mb_strtoupper($IN_SOBRENOME,"UTF-8");
			mb_strtoupper($IN_RAZAO,"UTF-8");
			mb_strtoupper($IN_END,"UTF-8");
			mb_strtoupper($IN_NUM,"UTF-8");
			mb_strtoupper($IN_COMPL,"UTF-8");
			mb_strtoupper($IN_BAIRRO,"UTF-8");
			mb_strtolower($IN_IDIOMA,"UTF-8");
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT F_RegistraUsuario_INSERT(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) as ret"
			)) 
			{
				$sql_LANG         = $mysqli->escape_String($IN_LANG);
				$sql_TOKEN_TMP    = $mysqli->escape_String($TOKEN_TMP);
				$sql_USERNAME     = $mysqli->escape_String($IN_USERNAME);
				$sql_PASSWORD     = $mysqli->escape_String($PASSWORD);
				$sql_RANDOM_SALT  = $mysqli->escape_String($RANDOM_SALT);
				$sql_EMAIL        = $mysqli->escape_String($IN_EMAIL);
				$sql_NOME         = $mysqli->escape_String($IN_NOME);
				$sql_SOBRENOME    = $mysqli->escape_String($IN_SOBRENOME);
				$sql_CPF          = $mysqli->escape_String($IN_CPF);
				$sql_RAZAO        = $mysqli->escape_String($IN_RAZAO);
				$sql_CNPJ         = $mysqli->escape_String($IN_CNPJ);
				$sql_FONEFIXO1    = $mysqli->escape_String($IN_FONEFIXO1);
				$sql_CELULAR1     = $mysqli->escape_String($IN_CELULAR1);
				$sql_IDIOMA       = $mysqli->escape_String($IN_IDIOMA);
				$sql_PAIS         = $mysqli->escape_String($IN_PAIS);
				$sql_CEP          = $mysqli->escape_String($IN_CEP);
				$sql_UF           = $mysqli->escape_String($IN_UF);
				$sql_CIDADE       = $mysqli->escape_String($IN_CIDADE);
				$sql_END          = $mysqli->escape_String($IN_END);
				$sql_NUM          = $mysqli->escape_String($IN_NUM);
				$sql_COMPL        = $mysqli->escape_String($IN_COMPL);
				$sql_BAIRRO       = $mysqli->escape_String($IN_BAIRRO);
				$sql_INDICACAO    = $mysqli->escape_String($IN_INDICACAO);
				//
				$stmt->bind_param('sssssssssssssssssssssss', $sql_LANG,$sql_TOKEN_TMP,$sql_USERNAME,$sql_PASSWORD,$sql_RANDOM_SALT,$sql_EMAIL,$sql_NOME,$sql_SOBRENOME,$sql_CPF,$sql_RAZAO,$sql_CNPJ,$sql_FONEFIXO1,$sql_CELULAR1,$sql_IDIOMA,$sql_PAIS,$sql_CEP,$sql_UF,$sql_CIDADE,$sql_END,$sql_NUM,$sql_COMPL,$sql_BAIRRO,$sql_INDICACAO);
				
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_RESULT);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|".TXT_ERRO_SOLICITACAO."|alert|");
					exit;
				}
				else
				{
					//status|msg|user_account_id|cliente_gescond_id|plano_dias_gratis|plano_tipo|plano_tit|plano_valor|
					list($STATUS, $MSG, $USER_ACCOUNT_ID) = explode("|", $o_RESULT);
					
					if($STATUS == 0)
					{
						die($STATUS."|".$MSG."|alert|");
						exit;
					}
					
					//echo "\n\n($STATUS)($MSG)($USER_ACCOUNT_ID)($CLIENTE_GESCOND_ID)($PLANO_DIAS_GRATIS)($PLANO_TIPO)($PLANO_TITULO)($PLANO_VALOR_TMP)\n\n";
					
					#Formata URL de confirmacao
					$CONF_URL = WEB_ADDRESS."/cgi-bin/conf_registro_user.php?l=".$IN_LANG."&t=".$TOKEN_TMP;
					
					## Create a new PHPMailer instance
					$mail = new PHPMailer(true);
					
					##Formata array com os dados
					$template_map = array(
						'{{IMG_URL}}'         => IMG_URL,
						'{{NOME}}'            => mb_strtoupper($IN_NOME,"UTF-8"),
						'{{SOBRENOME}}'       => mb_strtoupper($IN_SOBRENOME,"UTF-8"),
						'{{EMAIL}}'           => mb_strtolower($IN_EMAIL,"UTF-8"),
						'{{CONFIRMACAO_URL}}' => $CONF_URL
					);
					
					
					
					##Carrega texto do email via template ja processado
					$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_REGISTRO_USER, $template_map, $IN_LANG);
					
					$isMailSent = 0;
					
					try 
					{
						ini_set("sendmail_from", "");
						$mail->CharSet = 'UTF-8';
						$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
						
						$mail->AddAddress($IN_EMAIL);//Set who the message is to be sent to
						
						//if(isset(EMAIL_CONTATO_BCC))
						//{
						//	$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
						//}
						
						$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
						
						$mail->Subject = TXT_EMAIL_SUBJECT_REGISTRO;//Set the subject line
						
						//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
						//$mail->MsgHTML(file_get_contents('contents.html'));
						$mail->MsgHTML($body);
						//$mail->AddAttachment('images/phpmailer.gif');      // attachment
						//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
						//if($mail->Send();
						if ($mail->send()) { $isMailSent = 1; }
					} 
					catch (phpmailerException $e) 
					{
						$MailError = $e->errorMessage();
					} 
					catch (Exception $e) 
					{
						$MailException = $e->getMessage();
					}
					
					//send the message, check for errors
					if (!$isMailSent) 
					{
						error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_REGISTRO_USER."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
					}
					
					die($STATUS."|".TXT_MSG_REGISTRO_EFETIVADO."|success|");
					exit; 
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
