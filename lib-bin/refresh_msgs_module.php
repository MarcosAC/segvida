<?php
#################################################################################
## Prodfy Mudas
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: refresh_msgs_module.php
## Função..........: Atualiza modulo de mensagens habilitando link de nova mensagem e a exibicao das mensagens existentes
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_USERNAME = "";
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		//$IN_USERNAME   = test_input($_GET["username"]);
*/		

		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		//$IN_USERNAME       = test_input($_POST["u"]);
		
		
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		$TMP_idPRODFY_CLIENTE_ID = $_SESSION['user_idprodfy_cliente'];
		$IN_USERNAME             = $_SESSION['user_username'];
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		carrega_idioma($IN_LANG);
		
		if( empty($TMP_idPRODFY_CLIENTE_ID) )
		{
			$GO=0;
			die("0|".TXT_SYS_NO_SESSION."|alert|");
			exit;
		}
		
		## Autentica dados obrigatorios
		if( !empty($IN_USERNAME) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			$tmp_idPRODFY_CLIENTE_ID = $_SESSION['user_idprodfy_cliente'];
			
			$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
			$sql_SID_idPRODFY_CLIENTE = $mysqli->escape_string($tmp_idPRODFY_CLIENTE_ID);
			$sql_USERNAME             = $mysqli->escape_string($IN_USERNAME);
			
			
			####
			# Verifica se há colaboradores configurados
			####
			{
				$user_total_colaboradores = 0;
				
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT upper(CB.`username`) as username,
	              concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome
	         FROM `COLABORADOR` CB
	   INNER JOIN `PRODFY_USER_ACCOUNT` U
	           ON U.`username` = CB.`username`
	        WHERE CB.`idPRODFY_CLIENTE` = ?"
				)) 
				{
					$stmt->bind_param('s', $sql_SID_idPRODFY_CLIENTE);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_COLAB_USERNAME, $o_COLAB_NOME);
					
					$o_TOTAL_COLABORADORES=0;
					$COLABS_LIST = '';
					
					while($stmt->fetch())
					{
						$COLABS_LIST .= '<option value="'.$o_COLAB_USERNAME.'" data-subtext="('.$o_COLAB_NOME.')">'.$o_COLAB_USERNAME.'</option>';
						$o_TOTAL_COLABORADORES++;
					}
					
					$user_total_colaboradores = $o_TOTAL_COLABORADORES;
					
				}# /prepara query #
				else
				{
					die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
					exit;
				}
			}
			
			
			
			####
			# Processa mensagens do sistema
			####
			{
				## Carrega mensagens do usuario
				
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT M.`idMENSAGENS` as idMENSAGENS,
	              M.`idPRODFY_CLIENTE` as idPRODFY_CLIENTE,
	              M.`from_username` as from_username,
	              U_FRM.`nome` as from_nome,
	              U_FRM.`sobrenome` as from_sobrenome,
	              U_FRM.`profile_pic` as from_profile_pic,
	              M.`to_username` as to_username,
	              M.`subject` as subject,
	              M.`msg` as msg,
	              DATE_FORMAT(M.`date_sent`,?) as date_sent,
	              M.`anexo_filename` as anexo_filename,
	              M.`ip` as ip,
	              (SELECT count(1) 
	                 FROM `MENSAGENS` M2
	                WHERE upper(M2.`to_username`) = upper(?)
	                  AND M2.`idPRODFY_CLIENTE`  = C.`idPRODFY_CLIENTE`
	              ) as total_msgs
	        FROM `MENSAGENS` as M
	   LEFT JOIN `PRODFY_CLIENTE` as C
	          ON C.`idPRODFY_CLIENTE` = M.`idPRODFY_CLIENTE`
	   LEFT JOIN `PRODFY_USER_ACCOUNT` as U_FRM
	          ON U_FRM.`username` = M.`from_username`
	       WHERE upper(M.`to_username`) = upper(?)
	       ORDER BY M.`date_sent` DESC 
	          LIMIT 5"
				)) 
				{
					$stmt->bind_param('sss', $sql_DDMMYYYYHHMMSS, $sql_USERNAME, $sql_USERNAME);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_MSG_idMENSAGENS, $o_MSG_idPRODFY_CLIENTE, 
					$o_MSG_FROM_USERNAME, $o_MSG_FROM_NOME, $o_MSG_FROM_SOBRENOME, $o_MSG_FROM_PROFILE_PIC, 
					$o_MSG_TO_USERNAME, $o_MSG_SUBJECT, $o_MSG_MSG, $o_MSG_DATE_SENT, $o_MSG_ANEXO_FILENAME, 
					$o_MSG_IP, $o_MSGS_TOTAL);
					
					$msg_menu_msgs  = '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">';
					
					if( $user_total_colaboradores > 0)
					{
						$msg_menu_msgs .= '<li><div class="text-center"><a role="button" data-toggle="modal" data-target="#enviarmsgs-modal"><span><i class="fa fa-pencil"></i>&nbsp;'.TXT_MOD_MSGS_ENVIAR_MSG.'</span></a></div></li>';
					}
					else
					{
						$msg_menu_msgs .= '<li><div class="text-center"><a id="btn_no_new_msgs" data-text="'.TXT_MOD_MSGS_NENHUM_COLABORADOR_CONFIGURADO.'" role="button"><span><i class="fa fa-pencil"></i>&nbsp;'.TXT_MOD_MSGS_ENVIAR_MSG.'</span></a></div></li>';
					}
					
					
					while($stmt->fetch())
					{
						## Formata as mensagens iniciais
						
						## Formata nome do usuario
						$msg_from_name = $o_MSG_FROM_NOME." ".$o_MSG_FROM_SOBRENOME;
						
						## Formata imagem de perfil
						if($o_MSG_FROM_PROFILE_PIC && 
						   file_exists(PROFILE_PICS_PATH.'/'.$o_MSG_FROM_PROFILE_PIC)
							)  { $msg_profile_pic = $o_MSG_FROM_PROFILE_PIC; }
						else { $msg_profile_pic = 'no_profile_pic.jpg'; }
						
						## Formata data de envio
						$msg_dth_envio = $o_MSG_DATE_SENT;
						
						## Formata assunto
						$msg_assunto = $o_MSG_SUBJECT;
						
						## Formata mensagem
						$msg_menu_msgs .= '<li><a><span class="image"><img src="img/'.$msg_profile_pic.'" alt="" /></span>';
						$msg_menu_msgs .= '<span><span>'.$msg_from_name.'</span><span class="time">'.$msg_dth_envio.'</span></span>';
						$msg_menu_msgs .= '<span class="message">'.$msg_assunto.'</span></a></li>';
						
						## Formata total geral de mensagens
						$msg_total = $o_MSGS_TOTAL;
						
					}
					
					##Se nao encontrou dados, retorna
					if ($stmt->num_rows == 0) 
					{ 
						$msg_total = '';
						$msg_menu_msgs .= '<li><div class="text-center"><a role="button"><strong><i class="fa fa-exclamation-triangle"></i>&nbsp;'.TXT_MOD_MSGS_NENHUMA_MSG_ENCONTRADA.'</strong></a></div></li></ul></ul>';
					}
					else
					{
						$msg_menu_msgs .= '<li><div class="text-center"><a href="prodfy.php?pg=msgs-msgs" role="button"><strong>'.TXT_MOD_MSGS_VER_TODAS_AS_MSGS.'</strong>&nbsp;<i class="fa fa-plus-circle"></i></a></div></li></ul></ul>';
					}
					
				}# /prepara query #
				else
				{
					die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
					exit;
				}
				
				## Formata total de mensagens
				#$msg_total = 0;
				
				## Formata botao envelope
				$msg_menu_link    = '<a role="button" class="dropdown-toggle info-number '.$msg_menu_botao_status_class.'" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i><span class="badge bg-green">'.$msg_total.'</span></a>';
				
				## Formata painel de mensagens
				$msg_menu_panel = $msg_menu_link.$msg_menu_msgs;
			}
			
			die("1||success|".$COLABS_LIST."|".$msg_menu_panel."|");
			exit;
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
