<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-calor.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 7/9/2017 16:4:23
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_IDCLIENTE = $IN_ANO = $IN_MES = $IN_PLANILHA_NUM = $IN_UNIDADE_SITE = $IN_DATA_ELABORACAO = $IN_AREA = $IN_SETOR = $IN_GES = $IN_CARGO_FUNCAO = $IN_CBO = $IN_ATIV_MACRO = $IN_ANALISES = $IN_ANALISE_AMOSTRA_1 = $IN_ANALISE_DATA_AMOSTRAGEM_1 = $IN_ANALISE_TAREFA_EXEC_1 = $IN_ANALISE_PROC_PROD_1 = $IN_ANALISE_OBS_PERT_1 = $IN_ANALISE_AMOSTRA_2 = $IN_ANALISE_DATA_AMOSTRAGEM_2 = $IN_ANALISE_TAREFA_EXEC_2 = $IN_ANALISE_PROC_PROD_2 = $IN_ANALISE_OBS_PERT_2 = $IN_ANALISE_AMOSTRA_3 = $IN_ANALISE_DATA_AMOSTRAGEM_3 = $IN_ANALISE_TAREFA_EXEC_3 = $IN_ANALISE_PROC_PROD_3 = $IN_ANALISE_OBS_PERT_3 = $IN_ANALISE_AMOSTRA_4 = $IN_ANALISE_DATA_AMOSTRAGEM_4 = $IN_ANALISE_TAREFA_EXEC_4 = $IN_ANALISE_PROC_PROD_4 = $IN_ANALISE_OBS_PERT_4 = $IN_ANALISE_AMOSTRA_5 = $IN_ANALISE_DATA_AMOSTRAGEM_5 = $IN_ANALISE_TAREFA_EXEC_5 = $IN_ANALISE_PROC_PROD_5 = $IN_ANALISE_OBS_PERT_5 = $IN_ANALISE_AMOSTRA_6 = $IN_ANALISE_DATA_AMOSTRAGEM_6 = $IN_ANALISE_TAREFA_EXEC_6 = $IN_ANALISE_PROC_PROD_6 = $IN_ANALISE_OBS_PERT_6 = $IN_JOR_TRAB = $IN_TEMPO_EXPO = $IN_TIPO_EXPO = $IN_MEIO_PROPAG = $IN_FONTE_GERADORA = $IN_EPC = $IN_EPI = $IN_DISP_1 = $IN_DISP_2 = $IN_REF_LT = $IN_QDR_COMP = $IN_METAB = $IN_COLETAS = $IN_COLETA_PASSO_1 = $IN_COLETA_CARGA_SOLAR_1 = $IN_COLETA_TIPO_ATIV_1 = $IN_COLETA_TEMP_PASSO_1 = $IN_COLETA_DESCR_TAREFA_PASSO_1 = $IN_COLETA_TBNM_1 = $IN_COLETA_TBSM_1 = $IN_COLETA_TGM_1 = $IN_COLETA_UR_1 = $IN_COLETA_VAR_1 = $IN_COLETA_M_1 = $IN_COLETA_PASSO_2 = $IN_COLETA_CARGA_SOLAR_2 = $IN_COLETA_TIPO_ATIV_2 = $IN_COLETA_TEMP_PASSO_2 = $IN_COLETA_DESCR_TAREFA_PASSO_2 = $IN_COLETA_TBNM_2 = $IN_COLETA_TBSM_2 = $IN_COLETA_TGM_2 = $IN_COLETA_UR_2 = $IN_COLETA_VAR_2 = $IN_COLETA_M_2 = $IN_COLETA_PASSO_3 = $IN_COLETA_CARGA_SOLAR_3 = $IN_COLETA_TIPO_ATIV_3 = $IN_COLETA_TEMP_PASSO_3 = $IN_COLETA_DESCR_TAREFA_PASSO_3 = $IN_COLETA_TBNM_3 = $IN_COLETA_TBSM_3 = $IN_COLETA_TGM_3 = $IN_COLETA_UR_3 = $IN_COLETA_VAR_3 = $IN_COLETA_M_3 = $IN_COLETA_PASSO_4 = $IN_COLETA_CARGA_SOLAR_4 = $IN_COLETA_TIPO_ATIV_4 = $IN_COLETA_TEMP_PASSO_4 = $IN_COLETA_DESCR_TAREFA_PASSO_4 = $IN_COLETA_TBNM_4 = $IN_COLETA_TBSM_4 = $IN_COLETA_TGM_4 = $IN_COLETA_UR_4 = $IN_COLETA_VAR_4 = $IN_COLETA_M_4 = $IN_COLETA_PASSO_5 = $IN_COLETA_CARGA_SOLAR_5 = $IN_COLETA_TIPO_ATIV_5 = $IN_COLETA_TEMP_PASSO_5 = $IN_COLETA_DESCR_TAREFA_PASSO_5 = $IN_COLETA_TBNM_5 = $IN_COLETA_TBSM_5 = $IN_COLETA_TGM_5 = $IN_COLETA_UR_5 = $IN_COLETA_VAR_5 = $IN_COLETA_M_5 = $IN_COLETA_PASSO_6 = $IN_COLETA_CARGA_SOLAR_6 = $IN_COLETA_TIPO_ATIV_6 = $IN_COLETA_TEMP_PASSO_6 = $IN_COLETA_DESCR_TAREFA_PASSO_6 = $IN_COLETA_TBNM_6 = $IN_COLETA_TBSM_6 = $IN_COLETA_TGM_6 = $IN_COLETA_UR_6 = $IN_COLETA_VAR_6 = $IN_COLETA_M_6 = $IN_RESP_CAMPO_IDCOLABORADOR = $IN_RESP_TECNICO_IDCOLABORADOR = $IN_REGISTRO_RC = $IN_REGISTRO_RT = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_IDCLIENTE = test_input($_GET["idcliente"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_PLANILHA_NUM = test_input($_GET["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_GET["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_GET["data_elaboracao"]);
		$IN_AREA = test_input($_GET["area"]);
		$IN_SETOR = test_input($_GET["setor"]);
		$IN_GES = test_input($_GET["ges"]);
		$IN_CARGO_FUNCAO = test_input($_GET["cargo_funcao"]);
		$IN_CBO = test_input($_GET["cbo"]);
		$IN_ATIV_MACRO = test_input($_GET["ativ_macro"]);
		$IN_ANALISES = test_input($_GET["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_GET["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_GET["analise_data_amostragem_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_GET["analise_tarefa_exec_1"]);
		$IN_ANALISE_PROC_PROD_1 = test_input($_GET["analise_proc_prod_1"]);
		$IN_ANALISE_OBS_PERT_1 = test_input($_GET["analise_obs_pert_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_GET["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_GET["analise_data_amostragem_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_GET["analise_tarefa_exec_2"]);
		$IN_ANALISE_PROC_PROD_2 = test_input($_GET["analise_proc_prod_2"]);
		$IN_ANALISE_OBS_PERT_2 = test_input($_GET["analise_obs_pert_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_GET["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_GET["analise_data_amostragem_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_GET["analise_tarefa_exec_3"]);
		$IN_ANALISE_PROC_PROD_3 = test_input($_GET["analise_proc_prod_3"]);
		$IN_ANALISE_OBS_PERT_3 = test_input($_GET["analise_obs_pert_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_GET["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_GET["analise_data_amostragem_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_GET["analise_tarefa_exec_4"]);
		$IN_ANALISE_PROC_PROD_4 = test_input($_GET["analise_proc_prod_4"]);
		$IN_ANALISE_OBS_PERT_4 = test_input($_GET["analise_obs_pert_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_GET["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_GET["analise_data_amostragem_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_GET["analise_tarefa_exec_5"]);
		$IN_ANALISE_PROC_PROD_5 = test_input($_GET["analise_proc_prod_5"]);
		$IN_ANALISE_OBS_PERT_5 = test_input($_GET["analise_obs_pert_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_GET["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_GET["analise_data_amostragem_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_GET["analise_tarefa_exec_6"]);
		$IN_ANALISE_PROC_PROD_6 = test_input($_GET["analise_proc_prod_6"]);
		$IN_ANALISE_OBS_PERT_6 = test_input($_GET["analise_obs_pert_6"]);
		$IN_JOR_TRAB = test_input($_GET["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_GET["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_GET["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_GET["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_GET["fonte_geradora"]);
		$IN_EPC = test_input($_GET["epc"]);
		$IN_EPI = test_input($_GET["epi"]);
		$IN_DISP_1 = test_input($_GET["disp_1"]);
		$IN_DISP_2 = test_input($_GET["disp_2"]);
		$IN_REF_LT = test_input($_GET["ref_lt"]);
		$IN_QDR_COMP = test_input($_GET["qdr_comp"]);
		$IN_METAB = test_input($_GET["metab"]);
		$IN_COLETAS = test_input($_GET["coletas"]);
		$IN_COLETA_PASSO_1 = test_input($_GET["coleta_passo_1"]);
		$IN_COLETA_CARGA_SOLAR_1 = test_input($_GET["coleta_carga_solar_1"]);
		$IN_COLETA_TIPO_ATIV_1 = test_input($_GET["coleta_tipo_ativ_1"]);
		$IN_COLETA_TEMP_PASSO_1 = test_input($_GET["coleta_temp_passo_1"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_1 = test_input($_GET["coleta_descr_tarefa_passo_1"]);
		$IN_COLETA_TBNM_1 = test_input($_GET["coleta_tbnm_1"]);
		$IN_COLETA_TBSM_1 = test_input($_GET["coleta_tbsm_1"]);
		$IN_COLETA_TGM_1 = test_input($_GET["coleta_tgm_1"]);
		$IN_COLETA_UR_1 = test_input($_GET["coleta_ur_1"]);
		$IN_COLETA_VAR_1 = test_input($_GET["coleta_var_1"]);
		$IN_COLETA_M_1 = test_input($_GET["coleta_m_1"]);
		$IN_COLETA_PASSO_2 = test_input($_GET["coleta_passo_2"]);
		$IN_COLETA_CARGA_SOLAR_2 = test_input($_GET["coleta_carga_solar_2"]);
		$IN_COLETA_TIPO_ATIV_2 = test_input($_GET["coleta_tipo_ativ_2"]);
		$IN_COLETA_TEMP_PASSO_2 = test_input($_GET["coleta_temp_passo_2"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_2 = test_input($_GET["coleta_descr_tarefa_passo_2"]);
		$IN_COLETA_TBNM_2 = test_input($_GET["coleta_tbnm_2"]);
		$IN_COLETA_TBSM_2 = test_input($_GET["coleta_tbsm_2"]);
		$IN_COLETA_TGM_2 = test_input($_GET["coleta_tgm_2"]);
		$IN_COLETA_UR_2 = test_input($_GET["coleta_ur_2"]);
		$IN_COLETA_VAR_2 = test_input($_GET["coleta_var_2"]);
		$IN_COLETA_M_2 = test_input($_GET["coleta_m_2"]);
		$IN_COLETA_PASSO_3 = test_input($_GET["coleta_passo_3"]);
		$IN_COLETA_CARGA_SOLAR_3 = test_input($_GET["coleta_carga_solar_3"]);
		$IN_COLETA_TIPO_ATIV_3 = test_input($_GET["coleta_tipo_ativ_3"]);
		$IN_COLETA_TEMP_PASSO_3 = test_input($_GET["coleta_temp_passo_3"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_3 = test_input($_GET["coleta_descr_tarefa_passo_3"]);
		$IN_COLETA_TBNM_3 = test_input($_GET["coleta_tbnm_3"]);
		$IN_COLETA_TBSM_3 = test_input($_GET["coleta_tbsm_3"]);
		$IN_COLETA_TGM_3 = test_input($_GET["coleta_tgm_3"]);
		$IN_COLETA_UR_3 = test_input($_GET["coleta_ur_3"]);
		$IN_COLETA_VAR_3 = test_input($_GET["coleta_var_3"]);
		$IN_COLETA_M_3 = test_input($_GET["coleta_m_3"]);
		$IN_COLETA_PASSO_4 = test_input($_GET["coleta_passo_4"]);
		$IN_COLETA_CARGA_SOLAR_4 = test_input($_GET["coleta_carga_solar_4"]);
		$IN_COLETA_TIPO_ATIV_4 = test_input($_GET["coleta_tipo_ativ_4"]);
		$IN_COLETA_TEMP_PASSO_4 = test_input($_GET["coleta_temp_passo_4"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_4 = test_input($_GET["coleta_descr_tarefa_passo_4"]);
		$IN_COLETA_TBNM_4 = test_input($_GET["coleta_tbnm_4"]);
		$IN_COLETA_TBSM_4 = test_input($_GET["coleta_tbsm_4"]);
		$IN_COLETA_TGM_4 = test_input($_GET["coleta_tgm_4"]);
		$IN_COLETA_UR_4 = test_input($_GET["coleta_ur_4"]);
		$IN_COLETA_VAR_4 = test_input($_GET["coleta_var_4"]);
		$IN_COLETA_M_4 = test_input($_GET["coleta_m_4"]);
		$IN_COLETA_PASSO_5 = test_input($_GET["coleta_passo_5"]);
		$IN_COLETA_CARGA_SOLAR_5 = test_input($_GET["coleta_carga_solar_5"]);
		$IN_COLETA_TIPO_ATIV_5 = test_input($_GET["coleta_tipo_ativ_5"]);
		$IN_COLETA_TEMP_PASSO_5 = test_input($_GET["coleta_temp_passo_5"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_5 = test_input($_GET["coleta_descr_tarefa_passo_5"]);
		$IN_COLETA_TBNM_5 = test_input($_GET["coleta_tbnm_5"]);
		$IN_COLETA_TBSM_5 = test_input($_GET["coleta_tbsm_5"]);
		$IN_COLETA_TGM_5 = test_input($_GET["coleta_tgm_5"]);
		$IN_COLETA_UR_5 = test_input($_GET["coleta_ur_5"]);
		$IN_COLETA_VAR_5 = test_input($_GET["coleta_var_5"]);
		$IN_COLETA_M_5 = test_input($_GET["coleta_m_5"]);
		$IN_COLETA_PASSO_6 = test_input($_GET["coleta_passo_6"]);
		$IN_COLETA_CARGA_SOLAR_6 = test_input($_GET["coleta_carga_solar_6"]);
		$IN_COLETA_TIPO_ATIV_6 = test_input($_GET["coleta_tipo_ativ_6"]);
		$IN_COLETA_TEMP_PASSO_6 = test_input($_GET["coleta_temp_passo_6"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_6 = test_input($_GET["coleta_descr_tarefa_passo_6"]);
		$IN_COLETA_TBNM_6 = test_input($_GET["coleta_tbnm_6"]);
		$IN_COLETA_TBSM_6 = test_input($_GET["coleta_tbsm_6"]);
		$IN_COLETA_TGM_6 = test_input($_GET["coleta_tgm_6"]);
		$IN_COLETA_UR_6 = test_input($_GET["coleta_ur_6"]);
		$IN_COLETA_VAR_6 = test_input($_GET["coleta_var_6"]);
		$IN_COLETA_M_6 = test_input($_GET["coleta_m_6"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_GET["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_GET["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_GET["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_GET["registro_rt"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_IDCLIENTE = test_input($_POST["idcliente"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_PLANILHA_NUM = test_input($_POST["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_POST["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_POST["data_elaboracao"]);
		$IN_AREA = test_input($_POST["area"]);
		$IN_SETOR = test_input($_POST["setor"]);
		$IN_GES = test_input($_POST["ges"]);
		$IN_CARGO_FUNCAO = test_input($_POST["cargo_funcao"]);
		$IN_CBO = test_input($_POST["cbo"]);
		$IN_ATIV_MACRO = test_input($_POST["ativ_macro"]);
		$IN_ANALISES = test_input($_POST["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_POST["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_POST["analise_data_amostragem_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_POST["analise_tarefa_exec_1"]);
		$IN_ANALISE_PROC_PROD_1 = test_input($_POST["analise_proc_prod_1"]);
		$IN_ANALISE_OBS_PERT_1 = test_input($_POST["analise_obs_pert_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_POST["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_POST["analise_data_amostragem_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_POST["analise_tarefa_exec_2"]);
		$IN_ANALISE_PROC_PROD_2 = test_input($_POST["analise_proc_prod_2"]);
		$IN_ANALISE_OBS_PERT_2 = test_input($_POST["analise_obs_pert_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_POST["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_POST["analise_data_amostragem_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_POST["analise_tarefa_exec_3"]);
		$IN_ANALISE_PROC_PROD_3 = test_input($_POST["analise_proc_prod_3"]);
		$IN_ANALISE_OBS_PERT_3 = test_input($_POST["analise_obs_pert_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_POST["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_POST["analise_data_amostragem_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_POST["analise_tarefa_exec_4"]);
		$IN_ANALISE_PROC_PROD_4 = test_input($_POST["analise_proc_prod_4"]);
		$IN_ANALISE_OBS_PERT_4 = test_input($_POST["analise_obs_pert_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_POST["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_POST["analise_data_amostragem_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_POST["analise_tarefa_exec_5"]);
		$IN_ANALISE_PROC_PROD_5 = test_input($_POST["analise_proc_prod_5"]);
		$IN_ANALISE_OBS_PERT_5 = test_input($_POST["analise_obs_pert_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_POST["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_POST["analise_data_amostragem_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_POST["analise_tarefa_exec_6"]);
		$IN_ANALISE_PROC_PROD_6 = test_input($_POST["analise_proc_prod_6"]);
		$IN_ANALISE_OBS_PERT_6 = test_input($_POST["analise_obs_pert_6"]);
		$IN_JOR_TRAB = test_input($_POST["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_POST["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_POST["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_POST["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_POST["fonte_geradora"]);
		$IN_EPC = test_input($_POST["epc"]);
		$IN_EPI = test_input($_POST["epi"]);
		$IN_DISP_1 = test_input($_POST["disp_1"]);
		$IN_DISP_2 = test_input($_POST["disp_2"]);
		$IN_REF_LT = test_input($_POST["ref_lt"]);
		$IN_QDR_COMP = test_input($_POST["qdr_comp"]);
		$IN_METAB = test_input($_POST["metab"]);
		$IN_COLETAS = test_input($_POST["coletas"]);
		$IN_COLETA_PASSO_1 = test_input($_POST["coleta_passo_1"]);
		$IN_COLETA_CARGA_SOLAR_1 = test_input($_POST["coleta_carga_solar_1"]);
		$IN_COLETA_TIPO_ATIV_1 = test_input($_POST["coleta_tipo_ativ_1"]);
		$IN_COLETA_TEMP_PASSO_1 = test_input($_POST["coleta_temp_passo_1"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_1 = test_input($_POST["coleta_descr_tarefa_passo_1"]);
		$IN_COLETA_TBNM_1 = test_input($_POST["coleta_tbnm_1"]);
		$IN_COLETA_TBSM_1 = test_input($_POST["coleta_tbsm_1"]);
		$IN_COLETA_TGM_1 = test_input($_POST["coleta_tgm_1"]);
		$IN_COLETA_UR_1 = test_input($_POST["coleta_ur_1"]);
		$IN_COLETA_VAR_1 = test_input($_POST["coleta_var_1"]);
		$IN_COLETA_M_1 = test_input($_POST["coleta_m_1"]);
		$IN_COLETA_PASSO_2 = test_input($_POST["coleta_passo_2"]);
		$IN_COLETA_CARGA_SOLAR_2 = test_input($_POST["coleta_carga_solar_2"]);
		$IN_COLETA_TIPO_ATIV_2 = test_input($_POST["coleta_tipo_ativ_2"]);
		$IN_COLETA_TEMP_PASSO_2 = test_input($_POST["coleta_temp_passo_2"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_2 = test_input($_POST["coleta_descr_tarefa_passo_2"]);
		$IN_COLETA_TBNM_2 = test_input($_POST["coleta_tbnm_2"]);
		$IN_COLETA_TBSM_2 = test_input($_POST["coleta_tbsm_2"]);
		$IN_COLETA_TGM_2 = test_input($_POST["coleta_tgm_2"]);
		$IN_COLETA_UR_2 = test_input($_POST["coleta_ur_2"]);
		$IN_COLETA_VAR_2 = test_input($_POST["coleta_var_2"]);
		$IN_COLETA_M_2 = test_input($_POST["coleta_m_2"]);
		$IN_COLETA_PASSO_3 = test_input($_POST["coleta_passo_3"]);
		$IN_COLETA_CARGA_SOLAR_3 = test_input($_POST["coleta_carga_solar_3"]);
		$IN_COLETA_TIPO_ATIV_3 = test_input($_POST["coleta_tipo_ativ_3"]);
		$IN_COLETA_TEMP_PASSO_3 = test_input($_POST["coleta_temp_passo_3"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_3 = test_input($_POST["coleta_descr_tarefa_passo_3"]);
		$IN_COLETA_TBNM_3 = test_input($_POST["coleta_tbnm_3"]);
		$IN_COLETA_TBSM_3 = test_input($_POST["coleta_tbsm_3"]);
		$IN_COLETA_TGM_3 = test_input($_POST["coleta_tgm_3"]);
		$IN_COLETA_UR_3 = test_input($_POST["coleta_ur_3"]);
		$IN_COLETA_VAR_3 = test_input($_POST["coleta_var_3"]);
		$IN_COLETA_M_3 = test_input($_POST["coleta_m_3"]);
		$IN_COLETA_PASSO_4 = test_input($_POST["coleta_passo_4"]);
		$IN_COLETA_CARGA_SOLAR_4 = test_input($_POST["coleta_carga_solar_4"]);
		$IN_COLETA_TIPO_ATIV_4 = test_input($_POST["coleta_tipo_ativ_4"]);
		$IN_COLETA_TEMP_PASSO_4 = test_input($_POST["coleta_temp_passo_4"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_4 = test_input($_POST["coleta_descr_tarefa_passo_4"]);
		$IN_COLETA_TBNM_4 = test_input($_POST["coleta_tbnm_4"]);
		$IN_COLETA_TBSM_4 = test_input($_POST["coleta_tbsm_4"]);
		$IN_COLETA_TGM_4 = test_input($_POST["coleta_tgm_4"]);
		$IN_COLETA_UR_4 = test_input($_POST["coleta_ur_4"]);
		$IN_COLETA_VAR_4 = test_input($_POST["coleta_var_4"]);
		$IN_COLETA_M_4 = test_input($_POST["coleta_m_4"]);
		$IN_COLETA_PASSO_5 = test_input($_POST["coleta_passo_5"]);
		$IN_COLETA_CARGA_SOLAR_5 = test_input($_POST["coleta_carga_solar_5"]);
		$IN_COLETA_TIPO_ATIV_5 = test_input($_POST["coleta_tipo_ativ_5"]);
		$IN_COLETA_TEMP_PASSO_5 = test_input($_POST["coleta_temp_passo_5"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_5 = test_input($_POST["coleta_descr_tarefa_passo_5"]);
		$IN_COLETA_TBNM_5 = test_input($_POST["coleta_tbnm_5"]);
		$IN_COLETA_TBSM_5 = test_input($_POST["coleta_tbsm_5"]);
		$IN_COLETA_TGM_5 = test_input($_POST["coleta_tgm_5"]);
		$IN_COLETA_UR_5 = test_input($_POST["coleta_ur_5"]);
		$IN_COLETA_VAR_5 = test_input($_POST["coleta_var_5"]);
		$IN_COLETA_M_5 = test_input($_POST["coleta_m_5"]);
		$IN_COLETA_PASSO_6 = test_input($_POST["coleta_passo_6"]);
		$IN_COLETA_CARGA_SOLAR_6 = test_input($_POST["coleta_carga_solar_6"]);
		$IN_COLETA_TIPO_ATIV_6 = test_input($_POST["coleta_tipo_ativ_6"]);
		$IN_COLETA_TEMP_PASSO_6 = test_input($_POST["coleta_temp_passo_6"]);
		$IN_COLETA_DESCR_TAREFA_PASSO_6 = test_input($_POST["coleta_descr_tarefa_passo_6"]);
		$IN_COLETA_TBNM_6 = test_input($_POST["coleta_tbnm_6"]);
		$IN_COLETA_TBSM_6 = test_input($_POST["coleta_tbsm_6"]);
		$IN_COLETA_TGM_6 = test_input($_POST["coleta_tgm_6"]);
		$IN_COLETA_UR_6 = test_input($_POST["coleta_ur_6"]);
		$IN_COLETA_VAR_6 = test_input($_POST["coleta_var_6"]);
		$IN_COLETA_M_6 = test_input($_POST["coleta_m_6"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_POST["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_POST["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_POST["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_POST["registro_rt"]);
		
		## case convertion
		mb_strtoupper($IN_REGISTRO_RC,"UTF-8");
		mb_strtoupper($IN_REGISTRO_RT,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_PERT_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_PERT_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_PERT_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_PERT_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_PERT_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_PERT_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_EPI, $IN_DISP_1, $IN_DISP_2, $IN_REF_LT, $IN_QDR_COMP, $IN_METAB, $IN_COLETAS, $IN_COLETA_PASSO_1, $IN_COLETA_CARGA_SOLAR_1, $IN_COLETA_TIPO_ATIV_1, $IN_COLETA_TEMP_PASSO_1, $IN_COLETA_DESCR_TAREFA_PASSO_1, $IN_COLETA_TBNM_1, $IN_COLETA_TBSM_1, $IN_COLETA_TGM_1, $IN_COLETA_UR_1, $IN_COLETA_VAR_1, $IN_COLETA_M_1, $IN_COLETA_PASSO_2, $IN_COLETA_CARGA_SOLAR_2, $IN_COLETA_TIPO_ATIV_2, $IN_COLETA_TEMP_PASSO_2, $IN_COLETA_DESCR_TAREFA_PASSO_2, $IN_COLETA_TBNM_2, $IN_COLETA_TBSM_2, $IN_COLETA_TGM_2, $IN_COLETA_UR_2, $IN_COLETA_VAR_2, $IN_COLETA_M_2, $IN_COLETA_PASSO_3, $IN_COLETA_CARGA_SOLAR_3, $IN_COLETA_TIPO_ATIV_3, $IN_COLETA_TEMP_PASSO_3, $IN_COLETA_DESCR_TAREFA_PASSO_3, $IN_COLETA_TBNM_3, $IN_COLETA_TBSM_3, $IN_COLETA_TGM_3, $IN_COLETA_UR_3, $IN_COLETA_VAR_3, $IN_COLETA_M_3, $IN_COLETA_PASSO_4, $IN_COLETA_CARGA_SOLAR_4, $IN_COLETA_TIPO_ATIV_4, $IN_COLETA_TEMP_PASSO_4, $IN_COLETA_DESCR_TAREFA_PASSO_4, $IN_COLETA_TBNM_4, $IN_COLETA_TBSM_4, $IN_COLETA_TGM_4, $IN_COLETA_UR_4, $IN_COLETA_VAR_4, $IN_COLETA_M_4, $IN_COLETA_PASSO_5, $IN_COLETA_CARGA_SOLAR_5, $IN_COLETA_TIPO_ATIV_5, $IN_COLETA_TEMP_PASSO_5, $IN_COLETA_DESCR_TAREFA_PASSO_5, $IN_COLETA_TBNM_5, $IN_COLETA_TBSM_5, $IN_COLETA_TGM_5, $IN_COLETA_UR_5, $IN_COLETA_VAR_5, $IN_COLETA_M_5, $IN_COLETA_PASSO_6, $IN_COLETA_CARGA_SOLAR_6, $IN_COLETA_TIPO_ATIV_6, $IN_COLETA_TEMP_PASSO_6, $IN_COLETA_DESCR_TAREFA_PASSO_6, $IN_COLETA_TBNM_6, $IN_COLETA_TBSM_6, $IN_COLETA_TGM_6, $IN_COLETA_UR_6, $IN_COLETA_VAR_6, $IN_COLETA_M_6, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_PERT_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_PERT_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_PERT_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_PERT_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_PERT_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_PERT_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_EPI, $IN_DISP_1, $IN_DISP_2, $IN_REF_LT, $IN_QDR_COMP, $IN_METAB, $IN_COLETAS, $IN_COLETA_PASSO_1, $IN_COLETA_CARGA_SOLAR_1, $IN_COLETA_TIPO_ATIV_1, $IN_COLETA_TEMP_PASSO_1, $IN_COLETA_DESCR_TAREFA_PASSO_1, $IN_COLETA_TBNM_1, $IN_COLETA_TBSM_1, $IN_COLETA_TGM_1, $IN_COLETA_UR_1, $IN_COLETA_VAR_1, $IN_COLETA_M_1, $IN_COLETA_PASSO_2, $IN_COLETA_CARGA_SOLAR_2, $IN_COLETA_TIPO_ATIV_2, $IN_COLETA_TEMP_PASSO_2, $IN_COLETA_DESCR_TAREFA_PASSO_2, $IN_COLETA_TBNM_2, $IN_COLETA_TBSM_2, $IN_COLETA_TGM_2, $IN_COLETA_UR_2, $IN_COLETA_VAR_2, $IN_COLETA_M_2, $IN_COLETA_PASSO_3, $IN_COLETA_CARGA_SOLAR_3, $IN_COLETA_TIPO_ATIV_3, $IN_COLETA_TEMP_PASSO_3, $IN_COLETA_DESCR_TAREFA_PASSO_3, $IN_COLETA_TBNM_3, $IN_COLETA_TBSM_3, $IN_COLETA_TGM_3, $IN_COLETA_UR_3, $IN_COLETA_VAR_3, $IN_COLETA_M_3, $IN_COLETA_PASSO_4, $IN_COLETA_CARGA_SOLAR_4, $IN_COLETA_TIPO_ATIV_4, $IN_COLETA_TEMP_PASSO_4, $IN_COLETA_DESCR_TAREFA_PASSO_4, $IN_COLETA_TBNM_4, $IN_COLETA_TBSM_4, $IN_COLETA_TGM_4, $IN_COLETA_UR_4, $IN_COLETA_VAR_4, $IN_COLETA_M_4, $IN_COLETA_PASSO_5, $IN_COLETA_CARGA_SOLAR_5, $IN_COLETA_TIPO_ATIV_5, $IN_COLETA_TEMP_PASSO_5, $IN_COLETA_DESCR_TAREFA_PASSO_5, $IN_COLETA_TBNM_5, $IN_COLETA_TBSM_5, $IN_COLETA_TGM_5, $IN_COLETA_UR_5, $IN_COLETA_VAR_5, $IN_COLETA_M_5, $IN_COLETA_PASSO_6, $IN_COLETA_CARGA_SOLAR_6, $IN_COLETA_TIPO_ATIV_6, $IN_COLETA_TEMP_PASSO_6, $IN_COLETA_DESCR_TAREFA_PASSO_6, $IN_COLETA_TBNM_6, $IN_COLETA_TBSM_6, $IN_COLETA_TGM_6, $IN_COLETA_UR_6, $IN_COLETA_VAR_6, $IN_COLETA_M_6, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_PERT_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_PERT_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_PERT_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_PERT_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_PERT_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_PERT_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_EPI, $IN_DISP_1, $IN_DISP_2, $IN_REF_LT, $IN_QDR_COMP, $IN_METAB, $IN_COLETAS, $IN_COLETA_PASSO_1, $IN_COLETA_CARGA_SOLAR_1, $IN_COLETA_TIPO_ATIV_1, $IN_COLETA_TEMP_PASSO_1, $IN_COLETA_DESCR_TAREFA_PASSO_1, $IN_COLETA_TBNM_1, $IN_COLETA_TBSM_1, $IN_COLETA_TGM_1, $IN_COLETA_UR_1, $IN_COLETA_VAR_1, $IN_COLETA_M_1, $IN_COLETA_PASSO_2, $IN_COLETA_CARGA_SOLAR_2, $IN_COLETA_TIPO_ATIV_2, $IN_COLETA_TEMP_PASSO_2, $IN_COLETA_DESCR_TAREFA_PASSO_2, $IN_COLETA_TBNM_2, $IN_COLETA_TBSM_2, $IN_COLETA_TGM_2, $IN_COLETA_UR_2, $IN_COLETA_VAR_2, $IN_COLETA_M_2, $IN_COLETA_PASSO_3, $IN_COLETA_CARGA_SOLAR_3, $IN_COLETA_TIPO_ATIV_3, $IN_COLETA_TEMP_PASSO_3, $IN_COLETA_DESCR_TAREFA_PASSO_3, $IN_COLETA_TBNM_3, $IN_COLETA_TBSM_3, $IN_COLETA_TGM_3, $IN_COLETA_UR_3, $IN_COLETA_VAR_3, $IN_COLETA_M_3, $IN_COLETA_PASSO_4, $IN_COLETA_CARGA_SOLAR_4, $IN_COLETA_TIPO_ATIV_4, $IN_COLETA_TEMP_PASSO_4, $IN_COLETA_DESCR_TAREFA_PASSO_4, $IN_COLETA_TBNM_4, $IN_COLETA_TBSM_4, $IN_COLETA_TGM_4, $IN_COLETA_UR_4, $IN_COLETA_VAR_4, $IN_COLETA_M_4, $IN_COLETA_PASSO_5, $IN_COLETA_CARGA_SOLAR_5, $IN_COLETA_TIPO_ATIV_5, $IN_COLETA_TEMP_PASSO_5, $IN_COLETA_DESCR_TAREFA_PASSO_5, $IN_COLETA_TBNM_5, $IN_COLETA_TBSM_5, $IN_COLETA_TGM_5, $IN_COLETA_UR_5, $IN_COLETA_VAR_5, $IN_COLETA_M_5, $IN_COLETA_PASSO_6, $IN_COLETA_CARGA_SOLAR_6, $IN_COLETA_TIPO_ATIV_6, $IN_COLETA_TEMP_PASSO_6, $IN_COLETA_DESCR_TAREFA_PASSO_6, $IN_COLETA_TBNM_6, $IN_COLETA_TBSM_6, $IN_COLETA_TGM_6, $IN_COLETA_UR_6, $IN_COLETA_VAR_6, $IN_COLETA_M_6, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## FILE UPLOAD
			case '5':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPLOAD($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
				
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id, 
              upper(CLNT.`nome_interno`) as nome_interno, 
              AA.`ano` as ano, 
              AA.`mes` as mes, 
              AA.`planilha_num` as planilha_num, 
              date_format(AA.`data_elaboracao`,?) as data_elaboracao,
              AA.`analises` as analises, 
              AA.`coletas` as coletas 
         FROM `CALOR` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 2,3,4,5"
		)) 
		{
			$stmt->bind_param('ss', $sql_DDMMYYYY, $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_DATA_ELABORACAO, $o_ANALISES, $o_COLETAS);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JAN; }
					if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_CALOR_FEV; }
					if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_CALOR_MAR; }
					if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_CALOR_ABR; }
					if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_CALOR_MAI; }
					if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JUN; }
					if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JUL; }
					if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_CALOR_AGO; }
					if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_CALOR_SET; }
					if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_CALOR_OUT; }
					if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_CALOR_NOV; }
					if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_CALOR_DEZ; }
				
					# Formata Datas Nulas
					if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
					
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td><small>'.$o_IDCLIENTE.'</small></td>';
					$TBODY_LIST .= '<td>'.$o_ANO.'</td>';
					$TBODY_LIST .= '<td>'.$o_MES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_PLANILHA_NUM.'</td>';
					$TBODY_LIST .= '<td>'.$o_DATA_ELABORACAO.'</td>';
					$TBODY_LIST .= '<td>'.$o_ANALISES.'</td>';
					$TBODY_LIST .= '<td>'.$o_COLETAS.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_PERT_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_PERT_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_PERT_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_PERT_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_PERT_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_PERT_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_EPI, $_DISP_1, $_DISP_2, $_REF_LT, $_QDR_COMP, $_METAB, $_COLETAS, $_COLETA_PASSO_1, $_COLETA_CARGA_SOLAR_1, $_COLETA_TIPO_ATIV_1, $_COLETA_TEMP_PASSO_1, $_COLETA_DESCR_TAREFA_PASSO_1, $_COLETA_TBNM_1, $_COLETA_TBSM_1, $_COLETA_TGM_1, $_COLETA_UR_1, $_COLETA_VAR_1, $_COLETA_M_1, $_COLETA_PASSO_2, $_COLETA_CARGA_SOLAR_2, $_COLETA_TIPO_ATIV_2, $_COLETA_TEMP_PASSO_2, $_COLETA_DESCR_TAREFA_PASSO_2, $_COLETA_TBNM_2, $_COLETA_TBSM_2, $_COLETA_TGM_2, $_COLETA_UR_2, $_COLETA_VAR_2, $_COLETA_M_2, $_COLETA_PASSO_3, $_COLETA_CARGA_SOLAR_3, $_COLETA_TIPO_ATIV_3, $_COLETA_TEMP_PASSO_3, $_COLETA_DESCR_TAREFA_PASSO_3, $_COLETA_TBNM_3, $_COLETA_TBSM_3, $_COLETA_TGM_3, $_COLETA_UR_3, $_COLETA_VAR_3, $_COLETA_M_3, $_COLETA_PASSO_4, $_COLETA_CARGA_SOLAR_4, $_COLETA_TIPO_ATIV_4, $_COLETA_TEMP_PASSO_4, $_COLETA_DESCR_TAREFA_PASSO_4, $_COLETA_TBNM_4, $_COLETA_TBSM_4, $_COLETA_TGM_4, $_COLETA_UR_4, $_COLETA_VAR_4, $_COLETA_M_4, $_COLETA_PASSO_5, $_COLETA_CARGA_SOLAR_5, $_COLETA_TIPO_ATIV_5, $_COLETA_TEMP_PASSO_5, $_COLETA_DESCR_TAREFA_PASSO_5, $_COLETA_TBNM_5, $_COLETA_TBSM_5, $_COLETA_TGM_5, $_COLETA_UR_5, $_COLETA_VAR_5, $_COLETA_M_5, $_COLETA_PASSO_6, $_COLETA_CARGA_SOLAR_6, $_COLETA_TIPO_ATIV_6, $_COLETA_TEMP_PASSO_6, $_COLETA_DESCR_TAREFA_PASSO_6, $_COLETA_TBNM_6, $_COLETA_TBSM_6, $_COLETA_TGM_6, $_COLETA_UR_6, $_COLETA_VAR_6, $_COLETA_M_6, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id
       FROM `CALOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_PERT_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_PERT_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_PERT_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_PERT_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_PERT_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_PERT_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_EPI, $_DISP_1, $_DISP_2, $_REF_LT, $_QDR_COMP, $_METAB, $_COLETAS, $_COLETA_PASSO_1, $_COLETA_CARGA_SOLAR_1, $_COLETA_TIPO_ATIV_1, $_COLETA_TEMP_PASSO_1, $_COLETA_DESCR_TAREFA_PASSO_1, $_COLETA_TBNM_1, $_COLETA_TBSM_1, $_COLETA_TGM_1, $_COLETA_UR_1, $_COLETA_VAR_1, $_COLETA_M_1, $_COLETA_PASSO_2, $_COLETA_CARGA_SOLAR_2, $_COLETA_TIPO_ATIV_2, $_COLETA_TEMP_PASSO_2, $_COLETA_DESCR_TAREFA_PASSO_2, $_COLETA_TBNM_2, $_COLETA_TBSM_2, $_COLETA_TGM_2, $_COLETA_UR_2, $_COLETA_VAR_2, $_COLETA_M_2, $_COLETA_PASSO_3, $_COLETA_CARGA_SOLAR_3, $_COLETA_TIPO_ATIV_3, $_COLETA_TEMP_PASSO_3, $_COLETA_DESCR_TAREFA_PASSO_3, $_COLETA_TBNM_3, $_COLETA_TBSM_3, $_COLETA_TGM_3, $_COLETA_UR_3, $_COLETA_VAR_3, $_COLETA_M_3, $_COLETA_PASSO_4, $_COLETA_CARGA_SOLAR_4, $_COLETA_TIPO_ATIV_4, $_COLETA_TEMP_PASSO_4, $_COLETA_DESCR_TAREFA_PASSO_4, $_COLETA_TBNM_4, $_COLETA_TBSM_4, $_COLETA_TGM_4, $_COLETA_UR_4, $_COLETA_VAR_4, $_COLETA_M_4, $_COLETA_PASSO_5, $_COLETA_CARGA_SOLAR_5, $_COLETA_TIPO_ATIV_5, $_COLETA_TEMP_PASSO_5, $_COLETA_DESCR_TAREFA_PASSO_5, $_COLETA_TBNM_5, $_COLETA_TBSM_5, $_COLETA_TGM_5, $_COLETA_UR_5, $_COLETA_VAR_5, $_COLETA_M_5, $_COLETA_PASSO_6, $_COLETA_CARGA_SOLAR_6, $_COLETA_TIPO_ATIV_6, $_COLETA_TEMP_PASSO_6, $_COLETA_DESCR_TAREFA_PASSO_6, $_COLETA_TBNM_6, $_COLETA_TBSM_6, $_COLETA_TGM_6, $_COLETA_UR_6, $_COLETA_VAR_6, $_COLETA_M_6, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_COLETA_IMG_ATIV_FILENAME_1_filename = "";
		if($_FILES["coleta_img_ativ_filename_1_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_1_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_1_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_1_file"]["error"] > 0) 
				{
					return "0|COLETA #1 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_1_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_1_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_1_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_1_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_1_filename);
				
			}
			else
			{
				return "0|COLETA #1 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_2_filename = "";
		if($_FILES["coleta_img_ativ_filename_2_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_2_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_2_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_2_file"]["error"] > 0) 
				{
					return "0|COLETA #2 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_2_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_2_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_2_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_2_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_2_filename);
				
			}
			else
			{
				return "0|COLETA #2 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_3_filename = "";
		if($_FILES["coleta_img_ativ_filename_3_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_3_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_3_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_3_file"]["error"] > 0) 
				{
					return "0|COLETA #3 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_3_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_3_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_3_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_3_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_3_filename);
				
			}
			else
			{
				return "0|COLETA #3 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_4_filename = "";
		if($_FILES["coleta_img_ativ_filename_4_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_4_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_4_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_4_file"]["error"] > 0) 
				{
					return "0|COLETA #4 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_4_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_4_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_4_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_4_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_4_filename);
				
			}
			else
			{
				return "0|COLETA #4 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_5_filename = "";
		if($_FILES["coleta_img_ativ_filename_5_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_5_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_5_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_5_file"]["error"] > 0) 
				{
					return "0|COLETA #5 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_5_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_5_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_5_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_5_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_5_filename);
				
			}
			else
			{
				return "0|COLETA #5 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_6_filename = "";
		if($_FILES["coleta_img_ativ_filename_6_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_6_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_6_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_6_file"]["error"] > 0) 
				{
					return "0|COLETA #6 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_6_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_6_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_6_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_6_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_6_filename);
				
			}
			else
			{
				return "0|COLETA #6 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_PROC_PROD_1 = $mysqli->escape_String($_ANALISE_PROC_PROD_1);
		$sql_ANALISE_OBS_PERT_1 = $mysqli->escape_String($_ANALISE_OBS_PERT_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_PROC_PROD_2 = $mysqli->escape_String($_ANALISE_PROC_PROD_2);
		$sql_ANALISE_OBS_PERT_2 = $mysqli->escape_String($_ANALISE_OBS_PERT_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_PROC_PROD_3 = $mysqli->escape_String($_ANALISE_PROC_PROD_3);
		$sql_ANALISE_OBS_PERT_3 = $mysqli->escape_String($_ANALISE_OBS_PERT_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_PROC_PROD_4 = $mysqli->escape_String($_ANALISE_PROC_PROD_4);
		$sql_ANALISE_OBS_PERT_4 = $mysqli->escape_String($_ANALISE_OBS_PERT_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_PROC_PROD_5 = $mysqli->escape_String($_ANALISE_PROC_PROD_5);
		$sql_ANALISE_OBS_PERT_5 = $mysqli->escape_String($_ANALISE_OBS_PERT_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_ANALISE_PROC_PROD_6 = $mysqli->escape_String($_ANALISE_PROC_PROD_6);
		$sql_ANALISE_OBS_PERT_6 = $mysqli->escape_String($_ANALISE_OBS_PERT_6);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_EPI = $mysqli->escape_String($_EPI);
		$sql_DISP_1 = $mysqli->escape_String($_DISP_1);
		$sql_DISP_2 = $mysqli->escape_String($_DISP_2);
		$sql_REF_LT = $mysqli->escape_String($_REF_LT);
		$sql_QDR_COMP = $mysqli->escape_String($_QDR_COMP);
		$sql_METAB = $mysqli->escape_String($_METAB);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_PASSO_1 = $mysqli->escape_String($_COLETA_PASSO_1);
		$sql_COLETA_CARGA_SOLAR_1 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_1);
		$sql_COLETA_TIPO_ATIV_1 = $mysqli->escape_String($_COLETA_TIPO_ATIV_1);
		$sql_COLETA_TEMP_PASSO_1 = $mysqli->escape_String($_COLETA_TEMP_PASSO_1);
		$sql_COLETA_DESCR_TAREFA_PASSO_1 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_1);
		$sql_COLETA_TBNM_1 = $mysqli->escape_String($_COLETA_TBNM_1);
		$sql_COLETA_TBSM_1 = $mysqli->escape_String($_COLETA_TBSM_1);
		$sql_COLETA_TGM_1 = $mysqli->escape_String($_COLETA_TGM_1);
		$sql_COLETA_UR_1 = $mysqli->escape_String($_COLETA_UR_1);
		$sql_COLETA_VAR_1 = $mysqli->escape_String($_COLETA_VAR_1);
		$sql_COLETA_M_1 = $mysqli->escape_String($_COLETA_M_1);
		$sql_COLETA_PASSO_2 = $mysqli->escape_String($_COLETA_PASSO_2);
		$sql_COLETA_CARGA_SOLAR_2 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_2);
		$sql_COLETA_TIPO_ATIV_2 = $mysqli->escape_String($_COLETA_TIPO_ATIV_2);
		$sql_COLETA_TEMP_PASSO_2 = $mysqli->escape_String($_COLETA_TEMP_PASSO_2);
		$sql_COLETA_DESCR_TAREFA_PASSO_2 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_2);
		$sql_COLETA_TBNM_2 = $mysqli->escape_String($_COLETA_TBNM_2);
		$sql_COLETA_TBSM_2 = $mysqli->escape_String($_COLETA_TBSM_2);
		$sql_COLETA_TGM_2 = $mysqli->escape_String($_COLETA_TGM_2);
		$sql_COLETA_UR_2 = $mysqli->escape_String($_COLETA_UR_2);
		$sql_COLETA_VAR_2 = $mysqli->escape_String($_COLETA_VAR_2);
		$sql_COLETA_M_2 = $mysqli->escape_String($_COLETA_M_2);
		$sql_COLETA_PASSO_3 = $mysqli->escape_String($_COLETA_PASSO_3);
		$sql_COLETA_CARGA_SOLAR_3 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_3);
		$sql_COLETA_TIPO_ATIV_3 = $mysqli->escape_String($_COLETA_TIPO_ATIV_3);
		$sql_COLETA_TEMP_PASSO_3 = $mysqli->escape_String($_COLETA_TEMP_PASSO_3);
		$sql_COLETA_DESCR_TAREFA_PASSO_3 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_3);
		$sql_COLETA_TBNM_3 = $mysqli->escape_String($_COLETA_TBNM_3);
		$sql_COLETA_TBSM_3 = $mysqli->escape_String($_COLETA_TBSM_3);
		$sql_COLETA_TGM_3 = $mysqli->escape_String($_COLETA_TGM_3);
		$sql_COLETA_UR_3 = $mysqli->escape_String($_COLETA_UR_3);
		$sql_COLETA_VAR_3 = $mysqli->escape_String($_COLETA_VAR_3);
		$sql_COLETA_M_3 = $mysqli->escape_String($_COLETA_M_3);
		$sql_COLETA_PASSO_4 = $mysqli->escape_String($_COLETA_PASSO_4);
		$sql_COLETA_CARGA_SOLAR_4 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_4);
		$sql_COLETA_TIPO_ATIV_4 = $mysqli->escape_String($_COLETA_TIPO_ATIV_4);
		$sql_COLETA_TEMP_PASSO_4 = $mysqli->escape_String($_COLETA_TEMP_PASSO_4);
		$sql_COLETA_DESCR_TAREFA_PASSO_4 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_4);
		$sql_COLETA_TBNM_4 = $mysqli->escape_String($_COLETA_TBNM_4);
		$sql_COLETA_TBSM_4 = $mysqli->escape_String($_COLETA_TBSM_4);
		$sql_COLETA_TGM_4 = $mysqli->escape_String($_COLETA_TGM_4);
		$sql_COLETA_UR_4 = $mysqli->escape_String($_COLETA_UR_4);
		$sql_COLETA_VAR_4 = $mysqli->escape_String($_COLETA_VAR_4);
		$sql_COLETA_M_4 = $mysqli->escape_String($_COLETA_M_4);
		$sql_COLETA_PASSO_5 = $mysqli->escape_String($_COLETA_PASSO_5);
		$sql_COLETA_CARGA_SOLAR_5 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_5);
		$sql_COLETA_TIPO_ATIV_5 = $mysqli->escape_String($_COLETA_TIPO_ATIV_5);
		$sql_COLETA_TEMP_PASSO_5 = $mysqli->escape_String($_COLETA_TEMP_PASSO_5);
		$sql_COLETA_DESCR_TAREFA_PASSO_5 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_5);
		$sql_COLETA_TBNM_5 = $mysqli->escape_String($_COLETA_TBNM_5);
		$sql_COLETA_TBSM_5 = $mysqli->escape_String($_COLETA_TBSM_5);
		$sql_COLETA_TGM_5 = $mysqli->escape_String($_COLETA_TGM_5);
		$sql_COLETA_UR_5 = $mysqli->escape_String($_COLETA_UR_5);
		$sql_COLETA_VAR_5 = $mysqli->escape_String($_COLETA_VAR_5);
		$sql_COLETA_M_5 = $mysqli->escape_String($_COLETA_M_5);
		$sql_COLETA_PASSO_6 = $mysqli->escape_String($_COLETA_PASSO_6);
		$sql_COLETA_CARGA_SOLAR_6 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_6);
		$sql_COLETA_TIPO_ATIV_6 = $mysqli->escape_String($_COLETA_TIPO_ATIV_6);
		$sql_COLETA_TEMP_PASSO_6 = $mysqli->escape_String($_COLETA_TEMP_PASSO_6);
		$sql_COLETA_DESCR_TAREFA_PASSO_6 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_6);
		$sql_COLETA_TBNM_6 = $mysqli->escape_String($_COLETA_TBNM_6);
		$sql_COLETA_TBSM_6 = $mysqli->escape_String($_COLETA_TBSM_6);
		$sql_COLETA_TGM_6 = $mysqli->escape_String($_COLETA_TGM_6);
		$sql_COLETA_UR_6 = $mysqli->escape_String($_COLETA_UR_6);
		$sql_COLETA_VAR_6 = $mysqli->escape_String($_COLETA_VAR_6);
		$sql_COLETA_M_6 = $mysqli->escape_String($_COLETA_M_6);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_COLETA_IMG_ATIV_FILENAME_1_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_1_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_2_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_2_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_3_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_3_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_4_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_4_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_5_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_5_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_6_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_6_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id
       FROM `CALOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `CALOR` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_proc_prod_1` = ?
		        ,`analise_obs_pert_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_proc_prod_2` = ?
		        ,`analise_obs_pert_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_proc_prod_3` = ?
		        ,`analise_obs_pert_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_proc_prod_4` = ?
		        ,`analise_obs_pert_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_proc_prod_5` = ?
		        ,`analise_obs_pert_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`analise_proc_prod_6` = ?
		        ,`analise_obs_pert_6` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`epc` = ?
		        ,`epi` = ?
		        ,`disp_1` = ?
		        ,`disp_2` = ?
		        ,`ref_lt` = ?
		        ,`qdr_comp` = ?
		        ,`metab` = ?
		        ,`coletas` = ?
		        ,`coleta_passo_1` = ?
		        ,`coleta_carga_solar_1` = ?
		        ,`coleta_tipo_ativ_1` = ?
		        ,`coleta_temp_passo_1` = ?
		        ,`coleta_descr_tarefa_passo_1` = ?
		        ,`coleta_tbnm_1` = ?
		        ,`coleta_tbsm_1` = ?
		        ,`coleta_tgm_1` = ?
		        ,`coleta_ur_1` = ?
		        ,`coleta_var_1` = ?
		        ,`coleta_m_1` = ?
		        ,`coleta_img_ativ_filename_1` = ?
		        ,`coleta_passo_2` = ?
		        ,`coleta_carga_solar_2` = ?
		        ,`coleta_tipo_ativ_2` = ?
		        ,`coleta_temp_passo_2` = ?
		        ,`coleta_descr_tarefa_passo_2` = ?
		        ,`coleta_tbnm_2` = ?
		        ,`coleta_tbsm_2` = ?
		        ,`coleta_tgm_2` = ?
		        ,`coleta_ur_2` = ?
		        ,`coleta_var_2` = ?
		        ,`coleta_m_2` = ?
		        ,`coleta_img_ativ_filename_2` = ?
		        ,`coleta_passo_3` = ?
		        ,`coleta_carga_solar_3` = ?
		        ,`coleta_tipo_ativ_3` = ?
		        ,`coleta_temp_passo_3` = ?
		        ,`coleta_descr_tarefa_passo_3` = ?
		        ,`coleta_tbnm_3` = ?
		        ,`coleta_tbsm_3` = ?
		        ,`coleta_tgm_3` = ?
		        ,`coleta_ur_3` = ?
		        ,`coleta_var_3` = ?
		        ,`coleta_m_3` = ?
		        ,`coleta_img_ativ_filename_3` = ?
		        ,`coleta_passo_4` = ?
		        ,`coleta_carga_solar_4` = ?
		        ,`coleta_tipo_ativ_4` = ?
		        ,`coleta_temp_passo_4` = ?
		        ,`coleta_descr_tarefa_passo_4` = ?
		        ,`coleta_tbnm_4` = ?
		        ,`coleta_tbsm_4` = ?
		        ,`coleta_tgm_4` = ?
		        ,`coleta_ur_4` = ?
		        ,`coleta_var_4` = ?
		        ,`coleta_m_4` = ?
		        ,`coleta_img_ativ_filename_4` = ?
		        ,`coleta_passo_5` = ?
		        ,`coleta_carga_solar_5` = ?
		        ,`coleta_tipo_ativ_5` = ?
		        ,`coleta_temp_passo_5` = ?
		        ,`coleta_descr_tarefa_passo_5` = ?
		        ,`coleta_tbnm_5` = ?
		        ,`coleta_tbsm_5` = ?
		        ,`coleta_tgm_5` = ?
		        ,`coleta_ur_5` = ?
		        ,`coleta_var_5` = ?
		        ,`coleta_m_5` = ?
		        ,`coleta_img_ativ_filename_5` = ?
		        ,`coleta_passo_6` = ?
		        ,`coleta_carga_solar_6` = ?
		        ,`coleta_tipo_ativ_6` = ?
		        ,`coleta_temp_passo_6` = ?
		        ,`coleta_descr_tarefa_passo_6` = ?
		        ,`coleta_tbnm_6` = ?
		        ,`coleta_tbsm_6` = ?
		        ,`coleta_tgm_6` = ?
		        ,`coleta_ur_6` = ?
		        ,`coleta_var_6` = ?
		        ,`coleta_m_6` = ?
		        ,`coleta_img_ativ_filename_6` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		        ,`logo_filename` = ?"
		)) 
		{
			$stmt->bind_param('ssddddssssssssdssssssssssssssssssssssssssssssssssssssssssdsssdsddddddssssdsddddddssssdsddddddssssdsddddddssssdsddddddssssdsddddddddssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_PROC_PROD_1, $sql_ANALISE_OBS_PERT_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_PROC_PROD_2, $sql_ANALISE_OBS_PERT_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_PROC_PROD_3, $sql_ANALISE_OBS_PERT_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_PROC_PROD_4, $sql_ANALISE_OBS_PERT_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_PROC_PROD_5, $sql_ANALISE_OBS_PERT_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_ANALISE_PROC_PROD_6, $sql_ANALISE_OBS_PERT_6, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_EPC, $sql_EPI, $sql_DISP_1, $sql_DISP_2, $sql_REF_LT, $sql_QDR_COMP, $sql_METAB, $sql_COLETAS, $sql_COLETA_PASSO_1, $sql_COLETA_CARGA_SOLAR_1, $sql_COLETA_TIPO_ATIV_1, $sql_COLETA_TEMP_PASSO_1, $sql_COLETA_DESCR_TAREFA_PASSO_1, $sql_COLETA_TBNM_1, $sql_COLETA_TBSM_1, $sql_COLETA_TGM_1, $sql_COLETA_UR_1, $sql_COLETA_VAR_1, $sql_COLETA_M_1, $sql_COLETA_IMG_ATIV_FILENAME_1_filename, $sql_COLETA_PASSO_2, $sql_COLETA_CARGA_SOLAR_2, $sql_COLETA_TIPO_ATIV_2, $sql_COLETA_TEMP_PASSO_2, $sql_COLETA_DESCR_TAREFA_PASSO_2, $sql_COLETA_TBNM_2, $sql_COLETA_TBSM_2, $sql_COLETA_TGM_2, $sql_COLETA_UR_2, $sql_COLETA_VAR_2, $sql_COLETA_M_2, $sql_COLETA_IMG_ATIV_FILENAME_2_filename, $sql_COLETA_PASSO_3, $sql_COLETA_CARGA_SOLAR_3, $sql_COLETA_TIPO_ATIV_3, $sql_COLETA_TEMP_PASSO_3, $sql_COLETA_DESCR_TAREFA_PASSO_3, $sql_COLETA_TBNM_3, $sql_COLETA_TBSM_3, $sql_COLETA_TGM_3, $sql_COLETA_UR_3, $sql_COLETA_VAR_3, $sql_COLETA_M_3, $sql_COLETA_IMG_ATIV_FILENAME_3_filename, $sql_COLETA_PASSO_4, $sql_COLETA_CARGA_SOLAR_4, $sql_COLETA_TIPO_ATIV_4, $sql_COLETA_TEMP_PASSO_4, $sql_COLETA_DESCR_TAREFA_PASSO_4, $sql_COLETA_TBNM_4, $sql_COLETA_TBSM_4, $sql_COLETA_TGM_4, $sql_COLETA_UR_4, $sql_COLETA_VAR_4, $sql_COLETA_M_4, $sql_COLETA_IMG_ATIV_FILENAME_4_filename, $sql_COLETA_PASSO_5, $sql_COLETA_CARGA_SOLAR_5, $sql_COLETA_TIPO_ATIV_5, $sql_COLETA_TEMP_PASSO_5, $sql_COLETA_DESCR_TAREFA_PASSO_5, $sql_COLETA_TBNM_5, $sql_COLETA_TBSM_5, $sql_COLETA_TGM_5, $sql_COLETA_UR_5, $sql_COLETA_VAR_5, $sql_COLETA_M_5, $sql_COLETA_IMG_ATIV_FILENAME_5_filename, $sql_COLETA_PASSO_6, $sql_COLETA_CARGA_SOLAR_6, $sql_COLETA_TIPO_ATIV_6, $sql_COLETA_TEMP_PASSO_6, $sql_COLETA_DESCR_TAREFA_PASSO_6, $sql_COLETA_TBNM_6, $sql_COLETA_TBSM_6, $sql_COLETA_TGM_6, $sql_COLETA_UR_6, $sql_COLETA_VAR_6, $sql_COLETA_M_6, $sql_COLETA_IMG_ATIV_FILENAME_6_filename, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_LOGO_FILENAME_filename);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_PERT_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_PERT_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_PERT_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_PERT_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_PERT_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_PERT_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_EPI, $_DISP_1, $_DISP_2, $_REF_LT, $_QDR_COMP, $_METAB, $_COLETAS, $_COLETA_PASSO_1, $_COLETA_CARGA_SOLAR_1, $_COLETA_TIPO_ATIV_1, $_COLETA_TEMP_PASSO_1, $_COLETA_DESCR_TAREFA_PASSO_1, $_COLETA_TBNM_1, $_COLETA_TBSM_1, $_COLETA_TGM_1, $_COLETA_UR_1, $_COLETA_VAR_1, $_COLETA_M_1, $_COLETA_PASSO_2, $_COLETA_CARGA_SOLAR_2, $_COLETA_TIPO_ATIV_2, $_COLETA_TEMP_PASSO_2, $_COLETA_DESCR_TAREFA_PASSO_2, $_COLETA_TBNM_2, $_COLETA_TBSM_2, $_COLETA_TGM_2, $_COLETA_UR_2, $_COLETA_VAR_2, $_COLETA_M_2, $_COLETA_PASSO_3, $_COLETA_CARGA_SOLAR_3, $_COLETA_TIPO_ATIV_3, $_COLETA_TEMP_PASSO_3, $_COLETA_DESCR_TAREFA_PASSO_3, $_COLETA_TBNM_3, $_COLETA_TBSM_3, $_COLETA_TGM_3, $_COLETA_UR_3, $_COLETA_VAR_3, $_COLETA_M_3, $_COLETA_PASSO_4, $_COLETA_CARGA_SOLAR_4, $_COLETA_TIPO_ATIV_4, $_COLETA_TEMP_PASSO_4, $_COLETA_DESCR_TAREFA_PASSO_4, $_COLETA_TBNM_4, $_COLETA_TBSM_4, $_COLETA_TGM_4, $_COLETA_UR_4, $_COLETA_VAR_4, $_COLETA_M_4, $_COLETA_PASSO_5, $_COLETA_CARGA_SOLAR_5, $_COLETA_TIPO_ATIV_5, $_COLETA_TEMP_PASSO_5, $_COLETA_DESCR_TAREFA_PASSO_5, $_COLETA_TBNM_5, $_COLETA_TBSM_5, $_COLETA_TGM_5, $_COLETA_UR_5, $_COLETA_VAR_5, $_COLETA_M_5, $_COLETA_PASSO_6, $_COLETA_CARGA_SOLAR_6, $_COLETA_TIPO_ATIV_6, $_COLETA_TEMP_PASSO_6, $_COLETA_DESCR_TAREFA_PASSO_6, $_COLETA_TBNM_6, $_COLETA_TBSM_6, $_COLETA_TGM_6, $_COLETA_UR_6, $_COLETA_VAR_6, $_COLETA_M_6, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_COLETA_IMG_ATIV_FILENAME_1_filename = "";
		if($_FILES["coleta_img_ativ_filename_1_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_1_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_1_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_1_file"]["error"] > 0) 
				{
					return "0|COLETA #1 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_1_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_1_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_1_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_1_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_1_filename);
				
			}
			else
			{
				return "0|COLETA #1 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_2_filename = "";
		if($_FILES["coleta_img_ativ_filename_2_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_2_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_2_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_2_file"]["error"] > 0) 
				{
					return "0|COLETA #2 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_2_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_2_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_2_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_2_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_2_filename);
				
			}
			else
			{
				return "0|COLETA #2 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_3_filename = "";
		if($_FILES["coleta_img_ativ_filename_3_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_3_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_3_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_3_file"]["error"] > 0) 
				{
					return "0|COLETA #3 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_3_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_3_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_3_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_3_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_3_filename);
				
			}
			else
			{
				return "0|COLETA #3 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_4_filename = "";
		if($_FILES["coleta_img_ativ_filename_4_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_4_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_4_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_4_file"]["error"] > 0) 
				{
					return "0|COLETA #4 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_4_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_4_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_4_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_4_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_4_filename);
				
			}
			else
			{
				return "0|COLETA #4 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_5_filename = "";
		if($_FILES["coleta_img_ativ_filename_5_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_5_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_5_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_5_file"]["error"] > 0) 
				{
					return "0|COLETA #5 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_5_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_5_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_5_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_5_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_5_filename);
				
			}
			else
			{
				return "0|COLETA #5 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_COLETA_IMG_ATIV_FILENAME_6_filename = "";
		if($_FILES["coleta_img_ativ_filename_6_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["coleta_img_ativ_filename_6_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["coleta_img_ativ_filename_6_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["coleta_img_ativ_filename_6_file"]["error"] > 0) 
				{
					return "0|COLETA #6 - IMAGEM ATIVIDADE - ".$_FILES["coleta_img_ativ_filename_6_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_COLETA_IMG_ATIV_FILENAME_6_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_6_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["coleta_img_ativ_filename_6_file"]["tmp_name"],ANEXOS_PATH."/" . $_COLETA_IMG_ATIV_FILENAME_6_filename);
				
			}
			else
			{
				return "0|COLETA #6 - IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_PROC_PROD_1 = $mysqli->escape_String($_ANALISE_PROC_PROD_1);
		$sql_ANALISE_OBS_PERT_1 = $mysqli->escape_String($_ANALISE_OBS_PERT_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_PROC_PROD_2 = $mysqli->escape_String($_ANALISE_PROC_PROD_2);
		$sql_ANALISE_OBS_PERT_2 = $mysqli->escape_String($_ANALISE_OBS_PERT_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_PROC_PROD_3 = $mysqli->escape_String($_ANALISE_PROC_PROD_3);
		$sql_ANALISE_OBS_PERT_3 = $mysqli->escape_String($_ANALISE_OBS_PERT_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_PROC_PROD_4 = $mysqli->escape_String($_ANALISE_PROC_PROD_4);
		$sql_ANALISE_OBS_PERT_4 = $mysqli->escape_String($_ANALISE_OBS_PERT_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_PROC_PROD_5 = $mysqli->escape_String($_ANALISE_PROC_PROD_5);
		$sql_ANALISE_OBS_PERT_5 = $mysqli->escape_String($_ANALISE_OBS_PERT_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_ANALISE_PROC_PROD_6 = $mysqli->escape_String($_ANALISE_PROC_PROD_6);
		$sql_ANALISE_OBS_PERT_6 = $mysqli->escape_String($_ANALISE_OBS_PERT_6);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_EPI = $mysqli->escape_String($_EPI);
		$sql_DISP_1 = $mysqli->escape_String($_DISP_1);
		$sql_DISP_2 = $mysqli->escape_String($_DISP_2);
		$sql_REF_LT = $mysqli->escape_String($_REF_LT);
		$sql_QDR_COMP = $mysqli->escape_String($_QDR_COMP);
		$sql_METAB = $mysqli->escape_String($_METAB);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_PASSO_1 = $mysqli->escape_String($_COLETA_PASSO_1);
		$sql_COLETA_CARGA_SOLAR_1 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_1);
		$sql_COLETA_TIPO_ATIV_1 = $mysqli->escape_String($_COLETA_TIPO_ATIV_1);
		$sql_COLETA_TEMP_PASSO_1 = $mysqli->escape_String($_COLETA_TEMP_PASSO_1);
		$sql_COLETA_DESCR_TAREFA_PASSO_1 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_1);
		$sql_COLETA_TBNM_1 = $mysqli->escape_String($_COLETA_TBNM_1);
		$sql_COLETA_TBSM_1 = $mysqli->escape_String($_COLETA_TBSM_1);
		$sql_COLETA_TGM_1 = $mysqli->escape_String($_COLETA_TGM_1);
		$sql_COLETA_UR_1 = $mysqli->escape_String($_COLETA_UR_1);
		$sql_COLETA_VAR_1 = $mysqli->escape_String($_COLETA_VAR_1);
		$sql_COLETA_M_1 = $mysqli->escape_String($_COLETA_M_1);
		$sql_COLETA_PASSO_2 = $mysqli->escape_String($_COLETA_PASSO_2);
		$sql_COLETA_CARGA_SOLAR_2 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_2);
		$sql_COLETA_TIPO_ATIV_2 = $mysqli->escape_String($_COLETA_TIPO_ATIV_2);
		$sql_COLETA_TEMP_PASSO_2 = $mysqli->escape_String($_COLETA_TEMP_PASSO_2);
		$sql_COLETA_DESCR_TAREFA_PASSO_2 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_2);
		$sql_COLETA_TBNM_2 = $mysqli->escape_String($_COLETA_TBNM_2);
		$sql_COLETA_TBSM_2 = $mysqli->escape_String($_COLETA_TBSM_2);
		$sql_COLETA_TGM_2 = $mysqli->escape_String($_COLETA_TGM_2);
		$sql_COLETA_UR_2 = $mysqli->escape_String($_COLETA_UR_2);
		$sql_COLETA_VAR_2 = $mysqli->escape_String($_COLETA_VAR_2);
		$sql_COLETA_M_2 = $mysqli->escape_String($_COLETA_M_2);
		$sql_COLETA_PASSO_3 = $mysqli->escape_String($_COLETA_PASSO_3);
		$sql_COLETA_CARGA_SOLAR_3 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_3);
		$sql_COLETA_TIPO_ATIV_3 = $mysqli->escape_String($_COLETA_TIPO_ATIV_3);
		$sql_COLETA_TEMP_PASSO_3 = $mysqli->escape_String($_COLETA_TEMP_PASSO_3);
		$sql_COLETA_DESCR_TAREFA_PASSO_3 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_3);
		$sql_COLETA_TBNM_3 = $mysqli->escape_String($_COLETA_TBNM_3);
		$sql_COLETA_TBSM_3 = $mysqli->escape_String($_COLETA_TBSM_3);
		$sql_COLETA_TGM_3 = $mysqli->escape_String($_COLETA_TGM_3);
		$sql_COLETA_UR_3 = $mysqli->escape_String($_COLETA_UR_3);
		$sql_COLETA_VAR_3 = $mysqli->escape_String($_COLETA_VAR_3);
		$sql_COLETA_M_3 = $mysqli->escape_String($_COLETA_M_3);
		$sql_COLETA_PASSO_4 = $mysqli->escape_String($_COLETA_PASSO_4);
		$sql_COLETA_CARGA_SOLAR_4 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_4);
		$sql_COLETA_TIPO_ATIV_4 = $mysqli->escape_String($_COLETA_TIPO_ATIV_4);
		$sql_COLETA_TEMP_PASSO_4 = $mysqli->escape_String($_COLETA_TEMP_PASSO_4);
		$sql_COLETA_DESCR_TAREFA_PASSO_4 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_4);
		$sql_COLETA_TBNM_4 = $mysqli->escape_String($_COLETA_TBNM_4);
		$sql_COLETA_TBSM_4 = $mysqli->escape_String($_COLETA_TBSM_4);
		$sql_COLETA_TGM_4 = $mysqli->escape_String($_COLETA_TGM_4);
		$sql_COLETA_UR_4 = $mysqli->escape_String($_COLETA_UR_4);
		$sql_COLETA_VAR_4 = $mysqli->escape_String($_COLETA_VAR_4);
		$sql_COLETA_M_4 = $mysqli->escape_String($_COLETA_M_4);
		$sql_COLETA_PASSO_5 = $mysqli->escape_String($_COLETA_PASSO_5);
		$sql_COLETA_CARGA_SOLAR_5 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_5);
		$sql_COLETA_TIPO_ATIV_5 = $mysqli->escape_String($_COLETA_TIPO_ATIV_5);
		$sql_COLETA_TEMP_PASSO_5 = $mysqli->escape_String($_COLETA_TEMP_PASSO_5);
		$sql_COLETA_DESCR_TAREFA_PASSO_5 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_5);
		$sql_COLETA_TBNM_5 = $mysqli->escape_String($_COLETA_TBNM_5);
		$sql_COLETA_TBSM_5 = $mysqli->escape_String($_COLETA_TBSM_5);
		$sql_COLETA_TGM_5 = $mysqli->escape_String($_COLETA_TGM_5);
		$sql_COLETA_UR_5 = $mysqli->escape_String($_COLETA_UR_5);
		$sql_COLETA_VAR_5 = $mysqli->escape_String($_COLETA_VAR_5);
		$sql_COLETA_M_5 = $mysqli->escape_String($_COLETA_M_5);
		$sql_COLETA_PASSO_6 = $mysqli->escape_String($_COLETA_PASSO_6);
		$sql_COLETA_CARGA_SOLAR_6 = $mysqli->escape_String($_COLETA_CARGA_SOLAR_6);
		$sql_COLETA_TIPO_ATIV_6 = $mysqli->escape_String($_COLETA_TIPO_ATIV_6);
		$sql_COLETA_TEMP_PASSO_6 = $mysqli->escape_String($_COLETA_TEMP_PASSO_6);
		$sql_COLETA_DESCR_TAREFA_PASSO_6 = $mysqli->escape_String($_COLETA_DESCR_TAREFA_PASSO_6);
		$sql_COLETA_TBNM_6 = $mysqli->escape_String($_COLETA_TBNM_6);
		$sql_COLETA_TBSM_6 = $mysqli->escape_String($_COLETA_TBSM_6);
		$sql_COLETA_TGM_6 = $mysqli->escape_String($_COLETA_TGM_6);
		$sql_COLETA_UR_6 = $mysqli->escape_String($_COLETA_UR_6);
		$sql_COLETA_VAR_6 = $mysqli->escape_String($_COLETA_VAR_6);
		$sql_COLETA_M_6 = $mysqli->escape_String($_COLETA_M_6);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_COLETA_IMG_ATIV_FILENAME_1_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_1_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_2_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_2_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_3_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_3_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_4_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_4_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_5_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_5_filename);
		$sql_COLETA_IMG_ATIV_FILENAME_6_filename = $mysqli->escape_String($_COLETA_IMG_ATIV_FILENAME_6_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id, 
            AA.`coleta_img_ativ_filename_1`, 
            AA.`coleta_img_ativ_filename_2`, 
            AA.`coleta_img_ativ_filename_3`, 
            AA.`coleta_img_ativ_filename_4`, 
            AA.`coleta_img_ativ_filename_5`, 
            AA.`coleta_img_ativ_filename_6`, 
            AA.`logo_filename`
       FROM `CALOR` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCALOR` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idCALOR, $o_COLETA_IMG_ATIV_FILENAME_1, $o_COLETA_IMG_ATIV_FILENAME_2, $o_COLETA_IMG_ATIV_FILENAME_3, $o_COLETA_IMG_ATIV_FILENAME_4, $o_COLETA_IMG_ATIV_FILENAME_5, $o_COLETA_IMG_ATIV_FILENAME_6, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Verifica se o registro que foi editado ja existe e nao e o proprio registro que foi alterado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id
       FROM `CALOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
        AND AA.`idCALOR` != ?"
		)) 
		{
			$stmt->bind_param('sddddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		if ($stmt = $mysqli->prepare(
		"UPDATE `CALOR` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_proc_prod_1` = ?
		        ,`analise_obs_pert_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_proc_prod_2` = ?
		        ,`analise_obs_pert_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_proc_prod_3` = ?
		        ,`analise_obs_pert_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_proc_prod_4` = ?
		        ,`analise_obs_pert_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_proc_prod_5` = ?
		        ,`analise_obs_pert_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`analise_proc_prod_6` = ?
		        ,`analise_obs_pert_6` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`epc` = ?
		        ,`epi` = ?
		        ,`disp_1` = ?
		        ,`disp_2` = ?
		        ,`ref_lt` = ?
		        ,`qdr_comp` = ?
		        ,`metab` = ?
		        ,`coletas` = ?
		        ,`coleta_passo_1` = ?
		        ,`coleta_carga_solar_1` = ?
		        ,`coleta_tipo_ativ_1` = ?
		        ,`coleta_temp_passo_1` = ?
		        ,`coleta_descr_tarefa_passo_1` = ?
		        ,`coleta_tbnm_1` = ?
		        ,`coleta_tbsm_1` = ?
		        ,`coleta_tgm_1` = ?
		        ,`coleta_ur_1` = ?
		        ,`coleta_var_1` = ?
		        ,`coleta_m_1` = ?
		        ,`coleta_passo_2` = ?
		        ,`coleta_carga_solar_2` = ?
		        ,`coleta_tipo_ativ_2` = ?
		        ,`coleta_temp_passo_2` = ?
		        ,`coleta_descr_tarefa_passo_2` = ?
		        ,`coleta_tbnm_2` = ?
		        ,`coleta_tbsm_2` = ?
		        ,`coleta_tgm_2` = ?
		        ,`coleta_ur_2` = ?
		        ,`coleta_var_2` = ?
		        ,`coleta_m_2` = ?
		        ,`coleta_passo_3` = ?
		        ,`coleta_carga_solar_3` = ?
		        ,`coleta_tipo_ativ_3` = ?
		        ,`coleta_temp_passo_3` = ?
		        ,`coleta_descr_tarefa_passo_3` = ?
		        ,`coleta_tbnm_3` = ?
		        ,`coleta_tbsm_3` = ?
		        ,`coleta_tgm_3` = ?
		        ,`coleta_ur_3` = ?
		        ,`coleta_var_3` = ?
		        ,`coleta_m_3` = ?
		        ,`coleta_passo_4` = ?
		        ,`coleta_carga_solar_4` = ?
		        ,`coleta_tipo_ativ_4` = ?
		        ,`coleta_temp_passo_4` = ?
		        ,`coleta_descr_tarefa_passo_4` = ?
		        ,`coleta_tbnm_4` = ?
		        ,`coleta_tbsm_4` = ?
		        ,`coleta_tgm_4` = ?
		        ,`coleta_ur_4` = ?
		        ,`coleta_var_4` = ?
		        ,`coleta_m_4` = ?
		        ,`coleta_passo_5` = ?
		        ,`coleta_carga_solar_5` = ?
		        ,`coleta_tipo_ativ_5` = ?
		        ,`coleta_temp_passo_5` = ?
		        ,`coleta_descr_tarefa_passo_5` = ?
		        ,`coleta_tbnm_5` = ?
		        ,`coleta_tbsm_5` = ?
		        ,`coleta_tgm_5` = ?
		        ,`coleta_ur_5` = ?
		        ,`coleta_var_5` = ?
		        ,`coleta_m_5` = ?
		        ,`coleta_passo_6` = ?
		        ,`coleta_carga_solar_6` = ?
		        ,`coleta_tipo_ativ_6` = ?
		        ,`coleta_temp_passo_6` = ?
		        ,`coleta_descr_tarefa_passo_6` = ?
		        ,`coleta_tbnm_6` = ?
		        ,`coleta_tbsm_6` = ?
		        ,`coleta_tgm_6` = ?
		        ,`coleta_ur_6` = ?
		        ,`coleta_var_6` = ?
		        ,`coleta_m_6` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idCALOR` = ?"
		)) 
		{
			$stmt->bind_param('sddddssssssssdssssssssssssssssssssssssssssssssssssssssssdsssdsddddddsssdsddddddsssdsddddddsssdsddddddsssdsddddddsssdsddddddddssss', $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_PROC_PROD_1, $sql_ANALISE_OBS_PERT_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_PROC_PROD_2, $sql_ANALISE_OBS_PERT_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_PROC_PROD_3, $sql_ANALISE_OBS_PERT_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_PROC_PROD_4, $sql_ANALISE_OBS_PERT_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_PROC_PROD_5, $sql_ANALISE_OBS_PERT_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_ANALISE_PROC_PROD_6, $sql_ANALISE_OBS_PERT_6, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_EPC, $sql_EPI, $sql_DISP_1, $sql_DISP_2, $sql_REF_LT, $sql_QDR_COMP, $sql_METAB, $sql_COLETAS, $sql_COLETA_PASSO_1, $sql_COLETA_CARGA_SOLAR_1, $sql_COLETA_TIPO_ATIV_1, $sql_COLETA_TEMP_PASSO_1, $sql_COLETA_DESCR_TAREFA_PASSO_1, $sql_COLETA_TBNM_1, $sql_COLETA_TBSM_1, $sql_COLETA_TGM_1, $sql_COLETA_UR_1, $sql_COLETA_VAR_1, $sql_COLETA_M_1, $sql_COLETA_PASSO_2, $sql_COLETA_CARGA_SOLAR_2, $sql_COLETA_TIPO_ATIV_2, $sql_COLETA_TEMP_PASSO_2, $sql_COLETA_DESCR_TAREFA_PASSO_2, $sql_COLETA_TBNM_2, $sql_COLETA_TBSM_2, $sql_COLETA_TGM_2, $sql_COLETA_UR_2, $sql_COLETA_VAR_2, $sql_COLETA_M_2, $sql_COLETA_PASSO_3, $sql_COLETA_CARGA_SOLAR_3, $sql_COLETA_TIPO_ATIV_3, $sql_COLETA_TEMP_PASSO_3, $sql_COLETA_DESCR_TAREFA_PASSO_3, $sql_COLETA_TBNM_3, $sql_COLETA_TBSM_3, $sql_COLETA_TGM_3, $sql_COLETA_UR_3, $sql_COLETA_VAR_3, $sql_COLETA_M_3, $sql_COLETA_PASSO_4, $sql_COLETA_CARGA_SOLAR_4, $sql_COLETA_TIPO_ATIV_4, $sql_COLETA_TEMP_PASSO_4, $sql_COLETA_DESCR_TAREFA_PASSO_4, $sql_COLETA_TBNM_4, $sql_COLETA_TBSM_4, $sql_COLETA_TGM_4, $sql_COLETA_UR_4, $sql_COLETA_VAR_4, $sql_COLETA_M_4, $sql_COLETA_PASSO_5, $sql_COLETA_CARGA_SOLAR_5, $sql_COLETA_TIPO_ATIV_5, $sql_COLETA_TEMP_PASSO_5, $sql_COLETA_DESCR_TAREFA_PASSO_5, $sql_COLETA_TBNM_5, $sql_COLETA_TBSM_5, $sql_COLETA_TGM_5, $sql_COLETA_UR_5, $sql_COLETA_VAR_5, $sql_COLETA_M_5, $sql_COLETA_PASSO_6, $sql_COLETA_CARGA_SOLAR_6, $sql_COLETA_TIPO_ATIV_6, $sql_COLETA_TEMP_PASSO_6, $sql_COLETA_DESCR_TAREFA_PASSO_6, $sql_COLETA_TBNM_6, $sql_COLETA_TBSM_6, $sql_COLETA_TGM_6, $sql_COLETA_UR_6, $sql_COLETA_VAR_6, $sql_COLETA_M_6, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza COLETA #1 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_1_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_1` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_1_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_1)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_1); }
						}
						else
						{
							return "0|COLETA #1 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza COLETA #2 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_2_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_2` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_2_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_2)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_2); }
						}
						else
						{
							return "0|COLETA #2 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza COLETA #3 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_3_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_3` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_3_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_3)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_3); }
						}
						else
						{
							return "0|COLETA #3 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza COLETA #4 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_4_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_4` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_4_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_4)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_4); }
						}
						else
						{
							return "0|COLETA #4 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza COLETA #5 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_5_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_5` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_5_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_5)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_5); }
						}
						else
						{
							return "0|COLETA #5 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza COLETA #6 - IMAGEM ATIVIDADE
				if($_COLETA_IMG_ATIV_FILENAME_6_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `coleta_img_ativ_filename_6` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_COLETA_IMG_ATIV_FILENAME_6_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_6)) { unlink (ANEXOS_PATH."/" . $o_COLETA_IMG_ATIV_FILENAME_6); }
						}
						else
						{
							return "0|COLETA #6 - IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				# Atualiza IMAGEM LOGOMARCA
				if($_LOGO_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `CALOR` SET 
					        `logo_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idCALOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_LOGO_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_LOGO_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_LOGO_FILENAME); }
						}
						else
						{
							return "0|IMAGEM LOGOMARCA - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}
				
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `CALOR` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idCALOR` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPLOAD
	####
	function executa_UPLOAD($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		#Extensoes permitidas para a foto
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (
				(($_FILES["file"]["type"] == "image/gif")   || 
				 ($_FILES["file"]["type"] == "image/jpeg")  || 
				 ($_FILES["file"]["type"] == "image/jpg")   || 
				 ($_FILES["file"]["type"] == "image/pjpeg") || 
				 ($_FILES["file"]["type"] == "image/x-png") || 
				 ($_FILES["file"]["type"] == "image/png")) && 
				 ($_FILES["file"]["size"] < 80000) &&
				 in_array($extension, $allowedExts)
			) 
		{
			if ($_FILES["file"]["error"] > 0) 
			{
				//return "Return Code: " . $_FILES["file"]["error"] . "<br>";
				return "0|".$_FILES["file"]["error"]."|error|";
				exit;
			}
			else
			{
				## Carrega nome existente da foto de perfil do usuario
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT U.`profile_pic` as foto
           FROM `SYSTEM_USER_ACCOUNT` U
          WHERE U.`idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_REG_ID = $mysqli->escape_String($_REG_ID);
					$stmt->bind_param('s', $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_NOME_FOTO_ATUAL);
					
					$stmt->fetch();
					
				}
				else
				{
					return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|";
					exit;
				}
				
				
				## Gera nome unico para o arquivo e
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(PROFILE_PICS_PATH."/" . $filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["file"]["tmp_name"],PROFILE_PICS_PATH."/" . $filename);
				
				## Atualiza base de dados com o nome novo da foto
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"UPDATE `SYSTEM_USER_ACCOUNT`
				    SET  `profile_pic` = ?
				  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_PROFILE_PIC = $mysqli->escape_String($filename);
					$sql_REG_ID      = $mysqli->escape_String($_REG_ID);
					//
					$stmt->bind_param('ss', $sql_PROFILE_PIC, $sql_REG_ID);
					
					if($stmt->execute())
					{
						## Apaga foto antiga do repositorio
						if( !isEmpty($o_NOME_FOTO_ATUAL) && 
						    ($o_NOME_FOTO_ATUAL != "no_profile_pic.jpg") && 
						    file_exists(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL)
						  )
						{ unlink(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL); }
						
						## Retorna OK com o nome da foto nova
						return "1|".TXT_MOD_PERFIL_FOTO_ATUALIZADA."|success|".$filename."|";
						exit;
					}
					else
					{
						return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
						exit;
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
			}
		}
		else
		{
			//echo "Invalid file";
			return "0|".TXT_UPLOAD_ARQUIVO_INVALIDO."|error|";
			exit;
		}
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = '%d/%m/%Y';
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id, AA.`idcliente` as idcliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
            AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_pert_1` as analise_obs_pert_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, AA.`analise_obs_pert_2` as analise_obs_pert_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_pert_3` as analise_obs_pert_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, AA.`analise_obs_pert_4` as analise_obs_pert_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_pert_5` as analise_obs_pert_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, AA.`analise_obs_pert_6` as analise_obs_pert_6, 
            AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, AA.`epi` as epi, AA.`disp_1` as disp_1, AA.`disp_2` as disp_2, AA.`ref_lt` as ref_lt, AA.`qdr_comp` as qdr_comp, AA.`metab` as metab, 
            AA.`coletas` as coletas, 
            AA.`coleta_passo_1` as coleta_passo_1, AA.`coleta_carga_solar_1` as coleta_carga_solar_1, AA.`coleta_tipo_ativ_1` as coleta_tipo_ativ_1, AA.`coleta_temp_passo_1` as coleta_temp_passo_1, AA.`coleta_descr_tarefa_passo_1` as coleta_descr_tarefa_passo_1, AA.`coleta_tbnm_1` as coleta_tbnm_1, AA.`coleta_tbsm_1` as coleta_tbsm_1, AA.`coleta_tgm_1` as coleta_tgm_1, AA.`coleta_ur_1` as coleta_ur_1, AA.`coleta_var_1` as coleta_var_1, AA.`coleta_m_1` as coleta_m_1, lower(AA.`coleta_img_ativ_filename_1`) as coleta_img_ativ_filename_1, 
            AA.`coleta_passo_2` as coleta_passo_2, AA.`coleta_carga_solar_2` as coleta_carga_solar_2, AA.`coleta_tipo_ativ_2` as coleta_tipo_ativ_2, AA.`coleta_temp_passo_2` as coleta_temp_passo_2, AA.`coleta_descr_tarefa_passo_2` as coleta_descr_tarefa_passo_2, AA.`coleta_tbnm_2` as coleta_tbnm_2, AA.`coleta_tbsm_2` as coleta_tbsm_2, AA.`coleta_tgm_2` as coleta_tgm_2, AA.`coleta_ur_2` as coleta_ur_2, AA.`coleta_var_2` as coleta_var_2, AA.`coleta_m_2` as coleta_m_2, lower(AA.`coleta_img_ativ_filename_2`) as coleta_img_ativ_filename_2, 
            AA.`coleta_passo_3` as coleta_passo_3, AA.`coleta_carga_solar_3` as coleta_carga_solar_3, AA.`coleta_tipo_ativ_3` as coleta_tipo_ativ_3, AA.`coleta_temp_passo_3` as coleta_temp_passo_3, AA.`coleta_descr_tarefa_passo_3` as coleta_descr_tarefa_passo_3, AA.`coleta_tbnm_3` as coleta_tbnm_3, AA.`coleta_tbsm_3` as coleta_tbsm_3, AA.`coleta_tgm_3` as coleta_tgm_3, AA.`coleta_ur_3` as coleta_ur_3, AA.`coleta_var_3` as coleta_var_3, AA.`coleta_m_3` as coleta_m_3, lower(AA.`coleta_img_ativ_filename_3`) as coleta_img_ativ_filename_3, 
            AA.`coleta_passo_4` as coleta_passo_4, AA.`coleta_carga_solar_4` as coleta_carga_solar_4, AA.`coleta_tipo_ativ_4` as coleta_tipo_ativ_4, AA.`coleta_temp_passo_4` as coleta_temp_passo_4, AA.`coleta_descr_tarefa_passo_4` as coleta_descr_tarefa_passo_4, AA.`coleta_tbnm_4` as coleta_tbnm_4, AA.`coleta_tbsm_4` as coleta_tbsm_4, AA.`coleta_tgm_4` as coleta_tgm_4, AA.`coleta_ur_4` as coleta_ur_4, AA.`coleta_var_4` as coleta_var_4, AA.`coleta_m_4` as coleta_m_4, lower(AA.`coleta_img_ativ_filename_4`) as coleta_img_ativ_filename_4, 
            AA.`coleta_passo_5` as coleta_passo_5, AA.`coleta_carga_solar_5` as coleta_carga_solar_5, AA.`coleta_tipo_ativ_5` as coleta_tipo_ativ_5, AA.`coleta_temp_passo_5` as coleta_temp_passo_5, AA.`coleta_descr_tarefa_passo_5` as coleta_descr_tarefa_passo_5, AA.`coleta_tbnm_5` as coleta_tbnm_5, AA.`coleta_tbsm_5` as coleta_tbsm_5, AA.`coleta_tgm_5` as coleta_tgm_5, AA.`coleta_ur_5` as coleta_ur_5, AA.`coleta_var_5` as coleta_var_5, AA.`coleta_m_5` as coleta_m_5, lower(AA.`coleta_img_ativ_filename_5`) as coleta_img_ativ_filename_5, 
            AA.`coleta_passo_6` as coleta_passo_6, AA.`coleta_carga_solar_6` as coleta_carga_solar_6, AA.`coleta_tipo_ativ_6` as coleta_tipo_ativ_6, AA.`coleta_temp_passo_6` as coleta_temp_passo_6, AA.`coleta_descr_tarefa_passo_6` as coleta_descr_tarefa_passo_6, AA.`coleta_tbnm_6` as coleta_tbnm_6, AA.`coleta_tbsm_6` as coleta_tbsm_6, AA.`coleta_tgm_6` as coleta_tgm_6, AA.`coleta_ur_6` as coleta_ur_6, AA.`coleta_var_6` as coleta_var_6, AA.`coleta_m_6` as coleta_m_6, lower(AA.`coleta_img_ativ_filename_6`) as coleta_img_ativ_filename_6, 
            AA.`resp_campo_idcolaborador` as resp_campo_idcolaborador, AA.`resp_tecnico_idcolaborador` as resp_tecnico_idcolaborador, upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`logo_filename`) as logo_filename
       FROM `CALOR` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCALOR` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssss', $sql_DATA_ELABORACAO, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_PERT_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_PERT_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_PERT_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_PERT_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_PERT_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_PERT_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_EPI, $o_DISP_1, $o_DISP_2, $o_REF_LT, $o_QDR_COMP, $o_METAB, $o_COLETAS, $o_COLETA_PASSO_1, $o_COLETA_CARGA_SOLAR_1, $o_COLETA_TIPO_ATIV_1, $o_COLETA_TEMP_PASSO_1, $o_COLETA_DESCR_TAREFA_PASSO_1, $o_COLETA_TBNM_1, $o_COLETA_TBSM_1, $o_COLETA_TGM_1, $o_COLETA_UR_1, $o_COLETA_VAR_1, $o_COLETA_M_1, $o_COLETA_IMG_ATIV_FILENAME_1, $o_COLETA_PASSO_2, $o_COLETA_CARGA_SOLAR_2, $o_COLETA_TIPO_ATIV_2, $o_COLETA_TEMP_PASSO_2, $o_COLETA_DESCR_TAREFA_PASSO_2, $o_COLETA_TBNM_2, $o_COLETA_TBSM_2, $o_COLETA_TGM_2, $o_COLETA_UR_2, $o_COLETA_VAR_2, $o_COLETA_M_2, $o_COLETA_IMG_ATIV_FILENAME_2, $o_COLETA_PASSO_3, $o_COLETA_CARGA_SOLAR_3, $o_COLETA_TIPO_ATIV_3, $o_COLETA_TEMP_PASSO_3, $o_COLETA_DESCR_TAREFA_PASSO_3, $o_COLETA_TBNM_3, $o_COLETA_TBSM_3, $o_COLETA_TGM_3, $o_COLETA_UR_3, $o_COLETA_VAR_3, $o_COLETA_M_3, $o_COLETA_IMG_ATIV_FILENAME_3, $o_COLETA_PASSO_4, $o_COLETA_CARGA_SOLAR_4, $o_COLETA_TIPO_ATIV_4, $o_COLETA_TEMP_PASSO_4, $o_COLETA_DESCR_TAREFA_PASSO_4, $o_COLETA_TBNM_4, $o_COLETA_TBSM_4, $o_COLETA_TGM_4, $o_COLETA_UR_4, $o_COLETA_VAR_4, $o_COLETA_M_4, $o_COLETA_IMG_ATIV_FILENAME_4, $o_COLETA_PASSO_5, $o_COLETA_CARGA_SOLAR_5, $o_COLETA_TIPO_ATIV_5, $o_COLETA_TEMP_PASSO_5, $o_COLETA_DESCR_TAREFA_PASSO_5, $o_COLETA_TBNM_5, $o_COLETA_TBSM_5, $o_COLETA_TGM_5, $o_COLETA_UR_5, $o_COLETA_VAR_5, $o_COLETA_M_5, $o_COLETA_IMG_ATIV_FILENAME_5, $o_COLETA_PASSO_6, $o_COLETA_CARGA_SOLAR_6, $o_COLETA_TIPO_ATIV_6, $o_COLETA_TEMP_PASSO_6, $o_COLETA_DESCR_TAREFA_PASSO_6, $o_COLETA_TBNM_6, $o_COLETA_TBSM_6, $o_COLETA_TGM_6, $o_COLETA_UR_6, $o_COLETA_VAR_6, $o_COLETA_M_6, $o_COLETA_IMG_ATIV_FILENAME_6, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			// Formata Unescape de Textareas
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_PERT_1 = unescape_string($o_ANALISE_OBS_PERT_1);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_PERT_2 = unescape_string($o_ANALISE_OBS_PERT_2);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_PERT_3 = unescape_string($o_ANALISE_OBS_PERT_3);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_PERT_4 = unescape_string($o_ANALISE_OBS_PERT_4);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_PERT_5 = unescape_string($o_ANALISE_OBS_PERT_5);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_PERT_6 = unescape_string($o_ANALISE_OBS_PERT_6);
			$o_COLETA_DESCR_TAREFA_PASSO_1 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_1);
			$o_COLETA_DESCR_TAREFA_PASSO_2 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_2);
			$o_COLETA_DESCR_TAREFA_PASSO_3 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_3);
			$o_COLETA_DESCR_TAREFA_PASSO_4 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_4);
			$o_COLETA_DESCR_TAREFA_PASSO_5 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_5);
			$o_COLETA_DESCR_TAREFA_PASSO_6 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_6);
			
			// Formata Datas Nulas
			if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_IDCLIENTE."|".$o_ANO."|".$o_MES."|".$o_PLANILHA_NUM."|".$o_UNIDADE_SITE."|".$o_DATA_ELABORACAO."|".$o_AREA."|".$o_SETOR."|".$o_GES."|".$o_CARGO_FUNCAO."|".$o_CBO."|".$o_ATIV_MACRO."|".$o_ANALISES."|".$o_ANALISE_AMOSTRA_1."|".$o_ANALISE_DATA_AMOSTRAGEM_1."|".$o_ANALISE_TAREFA_EXEC_1."|".$o_ANALISE_PROC_PROD_1."|".$o_ANALISE_OBS_PERT_1."|".$o_ANALISE_AMOSTRA_2."|".$o_ANALISE_DATA_AMOSTRAGEM_2."|".$o_ANALISE_TAREFA_EXEC_2."|".$o_ANALISE_PROC_PROD_2."|".$o_ANALISE_OBS_PERT_2."|".$o_ANALISE_AMOSTRA_3."|".$o_ANALISE_DATA_AMOSTRAGEM_3."|".$o_ANALISE_TAREFA_EXEC_3."|".$o_ANALISE_PROC_PROD_3."|".$o_ANALISE_OBS_PERT_3."|".$o_ANALISE_AMOSTRA_4."|".$o_ANALISE_DATA_AMOSTRAGEM_4."|".$o_ANALISE_TAREFA_EXEC_4."|".$o_ANALISE_PROC_PROD_4."|".$o_ANALISE_OBS_PERT_4."|".$o_ANALISE_AMOSTRA_5."|".$o_ANALISE_DATA_AMOSTRAGEM_5."|".$o_ANALISE_TAREFA_EXEC_5."|".$o_ANALISE_PROC_PROD_5."|".$o_ANALISE_OBS_PERT_5."|".$o_ANALISE_AMOSTRA_6."|".$o_ANALISE_DATA_AMOSTRAGEM_6."|".$o_ANALISE_TAREFA_EXEC_6."|".$o_ANALISE_PROC_PROD_6."|".$o_ANALISE_OBS_PERT_6."|".$o_JOR_TRAB."|".$o_TEMPO_EXPO."|".$o_TIPO_EXPO."|".$o_MEIO_PROPAG."|".$o_FONTE_GERADORA."|".$o_EPC."|".$o_EPI."|".$o_DISP_1."|".$o_DISP_2."|".$o_REF_LT."|".$o_QDR_COMP."|".$o_METAB."|".$o_COLETAS."|".$o_COLETA_PASSO_1."|".$o_COLETA_CARGA_SOLAR_1."|".$o_COLETA_TIPO_ATIV_1."|".$o_COLETA_TEMP_PASSO_1."|".$o_COLETA_DESCR_TAREFA_PASSO_1."|".$o_COLETA_TBNM_1."|".$o_COLETA_TBSM_1."|".$o_COLETA_TGM_1."|".$o_COLETA_UR_1."|".$o_COLETA_VAR_1."|".$o_COLETA_M_1."|".$o_COLETA_IMG_ATIV_FILENAME_1."|".$o_COLETA_PASSO_2."|".$o_COLETA_CARGA_SOLAR_2."|".$o_COLETA_TIPO_ATIV_2."|".$o_COLETA_TEMP_PASSO_2."|".$o_COLETA_DESCR_TAREFA_PASSO_2."|".$o_COLETA_TBNM_2."|".$o_COLETA_TBSM_2."|".$o_COLETA_TGM_2."|".$o_COLETA_UR_2."|".$o_COLETA_VAR_2."|".$o_COLETA_M_2."|".$o_COLETA_IMG_ATIV_FILENAME_2."|".$o_COLETA_PASSO_3."|".$o_COLETA_CARGA_SOLAR_3."|".$o_COLETA_TIPO_ATIV_3."|".$o_COLETA_TEMP_PASSO_3."|".$o_COLETA_DESCR_TAREFA_PASSO_3."|".$o_COLETA_TBNM_3."|".$o_COLETA_TBSM_3."|".$o_COLETA_TGM_3."|".$o_COLETA_UR_3."|".$o_COLETA_VAR_3."|".$o_COLETA_M_3."|".$o_COLETA_IMG_ATIV_FILENAME_3."|".$o_COLETA_PASSO_4."|".$o_COLETA_CARGA_SOLAR_4."|".$o_COLETA_TIPO_ATIV_4."|".$o_COLETA_TEMP_PASSO_4."|".$o_COLETA_DESCR_TAREFA_PASSO_4."|".$o_COLETA_TBNM_4."|".$o_COLETA_TBSM_4."|".$o_COLETA_TGM_4."|".$o_COLETA_UR_4."|".$o_COLETA_VAR_4."|".$o_COLETA_M_4."|".$o_COLETA_IMG_ATIV_FILENAME_4."|".$o_COLETA_PASSO_5."|".$o_COLETA_CARGA_SOLAR_5."|".$o_COLETA_TIPO_ATIV_5."|".$o_COLETA_TEMP_PASSO_5."|".$o_COLETA_DESCR_TAREFA_PASSO_5."|".$o_COLETA_TBNM_5."|".$o_COLETA_TBSM_5."|".$o_COLETA_TGM_5."|".$o_COLETA_UR_5."|".$o_COLETA_VAR_5."|".$o_COLETA_M_5."|".$o_COLETA_IMG_ATIV_FILENAME_5."|".$o_COLETA_PASSO_6."|".$o_COLETA_CARGA_SOLAR_6."|".$o_COLETA_TIPO_ATIV_6."|".$o_COLETA_TEMP_PASSO_6."|".$o_COLETA_DESCR_TAREFA_PASSO_6."|".$o_COLETA_TBNM_6."|".$o_COLETA_TBSM_6."|".$o_COLETA_TGM_6."|".$o_COLETA_UR_6."|".$o_COLETA_VAR_6."|".$o_COLETA_M_6."|".$o_COLETA_IMG_ATIV_FILENAME_6."|".$o_RESP_CAMPO_IDCOLABORADOR."|".$o_RESP_TECNICO_IDCOLABORADOR."|".$o_REGISTRO_RC."|".$o_REGISTRO_RT."|".$o_LOGO_FILENAME."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCALOR` as id, upper(CLNT.`nome_interno`) as nome_interno, AA.`ano` as ano, AA.`mes` as mes, 
            AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, 
            AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, 
            AA.`ativ_macro` as ativ_macro, AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, 
            date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, 
            AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_pert_1` as analise_obs_pert_1, 
            AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, 
            AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, 
            AA.`analise_obs_pert_2` as analise_obs_pert_2, AA.`analise_amostra_3` as analise_amostra_3, 
            date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, 
            AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_pert_3` as analise_obs_pert_3, 
            AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, 
            AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, 
            AA.`analise_obs_pert_4` as analise_obs_pert_4, AA.`analise_amostra_5` as analise_amostra_5, 
            date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, 
            AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_pert_5` as analise_obs_pert_5, 
            AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, 
            AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, 
            AA.`analise_obs_pert_6` as analise_obs_pert_6, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, 
            AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, 
            AA.`epi` as epi, AA.`disp_1` as disp_1, AA.`disp_2` as disp_2, AA.`ref_lt` as ref_lt, AA.`qdr_comp` as qdr_comp, 
            AA.`metab` as metab, AA.`coletas` as coletas, 
            AA.`coleta_passo_1` as coleta_passo_1, 
            AA.`coleta_carga_solar_1` as coleta_carga_solar_1, AA.`coleta_tipo_ativ_1` as coleta_tipo_ativ_1, 
            AA.`coleta_temp_passo_1` as coleta_temp_passo_1, AA.`coleta_descr_tarefa_passo_1` as coleta_descr_tarefa_passo_1, 
            AA.`coleta_tbnm_1` as coleta_tbnm_1, AA.`coleta_tbsm_1` as coleta_tbsm_1, AA.`coleta_tgm_1` as coleta_tgm_1, 
            AA.`coleta_ur_1` as coleta_ur_1, AA.`coleta_var_1` as coleta_var_1, AA.`coleta_m_1` as coleta_m_1, 
            lower(AA.`coleta_img_ativ_filename_1`) as coleta_img_ativ_filename_1, 
            AA.`coleta_passo_2` as coleta_passo_2, AA.`coleta_carga_solar_2` as coleta_carga_solar_2, 
            AA.`coleta_tipo_ativ_2` as coleta_tipo_ativ_2, AA.`coleta_temp_passo_2` as coleta_temp_passo_2, 
            AA.`coleta_descr_tarefa_passo_2` as coleta_descr_tarefa_passo_2, AA.`coleta_tbnm_2` as coleta_tbnm_2, 
            AA.`coleta_tbsm_2` as coleta_tbsm_2, AA.`coleta_tgm_2` as coleta_tgm_2, AA.`coleta_ur_2` as coleta_ur_2, 
            AA.`coleta_var_2` as coleta_var_2, AA.`coleta_m_2` as coleta_m_2, 
            lower(AA.`coleta_img_ativ_filename_2`) as coleta_img_ativ_filename_2, 
            AA.`coleta_passo_3` as coleta_passo_3, 
            AA.`coleta_carga_solar_3` as coleta_carga_solar_3, AA.`coleta_tipo_ativ_3` as coleta_tipo_ativ_3, 
            AA.`coleta_temp_passo_3` as coleta_temp_passo_3, AA.`coleta_descr_tarefa_passo_3` as coleta_descr_tarefa_passo_3, 
            AA.`coleta_tbnm_3` as coleta_tbnm_3, AA.`coleta_tbsm_3` as coleta_tbsm_3, AA.`coleta_tgm_3` as coleta_tgm_3, 
            AA.`coleta_ur_3` as coleta_ur_3, AA.`coleta_var_3` as coleta_var_3, AA.`coleta_m_3` as coleta_m_3, 
            lower(AA.`coleta_img_ativ_filename_3`) as coleta_img_ativ_filename_3, 
            AA.`coleta_passo_4` as coleta_passo_4, AA.`coleta_carga_solar_4` as coleta_carga_solar_4, 
            AA.`coleta_tipo_ativ_4` as coleta_tipo_ativ_4, AA.`coleta_temp_passo_4` as coleta_temp_passo_4, 
            AA.`coleta_descr_tarefa_passo_4` as coleta_descr_tarefa_passo_4, AA.`coleta_tbnm_4` as coleta_tbnm_4, 
            AA.`coleta_tbsm_4` as coleta_tbsm_4, AA.`coleta_tgm_4` as coleta_tgm_4, AA.`coleta_ur_4` as coleta_ur_4, 
            AA.`coleta_var_4` as coleta_var_4, AA.`coleta_m_4` as coleta_m_4, 
            lower(AA.`coleta_img_ativ_filename_4`) as coleta_img_ativ_filename_4, 
            AA.`coleta_passo_5` as coleta_passo_5, 
            AA.`coleta_carga_solar_5` as coleta_carga_solar_5, AA.`coleta_tipo_ativ_5` as coleta_tipo_ativ_5, 
            AA.`coleta_temp_passo_5` as coleta_temp_passo_5, AA.`coleta_descr_tarefa_passo_5` as coleta_descr_tarefa_passo_5, 
            AA.`coleta_tbnm_5` as coleta_tbnm_5, AA.`coleta_tbsm_5` as coleta_tbsm_5, AA.`coleta_tgm_5` as coleta_tgm_5, 
            AA.`coleta_ur_5` as coleta_ur_5, AA.`coleta_var_5` as coleta_var_5, AA.`coleta_m_5` as coleta_m_5, 
            lower(AA.`coleta_img_ativ_filename_5`) as coleta_img_ativ_filename_5, 
            AA.`coleta_passo_6` as coleta_passo_6, AA.`coleta_carga_solar_6` as coleta_carga_solar_6, 
            AA.`coleta_tipo_ativ_6` as coleta_tipo_ativ_6, AA.`coleta_temp_passo_6` as coleta_temp_passo_6, 
            AA.`coleta_descr_tarefa_passo_6` as coleta_descr_tarefa_passo_6, AA.`coleta_tbnm_6` as coleta_tbnm_6, 
            AA.`coleta_tbsm_6` as coleta_tbsm_6, AA.`coleta_tgm_6` as coleta_tgm_6, AA.`coleta_ur_6` as coleta_ur_6, 
            AA.`coleta_var_6` as coleta_var_6, AA.`coleta_m_6` as coleta_m_6, 
            lower(AA.`coleta_img_ativ_filename_6`) as coleta_img_ativ_filename_6, 
            (SELECT concat(upper(UA1.`nome`),' ',upper(UA1.`sobrenome`),' (',lower(CLBRDR1.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA1
              WHERE UA1.`username` = CLBRDR1.`username`
              LIMIT 1
            ) as resp_campo,
            (SELECT concat(upper(UA2.`nome`),' ',upper(UA2.`sobrenome`),' (',lower(CLBRDR2.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA2
              WHERE UA2.`username` = CLBRDR2.`username`
              LIMIT 1
            ) as resp_tec,
            upper(AA.`registro_rc`) as registro_rc, 
            upper(AA.`registro_rt`) as registro_rt, 
            lower(AA.`logo_filename`) as logo_filename,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `CALOR` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
    LEFT JOIN `COLABORADOR` as CLBRDR1
           ON CLBRDR1.`idcolaborador` = AA.`resp_campo_idcolaborador`
    LEFT JOIN `COLABORADOR` as CLBRDR2
           ON CLBRDR2.`idcolaborador` = AA.`resp_tecnico_idcolaborador`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCALOR` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssssss', $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_PERT_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_PERT_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_PERT_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_PERT_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_PERT_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_PERT_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_EPI, $o_DISP_1, $o_DISP_2, $o_REF_LT, $o_QDR_COMP, $o_METAB, $o_COLETAS, $o_COLETA_PASSO_1, $o_COLETA_CARGA_SOLAR_1, $o_COLETA_TIPO_ATIV_1, $o_COLETA_TEMP_PASSO_1, $o_COLETA_DESCR_TAREFA_PASSO_1, $o_COLETA_TBNM_1, $o_COLETA_TBSM_1, $o_COLETA_TGM_1, $o_COLETA_UR_1, $o_COLETA_VAR_1, $o_COLETA_M_1, $o_COLETA_IMG_ATIV_FILENAME_1, $o_COLETA_PASSO_2, $o_COLETA_CARGA_SOLAR_2, $o_COLETA_TIPO_ATIV_2, $o_COLETA_TEMP_PASSO_2, $o_COLETA_DESCR_TAREFA_PASSO_2, $o_COLETA_TBNM_2, $o_COLETA_TBSM_2, $o_COLETA_TGM_2, $o_COLETA_UR_2, $o_COLETA_VAR_2, $o_COLETA_M_2, $o_COLETA_IMG_ATIV_FILENAME_2, $o_COLETA_PASSO_3, $o_COLETA_CARGA_SOLAR_3, $o_COLETA_TIPO_ATIV_3, $o_COLETA_TEMP_PASSO_3, $o_COLETA_DESCR_TAREFA_PASSO_3, $o_COLETA_TBNM_3, $o_COLETA_TBSM_3, $o_COLETA_TGM_3, $o_COLETA_UR_3, $o_COLETA_VAR_3, $o_COLETA_M_3, $o_COLETA_IMG_ATIV_FILENAME_3, $o_COLETA_PASSO_4, $o_COLETA_CARGA_SOLAR_4, $o_COLETA_TIPO_ATIV_4, $o_COLETA_TEMP_PASSO_4, $o_COLETA_DESCR_TAREFA_PASSO_4, $o_COLETA_TBNM_4, $o_COLETA_TBSM_4, $o_COLETA_TGM_4, $o_COLETA_UR_4, $o_COLETA_VAR_4, $o_COLETA_M_4, $o_COLETA_IMG_ATIV_FILENAME_4, $o_COLETA_PASSO_5, $o_COLETA_CARGA_SOLAR_5, $o_COLETA_TIPO_ATIV_5, $o_COLETA_TEMP_PASSO_5, $o_COLETA_DESCR_TAREFA_PASSO_5, $o_COLETA_TBNM_5, $o_COLETA_TBSM_5, $o_COLETA_TGM_5, $o_COLETA_UR_5, $o_COLETA_VAR_5, $o_COLETA_M_5, $o_COLETA_IMG_ATIV_FILENAME_5, $o_COLETA_PASSO_6, $o_COLETA_CARGA_SOLAR_6, $o_COLETA_TIPO_ATIV_6, $o_COLETA_TEMP_PASSO_6, $o_COLETA_DESCR_TAREFA_PASSO_6, $o_COLETA_TBNM_6, $o_COLETA_TBSM_6, $o_COLETA_TGM_6, $o_COLETA_UR_6, $o_COLETA_VAR_6, $o_COLETA_M_6, $o_COLETA_IMG_ATIV_FILENAME_6, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_LOGO_FILENAME, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_PERT_1 = unescape_string($o_ANALISE_OBS_PERT_1);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_PERT_2 = unescape_string($o_ANALISE_OBS_PERT_2);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_PERT_3 = unescape_string($o_ANALISE_OBS_PERT_3);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_PERT_4 = unescape_string($o_ANALISE_OBS_PERT_4);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_PERT_5 = unescape_string($o_ANALISE_OBS_PERT_5);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_PERT_6 = unescape_string($o_ANALISE_OBS_PERT_6);
			$o_COLETA_DESCR_TAREFA_PASSO_1 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_1);
			$o_COLETA_DESCR_TAREFA_PASSO_2 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_2);
			$o_COLETA_DESCR_TAREFA_PASSO_3 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_3);
			$o_COLETA_DESCR_TAREFA_PASSO_4 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_4);
			$o_COLETA_DESCR_TAREFA_PASSO_5 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_5);
			$o_COLETA_DESCR_TAREFA_PASSO_6 = unescape_string($o_COLETA_DESCR_TAREFA_PASSO_6);
			$o_ANALISE_TAREFA_EXEC_1 = nl2br($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = nl2br($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_PERT_1 = nl2br($o_ANALISE_OBS_PERT_1);
			$o_ANALISE_TAREFA_EXEC_2 = nl2br($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = nl2br($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_PERT_2 = nl2br($o_ANALISE_OBS_PERT_2);
			$o_ANALISE_TAREFA_EXEC_3 = nl2br($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = nl2br($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_PERT_3 = nl2br($o_ANALISE_OBS_PERT_3);
			$o_ANALISE_TAREFA_EXEC_4 = nl2br($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = nl2br($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_PERT_4 = nl2br($o_ANALISE_OBS_PERT_4);
			$o_ANALISE_TAREFA_EXEC_5 = nl2br($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = nl2br($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_PERT_5 = nl2br($o_ANALISE_OBS_PERT_5);
			$o_ANALISE_TAREFA_EXEC_6 = nl2br($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = nl2br($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_PERT_6 = nl2br($o_ANALISE_OBS_PERT_6);
			$o_COLETA_DESCR_TAREFA_PASSO_1 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_1);
			$o_COLETA_DESCR_TAREFA_PASSO_2 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_2);
			$o_COLETA_DESCR_TAREFA_PASSO_3 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_3);
			$o_COLETA_DESCR_TAREFA_PASSO_4 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_4);
			$o_COLETA_DESCR_TAREFA_PASSO_5 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_5);
			$o_COLETA_DESCR_TAREFA_PASSO_6 = nl2br($o_COLETA_DESCR_TAREFA_PASSO_6);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_CALOR_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_CALOR_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_CALOR_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_CALOR_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_CALOR_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_CALOR_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_CALOR_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_CALOR_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_CALOR_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_CALOR_DEZ; }
				if( $o_ANALISES == "1"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_1; }
				if( $o_ANALISES == "2"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_2; }
				if( $o_ANALISES == "3"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_3; }
				if( $o_ANALISES == "4"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_4; }
				if( $o_ANALISES == "5"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_5; }
				if( $o_ANALISES == "6"){ $o_ANALISES_TXT = TXT_LAUDOS_CALOR_6; }
				if( $o_COLETAS == "1"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_1; }
				if( $o_COLETAS == "2"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_2; }
				if( $o_COLETAS == "3"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_3; }
				if( $o_COLETAS == "4"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_4; }
				if( $o_COLETAS == "5"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_5; }
				if( $o_COLETAS == "6"){ $o_COLETAS_TXT = TXT_LAUDOS_CALOR_6; }
				
				// Formata Datas Nulas
				if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_CLIENTE.':</b></th><td>'.$o_IDCLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_NUMERO_PLANILHA.':</b></th><td>'.$o_PLANILHA_NUM.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_UNIDADESITE.':</b></th><td>'.$o_UNIDADE_SITE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_DATA_ELABORACAO.':</b></th><td>'.$o_DATA_ELABORACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_AREA.':</b></th><td>'.$o_AREA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_SETOR.':</b></th><td>'.$o_SETOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_GES.':</b></th><td>'.$o_GES.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_CARGOFUNCAO.':</b></th><td>'.$o_CARGO_FUNCAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_CBO.':</b></th><td>'.$o_CBO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ATIVIDADE_MACRO.':</b></th><td>'.$o_ATIV_MACRO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISES.':</b></th><td>'.$o_ANALISES_TXT.'</td></tr>';
				
				# Formata Analises
				$o_ANALISES_1_SET = 0;
				$o_ANALISES_2_SET = 0;
				$o_ANALISES_3_SET = 0;
				$o_ANALISES_4_SET = 0;
				$o_ANALISES_5_SET = 0;
				$o_ANALISES_6_SET = 0;
				switch($o_ANALISES)
				{
					case '1':
						$o_ANALISES_1_SET = 1;
						break;
					case '2':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						break;
					case '3':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						break;
					case '4':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						break;
					case '5':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						break;
					case '6':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						$o_ANALISES_6_SET = 1;
						break;
					default:
						break;
				}
				if($o_ANALISES_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_1_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_1_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_1_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_1_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_1_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_1.'</td></tr>';
				}
				if($o_ANALISES_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_2_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_2_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_2_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_2_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_2_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_2.'</td></tr>';
				}
				if($o_ANALISES_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_3_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_3_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_3_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_3_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_3_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_3.'</td></tr>';
				}
				if($o_ANALISES_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_4_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_4_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_4_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_4_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_4_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_4.'</td></tr>';
				}
				if($o_ANALISES_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_5_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_5_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_5_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_5_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_5_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_5.'</td></tr>';
				}
				if($o_ANALISES_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_6_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_6_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_6_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_6_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_ANALISE_6_OBS_PERTINENTE.':</b></th><td>'.$o_ANALISE_OBS_PERT_6.'</td></tr>';
				}
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_JORNADA_DE_TRABALHO.':</b></th><td>'.$o_JOR_TRAB.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_TEMPO_EXPOSICAO.':</b></th><td>'.$o_TEMPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_TIPO_EXPOSICAO.':</b></th><td>'.$o_TIPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_MEIO_DE_PROPAGACAO.':</b></th><td>'.$o_MEIO_PROPAG.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_FONTE_GERADORA.':</b></th><td>'.$o_FONTE_GERADORA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_EPC.':</b></th><td>'.$o_EPC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_EPI.':</b></th><td>'.$o_EPI.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_DISPOSITIVO_1.':</b></th><td>'.$o_DISP_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_DISPOSITIVO_2.':</b></th><td>'.$o_DISP_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_LIMITE_DE_TOLERANCIA.':</b></th><td>'.$o_REF_LT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_QUADRO_COMPARATIVO.':</b></th><td>'.$o_QDR_COMP.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_METABOLISMO.':</b></th><td>'.$o_METAB.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETAS.':</b></th><td>'.$o_COLETAS_TXT.'</td></tr>';
				
				# Formata Coletas
				$o_COLETAS_1_SET = 0;
				$o_COLETAS_2_SET = 0;
				$o_COLETAS_3_SET = 0;
				$o_COLETAS_4_SET = 0;
				$o_COLETAS_5_SET = 0;
				$o_COLETAS_6_SET = 0;
				switch($o_COLETAS)
				{
					case '1':
						$o_COLETAS_1_SET = 1;
						break;
					case '2':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						break;
					case '3':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						break;
					case '4':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						break;
					case '5':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						break;
					case '6':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						$o_COLETAS_6_SET = 1;
						break;
					default:
						break;
				}
				if($o_COLETAS_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_PASSO.':</b></th><td>'.$o_COLETA_PASSO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_TBNM.':</b></th><td>'.$o_COLETA_TBNM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_TBSM.':</b></th><td>'.$o_COLETA_TBSM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_TGM.':</b></th><td>'.$o_COLETA_TGM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_UR.':</b></th><td>'.$o_COLETA_UR_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_VAR.':</b></th><td>'.$o_COLETA_VAR_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_M.':</b></th><td>'.$o_COLETA_M_1.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_1){ $o_COLETA_IMG_ATIV_FILENAME_1_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_1.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_1.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_1_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_1_TXT.'</td></tr>';
				}
				if($o_COLETAS_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_PASSO.':</b></th><td>'.$o_COLETA_PASSO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_TBNM.':</b></th><td>'.$o_COLETA_TBNM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_TBSM.':</b></th><td>'.$o_COLETA_TBSM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_TGM.':</b></th><td>'.$o_COLETA_TGM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_UR.':</b></th><td>'.$o_COLETA_UR_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_VAR.':</b></th><td>'.$o_COLETA_VAR_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_M.':</b></th><td>'.$o_COLETA_M_2.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_2){ $o_COLETA_IMG_ATIV_FILENAME_2_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_2.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_2.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_2_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_2_TXT.'</td></tr>';
				}
				if($o_COLETAS_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_PASSO.':</b></th><td>'.$o_COLETA_PASSO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_TBNM.':</b></th><td>'.$o_COLETA_TBNM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_TBSM.':</b></th><td>'.$o_COLETA_TBSM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_TGM.':</b></th><td>'.$o_COLETA_TGM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_UR.':</b></th><td>'.$o_COLETA_UR_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_VAR.':</b></th><td>'.$o_COLETA_VAR_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_M.':</b></th><td>'.$o_COLETA_M_3.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_3){ $o_COLETA_IMG_ATIV_FILENAME_3_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_3.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_3.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_3_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_3_TXT.'</td></tr>';
				}
				if($o_COLETAS_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_PASSO.':</b></th><td>'.$o_COLETA_PASSO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_TBNM.':</b></th><td>'.$o_COLETA_TBNM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_TBSM.':</b></th><td>'.$o_COLETA_TBSM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_TGM.':</b></th><td>'.$o_COLETA_TGM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_UR.':</b></th><td>'.$o_COLETA_UR_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_VAR.':</b></th><td>'.$o_COLETA_VAR_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_M.':</b></th><td>'.$o_COLETA_M_4.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_4){ $o_COLETA_IMG_ATIV_FILENAME_4_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_4.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_4.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_4_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_4_TXT.'</td></tr>';
				}
				if($o_COLETAS_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_PASSO.':</b></th><td>'.$o_COLETA_PASSO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_TBNM.':</b></th><td>'.$o_COLETA_TBNM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_TBSM.':</b></th><td>'.$o_COLETA_TBSM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_TGM.':</b></th><td>'.$o_COLETA_TGM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_UR.':</b></th><td>'.$o_COLETA_UR_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_VAR.':</b></th><td>'.$o_COLETA_VAR_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_M.':</b></th><td>'.$o_COLETA_M_5.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_5){ $o_COLETA_IMG_ATIV_FILENAME_5_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_5.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_5.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_5_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_5_TXT.'</td></tr>';
				}
				if($o_COLETAS_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_PASSO.':</b></th><td>'.$o_COLETA_PASSO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_CARGA_SOLAR.':</b></th><td>'.$o_COLETA_CARGA_SOLAR_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_TIPO_DE_ATIVIDADE.':</b></th><td>'.$o_COLETA_TIPO_ATIV_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_TEMPERATURA_DO_PASSO.':</b></th><td>'.$o_COLETA_TEMP_PASSO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_DESCRICAO_DA_TAREFA.':</b></th><td>'.$o_COLETA_DESCR_TAREFA_PASSO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_TBNM.':</b></th><td>'.$o_COLETA_TBNM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_TBSM.':</b></th><td>'.$o_COLETA_TBSM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_TGM.':</b></th><td>'.$o_COLETA_TGM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_UR.':</b></th><td>'.$o_COLETA_UR_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_VAR.':</b></th><td>'.$o_COLETA_VAR_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_M.':</b></th><td>'.$o_COLETA_M_6.'</td></tr>';
					if($o_COLETA_IMG_ATIV_FILENAME_6){ $o_COLETA_IMG_ATIV_FILENAME_6_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_COLETA_IMG_ATIV_FILENAME_6.'" target="_blank">'.$o_COLETA_IMG_ATIV_FILENAME_6.'</a>'; }
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_COLETA_6_IMG_ATIV.':</b></th><td>'.$o_COLETA_IMG_ATIV_FILENAME_6_TXT.'</td></tr>';
				}
				
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_RESPONSAVEL_CAMPO.':</b></th><td>'.$o_RESP_CAMPO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_RESPONSAVEL_TECNICO.':</b></th><td>'.$o_RESP_TECNICO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_RESPONSAVEL_CAMPO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_RESPONSAVEL_TECNICO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RT.'</td></tr>';
				if($o_LOGO_FILENAME){ $o_LOGO_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_LOGO_FILENAME.'" target="_blank">'.$o_LOGO_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_CALOR_IMAGEM_LOGOMARCA.':</b></th><td>'.$o_LOGO_FILENAME_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
