<?php
#################################################################################
## SEGVIDA
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: newpwd.php
## Função..........: Gera uma nova senha de acesso
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_TOKEN = "";
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG        = test_input($_GET["l"]);
		$IN_TOKEN       = test_input($_GET["t"]);
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_TOKEN) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.TXT_ERRO_DADOS_OBR.'</div>';
			show_msg($MY_MSG);
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## GERA TOKEN
			$TOKEN_TMP = strtoupper(gera_token());
			
			## GERA TOKEN
			#$TOKEN_TMP = strtoupper(gera_token());
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT F_ConfirmaRecuperaSenha_UPDATE(?,?) as ret"
			)) 
			{
				$sql_LANG     = $mysqli->escape_string($IN_LANG);
				$sql_TOKEN    = $mysqli->escape_string($IN_TOKEN);
				//
				$stmt->bind_param('ss', $sql_LANG,$sql_TOKEN);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_RESULT);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.TXT_ERRO_SOLICITACAO.'</div>';
					show_msg($MY_MSG);
					exit;
				}
				else
				{
					//status|msg|user_account_id|cliente_gescond_id|plano_dias_gratis|plano_tipo|plano_tit|plano_valor|
					list($STATUS, $MSG, $REG_USERNAME, $REG_EMAIL) = explode("|", $o_RESULT);
					
					if($STATUS == 0)
					{
						$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.$MSG.'</div>';
						show_msg($MY_MSG);
						exit;
					}
					else
					{
						show_reset_password_page($IN_LANG, $IN_TOKEN, $REG_USERNAME, $REG_EMAIL);
						exit;
					}
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
		
	}
	#Se nao for enviado via POST, nao mostra nada, apenas uma tela em branco.
	else
	{
		die("");
		exit;
	}
	
	
	
	function show_msg($_MSG)
	{
		$WEB_ADDRESS          = '//'.WEB_ADDRESS;
		$TXT_META_LANGUAGE    = TXT_META_LANGUAGE;
		$TXT_PAGE_TITLE       = TXT_PAGE_TITLE;
		$TXT_META_DESCRIPTION = TXT_META_DESCRIPTION;
		$TXT_META_AUTHOR      = TXT_META_AUTHOR;
		$TXT_META_KEYWORDS    = TXT_META_KEYWORDS;
		$TXT_META_SUBJECT     = TXT_META_SUBJECT;
		$TXT_META_COPYRIGHT   = TXT_META_COPYRIGHT;
		$TXT_META_ABSTRACT    = TXT_META_ABSTRACT;
		$TXT_TERMOS_DE_USO    = TXT_TERMOS_DE_USO;
		$TXT_POLITICA_DE_PRIVACIDADE = TXT_POLITICA_DE_PRIVACIDADE;
		
		$RET_MSG = <<<EOT
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="$TXT_META_LANGUAGE" />
		<meta name="description" content="$TXT_META_DESCRIPTION" />
		<meta name="author"      content="$TXT_META_AUTHOR">
		<meta name="keywords"    content="$TXT_META_KEYWORDS" />
		<meta name="subject"     content="$TXT_META_SUBJECT" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="$TXT_META_COPYRIGHT" />
		<meta name="abstract"    content="$TXT_META_ABSTRACT" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="$WEB_ADDRESS/img/favicon1.png" type="image/x-icon">
		
		<title>$TXT_PAGE_TITLE</title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="$WEB_ADDRESS/css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="$WEB_ADDRESS/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<!-- <link rel="stylesheet" href="$WEB_ADDRESS/css/offcanvas.css"> -->
		<link rel="stylesheet" href="$WEB_ADDRESS/fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="$WEB_ADDRESS/css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="$WEB_ADDRESS/css/login_form.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="$WEB_ADDRESS/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			.alert {
				width:40%;
			}
			
		</style>
		
	</head>
	<body>
		<div class="container center-block">
			<img src="$WEB_ADDRESS/img/logo_326x240.png" border=0 class="img-responsive center-block"><br/>
			
			$_MSG
			
			<br/>
		</div><!--/.container-->
		
		<div id='footer_panel' class="text-center">
			<a href="$WEB_ADDRESS/termos-de-uso.php" target="_blank" role="button">$TXT_TERMOS_DE_USO</a> | <a href="$WEB_ADDRESS/politica-de-privacidade.php" target="_blank" role="button">$TXT_POLITICA_DE_PRIVACIDADE</a>
		</div>
		
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="$WEB_ADDRESS/js/jquery.min.js"></script>
		<script src='$WEB_ADDRESS/js/jquery.storage.js'></script>
		<script src="$WEB_ADDRESS/js/bootstrap.min.js"></script>
		<!-- <script src="$WEB_ADDRESS/js/bootstrap-select.min.js"></script> -->
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="$WEB_ADDRESS/js/ie10-viewport-bug-workaround.js"></script>
		
	</body>
</html>
EOT;
		
		die($RET_MSG);
		exit;
	}
	
	
	
	function show_reset_password_page($_LANG, $_TOKEN, $_USERNAME, $_EMAIL)
	{
		$LB_SENHA_REDEFINIDA_COM_SUCESSO = LB_RESETPWD_SENHA_REDEFINIDA_COM_SUCESSO;
		$LB_INSTRUCAO_INICIAL            = LB_RESETPWD_INSTRUCAO_INICIAL;
		$LB_USERNAME                     = LB_RESETPWD_USERNAME;
		$LB_EMAIL                        = LB_RESETPWD_EMAIL;
		$LB_SENHA                        = LB_RESETPWD_SENHA;
		$LB_SENHA_CONFIRMACAO            = LB_RESETPWD_SENHA_CONFIRMACAO;
		$LB_CAPTCHA                      = LB_RESETPWD_CAPTCHA;
		$LB_BTN_AVANCAR                  = LB_RESETPWD_BTN_AVANCAR;
		$TXT_MODAL_BTN_FECHAR            = TXT_MODAL_BTN_FECHAR;
		$TXT_MODAL_MSG_CARREGANDO        = TXT_MODAL_MSG_CARREGANDO;
		
		$TXT_META_LANGUAGE               = TXT_META_LANGUAGE;
		$TXT_PAGE_TITLE                  = TXT_PAGE_TITLE;
		$TXT_META_DESCRIPTION            = TXT_META_DESCRIPTION;
		$TXT_META_AUTHOR                 = TXT_META_AUTHOR;
		$TXT_META_KEYWORDS               = TXT_META_KEYWORDS;
		$TXT_META_SUBJECT                = TXT_META_SUBJECT;
		$TXT_META_COPYRIGHT              = TXT_META_COPYRIGHT;
		$TXT_META_ABSTRACT               = TXT_META_ABSTRACT;
		$TXT_TERMOS_DE_USO               = TXT_TERMOS_DE_USO;
		$TXT_POLITICA_DE_PRIVACIDADE     = TXT_POLITICA_DE_PRIVACIDADE;
		$WEB_ADDRESS                     = '//'.WEB_ADDRESS;
		
		$RET_MSG = <<<EOT
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="$TXT_META_LANGUAGE" />
		<meta name="description" content="$TXT_META_DESCRIPTION" />
		<meta name="author"      content="$TXT_META_AUTHOR">
		<meta name="keywords"    content="$TXT_META_KEYWORDS" />
		<meta name="subject"     content="$TXT_META_SUBJECT" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="$TXT_META_COPYRIGHT" />
		<meta name="abstract"    content="$TXT_META_ABSTRACT" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="$WEB_ADDRESS/img/favicon1.png" type="image/x-icon">
		
		<title>$TXT_PAGE_TITLE</title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="$WEB_ADDRES/css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="$WEB_ADDRES/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="$WEB_ADDRES/fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="$WEB_ADDRES/css/resetpwd_form.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="$WEB_ADDRES/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			.alert {
				width:40%;
			}
			
		</style>
		
	</head>
	<body>
		<div class="container center-block">
			<br/><img src="$WEB_ADDRESS/img/logo_326x240.png" border=0 class="img-responsive center-block"><br/>
			
			<!-- Begin # DIV Form -->
			<div id="msg_reset_ok" class="center-block" style="display:none;">
				<div class="text-center center-block">
					<font color="#008000"><span class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></span></font>
				</div><br/>
				<div class="alert alert-success text-center center-block" role="alert">$LB_SENHA_REDEFINIDA_COM_SUCESSO</div>
			</div>
			<!-- End # DIV Form -->
			
			<!-- Begin # DIV Form -->
			<div id="div-forms" class="center-block">
				<div class="text-center center-block">
					<p><b>$LB_INSTRUCAO_INICIAL</b></p>
				</div>
				
				<!-- Begin | Lost Password Form -->
				<form id="reset-form">
					<div class="modal-body">
						<div>
							<table border=0 style="border-spacing: 4px;">
								<tr>
									<td align=right width=100 style="padding: 4px;">
										<b>$LB_USERNAME</b>
									</td>
									<td align=left width=200 style="padding: 4px;">
										&nbsp;<font color=blue>$_USERNAME</font>
										<input id="reset_username" type=hidden value="$_USERNAME" >
										<input id="reset_email" type=hidden value="$_EMAIL" >
										<input id="reset_token" type=hidden value="$_TOKEN" >
										<input id="lang" type=hidden value="$_LANG" >
									</td>
								</tr>
								<tr>
									<td align=right width=100 style="padding: 4px;">
										<b>$LB_EMAIL</b>
									</td>
									<td align=left width=200 style="padding: 4px;">
										&nbsp;<font color=blue>$_EMAIL</font>
									</td>
								</tr>
							</table>
						</div>
						<input id="reset_password"   type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="$LB_SENHA" required>
						<input id="reset_password2"  type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="$LB_SENHA_CONFIRMACAO" required>
						
						<div class="input-group">
							<span class="input-group-addon">
								<img id='reset_captcha_img' src="$WEB_ADDRESS/captcha/captcha_loading.png" width=70 height=22>
								<a id="reset_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
							</span>
							<input id="reset_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="$LB_CAPTCHA" required>
						</div>
					</div>
					<div class="modal-footer">
						<div>
							<button id="bt_reset" type="button" class="btn btn-yellow btn-lg btn-block">$LB_BTN_AVANCAR</button>
						</div>
					</div>
				</form>
				<!-- End | Lost Password Form -->
			
			</div>
			<!-- End # DIV Form -->
			
		</div><!--/.container-->
		
		<div id='footer_panel' class="text-center">
			<a href="$WEB_ADDRESS/termos-de-uso.php" target="_blank" role="button">$TXT_TERMOS_DE_USO</a> | <a href="$WEB_ADDRESS/politica-de-privacidade.php" target="_blank" role="button">$TXT_POLITICA_DE_PRIVACIDADE</a>
		</div>
		
		
<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal">$TXT_MODAL_BTN_FECHAR</button>
				<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" data-backdrop="static" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<!--<div class="modal-header">
			</div>-->
			<div class="modal-body">
				<p class="text-center center-block"><img src="$WEB_ADDRESS/img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color=#ffffff><b>$TXT_MODAL_MSG_CARREGANDO</b></font></p>
			</div>
			<!--<div class="modal-footer" id="msgBoxFooter">
			</div>-->
		</div>
	</div>
</div>
<!-- /msgBox -->
		
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="$WEB_ADDRESS/js/jquery.min.js"></script>
		<script src='$WEB_ADDRESS/js/jquery.storage.js'></script>
		<script src="$WEB_ADDRESS/js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="$WEB_ADDRESS/js/ie10-viewport-bug-workaround.js"></script>
		<script src="$WEB_ADDRESS/js/sha512.js"></script>
		<script src="$WEB_ADDRESS/js/resetpwd_form.js"></script>
	</body>
</html>
EOT;
		
		die($RET_MSG);
		exit;
	}
	
#################################################################################
###########
#####
##
?>
