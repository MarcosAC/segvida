<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: contrata_plano.php
##  Funcao: CONTROLLER - Efetua a contratacao de plano pago e gera token para cobranca
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 02/09/2017 10:06:22
#################################################################################

	//http://mudas.prodfy.com.br/cgi-bin/contrata_plano.php?l=br&p=PR1-M1
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_PLANO = "";
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["l"]);
		$IN_PLANO      = test_input($_GET["p"]);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_PLANO      = test_input($_POST["p"]);
		
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !isEmpty($IN_PLANO)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert||||");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##inicia sessao
			$init_sid = sec_session_start();
			$o_idSYSTEM_CLIENTE = $_SESSION['user_idsystem_cliente'];
			
			if(isEmpty($o_idSYSTEM_CLIENTE))
			{
				die("2|".TXT_USUARIO_NAO_LOGADO." ".TXT_FAVOR_EFETUAR_NOVO_LOGIN."|alert||||");
				exit;
			}
			
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error||||"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## Verifica codigo do plano informado
			
			## Escapes
			$sql_PLANO = $mysqli->escape_string($IN_PLANO);
			$sql_idSYSTEM_CLIENTE = $mysqli->escape_string($o_idSYSTEM_CLIENTE);
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT `codigo` AS codigo, `tipo` AS tipo, 
              CASE WHEN '".$IN_LANG."' = 'pt-br' THEN `titulo_br`
                   WHEN '".$IN_LANG."' = 'en-us' THEN `titulo_us`
                   WHEN '".$IN_LANG."' = 'es-es' THEN `titulo_sp`
               END AS titulo,
              FORMAT(`valor_cobranca`,2,'de_DE') AS valor_cobranca
         FROM `SYSTEM_PLANO_PAGTO` 
        WHERE `codigo` = ?"
			))
			{
				$stmt->bind_param('s', $sql_PLANO);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_PLANO_CODIGO, $o_PLANO_TIPO, $o_PLANO_TITULO, $o_PLANO_VALOR_COBRANCA);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|".TXT_PLANO_INEXISTENTE."|alert||||");
					exit; 
				}
				
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
				exit;
			}
			
			
			
			## GERA TOKEN
			$TOKEN_TMP = strtoupper(gera_token());
			
			$sql_PLANO = $mysqli->escape_String($IN_PLANO);
			
			##Prepara query
			$QUERY = "UPDATE `SYSTEM_CLIENTE` 
			    SET `pagto_token` = '".$TOKEN_TMP."',
			        `pagto_plano_codigo` = '".$sql_PLANO."'
			  WHERE `idSYSTEM_CLIENTE` = '".$sql_idSYSTEM_CLIENTE."' ";
			
			if ($stmt = $mysqli->prepare($QUERY)) 
			{
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				//$stmt->bind_result($o_RESULT);
				//$stmt->fetch();
				
				if($o_PLANO_TIPO == "M"){ $TIPO_TXT = TXT_MENSAL; }
				if($o_PLANO_TIPO == "A"){ $TIPO_TXT = TXT_ANUAL; }
				
				$plano_contratado = $o_PLANO_TITULO." - ".$TIPO_TXT." - R$ ".$o_PLANO_VALOR_COBRANCA;
				
				//status|mensagem|msgboxtype|plano_codigo|plano_titulo|pagto_token
				die("1|".TXT_MSG_CONTRATA_PLANO_SELECIONADO."|success|".$IN_PLANO."|".$plano_contratado."|".$TOKEN_TMP."|");
				exit;
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error||||");
	}
	
#################################################################################
###########
#####
##
?>
