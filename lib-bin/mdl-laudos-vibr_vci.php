<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-vibr_vci.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 10/9/2017 17:30:41
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_IDCLIENTE = $IN_ANO = $IN_MES = $IN_PLANILHA_NUM = $IN_UNIDADE_SITE = $IN_DATA_ELABORACAO = $IN_AREA = $IN_SETOR = $IN_GES = $IN_CARGO_FUNCAO = $IN_CBO = $IN_ATIV_MACRO = $IN_ANALISES = $IN_ANALISE_AMOSTRA_1 = $IN_ANALISE_DATA_AMOSTRAGEM_1 = $IN_ANALISE_PARADIGMA_1 = $IN_ANALISE_TAREFA_EXEC_1 = $IN_ANALISE_AMOSTRA_2 = $IN_ANALISE_DATA_AMOSTRAGEM_2 = $IN_ANALISE_PARADIGMA_2 = $IN_ANALISE_TAREFA_EXEC_2 = $IN_ANALISE_AMOSTRA_3 = $IN_ANALISE_DATA_AMOSTRAGEM_3 = $IN_ANALISE_PARADIGMA_3 = $IN_ANALISE_TAREFA_EXEC_3 = $IN_ANALISE_AMOSTRA_4 = $IN_ANALISE_DATA_AMOSTRAGEM_4 = $IN_ANALISE_PARADIGMA_4 = $IN_ANALISE_TAREFA_EXEC_4 = $IN_ANALISE_AMOSTRA_5 = $IN_ANALISE_DATA_AMOSTRAGEM_5 = $IN_ANALISE_PARADIGMA_5 = $IN_ANALISE_TAREFA_EXEC_5 = $IN_ANALISE_AMOSTRA_6 = $IN_ANALISE_DATA_AMOSTRAGEM_6 = $IN_ANALISE_PARADIGMA_6 = $IN_ANALISE_TAREFA_EXEC_6 = $IN_AGENTE_RISCO = $IN_JOR_TRAB = $IN_TEMPO_EXPO = $IN_TIPO_EXPO = $IN_MEIO_PROPAG = $IN_FONTE_GERADORA = $IN_EPC = $IN_FABR_EPI = $IN_MOD_EPI = $IN_CERT_APROV = $IN_GRAU_ATENUACAO = $IN_ACEL_1 = $IN_ACEL_MARCA_1 = $IN_ACEL_MODELO_1 = $IN_ACEL_SERIAL_1 = $IN_ACEL_CERT_CALIB_1 = $IN_ACEL_2 = $IN_ACEL_MARCA_2 = $IN_ACEL_MODELO_2 = $IN_ACEL_SERIAL_2 = $IN_ACEL_CERT_CALIB_2 = $IN_COLETAS = $IN_COLETA_AMOSTRA_1 = $IN_COLETA_DATA_1 = $IN_COLETA_DURACAO_1 = $IN_COLETA_ACELERACAO_X_1 = $IN_COLETA_ACELERACAO_Y_1 = $IN_COLETA_ACELERACAO_Z_1 = $IN_COLETA_VDV_X_1 = $IN_COLETA_VDV_Y_1 = $IN_COLETA_VDV_Z_1 = $IN_COLETA_FATOR_CRISTA_1 = $IN_COLETA_AMOSTRA_2 = $IN_COLETA_DATA_2 = $IN_COLETA_DURACAO_2 = $IN_COLETA_ACELERACAO_X_2 = $IN_COLETA_ACELERACAO_Y_2 = $IN_COLETA_ACELERACAO_Z_2 = $IN_COLETA_VDV_X_2 = $IN_COLETA_VDV_Y_2 = $IN_COLETA_VDV_Z_2 = $IN_COLETA_FATOR_CRISTA_2 = $IN_COLETA_AMOSTRA_3 = $IN_COLETA_DATA_3 = $IN_COLETA_DURACAO_3 = $IN_COLETA_ACELERACAO_X_3 = $IN_COLETA_ACELERACAO_Y_3 = $IN_COLETA_ACELERACAO_Z_3 = $IN_COLETA_VDV_X_3 = $IN_COLETA_VDV_Y_3 = $IN_COLETA_VDV_Z_3 = $IN_COLETA_FATOR_CRISTA_3 = $IN_COLETA_AMOSTRA_4 = $IN_COLETA_DATA_4 = $IN_COLETA_DURACAO_4 = $IN_COLETA_ACELERACAO_X_4 = $IN_COLETA_ACELERACAO_Y_4 = $IN_COLETA_ACELERACAO_Z_4 = $IN_COLETA_VDV_X_4 = $IN_COLETA_VDV_Y_4 = $IN_COLETA_VDV_Z_4 = $IN_COLETA_FATOR_CRISTA_4 = $IN_COLETA_AMOSTRA_5 = $IN_COLETA_DATA_5 = $IN_COLETA_DURACAO_5 = $IN_COLETA_ACELERACAO_X_5 = $IN_COLETA_ACELERACAO_Y_5 = $IN_COLETA_ACELERACAO_Z_5 = $IN_COLETA_VDV_X_5 = $IN_COLETA_VDV_Y_5 = $IN_COLETA_VDV_Z_5 = $IN_COLETA_FATOR_CRISTA_5 = $IN_COLETA_AMOSTRA_6 = $IN_COLETA_DATA_6 = $IN_COLETA_DURACAO_6 = $IN_COLETA_ACELERACAO_X_6 = $IN_COLETA_ACELERACAO_Y_6 = $IN_COLETA_ACELERACAO_Z_6 = $IN_COLETA_VDV_X_6 = $IN_COLETA_VDV_Y_6 = $IN_COLETA_VDV_Z_6 = $IN_COLETA_FATOR_CRISTA_6 = $IN_TEMPO_MEDICAO = $IN_NR09_AREN = $IN_NR09_VDVR = $IN_NR15_AREN = $IN_NR15_VDVR = $IN_RESP_CAMPO_IDCOLABORADOR = $IN_RESP_TECNICO_IDCOLABORADOR = $IN_REGISTRO_RC = $IN_REGISTRO_RT = "";
	
	####
	# Set Debug Mode
	####
	#$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_IDCLIENTE = test_input($_GET["idcliente"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_PLANILHA_NUM = test_input($_GET["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_GET["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_GET["data_elaboracao"]);
		$IN_AREA = test_input($_GET["area"]);
		$IN_SETOR = test_input($_GET["setor"]);
		$IN_GES = test_input($_GET["ges"]);
		$IN_CARGO_FUNCAO = test_input($_GET["cargo_funcao"]);
		$IN_CBO = test_input($_GET["cbo"]);
		$IN_ATIV_MACRO = test_input($_GET["ativ_macro"]);
		$IN_ANALISES = test_input($_GET["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_GET["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_GET["analise_data_amostragem_1"]);
		$IN_ANALISE_PARADIGMA_1 = test_input($_GET["analise_paradigma_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_GET["analise_tarefa_exec_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_GET["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_GET["analise_data_amostragem_2"]);
		$IN_ANALISE_PARADIGMA_2 = test_input($_GET["analise_paradigma_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_GET["analise_tarefa_exec_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_GET["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_GET["analise_data_amostragem_3"]);
		$IN_ANALISE_PARADIGMA_3 = test_input($_GET["analise_paradigma_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_GET["analise_tarefa_exec_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_GET["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_GET["analise_data_amostragem_4"]);
		$IN_ANALISE_PARADIGMA_4 = test_input($_GET["analise_paradigma_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_GET["analise_tarefa_exec_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_GET["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_GET["analise_data_amostragem_5"]);
		$IN_ANALISE_PARADIGMA_5 = test_input($_GET["analise_paradigma_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_GET["analise_tarefa_exec_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_GET["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_GET["analise_data_amostragem_6"]);
		$IN_ANALISE_PARADIGMA_6 = test_input($_GET["analise_paradigma_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_GET["analise_tarefa_exec_6"]);
		$IN_AGENTE_RISCO = test_input($_GET["agente_risco"]);
		$IN_JOR_TRAB = test_input($_GET["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_GET["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_GET["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_GET["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_GET["fonte_geradora"]);
		$IN_EPC = test_input($_GET["epc"]);
		$IN_FABR_EPI = test_input($_GET["fabr_epi"]);
		$IN_MOD_EPI = test_input($_GET["mod_epi"]);
		$IN_CERT_APROV = test_input($_GET["cert_aprov"]);
		$IN_GRAU_ATENUACAO = test_input($_GET["grau_atenuacao"]);
		$IN_ACEL_1 = test_input($_GET["acel_1"]);
		$IN_ACEL_MARCA_1 = test_input($_GET["acel_marca_1"]);
		$IN_ACEL_MODELO_1 = test_input($_GET["acel_modelo_1"]);
		$IN_ACEL_SERIAL_1 = test_input($_GET["acel_serial_1"]);
		$IN_ACEL_CERT_CALIB_1 = test_input($_GET["acel_cert_calib_1"]);
		$IN_ACEL_2 = test_input($_GET["acel_2"]);
		$IN_ACEL_MARCA_2 = test_input($_GET["acel_marca_2"]);
		$IN_ACEL_MODELO_2 = test_input($_GET["acel_modelo_2"]);
		$IN_ACEL_SERIAL_2 = test_input($_GET["acel_serial_2"]);
		$IN_ACEL_CERT_CALIB_2 = test_input($_GET["acel_cert_calib_2"]);
		$IN_COLETAS = test_input($_GET["coletas"]);
		$IN_COLETA_AMOSTRA_1 = test_input($_GET["coleta_amostra_1"]);
		$IN_COLETA_DATA_1 = test_input($_GET["coleta_data_1"]);
		$IN_COLETA_DURACAO_1 = test_input($_GET["coleta_duracao_1"]);
		$IN_COLETA_ACELERACAO_X_1 = test_input($_GET["coleta_aceleracao_x_1"]);
		$IN_COLETA_ACELERACAO_Y_1 = test_input($_GET["coleta_aceleracao_y_1"]);
		$IN_COLETA_ACELERACAO_Z_1 = test_input($_GET["coleta_aceleracao_z_1"]);
		$IN_COLETA_VDV_X_1 = test_input($_GET["coleta_vdv_x_1"]);
		$IN_COLETA_VDV_Y_1 = test_input($_GET["coleta_vdv_y_1"]);
		$IN_COLETA_VDV_Z_1 = test_input($_GET["coleta_vdv_z_1"]);
		$IN_COLETA_FATOR_CRISTA_1 = test_input($_GET["coleta_fator_crista_1"]);
		$IN_COLETA_AMOSTRA_2 = test_input($_GET["coleta_amostra_2"]);
		$IN_COLETA_DATA_2 = test_input($_GET["coleta_data_2"]);
		$IN_COLETA_DURACAO_2 = test_input($_GET["coleta_duracao_2"]);
		$IN_COLETA_ACELERACAO_X_2 = test_input($_GET["coleta_aceleracao_x_2"]);
		$IN_COLETA_ACELERACAO_Y_2 = test_input($_GET["coleta_aceleracao_y_2"]);
		$IN_COLETA_ACELERACAO_Z_2 = test_input($_GET["coleta_aceleracao_z_2"]);
		$IN_COLETA_VDV_X_2 = test_input($_GET["coleta_vdv_x_2"]);
		$IN_COLETA_VDV_Y_2 = test_input($_GET["coleta_vdv_y_2"]);
		$IN_COLETA_VDV_Z_2 = test_input($_GET["coleta_vdv_z_2"]);
		$IN_COLETA_FATOR_CRISTA_2 = test_input($_GET["coleta_fator_crista_2"]);
		$IN_COLETA_AMOSTRA_3 = test_input($_GET["coleta_amostra_3"]);
		$IN_COLETA_DATA_3 = test_input($_GET["coleta_data_3"]);
		$IN_COLETA_DURACAO_3 = test_input($_GET["coleta_duracao_3"]);
		$IN_COLETA_ACELERACAO_X_3 = test_input($_GET["coleta_aceleracao_x_3"]);
		$IN_COLETA_ACELERACAO_Y_3 = test_input($_GET["coleta_aceleracao_y_3"]);
		$IN_COLETA_ACELERACAO_Z_3 = test_input($_GET["coleta_aceleracao_z_3"]);
		$IN_COLETA_VDV_X_3 = test_input($_GET["coleta_vdv_x_3"]);
		$IN_COLETA_VDV_Y_3 = test_input($_GET["coleta_vdv_y_3"]);
		$IN_COLETA_VDV_Z_3 = test_input($_GET["coleta_vdv_z_3"]);
		$IN_COLETA_FATOR_CRISTA_3 = test_input($_GET["coleta_fator_crista_3"]);
		$IN_COLETA_AMOSTRA_4 = test_input($_GET["coleta_amostra_4"]);
		$IN_COLETA_DATA_4 = test_input($_GET["coleta_data_4"]);
		$IN_COLETA_DURACAO_4 = test_input($_GET["coleta_duracao_4"]);
		$IN_COLETA_ACELERACAO_X_4 = test_input($_GET["coleta_aceleracao_x_4"]);
		$IN_COLETA_ACELERACAO_Y_4 = test_input($_GET["coleta_aceleracao_y_4"]);
		$IN_COLETA_ACELERACAO_Z_4 = test_input($_GET["coleta_aceleracao_z_4"]);
		$IN_COLETA_VDV_X_4 = test_input($_GET["coleta_vdv_x_4"]);
		$IN_COLETA_VDV_Y_4 = test_input($_GET["coleta_vdv_y_4"]);
		$IN_COLETA_VDV_Z_4 = test_input($_GET["coleta_vdv_z_4"]);
		$IN_COLETA_FATOR_CRISTA_4 = test_input($_GET["coleta_fator_crista_4"]);
		$IN_COLETA_AMOSTRA_5 = test_input($_GET["coleta_amostra_5"]);
		$IN_COLETA_DATA_5 = test_input($_GET["coleta_data_5"]);
		$IN_COLETA_DURACAO_5 = test_input($_GET["coleta_duracao_5"]);
		$IN_COLETA_ACELERACAO_X_5 = test_input($_GET["coleta_aceleracao_x_5"]);
		$IN_COLETA_ACELERACAO_Y_5 = test_input($_GET["coleta_aceleracao_y_5"]);
		$IN_COLETA_ACELERACAO_Z_5 = test_input($_GET["coleta_aceleracao_z_5"]);
		$IN_COLETA_VDV_X_5 = test_input($_GET["coleta_vdv_x_5"]);
		$IN_COLETA_VDV_Y_5 = test_input($_GET["coleta_vdv_y_5"]);
		$IN_COLETA_VDV_Z_5 = test_input($_GET["coleta_vdv_z_5"]);
		$IN_COLETA_FATOR_CRISTA_5 = test_input($_GET["coleta_fator_crista_5"]);
		$IN_COLETA_AMOSTRA_6 = test_input($_GET["coleta_amostra_6"]);
		$IN_COLETA_DATA_6 = test_input($_GET["coleta_data_6"]);
		$IN_COLETA_DURACAO_6 = test_input($_GET["coleta_duracao_6"]);
		$IN_COLETA_ACELERACAO_X_6 = test_input($_GET["coleta_aceleracao_x_6"]);
		$IN_COLETA_ACELERACAO_Y_6 = test_input($_GET["coleta_aceleracao_y_6"]);
		$IN_COLETA_ACELERACAO_Z_6 = test_input($_GET["coleta_aceleracao_z_6"]);
		$IN_COLETA_VDV_X_6 = test_input($_GET["coleta_vdv_x_6"]);
		$IN_COLETA_VDV_Y_6 = test_input($_GET["coleta_vdv_y_6"]);
		$IN_COLETA_VDV_Z_6 = test_input($_GET["coleta_vdv_z_6"]);
		$IN_COLETA_FATOR_CRISTA_6 = test_input($_GET["coleta_fator_crista_6"]);
		$IN_TEMPO_MEDICAO = test_input($_GET["tempo_medicao"]);
		$IN_NR09_AREN = test_input($_GET["nr09_aren"]);
		$IN_NR09_VDVR = test_input($_GET["nr09_vdvr"]);
		$IN_NR15_AREN = test_input($_GET["nr15_aren"]);
		$IN_NR15_VDVR = test_input($_GET["nr15_vdvr"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_GET["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_GET["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_GET["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_GET["registro_rt"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_IDCLIENTE = test_input($_POST["idcliente"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_PLANILHA_NUM = test_input($_POST["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_POST["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_POST["data_elaboracao"]);
		$IN_AREA = test_input($_POST["area"]);
		$IN_SETOR = test_input($_POST["setor"]);
		$IN_GES = test_input($_POST["ges"]);
		$IN_CARGO_FUNCAO = test_input($_POST["cargo_funcao"]);
		$IN_CBO = test_input($_POST["cbo"]);
		$IN_ATIV_MACRO = test_input($_POST["ativ_macro"]);
		$IN_ANALISES = test_input($_POST["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_POST["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_POST["analise_data_amostragem_1"]);
		$IN_ANALISE_PARADIGMA_1 = test_input($_POST["analise_paradigma_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_POST["analise_tarefa_exec_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_POST["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_POST["analise_data_amostragem_2"]);
		$IN_ANALISE_PARADIGMA_2 = test_input($_POST["analise_paradigma_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_POST["analise_tarefa_exec_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_POST["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_POST["analise_data_amostragem_3"]);
		$IN_ANALISE_PARADIGMA_3 = test_input($_POST["analise_paradigma_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_POST["analise_tarefa_exec_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_POST["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_POST["analise_data_amostragem_4"]);
		$IN_ANALISE_PARADIGMA_4 = test_input($_POST["analise_paradigma_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_POST["analise_tarefa_exec_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_POST["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_POST["analise_data_amostragem_5"]);
		$IN_ANALISE_PARADIGMA_5 = test_input($_POST["analise_paradigma_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_POST["analise_tarefa_exec_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_POST["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_POST["analise_data_amostragem_6"]);
		$IN_ANALISE_PARADIGMA_6 = test_input($_POST["analise_paradigma_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_POST["analise_tarefa_exec_6"]);
		$IN_AGENTE_RISCO = test_input($_POST["agente_risco"]);
		$IN_JOR_TRAB = test_input($_POST["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_POST["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_POST["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_POST["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_POST["fonte_geradora"]);
		$IN_EPC = test_input($_POST["epc"]);
		$IN_FABR_EPI = test_input($_POST["fabr_epi"]);
		$IN_MOD_EPI = test_input($_POST["mod_epi"]);
		$IN_CERT_APROV = test_input($_POST["cert_aprov"]);
		$IN_GRAU_ATENUACAO = test_input($_POST["grau_atenuacao"]);
		$IN_ACEL_1 = test_input($_POST["acel_1"]);
		$IN_ACEL_MARCA_1 = test_input($_POST["acel_marca_1"]);
		$IN_ACEL_MODELO_1 = test_input($_POST["acel_modelo_1"]);
		$IN_ACEL_SERIAL_1 = test_input($_POST["acel_serial_1"]);
		$IN_ACEL_CERT_CALIB_1 = test_input($_POST["acel_cert_calib_1"]);
		$IN_ACEL_2 = test_input($_POST["acel_2"]);
		$IN_ACEL_MARCA_2 = test_input($_POST["acel_marca_2"]);
		$IN_ACEL_MODELO_2 = test_input($_POST["acel_modelo_2"]);
		$IN_ACEL_SERIAL_2 = test_input($_POST["acel_serial_2"]);
		$IN_ACEL_CERT_CALIB_2 = test_input($_POST["acel_cert_calib_2"]);
		$IN_COLETAS = test_input($_POST["coletas"]);
		$IN_COLETA_AMOSTRA_1 = test_input($_POST["coleta_amostra_1"]);
		$IN_COLETA_DATA_1 = test_input($_POST["coleta_data_1"]);
		$IN_COLETA_DURACAO_1 = test_input($_POST["coleta_duracao_1"]);
		$IN_COLETA_ACELERACAO_X_1 = test_input($_POST["coleta_aceleracao_x_1"]);
		$IN_COLETA_ACELERACAO_Y_1 = test_input($_POST["coleta_aceleracao_y_1"]);
		$IN_COLETA_ACELERACAO_Z_1 = test_input($_POST["coleta_aceleracao_z_1"]);
		$IN_COLETA_VDV_X_1 = test_input($_POST["coleta_vdv_x_1"]);
		$IN_COLETA_VDV_Y_1 = test_input($_POST["coleta_vdv_y_1"]);
		$IN_COLETA_VDV_Z_1 = test_input($_POST["coleta_vdv_z_1"]);
		$IN_COLETA_FATOR_CRISTA_1 = test_input($_POST["coleta_fator_crista_1"]);
		$IN_COLETA_AMOSTRA_2 = test_input($_POST["coleta_amostra_2"]);
		$IN_COLETA_DATA_2 = test_input($_POST["coleta_data_2"]);
		$IN_COLETA_DURACAO_2 = test_input($_POST["coleta_duracao_2"]);
		$IN_COLETA_ACELERACAO_X_2 = test_input($_POST["coleta_aceleracao_x_2"]);
		$IN_COLETA_ACELERACAO_Y_2 = test_input($_POST["coleta_aceleracao_y_2"]);
		$IN_COLETA_ACELERACAO_Z_2 = test_input($_POST["coleta_aceleracao_z_2"]);
		$IN_COLETA_VDV_X_2 = test_input($_POST["coleta_vdv_x_2"]);
		$IN_COLETA_VDV_Y_2 = test_input($_POST["coleta_vdv_y_2"]);
		$IN_COLETA_VDV_Z_2 = test_input($_POST["coleta_vdv_z_2"]);
		$IN_COLETA_FATOR_CRISTA_2 = test_input($_POST["coleta_fator_crista_2"]);
		$IN_COLETA_AMOSTRA_3 = test_input($_POST["coleta_amostra_3"]);
		$IN_COLETA_DATA_3 = test_input($_POST["coleta_data_3"]);
		$IN_COLETA_DURACAO_3 = test_input($_POST["coleta_duracao_3"]);
		$IN_COLETA_ACELERACAO_X_3 = test_input($_POST["coleta_aceleracao_x_3"]);
		$IN_COLETA_ACELERACAO_Y_3 = test_input($_POST["coleta_aceleracao_y_3"]);
		$IN_COLETA_ACELERACAO_Z_3 = test_input($_POST["coleta_aceleracao_z_3"]);
		$IN_COLETA_VDV_X_3 = test_input($_POST["coleta_vdv_x_3"]);
		$IN_COLETA_VDV_Y_3 = test_input($_POST["coleta_vdv_y_3"]);
		$IN_COLETA_VDV_Z_3 = test_input($_POST["coleta_vdv_z_3"]);
		$IN_COLETA_FATOR_CRISTA_3 = test_input($_POST["coleta_fator_crista_3"]);
		$IN_COLETA_AMOSTRA_4 = test_input($_POST["coleta_amostra_4"]);
		$IN_COLETA_DATA_4 = test_input($_POST["coleta_data_4"]);
		$IN_COLETA_DURACAO_4 = test_input($_POST["coleta_duracao_4"]);
		$IN_COLETA_ACELERACAO_X_4 = test_input($_POST["coleta_aceleracao_x_4"]);
		$IN_COLETA_ACELERACAO_Y_4 = test_input($_POST["coleta_aceleracao_y_4"]);
		$IN_COLETA_ACELERACAO_Z_4 = test_input($_POST["coleta_aceleracao_z_4"]);
		$IN_COLETA_VDV_X_4 = test_input($_POST["coleta_vdv_x_4"]);
		$IN_COLETA_VDV_Y_4 = test_input($_POST["coleta_vdv_y_4"]);
		$IN_COLETA_VDV_Z_4 = test_input($_POST["coleta_vdv_z_4"]);
		$IN_COLETA_FATOR_CRISTA_4 = test_input($_POST["coleta_fator_crista_4"]);
		$IN_COLETA_AMOSTRA_5 = test_input($_POST["coleta_amostra_5"]);
		$IN_COLETA_DATA_5 = test_input($_POST["coleta_data_5"]);
		$IN_COLETA_DURACAO_5 = test_input($_POST["coleta_duracao_5"]);
		$IN_COLETA_ACELERACAO_X_5 = test_input($_POST["coleta_aceleracao_x_5"]);
		$IN_COLETA_ACELERACAO_Y_5 = test_input($_POST["coleta_aceleracao_y_5"]);
		$IN_COLETA_ACELERACAO_Z_5 = test_input($_POST["coleta_aceleracao_z_5"]);
		$IN_COLETA_VDV_X_5 = test_input($_POST["coleta_vdv_x_5"]);
		$IN_COLETA_VDV_Y_5 = test_input($_POST["coleta_vdv_y_5"]);
		$IN_COLETA_VDV_Z_5 = test_input($_POST["coleta_vdv_z_5"]);
		$IN_COLETA_FATOR_CRISTA_5 = test_input($_POST["coleta_fator_crista_5"]);
		$IN_COLETA_AMOSTRA_6 = test_input($_POST["coleta_amostra_6"]);
		$IN_COLETA_DATA_6 = test_input($_POST["coleta_data_6"]);
		$IN_COLETA_DURACAO_6 = test_input($_POST["coleta_duracao_6"]);
		$IN_COLETA_ACELERACAO_X_6 = test_input($_POST["coleta_aceleracao_x_6"]);
		$IN_COLETA_ACELERACAO_Y_6 = test_input($_POST["coleta_aceleracao_y_6"]);
		$IN_COLETA_ACELERACAO_Z_6 = test_input($_POST["coleta_aceleracao_z_6"]);
		$IN_COLETA_VDV_X_6 = test_input($_POST["coleta_vdv_x_6"]);
		$IN_COLETA_VDV_Y_6 = test_input($_POST["coleta_vdv_y_6"]);
		$IN_COLETA_VDV_Z_6 = test_input($_POST["coleta_vdv_z_6"]);
		$IN_COLETA_FATOR_CRISTA_6 = test_input($_POST["coleta_fator_crista_6"]);
		$IN_TEMPO_MEDICAO = test_input($_POST["tempo_medicao"]);
		$IN_NR09_AREN = test_input($_POST["nr09_aren"]);
		$IN_NR09_VDVR = test_input($_POST["nr09_vdvr"]);
		$IN_NR15_AREN = test_input($_POST["nr15_aren"]);
		$IN_NR15_VDVR = test_input($_POST["nr15_vdvr"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_POST["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_POST["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_POST["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_POST["registro_rt"]);
		
		## case convertion
		mb_strtoupper($IN_REGISTRO_RC,"UTF-8");
		mb_strtoupper($IN_REGISTRO_RT,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_PARADIGMA_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_PARADIGMA_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_PARADIGMA_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_PARADIGMA_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_PARADIGMA_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_PARADIGMA_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_AGENTE_RISCO, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_FABR_EPI, $IN_MOD_EPI, $IN_CERT_APROV, $IN_GRAU_ATENUACAO, $IN_ACEL_1, $IN_ACEL_MARCA_1, $IN_ACEL_MODELO_1, $IN_ACEL_SERIAL_1, $IN_ACEL_CERT_CALIB_1, $IN_ACEL_2, $IN_ACEL_MARCA_2, $IN_ACEL_MODELO_2, $IN_ACEL_SERIAL_2, $IN_ACEL_CERT_CALIB_2, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_DURACAO_1, $IN_COLETA_ACELERACAO_X_1, $IN_COLETA_ACELERACAO_Y_1, $IN_COLETA_ACELERACAO_Z_1, $IN_COLETA_VDV_X_1, $IN_COLETA_VDV_Y_1, $IN_COLETA_VDV_Z_1, $IN_COLETA_FATOR_CRISTA_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_DURACAO_2, $IN_COLETA_ACELERACAO_X_2, $IN_COLETA_ACELERACAO_Y_2, $IN_COLETA_ACELERACAO_Z_2, $IN_COLETA_VDV_X_2, $IN_COLETA_VDV_Y_2, $IN_COLETA_VDV_Z_2, $IN_COLETA_FATOR_CRISTA_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_DURACAO_3, $IN_COLETA_ACELERACAO_X_3, $IN_COLETA_ACELERACAO_Y_3, $IN_COLETA_ACELERACAO_Z_3, $IN_COLETA_VDV_X_3, $IN_COLETA_VDV_Y_3, $IN_COLETA_VDV_Z_3, $IN_COLETA_FATOR_CRISTA_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_DURACAO_4, $IN_COLETA_ACELERACAO_X_4, $IN_COLETA_ACELERACAO_Y_4, $IN_COLETA_ACELERACAO_Z_4, $IN_COLETA_VDV_X_4, $IN_COLETA_VDV_Y_4, $IN_COLETA_VDV_Z_4, $IN_COLETA_FATOR_CRISTA_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_DURACAO_5, $IN_COLETA_ACELERACAO_X_5, $IN_COLETA_ACELERACAO_Y_5, $IN_COLETA_ACELERACAO_Z_5, $IN_COLETA_VDV_X_5, $IN_COLETA_VDV_Y_5, $IN_COLETA_VDV_Z_5, $IN_COLETA_FATOR_CRISTA_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_DURACAO_6, $IN_COLETA_ACELERACAO_X_6, $IN_COLETA_ACELERACAO_Y_6, $IN_COLETA_ACELERACAO_Z_6, $IN_COLETA_VDV_X_6, $IN_COLETA_VDV_Y_6, $IN_COLETA_VDV_Z_6, $IN_COLETA_FATOR_CRISTA_6, $IN_TEMPO_MEDICAO, $IN_NR09_AREN, $IN_NR09_VDVR, $IN_NR15_AREN, $IN_NR15_VDVR, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_PARADIGMA_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_PARADIGMA_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_PARADIGMA_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_PARADIGMA_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_PARADIGMA_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_PARADIGMA_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_AGENTE_RISCO, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_FABR_EPI, $IN_MOD_EPI, $IN_CERT_APROV, $IN_GRAU_ATENUACAO, $IN_ACEL_1, $IN_ACEL_MARCA_1, $IN_ACEL_MODELO_1, $IN_ACEL_SERIAL_1, $IN_ACEL_CERT_CALIB_1, $IN_ACEL_2, $IN_ACEL_MARCA_2, $IN_ACEL_MODELO_2, $IN_ACEL_SERIAL_2, $IN_ACEL_CERT_CALIB_2, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_DURACAO_1, $IN_COLETA_ACELERACAO_X_1, $IN_COLETA_ACELERACAO_Y_1, $IN_COLETA_ACELERACAO_Z_1, $IN_COLETA_VDV_X_1, $IN_COLETA_VDV_Y_1, $IN_COLETA_VDV_Z_1, $IN_COLETA_FATOR_CRISTA_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_DURACAO_2, $IN_COLETA_ACELERACAO_X_2, $IN_COLETA_ACELERACAO_Y_2, $IN_COLETA_ACELERACAO_Z_2, $IN_COLETA_VDV_X_2, $IN_COLETA_VDV_Y_2, $IN_COLETA_VDV_Z_2, $IN_COLETA_FATOR_CRISTA_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_DURACAO_3, $IN_COLETA_ACELERACAO_X_3, $IN_COLETA_ACELERACAO_Y_3, $IN_COLETA_ACELERACAO_Z_3, $IN_COLETA_VDV_X_3, $IN_COLETA_VDV_Y_3, $IN_COLETA_VDV_Z_3, $IN_COLETA_FATOR_CRISTA_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_DURACAO_4, $IN_COLETA_ACELERACAO_X_4, $IN_COLETA_ACELERACAO_Y_4, $IN_COLETA_ACELERACAO_Z_4, $IN_COLETA_VDV_X_4, $IN_COLETA_VDV_Y_4, $IN_COLETA_VDV_Z_4, $IN_COLETA_FATOR_CRISTA_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_DURACAO_5, $IN_COLETA_ACELERACAO_X_5, $IN_COLETA_ACELERACAO_Y_5, $IN_COLETA_ACELERACAO_Z_5, $IN_COLETA_VDV_X_5, $IN_COLETA_VDV_Y_5, $IN_COLETA_VDV_Z_5, $IN_COLETA_FATOR_CRISTA_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_DURACAO_6, $IN_COLETA_ACELERACAO_X_6, $IN_COLETA_ACELERACAO_Y_6, $IN_COLETA_ACELERACAO_Z_6, $IN_COLETA_VDV_X_6, $IN_COLETA_VDV_Y_6, $IN_COLETA_VDV_Z_6, $IN_COLETA_FATOR_CRISTA_6, $IN_TEMPO_MEDICAO, $IN_NR09_AREN, $IN_NR09_VDVR, $IN_NR15_AREN, $IN_NR15_VDVR, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_PARADIGMA_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_PARADIGMA_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_PARADIGMA_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_PARADIGMA_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_PARADIGMA_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_PARADIGMA_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_AGENTE_RISCO, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_EPC, $IN_FABR_EPI, $IN_MOD_EPI, $IN_CERT_APROV, $IN_GRAU_ATENUACAO, $IN_ACEL_1, $IN_ACEL_MARCA_1, $IN_ACEL_MODELO_1, $IN_ACEL_SERIAL_1, $IN_ACEL_CERT_CALIB_1, $IN_ACEL_2, $IN_ACEL_MARCA_2, $IN_ACEL_MODELO_2, $IN_ACEL_SERIAL_2, $IN_ACEL_CERT_CALIB_2, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_DURACAO_1, $IN_COLETA_ACELERACAO_X_1, $IN_COLETA_ACELERACAO_Y_1, $IN_COLETA_ACELERACAO_Z_1, $IN_COLETA_VDV_X_1, $IN_COLETA_VDV_Y_1, $IN_COLETA_VDV_Z_1, $IN_COLETA_FATOR_CRISTA_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_DURACAO_2, $IN_COLETA_ACELERACAO_X_2, $IN_COLETA_ACELERACAO_Y_2, $IN_COLETA_ACELERACAO_Z_2, $IN_COLETA_VDV_X_2, $IN_COLETA_VDV_Y_2, $IN_COLETA_VDV_Z_2, $IN_COLETA_FATOR_CRISTA_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_DURACAO_3, $IN_COLETA_ACELERACAO_X_3, $IN_COLETA_ACELERACAO_Y_3, $IN_COLETA_ACELERACAO_Z_3, $IN_COLETA_VDV_X_3, $IN_COLETA_VDV_Y_3, $IN_COLETA_VDV_Z_3, $IN_COLETA_FATOR_CRISTA_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_DURACAO_4, $IN_COLETA_ACELERACAO_X_4, $IN_COLETA_ACELERACAO_Y_4, $IN_COLETA_ACELERACAO_Z_4, $IN_COLETA_VDV_X_4, $IN_COLETA_VDV_Y_4, $IN_COLETA_VDV_Z_4, $IN_COLETA_FATOR_CRISTA_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_DURACAO_5, $IN_COLETA_ACELERACAO_X_5, $IN_COLETA_ACELERACAO_Y_5, $IN_COLETA_ACELERACAO_Z_5, $IN_COLETA_VDV_X_5, $IN_COLETA_VDV_Y_5, $IN_COLETA_VDV_Z_5, $IN_COLETA_FATOR_CRISTA_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_DURACAO_6, $IN_COLETA_ACELERACAO_X_6, $IN_COLETA_ACELERACAO_Y_6, $IN_COLETA_ACELERACAO_Z_6, $IN_COLETA_VDV_X_6, $IN_COLETA_VDV_Y_6, $IN_COLETA_VDV_Z_6, $IN_COLETA_FATOR_CRISTA_6, $IN_TEMPO_MEDICAO, $IN_NR09_AREN, $IN_NR09_VDVR, $IN_NR15_AREN, $IN_NR15_VDVR, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
				
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id, upper(CLNT.`nome_interno`) as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`analises` as analises, AA.`coletas` as coletas
       FROM `VIBR_VCI` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
   ORDER BY 2,3,4,5"
		)) 
		{
			$stmt->bind_param('ss', $sql_DATA_ELABORACAO, $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_DATA_ELABORACAO, $o_ANALISES, $o_COLETAS);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JAN; }
					if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_FEV; }
					if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_MAR; }
					if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_ABR; }
					if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_MAI; }
					if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JUN; }
					if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JUL; }
					if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_AGO; }
					if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_SET; }
					if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_OUT; }
					if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_NOV; }
					if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_DEZ; }
					if( $o_ANALISES == "1"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_1; }
					if( $o_ANALISES == "2"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_2; }
					if( $o_ANALISES == "3"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_3; }
					if( $o_ANALISES == "4"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_4; }
					if( $o_ANALISES == "5"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_5; }
					if( $o_ANALISES == "6"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_6; }
					if( $o_COLETAS == "1"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_1; }
					if( $o_COLETAS == "2"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_2; }
					if( $o_COLETAS == "3"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_3; }
					if( $o_COLETAS == "4"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_4; }
					if( $o_COLETAS == "5"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_5; }
					if( $o_COLETAS == "6"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_6; }
				
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td><small>'.$o_IDCLIENTE.'</small></td>';
					$TBODY_LIST .= '<td>'.$o_ANO.'</td>';
					$TBODY_LIST .= '<td>'.$o_MES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_PLANILHA_NUM.'</td>';
					$TBODY_LIST .= '<td>'.$o_DATA_ELABORACAO.'</td>';
					$TBODY_LIST .= '<td>'.$o_ANALISES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_COLETAS_TXT.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_PARADIGMA_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_PARADIGMA_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_PARADIGMA_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_PARADIGMA_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_PARADIGMA_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_PARADIGMA_6, $_ANALISE_TAREFA_EXEC_6, $_AGENTE_RISCO, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_FABR_EPI, $_MOD_EPI, $_CERT_APROV, $_GRAU_ATENUACAO, $_ACEL_1, $_ACEL_MARCA_1, $_ACEL_MODELO_1, $_ACEL_SERIAL_1, $_ACEL_CERT_CALIB_1, $_ACEL_2, $_ACEL_MARCA_2, $_ACEL_MODELO_2, $_ACEL_SERIAL_2, $_ACEL_CERT_CALIB_2, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_DURACAO_1, $_COLETA_ACELERACAO_X_1, $_COLETA_ACELERACAO_Y_1, $_COLETA_ACELERACAO_Z_1, $_COLETA_VDV_X_1, $_COLETA_VDV_Y_1, $_COLETA_VDV_Z_1, $_COLETA_FATOR_CRISTA_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_DURACAO_2, $_COLETA_ACELERACAO_X_2, $_COLETA_ACELERACAO_Y_2, $_COLETA_ACELERACAO_Z_2, $_COLETA_VDV_X_2, $_COLETA_VDV_Y_2, $_COLETA_VDV_Z_2, $_COLETA_FATOR_CRISTA_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_DURACAO_3, $_COLETA_ACELERACAO_X_3, $_COLETA_ACELERACAO_Y_3, $_COLETA_ACELERACAO_Z_3, $_COLETA_VDV_X_3, $_COLETA_VDV_Y_3, $_COLETA_VDV_Z_3, $_COLETA_FATOR_CRISTA_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_DURACAO_4, $_COLETA_ACELERACAO_X_4, $_COLETA_ACELERACAO_Y_4, $_COLETA_ACELERACAO_Z_4, $_COLETA_VDV_X_4, $_COLETA_VDV_Y_4, $_COLETA_VDV_Z_4, $_COLETA_FATOR_CRISTA_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_DURACAO_5, $_COLETA_ACELERACAO_X_5, $_COLETA_ACELERACAO_Y_5, $_COLETA_ACELERACAO_Z_5, $_COLETA_VDV_X_5, $_COLETA_VDV_Y_5, $_COLETA_VDV_Z_5, $_COLETA_FATOR_CRISTA_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_DURACAO_6, $_COLETA_ACELERACAO_X_6, $_COLETA_ACELERACAO_Y_6, $_COLETA_ACELERACAO_Z_6, $_COLETA_VDV_X_6, $_COLETA_VDV_Y_6, $_COLETA_VDV_Z_6, $_COLETA_FATOR_CRISTA_6, $_TEMPO_MEDICAO, $_NR09_AREN, $_NR09_VDVR, $_NR15_AREN, $_NR15_VDVR, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id
       FROM `VIBR_VCI` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_PARADIGMA_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_PARADIGMA_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_PARADIGMA_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_PARADIGMA_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_PARADIGMA_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_PARADIGMA_6, $_ANALISE_TAREFA_EXEC_6, $_AGENTE_RISCO, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_FABR_EPI, $_MOD_EPI, $_CERT_APROV, $_GRAU_ATENUACAO, $_ACEL_1, $_ACEL_MARCA_1, $_ACEL_MODELO_1, $_ACEL_SERIAL_1, $_ACEL_CERT_CALIB_1, $_ACEL_2, $_ACEL_MARCA_2, $_ACEL_MODELO_2, $_ACEL_SERIAL_2, $_ACEL_CERT_CALIB_2, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_DURACAO_1, $_COLETA_ACELERACAO_X_1, $_COLETA_ACELERACAO_Y_1, $_COLETA_ACELERACAO_Z_1, $_COLETA_VDV_X_1, $_COLETA_VDV_Y_1, $_COLETA_VDV_Z_1, $_COLETA_FATOR_CRISTA_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_DURACAO_2, $_COLETA_ACELERACAO_X_2, $_COLETA_ACELERACAO_Y_2, $_COLETA_ACELERACAO_Z_2, $_COLETA_VDV_X_2, $_COLETA_VDV_Y_2, $_COLETA_VDV_Z_2, $_COLETA_FATOR_CRISTA_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_DURACAO_3, $_COLETA_ACELERACAO_X_3, $_COLETA_ACELERACAO_Y_3, $_COLETA_ACELERACAO_Z_3, $_COLETA_VDV_X_3, $_COLETA_VDV_Y_3, $_COLETA_VDV_Z_3, $_COLETA_FATOR_CRISTA_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_DURACAO_4, $_COLETA_ACELERACAO_X_4, $_COLETA_ACELERACAO_Y_4, $_COLETA_ACELERACAO_Z_4, $_COLETA_VDV_X_4, $_COLETA_VDV_Y_4, $_COLETA_VDV_Z_4, $_COLETA_FATOR_CRISTA_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_DURACAO_5, $_COLETA_ACELERACAO_X_5, $_COLETA_ACELERACAO_Y_5, $_COLETA_ACELERACAO_Z_5, $_COLETA_VDV_X_5, $_COLETA_VDV_Y_5, $_COLETA_VDV_Z_5, $_COLETA_FATOR_CRISTA_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_DURACAO_6, $_COLETA_ACELERACAO_X_6, $_COLETA_ACELERACAO_Y_6, $_COLETA_ACELERACAO_Z_6, $_COLETA_VDV_X_6, $_COLETA_VDV_Y_6, $_COLETA_VDV_Z_6, $_COLETA_FATOR_CRISTA_6, $_TEMPO_MEDICAO, $_NR09_AREN, $_NR09_VDVR, $_NR15_AREN, $_NR15_VDVR, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_1_filename = "";
		if($_FILES["img_ativ_filename_1_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_1_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_1_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_1_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE #1 - ".$_FILES["img_ativ_filename_1_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_1_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_1_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_1_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_1_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE #1 - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_IMG_ATIV_FILENAME_2_filename = "";
		if($_FILES["img_ativ_filename_2_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_2_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_2_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_2_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE #2 - ".$_FILES["img_ativ_filename_2_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_2_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_2_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_2_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_2_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE #2 - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_PARADIGMA_1 = $mysqli->escape_String($_ANALISE_PARADIGMA_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_PARADIGMA_2 = $mysqli->escape_String($_ANALISE_PARADIGMA_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_PARADIGMA_3 = $mysqli->escape_String($_ANALISE_PARADIGMA_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_PARADIGMA_4 = $mysqli->escape_String($_ANALISE_PARADIGMA_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_PARADIGMA_5 = $mysqli->escape_String($_ANALISE_PARADIGMA_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_PARADIGMA_6 = $mysqli->escape_String($_ANALISE_PARADIGMA_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_AGENTE_RISCO = $mysqli->escape_String($_AGENTE_RISCO);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_FABR_EPI = $mysqli->escape_String($_FABR_EPI);
		$sql_MOD_EPI = $mysqli->escape_String($_MOD_EPI);
		$sql_CERT_APROV = $mysqli->escape_String($_CERT_APROV);
		$sql_GRAU_ATENUACAO = $mysqli->escape_String($_GRAU_ATENUACAO);
		$sql_ACEL_1 = $mysqli->escape_String($_ACEL_1);
		$sql_ACEL_MARCA_1 = $mysqli->escape_String($_ACEL_MARCA_1);
		$sql_ACEL_MODELO_1 = $mysqli->escape_String($_ACEL_MODELO_1);
		$sql_ACEL_SERIAL_1 = $mysqli->escape_String($_ACEL_SERIAL_1);
		$sql_ACEL_CERT_CALIB_1 = $mysqli->escape_String($_ACEL_CERT_CALIB_1);
		$sql_ACEL_2 = $mysqli->escape_String($_ACEL_2);
		$sql_ACEL_MARCA_2 = $mysqli->escape_String($_ACEL_MARCA_2);
		$sql_ACEL_MODELO_2 = $mysqli->escape_String($_ACEL_MODELO_2);
		$sql_ACEL_SERIAL_2 = $mysqli->escape_String($_ACEL_SERIAL_2);
		$sql_ACEL_CERT_CALIB_2 = $mysqli->escape_String($_ACEL_CERT_CALIB_2);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_AMOSTRA_1 = $mysqli->escape_String($_COLETA_AMOSTRA_1);
		$sql_COLETA_DATA_1 = $mysqli->escape_String($_COLETA_DATA_1);
		$sql_COLETA_DURACAO_1 = $mysqli->escape_String($_COLETA_DURACAO_1);
		$sql_COLETA_ACELERACAO_X_1 = $mysqli->escape_String($_COLETA_ACELERACAO_X_1);
		$sql_COLETA_ACELERACAO_Y_1 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_1);
		$sql_COLETA_ACELERACAO_Z_1 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_1);
		$sql_COLETA_VDV_X_1 = $mysqli->escape_String($_COLETA_VDV_X_1);
		$sql_COLETA_VDV_Y_1 = $mysqli->escape_String($_COLETA_VDV_Y_1);
		$sql_COLETA_VDV_Z_1 = $mysqli->escape_String($_COLETA_VDV_Z_1);
		$sql_COLETA_FATOR_CRISTA_1 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_1);
		$sql_COLETA_AMOSTRA_2 = $mysqli->escape_String($_COLETA_AMOSTRA_2);
		$sql_COLETA_DATA_2 = $mysqli->escape_String($_COLETA_DATA_2);
		$sql_COLETA_DURACAO_2 = $mysqli->escape_String($_COLETA_DURACAO_2);
		$sql_COLETA_ACELERACAO_X_2 = $mysqli->escape_String($_COLETA_ACELERACAO_X_2);
		$sql_COLETA_ACELERACAO_Y_2 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_2);
		$sql_COLETA_ACELERACAO_Z_2 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_2);
		$sql_COLETA_VDV_X_2 = $mysqli->escape_String($_COLETA_VDV_X_2);
		$sql_COLETA_VDV_Y_2 = $mysqli->escape_String($_COLETA_VDV_Y_2);
		$sql_COLETA_VDV_Z_2 = $mysqli->escape_String($_COLETA_VDV_Z_2);
		$sql_COLETA_FATOR_CRISTA_2 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_2);
		$sql_COLETA_AMOSTRA_3 = $mysqli->escape_String($_COLETA_AMOSTRA_3);
		$sql_COLETA_DATA_3 = $mysqli->escape_String($_COLETA_DATA_3);
		$sql_COLETA_DURACAO_3 = $mysqli->escape_String($_COLETA_DURACAO_3);
		$sql_COLETA_ACELERACAO_X_3 = $mysqli->escape_String($_COLETA_ACELERACAO_X_3);
		$sql_COLETA_ACELERACAO_Y_3 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_3);
		$sql_COLETA_ACELERACAO_Z_3 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_3);
		$sql_COLETA_VDV_X_3 = $mysqli->escape_String($_COLETA_VDV_X_3);
		$sql_COLETA_VDV_Y_3 = $mysqli->escape_String($_COLETA_VDV_Y_3);
		$sql_COLETA_VDV_Z_3 = $mysqli->escape_String($_COLETA_VDV_Z_3);
		$sql_COLETA_FATOR_CRISTA_3 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_3);
		$sql_COLETA_AMOSTRA_4 = $mysqli->escape_String($_COLETA_AMOSTRA_4);
		$sql_COLETA_DATA_4 = $mysqli->escape_String($_COLETA_DATA_4);
		$sql_COLETA_DURACAO_4 = $mysqli->escape_String($_COLETA_DURACAO_4);
		$sql_COLETA_ACELERACAO_X_4 = $mysqli->escape_String($_COLETA_ACELERACAO_X_4);
		$sql_COLETA_ACELERACAO_Y_4 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_4);
		$sql_COLETA_ACELERACAO_Z_4 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_4);
		$sql_COLETA_VDV_X_4 = $mysqli->escape_String($_COLETA_VDV_X_4);
		$sql_COLETA_VDV_Y_4 = $mysqli->escape_String($_COLETA_VDV_Y_4);
		$sql_COLETA_VDV_Z_4 = $mysqli->escape_String($_COLETA_VDV_Z_4);
		$sql_COLETA_FATOR_CRISTA_4 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_4);
		$sql_COLETA_AMOSTRA_5 = $mysqli->escape_String($_COLETA_AMOSTRA_5);
		$sql_COLETA_DATA_5 = $mysqli->escape_String($_COLETA_DATA_5);
		$sql_COLETA_DURACAO_5 = $mysqli->escape_String($_COLETA_DURACAO_5);
		$sql_COLETA_ACELERACAO_X_5 = $mysqli->escape_String($_COLETA_ACELERACAO_X_5);
		$sql_COLETA_ACELERACAO_Y_5 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_5);
		$sql_COLETA_ACELERACAO_Z_5 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_5);
		$sql_COLETA_VDV_X_5 = $mysqli->escape_String($_COLETA_VDV_X_5);
		$sql_COLETA_VDV_Y_5 = $mysqli->escape_String($_COLETA_VDV_Y_5);
		$sql_COLETA_VDV_Z_5 = $mysqli->escape_String($_COLETA_VDV_Z_5);
		$sql_COLETA_FATOR_CRISTA_5 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_5);
		$sql_COLETA_AMOSTRA_6 = $mysqli->escape_String($_COLETA_AMOSTRA_6);
		$sql_COLETA_DATA_6 = $mysqli->escape_String($_COLETA_DATA_6);
		$sql_COLETA_DURACAO_6 = $mysqli->escape_String($_COLETA_DURACAO_6);
		$sql_COLETA_ACELERACAO_X_6 = $mysqli->escape_String($_COLETA_ACELERACAO_X_6);
		$sql_COLETA_ACELERACAO_Y_6 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_6);
		$sql_COLETA_ACELERACAO_Z_6 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_6);
		$sql_COLETA_VDV_X_6 = $mysqli->escape_String($_COLETA_VDV_X_6);
		$sql_COLETA_VDV_Y_6 = $mysqli->escape_String($_COLETA_VDV_Y_6);
		$sql_COLETA_VDV_Z_6 = $mysqli->escape_String($_COLETA_VDV_Z_6);
		$sql_COLETA_FATOR_CRISTA_6 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_6);
		$sql_TEMPO_MEDICAO = $mysqli->escape_String($_TEMPO_MEDICAO);
		$sql_NR09_AREN = $mysqli->escape_String($_NR09_AREN);
		$sql_NR09_VDVR = $mysqli->escape_String($_NR09_VDVR);
		$sql_NR15_AREN = $mysqli->escape_String($_NR15_AREN);
		$sql_NR15_VDVR = $mysqli->escape_String($_NR15_VDVR);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_1_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_1_filename);
		$sql_IMG_ATIV_FILENAME_2_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_2_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id
       FROM `VIBR_VCI` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		if($_DEBUG == 1)
		{
			//($statement = $mysqli->prepare($SQL)) or trigger_error($mysqli->error, E_USER_ERROR);
			//$statement->execute() or trigger_error($statement->error, E_USER_ERROR);
		}
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `VIBR_VCI` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_paradigma_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_paradigma_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_paradigma_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_paradigma_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_paradigma_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_paradigma_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`agente_risco` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`epc` = ?
		        ,`fabr_epi` = ?
		        ,`mod_epi` = ?
		        ,`cert_aprov` = ?
		        ,`grau_atenuacao` = ?
		        ,`acel_1` = ?
		        ,`acel_marca_1` = ?
		        ,`acel_modelo_1` = ?
		        ,`acel_serial_1` = ?
		        ,`acel_cert_calib_1` = ?
		        ,`acel_2` = ?
		        ,`acel_marca_2` = ?
		        ,`acel_modelo_2` = ?
		        ,`acel_serial_2` = ?
		        ,`acel_cert_calib_2` = ?
		        ,`coletas` = ?
		        ,`coleta_amostra_1` = ?
		        ,`coleta_data_1` = ?
		        ,`coleta_duracao_1` = ?
		        ,`coleta_aceleracao_x_1` = ?
		        ,`coleta_aceleracao_y_1` = ?
		        ,`coleta_aceleracao_z_1` = ?
		        ,`coleta_vdv_x_1` = ?
		        ,`coleta_vdv_y_1` = ?
		        ,`coleta_vdv_z_1` = ?
		        ,`coleta_fator_crista_1` = ?
		        ,`coleta_amostra_2` = ?
		        ,`coleta_data_2` = ?
		        ,`coleta_duracao_2` = ?
		        ,`coleta_aceleracao_x_2` = ?
		        ,`coleta_aceleracao_y_2` = ?
		        ,`coleta_aceleracao_z_2` = ?
		        ,`coleta_vdv_x_2` = ?
		        ,`coleta_vdv_y_2` = ?
		        ,`coleta_vdv_z_2` = ?
		        ,`coleta_fator_crista_2` = ?
		        ,`coleta_amostra_3` = ?
		        ,`coleta_data_3` = ?
		        ,`coleta_duracao_3` = ?
		        ,`coleta_aceleracao_x_3` = ?
		        ,`coleta_aceleracao_y_3` = ?
		        ,`coleta_aceleracao_z_3` = ?
		        ,`coleta_vdv_x_3` = ?
		        ,`coleta_vdv_y_3` = ?
		        ,`coleta_vdv_z_3` = ?
		        ,`coleta_fator_crista_3` = ?
		        ,`coleta_amostra_4` = ?
		        ,`coleta_data_4` = ?
		        ,`coleta_duracao_4` = ?
		        ,`coleta_aceleracao_x_4` = ?
		        ,`coleta_aceleracao_y_4` = ?
		        ,`coleta_aceleracao_z_4` = ?
		        ,`coleta_vdv_x_4` = ?
		        ,`coleta_vdv_y_4` = ?
		        ,`coleta_vdv_z_4` = ?
		        ,`coleta_fator_crista_4` = ?
		        ,`coleta_amostra_5` = ?
		        ,`coleta_data_5` = ?
		        ,`coleta_duracao_5` = ?
		        ,`coleta_aceleracao_x_5` = ?
		        ,`coleta_aceleracao_y_5` = ?
		        ,`coleta_aceleracao_z_5` = ?
		        ,`coleta_vdv_x_5` = ?
		        ,`coleta_vdv_y_5` = ?
		        ,`coleta_vdv_z_5` = ?
		        ,`coleta_fator_crista_5` = ?
		        ,`coleta_amostra_6` = ?
		        ,`coleta_data_6` = ?
		        ,`coleta_duracao_6` = ?
		        ,`coleta_aceleracao_x_6` = ?
		        ,`coleta_aceleracao_y_6` = ?
		        ,`coleta_aceleracao_z_6` = ?
		        ,`coleta_vdv_x_6` = ?
		        ,`coleta_vdv_y_6` = ?
		        ,`coleta_vdv_z_6` = ?
		        ,`coleta_fator_crista_6` = ?
		        ,`tempo_medicao` = ?
		        ,`nr09_aren` = ?
		        ,`nr09_vdvr` = ?
		        ,`nr15_aren` = ?
		        ,`nr15_vdvr` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		        ,`img_ativ_filename_1` = ?
		        ,`img_ativ_filename_2` = ?
		        ,`logo_filename` = ?"
		)) 
		{
			$stmt->bind_param('ssddddssssssssdsssssssssssssssssssssssssssssssssssssssssssssdsssdddddddsssdddddddsssdddddddsssdddddddsssdddddddsssddddddddddddddsssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_PARADIGMA_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_PARADIGMA_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_PARADIGMA_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_PARADIGMA_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_PARADIGMA_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_PARADIGMA_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_AGENTE_RISCO, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_EPC, $sql_FABR_EPI, $sql_MOD_EPI, $sql_CERT_APROV, $sql_GRAU_ATENUACAO, $sql_ACEL_1, $sql_ACEL_MARCA_1, $sql_ACEL_MODELO_1, $sql_ACEL_SERIAL_1, $sql_ACEL_CERT_CALIB_1, $sql_ACEL_2, $sql_ACEL_MARCA_2, $sql_ACEL_MODELO_2, $sql_ACEL_SERIAL_2, $sql_ACEL_CERT_CALIB_2, $sql_COLETAS, $sql_COLETA_AMOSTRA_1, $sql_COLETA_DATA_1, $sql_COLETA_DURACAO_1, $sql_COLETA_ACELERACAO_X_1, $sql_COLETA_ACELERACAO_Y_1, $sql_COLETA_ACELERACAO_Z_1, $sql_COLETA_VDV_X_1, $sql_COLETA_VDV_Y_1, $sql_COLETA_VDV_Z_1, $sql_COLETA_FATOR_CRISTA_1, $sql_COLETA_AMOSTRA_2, $sql_COLETA_DATA_2, $sql_COLETA_DURACAO_2, $sql_COLETA_ACELERACAO_X_2, $sql_COLETA_ACELERACAO_Y_2, $sql_COLETA_ACELERACAO_Z_2, $sql_COLETA_VDV_X_2, $sql_COLETA_VDV_Y_2, $sql_COLETA_VDV_Z_2, $sql_COLETA_FATOR_CRISTA_2, $sql_COLETA_AMOSTRA_3, $sql_COLETA_DATA_3, $sql_COLETA_DURACAO_3, $sql_COLETA_ACELERACAO_X_3, $sql_COLETA_ACELERACAO_Y_3, $sql_COLETA_ACELERACAO_Z_3, $sql_COLETA_VDV_X_3, $sql_COLETA_VDV_Y_3, $sql_COLETA_VDV_Z_3, $sql_COLETA_FATOR_CRISTA_3, $sql_COLETA_AMOSTRA_4, $sql_COLETA_DATA_4, $sql_COLETA_DURACAO_4, $sql_COLETA_ACELERACAO_X_4, $sql_COLETA_ACELERACAO_Y_4, $sql_COLETA_ACELERACAO_Z_4, $sql_COLETA_VDV_X_4, $sql_COLETA_VDV_Y_4, $sql_COLETA_VDV_Z_4, $sql_COLETA_FATOR_CRISTA_4, $sql_COLETA_AMOSTRA_5, $sql_COLETA_DATA_5, $sql_COLETA_DURACAO_5, $sql_COLETA_ACELERACAO_X_5, $sql_COLETA_ACELERACAO_Y_5, $sql_COLETA_ACELERACAO_Z_5, $sql_COLETA_VDV_X_5, $sql_COLETA_VDV_Y_5, $sql_COLETA_VDV_Z_5, $sql_COLETA_FATOR_CRISTA_5, $sql_COLETA_AMOSTRA_6, $sql_COLETA_DATA_6, $sql_COLETA_DURACAO_6, $sql_COLETA_ACELERACAO_X_6, $sql_COLETA_ACELERACAO_Y_6, $sql_COLETA_ACELERACAO_Z_6, $sql_COLETA_VDV_X_6, $sql_COLETA_VDV_Y_6, $sql_COLETA_VDV_Z_6, $sql_COLETA_FATOR_CRISTA_6, $sql_TEMPO_MEDICAO, $sql_NR09_AREN, $sql_NR09_VDVR, $sql_NR15_AREN, $sql_NR15_VDVR, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_IMG_ATIV_FILENAME_1_filename, $sql_IMG_ATIV_FILENAME_2_filename, $sql_LOGO_FILENAME_filename);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_PARADIGMA_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_PARADIGMA_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_PARADIGMA_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_PARADIGMA_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_PARADIGMA_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_PARADIGMA_6, $_ANALISE_TAREFA_EXEC_6, $_AGENTE_RISCO, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_EPC, $_FABR_EPI, $_MOD_EPI, $_CERT_APROV, $_GRAU_ATENUACAO, $_ACEL_1, $_ACEL_MARCA_1, $_ACEL_MODELO_1, $_ACEL_SERIAL_1, $_ACEL_CERT_CALIB_1, $_ACEL_2, $_ACEL_MARCA_2, $_ACEL_MODELO_2, $_ACEL_SERIAL_2, $_ACEL_CERT_CALIB_2, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_DURACAO_1, $_COLETA_ACELERACAO_X_1, $_COLETA_ACELERACAO_Y_1, $_COLETA_ACELERACAO_Z_1, $_COLETA_VDV_X_1, $_COLETA_VDV_Y_1, $_COLETA_VDV_Z_1, $_COLETA_FATOR_CRISTA_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_DURACAO_2, $_COLETA_ACELERACAO_X_2, $_COLETA_ACELERACAO_Y_2, $_COLETA_ACELERACAO_Z_2, $_COLETA_VDV_X_2, $_COLETA_VDV_Y_2, $_COLETA_VDV_Z_2, $_COLETA_FATOR_CRISTA_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_DURACAO_3, $_COLETA_ACELERACAO_X_3, $_COLETA_ACELERACAO_Y_3, $_COLETA_ACELERACAO_Z_3, $_COLETA_VDV_X_3, $_COLETA_VDV_Y_3, $_COLETA_VDV_Z_3, $_COLETA_FATOR_CRISTA_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_DURACAO_4, $_COLETA_ACELERACAO_X_4, $_COLETA_ACELERACAO_Y_4, $_COLETA_ACELERACAO_Z_4, $_COLETA_VDV_X_4, $_COLETA_VDV_Y_4, $_COLETA_VDV_Z_4, $_COLETA_FATOR_CRISTA_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_DURACAO_5, $_COLETA_ACELERACAO_X_5, $_COLETA_ACELERACAO_Y_5, $_COLETA_ACELERACAO_Z_5, $_COLETA_VDV_X_5, $_COLETA_VDV_Y_5, $_COLETA_VDV_Z_5, $_COLETA_FATOR_CRISTA_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_DURACAO_6, $_COLETA_ACELERACAO_X_6, $_COLETA_ACELERACAO_Y_6, $_COLETA_ACELERACAO_Z_6, $_COLETA_VDV_X_6, $_COLETA_VDV_Y_6, $_COLETA_VDV_Z_6, $_COLETA_FATOR_CRISTA_6, $_TEMPO_MEDICAO, $_NR09_AREN, $_NR09_VDVR, $_NR15_AREN, $_NR15_VDVR, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_1_filename = "";
		if($_FILES["img_ativ_filename_1_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_1_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_1_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_1_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE #1 - ".$_FILES["img_ativ_filename_1_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_1_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_1_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_1_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_1_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE #1 - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_IMG_ATIV_FILENAME_2_filename = "";
		if($_FILES["img_ativ_filename_2_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_2_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_2_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_2_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE #2 - ".$_FILES["img_ativ_filename_2_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_2_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_2_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_2_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_2_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE #2 - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_PARADIGMA_1 = $mysqli->escape_String($_ANALISE_PARADIGMA_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_PARADIGMA_2 = $mysqli->escape_String($_ANALISE_PARADIGMA_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_PARADIGMA_3 = $mysqli->escape_String($_ANALISE_PARADIGMA_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_PARADIGMA_4 = $mysqli->escape_String($_ANALISE_PARADIGMA_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_PARADIGMA_5 = $mysqli->escape_String($_ANALISE_PARADIGMA_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_PARADIGMA_6 = $mysqli->escape_String($_ANALISE_PARADIGMA_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_AGENTE_RISCO = $mysqli->escape_String($_AGENTE_RISCO);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_FABR_EPI = $mysqli->escape_String($_FABR_EPI);
		$sql_MOD_EPI = $mysqli->escape_String($_MOD_EPI);
		$sql_CERT_APROV = $mysqli->escape_String($_CERT_APROV);
		$sql_GRAU_ATENUACAO = $mysqli->escape_String($_GRAU_ATENUACAO);
		$sql_ACEL_1 = $mysqli->escape_String($_ACEL_1);
		$sql_ACEL_MARCA_1 = $mysqli->escape_String($_ACEL_MARCA_1);
		$sql_ACEL_MODELO_1 = $mysqli->escape_String($_ACEL_MODELO_1);
		$sql_ACEL_SERIAL_1 = $mysqli->escape_String($_ACEL_SERIAL_1);
		$sql_ACEL_CERT_CALIB_1 = $mysqli->escape_String($_ACEL_CERT_CALIB_1);
		$sql_ACEL_2 = $mysqli->escape_String($_ACEL_2);
		$sql_ACEL_MARCA_2 = $mysqli->escape_String($_ACEL_MARCA_2);
		$sql_ACEL_MODELO_2 = $mysqli->escape_String($_ACEL_MODELO_2);
		$sql_ACEL_SERIAL_2 = $mysqli->escape_String($_ACEL_SERIAL_2);
		$sql_ACEL_CERT_CALIB_2 = $mysqli->escape_String($_ACEL_CERT_CALIB_2);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_AMOSTRA_1 = $mysqli->escape_String($_COLETA_AMOSTRA_1);
		$sql_COLETA_DATA_1 = $mysqli->escape_String($_COLETA_DATA_1);
		$sql_COLETA_DURACAO_1 = $mysqli->escape_String($_COLETA_DURACAO_1);
		$sql_COLETA_ACELERACAO_X_1 = $mysqli->escape_String($_COLETA_ACELERACAO_X_1);
		$sql_COLETA_ACELERACAO_Y_1 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_1);
		$sql_COLETA_ACELERACAO_Z_1 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_1);
		$sql_COLETA_VDV_X_1 = $mysqli->escape_String($_COLETA_VDV_X_1);
		$sql_COLETA_VDV_Y_1 = $mysqli->escape_String($_COLETA_VDV_Y_1);
		$sql_COLETA_VDV_Z_1 = $mysqli->escape_String($_COLETA_VDV_Z_1);
		$sql_COLETA_FATOR_CRISTA_1 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_1);
		$sql_COLETA_AMOSTRA_2 = $mysqli->escape_String($_COLETA_AMOSTRA_2);
		$sql_COLETA_DATA_2 = $mysqli->escape_String($_COLETA_DATA_2);
		$sql_COLETA_DURACAO_2 = $mysqli->escape_String($_COLETA_DURACAO_2);
		$sql_COLETA_ACELERACAO_X_2 = $mysqli->escape_String($_COLETA_ACELERACAO_X_2);
		$sql_COLETA_ACELERACAO_Y_2 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_2);
		$sql_COLETA_ACELERACAO_Z_2 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_2);
		$sql_COLETA_VDV_X_2 = $mysqli->escape_String($_COLETA_VDV_X_2);
		$sql_COLETA_VDV_Y_2 = $mysqli->escape_String($_COLETA_VDV_Y_2);
		$sql_COLETA_VDV_Z_2 = $mysqli->escape_String($_COLETA_VDV_Z_2);
		$sql_COLETA_FATOR_CRISTA_2 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_2);
		$sql_COLETA_AMOSTRA_3 = $mysqli->escape_String($_COLETA_AMOSTRA_3);
		$sql_COLETA_DATA_3 = $mysqli->escape_String($_COLETA_DATA_3);
		$sql_COLETA_DURACAO_3 = $mysqli->escape_String($_COLETA_DURACAO_3);
		$sql_COLETA_ACELERACAO_X_3 = $mysqli->escape_String($_COLETA_ACELERACAO_X_3);
		$sql_COLETA_ACELERACAO_Y_3 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_3);
		$sql_COLETA_ACELERACAO_Z_3 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_3);
		$sql_COLETA_VDV_X_3 = $mysqli->escape_String($_COLETA_VDV_X_3);
		$sql_COLETA_VDV_Y_3 = $mysqli->escape_String($_COLETA_VDV_Y_3);
		$sql_COLETA_VDV_Z_3 = $mysqli->escape_String($_COLETA_VDV_Z_3);
		$sql_COLETA_FATOR_CRISTA_3 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_3);
		$sql_COLETA_AMOSTRA_4 = $mysqli->escape_String($_COLETA_AMOSTRA_4);
		$sql_COLETA_DATA_4 = $mysqli->escape_String($_COLETA_DATA_4);
		$sql_COLETA_DURACAO_4 = $mysqli->escape_String($_COLETA_DURACAO_4);
		$sql_COLETA_ACELERACAO_X_4 = $mysqli->escape_String($_COLETA_ACELERACAO_X_4);
		$sql_COLETA_ACELERACAO_Y_4 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_4);
		$sql_COLETA_ACELERACAO_Z_4 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_4);
		$sql_COLETA_VDV_X_4 = $mysqli->escape_String($_COLETA_VDV_X_4);
		$sql_COLETA_VDV_Y_4 = $mysqli->escape_String($_COLETA_VDV_Y_4);
		$sql_COLETA_VDV_Z_4 = $mysqli->escape_String($_COLETA_VDV_Z_4);
		$sql_COLETA_FATOR_CRISTA_4 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_4);
		$sql_COLETA_AMOSTRA_5 = $mysqli->escape_String($_COLETA_AMOSTRA_5);
		$sql_COLETA_DATA_5 = $mysqli->escape_String($_COLETA_DATA_5);
		$sql_COLETA_DURACAO_5 = $mysqli->escape_String($_COLETA_DURACAO_5);
		$sql_COLETA_ACELERACAO_X_5 = $mysqli->escape_String($_COLETA_ACELERACAO_X_5);
		$sql_COLETA_ACELERACAO_Y_5 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_5);
		$sql_COLETA_ACELERACAO_Z_5 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_5);
		$sql_COLETA_VDV_X_5 = $mysqli->escape_String($_COLETA_VDV_X_5);
		$sql_COLETA_VDV_Y_5 = $mysqli->escape_String($_COLETA_VDV_Y_5);
		$sql_COLETA_VDV_Z_5 = $mysqli->escape_String($_COLETA_VDV_Z_5);
		$sql_COLETA_FATOR_CRISTA_5 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_5);
		$sql_COLETA_AMOSTRA_6 = $mysqli->escape_String($_COLETA_AMOSTRA_6);
		$sql_COLETA_DATA_6 = $mysqli->escape_String($_COLETA_DATA_6);
		$sql_COLETA_DURACAO_6 = $mysqli->escape_String($_COLETA_DURACAO_6);
		$sql_COLETA_ACELERACAO_X_6 = $mysqli->escape_String($_COLETA_ACELERACAO_X_6);
		$sql_COLETA_ACELERACAO_Y_6 = $mysqli->escape_String($_COLETA_ACELERACAO_Y_6);
		$sql_COLETA_ACELERACAO_Z_6 = $mysqli->escape_String($_COLETA_ACELERACAO_Z_6);
		$sql_COLETA_VDV_X_6 = $mysqli->escape_String($_COLETA_VDV_X_6);
		$sql_COLETA_VDV_Y_6 = $mysqli->escape_String($_COLETA_VDV_Y_6);
		$sql_COLETA_VDV_Z_6 = $mysqli->escape_String($_COLETA_VDV_Z_6);
		$sql_COLETA_FATOR_CRISTA_6 = $mysqli->escape_String($_COLETA_FATOR_CRISTA_6);
		$sql_TEMPO_MEDICAO = $mysqli->escape_String($_TEMPO_MEDICAO);
		$sql_NR09_AREN = $mysqli->escape_String($_NR09_AREN);
		$sql_NR09_VDVR = $mysqli->escape_String($_NR09_VDVR);
		$sql_NR15_AREN = $mysqli->escape_String($_NR15_AREN);
		$sql_NR15_VDVR = $mysqli->escape_String($_NR15_VDVR);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_1_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_1_filename);
		$sql_IMG_ATIV_FILENAME_2_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_2_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id, AA.`img_ativ_filename_1`, AA.`img_ativ_filename_2`, AA.`logo_filename`
       FROM `VIBR_VCI` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVIBR_VCI` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idVIBR_VCI, $o_IMG_ATIV_FILENAME_1, $o_IMG_ATIV_FILENAME_2, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id
       FROM `VIBR_VCI` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
        AND AA.`idVIBR_VCI` != ?"
		)) 
		{
			$stmt->bind_param('sddddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		if ($stmt = $mysqli->prepare(
		"UPDATE `VIBR_VCI` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_paradigma_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_paradigma_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_paradigma_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_paradigma_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_paradigma_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_paradigma_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`agente_risco` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`epc` = ?
		        ,`fabr_epi` = ?
		        ,`mod_epi` = ?
		        ,`cert_aprov` = ?
		        ,`grau_atenuacao` = ?
		        ,`acel_1` = ?
		        ,`acel_marca_1` = ?
		        ,`acel_modelo_1` = ?
		        ,`acel_serial_1` = ?
		        ,`acel_cert_calib_1` = ?
		        ,`acel_2` = ?
		        ,`acel_marca_2` = ?
		        ,`acel_modelo_2` = ?
		        ,`acel_serial_2` = ?
		        ,`acel_cert_calib_2` = ?
		        ,`coletas` = ?
		        ,`coleta_amostra_1` = ?
		        ,`coleta_data_1` = ?
		        ,`coleta_duracao_1` = ?
		        ,`coleta_aceleracao_x_1` = ?
		        ,`coleta_aceleracao_y_1` = ?
		        ,`coleta_aceleracao_z_1` = ?
		        ,`coleta_vdv_x_1` = ?
		        ,`coleta_vdv_y_1` = ?
		        ,`coleta_vdv_z_1` = ?
		        ,`coleta_fator_crista_1` = ?
		        ,`coleta_amostra_2` = ?
		        ,`coleta_data_2` = ?
		        ,`coleta_duracao_2` = ?
		        ,`coleta_aceleracao_x_2` = ?
		        ,`coleta_aceleracao_y_2` = ?
		        ,`coleta_aceleracao_z_2` = ?
		        ,`coleta_vdv_x_2` = ?
		        ,`coleta_vdv_y_2` = ?
		        ,`coleta_vdv_z_2` = ?
		        ,`coleta_fator_crista_2` = ?
		        ,`coleta_amostra_3` = ?
		        ,`coleta_data_3` = ?
		        ,`coleta_duracao_3` = ?
		        ,`coleta_aceleracao_x_3` = ?
		        ,`coleta_aceleracao_y_3` = ?
		        ,`coleta_aceleracao_z_3` = ?
		        ,`coleta_vdv_x_3` = ?
		        ,`coleta_vdv_y_3` = ?
		        ,`coleta_vdv_z_3` = ?
		        ,`coleta_fator_crista_3` = ?
		        ,`coleta_amostra_4` = ?
		        ,`coleta_data_4` = ?
		        ,`coleta_duracao_4` = ?
		        ,`coleta_aceleracao_x_4` = ?
		        ,`coleta_aceleracao_y_4` = ?
		        ,`coleta_aceleracao_z_4` = ?
		        ,`coleta_vdv_x_4` = ?
		        ,`coleta_vdv_y_4` = ?
		        ,`coleta_vdv_z_4` = ?
		        ,`coleta_fator_crista_4` = ?
		        ,`coleta_amostra_5` = ?
		        ,`coleta_data_5` = ?
		        ,`coleta_duracao_5` = ?
		        ,`coleta_aceleracao_x_5` = ?
		        ,`coleta_aceleracao_y_5` = ?
		        ,`coleta_aceleracao_z_5` = ?
		        ,`coleta_vdv_x_5` = ?
		        ,`coleta_vdv_y_5` = ?
		        ,`coleta_vdv_z_5` = ?
		        ,`coleta_fator_crista_5` = ?
		        ,`coleta_amostra_6` = ?
		        ,`coleta_data_6` = ?
		        ,`coleta_duracao_6` = ?
		        ,`coleta_aceleracao_x_6` = ?
		        ,`coleta_aceleracao_y_6` = ?
		        ,`coleta_aceleracao_z_6` = ?
		        ,`coleta_vdv_x_6` = ?
		        ,`coleta_vdv_y_6` = ?
		        ,`coleta_vdv_z_6` = ?
		        ,`coleta_fator_crista_6` = ?
		        ,`tempo_medicao` = ?
		        ,`nr09_aren` = ?
		        ,`nr09_vdvr` = ?
		        ,`nr15_aren` = ?
		        ,`nr15_vdvr` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idVIBR_VCI` = ?"
		)) 
		{
			$stmt->bind_param('sddddssssssssdsssssssssssssssssssssssssssssssssssssssssssssdsssdddddddsssdddddddsssdddddddsssdddddddsssdddddddsssddddddddddddddssss', $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_PARADIGMA_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_PARADIGMA_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_PARADIGMA_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_PARADIGMA_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_PARADIGMA_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_PARADIGMA_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_AGENTE_RISCO, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_EPC, $sql_FABR_EPI, $sql_MOD_EPI, $sql_CERT_APROV, $sql_GRAU_ATENUACAO, $sql_ACEL_1, $sql_ACEL_MARCA_1, $sql_ACEL_MODELO_1, $sql_ACEL_SERIAL_1, $sql_ACEL_CERT_CALIB_1, $sql_ACEL_2, $sql_ACEL_MARCA_2, $sql_ACEL_MODELO_2, $sql_ACEL_SERIAL_2, $sql_ACEL_CERT_CALIB_2, $sql_COLETAS, $sql_COLETA_AMOSTRA_1, $sql_COLETA_DATA_1, $sql_COLETA_DURACAO_1, $sql_COLETA_ACELERACAO_X_1, $sql_COLETA_ACELERACAO_Y_1, $sql_COLETA_ACELERACAO_Z_1, $sql_COLETA_VDV_X_1, $sql_COLETA_VDV_Y_1, $sql_COLETA_VDV_Z_1, $sql_COLETA_FATOR_CRISTA_1, $sql_COLETA_AMOSTRA_2, $sql_COLETA_DATA_2, $sql_COLETA_DURACAO_2, $sql_COLETA_ACELERACAO_X_2, $sql_COLETA_ACELERACAO_Y_2, $sql_COLETA_ACELERACAO_Z_2, $sql_COLETA_VDV_X_2, $sql_COLETA_VDV_Y_2, $sql_COLETA_VDV_Z_2, $sql_COLETA_FATOR_CRISTA_2, $sql_COLETA_AMOSTRA_3, $sql_COLETA_DATA_3, $sql_COLETA_DURACAO_3, $sql_COLETA_ACELERACAO_X_3, $sql_COLETA_ACELERACAO_Y_3, $sql_COLETA_ACELERACAO_Z_3, $sql_COLETA_VDV_X_3, $sql_COLETA_VDV_Y_3, $sql_COLETA_VDV_Z_3, $sql_COLETA_FATOR_CRISTA_3, $sql_COLETA_AMOSTRA_4, $sql_COLETA_DATA_4, $sql_COLETA_DURACAO_4, $sql_COLETA_ACELERACAO_X_4, $sql_COLETA_ACELERACAO_Y_4, $sql_COLETA_ACELERACAO_Z_4, $sql_COLETA_VDV_X_4, $sql_COLETA_VDV_Y_4, $sql_COLETA_VDV_Z_4, $sql_COLETA_FATOR_CRISTA_4, $sql_COLETA_AMOSTRA_5, $sql_COLETA_DATA_5, $sql_COLETA_DURACAO_5, $sql_COLETA_ACELERACAO_X_5, $sql_COLETA_ACELERACAO_Y_5, $sql_COLETA_ACELERACAO_Z_5, $sql_COLETA_VDV_X_5, $sql_COLETA_VDV_Y_5, $sql_COLETA_VDV_Z_5, $sql_COLETA_FATOR_CRISTA_5, $sql_COLETA_AMOSTRA_6, $sql_COLETA_DATA_6, $sql_COLETA_DURACAO_6, $sql_COLETA_ACELERACAO_X_6, $sql_COLETA_ACELERACAO_Y_6, $sql_COLETA_ACELERACAO_Z_6, $sql_COLETA_VDV_X_6, $sql_COLETA_VDV_Y_6, $sql_COLETA_VDV_Z_6, $sql_COLETA_FATOR_CRISTA_6, $sql_TEMPO_MEDICAO, $sql_NR09_AREN, $sql_NR09_VDVR, $sql_NR15_AREN, $sql_NR15_VDVR, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza IMAGEM ATIVIDADE #1
				if($_IMG_ATIV_FILENAME_1_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `VIBR_VCI` SET 
					        `img_ativ_filename_1` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idVIBR_VCI` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_IMG_ATIV_FILENAME_1_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME_1)) { unlink (ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME_1); }
						}
						else
						{
							return "0|IMAGEM ATIVIDADE #1 - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				# Atualiza IMAGEM ATIVIDADE #2
				if($_IMG_ATIV_FILENAME_2_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `VIBR_VCI` SET 
					        `img_ativ_filename_2` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idVIBR_VCI` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_IMG_ATIV_FILENAME_2_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME_2)) { unlink (ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME_2); }
						}
						else
						{
							return "0|IMAGEM ATIVIDADE #2 - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				# Atualiza IMAGEM LOGOMARCA
				if($_LOGO_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `VIBR_VCI` SET 
					        `logo_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idVIBR_VCI` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_LOGO_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_LOGO_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_LOGO_FILENAME); }
						}
						else
						{
							return "0|IMAGEM LOGOMARCA - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `VIBR_VCI` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idVIBR_VCI` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = '%d/%m/%Y';
		$sql_COLETA_DATA_1 = '%d/%m/%Y';
		$sql_COLETA_DATA_2 = '%d/%m/%Y';
		$sql_COLETA_DATA_3 = '%d/%m/%Y';
		$sql_COLETA_DATA_4 = '%d/%m/%Y';
		$sql_COLETA_DATA_5 = '%d/%m/%Y';
		$sql_COLETA_DATA_6 = '%d/%m/%Y';
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id, AA.`idcliente` as idcliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_paradigma_1` as analise_paradigma_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_paradigma_2` as analise_paradigma_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_paradigma_3` as analise_paradigma_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_paradigma_4` as analise_paradigma_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_paradigma_5` as analise_paradigma_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_paradigma_6` as analise_paradigma_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`agente_risco` as agente_risco, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, AA.`fabr_epi` as fabr_epi, AA.`mod_epi` as mod_epi, AA.`cert_aprov` as cert_aprov, AA.`grau_atenuacao` as grau_atenuacao, AA.`acel_1` as acel_1, AA.`acel_marca_1` as acel_marca_1, AA.`acel_modelo_1` as acel_modelo_1, AA.`acel_serial_1` as acel_serial_1, AA.`acel_cert_calib_1` as acel_cert_calib_1, AA.`acel_2` as acel_2, AA.`acel_marca_2` as acel_marca_2, AA.`acel_modelo_2` as acel_modelo_2, AA.`acel_serial_2` as acel_serial_2, AA.`acel_cert_calib_2` as acel_cert_calib_2, AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_duracao_1` as coleta_duracao_1, AA.`coleta_aceleracao_x_1` as coleta_aceleracao_x_1, AA.`coleta_aceleracao_y_1` as coleta_aceleracao_y_1, AA.`coleta_aceleracao_z_1` as coleta_aceleracao_z_1, AA.`coleta_vdv_x_1` as coleta_vdv_x_1, AA.`coleta_vdv_y_1` as coleta_vdv_y_1, AA.`coleta_vdv_z_1` as coleta_vdv_z_1, AA.`coleta_fator_crista_1` as coleta_fator_crista_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_duracao_2` as coleta_duracao_2, AA.`coleta_aceleracao_x_2` as coleta_aceleracao_x_2, AA.`coleta_aceleracao_y_2` as coleta_aceleracao_y_2, AA.`coleta_aceleracao_z_2` as coleta_aceleracao_z_2, AA.`coleta_vdv_x_2` as coleta_vdv_x_2, AA.`coleta_vdv_y_2` as coleta_vdv_y_2, AA.`coleta_vdv_z_2` as coleta_vdv_z_2, AA.`coleta_fator_crista_2` as coleta_fator_crista_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_duracao_3` as coleta_duracao_3, AA.`coleta_aceleracao_x_3` as coleta_aceleracao_x_3, AA.`coleta_aceleracao_y_3` as coleta_aceleracao_y_3, AA.`coleta_aceleracao_z_3` as coleta_aceleracao_z_3, AA.`coleta_vdv_x_3` as coleta_vdv_x_3, AA.`coleta_vdv_y_3` as coleta_vdv_y_3, AA.`coleta_vdv_z_3` as coleta_vdv_z_3, AA.`coleta_fator_crista_3` as coleta_fator_crista_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_duracao_4` as coleta_duracao_4, AA.`coleta_aceleracao_x_4` as coleta_aceleracao_x_4, AA.`coleta_aceleracao_y_4` as coleta_aceleracao_y_4, AA.`coleta_aceleracao_z_4` as coleta_aceleracao_z_4, AA.`coleta_vdv_x_4` as coleta_vdv_x_4, AA.`coleta_vdv_y_4` as coleta_vdv_y_4, AA.`coleta_vdv_z_4` as coleta_vdv_z_4, AA.`coleta_fator_crista_4` as coleta_fator_crista_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_duracao_5` as coleta_duracao_5, AA.`coleta_aceleracao_x_5` as coleta_aceleracao_x_5, AA.`coleta_aceleracao_y_5` as coleta_aceleracao_y_5, AA.`coleta_aceleracao_z_5` as coleta_aceleracao_z_5, AA.`coleta_vdv_x_5` as coleta_vdv_x_5, AA.`coleta_vdv_y_5` as coleta_vdv_y_5, AA.`coleta_vdv_z_5` as coleta_vdv_z_5, AA.`coleta_fator_crista_5` as coleta_fator_crista_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_duracao_6` as coleta_duracao_6, AA.`coleta_aceleracao_x_6` as coleta_aceleracao_x_6, AA.`coleta_aceleracao_y_6` as coleta_aceleracao_y_6, AA.`coleta_aceleracao_z_6` as coleta_aceleracao_z_6, AA.`coleta_vdv_x_6` as coleta_vdv_x_6, AA.`coleta_vdv_y_6` as coleta_vdv_y_6, AA.`coleta_vdv_z_6` as coleta_vdv_z_6, AA.`coleta_fator_crista_6` as coleta_fator_crista_6, AA.`tempo_medicao` as tempo_medicao, AA.`nr09_aren` as nr09_aren, AA.`nr09_vdvr` as nr09_vdvr, AA.`nr15_aren` as nr15_aren, AA.`nr15_vdvr` as nr15_vdvr, AA.`resp_campo_idcolaborador` as resp_campo_idcolaborador, AA.`resp_tecnico_idcolaborador` as resp_tecnico_idcolaborador, upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename_1`) as img_ativ_filename_1, lower(AA.`img_ativ_filename_2`) as img_ativ_filename_2, lower(AA.`logo_filename`) as logo_filename
       FROM `VIBR_VCI` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVIBR_VCI` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssssssssss', $sql_DATA_ELABORACAO, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_COLETA_DATA_1, $sql_COLETA_DATA_2, $sql_COLETA_DATA_3, $sql_COLETA_DATA_4, $sql_COLETA_DATA_5, $sql_COLETA_DATA_6, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_PARADIGMA_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_PARADIGMA_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_PARADIGMA_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_PARADIGMA_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_PARADIGMA_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_PARADIGMA_6, $o_ANALISE_TAREFA_EXEC_6, $o_AGENTE_RISCO, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_FABR_EPI, $o_MOD_EPI, $o_CERT_APROV, $o_GRAU_ATENUACAO, $o_ACEL_1, $o_ACEL_MARCA_1, $o_ACEL_MODELO_1, $o_ACEL_SERIAL_1, $o_ACEL_CERT_CALIB_1, $o_ACEL_2, $o_ACEL_MARCA_2, $o_ACEL_MODELO_2, $o_ACEL_SERIAL_2, $o_ACEL_CERT_CALIB_2, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_DURACAO_1, $o_COLETA_ACELERACAO_X_1, $o_COLETA_ACELERACAO_Y_1, $o_COLETA_ACELERACAO_Z_1, $o_COLETA_VDV_X_1, $o_COLETA_VDV_Y_1, $o_COLETA_VDV_Z_1, $o_COLETA_FATOR_CRISTA_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_DURACAO_2, $o_COLETA_ACELERACAO_X_2, $o_COLETA_ACELERACAO_Y_2, $o_COLETA_ACELERACAO_Z_2, $o_COLETA_VDV_X_2, $o_COLETA_VDV_Y_2, $o_COLETA_VDV_Z_2, $o_COLETA_FATOR_CRISTA_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_DURACAO_3, $o_COLETA_ACELERACAO_X_3, $o_COLETA_ACELERACAO_Y_3, $o_COLETA_ACELERACAO_Z_3, $o_COLETA_VDV_X_3, $o_COLETA_VDV_Y_3, $o_COLETA_VDV_Z_3, $o_COLETA_FATOR_CRISTA_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_DURACAO_4, $o_COLETA_ACELERACAO_X_4, $o_COLETA_ACELERACAO_Y_4, $o_COLETA_ACELERACAO_Z_4, $o_COLETA_VDV_X_4, $o_COLETA_VDV_Y_4, $o_COLETA_VDV_Z_4, $o_COLETA_FATOR_CRISTA_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_DURACAO_5, $o_COLETA_ACELERACAO_X_5, $o_COLETA_ACELERACAO_Y_5, $o_COLETA_ACELERACAO_Z_5, $o_COLETA_VDV_X_5, $o_COLETA_VDV_Y_5, $o_COLETA_VDV_Z_5, $o_COLETA_FATOR_CRISTA_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_DURACAO_6, $o_COLETA_ACELERACAO_X_6, $o_COLETA_ACELERACAO_Y_6, $o_COLETA_ACELERACAO_Z_6, $o_COLETA_VDV_X_6, $o_COLETA_VDV_Y_6, $o_COLETA_VDV_Z_6, $o_COLETA_FATOR_CRISTA_6, $o_TEMPO_MEDICAO, $o_NR09_AREN, $o_NR09_VDVR, $o_NR15_AREN, $o_NR15_VDVR, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME_1, $o_IMG_ATIV_FILENAME_2, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			// Formata Unescape de Textareas
			$o_ANALISE_PARADIGMA_1 = unescape_string($o_ANALISE_PARADIGMA_1);
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PARADIGMA_2 = unescape_string($o_ANALISE_PARADIGMA_2);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PARADIGMA_3 = unescape_string($o_ANALISE_PARADIGMA_3);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PARADIGMA_4 = unescape_string($o_ANALISE_PARADIGMA_4);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PARADIGMA_5 = unescape_string($o_ANALISE_PARADIGMA_5);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PARADIGMA_6 = unescape_string($o_ANALISE_PARADIGMA_6);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			
			// Formata Datas Nulas
			if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
			if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
			if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
			if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
			if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
			if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
			if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_IDCLIENTE."|".$o_ANO."|".$o_MES."|".$o_PLANILHA_NUM."|".$o_UNIDADE_SITE."|".$o_DATA_ELABORACAO."|".$o_AREA."|".$o_SETOR."|".$o_GES."|".$o_CARGO_FUNCAO."|".$o_CBO."|".$o_ATIV_MACRO."|".$o_ANALISES."|".$o_ANALISE_AMOSTRA_1."|".$o_ANALISE_DATA_AMOSTRAGEM_1."|".$o_ANALISE_PARADIGMA_1."|".$o_ANALISE_TAREFA_EXEC_1."|".$o_ANALISE_AMOSTRA_2."|".$o_ANALISE_DATA_AMOSTRAGEM_2."|".$o_ANALISE_PARADIGMA_2."|".$o_ANALISE_TAREFA_EXEC_2."|".$o_ANALISE_AMOSTRA_3."|".$o_ANALISE_DATA_AMOSTRAGEM_3."|".$o_ANALISE_PARADIGMA_3."|".$o_ANALISE_TAREFA_EXEC_3."|".$o_ANALISE_AMOSTRA_4."|".$o_ANALISE_DATA_AMOSTRAGEM_4."|".$o_ANALISE_PARADIGMA_4."|".$o_ANALISE_TAREFA_EXEC_4."|".$o_ANALISE_AMOSTRA_5."|".$o_ANALISE_DATA_AMOSTRAGEM_5."|".$o_ANALISE_PARADIGMA_5."|".$o_ANALISE_TAREFA_EXEC_5."|".$o_ANALISE_AMOSTRA_6."|".$o_ANALISE_DATA_AMOSTRAGEM_6."|".$o_ANALISE_PARADIGMA_6."|".$o_ANALISE_TAREFA_EXEC_6."|".$o_AGENTE_RISCO."|".$o_JOR_TRAB."|".$o_TEMPO_EXPO."|".$o_TIPO_EXPO."|".$o_MEIO_PROPAG."|".$o_FONTE_GERADORA."|".$o_EPC."|".$o_FABR_EPI."|".$o_MOD_EPI."|".$o_CERT_APROV."|".$o_GRAU_ATENUACAO."|".$o_ACEL_1."|".$o_ACEL_MARCA_1."|".$o_ACEL_MODELO_1."|".$o_ACEL_SERIAL_1."|".$o_ACEL_CERT_CALIB_1."|".$o_ACEL_2."|".$o_ACEL_MARCA_2."|".$o_ACEL_MODELO_2."|".$o_ACEL_SERIAL_2."|".$o_ACEL_CERT_CALIB_2."|".$o_COLETAS."|".$o_COLETA_AMOSTRA_1."|".$o_COLETA_DATA_1."|".$o_COLETA_DURACAO_1."|".$o_COLETA_ACELERACAO_X_1."|".$o_COLETA_ACELERACAO_Y_1."|".$o_COLETA_ACELERACAO_Z_1."|".$o_COLETA_VDV_X_1."|".$o_COLETA_VDV_Y_1."|".$o_COLETA_VDV_Z_1."|".$o_COLETA_FATOR_CRISTA_1."|".$o_COLETA_AMOSTRA_2."|".$o_COLETA_DATA_2."|".$o_COLETA_DURACAO_2."|".$o_COLETA_ACELERACAO_X_2."|".$o_COLETA_ACELERACAO_Y_2."|".$o_COLETA_ACELERACAO_Z_2."|".$o_COLETA_VDV_X_2."|".$o_COLETA_VDV_Y_2."|".$o_COLETA_VDV_Z_2."|".$o_COLETA_FATOR_CRISTA_2."|".$o_COLETA_AMOSTRA_3."|".$o_COLETA_DATA_3."|".$o_COLETA_DURACAO_3."|".$o_COLETA_ACELERACAO_X_3."|".$o_COLETA_ACELERACAO_Y_3."|".$o_COLETA_ACELERACAO_Z_3."|".$o_COLETA_VDV_X_3."|".$o_COLETA_VDV_Y_3."|".$o_COLETA_VDV_Z_3."|".$o_COLETA_FATOR_CRISTA_3."|".$o_COLETA_AMOSTRA_4."|".$o_COLETA_DATA_4."|".$o_COLETA_DURACAO_4."|".$o_COLETA_ACELERACAO_X_4."|".$o_COLETA_ACELERACAO_Y_4."|".$o_COLETA_ACELERACAO_Z_4."|".$o_COLETA_VDV_X_4."|".$o_COLETA_VDV_Y_4."|".$o_COLETA_VDV_Z_4."|".$o_COLETA_FATOR_CRISTA_4."|".$o_COLETA_AMOSTRA_5."|".$o_COLETA_DATA_5."|".$o_COLETA_DURACAO_5."|".$o_COLETA_ACELERACAO_X_5."|".$o_COLETA_ACELERACAO_Y_5."|".$o_COLETA_ACELERACAO_Z_5."|".$o_COLETA_VDV_X_5."|".$o_COLETA_VDV_Y_5."|".$o_COLETA_VDV_Z_5."|".$o_COLETA_FATOR_CRISTA_5."|".$o_COLETA_AMOSTRA_6."|".$o_COLETA_DATA_6."|".$o_COLETA_DURACAO_6."|".$o_COLETA_ACELERACAO_X_6."|".$o_COLETA_ACELERACAO_Y_6."|".$o_COLETA_ACELERACAO_Z_6."|".$o_COLETA_VDV_X_6."|".$o_COLETA_VDV_Y_6."|".$o_COLETA_VDV_Z_6."|".$o_COLETA_FATOR_CRISTA_6."|".$o_TEMPO_MEDICAO."|".$o_NR09_AREN."|".$o_NR09_VDVR."|".$o_NR15_AREN."|".$o_NR15_VDVR."|".$o_RESP_CAMPO_IDCOLABORADOR."|".$o_RESP_TECNICO_IDCOLABORADOR."|".$o_REGISTRO_RC."|".$o_REGISTRO_RT."|".$o_IMG_ATIV_FILENAME_1."|".$o_IMG_ATIV_FILENAME_2."|".$o_LOGO_FILENAME."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVIBR_VCI` as id, upper(CLNT.`nome_interno`) as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_paradigma_1` as analise_paradigma_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_paradigma_2` as analise_paradigma_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_paradigma_3` as analise_paradigma_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_paradigma_4` as analise_paradigma_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_paradigma_5` as analise_paradigma_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_paradigma_6` as analise_paradigma_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`agente_risco` as agente_risco, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, AA.`fabr_epi` as fabr_epi, AA.`mod_epi` as mod_epi, AA.`cert_aprov` as cert_aprov, AA.`grau_atenuacao` as grau_atenuacao, AA.`acel_1` as acel_1, AA.`acel_marca_1` as acel_marca_1, AA.`acel_modelo_1` as acel_modelo_1, AA.`acel_serial_1` as acel_serial_1, AA.`acel_cert_calib_1` as acel_cert_calib_1, AA.`acel_2` as acel_2, AA.`acel_marca_2` as acel_marca_2, AA.`acel_modelo_2` as acel_modelo_2, AA.`acel_serial_2` as acel_serial_2, AA.`acel_cert_calib_2` as acel_cert_calib_2, AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_duracao_1` as coleta_duracao_1, AA.`coleta_aceleracao_x_1` as coleta_aceleracao_x_1, AA.`coleta_aceleracao_y_1` as coleta_aceleracao_y_1, AA.`coleta_aceleracao_z_1` as coleta_aceleracao_z_1, AA.`coleta_vdv_x_1` as coleta_vdv_x_1, AA.`coleta_vdv_y_1` as coleta_vdv_y_1, AA.`coleta_vdv_z_1` as coleta_vdv_z_1, AA.`coleta_fator_crista_1` as coleta_fator_crista_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_duracao_2` as coleta_duracao_2, AA.`coleta_aceleracao_x_2` as coleta_aceleracao_x_2, AA.`coleta_aceleracao_y_2` as coleta_aceleracao_y_2, AA.`coleta_aceleracao_z_2` as coleta_aceleracao_z_2, AA.`coleta_vdv_x_2` as coleta_vdv_x_2, AA.`coleta_vdv_y_2` as coleta_vdv_y_2, AA.`coleta_vdv_z_2` as coleta_vdv_z_2, AA.`coleta_fator_crista_2` as coleta_fator_crista_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_duracao_3` as coleta_duracao_3, AA.`coleta_aceleracao_x_3` as coleta_aceleracao_x_3, AA.`coleta_aceleracao_y_3` as coleta_aceleracao_y_3, AA.`coleta_aceleracao_z_3` as coleta_aceleracao_z_3, AA.`coleta_vdv_x_3` as coleta_vdv_x_3, AA.`coleta_vdv_y_3` as coleta_vdv_y_3, AA.`coleta_vdv_z_3` as coleta_vdv_z_3, AA.`coleta_fator_crista_3` as coleta_fator_crista_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_duracao_4` as coleta_duracao_4, AA.`coleta_aceleracao_x_4` as coleta_aceleracao_x_4, AA.`coleta_aceleracao_y_4` as coleta_aceleracao_y_4, AA.`coleta_aceleracao_z_4` as coleta_aceleracao_z_4, AA.`coleta_vdv_x_4` as coleta_vdv_x_4, AA.`coleta_vdv_y_4` as coleta_vdv_y_4, AA.`coleta_vdv_z_4` as coleta_vdv_z_4, AA.`coleta_fator_crista_4` as coleta_fator_crista_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_duracao_5` as coleta_duracao_5, AA.`coleta_aceleracao_x_5` as coleta_aceleracao_x_5, AA.`coleta_aceleracao_y_5` as coleta_aceleracao_y_5, AA.`coleta_aceleracao_z_5` as coleta_aceleracao_z_5, AA.`coleta_vdv_x_5` as coleta_vdv_x_5, AA.`coleta_vdv_y_5` as coleta_vdv_y_5, AA.`coleta_vdv_z_5` as coleta_vdv_z_5, AA.`coleta_fator_crista_5` as coleta_fator_crista_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_duracao_6` as coleta_duracao_6, AA.`coleta_aceleracao_x_6` as coleta_aceleracao_x_6, AA.`coleta_aceleracao_y_6` as coleta_aceleracao_y_6, AA.`coleta_aceleracao_z_6` as coleta_aceleracao_z_6, AA.`coleta_vdv_x_6` as coleta_vdv_x_6, AA.`coleta_vdv_y_6` as coleta_vdv_y_6, AA.`coleta_vdv_z_6` as coleta_vdv_z_6, AA.`coleta_fator_crista_6` as coleta_fator_crista_6, AA.`tempo_medicao` as tempo_medicao, AA.`nr09_aren` as nr09_aren, AA.`nr09_vdvr` as nr09_vdvr, AA.`nr15_aren` as nr15_aren, AA.`nr15_vdvr` as nr15_vdvr, 
            (SELECT concat(upper(UA1.`nome`),' ',upper(UA1.`sobrenome`),' (',lower(CLBRDR1.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA1
              WHERE UA1.`username` = CLBRDR1.`username`
              LIMIT 1
            ) as resp_campo,
            (SELECT concat(upper(UA2.`nome`),' ',upper(UA2.`sobrenome`),' (',lower(CLBRDR2.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA2
              WHERE UA2.`username` = CLBRDR2.`username`
              LIMIT 1
            ) as resp_tec,upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename_1`) as img_ativ_filename_1, lower(AA.`img_ativ_filename_2`) as img_ativ_filename_2, lower(AA.`logo_filename`) as logo_filename,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `VIBR_VCI` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
    LEFT JOIN `COLABORADOR` as CLBRDR1
           ON CLBRDR1.`idcolaborador` = AA.`resp_campo_idcolaborador`
    LEFT JOIN `COLABORADOR` as CLBRDR2
           ON CLBRDR2.`idcolaborador` = AA.`resp_tecnico_idcolaborador`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVIBR_VCI` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssssssssssss', $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_PARADIGMA_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_PARADIGMA_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_PARADIGMA_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_PARADIGMA_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_PARADIGMA_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_PARADIGMA_6, $o_ANALISE_TAREFA_EXEC_6, $o_AGENTE_RISCO, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_FABR_EPI, $o_MOD_EPI, $o_CERT_APROV, $o_GRAU_ATENUACAO, $o_ACEL_1, $o_ACEL_MARCA_1, $o_ACEL_MODELO_1, $o_ACEL_SERIAL_1, $o_ACEL_CERT_CALIB_1, $o_ACEL_2, $o_ACEL_MARCA_2, $o_ACEL_MODELO_2, $o_ACEL_SERIAL_2, $o_ACEL_CERT_CALIB_2, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_DURACAO_1, $o_COLETA_ACELERACAO_X_1, $o_COLETA_ACELERACAO_Y_1, $o_COLETA_ACELERACAO_Z_1, $o_COLETA_VDV_X_1, $o_COLETA_VDV_Y_1, $o_COLETA_VDV_Z_1, $o_COLETA_FATOR_CRISTA_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_DURACAO_2, $o_COLETA_ACELERACAO_X_2, $o_COLETA_ACELERACAO_Y_2, $o_COLETA_ACELERACAO_Z_2, $o_COLETA_VDV_X_2, $o_COLETA_VDV_Y_2, $o_COLETA_VDV_Z_2, $o_COLETA_FATOR_CRISTA_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_DURACAO_3, $o_COLETA_ACELERACAO_X_3, $o_COLETA_ACELERACAO_Y_3, $o_COLETA_ACELERACAO_Z_3, $o_COLETA_VDV_X_3, $o_COLETA_VDV_Y_3, $o_COLETA_VDV_Z_3, $o_COLETA_FATOR_CRISTA_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_DURACAO_4, $o_COLETA_ACELERACAO_X_4, $o_COLETA_ACELERACAO_Y_4, $o_COLETA_ACELERACAO_Z_4, $o_COLETA_VDV_X_4, $o_COLETA_VDV_Y_4, $o_COLETA_VDV_Z_4, $o_COLETA_FATOR_CRISTA_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_DURACAO_5, $o_COLETA_ACELERACAO_X_5, $o_COLETA_ACELERACAO_Y_5, $o_COLETA_ACELERACAO_Z_5, $o_COLETA_VDV_X_5, $o_COLETA_VDV_Y_5, $o_COLETA_VDV_Z_5, $o_COLETA_FATOR_CRISTA_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_DURACAO_6, $o_COLETA_ACELERACAO_X_6, $o_COLETA_ACELERACAO_Y_6, $o_COLETA_ACELERACAO_Z_6, $o_COLETA_VDV_X_6, $o_COLETA_VDV_Y_6, $o_COLETA_VDV_Z_6, $o_COLETA_FATOR_CRISTA_6, $o_TEMPO_MEDICAO, $o_NR09_AREN, $o_NR09_VDVR, $o_NR15_AREN, $o_NR15_VDVR, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME_1, $o_IMG_ATIV_FILENAME_2, $o_LOGO_FILENAME, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			$o_ANALISE_PARADIGMA_1 = unescape_string($o_ANALISE_PARADIGMA_1);
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PARADIGMA_2 = unescape_string($o_ANALISE_PARADIGMA_2);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PARADIGMA_3 = unescape_string($o_ANALISE_PARADIGMA_3);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PARADIGMA_4 = unescape_string($o_ANALISE_PARADIGMA_4);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PARADIGMA_5 = unescape_string($o_ANALISE_PARADIGMA_5);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PARADIGMA_6 = unescape_string($o_ANALISE_PARADIGMA_6);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PARADIGMA_1 = nl2br($o_ANALISE_PARADIGMA_1);
			$o_ANALISE_TAREFA_EXEC_1 = nl2br($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PARADIGMA_2 = nl2br($o_ANALISE_PARADIGMA_2);
			$o_ANALISE_TAREFA_EXEC_2 = nl2br($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PARADIGMA_3 = nl2br($o_ANALISE_PARADIGMA_3);
			$o_ANALISE_TAREFA_EXEC_3 = nl2br($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PARADIGMA_4 = nl2br($o_ANALISE_PARADIGMA_4);
			$o_ANALISE_TAREFA_EXEC_4 = nl2br($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PARADIGMA_5 = nl2br($o_ANALISE_PARADIGMA_5);
			$o_ANALISE_TAREFA_EXEC_5 = nl2br($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PARADIGMA_6 = nl2br($o_ANALISE_PARADIGMA_6);
			$o_ANALISE_TAREFA_EXEC_6 = nl2br($o_ANALISE_TAREFA_EXEC_6);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_VIBR_VCI_DEZ; }
				if( $o_ANALISES == "1"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_1; }
				if( $o_ANALISES == "2"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_2; }
				if( $o_ANALISES == "3"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_3; }
				if( $o_ANALISES == "4"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_4; }
				if( $o_ANALISES == "5"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_5; }
				if( $o_ANALISES == "6"){ $o_ANALISES_TXT = TXT_LAUDOS_VIBR_VCI_6; }
				if( $o_COLETAS == "1"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_1; }
				if( $o_COLETAS == "2"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_2; }
				if( $o_COLETAS == "3"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_3; }
				if( $o_COLETAS == "4"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_4; }
				if( $o_COLETAS == "5"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_5; }
				if( $o_COLETAS == "6"){ $o_COLETAS_TXT = TXT_LAUDOS_VIBR_VCI_6; }
				
				// Formata Datas Nulas
				if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
				if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
				if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
				if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
				if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
				if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
				if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_CLIENTE.':</b></th><td>'.$o_IDCLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_NUMERO_PLANILHA.':</b></th><td>'.$o_PLANILHA_NUM.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_UNIDADESITE.':</b></th><td>'.$o_UNIDADE_SITE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_DATA_ELABORACAO.':</b></th><td>'.$o_DATA_ELABORACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_AREA.':</b></th><td>'.$o_AREA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_SETOR.':</b></th><td>'.$o_SETOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_GES.':</b></th><td>'.$o_GES.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_CARGOFUNCAO.':</b></th><td>'.$o_CARGO_FUNCAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_CBO.':</b></th><td>'.$o_CBO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ATIVIDADE_MACRO.':</b></th><td>'.$o_ATIV_MACRO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISES.':</b></th><td>'.$o_ANALISES_TXT.'</td></tr>';
				
				# Formata Analises
				$o_ANALISES_1_SET = 0;
				$o_ANALISES_2_SET = 0;
				$o_ANALISES_3_SET = 0;
				$o_ANALISES_4_SET = 0;
				$o_ANALISES_5_SET = 0;
				$o_ANALISES_6_SET = 0;
				switch($o_ANALISES)
				{
					case '1':
						$o_ANALISES_1_SET = 1;
						break;
					case '2':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						break;
					case '3':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						break;
					case '4':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						break;
					case '5':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						break;
					case '6':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						$o_ANALISES_6_SET = 1;
						break;
					default:
						break;
				}
				
				if($o_ANALISES_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_1_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_1_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_1_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_1_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_1.'</td></tr>';
				}
				if($o_ANALISES_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_2_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_2_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_2_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_2_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_2.'</td></tr>';
				}
				if($o_ANALISES_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_3_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_3_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_3_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_3_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_3.'</td></tr>';
				}
				if($o_ANALISES_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_4_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_4_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_4_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_4_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_4.'</td></tr>';
				}
				if($o_ANALISES_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_5_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_5_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_5_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_5_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_5.'</td></tr>';
				}
				if($o_ANALISES_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_6_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_6_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_6_PARADIGMA.':</b></th><td>'.$o_ANALISE_PARADIGMA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ANALISE_6_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_6.'</td></tr>';
				}
				
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_AGENTE_DE_RISCO.':</b></th><td>'.$o_AGENTE_RISCO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_JORNADA_DE_TRABALHO.':</b></th><td>'.$o_JOR_TRAB.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_TEMPO_EXPOSICAO.':</b></th><td>'.$o_TEMPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_TIPO_EXPOSICAO.':</b></th><td>'.$o_TIPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_MEIO_DE_PROPAGACAO.':</b></th><td>'.$o_MEIO_PROPAG.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_FONTE_GERADORA.':</b></th><td>'.$o_FONTE_GERADORA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_EPC.':</b></th><td>'.$o_EPC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_FABRICANTE_EPI.':</b></th><td>'.$o_FABR_EPI.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_MODELO_EPI.':</b></th><td>'.$o_MOD_EPI.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_CERTIFICADO_DE_APROVACAO.':</b></th><td>'.$o_CERT_APROV.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_GRAU_DE_ATENUACAO.':</b></th><td>'.$o_GRAU_ATENUACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1.':</b></th><td>'.$o_ACEL_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_MARCA.':</b></th><td>'.$o_ACEL_MARCA_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_MODELO.':</b></th><td>'.$o_ACEL_MODELO_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_N_SERIAL.':</b></th><td>'.$o_ACEL_SERIAL_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_CERT_DE_CALIBRACAO.':</b></th><td>'.$o_ACEL_CERT_CALIB_1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2.':</b></th><td>'.$o_ACEL_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_MARCA.':</b></th><td>'.$o_ACEL_MARCA_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_MODELO.':</b></th><td>'.$o_ACEL_MODELO_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_N_SERIAL.':</b></th><td>'.$o_ACEL_SERIAL_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_CERT_DE_CALIBRACAO.':</b></th><td>'.$o_ACEL_CERT_CALIB_2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETAS.':</b></th><td>'.$o_COLETAS_TXT.'</td></tr>';
				
				# Formata Coletas
				$o_COLETAS_1_SET = 0;
				$o_COLETAS_2_SET = 0;
				$o_COLETAS_3_SET = 0;
				$o_COLETAS_4_SET = 0;
				$o_COLETAS_5_SET = 0;
				$o_COLETAS_6_SET = 0;
				switch($o_COLETAS)
				{
					case '1':
						$o_COLETAS_1_SET = 1;
						break;
					case '2':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						break;
					case '3':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						break;
					case '4':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						break;
					case '5':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						break;
					case '6':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						$o_COLETAS_6_SET = 1;
						break;
					default:
						break;
				}
				
				if($o_COLETAS_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_DATA.':</b></th><td>'.$o_COLETA_DATA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_1_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_1.'</td></tr>';
				}
				if($o_COLETAS_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_DATA.':</b></th><td>'.$o_COLETA_DATA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_2_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_2.'</td></tr>';
				}
				if($o_COLETAS_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_DATA.':</b></th><td>'.$o_COLETA_DATA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_3_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_3.'</td></tr>';
				}
				if($o_COLETAS_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_DATA.':</b></th><td>'.$o_COLETA_DATA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_4_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_4.'</td></tr>';
				}
				if($o_COLETAS_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_DATA.':</b></th><td>'.$o_COLETA_DATA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_5_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_5.'</td></tr>';
				}
				if($o_COLETAS_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_DATA.':</b></th><td>'.$o_COLETA_DATA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_DURACAO.':</b></th><td>'.$o_COLETA_DURACAO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_X.':</b></th><td>'.$o_COLETA_ACELERACAO_X_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_Y.':</b></th><td>'.$o_COLETA_ACELERACAO_Y_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_Z.':</b></th><td>'.$o_COLETA_ACELERACAO_Z_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_X.':</b></th><td>'.$o_COLETA_VDV_X_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_Y.':</b></th><td>'.$o_COLETA_VDV_Y_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_Z.':</b></th><td>'.$o_COLETA_VDV_Z_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_COLETA_6_FATOR_CRISTA.':</b></th><td>'.$o_COLETA_FATOR_CRISTA_6.'</td></tr>';
				}
				
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_TEMPO_DE_MEDICAO.':</b></th><td>'.$o_TEMPO_MEDICAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_NR09_AREN.':</b></th><td>'.$o_NR09_AREN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_NR09_VDVR.':</b></th><td>'.$o_NR09_VDVR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_NR15_AREN.':</b></th><td>'.$o_NR15_AREN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_NR15_VDVR.':</b></th><td>'.$o_NR15_VDVR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_CAMPO.':</b></th><td>'.$o_RESP_CAMPO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_TECNICO.':</b></th><td>'.$o_RESP_TECNICO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_CAMPO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_TECNICO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RT.'</td></tr>';
				if($o_IMG_ATIV_FILENAME_1){ $o_IMG_ATIV_FILENAME_1_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_IMG_ATIV_FILENAME_1.'" target="_blank">'.$o_IMG_ATIV_FILENAME_1.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_IMAGEM_ATIVIDADE_1.':</b></th><td>'.$o_IMG_ATIV_FILENAME_1_TXT.'</td></tr>';
				if($o_IMG_ATIV_FILENAME_2){ $o_IMG_ATIV_FILENAME_2_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_IMG_ATIV_FILENAME_2.'" target="_blank">'.$o_IMG_ATIV_FILENAME_2.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_IMAGEM_ATIVIDADE_2.':</b></th><td>'.$o_IMG_ATIV_FILENAME_2_TXT.'</td></tr>';
				if($o_LOGO_FILENAME){ $o_LOGO_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_LOGO_FILENAME.'" target="_blank">'.$o_LOGO_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VIBR_VCI_IMAGEM_LOGOMARCA.':</b></th><td>'.$o_LOGO_FILENAME_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
