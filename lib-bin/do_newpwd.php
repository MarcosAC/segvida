<?php
#################################################################################
## SEGVIDA
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: do_newpwd.php
## Função..........: Efetua a alteracao da senha de acesso
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_TOKEN = $IN_PASSWORD = "";
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_TOKEN      = test_input($_GET["token"]);
		$IN_PASSWORD   = test_input($_GET["password"]);
		
*/

		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_TOKEN      = test_input($_POST["t"]);
		$IN_PASSWORD   = test_input($_POST["p"]);
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_TOKEN) &&
				!empty($IN_PASSWORD)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			####
			# 1 - Obtem salt do token informado para gerar nova senha criptografada
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT U.`salt` as salt, U.`email` as email, upper(`username`) as username
				 FROM `SYSTEM_USER_ACCOUNT` U
				WHERE upper(U.`token_tmp`) = upper(?) LIMIT 1"
			)) 
			{
				$sql_TOKEN = $mysqli->escape_string($IN_TOKEN);
				//
				$stmt->bind_param('s', $sql_TOKEN);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_SALT, $o_EMAIL, $o_USERNAME);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0)
				{
					die("0|".TXT_RESETPWD_TOKEN_EXPIRADO."|alert|");
					exit;
				}
				else
				{ 
					$stmt->fetch();
					
					## faz o hash da senha com um salt excusivo.
					$tmp_password = hash('sha512', $IN_PASSWORD . $o_SALT);
					
					## Atualiza a senha da conta
					
					##Prepara query
					if ($stmt = $mysqli->prepare(
					"UPDATE `SYSTEM_USER_ACCOUNT` U 
					    SET U.`senha` = ?,
					        U.`token_tmp` = null
					  WHERE U.`token_tmp` = ?"
					)) 
					{
						$sql_PASSWORD = $mysqli->escape_string($tmp_password);
						$sql_TOKEN    = $mysqli->escape_string($IN_TOKEN);
						//
						$stmt->bind_param('ss', $sql_PASSWORD, $sql_TOKEN);
						$stmt->execute();
					}
					
					## Remove entradas que podem estar bloqueando a conta
					$now = time();
					
					// Todas as tentativas de login são contadas dentro do intervalo das últimas 2 horas. 
					$valid_attempts = $now - (2 * 60 * 60);
					
					##Prepara query
					if ($stmt = $mysqli->prepare(
					"DELETE FROM `LOGIN_TENTATIVAS` 
					  WHERE upper(`username`) = upper(?) 
					    AND time > '$valid_attempts'"
					)) 
					{
						//$tmp_password = $mysqli->escape_string($tmp_password);
						$stmt->bind_param('s', $o_USERNAME);
						$stmt->execute();
					}
					
					## Notifica da alteracao da senha bem sucedida
					
					## Create a new PHPMailer instance
					$mail = new PHPMailer(true);
					
					##Formata array com os dados
					$template_map = array(
						'{{IMG_URL}}'         => IMG_URL
					);
					
					
					
					##Carrega texto do email via template ja processado
					$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_NOVA_SENHA_CONCLUIDO,$template_map,$IN_LANG);
					
					$isMailSent = 0;
					
					try 
					{
						$mail->CharSet = 'UTF-8';
						$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
						$mail->AddAddress($o_EMAIL);//Set who the message is to be sent to
						
						//if(!empty(EMAIL_CONTATO_BCC))
						//{
						//	$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
						//}
						
						$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
						
						$mail->Subject = TXT_EMAIL_SUBJECT_SENHA_ALTERADA;//Set the subject line
						
						$mail->MsgHTML($body);
						
						if ($mail->send()) { $isMailSent = 1; }
					} 
					catch (phpmailerException $e) 
					{
						$MailError = $e->errorMessage();
					} 
					catch (Exception $e) 
					{
						$MailException = $e->getMessage();
					}
					
					//send the message, check for errors
					if (!$isMailSent) 
					{
						error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_NOVA_SENHA_CONCLUIDO."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
					}
					
					die("1|".TXT_RESETPWD_SENHA_ALTERADA_COM_SUCESSO."|success|");
					exit; 
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
