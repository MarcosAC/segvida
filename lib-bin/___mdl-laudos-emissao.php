<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-emissao.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 4/10/2017 14:31:57
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_ANO = $IN_MES = $IN_CLIENTE = $IN_LD_BIO_PLAN = $IN_LD_BIO_MOD = $IN_LD_CALOR_PLAN = $IN_LD_CALOR_MOD = $IN_LD_ELETR_PLAN = $IN_LD_ELETR_MOD = $IN_LD_EXPL_PLAN = $IN_LD_EXPL_MOD = $IN_LD_INFL_PLAN = $IN_LD_INFL_MOD = $IN_LD_PART_PLAN = $IN_LD_PART_MOD = $IN_LD_POEI_PLAN = $IN_LD_POEI_MOD = $IN_LD_RAD_PLAN = $IN_LD_RAD_MOD = $IN_LD_RIS_PLAN = $IN_LD_RIS_MOD = $IN_LD_RUI_PLAN = $IN_LD_RUI_MOD = $IN_LD_VAP_PLAN = $IN_LD_VAP_MOD = $IN_LD_VBRVCI_PLAN = $IN_LD_VBRVCI_MOD = $IN_LD_VBRVMB_PLAN = $IN_LD_VBRVMB_MOD = "";
	
	$IN_LD_BIO_SET = $IN_LD_CALOR_SET = $IN_LD_ELETR_SET = $IN_LD_EXPL_SET = $IN_LD_INFL_SET = $IN_LD_PART_SET = $IN_LD_POEI_SET = $IN_LD_RAD_SET = $IN_LD_RIS_SET = $IN_LD_RUI_SET = $IN_LD_VAP_SET = $IN_LD_VBRVCI_SET = $IN_LD_VBRVMB_SET = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_CLIENTE = test_input($_GET["cliente"]);
		$IN_LD_BIO_PLAN = test_input($_GET["ld_bio_plan"]);
		$IN_LD_BIO_MOD = test_input($_GET["ld_bio_mod"]);
		$IN_LD_CALOR_PLAN = test_input($_GET["ld_calor_plan"]);
		$IN_LD_CALOR_MOD = test_input($_GET["ld_calor_mod"]);
		$IN_LD_ELETR_PLAN = test_input($_GET["ld_eletr_plan"]);
		$IN_LD_ELETR_MOD = test_input($_GET["ld_eletr_mod"]);
		$IN_LD_EXPL_PLAN = test_input($_GET["ld_expl_plan"]);
		$IN_LD_EXPL_MOD = test_input($_GET["ld_expl_mod"]);
		$IN_LD_INFL_PLAN = test_input($_GET["ld_infl_plan"]);
		$IN_LD_INFL_MOD = test_input($_GET["ld_infl_mod"]);
		$IN_LD_PART_PLAN = test_input($_GET["ld_part_plan"]);
		$IN_LD_PART_MOD = test_input($_GET["ld_part_mod"]);
		$IN_LD_POEI_PLAN = test_input($_GET["ld_poei_plan"]);
		$IN_LD_POEI_MOD = test_input($_GET["ld_poei_mod"]);
		$IN_LD_RAD_PLAN = test_input($_GET["ld_rad_plan"]);
		$IN_LD_RAD_MOD = test_input($_GET["ld_rad_mod"]);
		$IN_LD_RUI_PLAN = test_input($_GET["ld_rui_plan"]);
		$IN_LD_RUI_MOD = test_input($_GET["ld_rui_mod"]);
		$IN_LD_VAP_PLAN = test_input($_GET["ld_vap_plan"]);
		$IN_LD_VAP_MOD = test_input($_GET["ld_vap_mod"]);
		$IN_LD_VBRVCI_PLAN = test_input($_GET["ld_vbrvci_plan"]);
		$IN_LD_VBRVCI_MOD = test_input($_GET["ld_vbrvci_mod"]);
		$IN_LD_VBRVMB_PLAN = test_input($_GET["ld_vbrvmb_plan"]);
		$IN_LD_VBRVMB_MOD = test_input($_GET["ld_vbrvmb_mod"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_CLIENTE = test_input($_POST["cliente"]);
		$IN_LD_BIO_SET     = test_input($_POST["ld_bio_set"]);
		$IN_LD_BIO_PLAN    = test_input($_POST["ld_bio_plan"]);
		$IN_LD_BIO_MOD     = test_input($_POST["ld_bio_mod"]);
		$IN_LD_CALOR_SET   = test_input($_POST["ld_calor_set"]);
		$IN_LD_CALOR_PLAN  = test_input($_POST["ld_calor_plan"]);
		$IN_LD_CALOR_MOD   = test_input($_POST["ld_calor_mod"]);
		$IN_LD_ELETR_SET   = test_input($_POST["ld_eletr_set"]);
		$IN_LD_ELETR_PLAN  = test_input($_POST["ld_eletr_plan"]);
		$IN_LD_ELETR_MOD   = test_input($_POST["ld_eletr_mod"]);
		$IN_LD_EXPL_SET    = test_input($_POST["ld_expl_set"]);
		$IN_LD_EXPL_PLAN   = test_input($_POST["ld_expl_plan"]);
		$IN_LD_EXPL_MOD    = test_input($_POST["ld_expl_mod"]);
		$IN_LD_INFL_SET    = test_input($_POST["ld_infl_set"]);
		$IN_LD_INFL_PLAN   = test_input($_POST["ld_infl_plan"]);
		$IN_LD_INFL_MOD    = test_input($_POST["ld_infl_mod"]);
		$IN_LD_PART_SET    = test_input($_POST["ld_part_set"]);
		$IN_LD_PART_PLAN   = test_input($_POST["ld_part_plan"]);
		$IN_LD_PART_MOD    = test_input($_POST["ld_part_mod"]);
		$IN_LD_POEI_SET    = test_input($_POST["ld_poei_set"]);
		$IN_LD_POEI_PLAN   = test_input($_POST["ld_poei_plan"]);
		$IN_LD_POEI_MOD    = test_input($_POST["ld_poei_mod"]);
		$IN_LD_RAD_SET     = test_input($_POST["ld_rad_set"]);
		$IN_LD_RAD_PLAN    = test_input($_POST["ld_rad_plan"]);
		$IN_LD_RAD_MOD     = test_input($_POST["ld_rad_mod"]);
		$IN_LD_RIS_SET     = test_input($_POST["ld_ris_set"]);
		$IN_LD_RIS_PLAN    = test_input($_POST["ld_ris_plan"]);
		$IN_LD_RIS_MOD     = test_input($_POST["ld_ris_mod"]);
		$IN_LD_RUI_SET     = test_input($_POST["ld_rui_set"]);
		$IN_LD_RUI_PLAN    = test_input($_POST["ld_rui_plan"]);
		$IN_LD_RUI_MOD     = test_input($_POST["ld_rui_mod"]);
		$IN_LD_VAP_SET     = test_input($_POST["ld_vap_set"]);
		$IN_LD_VAP_PLAN    = test_input($_POST["ld_vap_plan"]);
		$IN_LD_VAP_MOD     = test_input($_POST["ld_vap_mod"]);
		$IN_LD_VBRVCI_SET  = test_input($_POST["ld_vbrvci_set"]);
		$IN_LD_VBRVCI_PLAN = test_input($_POST["ld_vbrvci_plan"]);
		$IN_LD_VBRVCI_MOD  = test_input($_POST["ld_vbrvci_mod"]);
		$IN_LD_VBRVMB_SET  = test_input($_POST["ld_vbrvmb_set"]);
		$IN_LD_VBRVMB_PLAN = test_input($_POST["ld_vbrvmb_plan"]);
		$IN_LD_VBRVMB_MOD  = test_input($_POST["ld_vbrvmb_mod"]);
		
		## case convertion
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## GERAR LAUDO FINAL
			case '8':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_GERAR($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_SET, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_SET, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_SET, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_SET, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_SET, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_SET, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_SET, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_SET, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RIS_SET, $IN_LD_RIS_PLAN, $IN_LD_RIS_MOD, $IN_LD_RUI_SET, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_SET, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_SET, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_SET, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_CLIENTE) || isEmpty($IN_LD_BIO_PLAN) || isEmpty($IN_LD_BIO_MOD) || isEmpty($IN_LD_CALOR_PLAN) || isEmpty($IN_LD_CALOR_MOD) || isEmpty($IN_LD_ELETR_PLAN) || isEmpty($IN_LD_ELETR_MOD) || isEmpty($IN_LD_EXPL_PLAN) || isEmpty($IN_LD_EXPL_MOD) || isEmpty($IN_LD_INFL_PLAN) || isEmpty($IN_LD_INFL_MOD) || isEmpty($IN_LD_PART_PLAN) || isEmpty($IN_LD_PART_MOD) || isEmpty($IN_LD_POEI_PLAN) || isEmpty($IN_LD_POEI_MOD) || isEmpty($IN_LD_RAD_PLAN) || isEmpty($IN_LD_RAD_MOD) || isEmpty($IN_LD_RUI_PLAN) || isEmpty($IN_LD_RUI_MOD) || isEmpty($IN_LD_VAP_PLAN) || isEmpty($IN_LD_VAP_MOD) || isEmpty($IN_LD_VBRVCI_PLAN) || isEmpty($IN_LD_VBRVCI_MOD) || isEmpty($IN_LD_VBRVMB_PLAN) || isEmpty($IN_LD_VBRVMB_MOD) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_CLIENTE) || isEmpty($IN_LD_BIO_PLAN) || isEmpty($IN_LD_BIO_MOD) || isEmpty($IN_LD_CALOR_PLAN) || isEmpty($IN_LD_CALOR_MOD) || isEmpty($IN_LD_ELETR_PLAN) || isEmpty($IN_LD_ELETR_MOD) || isEmpty($IN_LD_EXPL_PLAN) || isEmpty($IN_LD_EXPL_MOD) || isEmpty($IN_LD_INFL_PLAN) || isEmpty($IN_LD_INFL_MOD) || isEmpty($IN_LD_PART_PLAN) || isEmpty($IN_LD_PART_MOD) || isEmpty($IN_LD_POEI_PLAN) || isEmpty($IN_LD_POEI_MOD) || isEmpty($IN_LD_RAD_PLAN) || isEmpty($IN_LD_RAD_MOD) || isEmpty($IN_LD_RUI_PLAN) || isEmpty($IN_LD_RUI_MOD) || isEmpty($IN_LD_VAP_PLAN) || isEmpty($IN_LD_VAP_MOD) || isEmpty($IN_LD_VBRVCI_PLAN) || isEmpty($IN_LD_VBRVCI_MOD) || isEmpty($IN_LD_VBRVMB_PLAN) || isEmpty($IN_LD_VBRVMB_MOD) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# GERA LAUDO FINAL
	####
	function executa_GERAR($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_SET, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_SET, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_SET, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_SET, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_SET, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_SET, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_SET, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_SET, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RIS_SET, $_LD_RIS_PLAN, $_LD_RIS_MOD, $_LD_RUI_SET, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_SET, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_SET, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_SET, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_DDMMAAAA = '%d/%m/%Y';
		//$sql_LD_BIO_PLAN    = $mysqli->escape_String($_LD_BIO_PLAN);
		//$sql_LD_BIO_MOD     = $mysqli->escape_String($_LD_BIO_MOD);
		//$sql_LD_CALOR_PLAN  = $mysqli->escape_String($_LD_CALOR_PLAN);
		//$sql_LD_CALOR_MOD   = $mysqli->escape_String($_LD_CALOR_MOD);
		//$sql_LD_ELETR_PLAN  = $mysqli->escape_String($_LD_ELETR_PLAN);
		//$sql_LD_ELETR_MOD   = $mysqli->escape_String($_LD_ELETR_MOD);
		//$sql_LD_EXPL_PLAN   = $mysqli->escape_String($_LD_EXPL_PLAN);
		//$sql_LD_EXPL_MOD    = $mysqli->escape_String($_LD_EXPL_MOD);
		//$sql_LD_INFL_PLAN   = $mysqli->escape_String($_LD_INFL_PLAN);
		//$sql_LD_INFL_MOD    = $mysqli->escape_String($_LD_INFL_MOD);
		//$sql_LD_PART_PLAN   = $mysqli->escape_String($_LD_PART_PLAN);
		//$sql_LD_PART_MOD    = $mysqli->escape_String($_LD_PART_MOD);
		//$sql_LD_POEI_PLAN   = $mysqli->escape_String($_LD_POEI_PLAN);
		//$sql_LD_POEI_MOD    = $mysqli->escape_String($_LD_POEI_MOD);
		//$sql_LD_RAD_PLAN    = $mysqli->escape_String($_LD_RAD_PLAN);
		//$sql_LD_RAD_MOD     = $mysqli->escape_String($_LD_RAD_MOD);
		//$sql_LD_RIS_PLAN    = $mysqli->escape_String($_LD_RIS_PLAN);
		//$sql_LD_RIS_MOD     = $mysqli->escape_String($_LD_RIS_MOD);
		//$sql_LD_RUI_PLAN    = $mysqli->escape_String($_LD_RUI_PLAN);
		//$sql_LD_RUI_MOD     = $mysqli->escape_String($_LD_RUI_MOD);
		//$sql_LD_VAP_PLAN    = $mysqli->escape_String($_LD_VAP_PLAN);
		//$sql_LD_VAP_MOD     = $mysqli->escape_String($_LD_VAP_MOD);
		//$sql_LD_VBRVCI_PLAN = $mysqli->escape_String($_LD_VBRVCI_PLAN);
		//$sql_LD_VBRVCI_MOD  = $mysqli->escape_String($_LD_VBRVCI_MOD);
		//$sql_LD_VBRVMB_PLAN = $mysqli->escape_String($_LD_VBRVMB_PLAN);
		//$sql_LD_VBRVMB_MOD  = $mysqli->escape_String($_LD_VBRVMB_MOD);
		
		####
		# PROCESSA LAUDOS
		####
		$RET_MSG = array(
			"bio_msg"     => "",
			"bio_file"    => "",
			"calor_msg"   => "",
			"calor_file"  => "",
			"eletr_msg"   => "",
			"eletr_file"  => "",
			"expl_msg"    => "",
			"expl_file"   => "",
			"infl_msg"    => "",
			"infl_file"   => "",
			"part_msg"    => "",
			"part_file"   => "",
			"poei_msg"    => "",
			"poei_file"   => "",
			"rad_msg"     => "",
			"rad_file"    => "",
			"ris_msg"     => "",
			"ris_file"    => "",
			"rui_msg"     => "",
			"rui_file"    => "",
			"vap_msg"     => "",
			"vap_file"    => "",
			"vbrvci_msg"  => "",
			"vbrvci_file" => "",
			"vbrvmb_msg"  => "",
			"vbrvmb_file" => ""
		);
		
		## Image Type
		/*
		$_img_type = array(
			"1" => "gif",
			"2" => "jpg",
			"3" => "png"
		);
		*/
		
		/** Include PHPExcel */
		require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
		
		$LAUDOS_COUNT=0;
		
		####
		# BIOLOGICO
		####
		if($_LD_BIO_SET)
		{
			$LAUDOS_COUNT++;
			$sql_LD_BIO_PLAN    = $mysqli->escape_String($_LD_BIO_PLAN);
			$sql_LD_BIO_MOD     = $mysqli->escape_String($_LD_BIO_MOD);
			
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_BIO_MOD);
			
			## Carrega dados do laudo para a planilha indicada
			if ($stmt = $mysqli->prepare(
			"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, 
              AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, 
              AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
              AA.`tarefa_exec` as tarefa_exec, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, 
              AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epi` as epi, AA.`epc` as epc, 
              AA.`enq_nr14` as enq_nr14, 
              concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
              concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
              upper(AA.`registro_rc`) as registro_rc, 
              upper(AA.`registro_rt`) as registro_rt, 
              lower(AA.`img_ativ_filename`) as img_ativ_filename, 
              lower(AA.`logo_filename`) as logo_filename
         FROM `BIOLOGICO` AA
   INNER JOIN `CLIENTE` C
           ON C.`idCLIENTE` = AA.`idCLIENTE`
   INNER JOIN `COLABORADOR` CLB1
           ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
   INNER JOIN `SYSTEM_USER_ACCOUNT` U1
           ON U1.`username` = CLB1.`username`
   INNER JOIN `COLABORADOR` CLB2
           ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
   INNER JOIN `SYSTEM_USER_ACCOUNT` U2
           ON U2.`username` = CLB2.`username`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
          AND AA.`idBIOLOGICO` = ?
          LIMIT 1"
			)) 
			{
				$stmt->bind_param('sss', $sql_DDMMAAAA, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_BIO_PLAN);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_TAREFA_EXEC, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPI, $o_EPC, $o_ENQ_NR14, $o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
				$stmt->fetch();
				
				// Formata Unescape de Textareas
				$o_TAREFA_EXEC = unescape_string($o_TAREFA_EXEC);
				
				// Formata Datas Nulas
				if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$RET_MSG['bio_msg']  = "Nenhum registro encontrado!";
					$RET_MSG['bio_file'] = "";
				}
				else
				{
					# Gera Planilha
					
					# Create new PHPExcel object
					//$objPHPExcel = new PHPExcel();
					//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
					$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
					
					$locale = 'pt_br';
					$validLocale = PHPExcel_Settings::setLocale($locale);
					
					# Set document properties
					$objPHPExcel->getProperties()
					            ->setCreator("SEGVIDA")
					            ->setLastModifiedBy("SEGVIDA")
					            ->setTitle("Laudo Biológico")
					            ->setSubject("")
					            ->setDescription("Laudo Biológico")
					            ->setKeywords("segvida laudo biológico")
					            ->setCategory("Laudo");
					
					## IMAGEM LOGOMARCA
					list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
					
					$img_logo_type2 = $_img_type[$img_logo_type];
					$img_logo_width_points  = $img_logo_width * 0.75;
					$img_logo_height_points = $img_logo_height * 0.75;
					
					error_log("mdl-laudos-emissao.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
					
					switch($img_logo_type)
					{
						case '1'://gif
							$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
							break;
						case '2'://jpg
							$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
							break;
						case '3'://png
						default:
							$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
							break;
					}
					//$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
					
					// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
					$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
					$objDrawing->setName('img_logo');
					$objDrawing->setDescription('img_logo');
					$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
					$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
					$objDrawing->setResizeProportional(false);
					$objDrawing->setWidth($img_logo_width_points);
					$objDrawing->setHeight($img_logo_height_points);
					//$objDrawing->setOffsetX(110);
					$objDrawing->setCoordinates('A1');
					$objDrawing->setImageResource($gd_img_logo);
					$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					
					
					## EMPRESA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J1', $o_CLIENTE);
					
					## PLANILHA_NUM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('V1', $o_PLANILHA_NUM);
					
					## UNIDADE
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J2', $o_UNIDADE_SITE);
					
					## DATA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('V2', $o_DATA_ELABORACAO);
					
					## AREA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J3', $o_AREA);
					
					## SETOR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('O3', $o_SETOR);
					
					## GES
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('V3', $o_GES);
					
					## CARGO_FUNCAO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J4', $o_CARGO_FUNCAO);
					
					## CBO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('V4', $o_CBO);
					
					## ATIV_MACRO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A6', $o_ATIV_MACRO);
					
					## TAREFA_EXEC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A11', $o_TAREFA_EXEC);
					
					## JOR_TRAB
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A19', $o_JOR_TRAB);
					
					## TEMPO_EXPO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D19', $o_TEMPO_EXPO);
					
					## TIPO_EXPO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('G19', $o_TIPO_EXPO);
					
					## MEIO_PROPAG
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J19', $o_MEIO_PROPAG);
					
					## FONTE_GERADORA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('M19', $o_FONTE_GERADORA);
					
					## EPI
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('F21', $o_EPI);
					
					## EPC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('F22', $o_EPC);
					
					## ENQ_NR14
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A24', $o_ENQ_NR14);
					
					## RESP_CAMPO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D35', $o_RESP_CAMPO);
					
					## RESP_TECNICO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('P35', $o_RESP_TECNICO);
					
					## REGISTRO_RC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D36', $o_REGISTRO_RC);
					
					## REGISTRO_RT
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('P36', $o_REGISTRO_RT);
					
					//$o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, 
					//$o_TAREFA_EXEC, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPI, $o_EPC, $o_ENQ_NR14, 
					//$o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, 
					//$o_LOGO_FILENAME
					
					
					## IMAGEM ATIVIDADE
					list($img_ativ_width, $img_ativ_height, $img_ativ_type, $img_ativ_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
					
					$img_ativ_width_points  = $img_ativ_width * 0.75;
					$img_ativ_height_points = $img_ativ_height * 0.75;
					
					switch($img_ativ_type)
					{
						case '1'://gif
							$gd_img_ativ = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
							break;
						case '2'://jpg
							$gd_img_ativ = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
							break;
						case '3'://png
						default:
							$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
							break;
					}
					//$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
					
					// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
					$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
					$objDrawing->setName('img_ativ');
					$objDrawing->setDescription('img_ativ');
					$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
					$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
					$objDrawing->setResizeProportional(false);
					$objDrawing->setWidth($img_ativ_width_points);
					$objDrawing->setHeight($img_ativ_height_points);
					//$objDrawing->setOffsetX(110);
					$objDrawing->setCoordinates('R11');
					$objDrawing->setImageResource($gd_img_ativ);
					$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
					
					
					
					
					
					//$objPHPExcel->getActiveSheet()
					//            ->getStyle('A'.$i)
					//            ->getAlignment()
					//            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
					//            ->setWrapText(true);
					
					// Rename worksheet
					$objPHPExcel->getActiveSheet()->setTitle('Laudo Biológico');
					
					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
					
					
					// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
					PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
					
					## Grava arquivo
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
					
					//$link = LAUDO_FINAL_URL."/".$_FILENAME;
					
					//$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo XLSX.</b></a>";
					
					$RET_MSG['bio_msg']  = "";
					$RET_MSG['bio_file'] = $_FILENAME;
					
				}
			}
			else
			{
				if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
				      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
				exit;
			}
		}
		
		
		$link = LAUDO_FINAL_URL."/".$RET_MSG['bio_file'];
		
		//$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo XLSX.</b></a>";
		
		if($LAUDOS_COUNT == 1){ $_MSG = "Laudo Final gerado com sucesso:"; }
		else if($LAUDOS_COUNT > 1){ $_MSG = "Laudos Finais gerados com sucesso:"; }
		
		return "1|".$_MSG." <a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Baixar Arquivo</b></a>|success|";
		exit;
		
	}
	
	function getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT lower(AA.`arquivo_filename`) as arquivo_filename
       FROM `PLANILHA_MODELO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idPLANILHA_MODELO` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ARQUIVO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			/*
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_LAUDO."|".$o_ARQUIVO_FILENAME."|";
				exit;
			}
			*/
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		return $o_ARQUIVO_FILENAME;
		//exit;
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_LD_BIO_PLAN = $mysqli->escape_String($_LD_BIO_PLAN);
		$sql_LD_BIO_MOD = $mysqli->escape_String($_LD_BIO_MOD);
		$sql_LD_CALOR_PLAN = $mysqli->escape_String($_LD_CALOR_PLAN);
		$sql_LD_CALOR_MOD = $mysqli->escape_String($_LD_CALOR_MOD);
		$sql_LD_ELETR_PLAN = $mysqli->escape_String($_LD_ELETR_PLAN);
		$sql_LD_ELETR_MOD = $mysqli->escape_String($_LD_ELETR_MOD);
		$sql_LD_EXPL_PLAN = $mysqli->escape_String($_LD_EXPL_PLAN);
		$sql_LD_EXPL_MOD = $mysqli->escape_String($_LD_EXPL_MOD);
		$sql_LD_INFL_PLAN = $mysqli->escape_String($_LD_INFL_PLAN);
		$sql_LD_INFL_MOD = $mysqli->escape_String($_LD_INFL_MOD);
		$sql_LD_PART_PLAN = $mysqli->escape_String($_LD_PART_PLAN);
		$sql_LD_PART_MOD = $mysqli->escape_String($_LD_PART_MOD);
		$sql_LD_POEI_PLAN = $mysqli->escape_String($_LD_POEI_PLAN);
		$sql_LD_POEI_MOD = $mysqli->escape_String($_LD_POEI_MOD);
		$sql_LD_RAD_PLAN = $mysqli->escape_String($_LD_RAD_PLAN);
		$sql_LD_RAD_MOD = $mysqli->escape_String($_LD_RAD_MOD);
		$sql_LD_RUI_PLAN = $mysqli->escape_String($_LD_RUI_PLAN);
		$sql_LD_RUI_MOD = $mysqli->escape_String($_LD_RUI_MOD);
		$sql_LD_VAP_PLAN = $mysqli->escape_String($_LD_VAP_PLAN);
		$sql_LD_VAP_MOD = $mysqli->escape_String($_LD_VAP_MOD);
		$sql_LD_VBRVCI_PLAN = $mysqli->escape_String($_LD_VBRVCI_PLAN);
		$sql_LD_VBRVCI_MOD = $mysqli->escape_String($_LD_VBRVCI_MOD);
		$sql_LD_VBRVMB_PLAN = $mysqli->escape_String($_LD_VBRVMB_PLAN);
		$sql_LD_VBRVMB_MOD = $mysqli->escape_String($_LD_VBRVMB_MOD);
		
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
"
		)) 
		{
			$stmt->bind_param('s', $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		if($_DEBUG == 1)
		{
			//$SQL = "";
			//($statement = $mysqli->prepare($SQL)) or trigger_error($mysqli->error, E_USER_ERROR);
			//$statement->execute() or trigger_error($statement->error, E_USER_ERROR);
		}
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `EMISSAO` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `ano` = ?
		        ,`mes` = ?
		        ,`cliente` = ?
		        ,`ld_bio_plan` = ?
		        ,`ld_bio_mod` = ?
		        ,`ld_calor_plan` = ?
		        ,`ld_calor_mod` = ?
		        ,`ld_eletr_plan` = ?
		        ,`ld_eletr_mod` = ?
		        ,`ld_expl_plan` = ?
		        ,`ld_expl_mod` = ?
		        ,`ld_infl_plan` = ?
		        ,`ld_infl_mod` = ?
		        ,`ld_part_plan` = ?
		        ,`ld_part_mod` = ?
		        ,`ld_poei_plan` = ?
		        ,`ld_poei_mod` = ?
		        ,`ld_rad_plan` = ?
		        ,`ld_rad_mod` = ?
		        ,`ld_rui_plan` = ?
		        ,`ld_rui_mod` = ?
		        ,`ld_vap_plan` = ?
		        ,`ld_vap_mod` = ?
		        ,`ld_vbrvci_plan` = ?
		        ,`ld_vbrvci_mod` = ?
		        ,`ld_vbrvmb_plan` = ?
		        ,`ld_vbrvmb_mod` = ?
"
		)) 
		{
			$stmt->bind_param('sssdddddddddddddddddddddddddd', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_CLIENTE, $sql_LD_BIO_PLAN, $sql_LD_BIO_MOD, $sql_LD_CALOR_PLAN, $sql_LD_CALOR_MOD, $sql_LD_ELETR_PLAN, $sql_LD_ELETR_MOD, $sql_LD_EXPL_PLAN, $sql_LD_EXPL_MOD, $sql_LD_INFL_PLAN, $sql_LD_INFL_MOD, $sql_LD_PART_PLAN, $sql_LD_PART_MOD, $sql_LD_POEI_PLAN, $sql_LD_POEI_MOD, $sql_LD_RAD_PLAN, $sql_LD_RAD_MOD, $sql_LD_RUI_PLAN, $sql_LD_RUI_MOD, $sql_LD_VAP_PLAN, $sql_LD_VAP_MOD, $sql_LD_VBRVCI_PLAN, $sql_LD_VBRVCI_MOD, $sql_LD_VBRVMB_PLAN, $sql_LD_VBRVMB_MOD);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_LD_BIO_PLAN = $mysqli->escape_String($_LD_BIO_PLAN);
		$sql_LD_BIO_MOD = $mysqli->escape_String($_LD_BIO_MOD);
		$sql_LD_CALOR_PLAN = $mysqli->escape_String($_LD_CALOR_PLAN);
		$sql_LD_CALOR_MOD = $mysqli->escape_String($_LD_CALOR_MOD);
		$sql_LD_ELETR_PLAN = $mysqli->escape_String($_LD_ELETR_PLAN);
		$sql_LD_ELETR_MOD = $mysqli->escape_String($_LD_ELETR_MOD);
		$sql_LD_EXPL_PLAN = $mysqli->escape_String($_LD_EXPL_PLAN);
		$sql_LD_EXPL_MOD = $mysqli->escape_String($_LD_EXPL_MOD);
		$sql_LD_INFL_PLAN = $mysqli->escape_String($_LD_INFL_PLAN);
		$sql_LD_INFL_MOD = $mysqli->escape_String($_LD_INFL_MOD);
		$sql_LD_PART_PLAN = $mysqli->escape_String($_LD_PART_PLAN);
		$sql_LD_PART_MOD = $mysqli->escape_String($_LD_PART_MOD);
		$sql_LD_POEI_PLAN = $mysqli->escape_String($_LD_POEI_PLAN);
		$sql_LD_POEI_MOD = $mysqli->escape_String($_LD_POEI_MOD);
		$sql_LD_RAD_PLAN = $mysqli->escape_String($_LD_RAD_PLAN);
		$sql_LD_RAD_MOD = $mysqli->escape_String($_LD_RAD_MOD);
		$sql_LD_RUI_PLAN = $mysqli->escape_String($_LD_RUI_PLAN);
		$sql_LD_RUI_MOD = $mysqli->escape_String($_LD_RUI_MOD);
		$sql_LD_VAP_PLAN = $mysqli->escape_String($_LD_VAP_PLAN);
		$sql_LD_VAP_MOD = $mysqli->escape_String($_LD_VAP_MOD);
		$sql_LD_VBRVCI_PLAN = $mysqli->escape_String($_LD_VBRVCI_PLAN);
		$sql_LD_VBRVCI_MOD = $mysqli->escape_String($_LD_VBRVCI_MOD);
		$sql_LD_VBRVMB_PLAN = $mysqli->escape_String($_LD_VBRVMB_PLAN);
		$sql_LD_VBRVMB_MOD = $mysqli->escape_String($_LD_VBRVMB_MOD);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idEMISSAO);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Verifica se o registro editado ja existe
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` != ?"
		)) 
		{
			$stmt->bind_param('sd', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua Atualizacao
		if ($stmt = $mysqli->prepare(
		"UPDATE `EMISSAO` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `ano` = ?
		        ,`mes` = ?
		        ,`cliente` = ?
		        ,`ld_bio_plan` = ?
		        ,`ld_bio_mod` = ?
		        ,`ld_calor_plan` = ?
		        ,`ld_calor_mod` = ?
		        ,`ld_eletr_plan` = ?
		        ,`ld_eletr_mod` = ?
		        ,`ld_expl_plan` = ?
		        ,`ld_expl_mod` = ?
		        ,`ld_infl_plan` = ?
		        ,`ld_infl_mod` = ?
		        ,`ld_part_plan` = ?
		        ,`ld_part_mod` = ?
		        ,`ld_poei_plan` = ?
		        ,`ld_poei_mod` = ?
		        ,`ld_rad_plan` = ?
		        ,`ld_rad_mod` = ?
		        ,`ld_rui_plan` = ?
		        ,`ld_rui_mod` = ?
		        ,`ld_vap_plan` = ?
		        ,`ld_vap_mod` = ?
		        ,`ld_vbrvci_plan` = ?
		        ,`ld_vbrvci_mod` = ?
		        ,`ld_vbrvmb_plan` = ?
		        ,`ld_vbrvmb_mod` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idEMISSAO` = ?"
		)) 
		{
			$stmt->bind_param('ssddddddddddddddddddddddddddss', $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_CLIENTE, $sql_LD_BIO_PLAN, $sql_LD_BIO_MOD, $sql_LD_CALOR_PLAN, $sql_LD_CALOR_MOD, $sql_LD_ELETR_PLAN, $sql_LD_ELETR_MOD, $sql_LD_EXPL_PLAN, $sql_LD_EXPL_MOD, $sql_LD_INFL_PLAN, $sql_LD_INFL_MOD, $sql_LD_PART_PLAN, $sql_LD_PART_MOD, $sql_LD_POEI_PLAN, $sql_LD_POEI_MOD, $sql_LD_RAD_PLAN, $sql_LD_RAD_MOD, $sql_LD_RUI_PLAN, $sql_LD_RUI_MOD, $sql_LD_VAP_PLAN, $sql_LD_VAP_MOD, $sql_LD_VBRVCI_PLAN, $sql_LD_VBRVCI_MOD, $sql_LD_VBRVMB_PLAN, $sql_LD_VBRVMB_MOD, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `EMISSAO` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idEMISSAO` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id, AA.`ano` as ano, AA.`mes` as mes, AA.`cliente` as cliente, AA.`ld_bio_plan` as ld_bio_plan, AA.`ld_bio_mod` as ld_bio_mod, AA.`ld_calor_plan` as ld_calor_plan, AA.`ld_calor_mod` as ld_calor_mod, AA.`ld_eletr_plan` as ld_eletr_plan, AA.`ld_eletr_mod` as ld_eletr_mod, AA.`ld_expl_plan` as ld_expl_plan, AA.`ld_expl_mod` as ld_expl_mod, AA.`ld_infl_plan` as ld_infl_plan, AA.`ld_infl_mod` as ld_infl_mod, AA.`ld_part_plan` as ld_part_plan, AA.`ld_part_mod` as ld_part_mod, AA.`ld_poei_plan` as ld_poei_plan, AA.`ld_poei_mod` as ld_poei_mod, AA.`ld_rad_plan` as ld_rad_plan, AA.`ld_rad_mod` as ld_rad_mod, AA.`ld_rui_plan` as ld_rui_plan, AA.`ld_rui_mod` as ld_rui_mod, AA.`ld_vap_plan` as ld_vap_plan, AA.`ld_vap_mod` as ld_vap_mod, AA.`ld_vbrvci_plan` as ld_vbrvci_plan, AA.`ld_vbrvci_mod` as ld_vbrvci_mod, AA.`ld_vbrvmb_plan` as ld_vbrvmb_plan, AA.`ld_vbrvmb_mod` as ld_vbrvmb_mod
       FROM `EMISSAO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_CLIENTE, $o_LD_BIO_PLAN, $o_LD_BIO_MOD, $o_LD_CALOR_PLAN, $o_LD_CALOR_MOD, $o_LD_ELETR_PLAN, $o_LD_ELETR_MOD, $o_LD_EXPL_PLAN, $o_LD_EXPL_MOD, $o_LD_INFL_PLAN, $o_LD_INFL_MOD, $o_LD_PART_PLAN, $o_LD_PART_MOD, $o_LD_POEI_PLAN, $o_LD_POEI_MOD, $o_LD_RAD_PLAN, $o_LD_RAD_MOD, $o_LD_RUI_PLAN, $o_LD_RUI_MOD, $o_LD_VAP_PLAN, $o_LD_VAP_MOD, $o_LD_VBRVCI_PLAN, $o_LD_VBRVCI_MOD, $o_LD_VBRVMB_PLAN, $o_LD_VBRVMB_MOD);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_CLIENTE."|".$o_LD_BIO_PLAN."|".$o_LD_BIO_MOD."|".$o_LD_CALOR_PLAN."|".$o_LD_CALOR_MOD."|".$o_LD_ELETR_PLAN."|".$o_LD_ELETR_MOD."|".$o_LD_EXPL_PLAN."|".$o_LD_EXPL_MOD."|".$o_LD_INFL_PLAN."|".$o_LD_INFL_MOD."|".$o_LD_PART_PLAN."|".$o_LD_PART_MOD."|".$o_LD_POEI_PLAN."|".$o_LD_POEI_MOD."|".$o_LD_RAD_PLAN."|".$o_LD_RAD_MOD."|".$o_LD_RUI_PLAN."|".$o_LD_RUI_MOD."|".$o_LD_VAP_PLAN."|".$o_LD_VAP_MOD."|".$o_LD_VBRVCI_PLAN."|".$o_LD_VBRVCI_MOD."|".$o_LD_VBRVMB_PLAN."|".$o_LD_VBRVMB_MOD."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id, AA.`ano` as ano, AA.`mes` as mes, upper(CLNT.`nome_interno`) as nome_interno, BLGC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, CLR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, LTRCDD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, XPLSV.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, NFLMVL.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, PRTCLD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, PR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, RDC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, RD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VPR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VBRVC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VBRVMB.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `EMISSAO` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`cliente`
    LEFT JOIN `BIOLOGICO` as BLGC
           ON BLGC.`planilha_num` = AA.`ld_bio_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_bio_mod`
    LEFT JOIN `CALOR` as CLR
           ON CLR.`planilha_num` = AA.`ld_calor_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_calor_mod`
    LEFT JOIN `ELETRICIDADE` as LTRCDD
           ON LTRCDD.`planilha_num` = AA.`ld_eletr_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_eletr_mod`
    LEFT JOIN `EXPLOSIVO` as XPLSV
           ON XPLSV.`planilha_num` = AA.`ld_expl_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_expl_mod`
    LEFT JOIN `INFLAMAVEL` as NFLMVL
           ON NFLMVL.`planilha_num` = AA.`ld_infl_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_infl_mod`
    LEFT JOIN `PARTICULADO` as PRTCLD
           ON PRTCLD.`planilha_num` = AA.`ld_part_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_part_mod`
    LEFT JOIN `POEIRA` as PR
           ON PR.`planilha_num` = AA.`ld_poei_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_poei_mod`
    LEFT JOIN `RADIACAO` as RDC
           ON RDC.`planilha_num` = AA.`ld_rad_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_rad_mod`
    LEFT JOIN `RUIDO` as RD
           ON RD.`planilha_num` = AA.`ld_rui_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_rui_mod`
    LEFT JOIN `VAPOR` as VPR
           ON VPR.`planilha_num` = AA.`ld_vap_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vap_mod`
    LEFT JOIN `VIBR_VCI` as VBRVC
           ON VBRVC.`planilha_num` = AA.`ld_vbrvci_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vbrvci_mod`
    LEFT JOIN `VIBR_VMB` as VBRVMB
           ON VBRVMB.`planilha_num` = AA.`ld_vbrvmb_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vbrvmb_mod`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_CLIENTE, $o_LD_BIO_PLAN, $o_LD_BIO_MOD, $o_LD_CALOR_PLAN, $o_LD_CALOR_MOD, $o_LD_ELETR_PLAN, $o_LD_ELETR_MOD, $o_LD_EXPL_PLAN, $o_LD_EXPL_MOD, $o_LD_INFL_PLAN, $o_LD_INFL_MOD, $o_LD_PART_PLAN, $o_LD_PART_MOD, $o_LD_POEI_PLAN, $o_LD_POEI_MOD, $o_LD_RAD_PLAN, $o_LD_RAD_MOD, $o_LD_RUI_PLAN, $o_LD_RUI_MOD, $o_LD_VAP_PLAN, $o_LD_VAP_MOD, $o_LD_VBRVCI_PLAN, $o_LD_VBRVCI_MOD, $o_LD_VBRVMB_PLAN, $o_LD_VBRVMB_MOD, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_DEZ; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_CLIENTE.':</b></th><td>'.$o_CLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_N_PLANILHA.':</b></th><td>'.$o_LD_BIO_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_MODELO.':</b></th><td>'.$o_LD_BIO_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_CALOR_N_PLANILHA.':</b></th><td>'.$o_LD_CALOR_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_CALOR_MODELO.':</b></th><td>'.$o_LD_CALOR_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_N_PLANILHA.':</b></th><td>'.$o_LD_ELETR_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_MODELO.':</b></th><td>'.$o_LD_ELETR_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_N_PLANILHA.':</b></th><td>'.$o_LD_EXPL_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_MODELO.':</b></th><td>'.$o_LD_EXPL_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_N_PLANILHA.':</b></th><td>'.$o_LD_INFL_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_MODELO.':</b></th><td>'.$o_LD_INFL_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_N_PLANILHA.':</b></th><td>'.$o_LD_PART_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_MODELO.':</b></th><td>'.$o_LD_PART_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_N_PLANILHA.':</b></th><td>'.$o_LD_POEI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_MODELO.':</b></th><td>'.$o_LD_POEI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_N_PLANILHA.':</b></th><td>'.$o_LD_RAD_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_MODELO.':</b></th><td>'.$o_LD_RAD_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_N_PLANILHA.':</b></th><td>'.$o_LD_RUI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_MODELO.':</b></th><td>'.$o_LD_RUI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_N_PLANILHA.':</b></th><td>'.$o_LD_VAP_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_MODELO.':</b></th><td>'.$o_LD_VAP_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_N_PLANILHA.':</b></th><td>'.$o_LD_VBRVCI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_MODELO.':</b></th><td>'.$o_LD_VBRVCI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_N_PLANILHA.':</b></th><td>'.$o_LD_VBRVMB_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_MODELO.':</b></th><td>'.$o_LD_VBRVMB_MOD.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
