<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: index.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 02/09/2017 13:22:11
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<style>
		.panel-default > .panel-heading-custom 
		{
			background: #003a1f; 
			color: #fff;
			font-size: 100%;
			line-height:20px;
		}
		.panel_bgcolor_gray 
		{
			background: #e6e6e6;
		}
		.panel_info
		{
			color:#000;
			font-size: 200%;
			font-weight: bold;
		}
		.panel_color_green
		{
			color:#003a1f;
		}
		.panel_color_black
		{
			color:#000;
		}
		.columns {
				float: left;
				width: 20%;//33.3%;
				padding: 8px;
		}
	</style>
<?php
	}
	
	####
	# FUNCOES DE APOIO
	####
	function geraPainel($_IND_ACESSAR, $_TOTAL, $_ICON, $_PG_LINK, $_TITLE)
	{
		$RET = '';
		$RET .= '<div class="columns">';
		$RET .= '	<div class="panel panel-default">';
		$RET .= '		<div class="panel-heading panel-heading-custom">';
		$RET .= '			<h3 class="panel-title">'.$_TITLE.'</h3>';
		$RET .= '		</div>';
		$RET .= '		<div class="panel-body panel_bgcolor_gray">';
		$RET .= '			<center><table><tr>';
		$RET .= '				<td><i class="fa '.$_ICON.' fa-2x panel_color_green"></i>&nbsp;&nbsp;</td>';
		$RET .= '				<td><div class="panel_info">'.$_TOTAL.'</div></td>';
		$RET .= '			</tr></table></center>';
		$RET .= '		</div>';
		if($_IND_ACESSAR == 1)
		{
			$RET .= '		<div class="panel-footer panel-heading-custom"><a href="main.php?pg='.$_PG_LINK.'" style="color:white;">+ '.TXT_PAINEL_DASHBOARD_PAINEL_DETALHES.'</a></div>';
		}
		else
		{
			$RET .= '		<div class="panel-footer panel-heading-custom"><a style="color:#737475; cursor: not-allowed;"><i>+ '.TXT_PAINEL_DASHBOARD_PAINEL_DETALHES.'</i></a></div>';
		}
		$RET .= '	</div>';
		$RET .= '</div>';
		return $RET;
	}
	function getTotalColaboradores($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `COLABORADOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalDispositivos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `DISPOSITIVO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		return $RET;
	}
	function getTotalClientes($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `CLIENTE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalContratos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `CONTRATO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalModelos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `PLANILHA_MODELO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosBIO($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `BIOLOGICO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosCALOR($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `CALOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosELETR($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `ELETRICIDADE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosEXPL($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `EXPLOSIVO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosINFL($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `INFLAMAVEL` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosPART($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `PARTICULADO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosPOEI($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `POEIRA` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosRAD($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `RADIACAO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosRUI($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `RUIDO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosVAP($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `VAPOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosVBRVCI($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `VIBR_VCI` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLaudosVBRVMB($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `VIBR_VMB` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		## Carrega dados
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			
			####
			# Carrega Matriz de Acesso do Usuário
			####
			{
				# Carrega relacao de modulos
				$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				$sql_USER_CATEG = $mysqli->escape_String($SID_CTRL->getUSER_CATEGORIA());
				$sql_idCOLABORADOR = $mysqli->escape_String($SID_CTRL->getUSER_IDCOLABORADOR());
				
				
				//$sql_USER_CATEG = $mysqli->escape_String($user_categoria);
				//$sql_idCOLABORADOR = $mysqli->escape_String($user_colaborador_id);
				
				$QUERY = "SELECT PNA.`modulo_codigo`,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_acessar` is null THEN 0 ELSE CA.`ind_acessar` END
	              END as ind_acessar,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_editar` is null THEN 0 ELSE CA.`ind_editar` END
	              END as ind_editar,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_apagar` is null THEN 0 ELSE CA.`ind_apagar` END
	              END as ind_apagar
	         FROM `SYSTEM_NIVEL_ACESSO` PNA
	    LEFT JOIN `COLABORADOR_ACESSO` CA
	           ON CA.`modulo_codigo` = PNA.`modulo_codigo`
	          AND CA.`idSYSTEM_CLIENTE` = '".$sql_idSYSTEM_CLIENTE."'
	          AND CA.`idCOLABORADOR` = '".$sql_idCOLABORADOR."'
	        WHERE `modulo_liberado` = 1 
	     ORDER BY `ordem`";
				if ($stmt = $mysqli->prepare($QUERY)) 
				{
					//$stmt->bind_param('ssssssss', $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
					$stmt->execute();
					$stmt->store_result();
					//
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_MOD_COD, $o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
					
					$ACESSO_CTRL = array();
					
					while($stmt->fetch())
					{
						# Define nivel de acesso
						if($o_MOD_COD)
						{
							$ACESSO_CTRL[$o_MOD_COD] = new AcessoCTRL($o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
						}
					}
					
				}
			}
			
			##Carrega Indicadores do Dashboard
			
			##Escapes
			$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT lower(AA.`codigo`) as codigo,
	            AA.`ordem` as ordem
	       FROM `SYSTEM_CFG_DASHBOARD` AA
	 INNER JOIN `SYSTEM_CLIENTE` C
	         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
	      WHERE AA.`idSYSTEM_CLIENTE` = ?
	        AND AA.`ind_exibir` = 1
	   ORDER BY AA.`ordem`"
			)) 
			{
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CODIGO,$o_ORDEM);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$RET = 0;
				}
				else
				{
					while($stmt->fetch())
					{
						switch($o_CODIGO)
						{
							/*
							case 'lote':
								$o_TOTAL = getTotalLotes($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['prod-lote']->acessar, $o_TOTAL, 'fa-archive', 'prod-lote', TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES);
								break;
							*/
							case 'contrato':
								$o_TOTAL = getTotalContratos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['adm-contrato']->acessar, $o_TOTAL, 'fa-file-text', 'adm-contrato', TXT_PAINEL_DASHBOARD_TOTAL_DE_CONTRATOS);
								break;
							case 'cliente':
								$o_TOTAL = getTotalClientes($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['adm-cliente']->acessar, $o_TOTAL, 'fa-address-card', 'adm-cliente', TXT_PAINEL_DASHBOARD_TOTAL_DE_CLIENTES);
								break;
							case 'modelo':
								$o_TOTAL = getTotalModelos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['cfg-planilha_modelo']->acessar, $o_TOTAL, 'fa-file-excel-o', 'cfg-planilha_modelo', TXT_PAINEL_DASHBOARD_TOTAL_DE_MODELOS);
								break;
							case 'colaborador':
								$o_TOTAL = getTotalColaboradores($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['cfg-colabs']->acessar, $o_TOTAL, 'fa-id-badge', 'cfg-colabs', TXT_PAINEL_DASHBOARD_TOTAL_DE_COLABORADORES);
								break;
							/*
							case 'dispositivo':
								$o_TOTAL = getTotalDispositivos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['cfg-dispositivos']->acessar, $o_TOTAL, 'fa-mobile', 'cfg-dispositivos', TXT_PAINEL_DASHBOARD_TOTAL_DE_DISPOSITIVOS);
								break;
							*/
							case 'ld_bio':
								$o_TOTAL = getTotalLaudosBIO($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-bio']->acessar, $o_TOTAL, 'fa-file', 'laudos-bio', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_BIO);
								break;
							case 'ld_calor':
								$o_TOTAL = getTotalLaudosCALOR($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-calor']->acessar, $o_TOTAL, 'fa-file', 'laudos-calor', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_CALOR);
								break;
							case 'ld_eletr':
								$o_TOTAL = getTotalLaudosELETR($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-eletr']->acessar, $o_TOTAL, 'fa-file', 'laudos-eletr', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_ELETR);
								break;
							case 'ld_expl':
								$o_TOTAL = getTotalLaudosEXPL($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-explo']->acessar, $o_TOTAL, 'fa-file', 'laudos-explo', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_EXPL);
								break;
							case 'ld_infl':
								$o_TOTAL = getTotalLaudosINFL($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-infl']->acessar, $o_TOTAL, 'fa-file', 'laudos-infl', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_INFL);
								break;
							case 'ld_part':
								$o_TOTAL = getTotalLaudosPART($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-part']->acessar, $o_TOTAL, 'fa-file', 'laudos-part', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_PART);
								break;
							case 'ld_poei':
								$o_TOTAL = getTotalLaudosPOEI($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-poeira']->acessar, $o_TOTAL, 'fa-file', 'laudos-poeira', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_POEI);
								break;
							case 'ld_rad':
								$o_TOTAL = getTotalLaudosRAD($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-rad']->acessar, $o_TOTAL, 'fa-file', 'laudos-rad', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_RAD);
								break;
							case 'ld_rui':
								$o_TOTAL = getTotalLaudosRUI($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-ruido']->acessar, $o_TOTAL, 'fa-file', 'laudos-ruido', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_RUI);
								break;
							case 'ld_vap':
								$o_TOTAL = getTotalLaudosVAP($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-vapor']->acessar, $o_TOTAL, 'fa-file', 'laudos-vapor', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VAP);
								break;
							case 'ld_vbrvci':
								$o_TOTAL = getTotalLaudosVBRVCI($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-vibr_vci']->acessar, $o_TOTAL, 'fa-file', 'laudos-vibr_vci', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VBRVCI);
								break;
							case 'ld_vbrvmb':
								$o_TOTAL = getTotalLaudosVBRVMB($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['laudos-vibr_vmb']->acessar, $o_TOTAL, 'fa-file', 'laudos-vibr_vmb', TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VBRVMB);
								break;
							default:
								break;
						}
					}
				}
				
			}
			
		}
		##/carega planos
	

		
?>
		<!-- main_table -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_PAINEL_DASHBOARD_PAINEL_DE_CONTROLE; ?></a></li>
							<li class="active"><span><?php echo TXT_PAINEL_DASHBOARD_DASHBOARD; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div class="row">
						<?php echo $PANELS; ?>
					</div>
					
				</div>
			</div>
		</div>
		<!-- /main_tabe -->
		
<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
<?php
	} 
?>
