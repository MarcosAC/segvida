<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-vapor.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 10/9/2017 13:51:42
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_IDCLIENTE = $IN_ANO = $IN_MES = $IN_PLANILHA_NUM = $IN_UNIDADE_SITE = $IN_DATA_ELABORACAO = $IN_AREA = $IN_SETOR = $IN_GES = $IN_CARGO_FUNCAO = $IN_CBO = $IN_ATIV_MACRO = $IN_ANALISES = $IN_ANALISE_AMOSTRA_1 = $IN_ANALISE_DATA_AMOSTRAGEM_1 = $IN_ANALISE_TAREFA_EXEC_1 = $IN_ANALISE_PROC_PROD_1 = $IN_ANALISE_OBS_TAREFA_1 = $IN_ANALISE_AMOSTRA_2 = $IN_ANALISE_DATA_AMOSTRAGEM_2 = $IN_ANALISE_TAREFA_EXEC_2 = $IN_ANALISE_PROC_PROD_2 = $IN_ANALISE_OBS_TAREFA_2 = $IN_ANALISE_AMOSTRA_3 = $IN_ANALISE_DATA_AMOSTRAGEM_3 = $IN_ANALISE_TAREFA_EXEC_3 = $IN_ANALISE_PROC_PROD_3 = $IN_ANALISE_OBS_TAREFA_3 = $IN_ANALISE_AMOSTRA_4 = $IN_ANALISE_DATA_AMOSTRAGEM_4 = $IN_ANALISE_TAREFA_EXEC_4 = $IN_ANALISE_PROC_PROD_4 = $IN_ANALISE_OBS_TAREFA_4 = $IN_ANALISE_AMOSTRA_5 = $IN_ANALISE_DATA_AMOSTRAGEM_5 = $IN_ANALISE_TAREFA_EXEC_5 = $IN_ANALISE_PROC_PROD_5 = $IN_ANALISE_OBS_TAREFA_5 = $IN_ANALISE_AMOSTRA_6 = $IN_ANALISE_DATA_AMOSTRAGEM_6 = $IN_ANALISE_TAREFA_EXEC_6 = $IN_ANALISE_PROC_PROD_6 = $IN_ANALISE_OBS_TAREFA_6 = $IN_JOR_TRAB = $IN_TEMPO_EXPO = $IN_TIPO_EXPO = $IN_MEIO_PROPAG = $IN_FONTE_GERADORA = $IN_MITIGACAO = $IN_COLETAS = $IN_COLETA_AMOSTRA_1 = $IN_COLETA_DATA_1 = $IN_COLETA_NUM_SERIAL_1 = $IN_COLETA_NUM_AMOSTRADOR_1 = $IN_COLETA_NUM_RELAT_ENSAIO_1 = $IN_COLETA_VAZAO_1 = $IN_COLETA_TEMPO_AMOSTRAGEM_1 = $IN_COLETA_MASSA1_1 = $IN_COLETA_MASSA2_1 = $IN_COLETA_MASSA3_1 = $IN_COLETA_MASSA4_1 = $IN_COLETA_MASSA5_1 = $IN_COLETA_AMOSTRA_2 = $IN_COLETA_DATA_2 = $IN_COLETA_NUM_SERIAL_2 = $IN_COLETA_NUM_AMOSTRADOR_2 = $IN_COLETA_NUM_RELAT_ENSAIO_2 = $IN_COLETA_VAZAO_2 = $IN_COLETA_TEMPO_AMOSTRAGEM_2 = $IN_COLETA_MASSA1_2 = $IN_COLETA_MASSA2_2 = $IN_COLETA_MASSA3_2 = $IN_COLETA_MASSA4_2 = $IN_COLETA_MASSA5_2 = $IN_COLETA_AMOSTRA_3 = $IN_COLETA_DATA_3 = $IN_COLETA_NUM_SERIAL_3 = $IN_COLETA_NUM_AMOSTRADOR_3 = $IN_COLETA_NUM_RELAT_ENSAIO_3 = $IN_COLETA_VAZAO_3 = $IN_COLETA_TEMPO_AMOSTRAGEM_3 = $IN_COLETA_MASSA1_3 = $IN_COLETA_MASSA2_3 = $IN_COLETA_MASSA3_3 = $IN_COLETA_MASSA4_3 = $IN_COLETA_MASSA5_3 = $IN_COLETA_AMOSTRA_4 = $IN_COLETA_DATA_4 = $IN_COLETA_NUM_SERIAL_4 = $IN_COLETA_NUM_AMOSTRADOR_4 = $IN_COLETA_NUM_RELAT_ENSAIO_4 = $IN_COLETA_VAZAO_4 = $IN_COLETA_TEMPO_AMOSTRAGEM_4 = $IN_COLETA_MASSA1_4 = $IN_COLETA_MASSA2_4 = $IN_COLETA_MASSA3_4 = $IN_COLETA_MASSA4_4 = $IN_COLETA_MASSA5_4 = $IN_COLETA_AMOSTRA_5 = $IN_COLETA_DATA_5 = $IN_COLETA_NUM_SERIAL_5 = $IN_COLETA_NUM_AMOSTRADOR_5 = $IN_COLETA_NUM_RELAT_ENSAIO_5 = $IN_COLETA_VAZAO_5 = $IN_COLETA_TEMPO_AMOSTRAGEM_5 = $IN_COLETA_MASSA1_5 = $IN_COLETA_MASSA2_5 = $IN_COLETA_MASSA3_5 = $IN_COLETA_MASSA4_5 = $IN_COLETA_MASSA5_5 = $IN_COLETA_AMOSTRA_6 = $IN_COLETA_DATA_6 = $IN_COLETA_NUM_SERIAL_6 = $IN_COLETA_NUM_AMOSTRADOR_6 = $IN_COLETA_NUM_RELAT_ENSAIO_6 = $IN_COLETA_VAZAO_6 = $IN_COLETA_TEMPO_AMOSTRAGEM_6 = $IN_COLETA_MASSA1_6 = $IN_COLETA_MASSA2_6 = $IN_COLETA_MASSA3_6 = $IN_COLETA_MASSA4_6 = $IN_COLETA_MASSA5_6 = $IN_RESPIRADOR = $IN_CERT_APROV = $IN_RESP_CAMPO_IDCOLABORADOR = $IN_RESP_TECNICO_IDCOLABORADOR = $IN_REGISTRO_RC = $IN_REGISTRO_RT = "";
	
	####
	# Set Debug Mode
	####
	#$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_IDCLIENTE = test_input($_GET["idcliente"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_PLANILHA_NUM = test_input($_GET["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_GET["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_GET["data_elaboracao"]);
		$IN_AREA = test_input($_GET["area"]);
		$IN_SETOR = test_input($_GET["setor"]);
		$IN_GES = test_input($_GET["ges"]);
		$IN_CARGO_FUNCAO = test_input($_GET["cargo_funcao"]);
		$IN_CBO = test_input($_GET["cbo"]);
		$IN_ATIV_MACRO = test_input($_GET["ativ_macro"]);
		$IN_ANALISES = test_input($_GET["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_GET["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_GET["analise_data_amostragem_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_GET["analise_tarefa_exec_1"]);
		$IN_ANALISE_PROC_PROD_1 = test_input($_GET["analise_proc_prod_1"]);
		$IN_ANALISE_OBS_TAREFA_1 = test_input($_GET["analise_obs_tarefa_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_GET["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_GET["analise_data_amostragem_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_GET["analise_tarefa_exec_2"]);
		$IN_ANALISE_PROC_PROD_2 = test_input($_GET["analise_proc_prod_2"]);
		$IN_ANALISE_OBS_TAREFA_2 = test_input($_GET["analise_obs_tarefa_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_GET["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_GET["analise_data_amostragem_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_GET["analise_tarefa_exec_3"]);
		$IN_ANALISE_PROC_PROD_3 = test_input($_GET["analise_proc_prod_3"]);
		$IN_ANALISE_OBS_TAREFA_3 = test_input($_GET["analise_obs_tarefa_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_GET["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_GET["analise_data_amostragem_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_GET["analise_tarefa_exec_4"]);
		$IN_ANALISE_PROC_PROD_4 = test_input($_GET["analise_proc_prod_4"]);
		$IN_ANALISE_OBS_TAREFA_4 = test_input($_GET["analise_obs_tarefa_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_GET["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_GET["analise_data_amostragem_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_GET["analise_tarefa_exec_5"]);
		$IN_ANALISE_PROC_PROD_5 = test_input($_GET["analise_proc_prod_5"]);
		$IN_ANALISE_OBS_TAREFA_5 = test_input($_GET["analise_obs_tarefa_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_GET["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_GET["analise_data_amostragem_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_GET["analise_tarefa_exec_6"]);
		$IN_ANALISE_PROC_PROD_6 = test_input($_GET["analise_proc_prod_6"]);
		$IN_ANALISE_OBS_TAREFA_6 = test_input($_GET["analise_obs_tarefa_6"]);
		$IN_JOR_TRAB = test_input($_GET["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_GET["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_GET["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_GET["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_GET["fonte_geradora"]);
		$IN_MITIGACAO = test_input($_GET["mitigacao"]);
		$IN_COLETAS = test_input($_GET["coletas"]);
		$IN_COLETA_AMOSTRA_1 = test_input($_GET["coleta_amostra_1"]);
		$IN_COLETA_DATA_1 = test_input($_GET["coleta_data_1"]);
		$IN_COLETA_NUM_SERIAL_1 = test_input($_GET["coleta_num_serial_1"]);
		$IN_COLETA_NUM_AMOSTRADOR_1 = test_input($_GET["coleta_num_amostrador_1"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_1 = test_input($_GET["coleta_num_relat_ensaio_1"]);
		$IN_COLETA_VAZAO_1 = test_input($_GET["coleta_vazao_1"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_1 = test_input($_GET["coleta_tempo_amostragem_1"]);
		$IN_COLETA_MASSA1_1 = test_input($_GET["coleta_massa1_1"]);
		$IN_COLETA_MASSA2_1 = test_input($_GET["coleta_massa2_1"]);
		$IN_COLETA_MASSA3_1 = test_input($_GET["coleta_massa3_1"]);
		$IN_COLETA_MASSA4_1 = test_input($_GET["coleta_massa4_1"]);
		$IN_COLETA_MASSA5_1 = test_input($_GET["coleta_massa5_1"]);
		$IN_COLETA_AMOSTRA_2 = test_input($_GET["coleta_amostra_2"]);
		$IN_COLETA_DATA_2 = test_input($_GET["coleta_data_2"]);
		$IN_COLETA_NUM_SERIAL_2 = test_input($_GET["coleta_num_serial_2"]);
		$IN_COLETA_NUM_AMOSTRADOR_2 = test_input($_GET["coleta_num_amostrador_2"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_2 = test_input($_GET["coleta_num_relat_ensaio_2"]);
		$IN_COLETA_VAZAO_2 = test_input($_GET["coleta_vazao_2"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_2 = test_input($_GET["coleta_tempo_amostragem_2"]);
		$IN_COLETA_MASSA1_2 = test_input($_GET["coleta_massa1_2"]);
		$IN_COLETA_MASSA2_2 = test_input($_GET["coleta_massa2_2"]);
		$IN_COLETA_MASSA3_2 = test_input($_GET["coleta_massa3_2"]);
		$IN_COLETA_MASSA4_2 = test_input($_GET["coleta_massa4_2"]);
		$IN_COLETA_MASSA5_2 = test_input($_GET["coleta_massa5_2"]);
		$IN_COLETA_AMOSTRA_3 = test_input($_GET["coleta_amostra_3"]);
		$IN_COLETA_DATA_3 = test_input($_GET["coleta_data_3"]);
		$IN_COLETA_NUM_SERIAL_3 = test_input($_GET["coleta_num_serial_3"]);
		$IN_COLETA_NUM_AMOSTRADOR_3 = test_input($_GET["coleta_num_amostrador_3"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_3 = test_input($_GET["coleta_num_relat_ensaio_3"]);
		$IN_COLETA_VAZAO_3 = test_input($_GET["coleta_vazao_3"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_3 = test_input($_GET["coleta_tempo_amostragem_3"]);
		$IN_COLETA_MASSA1_3 = test_input($_GET["coleta_massa1_3"]);
		$IN_COLETA_MASSA2_3 = test_input($_GET["coleta_massa2_3"]);
		$IN_COLETA_MASSA3_3 = test_input($_GET["coleta_massa3_3"]);
		$IN_COLETA_MASSA4_3 = test_input($_GET["coleta_massa4_3"]);
		$IN_COLETA_MASSA5_3 = test_input($_GET["coleta_massa5_3"]);
		$IN_COLETA_AMOSTRA_4 = test_input($_GET["coleta_amostra_4"]);
		$IN_COLETA_DATA_4 = test_input($_GET["coleta_data_4"]);
		$IN_COLETA_NUM_SERIAL_4 = test_input($_GET["coleta_num_serial_4"]);
		$IN_COLETA_NUM_AMOSTRADOR_4 = test_input($_GET["coleta_num_amostrador_4"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_4 = test_input($_GET["coleta_num_relat_ensaio_4"]);
		$IN_COLETA_VAZAO_4 = test_input($_GET["coleta_vazao_4"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_4 = test_input($_GET["coleta_tempo_amostragem_4"]);
		$IN_COLETA_MASSA1_4 = test_input($_GET["coleta_massa1_4"]);
		$IN_COLETA_MASSA2_4 = test_input($_GET["coleta_massa2_4"]);
		$IN_COLETA_MASSA3_4 = test_input($_GET["coleta_massa3_4"]);
		$IN_COLETA_MASSA4_4 = test_input($_GET["coleta_massa4_4"]);
		$IN_COLETA_MASSA5_4 = test_input($_GET["coleta_massa5_4"]);
		$IN_COLETA_AMOSTRA_5 = test_input($_GET["coleta_amostra_5"]);
		$IN_COLETA_DATA_5 = test_input($_GET["coleta_data_5"]);
		$IN_COLETA_NUM_SERIAL_5 = test_input($_GET["coleta_num_serial_5"]);
		$IN_COLETA_NUM_AMOSTRADOR_5 = test_input($_GET["coleta_num_amostrador_5"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_5 = test_input($_GET["coleta_num_relat_ensaio_5"]);
		$IN_COLETA_VAZAO_5 = test_input($_GET["coleta_vazao_5"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_5 = test_input($_GET["coleta_tempo_amostragem_5"]);
		$IN_COLETA_MASSA1_5 = test_input($_GET["coleta_massa1_5"]);
		$IN_COLETA_MASSA2_5 = test_input($_GET["coleta_massa2_5"]);
		$IN_COLETA_MASSA3_5 = test_input($_GET["coleta_massa3_5"]);
		$IN_COLETA_MASSA4_5 = test_input($_GET["coleta_massa4_5"]);
		$IN_COLETA_MASSA5_5 = test_input($_GET["coleta_massa5_5"]);
		$IN_COLETA_AMOSTRA_6 = test_input($_GET["coleta_amostra_6"]);
		$IN_COLETA_DATA_6 = test_input($_GET["coleta_data_6"]);
		$IN_COLETA_NUM_SERIAL_6 = test_input($_GET["coleta_num_serial_6"]);
		$IN_COLETA_NUM_AMOSTRADOR_6 = test_input($_GET["coleta_num_amostrador_6"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_6 = test_input($_GET["coleta_num_relat_ensaio_6"]);
		$IN_COLETA_VAZAO_6 = test_input($_GET["coleta_vazao_6"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_6 = test_input($_GET["coleta_tempo_amostragem_6"]);
		$IN_COLETA_MASSA1_6 = test_input($_GET["coleta_massa1_6"]);
		$IN_COLETA_MASSA2_6 = test_input($_GET["coleta_massa2_6"]);
		$IN_COLETA_MASSA3_6 = test_input($_GET["coleta_massa3_6"]);
		$IN_COLETA_MASSA4_6 = test_input($_GET["coleta_massa4_6"]);
		$IN_COLETA_MASSA5_6 = test_input($_GET["coleta_massa5_6"]);
		$IN_RESPIRADOR = test_input($_GET["respirador"]);
		$IN_CERT_APROV = test_input($_GET["cert_aprov"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_GET["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_GET["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_GET["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_GET["registro_rt"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_IDCLIENTE = test_input($_POST["idcliente"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_PLANILHA_NUM = test_input($_POST["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_POST["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_POST["data_elaboracao"]);
		$IN_AREA = test_input($_POST["area"]);
		$IN_SETOR = test_input($_POST["setor"]);
		$IN_GES = test_input($_POST["ges"]);
		$IN_CARGO_FUNCAO = test_input($_POST["cargo_funcao"]);
		$IN_CBO = test_input($_POST["cbo"]);
		$IN_ATIV_MACRO = test_input($_POST["ativ_macro"]);
		$IN_ANALISES = test_input($_POST["analises"]);
		$IN_ANALISE_AMOSTRA_1 = test_input($_POST["analise_amostra_1"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_1 = test_input($_POST["analise_data_amostragem_1"]);
		$IN_ANALISE_TAREFA_EXEC_1 = test_input($_POST["analise_tarefa_exec_1"]);
		$IN_ANALISE_PROC_PROD_1 = test_input($_POST["analise_proc_prod_1"]);
		$IN_ANALISE_OBS_TAREFA_1 = test_input($_POST["analise_obs_tarefa_1"]);
		$IN_ANALISE_AMOSTRA_2 = test_input($_POST["analise_amostra_2"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_2 = test_input($_POST["analise_data_amostragem_2"]);
		$IN_ANALISE_TAREFA_EXEC_2 = test_input($_POST["analise_tarefa_exec_2"]);
		$IN_ANALISE_PROC_PROD_2 = test_input($_POST["analise_proc_prod_2"]);
		$IN_ANALISE_OBS_TAREFA_2 = test_input($_POST["analise_obs_tarefa_2"]);
		$IN_ANALISE_AMOSTRA_3 = test_input($_POST["analise_amostra_3"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_3 = test_input($_POST["analise_data_amostragem_3"]);
		$IN_ANALISE_TAREFA_EXEC_3 = test_input($_POST["analise_tarefa_exec_3"]);
		$IN_ANALISE_PROC_PROD_3 = test_input($_POST["analise_proc_prod_3"]);
		$IN_ANALISE_OBS_TAREFA_3 = test_input($_POST["analise_obs_tarefa_3"]);
		$IN_ANALISE_AMOSTRA_4 = test_input($_POST["analise_amostra_4"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_4 = test_input($_POST["analise_data_amostragem_4"]);
		$IN_ANALISE_TAREFA_EXEC_4 = test_input($_POST["analise_tarefa_exec_4"]);
		$IN_ANALISE_PROC_PROD_4 = test_input($_POST["analise_proc_prod_4"]);
		$IN_ANALISE_OBS_TAREFA_4 = test_input($_POST["analise_obs_tarefa_4"]);
		$IN_ANALISE_AMOSTRA_5 = test_input($_POST["analise_amostra_5"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_5 = test_input($_POST["analise_data_amostragem_5"]);
		$IN_ANALISE_TAREFA_EXEC_5 = test_input($_POST["analise_tarefa_exec_5"]);
		$IN_ANALISE_PROC_PROD_5 = test_input($_POST["analise_proc_prod_5"]);
		$IN_ANALISE_OBS_TAREFA_5 = test_input($_POST["analise_obs_tarefa_5"]);
		$IN_ANALISE_AMOSTRA_6 = test_input($_POST["analise_amostra_6"]);
		$IN_ANALISE_DATA_AMOSTRAGEM_6 = test_input($_POST["analise_data_amostragem_6"]);
		$IN_ANALISE_TAREFA_EXEC_6 = test_input($_POST["analise_tarefa_exec_6"]);
		$IN_ANALISE_PROC_PROD_6 = test_input($_POST["analise_proc_prod_6"]);
		$IN_ANALISE_OBS_TAREFA_6 = test_input($_POST["analise_obs_tarefa_6"]);
		$IN_JOR_TRAB = test_input($_POST["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_POST["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_POST["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_POST["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_POST["fonte_geradora"]);
		$IN_MITIGACAO = test_input($_POST["mitigacao"]);
		$IN_COLETAS = test_input($_POST["coletas"]);
		$IN_COLETA_AMOSTRA_1 = test_input($_POST["coleta_amostra_1"]);
		$IN_COLETA_DATA_1 = test_input($_POST["coleta_data_1"]);
		$IN_COLETA_NUM_SERIAL_1 = test_input($_POST["coleta_num_serial_1"]);
		$IN_COLETA_NUM_AMOSTRADOR_1 = test_input($_POST["coleta_num_amostrador_1"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_1 = test_input($_POST["coleta_num_relat_ensaio_1"]);
		$IN_COLETA_VAZAO_1 = test_input($_POST["coleta_vazao_1"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_1 = test_input($_POST["coleta_tempo_amostragem_1"]);
		$IN_COLETA_MASSA1_1 = test_input($_POST["coleta_massa1_1"]);
		$IN_COLETA_MASSA2_1 = test_input($_POST["coleta_massa2_1"]);
		$IN_COLETA_MASSA3_1 = test_input($_POST["coleta_massa3_1"]);
		$IN_COLETA_MASSA4_1 = test_input($_POST["coleta_massa4_1"]);
		$IN_COLETA_MASSA5_1 = test_input($_POST["coleta_massa5_1"]);
		$IN_COLETA_AMOSTRA_2 = test_input($_POST["coleta_amostra_2"]);
		$IN_COLETA_DATA_2 = test_input($_POST["coleta_data_2"]);
		$IN_COLETA_NUM_SERIAL_2 = test_input($_POST["coleta_num_serial_2"]);
		$IN_COLETA_NUM_AMOSTRADOR_2 = test_input($_POST["coleta_num_amostrador_2"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_2 = test_input($_POST["coleta_num_relat_ensaio_2"]);
		$IN_COLETA_VAZAO_2 = test_input($_POST["coleta_vazao_2"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_2 = test_input($_POST["coleta_tempo_amostragem_2"]);
		$IN_COLETA_MASSA1_2 = test_input($_POST["coleta_massa1_2"]);
		$IN_COLETA_MASSA2_2 = test_input($_POST["coleta_massa2_2"]);
		$IN_COLETA_MASSA3_2 = test_input($_POST["coleta_massa3_2"]);
		$IN_COLETA_MASSA4_2 = test_input($_POST["coleta_massa4_2"]);
		$IN_COLETA_MASSA5_2 = test_input($_POST["coleta_massa5_2"]);
		$IN_COLETA_AMOSTRA_3 = test_input($_POST["coleta_amostra_3"]);
		$IN_COLETA_DATA_3 = test_input($_POST["coleta_data_3"]);
		$IN_COLETA_NUM_SERIAL_3 = test_input($_POST["coleta_num_serial_3"]);
		$IN_COLETA_NUM_AMOSTRADOR_3 = test_input($_POST["coleta_num_amostrador_3"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_3 = test_input($_POST["coleta_num_relat_ensaio_3"]);
		$IN_COLETA_VAZAO_3 = test_input($_POST["coleta_vazao_3"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_3 = test_input($_POST["coleta_tempo_amostragem_3"]);
		$IN_COLETA_MASSA1_3 = test_input($_POST["coleta_massa1_3"]);
		$IN_COLETA_MASSA2_3 = test_input($_POST["coleta_massa2_3"]);
		$IN_COLETA_MASSA3_3 = test_input($_POST["coleta_massa3_3"]);
		$IN_COLETA_MASSA4_3 = test_input($_POST["coleta_massa4_3"]);
		$IN_COLETA_MASSA5_3 = test_input($_POST["coleta_massa5_3"]);
		$IN_COLETA_AMOSTRA_4 = test_input($_POST["coleta_amostra_4"]);
		$IN_COLETA_DATA_4 = test_input($_POST["coleta_data_4"]);
		$IN_COLETA_NUM_SERIAL_4 = test_input($_POST["coleta_num_serial_4"]);
		$IN_COLETA_NUM_AMOSTRADOR_4 = test_input($_POST["coleta_num_amostrador_4"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_4 = test_input($_POST["coleta_num_relat_ensaio_4"]);
		$IN_COLETA_VAZAO_4 = test_input($_POST["coleta_vazao_4"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_4 = test_input($_POST["coleta_tempo_amostragem_4"]);
		$IN_COLETA_MASSA1_4 = test_input($_POST["coleta_massa1_4"]);
		$IN_COLETA_MASSA2_4 = test_input($_POST["coleta_massa2_4"]);
		$IN_COLETA_MASSA3_4 = test_input($_POST["coleta_massa3_4"]);
		$IN_COLETA_MASSA4_4 = test_input($_POST["coleta_massa4_4"]);
		$IN_COLETA_MASSA5_4 = test_input($_POST["coleta_massa5_4"]);
		$IN_COLETA_AMOSTRA_5 = test_input($_POST["coleta_amostra_5"]);
		$IN_COLETA_DATA_5 = test_input($_POST["coleta_data_5"]);
		$IN_COLETA_NUM_SERIAL_5 = test_input($_POST["coleta_num_serial_5"]);
		$IN_COLETA_NUM_AMOSTRADOR_5 = test_input($_POST["coleta_num_amostrador_5"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_5 = test_input($_POST["coleta_num_relat_ensaio_5"]);
		$IN_COLETA_VAZAO_5 = test_input($_POST["coleta_vazao_5"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_5 = test_input($_POST["coleta_tempo_amostragem_5"]);
		$IN_COLETA_MASSA1_5 = test_input($_POST["coleta_massa1_5"]);
		$IN_COLETA_MASSA2_5 = test_input($_POST["coleta_massa2_5"]);
		$IN_COLETA_MASSA3_5 = test_input($_POST["coleta_massa3_5"]);
		$IN_COLETA_MASSA4_5 = test_input($_POST["coleta_massa4_5"]);
		$IN_COLETA_MASSA5_5 = test_input($_POST["coleta_massa5_5"]);
		$IN_COLETA_AMOSTRA_6 = test_input($_POST["coleta_amostra_6"]);
		$IN_COLETA_DATA_6 = test_input($_POST["coleta_data_6"]);
		$IN_COLETA_NUM_SERIAL_6 = test_input($_POST["coleta_num_serial_6"]);
		$IN_COLETA_NUM_AMOSTRADOR_6 = test_input($_POST["coleta_num_amostrador_6"]);
		$IN_COLETA_NUM_RELAT_ENSAIO_6 = test_input($_POST["coleta_num_relat_ensaio_6"]);
		$IN_COLETA_VAZAO_6 = test_input($_POST["coleta_vazao_6"]);
		$IN_COLETA_TEMPO_AMOSTRAGEM_6 = test_input($_POST["coleta_tempo_amostragem_6"]);
		$IN_COLETA_MASSA1_6 = test_input($_POST["coleta_massa1_6"]);
		$IN_COLETA_MASSA2_6 = test_input($_POST["coleta_massa2_6"]);
		$IN_COLETA_MASSA3_6 = test_input($_POST["coleta_massa3_6"]);
		$IN_COLETA_MASSA4_6 = test_input($_POST["coleta_massa4_6"]);
		$IN_COLETA_MASSA5_6 = test_input($_POST["coleta_massa5_6"]);
		$IN_RESPIRADOR = test_input($_POST["respirador"]);
		$IN_CERT_APROV = test_input($_POST["cert_aprov"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_POST["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_POST["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_POST["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_POST["registro_rt"]);
		
		## case convertion
		mb_strtoupper($IN_REGISTRO_RC,"UTF-8");
		mb_strtoupper($IN_REGISTRO_RT,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_TAREFA_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_TAREFA_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_TAREFA_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_TAREFA_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_TAREFA_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_TAREFA_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_MITIGACAO, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_NUM_SERIAL_1, $IN_COLETA_NUM_AMOSTRADOR_1, $IN_COLETA_NUM_RELAT_ENSAIO_1, $IN_COLETA_VAZAO_1, $IN_COLETA_TEMPO_AMOSTRAGEM_1, $IN_COLETA_MASSA1_1, $IN_COLETA_MASSA2_1, $IN_COLETA_MASSA3_1, $IN_COLETA_MASSA4_1, $IN_COLETA_MASSA5_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_NUM_SERIAL_2, $IN_COLETA_NUM_AMOSTRADOR_2, $IN_COLETA_NUM_RELAT_ENSAIO_2, $IN_COLETA_VAZAO_2, $IN_COLETA_TEMPO_AMOSTRAGEM_2, $IN_COLETA_MASSA1_2, $IN_COLETA_MASSA2_2, $IN_COLETA_MASSA3_2, $IN_COLETA_MASSA4_2, $IN_COLETA_MASSA5_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_NUM_SERIAL_3, $IN_COLETA_NUM_AMOSTRADOR_3, $IN_COLETA_NUM_RELAT_ENSAIO_3, $IN_COLETA_VAZAO_3, $IN_COLETA_TEMPO_AMOSTRAGEM_3, $IN_COLETA_MASSA1_3, $IN_COLETA_MASSA2_3, $IN_COLETA_MASSA3_3, $IN_COLETA_MASSA4_3, $IN_COLETA_MASSA5_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_NUM_SERIAL_4, $IN_COLETA_NUM_AMOSTRADOR_4, $IN_COLETA_NUM_RELAT_ENSAIO_4, $IN_COLETA_VAZAO_4, $IN_COLETA_TEMPO_AMOSTRAGEM_4, $IN_COLETA_MASSA1_4, $IN_COLETA_MASSA2_4, $IN_COLETA_MASSA3_4, $IN_COLETA_MASSA4_4, $IN_COLETA_MASSA5_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_NUM_SERIAL_5, $IN_COLETA_NUM_AMOSTRADOR_5, $IN_COLETA_NUM_RELAT_ENSAIO_5, $IN_COLETA_VAZAO_5, $IN_COLETA_TEMPO_AMOSTRAGEM_5, $IN_COLETA_MASSA1_5, $IN_COLETA_MASSA2_5, $IN_COLETA_MASSA3_5, $IN_COLETA_MASSA4_5, $IN_COLETA_MASSA5_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_NUM_SERIAL_6, $IN_COLETA_NUM_AMOSTRADOR_6, $IN_COLETA_NUM_RELAT_ENSAIO_6, $IN_COLETA_VAZAO_6, $IN_COLETA_TEMPO_AMOSTRAGEM_6, $IN_COLETA_MASSA1_6, $IN_COLETA_MASSA2_6, $IN_COLETA_MASSA3_6, $IN_COLETA_MASSA4_6, $IN_COLETA_MASSA5_6, $IN_RESPIRADOR, $IN_CERT_APROV, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_TAREFA_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_TAREFA_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_TAREFA_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_TAREFA_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_TAREFA_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_TAREFA_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_MITIGACAO, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_NUM_SERIAL_1, $IN_COLETA_NUM_AMOSTRADOR_1, $IN_COLETA_NUM_RELAT_ENSAIO_1, $IN_COLETA_VAZAO_1, $IN_COLETA_TEMPO_AMOSTRAGEM_1, $IN_COLETA_MASSA1_1, $IN_COLETA_MASSA2_1, $IN_COLETA_MASSA3_1, $IN_COLETA_MASSA4_1, $IN_COLETA_MASSA5_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_NUM_SERIAL_2, $IN_COLETA_NUM_AMOSTRADOR_2, $IN_COLETA_NUM_RELAT_ENSAIO_2, $IN_COLETA_VAZAO_2, $IN_COLETA_TEMPO_AMOSTRAGEM_2, $IN_COLETA_MASSA1_2, $IN_COLETA_MASSA2_2, $IN_COLETA_MASSA3_2, $IN_COLETA_MASSA4_2, $IN_COLETA_MASSA5_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_NUM_SERIAL_3, $IN_COLETA_NUM_AMOSTRADOR_3, $IN_COLETA_NUM_RELAT_ENSAIO_3, $IN_COLETA_VAZAO_3, $IN_COLETA_TEMPO_AMOSTRAGEM_3, $IN_COLETA_MASSA1_3, $IN_COLETA_MASSA2_3, $IN_COLETA_MASSA3_3, $IN_COLETA_MASSA4_3, $IN_COLETA_MASSA5_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_NUM_SERIAL_4, $IN_COLETA_NUM_AMOSTRADOR_4, $IN_COLETA_NUM_RELAT_ENSAIO_4, $IN_COLETA_VAZAO_4, $IN_COLETA_TEMPO_AMOSTRAGEM_4, $IN_COLETA_MASSA1_4, $IN_COLETA_MASSA2_4, $IN_COLETA_MASSA3_4, $IN_COLETA_MASSA4_4, $IN_COLETA_MASSA5_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_NUM_SERIAL_5, $IN_COLETA_NUM_AMOSTRADOR_5, $IN_COLETA_NUM_RELAT_ENSAIO_5, $IN_COLETA_VAZAO_5, $IN_COLETA_TEMPO_AMOSTRAGEM_5, $IN_COLETA_MASSA1_5, $IN_COLETA_MASSA2_5, $IN_COLETA_MASSA3_5, $IN_COLETA_MASSA4_5, $IN_COLETA_MASSA5_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_NUM_SERIAL_6, $IN_COLETA_NUM_AMOSTRADOR_6, $IN_COLETA_NUM_RELAT_ENSAIO_6, $IN_COLETA_VAZAO_6, $IN_COLETA_TEMPO_AMOSTRAGEM_6, $IN_COLETA_MASSA1_6, $IN_COLETA_MASSA2_6, $IN_COLETA_MASSA3_6, $IN_COLETA_MASSA4_6, $IN_COLETA_MASSA5_6, $IN_RESPIRADOR, $IN_CERT_APROV, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) || isEmpty($IN_ANALISES) || isEmpty($IN_COLETAS) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_ANALISES, $IN_ANALISE_AMOSTRA_1, $IN_ANALISE_DATA_AMOSTRAGEM_1, $IN_ANALISE_TAREFA_EXEC_1, $IN_ANALISE_PROC_PROD_1, $IN_ANALISE_OBS_TAREFA_1, $IN_ANALISE_AMOSTRA_2, $IN_ANALISE_DATA_AMOSTRAGEM_2, $IN_ANALISE_TAREFA_EXEC_2, $IN_ANALISE_PROC_PROD_2, $IN_ANALISE_OBS_TAREFA_2, $IN_ANALISE_AMOSTRA_3, $IN_ANALISE_DATA_AMOSTRAGEM_3, $IN_ANALISE_TAREFA_EXEC_3, $IN_ANALISE_PROC_PROD_3, $IN_ANALISE_OBS_TAREFA_3, $IN_ANALISE_AMOSTRA_4, $IN_ANALISE_DATA_AMOSTRAGEM_4, $IN_ANALISE_TAREFA_EXEC_4, $IN_ANALISE_PROC_PROD_4, $IN_ANALISE_OBS_TAREFA_4, $IN_ANALISE_AMOSTRA_5, $IN_ANALISE_DATA_AMOSTRAGEM_5, $IN_ANALISE_TAREFA_EXEC_5, $IN_ANALISE_PROC_PROD_5, $IN_ANALISE_OBS_TAREFA_5, $IN_ANALISE_AMOSTRA_6, $IN_ANALISE_DATA_AMOSTRAGEM_6, $IN_ANALISE_TAREFA_EXEC_6, $IN_ANALISE_PROC_PROD_6, $IN_ANALISE_OBS_TAREFA_6, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_MITIGACAO, $IN_COLETAS, $IN_COLETA_AMOSTRA_1, $IN_COLETA_DATA_1, $IN_COLETA_NUM_SERIAL_1, $IN_COLETA_NUM_AMOSTRADOR_1, $IN_COLETA_NUM_RELAT_ENSAIO_1, $IN_COLETA_VAZAO_1, $IN_COLETA_TEMPO_AMOSTRAGEM_1, $IN_COLETA_MASSA1_1, $IN_COLETA_MASSA2_1, $IN_COLETA_MASSA3_1, $IN_COLETA_MASSA4_1, $IN_COLETA_MASSA5_1, $IN_COLETA_AMOSTRA_2, $IN_COLETA_DATA_2, $IN_COLETA_NUM_SERIAL_2, $IN_COLETA_NUM_AMOSTRADOR_2, $IN_COLETA_NUM_RELAT_ENSAIO_2, $IN_COLETA_VAZAO_2, $IN_COLETA_TEMPO_AMOSTRAGEM_2, $IN_COLETA_MASSA1_2, $IN_COLETA_MASSA2_2, $IN_COLETA_MASSA3_2, $IN_COLETA_MASSA4_2, $IN_COLETA_MASSA5_2, $IN_COLETA_AMOSTRA_3, $IN_COLETA_DATA_3, $IN_COLETA_NUM_SERIAL_3, $IN_COLETA_NUM_AMOSTRADOR_3, $IN_COLETA_NUM_RELAT_ENSAIO_3, $IN_COLETA_VAZAO_3, $IN_COLETA_TEMPO_AMOSTRAGEM_3, $IN_COLETA_MASSA1_3, $IN_COLETA_MASSA2_3, $IN_COLETA_MASSA3_3, $IN_COLETA_MASSA4_3, $IN_COLETA_MASSA5_3, $IN_COLETA_AMOSTRA_4, $IN_COLETA_DATA_4, $IN_COLETA_NUM_SERIAL_4, $IN_COLETA_NUM_AMOSTRADOR_4, $IN_COLETA_NUM_RELAT_ENSAIO_4, $IN_COLETA_VAZAO_4, $IN_COLETA_TEMPO_AMOSTRAGEM_4, $IN_COLETA_MASSA1_4, $IN_COLETA_MASSA2_4, $IN_COLETA_MASSA3_4, $IN_COLETA_MASSA4_4, $IN_COLETA_MASSA5_4, $IN_COLETA_AMOSTRA_5, $IN_COLETA_DATA_5, $IN_COLETA_NUM_SERIAL_5, $IN_COLETA_NUM_AMOSTRADOR_5, $IN_COLETA_NUM_RELAT_ENSAIO_5, $IN_COLETA_VAZAO_5, $IN_COLETA_TEMPO_AMOSTRAGEM_5, $IN_COLETA_MASSA1_5, $IN_COLETA_MASSA2_5, $IN_COLETA_MASSA3_5, $IN_COLETA_MASSA4_5, $IN_COLETA_MASSA5_5, $IN_COLETA_AMOSTRA_6, $IN_COLETA_DATA_6, $IN_COLETA_NUM_SERIAL_6, $IN_COLETA_NUM_AMOSTRADOR_6, $IN_COLETA_NUM_RELAT_ENSAIO_6, $IN_COLETA_VAZAO_6, $IN_COLETA_TEMPO_AMOSTRAGEM_6, $IN_COLETA_MASSA1_6, $IN_COLETA_MASSA2_6, $IN_COLETA_MASSA3_6, $IN_COLETA_MASSA4_6, $IN_COLETA_MASSA5_6, $IN_RESPIRADOR, $IN_CERT_APROV, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
				
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id, upper(CLNT.`nome_interno`) as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`analises` as analises, AA.`coletas` as coletas
       FROM `VAPOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
   ORDER BY 2,3,4,5"
		)) 
		{
			$stmt->bind_param('ss', $sql_DATA_ELABORACAO, $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_DATA_ELABORACAO, $o_ANALISES, $o_COLETAS);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JAN; }
					if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_FEV; }
					if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_MAR; }
					if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_ABR; }
					if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_MAI; }
					if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JUN; }
					if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JUL; }
					if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_AGO; }
					if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_SET; }
					if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_OUT; }
					if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_NOV; }
					if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_DEZ; }
					if( $o_ANALISES == "1"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_1; }
					if( $o_ANALISES == "2"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_2; }
					if( $o_ANALISES == "3"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_3; }
					if( $o_ANALISES == "4"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_4; }
					if( $o_ANALISES == "5"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_5; }
					if( $o_ANALISES == "6"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_6; }
					if( $o_COLETAS == "1"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_1; }
					if( $o_COLETAS == "2"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_2; }
					if( $o_COLETAS == "3"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_3; }
					if( $o_COLETAS == "4"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_4; }
					if( $o_COLETAS == "5"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_5; }
					if( $o_COLETAS == "6"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_6; }
				
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td><small>'.$o_IDCLIENTE.'</small></td>';
					$TBODY_LIST .= '<td>'.$o_ANO.'</td>';
					$TBODY_LIST .= '<td>'.$o_MES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_PLANILHA_NUM.'</td>';
					$TBODY_LIST .= '<td>'.$o_DATA_ELABORACAO.'</td>';
					$TBODY_LIST .= '<td>'.$o_ANALISES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_COLETAS_TXT.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_TAREFA_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_TAREFA_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_TAREFA_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_TAREFA_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_TAREFA_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_TAREFA_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_MITIGACAO, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_NUM_SERIAL_1, $_COLETA_NUM_AMOSTRADOR_1, $_COLETA_NUM_RELAT_ENSAIO_1, $_COLETA_VAZAO_1, $_COLETA_TEMPO_AMOSTRAGEM_1, $_COLETA_MASSA1_1, $_COLETA_MASSA2_1, $_COLETA_MASSA3_1, $_COLETA_MASSA4_1, $_COLETA_MASSA5_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_NUM_SERIAL_2, $_COLETA_NUM_AMOSTRADOR_2, $_COLETA_NUM_RELAT_ENSAIO_2, $_COLETA_VAZAO_2, $_COLETA_TEMPO_AMOSTRAGEM_2, $_COLETA_MASSA1_2, $_COLETA_MASSA2_2, $_COLETA_MASSA3_2, $_COLETA_MASSA4_2, $_COLETA_MASSA5_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_NUM_SERIAL_3, $_COLETA_NUM_AMOSTRADOR_3, $_COLETA_NUM_RELAT_ENSAIO_3, $_COLETA_VAZAO_3, $_COLETA_TEMPO_AMOSTRAGEM_3, $_COLETA_MASSA1_3, $_COLETA_MASSA2_3, $_COLETA_MASSA3_3, $_COLETA_MASSA4_3, $_COLETA_MASSA5_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_NUM_SERIAL_4, $_COLETA_NUM_AMOSTRADOR_4, $_COLETA_NUM_RELAT_ENSAIO_4, $_COLETA_VAZAO_4, $_COLETA_TEMPO_AMOSTRAGEM_4, $_COLETA_MASSA1_4, $_COLETA_MASSA2_4, $_COLETA_MASSA3_4, $_COLETA_MASSA4_4, $_COLETA_MASSA5_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_NUM_SERIAL_5, $_COLETA_NUM_AMOSTRADOR_5, $_COLETA_NUM_RELAT_ENSAIO_5, $_COLETA_VAZAO_5, $_COLETA_TEMPO_AMOSTRAGEM_5, $_COLETA_MASSA1_5, $_COLETA_MASSA2_5, $_COLETA_MASSA3_5, $_COLETA_MASSA4_5, $_COLETA_MASSA5_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_NUM_SERIAL_6, $_COLETA_NUM_AMOSTRADOR_6, $_COLETA_NUM_RELAT_ENSAIO_6, $_COLETA_VAZAO_6, $_COLETA_TEMPO_AMOSTRAGEM_6, $_COLETA_MASSA1_6, $_COLETA_MASSA2_6, $_COLETA_MASSA3_6, $_COLETA_MASSA4_6, $_COLETA_MASSA5_6, $_RESPIRADOR, $_CERT_APROV, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id
       FROM `VAPOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_TAREFA_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_TAREFA_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_TAREFA_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_TAREFA_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_TAREFA_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_TAREFA_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_MITIGACAO, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_NUM_SERIAL_1, $_COLETA_NUM_AMOSTRADOR_1, $_COLETA_NUM_RELAT_ENSAIO_1, $_COLETA_VAZAO_1, $_COLETA_TEMPO_AMOSTRAGEM_1, $_COLETA_MASSA1_1, $_COLETA_MASSA2_1, $_COLETA_MASSA3_1, $_COLETA_MASSA4_1, $_COLETA_MASSA5_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_NUM_SERIAL_2, $_COLETA_NUM_AMOSTRADOR_2, $_COLETA_NUM_RELAT_ENSAIO_2, $_COLETA_VAZAO_2, $_COLETA_TEMPO_AMOSTRAGEM_2, $_COLETA_MASSA1_2, $_COLETA_MASSA2_2, $_COLETA_MASSA3_2, $_COLETA_MASSA4_2, $_COLETA_MASSA5_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_NUM_SERIAL_3, $_COLETA_NUM_AMOSTRADOR_3, $_COLETA_NUM_RELAT_ENSAIO_3, $_COLETA_VAZAO_3, $_COLETA_TEMPO_AMOSTRAGEM_3, $_COLETA_MASSA1_3, $_COLETA_MASSA2_3, $_COLETA_MASSA3_3, $_COLETA_MASSA4_3, $_COLETA_MASSA5_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_NUM_SERIAL_4, $_COLETA_NUM_AMOSTRADOR_4, $_COLETA_NUM_RELAT_ENSAIO_4, $_COLETA_VAZAO_4, $_COLETA_TEMPO_AMOSTRAGEM_4, $_COLETA_MASSA1_4, $_COLETA_MASSA2_4, $_COLETA_MASSA3_4, $_COLETA_MASSA4_4, $_COLETA_MASSA5_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_NUM_SERIAL_5, $_COLETA_NUM_AMOSTRADOR_5, $_COLETA_NUM_RELAT_ENSAIO_5, $_COLETA_VAZAO_5, $_COLETA_TEMPO_AMOSTRAGEM_5, $_COLETA_MASSA1_5, $_COLETA_MASSA2_5, $_COLETA_MASSA3_5, $_COLETA_MASSA4_5, $_COLETA_MASSA5_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_NUM_SERIAL_6, $_COLETA_NUM_AMOSTRADOR_6, $_COLETA_NUM_RELAT_ENSAIO_6, $_COLETA_VAZAO_6, $_COLETA_TEMPO_AMOSTRAGEM_6, $_COLETA_MASSA1_6, $_COLETA_MASSA2_6, $_COLETA_MASSA3_6, $_COLETA_MASSA4_6, $_COLETA_MASSA5_6, $_RESPIRADOR, $_CERT_APROV, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_filename = "";
		if($_FILES["img_ativ_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE - ".$_FILES["img_ativ_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_PROC_PROD_1 = $mysqli->escape_String($_ANALISE_PROC_PROD_1);
		$sql_ANALISE_OBS_TAREFA_1 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_PROC_PROD_2 = $mysqli->escape_String($_ANALISE_PROC_PROD_2);
		$sql_ANALISE_OBS_TAREFA_2 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_PROC_PROD_3 = $mysqli->escape_String($_ANALISE_PROC_PROD_3);
		$sql_ANALISE_OBS_TAREFA_3 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_PROC_PROD_4 = $mysqli->escape_String($_ANALISE_PROC_PROD_4);
		$sql_ANALISE_OBS_TAREFA_4 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_PROC_PROD_5 = $mysqli->escape_String($_ANALISE_PROC_PROD_5);
		$sql_ANALISE_OBS_TAREFA_5 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_ANALISE_PROC_PROD_6 = $mysqli->escape_String($_ANALISE_PROC_PROD_6);
		$sql_ANALISE_OBS_TAREFA_6 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_6);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_MITIGACAO = $mysqli->escape_String($_MITIGACAO);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_AMOSTRA_1 = $mysqli->escape_String($_COLETA_AMOSTRA_1);
		$sql_COLETA_DATA_1 = $mysqli->escape_String($_COLETA_DATA_1);
		$sql_COLETA_NUM_SERIAL_1 = $mysqli->escape_String($_COLETA_NUM_SERIAL_1);
		$sql_COLETA_NUM_AMOSTRADOR_1 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_1);
		$sql_COLETA_NUM_RELAT_ENSAIO_1 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_1);
		$sql_COLETA_VAZAO_1 = $mysqli->escape_String($_COLETA_VAZAO_1);
		$sql_COLETA_TEMPO_AMOSTRAGEM_1 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_1);
		$sql_COLETA_MASSA1_1 = $mysqli->escape_String($_COLETA_MASSA1_1);
		$sql_COLETA_MASSA2_1 = $mysqli->escape_String($_COLETA_MASSA2_1);
		$sql_COLETA_MASSA3_1 = $mysqli->escape_String($_COLETA_MASSA3_1);
		$sql_COLETA_MASSA4_1 = $mysqli->escape_String($_COLETA_MASSA4_1);
		$sql_COLETA_MASSA5_1 = $mysqli->escape_String($_COLETA_MASSA5_1);
		$sql_COLETA_AMOSTRA_2 = $mysqli->escape_String($_COLETA_AMOSTRA_2);
		$sql_COLETA_DATA_2 = $mysqli->escape_String($_COLETA_DATA_2);
		$sql_COLETA_NUM_SERIAL_2 = $mysqli->escape_String($_COLETA_NUM_SERIAL_2);
		$sql_COLETA_NUM_AMOSTRADOR_2 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_2);
		$sql_COLETA_NUM_RELAT_ENSAIO_2 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_2);
		$sql_COLETA_VAZAO_2 = $mysqli->escape_String($_COLETA_VAZAO_2);
		$sql_COLETA_TEMPO_AMOSTRAGEM_2 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_2);
		$sql_COLETA_MASSA1_2 = $mysqli->escape_String($_COLETA_MASSA1_2);
		$sql_COLETA_MASSA2_2 = $mysqli->escape_String($_COLETA_MASSA2_2);
		$sql_COLETA_MASSA3_2 = $mysqli->escape_String($_COLETA_MASSA3_2);
		$sql_COLETA_MASSA4_2 = $mysqli->escape_String($_COLETA_MASSA4_2);
		$sql_COLETA_MASSA5_2 = $mysqli->escape_String($_COLETA_MASSA5_2);
		$sql_COLETA_AMOSTRA_3 = $mysqli->escape_String($_COLETA_AMOSTRA_3);
		$sql_COLETA_DATA_3 = $mysqli->escape_String($_COLETA_DATA_3);
		$sql_COLETA_NUM_SERIAL_3 = $mysqli->escape_String($_COLETA_NUM_SERIAL_3);
		$sql_COLETA_NUM_AMOSTRADOR_3 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_3);
		$sql_COLETA_NUM_RELAT_ENSAIO_3 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_3);
		$sql_COLETA_VAZAO_3 = $mysqli->escape_String($_COLETA_VAZAO_3);
		$sql_COLETA_TEMPO_AMOSTRAGEM_3 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_3);
		$sql_COLETA_MASSA1_3 = $mysqli->escape_String($_COLETA_MASSA1_3);
		$sql_COLETA_MASSA2_3 = $mysqli->escape_String($_COLETA_MASSA2_3);
		$sql_COLETA_MASSA3_3 = $mysqli->escape_String($_COLETA_MASSA3_3);
		$sql_COLETA_MASSA4_3 = $mysqli->escape_String($_COLETA_MASSA4_3);
		$sql_COLETA_MASSA5_3 = $mysqli->escape_String($_COLETA_MASSA5_3);
		$sql_COLETA_AMOSTRA_4 = $mysqli->escape_String($_COLETA_AMOSTRA_4);
		$sql_COLETA_DATA_4 = $mysqli->escape_String($_COLETA_DATA_4);
		$sql_COLETA_NUM_SERIAL_4 = $mysqli->escape_String($_COLETA_NUM_SERIAL_4);
		$sql_COLETA_NUM_AMOSTRADOR_4 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_4);
		$sql_COLETA_NUM_RELAT_ENSAIO_4 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_4);
		$sql_COLETA_VAZAO_4 = $mysqli->escape_String($_COLETA_VAZAO_4);
		$sql_COLETA_TEMPO_AMOSTRAGEM_4 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_4);
		$sql_COLETA_MASSA1_4 = $mysqli->escape_String($_COLETA_MASSA1_4);
		$sql_COLETA_MASSA2_4 = $mysqli->escape_String($_COLETA_MASSA2_4);
		$sql_COLETA_MASSA3_4 = $mysqli->escape_String($_COLETA_MASSA3_4);
		$sql_COLETA_MASSA4_4 = $mysqli->escape_String($_COLETA_MASSA4_4);
		$sql_COLETA_MASSA5_4 = $mysqli->escape_String($_COLETA_MASSA5_4);
		$sql_COLETA_AMOSTRA_5 = $mysqli->escape_String($_COLETA_AMOSTRA_5);
		$sql_COLETA_DATA_5 = $mysqli->escape_String($_COLETA_DATA_5);
		$sql_COLETA_NUM_SERIAL_5 = $mysqli->escape_String($_COLETA_NUM_SERIAL_5);
		$sql_COLETA_NUM_AMOSTRADOR_5 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_5);
		$sql_COLETA_NUM_RELAT_ENSAIO_5 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_5);
		$sql_COLETA_VAZAO_5 = $mysqli->escape_String($_COLETA_VAZAO_5);
		$sql_COLETA_TEMPO_AMOSTRAGEM_5 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_5);
		$sql_COLETA_MASSA1_5 = $mysqli->escape_String($_COLETA_MASSA1_5);
		$sql_COLETA_MASSA2_5 = $mysqli->escape_String($_COLETA_MASSA2_5);
		$sql_COLETA_MASSA3_5 = $mysqli->escape_String($_COLETA_MASSA3_5);
		$sql_COLETA_MASSA4_5 = $mysqli->escape_String($_COLETA_MASSA4_5);
		$sql_COLETA_MASSA5_5 = $mysqli->escape_String($_COLETA_MASSA5_5);
		$sql_COLETA_AMOSTRA_6 = $mysqli->escape_String($_COLETA_AMOSTRA_6);
		$sql_COLETA_DATA_6 = $mysqli->escape_String($_COLETA_DATA_6);
		$sql_COLETA_NUM_SERIAL_6 = $mysqli->escape_String($_COLETA_NUM_SERIAL_6);
		$sql_COLETA_NUM_AMOSTRADOR_6 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_6);
		$sql_COLETA_NUM_RELAT_ENSAIO_6 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_6);
		$sql_COLETA_VAZAO_6 = $mysqli->escape_String($_COLETA_VAZAO_6);
		$sql_COLETA_TEMPO_AMOSTRAGEM_6 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_6);
		$sql_COLETA_MASSA1_6 = $mysqli->escape_String($_COLETA_MASSA1_6);
		$sql_COLETA_MASSA2_6 = $mysqli->escape_String($_COLETA_MASSA2_6);
		$sql_COLETA_MASSA3_6 = $mysqli->escape_String($_COLETA_MASSA3_6);
		$sql_COLETA_MASSA4_6 = $mysqli->escape_String($_COLETA_MASSA4_6);
		$sql_COLETA_MASSA5_6 = $mysqli->escape_String($_COLETA_MASSA5_6);
		$sql_RESPIRADOR = $mysqli->escape_String($_RESPIRADOR);
		$sql_CERT_APROV = $mysqli->escape_String($_CERT_APROV);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		
		## Verifica se o registro já está cadastrado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id
       FROM `VAPOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `VAPOR` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_proc_prod_1` = ?
		        ,`analise_obs_tarefa_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_proc_prod_2` = ?
		        ,`analise_obs_tarefa_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_proc_prod_3` = ?
		        ,`analise_obs_tarefa_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_proc_prod_4` = ?
		        ,`analise_obs_tarefa_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_proc_prod_5` = ?
		        ,`analise_obs_tarefa_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`analise_proc_prod_6` = ?
		        ,`analise_obs_tarefa_6` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`mitigacao` = ?
		        ,`coletas` = ?
		        ,`coleta_amostra_1` = ?
		        ,`coleta_data_1` = ?
		        ,`coleta_num_serial_1` = ?
		        ,`coleta_num_amostrador_1` = ?
		        ,`coleta_num_relat_ensaio_1` = ?
		        ,`coleta_vazao_1` = ?
		        ,`coleta_tempo_amostragem_1` = ?
		        ,`coleta_massa1_1` = ?
		        ,`coleta_massa2_1` = ?
		        ,`coleta_massa3_1` = ?
		        ,`coleta_massa4_1` = ?
		        ,`coleta_massa5_1` = ?
		        ,`coleta_amostra_2` = ?
		        ,`coleta_data_2` = ?
		        ,`coleta_num_serial_2` = ?
		        ,`coleta_num_amostrador_2` = ?
		        ,`coleta_num_relat_ensaio_2` = ?
		        ,`coleta_vazao_2` = ?
		        ,`coleta_tempo_amostragem_2` = ?
		        ,`coleta_massa1_2` = ?
		        ,`coleta_massa2_2` = ?
		        ,`coleta_massa3_2` = ?
		        ,`coleta_massa4_2` = ?
		        ,`coleta_massa5_2` = ?
		        ,`coleta_amostra_3` = ?
		        ,`coleta_data_3` = ?
		        ,`coleta_num_serial_3` = ?
		        ,`coleta_num_amostrador_3` = ?
		        ,`coleta_num_relat_ensaio_3` = ?
		        ,`coleta_vazao_3` = ?
		        ,`coleta_tempo_amostragem_3` = ?
		        ,`coleta_massa1_3` = ?
		        ,`coleta_massa2_3` = ?
		        ,`coleta_massa3_3` = ?
		        ,`coleta_massa4_3` = ?
		        ,`coleta_massa5_3` = ?
		        ,`coleta_amostra_4` = ?
		        ,`coleta_data_4` = ?
		        ,`coleta_num_serial_4` = ?
		        ,`coleta_num_amostrador_4` = ?
		        ,`coleta_num_relat_ensaio_4` = ?
		        ,`coleta_vazao_4` = ?
		        ,`coleta_tempo_amostragem_4` = ?
		        ,`coleta_massa1_4` = ?
		        ,`coleta_massa2_4` = ?
		        ,`coleta_massa3_4` = ?
		        ,`coleta_massa4_4` = ?
		        ,`coleta_massa5_4` = ?
		        ,`coleta_amostra_5` = ?
		        ,`coleta_data_5` = ?
		        ,`coleta_num_serial_5` = ?
		        ,`coleta_num_amostrador_5` = ?
		        ,`coleta_num_relat_ensaio_5` = ?
		        ,`coleta_vazao_5` = ?
		        ,`coleta_tempo_amostragem_5` = ?
		        ,`coleta_massa1_5` = ?
		        ,`coleta_massa2_5` = ?
		        ,`coleta_massa3_5` = ?
		        ,`coleta_massa4_5` = ?
		        ,`coleta_massa5_5` = ?
		        ,`coleta_amostra_6` = ?
		        ,`coleta_data_6` = ?
		        ,`coleta_num_serial_6` = ?
		        ,`coleta_num_amostrador_6` = ?
		        ,`coleta_num_relat_ensaio_6` = ?
		        ,`coleta_vazao_6` = ?
		        ,`coleta_tempo_amostragem_6` = ?
		        ,`coleta_massa1_6` = ?
		        ,`coleta_massa2_6` = ?
		        ,`coleta_massa3_6` = ?
		        ,`coleta_massa4_6` = ?
		        ,`coleta_massa5_6` = ?
		        ,`respirador` = ?
		        ,`cert_aprov` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		        ,`img_ativ_filename` = ?
		        ,`logo_filename` = ?
"
		)) 
		{
			$stmt->bind_param('ssddddssssssssdssssssssssssssssssssssssssssssssssssdsssssdddddddsssssdddddddsssssdddddddsssssdddddddsssssdddddddsssssdddddddssddssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_PROC_PROD_1, $sql_ANALISE_OBS_TAREFA_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_PROC_PROD_2, $sql_ANALISE_OBS_TAREFA_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_PROC_PROD_3, $sql_ANALISE_OBS_TAREFA_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_PROC_PROD_4, $sql_ANALISE_OBS_TAREFA_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_PROC_PROD_5, $sql_ANALISE_OBS_TAREFA_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_ANALISE_PROC_PROD_6, $sql_ANALISE_OBS_TAREFA_6, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_MITIGACAO, $sql_COLETAS, $sql_COLETA_AMOSTRA_1, $sql_COLETA_DATA_1, $sql_COLETA_NUM_SERIAL_1, $sql_COLETA_NUM_AMOSTRADOR_1, $sql_COLETA_NUM_RELAT_ENSAIO_1, $sql_COLETA_VAZAO_1, $sql_COLETA_TEMPO_AMOSTRAGEM_1, $sql_COLETA_MASSA1_1, $sql_COLETA_MASSA2_1, $sql_COLETA_MASSA3_1, $sql_COLETA_MASSA4_1, $sql_COLETA_MASSA5_1, $sql_COLETA_AMOSTRA_2, $sql_COLETA_DATA_2, $sql_COLETA_NUM_SERIAL_2, $sql_COLETA_NUM_AMOSTRADOR_2, $sql_COLETA_NUM_RELAT_ENSAIO_2, $sql_COLETA_VAZAO_2, $sql_COLETA_TEMPO_AMOSTRAGEM_2, $sql_COLETA_MASSA1_2, $sql_COLETA_MASSA2_2, $sql_COLETA_MASSA3_2, $sql_COLETA_MASSA4_2, $sql_COLETA_MASSA5_2, $sql_COLETA_AMOSTRA_3, $sql_COLETA_DATA_3, $sql_COLETA_NUM_SERIAL_3, $sql_COLETA_NUM_AMOSTRADOR_3, $sql_COLETA_NUM_RELAT_ENSAIO_3, $sql_COLETA_VAZAO_3, $sql_COLETA_TEMPO_AMOSTRAGEM_3, $sql_COLETA_MASSA1_3, $sql_COLETA_MASSA2_3, $sql_COLETA_MASSA3_3, $sql_COLETA_MASSA4_3, $sql_COLETA_MASSA5_3, $sql_COLETA_AMOSTRA_4, $sql_COLETA_DATA_4, $sql_COLETA_NUM_SERIAL_4, $sql_COLETA_NUM_AMOSTRADOR_4, $sql_COLETA_NUM_RELAT_ENSAIO_4, $sql_COLETA_VAZAO_4, $sql_COLETA_TEMPO_AMOSTRAGEM_4, $sql_COLETA_MASSA1_4, $sql_COLETA_MASSA2_4, $sql_COLETA_MASSA3_4, $sql_COLETA_MASSA4_4, $sql_COLETA_MASSA5_4, $sql_COLETA_AMOSTRA_5, $sql_COLETA_DATA_5, $sql_COLETA_NUM_SERIAL_5, $sql_COLETA_NUM_AMOSTRADOR_5, $sql_COLETA_NUM_RELAT_ENSAIO_5, $sql_COLETA_VAZAO_5, $sql_COLETA_TEMPO_AMOSTRAGEM_5, $sql_COLETA_MASSA1_5, $sql_COLETA_MASSA2_5, $sql_COLETA_MASSA3_5, $sql_COLETA_MASSA4_5, $sql_COLETA_MASSA5_5, $sql_COLETA_AMOSTRA_6, $sql_COLETA_DATA_6, $sql_COLETA_NUM_SERIAL_6, $sql_COLETA_NUM_AMOSTRADOR_6, $sql_COLETA_NUM_RELAT_ENSAIO_6, $sql_COLETA_VAZAO_6, $sql_COLETA_TEMPO_AMOSTRAGEM_6, $sql_COLETA_MASSA1_6, $sql_COLETA_MASSA2_6, $sql_COLETA_MASSA3_6, $sql_COLETA_MASSA4_6, $sql_COLETA_MASSA5_6, $sql_RESPIRADOR, $sql_CERT_APROV, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_IMG_ATIV_FILENAME_filename, $sql_LOGO_FILENAME_filename);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_ANALISES, $_ANALISE_AMOSTRA_1, $_ANALISE_DATA_AMOSTRAGEM_1, $_ANALISE_TAREFA_EXEC_1, $_ANALISE_PROC_PROD_1, $_ANALISE_OBS_TAREFA_1, $_ANALISE_AMOSTRA_2, $_ANALISE_DATA_AMOSTRAGEM_2, $_ANALISE_TAREFA_EXEC_2, $_ANALISE_PROC_PROD_2, $_ANALISE_OBS_TAREFA_2, $_ANALISE_AMOSTRA_3, $_ANALISE_DATA_AMOSTRAGEM_3, $_ANALISE_TAREFA_EXEC_3, $_ANALISE_PROC_PROD_3, $_ANALISE_OBS_TAREFA_3, $_ANALISE_AMOSTRA_4, $_ANALISE_DATA_AMOSTRAGEM_4, $_ANALISE_TAREFA_EXEC_4, $_ANALISE_PROC_PROD_4, $_ANALISE_OBS_TAREFA_4, $_ANALISE_AMOSTRA_5, $_ANALISE_DATA_AMOSTRAGEM_5, $_ANALISE_TAREFA_EXEC_5, $_ANALISE_PROC_PROD_5, $_ANALISE_OBS_TAREFA_5, $_ANALISE_AMOSTRA_6, $_ANALISE_DATA_AMOSTRAGEM_6, $_ANALISE_TAREFA_EXEC_6, $_ANALISE_PROC_PROD_6, $_ANALISE_OBS_TAREFA_6, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_MITIGACAO, $_COLETAS, $_COLETA_AMOSTRA_1, $_COLETA_DATA_1, $_COLETA_NUM_SERIAL_1, $_COLETA_NUM_AMOSTRADOR_1, $_COLETA_NUM_RELAT_ENSAIO_1, $_COLETA_VAZAO_1, $_COLETA_TEMPO_AMOSTRAGEM_1, $_COLETA_MASSA1_1, $_COLETA_MASSA2_1, $_COLETA_MASSA3_1, $_COLETA_MASSA4_1, $_COLETA_MASSA5_1, $_COLETA_AMOSTRA_2, $_COLETA_DATA_2, $_COLETA_NUM_SERIAL_2, $_COLETA_NUM_AMOSTRADOR_2, $_COLETA_NUM_RELAT_ENSAIO_2, $_COLETA_VAZAO_2, $_COLETA_TEMPO_AMOSTRAGEM_2, $_COLETA_MASSA1_2, $_COLETA_MASSA2_2, $_COLETA_MASSA3_2, $_COLETA_MASSA4_2, $_COLETA_MASSA5_2, $_COLETA_AMOSTRA_3, $_COLETA_DATA_3, $_COLETA_NUM_SERIAL_3, $_COLETA_NUM_AMOSTRADOR_3, $_COLETA_NUM_RELAT_ENSAIO_3, $_COLETA_VAZAO_3, $_COLETA_TEMPO_AMOSTRAGEM_3, $_COLETA_MASSA1_3, $_COLETA_MASSA2_3, $_COLETA_MASSA3_3, $_COLETA_MASSA4_3, $_COLETA_MASSA5_3, $_COLETA_AMOSTRA_4, $_COLETA_DATA_4, $_COLETA_NUM_SERIAL_4, $_COLETA_NUM_AMOSTRADOR_4, $_COLETA_NUM_RELAT_ENSAIO_4, $_COLETA_VAZAO_4, $_COLETA_TEMPO_AMOSTRAGEM_4, $_COLETA_MASSA1_4, $_COLETA_MASSA2_4, $_COLETA_MASSA3_4, $_COLETA_MASSA4_4, $_COLETA_MASSA5_4, $_COLETA_AMOSTRA_5, $_COLETA_DATA_5, $_COLETA_NUM_SERIAL_5, $_COLETA_NUM_AMOSTRADOR_5, $_COLETA_NUM_RELAT_ENSAIO_5, $_COLETA_VAZAO_5, $_COLETA_TEMPO_AMOSTRAGEM_5, $_COLETA_MASSA1_5, $_COLETA_MASSA2_5, $_COLETA_MASSA3_5, $_COLETA_MASSA4_5, $_COLETA_MASSA5_5, $_COLETA_AMOSTRA_6, $_COLETA_DATA_6, $_COLETA_NUM_SERIAL_6, $_COLETA_NUM_AMOSTRADOR_6, $_COLETA_NUM_RELAT_ENSAIO_6, $_COLETA_VAZAO_6, $_COLETA_TEMPO_AMOSTRAGEM_6, $_COLETA_MASSA1_6, $_COLETA_MASSA2_6, $_COLETA_MASSA3_6, $_COLETA_MASSA4_6, $_COLETA_MASSA5_6, $_RESPIRADOR, $_CERT_APROV, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_filename = "";
		if($_FILES["img_ativ_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE - ".$_FILES["img_ativ_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_ANALISES = $mysqli->escape_String($_ANALISES);
		$sql_ANALISE_AMOSTRA_1 = $mysqli->escape_String($_ANALISE_AMOSTRA_1);
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_1);
		$sql_ANALISE_TAREFA_EXEC_1 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_1);
		$sql_ANALISE_PROC_PROD_1 = $mysqli->escape_String($_ANALISE_PROC_PROD_1);
		$sql_ANALISE_OBS_TAREFA_1 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_1);
		$sql_ANALISE_AMOSTRA_2 = $mysqli->escape_String($_ANALISE_AMOSTRA_2);
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_2);
		$sql_ANALISE_TAREFA_EXEC_2 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_2);
		$sql_ANALISE_PROC_PROD_2 = $mysqli->escape_String($_ANALISE_PROC_PROD_2);
		$sql_ANALISE_OBS_TAREFA_2 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_2);
		$sql_ANALISE_AMOSTRA_3 = $mysqli->escape_String($_ANALISE_AMOSTRA_3);
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_3);
		$sql_ANALISE_TAREFA_EXEC_3 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_3);
		$sql_ANALISE_PROC_PROD_3 = $mysqli->escape_String($_ANALISE_PROC_PROD_3);
		$sql_ANALISE_OBS_TAREFA_3 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_3);
		$sql_ANALISE_AMOSTRA_4 = $mysqli->escape_String($_ANALISE_AMOSTRA_4);
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_4);
		$sql_ANALISE_TAREFA_EXEC_4 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_4);
		$sql_ANALISE_PROC_PROD_4 = $mysqli->escape_String($_ANALISE_PROC_PROD_4);
		$sql_ANALISE_OBS_TAREFA_4 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_4);
		$sql_ANALISE_AMOSTRA_5 = $mysqli->escape_String($_ANALISE_AMOSTRA_5);
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_5);
		$sql_ANALISE_TAREFA_EXEC_5 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_5);
		$sql_ANALISE_PROC_PROD_5 = $mysqli->escape_String($_ANALISE_PROC_PROD_5);
		$sql_ANALISE_OBS_TAREFA_5 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_5);
		$sql_ANALISE_AMOSTRA_6 = $mysqli->escape_String($_ANALISE_AMOSTRA_6);
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = $mysqli->escape_String($_ANALISE_DATA_AMOSTRAGEM_6);
		$sql_ANALISE_TAREFA_EXEC_6 = $mysqli->escape_String($_ANALISE_TAREFA_EXEC_6);
		$sql_ANALISE_PROC_PROD_6 = $mysqli->escape_String($_ANALISE_PROC_PROD_6);
		$sql_ANALISE_OBS_TAREFA_6 = $mysqli->escape_String($_ANALISE_OBS_TAREFA_6);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_MITIGACAO = $mysqli->escape_String($_MITIGACAO);
		$sql_COLETAS = $mysqli->escape_String($_COLETAS);
		$sql_COLETA_AMOSTRA_1 = $mysqli->escape_String($_COLETA_AMOSTRA_1);
		$sql_COLETA_DATA_1 = $mysqli->escape_String($_COLETA_DATA_1);
		$sql_COLETA_NUM_SERIAL_1 = $mysqli->escape_String($_COLETA_NUM_SERIAL_1);
		$sql_COLETA_NUM_AMOSTRADOR_1 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_1);
		$sql_COLETA_NUM_RELAT_ENSAIO_1 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_1);
		$sql_COLETA_VAZAO_1 = $mysqli->escape_String($_COLETA_VAZAO_1);
		$sql_COLETA_TEMPO_AMOSTRAGEM_1 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_1);
		$sql_COLETA_MASSA1_1 = $mysqli->escape_String($_COLETA_MASSA1_1);
		$sql_COLETA_MASSA2_1 = $mysqli->escape_String($_COLETA_MASSA2_1);
		$sql_COLETA_MASSA3_1 = $mysqli->escape_String($_COLETA_MASSA3_1);
		$sql_COLETA_MASSA4_1 = $mysqli->escape_String($_COLETA_MASSA4_1);
		$sql_COLETA_MASSA5_1 = $mysqli->escape_String($_COLETA_MASSA5_1);
		$sql_COLETA_AMOSTRA_2 = $mysqli->escape_String($_COLETA_AMOSTRA_2);
		$sql_COLETA_DATA_2 = $mysqli->escape_String($_COLETA_DATA_2);
		$sql_COLETA_NUM_SERIAL_2 = $mysqli->escape_String($_COLETA_NUM_SERIAL_2);
		$sql_COLETA_NUM_AMOSTRADOR_2 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_2);
		$sql_COLETA_NUM_RELAT_ENSAIO_2 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_2);
		$sql_COLETA_VAZAO_2 = $mysqli->escape_String($_COLETA_VAZAO_2);
		$sql_COLETA_TEMPO_AMOSTRAGEM_2 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_2);
		$sql_COLETA_MASSA1_2 = $mysqli->escape_String($_COLETA_MASSA1_2);
		$sql_COLETA_MASSA2_2 = $mysqli->escape_String($_COLETA_MASSA2_2);
		$sql_COLETA_MASSA3_2 = $mysqli->escape_String($_COLETA_MASSA3_2);
		$sql_COLETA_MASSA4_2 = $mysqli->escape_String($_COLETA_MASSA4_2);
		$sql_COLETA_MASSA5_2 = $mysqli->escape_String($_COLETA_MASSA5_2);
		$sql_COLETA_AMOSTRA_3 = $mysqli->escape_String($_COLETA_AMOSTRA_3);
		$sql_COLETA_DATA_3 = $mysqli->escape_String($_COLETA_DATA_3);
		$sql_COLETA_NUM_SERIAL_3 = $mysqli->escape_String($_COLETA_NUM_SERIAL_3);
		$sql_COLETA_NUM_AMOSTRADOR_3 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_3);
		$sql_COLETA_NUM_RELAT_ENSAIO_3 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_3);
		$sql_COLETA_VAZAO_3 = $mysqli->escape_String($_COLETA_VAZAO_3);
		$sql_COLETA_TEMPO_AMOSTRAGEM_3 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_3);
		$sql_COLETA_MASSA1_3 = $mysqli->escape_String($_COLETA_MASSA1_3);
		$sql_COLETA_MASSA2_3 = $mysqli->escape_String($_COLETA_MASSA2_3);
		$sql_COLETA_MASSA3_3 = $mysqli->escape_String($_COLETA_MASSA3_3);
		$sql_COLETA_MASSA4_3 = $mysqli->escape_String($_COLETA_MASSA4_3);
		$sql_COLETA_MASSA5_3 = $mysqli->escape_String($_COLETA_MASSA5_3);
		$sql_COLETA_AMOSTRA_4 = $mysqli->escape_String($_COLETA_AMOSTRA_4);
		$sql_COLETA_DATA_4 = $mysqli->escape_String($_COLETA_DATA_4);
		$sql_COLETA_NUM_SERIAL_4 = $mysqli->escape_String($_COLETA_NUM_SERIAL_4);
		$sql_COLETA_NUM_AMOSTRADOR_4 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_4);
		$sql_COLETA_NUM_RELAT_ENSAIO_4 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_4);
		$sql_COLETA_VAZAO_4 = $mysqli->escape_String($_COLETA_VAZAO_4);
		$sql_COLETA_TEMPO_AMOSTRAGEM_4 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_4);
		$sql_COLETA_MASSA1_4 = $mysqli->escape_String($_COLETA_MASSA1_4);
		$sql_COLETA_MASSA2_4 = $mysqli->escape_String($_COLETA_MASSA2_4);
		$sql_COLETA_MASSA3_4 = $mysqli->escape_String($_COLETA_MASSA3_4);
		$sql_COLETA_MASSA4_4 = $mysqli->escape_String($_COLETA_MASSA4_4);
		$sql_COLETA_MASSA5_4 = $mysqli->escape_String($_COLETA_MASSA5_4);
		$sql_COLETA_AMOSTRA_5 = $mysqli->escape_String($_COLETA_AMOSTRA_5);
		$sql_COLETA_DATA_5 = $mysqli->escape_String($_COLETA_DATA_5);
		$sql_COLETA_NUM_SERIAL_5 = $mysqli->escape_String($_COLETA_NUM_SERIAL_5);
		$sql_COLETA_NUM_AMOSTRADOR_5 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_5);
		$sql_COLETA_NUM_RELAT_ENSAIO_5 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_5);
		$sql_COLETA_VAZAO_5 = $mysqli->escape_String($_COLETA_VAZAO_5);
		$sql_COLETA_TEMPO_AMOSTRAGEM_5 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_5);
		$sql_COLETA_MASSA1_5 = $mysqli->escape_String($_COLETA_MASSA1_5);
		$sql_COLETA_MASSA2_5 = $mysqli->escape_String($_COLETA_MASSA2_5);
		$sql_COLETA_MASSA3_5 = $mysqli->escape_String($_COLETA_MASSA3_5);
		$sql_COLETA_MASSA4_5 = $mysqli->escape_String($_COLETA_MASSA4_5);
		$sql_COLETA_MASSA5_5 = $mysqli->escape_String($_COLETA_MASSA5_5);
		$sql_COLETA_AMOSTRA_6 = $mysqli->escape_String($_COLETA_AMOSTRA_6);
		$sql_COLETA_DATA_6 = $mysqli->escape_String($_COLETA_DATA_6);
		$sql_COLETA_NUM_SERIAL_6 = $mysqli->escape_String($_COLETA_NUM_SERIAL_6);
		$sql_COLETA_NUM_AMOSTRADOR_6 = $mysqli->escape_String($_COLETA_NUM_AMOSTRADOR_6);
		$sql_COLETA_NUM_RELAT_ENSAIO_6 = $mysqli->escape_String($_COLETA_NUM_RELAT_ENSAIO_6);
		$sql_COLETA_VAZAO_6 = $mysqli->escape_String($_COLETA_VAZAO_6);
		$sql_COLETA_TEMPO_AMOSTRAGEM_6 = $mysqli->escape_String($_COLETA_TEMPO_AMOSTRAGEM_6);
		$sql_COLETA_MASSA1_6 = $mysqli->escape_String($_COLETA_MASSA1_6);
		$sql_COLETA_MASSA2_6 = $mysqli->escape_String($_COLETA_MASSA2_6);
		$sql_COLETA_MASSA3_6 = $mysqli->escape_String($_COLETA_MASSA3_6);
		$sql_COLETA_MASSA4_6 = $mysqli->escape_String($_COLETA_MASSA4_6);
		$sql_COLETA_MASSA5_6 = $mysqli->escape_String($_COLETA_MASSA5_6);
		$sql_RESPIRADOR = $mysqli->escape_String($_RESPIRADOR);
		$sql_CERT_APROV = $mysqli->escape_String($_CERT_APROV);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id, AA.`img_ativ_filename`, AA.`logo_filename`
       FROM `VAPOR` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVAPOR` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idVAPOR, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Verifica se o registro editado ja existe
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id
       FROM `VAPOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
        AND AA.`idVAPOR` != ?"
		)) 
		{
			$stmt->bind_param('sddddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua atualizacao
		if ($stmt = $mysqli->prepare(
		"UPDATE `VAPOR` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`analises` = ?
		        ,`analise_amostra_1` = ?
		        ,`analise_data_amostragem_1` = ?
		        ,`analise_tarefa_exec_1` = ?
		        ,`analise_proc_prod_1` = ?
		        ,`analise_obs_tarefa_1` = ?
		        ,`analise_amostra_2` = ?
		        ,`analise_data_amostragem_2` = ?
		        ,`analise_tarefa_exec_2` = ?
		        ,`analise_proc_prod_2` = ?
		        ,`analise_obs_tarefa_2` = ?
		        ,`analise_amostra_3` = ?
		        ,`analise_data_amostragem_3` = ?
		        ,`analise_tarefa_exec_3` = ?
		        ,`analise_proc_prod_3` = ?
		        ,`analise_obs_tarefa_3` = ?
		        ,`analise_amostra_4` = ?
		        ,`analise_data_amostragem_4` = ?
		        ,`analise_tarefa_exec_4` = ?
		        ,`analise_proc_prod_4` = ?
		        ,`analise_obs_tarefa_4` = ?
		        ,`analise_amostra_5` = ?
		        ,`analise_data_amostragem_5` = ?
		        ,`analise_tarefa_exec_5` = ?
		        ,`analise_proc_prod_5` = ?
		        ,`analise_obs_tarefa_5` = ?
		        ,`analise_amostra_6` = ?
		        ,`analise_data_amostragem_6` = ?
		        ,`analise_tarefa_exec_6` = ?
		        ,`analise_proc_prod_6` = ?
		        ,`analise_obs_tarefa_6` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`mitigacao` = ?
		        ,`coletas` = ?
		        ,`coleta_amostra_1` = ?
		        ,`coleta_data_1` = ?
		        ,`coleta_num_serial_1` = ?
		        ,`coleta_num_amostrador_1` = ?
		        ,`coleta_num_relat_ensaio_1` = ?
		        ,`coleta_vazao_1` = ?
		        ,`coleta_tempo_amostragem_1` = ?
		        ,`coleta_massa1_1` = ?
		        ,`coleta_massa2_1` = ?
		        ,`coleta_massa3_1` = ?
		        ,`coleta_massa4_1` = ?
		        ,`coleta_massa5_1` = ?
		        ,`coleta_amostra_2` = ?
		        ,`coleta_data_2` = ?
		        ,`coleta_num_serial_2` = ?
		        ,`coleta_num_amostrador_2` = ?
		        ,`coleta_num_relat_ensaio_2` = ?
		        ,`coleta_vazao_2` = ?
		        ,`coleta_tempo_amostragem_2` = ?
		        ,`coleta_massa1_2` = ?
		        ,`coleta_massa2_2` = ?
		        ,`coleta_massa3_2` = ?
		        ,`coleta_massa4_2` = ?
		        ,`coleta_massa5_2` = ?
		        ,`coleta_amostra_3` = ?
		        ,`coleta_data_3` = ?
		        ,`coleta_num_serial_3` = ?
		        ,`coleta_num_amostrador_3` = ?
		        ,`coleta_num_relat_ensaio_3` = ?
		        ,`coleta_vazao_3` = ?
		        ,`coleta_tempo_amostragem_3` = ?
		        ,`coleta_massa1_3` = ?
		        ,`coleta_massa2_3` = ?
		        ,`coleta_massa3_3` = ?
		        ,`coleta_massa4_3` = ?
		        ,`coleta_massa5_3` = ?
		        ,`coleta_amostra_4` = ?
		        ,`coleta_data_4` = ?
		        ,`coleta_num_serial_4` = ?
		        ,`coleta_num_amostrador_4` = ?
		        ,`coleta_num_relat_ensaio_4` = ?
		        ,`coleta_vazao_4` = ?
		        ,`coleta_tempo_amostragem_4` = ?
		        ,`coleta_massa1_4` = ?
		        ,`coleta_massa2_4` = ?
		        ,`coleta_massa3_4` = ?
		        ,`coleta_massa4_4` = ?
		        ,`coleta_massa5_4` = ?
		        ,`coleta_amostra_5` = ?
		        ,`coleta_data_5` = ?
		        ,`coleta_num_serial_5` = ?
		        ,`coleta_num_amostrador_5` = ?
		        ,`coleta_num_relat_ensaio_5` = ?
		        ,`coleta_vazao_5` = ?
		        ,`coleta_tempo_amostragem_5` = ?
		        ,`coleta_massa1_5` = ?
		        ,`coleta_massa2_5` = ?
		        ,`coleta_massa3_5` = ?
		        ,`coleta_massa4_5` = ?
		        ,`coleta_massa5_5` = ?
		        ,`coleta_amostra_6` = ?
		        ,`coleta_data_6` = ?
		        ,`coleta_num_serial_6` = ?
		        ,`coleta_num_amostrador_6` = ?
		        ,`coleta_num_relat_ensaio_6` = ?
		        ,`coleta_vazao_6` = ?
		        ,`coleta_tempo_amostragem_6` = ?
		        ,`coleta_massa1_6` = ?
		        ,`coleta_massa2_6` = ?
		        ,`coleta_massa3_6` = ?
		        ,`coleta_massa4_6` = ?
		        ,`coleta_massa5_6` = ?
		        ,`respirador` = ?
		        ,`cert_aprov` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idVAPOR` = ?"
		)) 
		{
			$stmt->bind_param('sddddssssssssdssssssssssssssssssssssssssssssssssssdsssssdddddddsssssdddddddsssssdddddddsssssdddddddsssssdddddddsssssdddddddssddssss', $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_ANALISES, $sql_ANALISE_AMOSTRA_1, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_TAREFA_EXEC_1, $sql_ANALISE_PROC_PROD_1, $sql_ANALISE_OBS_TAREFA_1, $sql_ANALISE_AMOSTRA_2, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_TAREFA_EXEC_2, $sql_ANALISE_PROC_PROD_2, $sql_ANALISE_OBS_TAREFA_2, $sql_ANALISE_AMOSTRA_3, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_TAREFA_EXEC_3, $sql_ANALISE_PROC_PROD_3, $sql_ANALISE_OBS_TAREFA_3, $sql_ANALISE_AMOSTRA_4, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_TAREFA_EXEC_4, $sql_ANALISE_PROC_PROD_4, $sql_ANALISE_OBS_TAREFA_4, $sql_ANALISE_AMOSTRA_5, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_TAREFA_EXEC_5, $sql_ANALISE_PROC_PROD_5, $sql_ANALISE_OBS_TAREFA_5, $sql_ANALISE_AMOSTRA_6, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_ANALISE_TAREFA_EXEC_6, $sql_ANALISE_PROC_PROD_6, $sql_ANALISE_OBS_TAREFA_6, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_MITIGACAO, $sql_COLETAS, $sql_COLETA_AMOSTRA_1, $sql_COLETA_DATA_1, $sql_COLETA_NUM_SERIAL_1, $sql_COLETA_NUM_AMOSTRADOR_1, $sql_COLETA_NUM_RELAT_ENSAIO_1, $sql_COLETA_VAZAO_1, $sql_COLETA_TEMPO_AMOSTRAGEM_1, $sql_COLETA_MASSA1_1, $sql_COLETA_MASSA2_1, $sql_COLETA_MASSA3_1, $sql_COLETA_MASSA4_1, $sql_COLETA_MASSA5_1, $sql_COLETA_AMOSTRA_2, $sql_COLETA_DATA_2, $sql_COLETA_NUM_SERIAL_2, $sql_COLETA_NUM_AMOSTRADOR_2, $sql_COLETA_NUM_RELAT_ENSAIO_2, $sql_COLETA_VAZAO_2, $sql_COLETA_TEMPO_AMOSTRAGEM_2, $sql_COLETA_MASSA1_2, $sql_COLETA_MASSA2_2, $sql_COLETA_MASSA3_2, $sql_COLETA_MASSA4_2, $sql_COLETA_MASSA5_2, $sql_COLETA_AMOSTRA_3, $sql_COLETA_DATA_3, $sql_COLETA_NUM_SERIAL_3, $sql_COLETA_NUM_AMOSTRADOR_3, $sql_COLETA_NUM_RELAT_ENSAIO_3, $sql_COLETA_VAZAO_3, $sql_COLETA_TEMPO_AMOSTRAGEM_3, $sql_COLETA_MASSA1_3, $sql_COLETA_MASSA2_3, $sql_COLETA_MASSA3_3, $sql_COLETA_MASSA4_3, $sql_COLETA_MASSA5_3, $sql_COLETA_AMOSTRA_4, $sql_COLETA_DATA_4, $sql_COLETA_NUM_SERIAL_4, $sql_COLETA_NUM_AMOSTRADOR_4, $sql_COLETA_NUM_RELAT_ENSAIO_4, $sql_COLETA_VAZAO_4, $sql_COLETA_TEMPO_AMOSTRAGEM_4, $sql_COLETA_MASSA1_4, $sql_COLETA_MASSA2_4, $sql_COLETA_MASSA3_4, $sql_COLETA_MASSA4_4, $sql_COLETA_MASSA5_4, $sql_COLETA_AMOSTRA_5, $sql_COLETA_DATA_5, $sql_COLETA_NUM_SERIAL_5, $sql_COLETA_NUM_AMOSTRADOR_5, $sql_COLETA_NUM_RELAT_ENSAIO_5, $sql_COLETA_VAZAO_5, $sql_COLETA_TEMPO_AMOSTRAGEM_5, $sql_COLETA_MASSA1_5, $sql_COLETA_MASSA2_5, $sql_COLETA_MASSA3_5, $sql_COLETA_MASSA4_5, $sql_COLETA_MASSA5_5, $sql_COLETA_AMOSTRA_6, $sql_COLETA_DATA_6, $sql_COLETA_NUM_SERIAL_6, $sql_COLETA_NUM_AMOSTRADOR_6, $sql_COLETA_NUM_RELAT_ENSAIO_6, $sql_COLETA_VAZAO_6, $sql_COLETA_TEMPO_AMOSTRAGEM_6, $sql_COLETA_MASSA1_6, $sql_COLETA_MASSA2_6, $sql_COLETA_MASSA3_6, $sql_COLETA_MASSA4_6, $sql_COLETA_MASSA5_6, $sql_RESPIRADOR, $sql_CERT_APROV, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza IMAGEM ATIVIDADE
				if($_IMG_ATIV_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `VAPOR` SET 
					        `img_ativ_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idVAPOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_IMG_ATIV_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME); }
						}
						else
						{
							return "0|IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				# Atualiza IMAGEM LOGOMARCA
				if($_LOGO_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `VAPOR` SET 
					        `logo_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idVAPOR` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_LOGO_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_LOGO_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_LOGO_FILENAME); }
						}
						else
						{
							return "0|IMAGEM LOGOMARCA - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `VAPOR` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idVAPOR` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_1 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_2 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_3 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_4 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_5 = '%d/%m/%Y';
		$sql_ANALISE_DATA_AMOSTRAGEM_6 = '%d/%m/%Y';
		$sql_COLETA_DATA_1 = '%d/%m/%Y';
		$sql_COLETA_DATA_2 = '%d/%m/%Y';
		$sql_COLETA_DATA_3 = '%d/%m/%Y';
		$sql_COLETA_DATA_4 = '%d/%m/%Y';
		$sql_COLETA_DATA_5 = '%d/%m/%Y';
		$sql_COLETA_DATA_6 = '%d/%m/%Y';
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id, AA.`idcliente` as idcliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_tarefa_1` as analise_obs_tarefa_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, AA.`analise_obs_tarefa_2` as analise_obs_tarefa_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_tarefa_3` as analise_obs_tarefa_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, AA.`analise_obs_tarefa_4` as analise_obs_tarefa_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_tarefa_5` as analise_obs_tarefa_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, AA.`analise_obs_tarefa_6` as analise_obs_tarefa_6, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`mitigacao` as mitigacao, AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_num_serial_1` as coleta_num_serial_1, AA.`coleta_num_amostrador_1` as coleta_num_amostrador_1, AA.`coleta_num_relat_ensaio_1` as coleta_num_relat_ensaio_1, AA.`coleta_vazao_1` as coleta_vazao_1, AA.`coleta_tempo_amostragem_1` as coleta_tempo_amostragem_1, AA.`coleta_massa1_1` as coleta_massa1_1, AA.`coleta_massa2_1` as coleta_massa2_1, AA.`coleta_massa3_1` as coleta_massa3_1, AA.`coleta_massa4_1` as coleta_massa4_1, AA.`coleta_massa5_1` as coleta_massa5_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_num_serial_2` as coleta_num_serial_2, AA.`coleta_num_amostrador_2` as coleta_num_amostrador_2, AA.`coleta_num_relat_ensaio_2` as coleta_num_relat_ensaio_2, AA.`coleta_vazao_2` as coleta_vazao_2, AA.`coleta_tempo_amostragem_2` as coleta_tempo_amostragem_2, AA.`coleta_massa1_2` as coleta_massa1_2, AA.`coleta_massa2_2` as coleta_massa2_2, AA.`coleta_massa3_2` as coleta_massa3_2, AA.`coleta_massa4_2` as coleta_massa4_2, AA.`coleta_massa5_2` as coleta_massa5_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_num_serial_3` as coleta_num_serial_3, AA.`coleta_num_amostrador_3` as coleta_num_amostrador_3, AA.`coleta_num_relat_ensaio_3` as coleta_num_relat_ensaio_3, AA.`coleta_vazao_3` as coleta_vazao_3, AA.`coleta_tempo_amostragem_3` as coleta_tempo_amostragem_3, AA.`coleta_massa1_3` as coleta_massa1_3, AA.`coleta_massa2_3` as coleta_massa2_3, AA.`coleta_massa3_3` as coleta_massa3_3, AA.`coleta_massa4_3` as coleta_massa4_3, AA.`coleta_massa5_3` as coleta_massa5_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_num_serial_4` as coleta_num_serial_4, AA.`coleta_num_amostrador_4` as coleta_num_amostrador_4, AA.`coleta_num_relat_ensaio_4` as coleta_num_relat_ensaio_4, AA.`coleta_vazao_4` as coleta_vazao_4, AA.`coleta_tempo_amostragem_4` as coleta_tempo_amostragem_4, AA.`coleta_massa1_4` as coleta_massa1_4, AA.`coleta_massa2_4` as coleta_massa2_4, AA.`coleta_massa3_4` as coleta_massa3_4, AA.`coleta_massa4_4` as coleta_massa4_4, AA.`coleta_massa5_4` as coleta_massa5_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_num_serial_5` as coleta_num_serial_5, AA.`coleta_num_amostrador_5` as coleta_num_amostrador_5, AA.`coleta_num_relat_ensaio_5` as coleta_num_relat_ensaio_5, AA.`coleta_vazao_5` as coleta_vazao_5, AA.`coleta_tempo_amostragem_5` as coleta_tempo_amostragem_5, AA.`coleta_massa1_5` as coleta_massa1_5, AA.`coleta_massa2_5` as coleta_massa2_5, AA.`coleta_massa3_5` as coleta_massa3_5, AA.`coleta_massa4_5` as coleta_massa4_5, AA.`coleta_massa5_5` as coleta_massa5_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_num_serial_6` as coleta_num_serial_6, AA.`coleta_num_amostrador_6` as coleta_num_amostrador_6, AA.`coleta_num_relat_ensaio_6` as coleta_num_relat_ensaio_6, AA.`coleta_vazao_6` as coleta_vazao_6, AA.`coleta_tempo_amostragem_6` as coleta_tempo_amostragem_6, AA.`coleta_massa1_6` as coleta_massa1_6, AA.`coleta_massa2_6` as coleta_massa2_6, AA.`coleta_massa3_6` as coleta_massa3_6, AA.`coleta_massa4_6` as coleta_massa4_6, AA.`coleta_massa5_6` as coleta_massa5_6, AA.`respirador` as respirador, AA.`cert_aprov` as cert_aprov, AA.`resp_campo_idcolaborador` as resp_campo_idcolaborador, AA.`resp_tecnico_idcolaborador` as resp_tecnico_idcolaborador, upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename`) as img_ativ_filename, lower(AA.`logo_filename`) as logo_filename
       FROM `VAPOR` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVAPOR` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssssssssss', $sql_DATA_ELABORACAO, $sql_ANALISE_DATA_AMOSTRAGEM_1, $sql_ANALISE_DATA_AMOSTRAGEM_2, $sql_ANALISE_DATA_AMOSTRAGEM_3, $sql_ANALISE_DATA_AMOSTRAGEM_4, $sql_ANALISE_DATA_AMOSTRAGEM_5, $sql_ANALISE_DATA_AMOSTRAGEM_6, $sql_COLETA_DATA_1, $sql_COLETA_DATA_2, $sql_COLETA_DATA_3, $sql_COLETA_DATA_4, $sql_COLETA_DATA_5, $sql_COLETA_DATA_6, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_TAREFA_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_TAREFA_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_TAREFA_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_TAREFA_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_TAREFA_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_TAREFA_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_MITIGACAO, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_NUM_SERIAL_1, $o_COLETA_NUM_AMOSTRADOR_1, $o_COLETA_NUM_RELAT_ENSAIO_1, $o_COLETA_VAZAO_1, $o_COLETA_TEMPO_AMOSTRAGEM_1, $o_COLETA_MASSA1_1, $o_COLETA_MASSA2_1, $o_COLETA_MASSA3_1, $o_COLETA_MASSA4_1, $o_COLETA_MASSA5_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_NUM_SERIAL_2, $o_COLETA_NUM_AMOSTRADOR_2, $o_COLETA_NUM_RELAT_ENSAIO_2, $o_COLETA_VAZAO_2, $o_COLETA_TEMPO_AMOSTRAGEM_2, $o_COLETA_MASSA1_2, $o_COLETA_MASSA2_2, $o_COLETA_MASSA3_2, $o_COLETA_MASSA4_2, $o_COLETA_MASSA5_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_NUM_SERIAL_3, $o_COLETA_NUM_AMOSTRADOR_3, $o_COLETA_NUM_RELAT_ENSAIO_3, $o_COLETA_VAZAO_3, $o_COLETA_TEMPO_AMOSTRAGEM_3, $o_COLETA_MASSA1_3, $o_COLETA_MASSA2_3, $o_COLETA_MASSA3_3, $o_COLETA_MASSA4_3, $o_COLETA_MASSA5_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_NUM_SERIAL_4, $o_COLETA_NUM_AMOSTRADOR_4, $o_COLETA_NUM_RELAT_ENSAIO_4, $o_COLETA_VAZAO_4, $o_COLETA_TEMPO_AMOSTRAGEM_4, $o_COLETA_MASSA1_4, $o_COLETA_MASSA2_4, $o_COLETA_MASSA3_4, $o_COLETA_MASSA4_4, $o_COLETA_MASSA5_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_NUM_SERIAL_5, $o_COLETA_NUM_AMOSTRADOR_5, $o_COLETA_NUM_RELAT_ENSAIO_5, $o_COLETA_VAZAO_5, $o_COLETA_TEMPO_AMOSTRAGEM_5, $o_COLETA_MASSA1_5, $o_COLETA_MASSA2_5, $o_COLETA_MASSA3_5, $o_COLETA_MASSA4_5, $o_COLETA_MASSA5_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_NUM_SERIAL_6, $o_COLETA_NUM_AMOSTRADOR_6, $o_COLETA_NUM_RELAT_ENSAIO_6, $o_COLETA_VAZAO_6, $o_COLETA_TEMPO_AMOSTRAGEM_6, $o_COLETA_MASSA1_6, $o_COLETA_MASSA2_6, $o_COLETA_MASSA3_6, $o_COLETA_MASSA4_6, $o_COLETA_MASSA5_6, $o_RESPIRADOR, $o_CERT_APROV, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			// Formata Unescape de Textareas
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_TAREFA_1 = unescape_string($o_ANALISE_OBS_TAREFA_1);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_TAREFA_2 = unescape_string($o_ANALISE_OBS_TAREFA_2);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_TAREFA_3 = unescape_string($o_ANALISE_OBS_TAREFA_3);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_TAREFA_4 = unescape_string($o_ANALISE_OBS_TAREFA_4);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_TAREFA_5 = unescape_string($o_ANALISE_OBS_TAREFA_5);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_TAREFA_6 = unescape_string($o_ANALISE_OBS_TAREFA_6);
			
			// Formata Datas Nulas
			if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
			if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
			if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
			if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
			if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
			if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
			if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
			if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_IDCLIENTE."|".$o_ANO."|".$o_MES."|".$o_PLANILHA_NUM."|".$o_UNIDADE_SITE."|".$o_DATA_ELABORACAO."|".$o_AREA."|".$o_SETOR."|".$o_GES."|".$o_CARGO_FUNCAO."|".$o_CBO."|".$o_ATIV_MACRO."|".$o_ANALISES."|".$o_ANALISE_AMOSTRA_1."|".$o_ANALISE_DATA_AMOSTRAGEM_1."|".$o_ANALISE_TAREFA_EXEC_1."|".$o_ANALISE_PROC_PROD_1."|".$o_ANALISE_OBS_TAREFA_1."|".$o_ANALISE_AMOSTRA_2."|".$o_ANALISE_DATA_AMOSTRAGEM_2."|".$o_ANALISE_TAREFA_EXEC_2."|".$o_ANALISE_PROC_PROD_2."|".$o_ANALISE_OBS_TAREFA_2."|".$o_ANALISE_AMOSTRA_3."|".$o_ANALISE_DATA_AMOSTRAGEM_3."|".$o_ANALISE_TAREFA_EXEC_3."|".$o_ANALISE_PROC_PROD_3."|".$o_ANALISE_OBS_TAREFA_3."|".$o_ANALISE_AMOSTRA_4."|".$o_ANALISE_DATA_AMOSTRAGEM_4."|".$o_ANALISE_TAREFA_EXEC_4."|".$o_ANALISE_PROC_PROD_4."|".$o_ANALISE_OBS_TAREFA_4."|".$o_ANALISE_AMOSTRA_5."|".$o_ANALISE_DATA_AMOSTRAGEM_5."|".$o_ANALISE_TAREFA_EXEC_5."|".$o_ANALISE_PROC_PROD_5."|".$o_ANALISE_OBS_TAREFA_5."|".$o_ANALISE_AMOSTRA_6."|".$o_ANALISE_DATA_AMOSTRAGEM_6."|".$o_ANALISE_TAREFA_EXEC_6."|".$o_ANALISE_PROC_PROD_6."|".$o_ANALISE_OBS_TAREFA_6."|".$o_JOR_TRAB."|".$o_TEMPO_EXPO."|".$o_TIPO_EXPO."|".$o_MEIO_PROPAG."|".$o_FONTE_GERADORA."|".$o_MITIGACAO."|".$o_COLETAS."|".$o_COLETA_AMOSTRA_1."|".$o_COLETA_DATA_1."|".$o_COLETA_NUM_SERIAL_1."|".$o_COLETA_NUM_AMOSTRADOR_1."|".$o_COLETA_NUM_RELAT_ENSAIO_1."|".$o_COLETA_VAZAO_1."|".$o_COLETA_TEMPO_AMOSTRAGEM_1."|".$o_COLETA_MASSA1_1."|".$o_COLETA_MASSA2_1."|".$o_COLETA_MASSA3_1."|".$o_COLETA_MASSA4_1."|".$o_COLETA_MASSA5_1."|".$o_COLETA_AMOSTRA_2."|".$o_COLETA_DATA_2."|".$o_COLETA_NUM_SERIAL_2."|".$o_COLETA_NUM_AMOSTRADOR_2."|".$o_COLETA_NUM_RELAT_ENSAIO_2."|".$o_COLETA_VAZAO_2."|".$o_COLETA_TEMPO_AMOSTRAGEM_2."|".$o_COLETA_MASSA1_2."|".$o_COLETA_MASSA2_2."|".$o_COLETA_MASSA3_2."|".$o_COLETA_MASSA4_2."|".$o_COLETA_MASSA5_2."|".$o_COLETA_AMOSTRA_3."|".$o_COLETA_DATA_3."|".$o_COLETA_NUM_SERIAL_3."|".$o_COLETA_NUM_AMOSTRADOR_3."|".$o_COLETA_NUM_RELAT_ENSAIO_3."|".$o_COLETA_VAZAO_3."|".$o_COLETA_TEMPO_AMOSTRAGEM_3."|".$o_COLETA_MASSA1_3."|".$o_COLETA_MASSA2_3."|".$o_COLETA_MASSA3_3."|".$o_COLETA_MASSA4_3."|".$o_COLETA_MASSA5_3."|".$o_COLETA_AMOSTRA_4."|".$o_COLETA_DATA_4."|".$o_COLETA_NUM_SERIAL_4."|".$o_COLETA_NUM_AMOSTRADOR_4."|".$o_COLETA_NUM_RELAT_ENSAIO_4."|".$o_COLETA_VAZAO_4."|".$o_COLETA_TEMPO_AMOSTRAGEM_4."|".$o_COLETA_MASSA1_4."|".$o_COLETA_MASSA2_4."|".$o_COLETA_MASSA3_4."|".$o_COLETA_MASSA4_4."|".$o_COLETA_MASSA5_4."|".$o_COLETA_AMOSTRA_5."|".$o_COLETA_DATA_5."|".$o_COLETA_NUM_SERIAL_5."|".$o_COLETA_NUM_AMOSTRADOR_5."|".$o_COLETA_NUM_RELAT_ENSAIO_5."|".$o_COLETA_VAZAO_5."|".$o_COLETA_TEMPO_AMOSTRAGEM_5."|".$o_COLETA_MASSA1_5."|".$o_COLETA_MASSA2_5."|".$o_COLETA_MASSA3_5."|".$o_COLETA_MASSA4_5."|".$o_COLETA_MASSA5_5."|".$o_COLETA_AMOSTRA_6."|".$o_COLETA_DATA_6."|".$o_COLETA_NUM_SERIAL_6."|".$o_COLETA_NUM_AMOSTRADOR_6."|".$o_COLETA_NUM_RELAT_ENSAIO_6."|".$o_COLETA_VAZAO_6."|".$o_COLETA_TEMPO_AMOSTRAGEM_6."|".$o_COLETA_MASSA1_6."|".$o_COLETA_MASSA2_6."|".$o_COLETA_MASSA3_6."|".$o_COLETA_MASSA4_6."|".$o_COLETA_MASSA5_6."|".$o_RESPIRADOR."|".$o_CERT_APROV."|".$o_RESP_CAMPO_IDCOLABORADOR."|".$o_RESP_TECNICO_IDCOLABORADOR."|".$o_REGISTRO_RC."|".$o_REGISTRO_RT."|".$o_IMG_ATIV_FILENAME."|".$o_LOGO_FILENAME."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idVAPOR` as id, upper(CLNT.`nome_interno`) as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_proc_prod_1` as analise_proc_prod_1, AA.`analise_obs_tarefa_1` as analise_obs_tarefa_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_proc_prod_2` as analise_proc_prod_2, AA.`analise_obs_tarefa_2` as analise_obs_tarefa_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_proc_prod_3` as analise_proc_prod_3, AA.`analise_obs_tarefa_3` as analise_obs_tarefa_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_proc_prod_4` as analise_proc_prod_4, AA.`analise_obs_tarefa_4` as analise_obs_tarefa_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_proc_prod_5` as analise_proc_prod_5, AA.`analise_obs_tarefa_5` as analise_obs_tarefa_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, AA.`analise_proc_prod_6` as analise_proc_prod_6, AA.`analise_obs_tarefa_6` as analise_obs_tarefa_6, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`mitigacao` as mitigacao, AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_num_serial_1` as coleta_num_serial_1, AA.`coleta_num_amostrador_1` as coleta_num_amostrador_1, AA.`coleta_num_relat_ensaio_1` as coleta_num_relat_ensaio_1, AA.`coleta_vazao_1` as coleta_vazao_1, AA.`coleta_tempo_amostragem_1` as coleta_tempo_amostragem_1, AA.`coleta_massa1_1` as coleta_massa1_1, AA.`coleta_massa2_1` as coleta_massa2_1, AA.`coleta_massa3_1` as coleta_massa3_1, AA.`coleta_massa4_1` as coleta_massa4_1, AA.`coleta_massa5_1` as coleta_massa5_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_num_serial_2` as coleta_num_serial_2, AA.`coleta_num_amostrador_2` as coleta_num_amostrador_2, AA.`coleta_num_relat_ensaio_2` as coleta_num_relat_ensaio_2, AA.`coleta_vazao_2` as coleta_vazao_2, AA.`coleta_tempo_amostragem_2` as coleta_tempo_amostragem_2, AA.`coleta_massa1_2` as coleta_massa1_2, AA.`coleta_massa2_2` as coleta_massa2_2, AA.`coleta_massa3_2` as coleta_massa3_2, AA.`coleta_massa4_2` as coleta_massa4_2, AA.`coleta_massa5_2` as coleta_massa5_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_num_serial_3` as coleta_num_serial_3, AA.`coleta_num_amostrador_3` as coleta_num_amostrador_3, AA.`coleta_num_relat_ensaio_3` as coleta_num_relat_ensaio_3, AA.`coleta_vazao_3` as coleta_vazao_3, AA.`coleta_tempo_amostragem_3` as coleta_tempo_amostragem_3, AA.`coleta_massa1_3` as coleta_massa1_3, AA.`coleta_massa2_3` as coleta_massa2_3, AA.`coleta_massa3_3` as coleta_massa3_3, AA.`coleta_massa4_3` as coleta_massa4_3, AA.`coleta_massa5_3` as coleta_massa5_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_num_serial_4` as coleta_num_serial_4, AA.`coleta_num_amostrador_4` as coleta_num_amostrador_4, AA.`coleta_num_relat_ensaio_4` as coleta_num_relat_ensaio_4, AA.`coleta_vazao_4` as coleta_vazao_4, AA.`coleta_tempo_amostragem_4` as coleta_tempo_amostragem_4, AA.`coleta_massa1_4` as coleta_massa1_4, AA.`coleta_massa2_4` as coleta_massa2_4, AA.`coleta_massa3_4` as coleta_massa3_4, AA.`coleta_massa4_4` as coleta_massa4_4, AA.`coleta_massa5_4` as coleta_massa5_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_num_serial_5` as coleta_num_serial_5, AA.`coleta_num_amostrador_5` as coleta_num_amostrador_5, AA.`coleta_num_relat_ensaio_5` as coleta_num_relat_ensaio_5, AA.`coleta_vazao_5` as coleta_vazao_5, AA.`coleta_tempo_amostragem_5` as coleta_tempo_amostragem_5, AA.`coleta_massa1_5` as coleta_massa1_5, AA.`coleta_massa2_5` as coleta_massa2_5, AA.`coleta_massa3_5` as coleta_massa3_5, AA.`coleta_massa4_5` as coleta_massa4_5, AA.`coleta_massa5_5` as coleta_massa5_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_num_serial_6` as coleta_num_serial_6, AA.`coleta_num_amostrador_6` as coleta_num_amostrador_6, AA.`coleta_num_relat_ensaio_6` as coleta_num_relat_ensaio_6, AA.`coleta_vazao_6` as coleta_vazao_6, AA.`coleta_tempo_amostragem_6` as coleta_tempo_amostragem_6, AA.`coleta_massa1_6` as coleta_massa1_6, AA.`coleta_massa2_6` as coleta_massa2_6, AA.`coleta_massa3_6` as coleta_massa3_6, AA.`coleta_massa4_6` as coleta_massa4_6, AA.`coleta_massa5_6` as coleta_massa5_6, AA.`respirador` as respirador, AA.`cert_aprov` as cert_aprov, 
            (SELECT concat(upper(UA1.`nome`),' ',upper(UA1.`sobrenome`),' (',lower(CLBRDR1.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA1
              WHERE UA1.`username` = CLBRDR1.`username`
              LIMIT 1
            ) as resp_campo,
            (SELECT concat(upper(UA2.`nome`),' ',upper(UA2.`sobrenome`),' (',lower(CLBRDR2.`username`),')') 
               FROM `SYSTEM_USER_ACCOUNT` UA2
              WHERE UA2.`username` = CLBRDR2.`username`
              LIMIT 1
            ) as resp_tec,
            upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename`) as img_ativ_filename, lower(AA.`logo_filename`) as logo_filename,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `VAPOR` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
    LEFT JOIN `COLABORADOR` as CLBRDR1
           ON CLBRDR1.`idcolaborador` = AA.`resp_campo_idcolaborador`
    LEFT JOIN `COLABORADOR` as CLBRDR2
           ON CLBRDR2.`idcolaborador` = AA.`resp_tecnico_idcolaborador`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idVAPOR` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssssssssssssssss', $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_TAREFA_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_TAREFA_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_TAREFA_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_TAREFA_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_TAREFA_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_TAREFA_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_MITIGACAO, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_NUM_SERIAL_1, $o_COLETA_NUM_AMOSTRADOR_1, $o_COLETA_NUM_RELAT_ENSAIO_1, $o_COLETA_VAZAO_1, $o_COLETA_TEMPO_AMOSTRAGEM_1, $o_COLETA_MASSA1_1, $o_COLETA_MASSA2_1, $o_COLETA_MASSA3_1, $o_COLETA_MASSA4_1, $o_COLETA_MASSA5_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_NUM_SERIAL_2, $o_COLETA_NUM_AMOSTRADOR_2, $o_COLETA_NUM_RELAT_ENSAIO_2, $o_COLETA_VAZAO_2, $o_COLETA_TEMPO_AMOSTRAGEM_2, $o_COLETA_MASSA1_2, $o_COLETA_MASSA2_2, $o_COLETA_MASSA3_2, $o_COLETA_MASSA4_2, $o_COLETA_MASSA5_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_NUM_SERIAL_3, $o_COLETA_NUM_AMOSTRADOR_3, $o_COLETA_NUM_RELAT_ENSAIO_3, $o_COLETA_VAZAO_3, $o_COLETA_TEMPO_AMOSTRAGEM_3, $o_COLETA_MASSA1_3, $o_COLETA_MASSA2_3, $o_COLETA_MASSA3_3, $o_COLETA_MASSA4_3, $o_COLETA_MASSA5_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_NUM_SERIAL_4, $o_COLETA_NUM_AMOSTRADOR_4, $o_COLETA_NUM_RELAT_ENSAIO_4, $o_COLETA_VAZAO_4, $o_COLETA_TEMPO_AMOSTRAGEM_4, $o_COLETA_MASSA1_4, $o_COLETA_MASSA2_4, $o_COLETA_MASSA3_4, $o_COLETA_MASSA4_4, $o_COLETA_MASSA5_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_NUM_SERIAL_5, $o_COLETA_NUM_AMOSTRADOR_5, $o_COLETA_NUM_RELAT_ENSAIO_5, $o_COLETA_VAZAO_5, $o_COLETA_TEMPO_AMOSTRAGEM_5, $o_COLETA_MASSA1_5, $o_COLETA_MASSA2_5, $o_COLETA_MASSA3_5, $o_COLETA_MASSA4_5, $o_COLETA_MASSA5_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_NUM_SERIAL_6, $o_COLETA_NUM_AMOSTRADOR_6, $o_COLETA_NUM_RELAT_ENSAIO_6, $o_COLETA_VAZAO_6, $o_COLETA_TEMPO_AMOSTRAGEM_6, $o_COLETA_MASSA1_6, $o_COLETA_MASSA2_6, $o_COLETA_MASSA3_6, $o_COLETA_MASSA4_6, $o_COLETA_MASSA5_6, $o_RESPIRADOR, $o_CERT_APROV, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_TAREFA_1 = unescape_string($o_ANALISE_OBS_TAREFA_1);
			$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_TAREFA_2 = unescape_string($o_ANALISE_OBS_TAREFA_2);
			$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_TAREFA_3 = unescape_string($o_ANALISE_OBS_TAREFA_3);
			$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_TAREFA_4 = unescape_string($o_ANALISE_OBS_TAREFA_4);
			$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_TAREFA_5 = unescape_string($o_ANALISE_OBS_TAREFA_5);
			$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_TAREFA_6 = unescape_string($o_ANALISE_OBS_TAREFA_6);
			$o_ANALISE_TAREFA_EXEC_1 = nl2br($o_ANALISE_TAREFA_EXEC_1);
			$o_ANALISE_PROC_PROD_1 = nl2br($o_ANALISE_PROC_PROD_1);
			$o_ANALISE_OBS_TAREFA_1 = nl2br($o_ANALISE_OBS_TAREFA_1);
			$o_ANALISE_TAREFA_EXEC_2 = nl2br($o_ANALISE_TAREFA_EXEC_2);
			$o_ANALISE_PROC_PROD_2 = nl2br($o_ANALISE_PROC_PROD_2);
			$o_ANALISE_OBS_TAREFA_2 = nl2br($o_ANALISE_OBS_TAREFA_2);
			$o_ANALISE_TAREFA_EXEC_3 = nl2br($o_ANALISE_TAREFA_EXEC_3);
			$o_ANALISE_PROC_PROD_3 = nl2br($o_ANALISE_PROC_PROD_3);
			$o_ANALISE_OBS_TAREFA_3 = nl2br($o_ANALISE_OBS_TAREFA_3);
			$o_ANALISE_TAREFA_EXEC_4 = nl2br($o_ANALISE_TAREFA_EXEC_4);
			$o_ANALISE_PROC_PROD_4 = nl2br($o_ANALISE_PROC_PROD_4);
			$o_ANALISE_OBS_TAREFA_4 = nl2br($o_ANALISE_OBS_TAREFA_4);
			$o_ANALISE_TAREFA_EXEC_5 = nl2br($o_ANALISE_TAREFA_EXEC_5);
			$o_ANALISE_PROC_PROD_5 = nl2br($o_ANALISE_PROC_PROD_5);
			$o_ANALISE_OBS_TAREFA_5 = nl2br($o_ANALISE_OBS_TAREFA_5);
			$o_ANALISE_TAREFA_EXEC_6 = nl2br($o_ANALISE_TAREFA_EXEC_6);
			$o_ANALISE_PROC_PROD_6 = nl2br($o_ANALISE_PROC_PROD_6);
			$o_ANALISE_OBS_TAREFA_6 = nl2br($o_ANALISE_OBS_TAREFA_6);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_VAPOR_DEZ; }
				if( $o_ANALISES == "1"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_1; }
				if( $o_ANALISES == "2"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_2; }
				if( $o_ANALISES == "3"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_3; }
				if( $o_ANALISES == "4"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_4; }
				if( $o_ANALISES == "5"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_5; }
				if( $o_ANALISES == "6"){ $o_ANALISES_TXT = TXT_LAUDOS_VAPOR_6; }
				if( $o_COLETAS == "1"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_1; }
				if( $o_COLETAS == "2"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_2; }
				if( $o_COLETAS == "3"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_3; }
				if( $o_COLETAS == "4"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_4; }
				if( $o_COLETAS == "5"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_5; }
				if( $o_COLETAS == "6"){ $o_COLETAS_TXT = TXT_LAUDOS_VAPOR_6; }
				
				// Formata Datas Nulas
				if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
				if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
				if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
				if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
				if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
				if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
				if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
				if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_CLIENTE.':</b></th><td>'.$o_IDCLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_NUMERO_PLANILHA.':</b></th><td>'.$o_PLANILHA_NUM.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_UNIDADESITE.':</b></th><td>'.$o_UNIDADE_SITE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_DATA_ELABORACAO.':</b></th><td>'.$o_DATA_ELABORACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_AREA.':</b></th><td>'.$o_AREA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_SETOR.':</b></th><td>'.$o_SETOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_GES.':</b></th><td>'.$o_GES.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_CARGOFUNCAO.':</b></th><td>'.$o_CARGO_FUNCAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_CBO.':</b></th><td>'.$o_CBO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ATIVIDADE_MACRO.':</b></th><td>'.$o_ATIV_MACRO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISES.':</b></th><td>'.$o_ANALISES_TXT.'</td></tr>';
				
				# Formata Analises
				$o_ANALISES_1_SET = 0;
				$o_ANALISES_2_SET = 0;
				$o_ANALISES_3_SET = 0;
				$o_ANALISES_4_SET = 0;
				$o_ANALISES_5_SET = 0;
				$o_ANALISES_6_SET = 0;
				switch($o_ANALISES)
				{
					case '1':
						$o_ANALISES_1_SET = 1;
						break;
					case '2':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						break;
					case '3':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						break;
					case '4':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						break;
					case '5':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						break;
					case '6':
						$o_ANALISES_1_SET = 1;
						$o_ANALISES_2_SET = 1;
						$o_ANALISES_3_SET = 1;
						$o_ANALISES_4_SET = 1;
						$o_ANALISES_5_SET = 1;
						$o_ANALISES_6_SET = 1;
						break;
					default:
						break;
				}
				
				if($o_ANALISES_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_1_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_1_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_1_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_1_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_1_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_1.'</td></tr>';
				}
				if($o_ANALISES_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_2_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_2_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_2_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_2_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_2_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_2.'</td></tr>';
				}
				if($o_ANALISES_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_3_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_3_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_3_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_3_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_3_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_3.'</td></tr>';
				}
				if($o_ANALISES_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_4_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_4_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_4_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_4_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_4_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_4.'</td></tr>';
				}
				if($o_ANALISES_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_5_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_5_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_5_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_5_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_5_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_5.'</td></tr>';
				}
				if($o_ANALISES_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_6_AMOSTRA.':</b></th><td>'.$o_ANALISE_AMOSTRA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_6_DATA_AMOSTRAGEM.':</b></th><td>'.$o_ANALISE_DATA_AMOSTRAGEM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_6_TAREFA_EXECUTADA.':</b></th><td>'.$o_ANALISE_TAREFA_EXEC_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_6_PROCESSO_PRODUTIVO.':</b></th><td>'.$o_ANALISE_PROC_PROD_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_ANALISE_6_OBS_TAREFA.':</b></th><td>'.$o_ANALISE_OBS_TAREFA_6.'</td></tr>';
				}
				
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_JORNADA_DE_TRABALHO.':</b></th><td>'.$o_JOR_TRAB.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_TEMPO_EXPOSICAO.':</b></th><td>'.$o_TEMPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_TIPO_EXPOSICAO.':</b></th><td>'.$o_TIPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_MEIO_DE_PROPAGACAO.':</b></th><td>'.$o_MEIO_PROPAG.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_FONTE_GERADORA.':</b></th><td>'.$o_FONTE_GERADORA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_MITIGACAO.':</b></th><td>'.$o_MITIGACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETAS.':</b></th><td>'.$o_COLETAS_TXT.'</td></tr>';
				
				# Formata Coletas
				$o_COLETAS_1_SET = 0;
				$o_COLETAS_2_SET = 0;
				$o_COLETAS_3_SET = 0;
				$o_COLETAS_4_SET = 0;
				$o_COLETAS_5_SET = 0;
				$o_COLETAS_6_SET = 0;
				switch($o_COLETAS)
				{
					case '1':
						$o_COLETAS_1_SET = 1;
						break;
					case '2':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						break;
					case '3':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						break;
					case '4':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						break;
					case '5':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						break;
					case '6':
						$o_COLETAS_1_SET = 1;
						$o_COLETAS_2_SET = 1;
						$o_COLETAS_3_SET = 1;
						$o_COLETAS_4_SET = 1;
						$o_COLETAS_5_SET = 1;
						$o_COLETAS_6_SET = 1;
						break;
					default:
						break;
				}
				
				if($o_COLETAS_1_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_DATA.':</b></th><td>'.$o_COLETA_DATA_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_1.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_1_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_1.'</td></tr>';
				}
				if($o_COLETAS_2_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_DATA.':</b></th><td>'.$o_COLETA_DATA_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_2.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_2_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_2.'</td></tr>';
				}
				if($o_COLETAS_3_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_DATA.':</b></th><td>'.$o_COLETA_DATA_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_3.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_3_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_3.'</td></tr>';
				}
				if($o_COLETAS_4_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_DATA.':</b></th><td>'.$o_COLETA_DATA_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_4.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_4_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_4.'</td></tr>';
				}
				if($o_COLETAS_5_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_DATA.':</b></th><td>'.$o_COLETA_DATA_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_5.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_5_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_5.'</td></tr>';
				}
				if($o_COLETAS_6_SET)
				{
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_AMOSTRA.':</b></th><td>'.$o_COLETA_AMOSTRA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_DATA.':</b></th><td>'.$o_COLETA_DATA_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_N_SERIAL.':</b></th><td>'.$o_COLETA_NUM_SERIAL_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_N_AMOSTRADOR.':</b></th><td>'.$o_COLETA_NUM_AMOSTRADOR_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_N_RELATORIO_DE_ENSAIO.':</b></th><td>'.$o_COLETA_NUM_RELAT_ENSAIO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_VAZAO.':</b></th><td>'.$o_COLETA_VAZAO_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_TEMPO_AMOSTRAGEM.':</b></th><td>'.$o_COLETA_TEMPO_AMOSTRAGEM_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_MASSA_1.':</b></th><td>'.$o_COLETA_MASSA1_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_MASSA_2.':</b></th><td>'.$o_COLETA_MASSA2_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_MASSA_3.':</b></th><td>'.$o_COLETA_MASSA3_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_MASSA_4.':</b></th><td>'.$o_COLETA_MASSA4_6.'</td></tr>';
					$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_COLETA_6_MASSA_5.':</b></th><td>'.$o_COLETA_MASSA5_6.'</td></tr>';
				}
				
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_RESPIRADOR.':</b></th><td>'.$o_RESPIRADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_CERTIFICADO_DE_APROVACAO.':</b></th><td>'.$o_CERT_APROV.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_RESPONSAVEL_CAMPO.':</b></th><td>'.$o_RESP_CAMPO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_RESPONSAVEL_TECNICO.':</b></th><td>'.$o_RESP_TECNICO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_RESPONSAVEL_CAMPO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_RESPONSAVEL_TECNICO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RT.'</td></tr>';
				if($o_IMG_ATIV_FILENAME){ $o_IMG_ATIV_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_IMG_ATIV_FILENAME.'" target="_blank">'.$o_IMG_ATIV_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_IMAGEM_ATIVIDADE.':</b></th><td>'.$o_IMG_ATIV_FILENAME_TXT.'</td></tr>';
				if($o_LOGO_FILENAME){ $o_LOGO_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_LOGO_FILENAME.'" target="_blank">'.$o_LOGO_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_VAPOR_IMAGEM_LOGOMARCA.':</b></th><td>'.$o_LOGO_FILENAME_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
