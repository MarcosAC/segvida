/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-laudos-part.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  # Criacao: 8/9/2017 19:30:56
  ################################################################################# */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	

	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=laudos-part');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	

	////////////////////// CRONO ~ START
	// Return today's date and time
	var SYS_currentTime = new Date();
	
	// returns the year (four digits)
	var SYS_year = SYS_currentTime.getFullYear();
	var SYS_aaaa = SYS_year.toString();
	
	// returns the month (from 0 to 11)
	var SYS_month = SYS_currentTime.getMonth() + 1;
	var SYS_mm = SYS_month.toString();
	if(SYS_mm.length == 1){ SYS_mm = "0"+SYS_mm; }
	
	// returns the day of the month (from 1 to 31)
	var SYS_day = SYS_currentTime.getDate();
	var SYS_dd = SYS_day.toString();
	if(SYS_dd.length == 1){ SYS_dd = "0"+SYS_dd; }
	
	Date.prototype.holidays = 
	{
		// Feriados Gerais
		all: [
		'0101', // Jan 01 - Confraternizacao Universal
		'0414', // Abr 14 - Paixao de Cristo
		'0421', // Abr 21 - Tiradentes
		'0501', // Mai 01 - Dia do trabalho
		'0907', // Set 07 - Independencia do Brasil
		'1012', // Out 12 - Nossa Senhora Aparecida
		'1102', // Nov 02 - Finados
		'1115', // Nov 15 - Proclamacao da Republica
		'1225'  // Dez 25 - Natal
		],
		2017: [
		// Feriados especificos de 2017
		]
	};
	
	Date.prototype.addWorkingDays = function(days) 
	{
		while (days > 0) 
		{
			this.setDate(this.getDate() + 1);
			if (!this.isHoliday()) days--;
		}
		return this;
	};
	
	Date.prototype.substractWorkingDays = function(days) 
	{
		while (days > 0) 
		{
			this.setDate(this.getDate() - 1);
			if (!this.isHoliday()) days--;
		}
		return this;
	};
	
	Date.prototype.isHoliday = function() 
	{
		function zeroPad(n) 
		{
			n |= 0;
			return (n < 10 ? '0' : '') + n;
		}
		
		// if weekend return true from here it self;
		if (this.getDay() == 0 || this.getDay() == 6) { return true; } 
		
		var day = zeroPad(this.getMonth() + 1) + zeroPad(this.getDate());
		
		// if date is present in the holiday list return true;
		return !!~this.holidays.all.indexOf(day) || 
		(this.holidays[this.getFullYear()] ?
		!!~this.holidays[this.getFullYear()].indexOf(day) : false);
	};
	Date.prototype.toDDMMAAAA = function() 
	{
		var _ano = this.getFullYear();
		var _mes = this.getMonth() + 1;
		var _dia = this.getDate();
		var _dia = _dia.toString();
		if(_dia.length == 1){ _dia = "0"+_dia; }
		var _mes = _mes.toString();
		if(_mes.length == 1){ _mes = "0"+_mes; }
		return _dia+'/'+_mes+'/'+_ano;
	}
	/*
	// Uasage
	var date = new Date('2015-12-31');
	date.addWorkingDays(10);
	date.substractWorkingDays(10);
	alert(date.toDDMMAAAA());
	*/
	////////////////////// CRONO ~ STOP
	

	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	

	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	

	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	

	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined ||
	   ($.Storage.loadItem('language') != 'pt-br' && $.Storage.loadItem('language') != 'en-us' && $.Storage.loadItem('language') != 'es-es') 
	  )
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	

	////////////////////// FORMATACAO NUMERICA ~ START
	//SPRINTF javascript equivalent
	function sprintf() 
	{
		var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
	  var a = arguments;
	  var i = 0;
	  var format = a[i++];
	
	  // pad()
	  var pad = function(str, len, chr, leftJustify) {
	    if (!chr) {
	      chr = ' ';
	    }
	    var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
	      .join(chr);
	    return leftJustify ? str + padding : padding + str;
	  };
	
	  // justify()
	  var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
	    var diff = minWidth - value.length;
	    if (diff > 0) {
	      if (leftJustify || !zeroPad) {
	        value = pad(value, minWidth, customPadChar, leftJustify);
	      } else {
	        value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
	      }
	    }
	    return value;
	  };
	
	  // formatBaseX()
	  var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
	    // Note: casts negative numbers to positive ones
	    var number = value >>> 0;
	    prefix = prefix && number && {
	      '2': '0b',
	      '8': '0',
	      '16': '0x'
	    }[base] || '';
	    value = prefix + pad(number.toString(base), precision || 0, '0', false);
	    return justify(value, prefix, leftJustify, minWidth, zeroPad);
	  };
	
	  // formatString()
	  var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
	    if (precision != null) {
	      value = value.slice(0, precision);
	    }
	    return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
	  };
	
	  // doFormat()
	  var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
	    var number, prefix, method, textTransform, value;
	
	    if (substring === '%%') {
	      return '%';
	    }
	
	    // parse flags
	    var leftJustify = false;
	    var positivePrefix = '';
	    var zeroPad = false;
	    var prefixBaseX = false;
	    var customPadChar = ' ';
	    var flagsl = flags.length;
	    for (var j = 0; flags && j < flagsl; j++) {
	      switch (flags.charAt(j)) {
	        case ' ':
	          positivePrefix = ' ';
	          break;
	        case '+':
	          positivePrefix = '+';
	          break;
	        case '-':
	          leftJustify = true;
	          break;
	        case "'":
	          customPadChar = flags.charAt(j + 1);
	          break;
	        case '0':
	          zeroPad = true;
	          customPadChar = '0';
	          break;
	        case '#':
	          prefixBaseX = true;
	          break;
	      }
	    }
	
	    // parameters may be null, undefined, empty-string or real valued
	    // we want to ignore null, undefined and empty-string values
	    if (!minWidth) {
	      minWidth = 0;
	    } else if (minWidth === '*') {
	      minWidth = +a[i++];
	    } else if (minWidth.charAt(0) == '*') {
	      minWidth = +a[minWidth.slice(1, -1)];
	    } else {
	      minWidth = +minWidth;
	    }
	
	    // Note: undocumented perl feature:
	    if (minWidth < 0) {
	      minWidth = -minWidth;
	      leftJustify = true;
	    }
	
	    if (!isFinite(minWidth)) {
	      throw new Error('sprintf: (minimum-)width must be finite');
	    }
	
	    if (!precision) {
	      precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
	    } else if (precision === '*') {
	      precision = +a[i++];
	    } else if (precision.charAt(0) == '*') {
	      precision = +a[precision.slice(1, -1)];
	    } else {
	      precision = +precision;
	    }
	
	    // grab value using valueIndex if required?
	    value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];
	
	    switch (type) {
	      case 's':
	        return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
	      case 'c':
	        return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
	      case 'b':
	        return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'o':
	        return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'x':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'X':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
	          .toUpperCase();
	      case 'u':
	        return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'i':
	      case 'd':
	        number = +value || 0;
	        number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
	        prefix = number < 0 ? '-' : positivePrefix;
	        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad);
	      case 'e':
	      case 'E':
	      case 'f': // Should handle locales (as per setlocale)
	      case 'F':
	      case 'g':
	      case 'G':
	        number = +value;
	        prefix = number < 0 ? '-' : positivePrefix;
	        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
	        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
	        value = prefix + Math.abs(number)[method](precision);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
	      default:
	        return substring;
	    }
	  };
	
	  return format.replace(regex, doFormat);
	}
	// Formata valor de USD para BRL
	function currency_format (number, decimals, dec_point, thousands_sep) 
	{
		var n = number, prec = decimals;
		
		var toFixedFix = function (n,prec) {
		    var k = Math.pow(10,prec);
		    return (Math.round(n*k)/k).toString();
		};
		
		n = !isFinite(+n) ? 0 : +n;
		prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		var abs = toFixedFix(Math.abs(n), prec);
		var _, i;
		
		if (abs >= 1000) {
		    _ = abs.split(/\D/);
		    i = _[0].length % 3 || 3;
		
		    _[0] = s.slice(0,i + (n < 0)) +
		          _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		    s = _.join(dec);
		} else {
		    s = s.replace('.', dec);
		}
		
		var decPos = s.indexOf(dec);
		if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
		    s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		}
		else if (prec >= 1 && decPos === -1) {
		    s += dec+new Array(prec).join(0)+'0';
		}
		return s; 
	}
	// Formata currency on type
	function currencyFormat(fld, milSep, decSep, e) 
	{
		var sep = 0;
		var key = '';
		var i = j = 0;
		var len = len2 = 0;
		var strCheck = '0123456789';
		var aux = aux2 = '';
		var whichCode = (window.Event) ? e.which : e.keyCode;
		//
		if (whichCode == 13) return true;  // Enter
		if (whichCode == 8) return true;  // Delete
		key = String.fromCharCode(whichCode);  // Get key value from key code
		if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
		len = fld.value.length;
		for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
		for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
			  aux2 = '';
			  for (j = 0, i = len - 3; i >= 0; i--) {
			    if (j == 3) {
			      aux2 += milSep;
			      j = 0;
			    }//endif
			    aux2 += aux.charAt(i);
			    j++;
			  }//endfor
			  fld.value = '';
			  len2 = aux2.length;
			  for (i = len2 - 1; i >= 0; i--)
			  fld.value += aux2.charAt(i);
			  fld.value += decSep + aux.substr(len - 2, len);
			}//endif
		return false;
	}
	// Formata autoNumeric Defaults
	$.extend($.fn.autoNumeric.defaults, {aSign: 'R$ ', pSign:'p', aSep: '.', aDec: ','});
	// Format datepicker Defaults
	$.fn.datepicker.defaults.container='#cad-modal';
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	$('#reg_data_elaboracao').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_1').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_2').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_3').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_4').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_5').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_analise_data_amostragem_6').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_1').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_2').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_3').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_4').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_5').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	$('#reg_coleta_data_6').datepicker({autoclose: true, language: "pt-BR", format: "dd/mm/yyyy", todayBtn: "linked", todayHighlight: true});
	////////////////////// FORMATACAO NUMERICA ~ STOP
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric2", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				(45 <= event.which && event.which <= 45) || // -
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(40 <= event.which && event.which <= 41) || // ),(
				(45 <= event.which && event.which <= 45) || // -
				(48 <= event.which && event.which <= 57) || // 1-9
				(32 <= event.which && event.which <= 32) || // space
				(65 <= event.which && event.which <= 90) || // a-z
				(95 <= event.which && event.which <= 95) || // _
				(97 <= event.which && event.which <= 122) || // A-Z
				(192 <= event.which && event.which <= 196) || // A-Z
				(199 <= event.which && event.which <= 207) || // A-Z
				(209 <= event.which && event.which <= 214) || // A-Z
				(217 <= event.which && event.which <= 220) || // A-Z
				(224 <= event.which && event.which <= 228) || // A-Z
				(231 <= event.which && event.which <= 239) || // A-Z
				(241 <= event.which && event.which <= 246) || // A-Z
				(249 <= event.which && event.which <= 252) || // A-Z
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_barra", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(40 <= event.which && event.which <= 41) || // ),(
				(45 <= event.which && event.which <= 45) || // -
				(47 <= event.which && event.which <= 47) || // /
				(48 <= event.which && event.which <= 57) || // 1-9
				(32 <= event.which && event.which <= 32) || // space
				(65 <= event.which && event.which <= 90) || // a-z
				(95 <= event.which && event.which <= 95) || // _
				(97 <= event.which && event.which <= 122) || // A-Z
				(192 <= event.which && event.which <= 196) || // A-Z
				(199 <= event.which && event.which <= 207) || // A-Z
				(209 <= event.which && event.which <= 214) || // A-Z
				(217 <= event.which && event.which <= 220) || // A-Z
				(224 <= event.which && event.which <= 228) || // A-Z
				(231 <= event.which && event.which <= 239) || // A-Z
				(241 <= event.which && event.which <= 246) || // A-Z
				(249 <= event.which && event.which <= 252) || // A-Z
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".banco-digito", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    (88 <= event.which && event.which <= 88) || // X
		    (120 <= event.which && event.which <= 120) || // x
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_comma", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (44 <= event.which && event.which <= 44) || // ,(virgula)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".gps_coord", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	

	////////////////////// MSGBOX ~ START
	$(document).on("f_show_msgbox", function(e,_txt,_title,_type)
	{
		show_msgbox(_txt,_title,_type);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'ficha':
				$("#msgBoxIconFicha").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'pt-br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'en-us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'es-es':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	

	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		LoadingBoxPanel.remove();
	}
	////////////////////// LOADINGBOX ~ STOP
	

	////////////////////// Datatable ~ START
	var myTable = $('#main-datatable').DataTable(
	{
		dom: "lBfrtip",
		buttons: [
			{
				extend: "copy",
				className: "btn-sm",
				text: "<i class='fa fa-clipboard fa-lg'></i>",
				exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] }
			},
			{
				extend: "csv",
				className: "btn-sm",
				text: "<i class='fa fa-file-text-o fa-lg'></i>",
				exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] }
			},
			{
				extend: "excel",
				className: "btn-sm",
				text: "<i class='fa fa-file-excel-o fa-lg'></i>",
				title: "SEGVIDA - Laudos/Particionado",
				exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] }
			},
//			{
//				extend: "pdf",
//				className: "btn-sm",
//				text: "<i class='fa fa-file-pdf-o fa-lg'></i>",
//				title: "SEGVIDA - Laudos/Particionado",
//				exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] }
//			},
			{
				extend: "print",
				className: "btn-sm",
				text: "<i class='fa fa-print fa-lg'></i>",
				title: "SEGVIDA - Laudos/Particionado",
				exportOptions: { columns: [ 2, 3, 4, 5, 6, 7, 8 ] }
			},
		],
		responsive: true,
		fixedHeader: true,
		select: true,
		//info:true,
		language: 
		{
			    sEmptyTable: "Nenhum registro encontrado",
			          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			     sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
			  sInfoFiltered: "(Filtrados de _MAX_ registros)",
			   sInfoPostFix: "",
			 sInfoThousands: ".",
			    sLengthMenu: "_MENU_ Resultados por página",
			sLoadingRecords: "Carregando...",
			    sProcessing: "Processando...",
			   sZeroRecords: "Nenhum registro encontrado",
			        sSearch: "Pesquisar",
			      oPaginate: {
													    sNext: "<i class='fa fa-arrow-circle-right fa-lg'></i>",
													sPrevious: "<i class='fa fa-arrow-circle-left fa-lg'></i>",
													   sFirst: "Primeiro",
													    sLast: "Último"
												},
								oAria: {
													sSortAscending: ": Ordenar colunas de forma ascendente",
													sSortDescending: ": Ordenar colunas de forma descendente"
												}
												,
							buttons: {
												  copyTitle: 'Copiado para o clipboard',
												   copyKeys: 'Pressione <i>ctrl</i> ou <i>⌘</i> + <i>C</i> para copiar os dados da tabela para o clipboard. <br><br>Para cancelar, clique sobre esta mensagem ou pressione ESC.',
												copySuccess: {
																				_: 'Copiados %d registros',
																				1: 'Copiado 1 registro'
																		}
												}
		
		},
		
		'order': [[ 2, 'asc' ]],
		'columnDefs': [
			{ 
				'targets': 0,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '10px',
				'render': function (data,type, full, meta){ return '<div class="checkbox checkbox-primary"><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></div>'; }
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '50px'
			}
		]
		
	});

	// Handle click on "Select all" control
	$('#select-all').on('click', function()
	{
		// Get all rows with search applied
		//var rows = myTable.rows({ 'search': 'applied' }).nodes();
		var rows = myTable.rows({ 'page':'current', 'filter': 'applied' }).nodes();
		// Check/uncheck checkboxes for all rows in the table
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
	
	// Handle click on checkbox to set state of "Select all" control
	$('#main-datatable tbody').on('change', 'input[type="checkbox"]', function()
	{
		// If checkbox is not checked
		if(!this.checked)
		{
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if(el && el.checked && ('indeterminate' in el))
			{
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});
	
	$('#main-datatable').on( 'page.dt', function () 
	{
		$('#select-all').prop('checked', false);
	});
	////////////////////// Datatable ~ STOP
	

	////////////////////// VALIDACOES DE FORM ~ START
	var onlyNumbersReg = /^\\d*$/;
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var apenasLetrasNumerosUnderlinesRegex = /^\w+$/;//apenas letras, numeros e underlines
	var apenasNumerosRegex = /^\d*$/;
	// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
	var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	var usernameRegex = /^\w+$/;//apenas letras, numeros e underlines
	var isDecimal2Regex = /^\d+\.\d{0,2}$/;
	var isDecimal3Regex = /^\d+\.\d{0,3}$/;
	var isDecimal4Regex = /^\d+\.\d{0,4}$/;
	var isDecimal5Regex = /^\d+\.\d{0,5}$/;
	var isDecimal6Regex = /^\d+\.\d{0,6}$/;
	var isDecimal7Regex = /^\d+\.\d{0,7}$/;
	var isDecimal8Regex = /^\d+\.\d{0,8}$/;
	var isValidPTBRCurrencyRegex = /\d{1,3}(?:\.\d{3})*?,\d{2}/;
	
	function isDate(str)
	{
		var parms = str.split(/[\.\-\/]/);
		var yyyy = parseInt(parms[2],10);
		var mm   = parseInt(parms[1],10);
		var dd   = parseInt(parms[0],10);
		var date = new Date(yyyy,mm-1,dd,0,0,0,0);
		return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
	}
	
	function getDataMySQL(_data)
	{
		if( isEmpty(_data) ){ return ''; }
		var _ret = '';
		var _tmp = _data.split("/"); 
		_ret = _tmp[2]+'-'+_tmp[1]+'-'+_tmp[0];
		return _ret;
	}
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	

	////////////////////// Processa Form ~ START
	//ANO
	$(document).on('blur', '#reg_ano', function()
	{
		if( !frm_valida_ano(this.value,true) ){ return false; }
	});
	function frm_valida_ano(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length != 4 )
		{
			if( ret_msg )
			{
				show_msgbox('ANO deve ter 4 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//NUMERO_PLANILHA
	$(document).on('blur', '#reg_planilha_num', function()
	{
		if( !frm_valida_planilha_num(this.value,true) ){ return false; }
	});
	function frm_valida_planilha_num(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('NÚMERO PLANILHA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//UNIDADESITE
	$(document).on('blur', '#reg_unidade_site', function()
	{
		if( !frm_valida_unidade_site(this.value,true) ){ return false; }
	});
	function frm_valida_unidade_site(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 100 )
		{
			if( ret_msg )
			{
				show_msgbox('UNIDADE/SITE deve ter até 100 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//AREA
	$(document).on('blur', '#reg_area', function()
	{
		if( !frm_valida_area(this.value,true) ){ return false; }
	});
	function frm_valida_area(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('ÁREA deve ter até 30 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//SETOR
	$(document).on('blur', '#reg_setor', function()
	{
		if( !frm_valida_setor(this.value,true) ){ return false; }
	});
	function frm_valida_setor(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 20 )
		{
			if( ret_msg )
			{
				show_msgbox('SETOR deve ter até 20 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//GES
	$(document).on('blur', '#reg_ges', function()
	{
		if( !frm_valida_ges(this.value,true) ){ return false; }
	});
	function frm_valida_ges(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 7 )
		{
			if( ret_msg )
			{
				show_msgbox('GES deve ter até 7 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//CARGOFUNCAO
	$(document).on('blur', '#reg_cargo_funcao', function()
	{
		if( !frm_valida_cargo_funcao(this.value,true) ){ return false; }
	});
	function frm_valida_cargo_funcao(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 40 )
		{
			if( ret_msg )
			{
				show_msgbox('CARGO/FUNÇÃO deve ter até 40 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//CBO
	$(document).on('blur', '#reg_cbo', function()
	{
		if( !frm_valida_cbo(this.value,true) ){ return false; }
	});
	function frm_valida_cbo(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 7 )
		{
			if( ret_msg )
			{
				show_msgbox('CBO deve ter até 7 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ATIVIDADE_MACRO
	$(document).on('blur', '#reg_ativ_macro', function()
	{
		if( !frm_valida_ativ_macro(this.value,true) ){ return false; }
	});
	function frm_valida_ativ_macro(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ATIVIDADE MACRO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_1_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_1', function()
	{
		if( !frm_valida_analise_amostra_1(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #1 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_1', function()
	{
		if( !frm_valida_analise_tarefa_exec_1(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #1 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #1 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_1', function()
	{
		if( !frm_valida_analise_proc_prod_1(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #1 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #1 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_1', function()
	{
		if( !frm_valida_analise_obs_tarefa_1(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #1 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_2_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_2', function()
	{
		if( !frm_valida_analise_amostra_2(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #2 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_2', function()
	{
		if( !frm_valida_analise_tarefa_exec_2(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #2 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #2 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_2', function()
	{
		if( !frm_valida_analise_proc_prod_2(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #2 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #2 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_2', function()
	{
		if( !frm_valida_analise_obs_tarefa_2(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #2 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_3_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_3', function()
	{
		if( !frm_valida_analise_amostra_3(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #3 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_3', function()
	{
		if( !frm_valida_analise_tarefa_exec_3(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #3 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #3 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_3', function()
	{
		if( !frm_valida_analise_proc_prod_3(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #3 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #3 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_3', function()
	{
		if( !frm_valida_analise_obs_tarefa_3(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #3 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_4_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_4', function()
	{
		if( !frm_valida_analise_amostra_4(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #4 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_4', function()
	{
		if( !frm_valida_analise_tarefa_exec_4(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #4 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #4 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_4', function()
	{
		if( !frm_valida_analise_proc_prod_4(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #4 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #4 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_4', function()
	{
		if( !frm_valida_analise_obs_tarefa_4(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #4 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_5_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_5', function()
	{
		if( !frm_valida_analise_amostra_5(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #5 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_5', function()
	{
		if( !frm_valida_analise_tarefa_exec_5(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #5 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #5 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_5', function()
	{
		if( !frm_valida_analise_proc_prod_5(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #5 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #5 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_5', function()
	{
		if( !frm_valida_analise_obs_tarefa_5(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #5 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANALISE_6_AMOSTRA
	$(document).on('blur', '#reg_analise_amostra_6', function()
	{
		if( !frm_valida_analise_amostra_6(this.value,true) ){ return false; }
	});
	function frm_valida_analise_amostra_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #6 - TAREFA EXECUTADA
	$(document).on('blur', '#reg_analise_tarefa_exec_6', function()
	{
		if( !frm_valida_analise_tarefa_exec_6(this.value,true) ){ return false; }
	});
	function frm_valida_analise_tarefa_exec_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #6 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #6 - PROCESSO PRODUTIVO
	$(document).on('blur', '#reg_analise_proc_prod_6', function()
	{
		if( !frm_valida_analise_proc_prod_6(this.value,true) ){ return false; }
	});
	function frm_valida_analise_proc_prod_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #6 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//ANÁLISE #6 - OBS TAREFA
	$(document).on('blur', '#reg_analise_obs_tarefa_6', function()
	{
		if( !frm_valida_analise_obs_tarefa_6(this.value,true) ){ return false; }
	});
	function frm_valida_analise_obs_tarefa_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2000 )
		{
			if( ret_msg )
			{
				show_msgbox('ANÁLISE #6 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//JORNADA_DE_TRABALHO
	$(document).on('blur', '#reg_jor_trab', function()
	{
		if( !frm_valida_jor_trab(this.value,true) ){ return false; }
	});
	function frm_valida_jor_trab(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 5 )
		{
			if( ret_msg )
			{
				show_msgbox('JORNADA DE TRABALHO deve ter até 5 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//TEMPO_EXPOSICAO
	$(document).on('blur', '#reg_tempo_expo', function()
	{
		if( !frm_valida_tempo_expo(this.value,true) ){ return false; }
	});
	function frm_valida_tempo_expo(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('TEMPO EXPOSIÇÃO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//TIPO_EXPOSICAO
	$(document).on('blur', '#reg_tipo_expo', function()
	{
		if( !frm_valida_tipo_expo(this.value,true) ){ return false; }
	});
	function frm_valida_tipo_expo(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('TIPO EXPOSIÇÃO deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//MEIO_DE_PROPAGACAO
	$(document).on('blur', '#reg_meio_propag', function()
	{
		if( !frm_valida_meio_propag(this.value,true) ){ return false; }
	});
	function frm_valida_meio_propag(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('MEIO DE PROPAGAÇÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//FONTE_GERADORA
	$(document).on('blur', '#reg_fonte_geradora', function()
	{
		if( !frm_valida_fonte_geradora(this.value,true) ){ return false; }
	});
	function frm_valida_fonte_geradora(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('FONTE GERADORA deve ter até 30 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//MITIGACAO
	$(document).on('blur', '#reg_mitigacao', function()
	{
		if( !frm_valida_mitigacao(this.value,true) ){ return false; }
	});
	function frm_valida_mitigacao(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('MITIGAÇÃO deve ter até 30 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_1', function()
	{
		if( !frm_valida_coleta_amostra_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_1', function()
	{
		if( !frm_valida_coleta_num_serial_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_1', function()
	{
		if( !frm_valida_coleta_num_amostrador_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_1', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_1', function()
	{
		if( !frm_valida_coleta_vazao_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_1', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_1(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_1', function()
	{
		if( !frm_valida_coleta_massa1_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_1', function()
	{
		if( !frm_valida_coleta_massa2_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_1', function()
	{
		if( !frm_valida_coleta_massa3_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_1', function()
	{
		if( !frm_valida_coleta_massa4_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_1_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_1', function()
	{
		if( !frm_valida_coleta_massa5_1(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_1(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #1 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #1 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_2', function()
	{
		if( !frm_valida_coleta_amostra_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_2', function()
	{
		if( !frm_valida_coleta_num_serial_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_2', function()
	{
		if( !frm_valida_coleta_num_amostrador_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_2', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_2', function()
	{
		if( !frm_valida_coleta_vazao_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_2', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_2(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_2', function()
	{
		if( !frm_valida_coleta_massa1_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_2', function()
	{
		if( !frm_valida_coleta_massa2_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_2', function()
	{
		if( !frm_valida_coleta_massa3_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_2', function()
	{
		if( !frm_valida_coleta_massa4_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_2_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_2', function()
	{
		if( !frm_valida_coleta_massa5_2(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_2(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #2 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #2 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_3', function()
	{
		if( !frm_valida_coleta_amostra_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_3', function()
	{
		if( !frm_valida_coleta_num_serial_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_3', function()
	{
		if( !frm_valida_coleta_num_amostrador_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_3', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_3', function()
	{
		if( !frm_valida_coleta_vazao_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_3', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_3(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_3', function()
	{
		if( !frm_valida_coleta_massa1_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_3', function()
	{
		if( !frm_valida_coleta_massa2_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_3', function()
	{
		if( !frm_valida_coleta_massa3_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_3', function()
	{
		if( !frm_valida_coleta_massa4_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_3_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_3', function()
	{
		if( !frm_valida_coleta_massa5_3(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_3(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #3 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #3 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_4', function()
	{
		if( !frm_valida_coleta_amostra_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_4', function()
	{
		if( !frm_valida_coleta_num_serial_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_4', function()
	{
		if( !frm_valida_coleta_num_amostrador_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_4', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_4', function()
	{
		if( !frm_valida_coleta_vazao_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_4', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_4(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_4', function()
	{
		if( !frm_valida_coleta_massa1_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_4', function()
	{
		if( !frm_valida_coleta_massa2_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_4', function()
	{
		if( !frm_valida_coleta_massa3_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_4', function()
	{
		if( !frm_valida_coleta_massa4_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_4_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_4', function()
	{
		if( !frm_valida_coleta_massa5_4(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_4(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #4 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #4 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_5', function()
	{
		if( !frm_valida_coleta_amostra_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_5', function()
	{
		if( !frm_valida_coleta_num_serial_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_5', function()
	{
		if( !frm_valida_coleta_num_amostrador_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_5', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_5', function()
	{
		if( !frm_valida_coleta_vazao_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_5', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_5(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_5', function()
	{
		if( !frm_valida_coleta_massa1_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_5', function()
	{
		if( !frm_valida_coleta_massa2_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_5', function()
	{
		if( !frm_valida_coleta_massa3_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_5', function()
	{
		if( !frm_valida_coleta_massa4_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_5_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_5', function()
	{
		if( !frm_valida_coleta_massa5_5(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_5(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #5 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #5 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_AMOSTRA
	$(document).on('blur', '#reg_coleta_amostra_6', function()
	{
		if( !frm_valida_coleta_amostra_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_amostra_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 2 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_N_SERIAL
	$(document).on('blur', '#reg_coleta_num_serial_6', function()
	{
		if( !frm_valida_coleta_num_serial_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_serial_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - Nº SERIAL deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_N_AMOSTRADOR
	$(document).on('blur', '#reg_coleta_num_amostrador_6', function()
	{
		if( !frm_valida_coleta_num_amostrador_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_amostrador_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 10 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_N_RELATORIO_DE_ENSAIO
	$(document).on('blur', '#reg_coleta_num_relat_ensaio_6', function()
	{
		if( !frm_valida_coleta_num_relat_ensaio_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_num_relat_ensaio_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_VAZAO
	$(document).on('blur', '#reg_coleta_vazao_6', function()
	{
		if( !frm_valida_coleta_vazao_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_vazao_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - VAZÃO deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - VAZÃO com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_TEMPO_AMOSTRAGEM
	$(document).on('blur', '#reg_coleta_tempo_amostragem_6', function()
	{
		if( !frm_valida_coleta_tempo_amostragem_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_tempo_amostragem_6(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 3 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_MASSA_1
	$(document).on('blur', '#reg_coleta_massa1_6', function()
	{
		if( !frm_valida_coleta_massa1_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa1_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - MASSA #1 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - MASSA #1 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_MASSA_2
	$(document).on('blur', '#reg_coleta_massa2_6', function()
	{
		if( !frm_valida_coleta_massa2_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa2_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - MASSA #2 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - MASSA #2 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_MASSA_3
	$(document).on('blur', '#reg_coleta_massa3_6', function()
	{
		if( !frm_valida_coleta_massa3_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa3_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - MASSA #3 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - MASSA #3 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_MASSA_4
	$(document).on('blur', '#reg_coleta_massa4_6', function()
	{
		if( !frm_valida_coleta_massa4_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa4_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - MASSA #4 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - MASSA #4 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//COLETA_6_MASSA_5
	$(document).on('blur', '#reg_coleta_massa5_6', function()
	{
		if( !frm_valida_coleta_massa5_6(this.value,true) ){ return false; }
	});
	function frm_valida_coleta_massa5_6(value,ret_msg)
	{
		var _tmp = value;
		var _tmp = _tmp.replace(/,+/gi, ".");
		var _tmp = sprintf("%0.2f",_tmp);
		if( _tmp.length > 12 )
		{
			if( ret_msg )
			{
				show_msgbox('COLETA #6 - MASSA #5 deve ter até 12 dígitos!',' ','alert');
			}
			return false;
		}
		if( !isDecimal2Regex.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um valor de COLETA #6 - MASSA #5 com 2 casas decimais válido!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//RESPIRADOR
	$(document).on('blur', '#reg_respirador', function()
	{
		if( !frm_valida_respirador(this.value,true) ){ return false; }
	});
	function frm_valida_respirador(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 20 )
		{
			if( ret_msg )
			{
				show_msgbox('RESPIRADOR deve ter até 20 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//CERTIFICADO_DE_APROVACAO
	$(document).on('blur', '#reg_cert_aprov', function()
	{
		if( !frm_valida_cert_aprov(this.value,true) ){ return false; }
	});
	function frm_valida_cert_aprov(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 6 )
		{
			if( ret_msg )
			{
				show_msgbox('CERTIFICADO DE APROVAÇÃO deve ter até 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//RESPONSAVEL_CAMPO_REGISTRO
	$(document).on('blur', '#reg_registro_rc', function()
	{
		if( !frm_valida_registro_rc(this.value,true) ){ return false; }
	});
	function frm_valida_registro_rc(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 17 )
		{
			if( ret_msg )
			{
				show_msgbox('RESPONSÁVEL CAMPO - REGISTRO deve ter até 17 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//RESPONSAVEL_TECNICO_REGISTRO
	$(document).on('blur', '#reg_registro_rt', function()
	{
		if( !frm_valida_registro_rt(this.value,true) ){ return false; }
	});
	function frm_valida_registro_rt(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 17 )
		{
			if( ret_msg )
			{
				show_msgbox('RESPONSÁVEL TÉCNICO - REGISTRO deve ter até 17 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//on_change validations
	$(document).on("change", "#reg_idcliente", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_idcliente(this.value,true) ){ return false; }
		}
	});
	function frm_valida_idcliente(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CLIENTE é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_mes", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_mes(this.value,true) ){ return false; }
		}
	});
	function frm_valida_mes(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo MÊS é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_analises", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_analises(this.value,true) ){ return false; }
			setAnalises(this.value);
		}
	});
	function frm_valida_analises(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo ANÁLISES é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_coletas", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_coletas(this.value,true) ){ return false; }
			setColetas(this.value);
		}
	});
	function frm_valida_coletas(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo COLETAS é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_resp_campo_idcolaborador", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_resp_campo_idcolaborador(this.value,true) ){ return false; }
		}
	});
	function frm_valida_resp_campo_idcolaborador(value,ret_msg)
	{
		var _tmp = value;
		return true;
	}
	$(document).on("change", "#reg_resp_tecnico_idcolaborador", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_resp_tecnico_idcolaborador(this.value,true) ){ return false; }
		}
	});
	function frm_valida_resp_tecnico_idcolaborador(value,ret_msg)
	{
		var _tmp = value;
		return true;
	}
	function setAnalises(_set)
	{
		for(var i=1;i<=30;i++)
		{
			$('#panel_analise_'+i).hide();
		}
		var _set2 = _set * 5;
		for(var i=1;i<=_set2;i++)
		{
			$('#panel_analise_'+i).show();
		}
	}
	function setColetas(_set)
	{
		for(var i=1;i<=72;i++)
		{
			$('#panel_coleta_'+i).hide();
		}
		var _set2 = _set * 12;
		for(var i=1;i<=_set2;i++)
		{
			$('#panel_coleta_'+i).show();
		}
	}
	////////////////////// Processa Form ~ STOP
	

	////////////////////// Form ~ START
	var $formRegister = $('#cad-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	//Clear Inputs
	function clear_inputs()
	{
		if( $('#reg_idcliente').hasClass('selectpicker') ){ $('#reg_idcliente').selectpicker('val', ''); } else { $('#reg_idcliente').val(''); }
		$('#reg_ano').val(SYS_aaaa);
		$('#reg_mes').val(SYS_month);
		$('#reg_planilha_num').val('');
		$('#reg_unidade_site').val('');
		$('#reg_data_elaboracao').val('').datepicker('update', '');
		$('#reg_area').val('');
		$('#reg_setor').val('');
		$('#reg_ges').val('');
		$('#reg_cargo_funcao').val('');
		$('#reg_cbo').val('');
		$('#reg_ativ_macro').val('');
		$('#reg_analises').val('1');
		setAnalises('1');
		$('#reg_analise_amostra_1').val('');
		$('#reg_analise_data_amostragem_1').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_1').val('');
		$('#reg_analise_proc_prod_1').val('');
		$('#reg_analise_obs_tarefa_1').val('');
		$('#reg_analise_amostra_2').val('');
		$('#reg_analise_data_amostragem_2').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_2').val('');
		$('#reg_analise_proc_prod_2').val('');
		$('#reg_analise_obs_tarefa_2').val('');
		$('#reg_analise_amostra_3').val('');
		$('#reg_analise_data_amostragem_3').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_3').val('');
		$('#reg_analise_proc_prod_3').val('');
		$('#reg_analise_obs_tarefa_3').val('');
		$('#reg_analise_amostra_4').val('');
		$('#reg_analise_data_amostragem_4').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_4').val('');
		$('#reg_analise_proc_prod_4').val('');
		$('#reg_analise_obs_tarefa_4').val('');
		$('#reg_analise_amostra_5').val('');
		$('#reg_analise_data_amostragem_5').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_5').val('');
		$('#reg_analise_proc_prod_5').val('');
		$('#reg_analise_obs_tarefa_5').val('');
		$('#reg_analise_amostra_6').val('');
		$('#reg_analise_data_amostragem_6').val('').datepicker('update', '');
		$('#reg_analise_tarefa_exec_6').val('');
		$('#reg_analise_proc_prod_6').val('');
		$('#reg_analise_obs_tarefa_6').val('');
		$('#reg_jor_trab').val('');
		$('#reg_tempo_expo').val('');
		$('#reg_tipo_expo').val('');
		$('#reg_meio_propag').val('');
		$('#reg_fonte_geradora').val('');
		$('#reg_mitigacao').val('');
		$('#reg_coletas').val('1');
		setColetas('1');
		$('#reg_coleta_amostra_1').val('');
		$('#reg_coleta_data_1').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_1').val('');
		$('#reg_coleta_num_amostrador_1').val('');
		$('#reg_coleta_num_relat_ensaio_1').val('');
		$('#reg_coleta_vazao_1').val('');
		$('#reg_coleta_tempo_amostragem_1').val('');
		$('#reg_coleta_massa1_1').val('');
		$('#reg_coleta_massa2_1').val('');
		$('#reg_coleta_massa3_1').val('');
		$('#reg_coleta_massa4_1').val('');
		$('#reg_coleta_massa5_1').val('');
		$('#reg_coleta_amostra_2').val('');
		$('#reg_coleta_data_2').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_2').val('');
		$('#reg_coleta_num_amostrador_2').val('');
		$('#reg_coleta_num_relat_ensaio_2').val('');
		$('#reg_coleta_vazao_2').val('');
		$('#reg_coleta_tempo_amostragem_2').val('');
		$('#reg_coleta_massa1_2').val('');
		$('#reg_coleta_massa2_2').val('');
		$('#reg_coleta_massa3_2').val('');
		$('#reg_coleta_massa4_2').val('');
		$('#reg_coleta_massa5_2').val('');
		$('#reg_coleta_amostra_3').val('');
		$('#reg_coleta_data_3').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_3').val('');
		$('#reg_coleta_num_amostrador_3').val('');
		$('#reg_coleta_num_relat_ensaio_3').val('');
		$('#reg_coleta_vazao_3').val('');
		$('#reg_coleta_tempo_amostragem_3').val('');
		$('#reg_coleta_massa1_3').val('');
		$('#reg_coleta_massa2_3').val('');
		$('#reg_coleta_massa3_3').val('');
		$('#reg_coleta_massa4_3').val('');
		$('#reg_coleta_massa5_3').val('');
		$('#reg_coleta_amostra_4').val('');
		$('#reg_coleta_data_4').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_4').val('');
		$('#reg_coleta_num_amostrador_4').val('');
		$('#reg_coleta_num_relat_ensaio_4').val('');
		$('#reg_coleta_vazao_4').val('');
		$('#reg_coleta_tempo_amostragem_4').val('');
		$('#reg_coleta_massa1_4').val('');
		$('#reg_coleta_massa2_4').val('');
		$('#reg_coleta_massa3_4').val('');
		$('#reg_coleta_massa4_4').val('');
		$('#reg_coleta_massa5_4').val('');
		$('#reg_coleta_amostra_5').val('');
		$('#reg_coleta_data_5').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_5').val('');
		$('#reg_coleta_num_amostrador_5').val('');
		$('#reg_coleta_num_relat_ensaio_5').val('');
		$('#reg_coleta_vazao_5').val('');
		$('#reg_coleta_tempo_amostragem_5').val('');
		$('#reg_coleta_massa1_5').val('');
		$('#reg_coleta_massa2_5').val('');
		$('#reg_coleta_massa3_5').val('');
		$('#reg_coleta_massa4_5').val('');
		$('#reg_coleta_massa5_5').val('');
		$('#reg_coleta_amostra_6').val('');
		$('#reg_coleta_data_6').val('').datepicker('update', '');
		$('#reg_coleta_num_serial_6').val('');
		$('#reg_coleta_num_amostrador_6').val('');
		$('#reg_coleta_num_relat_ensaio_6').val('');
		$('#reg_coleta_vazao_6').val('');
		$('#reg_coleta_tempo_amostragem_6').val('');
		$('#reg_coleta_massa1_6').val('');
		$('#reg_coleta_massa2_6').val('');
		$('#reg_coleta_massa3_6').val('');
		$('#reg_coleta_massa4_6').val('');
		$('#reg_coleta_massa5_6').val('');
		$('#reg_respirador').val('');
		$('#reg_cert_aprov').val('');
		$('#reg_resp_campo_idcolaborador').val('');
		$('#reg_resp_tecnico_idcolaborador').val('');
		$('#reg_registro_rc').val('');
		$('#reg_registro_rt').val('');
		$('#reg_img_ativ_filename_file_info').val('');
		var control = $('#reg_img_ativ_filename_file');
		control.replaceWith( control = control.val(null).clone( true ) );
		$('#reg_logo_filename_file_info').val('');
		var control = $('#reg_logo_filename_file');
		control.replaceWith( control = control.val(null).clone( true ) );
	}
	
	// Help Info Popups
	$('#bt_img_ativ_filename_info').click( function () 
	{
		show_msgbox('Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.',' ','info');
		return false;
	});
	$('#bt_logo_filename_info').click( function () 
	{
		show_msgbox('Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.',' ','info');
		return false;
	});
	
	// Registrar
	$(document).on("f_show_register", function(e)
	{
		$('#div-label-type').text('NOVO REGISTRO');
		$('#bt_register').show();
		$('#bt_save').hide();
		clear_inputs();
		$('#cad-modal').modal('show');
		return false;
	});
	$('#bt_register').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_idcliente = $('#reg_idcliente').val();
		var rg_ano = $('#reg_ano').val();
		var rg_mes = $('#reg_mes').val();
		var rg_planilha_num = $('#reg_planilha_num').val();
		var rg_unidade_site = $('#reg_unidade_site').val();
		var rg_data_elaboracao = $('#reg_data_elaboracao').val();
		var rg_area = $('#reg_area').val();
		var rg_setor = $('#reg_setor').val();
		var rg_ges = $('#reg_ges').val();
		var rg_cargo_funcao = $('#reg_cargo_funcao').val();
		var rg_cbo = $('#reg_cbo').val();
		var rg_ativ_macro = $('#reg_ativ_macro').val();
		var rg_analises = $('#reg_analises').val();
		var rg_analise_amostra_1 = $('#reg_analise_amostra_1').val();
		var rg_analise_data_amostragem_1 = $('#reg_analise_data_amostragem_1').val();
		var rg_analise_tarefa_exec_1 = $('#reg_analise_tarefa_exec_1').val();
		var rg_analise_proc_prod_1 = $('#reg_analise_proc_prod_1').val();
		var rg_analise_obs_tarefa_1 = $('#reg_analise_obs_tarefa_1').val();
		var rg_analise_amostra_2 = $('#reg_analise_amostra_2').val();
		var rg_analise_data_amostragem_2 = $('#reg_analise_data_amostragem_2').val();
		var rg_analise_tarefa_exec_2 = $('#reg_analise_tarefa_exec_2').val();
		var rg_analise_proc_prod_2 = $('#reg_analise_proc_prod_2').val();
		var rg_analise_obs_tarefa_2 = $('#reg_analise_obs_tarefa_2').val();
		var rg_analise_amostra_3 = $('#reg_analise_amostra_3').val();
		var rg_analise_data_amostragem_3 = $('#reg_analise_data_amostragem_3').val();
		var rg_analise_tarefa_exec_3 = $('#reg_analise_tarefa_exec_3').val();
		var rg_analise_proc_prod_3 = $('#reg_analise_proc_prod_3').val();
		var rg_analise_obs_tarefa_3 = $('#reg_analise_obs_tarefa_3').val();
		var rg_analise_amostra_4 = $('#reg_analise_amostra_4').val();
		var rg_analise_data_amostragem_4 = $('#reg_analise_data_amostragem_4').val();
		var rg_analise_tarefa_exec_4 = $('#reg_analise_tarefa_exec_4').val();
		var rg_analise_proc_prod_4 = $('#reg_analise_proc_prod_4').val();
		var rg_analise_obs_tarefa_4 = $('#reg_analise_obs_tarefa_4').val();
		var rg_analise_amostra_5 = $('#reg_analise_amostra_5').val();
		var rg_analise_data_amostragem_5 = $('#reg_analise_data_amostragem_5').val();
		var rg_analise_tarefa_exec_5 = $('#reg_analise_tarefa_exec_5').val();
		var rg_analise_proc_prod_5 = $('#reg_analise_proc_prod_5').val();
		var rg_analise_obs_tarefa_5 = $('#reg_analise_obs_tarefa_5').val();
		var rg_analise_amostra_6 = $('#reg_analise_amostra_6').val();
		var rg_analise_data_amostragem_6 = $('#reg_analise_data_amostragem_6').val();
		var rg_analise_tarefa_exec_6 = $('#reg_analise_tarefa_exec_6').val();
		var rg_analise_proc_prod_6 = $('#reg_analise_proc_prod_6').val();
		var rg_analise_obs_tarefa_6 = $('#reg_analise_obs_tarefa_6').val();
		var rg_jor_trab = $('#reg_jor_trab').val();
		var rg_tempo_expo = $('#reg_tempo_expo').val();
		var rg_tipo_expo = $('#reg_tipo_expo').val();
		var rg_meio_propag = $('#reg_meio_propag').val();
		var rg_fonte_geradora = $('#reg_fonte_geradora').val();
		var rg_mitigacao = $('#reg_mitigacao').val();
		var rg_coletas = $('#reg_coletas').val();
		var rg_coleta_amostra_1 = $('#reg_coleta_amostra_1').val();
		var rg_coleta_data_1 = $('#reg_coleta_data_1').val();
		var rg_coleta_num_serial_1 = $('#reg_coleta_num_serial_1').val();
		var rg_coleta_num_amostrador_1 = $('#reg_coleta_num_amostrador_1').val();
		var rg_coleta_num_relat_ensaio_1 = $('#reg_coleta_num_relat_ensaio_1').val();
		var rg_coleta_vazao_1 = $('#reg_coleta_vazao_1').val(); var rg_coleta_vazao_1 = rg_coleta_vazao_1.replace(/,+/gi, "."); var rg_coleta_vazao_1 = sprintf("%0.2f",rg_coleta_vazao_1);
		var rg_coleta_tempo_amostragem_1 = $('#reg_coleta_tempo_amostragem_1').val();
		var rg_coleta_massa1_1 = $('#reg_coleta_massa1_1').val(); var rg_coleta_massa1_1 = rg_coleta_massa1_1.replace(/,+/gi, "."); var rg_coleta_massa1_1 = sprintf("%0.2f",rg_coleta_massa1_1);
		var rg_coleta_massa2_1 = $('#reg_coleta_massa2_1').val(); var rg_coleta_massa2_1 = rg_coleta_massa2_1.replace(/,+/gi, "."); var rg_coleta_massa2_1 = sprintf("%0.2f",rg_coleta_massa2_1);
		var rg_coleta_massa3_1 = $('#reg_coleta_massa3_1').val(); var rg_coleta_massa3_1 = rg_coleta_massa3_1.replace(/,+/gi, "."); var rg_coleta_massa3_1 = sprintf("%0.2f",rg_coleta_massa3_1);
		var rg_coleta_massa4_1 = $('#reg_coleta_massa4_1').val(); var rg_coleta_massa4_1 = rg_coleta_massa4_1.replace(/,+/gi, "."); var rg_coleta_massa4_1 = sprintf("%0.2f",rg_coleta_massa4_1);
		var rg_coleta_massa5_1 = $('#reg_coleta_massa5_1').val(); var rg_coleta_massa5_1 = rg_coleta_massa5_1.replace(/,+/gi, "."); var rg_coleta_massa5_1 = sprintf("%0.2f",rg_coleta_massa5_1);
		var rg_coleta_amostra_2 = $('#reg_coleta_amostra_2').val();
		var rg_coleta_data_2 = $('#reg_coleta_data_2').val();
		var rg_coleta_num_serial_2 = $('#reg_coleta_num_serial_2').val();
		var rg_coleta_num_amostrador_2 = $('#reg_coleta_num_amostrador_2').val();
		var rg_coleta_num_relat_ensaio_2 = $('#reg_coleta_num_relat_ensaio_2').val();
		var rg_coleta_vazao_2 = $('#reg_coleta_vazao_2').val(); var rg_coleta_vazao_2 = rg_coleta_vazao_2.replace(/,+/gi, "."); var rg_coleta_vazao_2 = sprintf("%0.2f",rg_coleta_vazao_2);
		var rg_coleta_tempo_amostragem_2 = $('#reg_coleta_tempo_amostragem_2').val();
		var rg_coleta_massa1_2 = $('#reg_coleta_massa1_2').val(); var rg_coleta_massa1_2 = rg_coleta_massa1_2.replace(/,+/gi, "."); var rg_coleta_massa1_2 = sprintf("%0.2f",rg_coleta_massa1_2);
		var rg_coleta_massa2_2 = $('#reg_coleta_massa2_2').val(); var rg_coleta_massa2_2 = rg_coleta_massa2_2.replace(/,+/gi, "."); var rg_coleta_massa2_2 = sprintf("%0.2f",rg_coleta_massa2_2);
		var rg_coleta_massa3_2 = $('#reg_coleta_massa3_2').val(); var rg_coleta_massa3_2 = rg_coleta_massa3_2.replace(/,+/gi, "."); var rg_coleta_massa3_2 = sprintf("%0.2f",rg_coleta_massa3_2);
		var rg_coleta_massa4_2 = $('#reg_coleta_massa4_2').val(); var rg_coleta_massa4_2 = rg_coleta_massa4_2.replace(/,+/gi, "."); var rg_coleta_massa4_2 = sprintf("%0.2f",rg_coleta_massa4_2);
		var rg_coleta_massa5_2 = $('#reg_coleta_massa5_2').val(); var rg_coleta_massa5_2 = rg_coleta_massa5_2.replace(/,+/gi, "."); var rg_coleta_massa5_2 = sprintf("%0.2f",rg_coleta_massa5_2);
		var rg_coleta_amostra_3 = $('#reg_coleta_amostra_3').val();
		var rg_coleta_data_3 = $('#reg_coleta_data_3').val();
		var rg_coleta_num_serial_3 = $('#reg_coleta_num_serial_3').val();
		var rg_coleta_num_amostrador_3 = $('#reg_coleta_num_amostrador_3').val();
		var rg_coleta_num_relat_ensaio_3 = $('#reg_coleta_num_relat_ensaio_3').val();
		var rg_coleta_vazao_3 = $('#reg_coleta_vazao_3').val(); var rg_coleta_vazao_3 = rg_coleta_vazao_3.replace(/,+/gi, "."); var rg_coleta_vazao_3 = sprintf("%0.2f",rg_coleta_vazao_3);
		var rg_coleta_tempo_amostragem_3 = $('#reg_coleta_tempo_amostragem_3').val();
		var rg_coleta_massa1_3 = $('#reg_coleta_massa1_3').val(); var rg_coleta_massa1_3 = rg_coleta_massa1_3.replace(/,+/gi, "."); var rg_coleta_massa1_3 = sprintf("%0.2f",rg_coleta_massa1_3);
		var rg_coleta_massa2_3 = $('#reg_coleta_massa2_3').val(); var rg_coleta_massa2_3 = rg_coleta_massa2_3.replace(/,+/gi, "."); var rg_coleta_massa2_3 = sprintf("%0.2f",rg_coleta_massa2_3);
		var rg_coleta_massa3_3 = $('#reg_coleta_massa3_3').val(); var rg_coleta_massa3_3 = rg_coleta_massa3_3.replace(/,+/gi, "."); var rg_coleta_massa3_3 = sprintf("%0.2f",rg_coleta_massa3_3);
		var rg_coleta_massa4_3 = $('#reg_coleta_massa4_3').val(); var rg_coleta_massa4_3 = rg_coleta_massa4_3.replace(/,+/gi, "."); var rg_coleta_massa4_3 = sprintf("%0.2f",rg_coleta_massa4_3);
		var rg_coleta_massa5_3 = $('#reg_coleta_massa5_3').val(); var rg_coleta_massa5_3 = rg_coleta_massa5_3.replace(/,+/gi, "."); var rg_coleta_massa5_3 = sprintf("%0.2f",rg_coleta_massa5_3);
		var rg_coleta_amostra_4 = $('#reg_coleta_amostra_4').val();
		var rg_coleta_data_4 = $('#reg_coleta_data_4').val();
		var rg_coleta_num_serial_4 = $('#reg_coleta_num_serial_4').val();
		var rg_coleta_num_amostrador_4 = $('#reg_coleta_num_amostrador_4').val();
		var rg_coleta_num_relat_ensaio_4 = $('#reg_coleta_num_relat_ensaio_4').val();
		var rg_coleta_vazao_4 = $('#reg_coleta_vazao_4').val(); var rg_coleta_vazao_4 = rg_coleta_vazao_4.replace(/,+/gi, "."); var rg_coleta_vazao_4 = sprintf("%0.2f",rg_coleta_vazao_4);
		var rg_coleta_tempo_amostragem_4 = $('#reg_coleta_tempo_amostragem_4').val();
		var rg_coleta_massa1_4 = $('#reg_coleta_massa1_4').val(); var rg_coleta_massa1_4 = rg_coleta_massa1_4.replace(/,+/gi, "."); var rg_coleta_massa1_4 = sprintf("%0.2f",rg_coleta_massa1_4);
		var rg_coleta_massa2_4 = $('#reg_coleta_massa2_4').val(); var rg_coleta_massa2_4 = rg_coleta_massa2_4.replace(/,+/gi, "."); var rg_coleta_massa2_4 = sprintf("%0.2f",rg_coleta_massa2_4);
		var rg_coleta_massa3_4 = $('#reg_coleta_massa3_4').val(); var rg_coleta_massa3_4 = rg_coleta_massa3_4.replace(/,+/gi, "."); var rg_coleta_massa3_4 = sprintf("%0.2f",rg_coleta_massa3_4);
		var rg_coleta_massa4_4 = $('#reg_coleta_massa4_4').val(); var rg_coleta_massa4_4 = rg_coleta_massa4_4.replace(/,+/gi, "."); var rg_coleta_massa4_4 = sprintf("%0.2f",rg_coleta_massa4_4);
		var rg_coleta_massa5_4 = $('#reg_coleta_massa5_4').val(); var rg_coleta_massa5_4 = rg_coleta_massa5_4.replace(/,+/gi, "."); var rg_coleta_massa5_4 = sprintf("%0.2f",rg_coleta_massa5_4);
		var rg_coleta_amostra_5 = $('#reg_coleta_amostra_5').val();
		var rg_coleta_data_5 = $('#reg_coleta_data_5').val();
		var rg_coleta_num_serial_5 = $('#reg_coleta_num_serial_5').val();
		var rg_coleta_num_amostrador_5 = $('#reg_coleta_num_amostrador_5').val();
		var rg_coleta_num_relat_ensaio_5 = $('#reg_coleta_num_relat_ensaio_5').val();
		var rg_coleta_vazao_5 = $('#reg_coleta_vazao_5').val(); var rg_coleta_vazao_5 = rg_coleta_vazao_5.replace(/,+/gi, "."); var rg_coleta_vazao_5 = sprintf("%0.2f",rg_coleta_vazao_5);
		var rg_coleta_tempo_amostragem_5 = $('#reg_coleta_tempo_amostragem_5').val();
		var rg_coleta_massa1_5 = $('#reg_coleta_massa1_5').val(); var rg_coleta_massa1_5 = rg_coleta_massa1_5.replace(/,+/gi, "."); var rg_coleta_massa1_5 = sprintf("%0.2f",rg_coleta_massa1_5);
		var rg_coleta_massa2_5 = $('#reg_coleta_massa2_5').val(); var rg_coleta_massa2_5 = rg_coleta_massa2_5.replace(/,+/gi, "."); var rg_coleta_massa2_5 = sprintf("%0.2f",rg_coleta_massa2_5);
		var rg_coleta_massa3_5 = $('#reg_coleta_massa3_5').val(); var rg_coleta_massa3_5 = rg_coleta_massa3_5.replace(/,+/gi, "."); var rg_coleta_massa3_5 = sprintf("%0.2f",rg_coleta_massa3_5);
		var rg_coleta_massa4_5 = $('#reg_coleta_massa4_5').val(); var rg_coleta_massa4_5 = rg_coleta_massa4_5.replace(/,+/gi, "."); var rg_coleta_massa4_5 = sprintf("%0.2f",rg_coleta_massa4_5);
		var rg_coleta_massa5_5 = $('#reg_coleta_massa5_5').val(); var rg_coleta_massa5_5 = rg_coleta_massa5_5.replace(/,+/gi, "."); var rg_coleta_massa5_5 = sprintf("%0.2f",rg_coleta_massa5_5);
		var rg_coleta_amostra_6 = $('#reg_coleta_amostra_6').val();
		var rg_coleta_data_6 = $('#reg_coleta_data_6').val();
		var rg_coleta_num_serial_6 = $('#reg_coleta_num_serial_6').val();
		var rg_coleta_num_amostrador_6 = $('#reg_coleta_num_amostrador_6').val();
		var rg_coleta_num_relat_ensaio_6 = $('#reg_coleta_num_relat_ensaio_6').val();
		var rg_coleta_vazao_6 = $('#reg_coleta_vazao_6').val(); var rg_coleta_vazao_6 = rg_coleta_vazao_6.replace(/,+/gi, "."); var rg_coleta_vazao_6 = sprintf("%0.2f",rg_coleta_vazao_6);
		var rg_coleta_tempo_amostragem_6 = $('#reg_coleta_tempo_amostragem_6').val();
		var rg_coleta_massa1_6 = $('#reg_coleta_massa1_6').val(); var rg_coleta_massa1_6 = rg_coleta_massa1_6.replace(/,+/gi, "."); var rg_coleta_massa1_6 = sprintf("%0.2f",rg_coleta_massa1_6);
		var rg_coleta_massa2_6 = $('#reg_coleta_massa2_6').val(); var rg_coleta_massa2_6 = rg_coleta_massa2_6.replace(/,+/gi, "."); var rg_coleta_massa2_6 = sprintf("%0.2f",rg_coleta_massa2_6);
		var rg_coleta_massa3_6 = $('#reg_coleta_massa3_6').val(); var rg_coleta_massa3_6 = rg_coleta_massa3_6.replace(/,+/gi, "."); var rg_coleta_massa3_6 = sprintf("%0.2f",rg_coleta_massa3_6);
		var rg_coleta_massa4_6 = $('#reg_coleta_massa4_6').val(); var rg_coleta_massa4_6 = rg_coleta_massa4_6.replace(/,+/gi, "."); var rg_coleta_massa4_6 = sprintf("%0.2f",rg_coleta_massa4_6);
		var rg_coleta_massa5_6 = $('#reg_coleta_massa5_6').val(); var rg_coleta_massa5_6 = rg_coleta_massa5_6.replace(/,+/gi, "."); var rg_coleta_massa5_6 = sprintf("%0.2f",rg_coleta_massa5_6);
		var rg_respirador = $('#reg_respirador').val();
		var rg_cert_aprov = $('#reg_cert_aprov').val();
		var rg_resp_campo_idcolaborador = $('#reg_resp_campo_idcolaborador').val();
		var rg_resp_tecnico_idcolaborador = $('#reg_resp_tecnico_idcolaborador').val();
		var rg_registro_rc = $('#reg_registro_rc').val();
		var rg_registro_rt = $('#reg_registro_rt').val();
		var rg_img_ativ_filename_file_info = $('#reg_img_ativ_filename_file_info').val();
		var rg_img_ativ_filename_filename_ext = rg_img_ativ_filename_file_info.split('.').pop().toLowerCase();
		var rg_img_ativ_filename_file_upload = document.getElementById('reg_img_ativ_filename_file').files[0];
		var rg_img_ativ_filename_max_file_size = $('#reg_img_ativ_filename_max_file_size').val();
		var rg_logo_filename_file_info = $('#reg_logo_filename_file_info').val();
		var rg_logo_filename_filename_ext = rg_logo_filename_file_info.split('.').pop().toLowerCase();
		var rg_logo_filename_file_upload = document.getElementById('reg_logo_filename_file').files[0];
		var rg_logo_filename_max_file_size = $('#reg_logo_filename_max_file_size').val();
		
		//CLIENTE
		if (isEmpty(rg_idcliente)) { show_msgbox('O campo CLIENTE é obrigatório!',' ','alert'); $('#reg_idcliente').focus(); return false; }
		//ANO
		if (isEmpty(rg_ano)) { show_msgbox('O campo ANO é obrigatório!',' ','alert'); $('#reg_ano').focus(); return false; }
		if(!isEmpty(rg_ano) && (rg_ano.length != 4)){ show_msgbox('ANO deve ter 4 dígitos!',' ','alert'); $('#reg_ano').focus(); return false; }
		//MES
		if (isEmpty(rg_mes)) { show_msgbox('O campo MÊS é obrigatório!',' ','alert'); $('#reg_mes').focus(); return false; }
		//NUMERO PLANILHA
		if (isEmpty(rg_planilha_num)) { show_msgbox('O campo NÚMERO PLANILHA é obrigatório!',' ','alert'); $('#reg_planilha_num').focus(); return false; }
		if (!isEmpty(rg_planilha_num) && rg_planilha_num.length > 2) { show_msgbox('NÚMERO PLANILHA deve ter até 2 dígitos!',' ','alert'); $('#reg_planilha_num').focus(); return false; }
		//UNIDADE/SITE
		//if (isEmpty(rg_unidade_site)) { show_msgbox('O campo UNIDADE/SITE é obrigatório!',' ','alert'); $('#reg_unidade_site').focus(); return false; }
		if (!isEmpty(rg_unidade_site) && rg_unidade_site.length > 100) { show_msgbox('UNIDADE/SITE deve ter até 100 dígitos!',' ','alert'); $('#reg_unidade_site').focus(); return false; }
		//DATA ELABORACAO
		if (!isEmpty(rg_data_elaboracao) && (rg_data_elaboracao.length != 10)){ show_msgbox('DATA ELABORAÇÃO deve ter 10 dígitos!',' ','alert'); $('#reg_data_elaboracao').focus(); return false; }
		if (!isEmpty(rg_data_elaboracao)){ if(!isDate(rg_data_elaboracao)){ show_msgbox('DATA ELABORAÇÃO - Informe uma data válida!',' ','alert'); $('#reg_data_elaboracao').focus(); return false; } else { rg_data_elaboracao = getDataMySQL(rg_data_elaboracao); } }
		//AREA
		if (!isEmpty(rg_area) && rg_area.length > 30) { show_msgbox('ÁREA deve ter até 30 dígitos!',' ','alert'); $('#reg_area').focus(); return false; }
		//SETOR
		if (!isEmpty(rg_setor) && rg_setor.length > 20) { show_msgbox('SETOR deve ter até 20 dígitos!',' ','alert'); $('#reg_setor').focus(); return false; }
		//GES
		if (!isEmpty(rg_ges) && rg_ges.length > 7) { show_msgbox('GES deve ter até 7 dígitos!',' ','alert'); $('#reg_ges').focus(); return false; }
		//CARGO/FUNCAO
		if (!isEmpty(rg_cargo_funcao) && rg_cargo_funcao.length > 40) { show_msgbox('CARGO/FUNÇÃO deve ter até 40 dígitos!',' ','alert'); $('#reg_cargo_funcao').focus(); return false; }
		//CBO
		if (!isEmpty(rg_cbo) && rg_cbo.length > 7) { show_msgbox('CBO deve ter até 7 dígitos!',' ','alert'); $('#reg_cbo').focus(); return false; }
		//ATIVIDADE MACRO
		if (!isEmpty(rg_ativ_macro) && rg_ativ_macro.length > 2000) { show_msgbox('ATIVIDADE MACRO deve ter até 2000 dígitos!',' ','alert'); $('#reg_ativ_macro').focus(); return false; }
		//ANALISES
		if (isEmpty(rg_analises)) { show_msgbox('O campo ANÁLISES é obrigatório!',' ','alert'); $('#reg_analises').focus(); return false; }
		//ANALISE #1 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_1) && rg_analise_amostra_1.length > 2) { show_msgbox('ANÁLISE #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_1').focus(); return false; }
		//ANALISE #1 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_1) && (rg_analise_data_amostragem_1.length != 10)){ show_msgbox('ANÁLISE #1 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_1').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_1)){ if(!isDate(rg_analise_data_amostragem_1)){ show_msgbox('ANÁLISE #1 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_1').focus(); return false; } else { rg_analise_data_amostragem_1 = getDataMySQL(rg_analise_data_amostragem_1); } }
		//ANALISE #1 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_1) && rg_analise_tarefa_exec_1.length > 2000) { show_msgbox('ANÁLISE #1 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_1').focus(); return false; }
		//ANALISE #1 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_1) && rg_analise_proc_prod_1.length > 2000) { show_msgbox('ANÁLISE #1 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_1').focus(); return false; }
		//ANALISE #1 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_1) && rg_analise_obs_tarefa_1.length > 2000) { show_msgbox('ANÁLISE #1 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_1').focus(); return false; }
		//ANALISE #2 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_2) && rg_analise_amostra_2.length > 2) { show_msgbox('ANÁLISE #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_2').focus(); return false; }
		//ANALISE #2 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_2) && (rg_analise_data_amostragem_2.length != 10)){ show_msgbox('ANÁLISE #2 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_2').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_2)){ if(!isDate(rg_analise_data_amostragem_2)){ show_msgbox('ANÁLISE #2 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_2').focus(); return false; } else { rg_analise_data_amostragem_2 = getDataMySQL(rg_analise_data_amostragem_2); } }
		//ANALISE #2 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_2) && rg_analise_tarefa_exec_2.length > 2000) { show_msgbox('ANÁLISE #2 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_2').focus(); return false; }
		//ANALISE #2 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_2) && rg_analise_proc_prod_2.length > 2000) { show_msgbox('ANÁLISE #2 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_2').focus(); return false; }
		//ANALISE #2 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_2) && rg_analise_obs_tarefa_2.length > 2000) { show_msgbox('ANÁLISE #2 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_2').focus(); return false; }
		//ANALISE #3 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_3) && rg_analise_amostra_3.length > 2) { show_msgbox('ANÁLISE #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_3').focus(); return false; }
		//ANALISE #3 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_3) && (rg_analise_data_amostragem_3.length != 10)){ show_msgbox('ANÁLISE #3 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_3').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_3)){ if(!isDate(rg_analise_data_amostragem_3)){ show_msgbox('ANÁLISE #3 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_3').focus(); return false; } else { rg_analise_data_amostragem_3 = getDataMySQL(rg_analise_data_amostragem_3); } }
		//ANALISE #3 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_3) && rg_analise_tarefa_exec_3.length > 2000) { show_msgbox('ANÁLISE #3 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_3').focus(); return false; }
		//ANALISE #3 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_3) && rg_analise_proc_prod_3.length > 2000) { show_msgbox('ANÁLISE #3 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_3').focus(); return false; }
		//ANALISE #3 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_3) && rg_analise_obs_tarefa_3.length > 2000) { show_msgbox('ANÁLISE #3 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_3').focus(); return false; }
		//ANALISE #4 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_4) && rg_analise_amostra_4.length > 2) { show_msgbox('ANÁLISE #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_4').focus(); return false; }
		//ANALISE #4 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_4) && (rg_analise_data_amostragem_4.length != 10)){ show_msgbox('ANÁLISE #4 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_4').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_4)){ if(!isDate(rg_analise_data_amostragem_4)){ show_msgbox('ANÁLISE #4 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_4').focus(); return false; } else { rg_analise_data_amostragem_4 = getDataMySQL(rg_analise_data_amostragem_4); } }
		//ANALISE #4 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_4) && rg_analise_tarefa_exec_4.length > 2000) { show_msgbox('ANÁLISE #4 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_4').focus(); return false; }
		//ANALISE #4 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_4) && rg_analise_proc_prod_4.length > 2000) { show_msgbox('ANÁLISE #4 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_4').focus(); return false; }
		//ANALISE #4 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_4) && rg_analise_obs_tarefa_4.length > 2000) { show_msgbox('ANÁLISE #4 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_4').focus(); return false; }
		//ANALISE #5 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_5) && rg_analise_amostra_5.length > 2) { show_msgbox('ANÁLISE #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_5').focus(); return false; }
		//ANALISE #5 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_5) && (rg_analise_data_amostragem_5.length != 10)){ show_msgbox('ANÁLISE #5 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_5').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_5)){ if(!isDate(rg_analise_data_amostragem_5)){ show_msgbox('ANÁLISE #5 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_5').focus(); return false; } else { rg_analise_data_amostragem_5 = getDataMySQL(rg_analise_data_amostragem_5); } }
		//ANALISE #5 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_5) && rg_analise_tarefa_exec_5.length > 2000) { show_msgbox('ANÁLISE #5 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_5').focus(); return false; }
		//ANALISE #5 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_5) && rg_analise_proc_prod_5.length > 2000) { show_msgbox('ANÁLISE #5 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_5').focus(); return false; }
		//ANALISE #5 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_5) && rg_analise_obs_tarefa_5.length > 2000) { show_msgbox('ANÁLISE #5 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_5').focus(); return false; }
		//ANALISE #6 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_6) && rg_analise_amostra_6.length > 2) { show_msgbox('ANÁLISE #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_6').focus(); return false; }
		//ANALISE #6 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_6) && (rg_analise_data_amostragem_6.length != 10)){ show_msgbox('ANÁLISE #6 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_6').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_6)){ if(!isDate(rg_analise_data_amostragem_6)){ show_msgbox('ANÁLISE #6 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_6').focus(); return false; } else { rg_analise_data_amostragem_6 = getDataMySQL(rg_analise_data_amostragem_6); } }
		//ANALISE #6 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_6) && rg_analise_tarefa_exec_6.length > 2000) { show_msgbox('ANÁLISE #6 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_6').focus(); return false; }
		//ANALISE #6 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_6) && rg_analise_proc_prod_6.length > 2000) { show_msgbox('ANÁLISE #6 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_6').focus(); return false; }
		//ANALISE #6 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_6) && rg_analise_obs_tarefa_6.length > 2000) { show_msgbox('ANÁLISE #6 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_6').focus(); return false; }
		//JORNADA DE TRABALHO
		if (!isEmpty(rg_jor_trab) && rg_jor_trab.length > 5) { show_msgbox('JORNADA DE TRABALHO deve ter até 5 dígitos!',' ','alert'); $('#reg_jor_trab').focus(); return false; }
		//TEMPO EXPOSICAO
		if (!isEmpty(rg_tempo_expo) && rg_tempo_expo.length > 3) { show_msgbox('TEMPO EXPOSIÇÃO deve ter até 3 dígitos!',' ','alert'); $('#reg_tempo_expo').focus(); return false; }
		//TIPO EXPOSICAO
		if (!isEmpty(rg_tipo_expo) && rg_tipo_expo.length > 2) { show_msgbox('TIPO EXPOSIÇÃO deve ter até 2 dígitos!',' ','alert'); $('#reg_tipo_expo').focus(); return false; }
		//MEIO DE PROPAGACAO
		if (!isEmpty(rg_meio_propag) && rg_meio_propag.length > 12) { show_msgbox('MEIO DE PROPAGAÇÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_meio_propag').focus(); return false; }
		//FONTE GERADORA
		if (!isEmpty(rg_fonte_geradora) && rg_fonte_geradora.length > 30) { show_msgbox('FONTE GERADORA deve ter até 30 dígitos!',' ','alert'); $('#reg_fonte_geradora').focus(); return false; }
		//MITIGACAO
		if (!isEmpty(rg_mitigacao) && rg_mitigacao.length > 30) { show_msgbox('MITIGAÇÃO deve ter até 30 dígitos!',' ','alert'); $('#reg_mitigacao').focus(); return false; }
		//COLETAS
		if (isEmpty(rg_coletas)) { show_msgbox('O campo COLETAS é obrigatório!',' ','alert'); $('#reg_coletas').focus(); return false; }
		//COLETA #1 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_1) && rg_coleta_amostra_1.length > 2) { show_msgbox('COLETA #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_1').focus(); return false; }
		//COLETA #1 - DATA
		if (!isEmpty(rg_coleta_data_1) && (rg_coleta_data_1.length != 10)){ show_msgbox('COLETA #1 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_1').focus(); return false; }
		if (!isEmpty(rg_coleta_data_1)){ if(!isDate(rg_coleta_data_1)){ show_msgbox('COLETA #1 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_1').focus(); return false; } else { rg_coleta_data_1 = getDataMySQL(rg_coleta_data_1); } }
		//COLETA #1 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_1) && rg_coleta_num_serial_1.length > 6) { show_msgbox('COLETA #1 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_1').focus(); return false; }
		//COLETA #1 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_1) && rg_coleta_num_amostrador_1.length > 10) { show_msgbox('COLETA #1 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_1').focus(); return false; }
		//COLETA #1 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_1) && rg_coleta_num_relat_ensaio_1.length > 3) { show_msgbox('COLETA #1 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_1').focus(); return false; }
		//COLETA #1 - VAZAO
		if (!isEmpty(rg_coleta_vazao_1) && rg_coleta_vazao_1.length > 12) { show_msgbox('COLETA #1 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_1').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_1) && !isDecimal2Regex.test(rg_coleta_vazao_1)) { show_msgbox('COLETA #1 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_1').focus(); return false; }
		//COLETA #1 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_1) && rg_coleta_tempo_amostragem_1.length > 3) { show_msgbox('COLETA #1 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_1').focus(); return false; }
		//COLETA #1 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_1) && rg_coleta_massa1_1.length > 12) { show_msgbox('COLETA #1 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_1) && !isDecimal2Regex.test(rg_coleta_massa1_1)) { show_msgbox('COLETA #1 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_1').focus(); return false; }
		//COLETA #1 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_1) && rg_coleta_massa2_1.length > 12) { show_msgbox('COLETA #1 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_1) && !isDecimal2Regex.test(rg_coleta_massa2_1)) { show_msgbox('COLETA #1 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_1').focus(); return false; }
		//COLETA #1 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_1) && rg_coleta_massa3_1.length > 12) { show_msgbox('COLETA #1 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_1) && !isDecimal2Regex.test(rg_coleta_massa3_1)) { show_msgbox('COLETA #1 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_1').focus(); return false; }
		//COLETA #1 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_1) && rg_coleta_massa4_1.length > 12) { show_msgbox('COLETA #1 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_1) && !isDecimal2Regex.test(rg_coleta_massa4_1)) { show_msgbox('COLETA #1 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_1').focus(); return false; }
		//COLETA #1 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_1) && rg_coleta_massa5_1.length > 12) { show_msgbox('COLETA #1 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_1) && !isDecimal2Regex.test(rg_coleta_massa5_1)) { show_msgbox('COLETA #1 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_1').focus(); return false; }
		//COLETA #2 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_2) && rg_coleta_amostra_2.length > 2) { show_msgbox('COLETA #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_2').focus(); return false; }
		//COLETA #2 - DATA
		if (!isEmpty(rg_coleta_data_2) && (rg_coleta_data_2.length != 10)){ show_msgbox('COLETA #2 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_2').focus(); return false; }
		if (!isEmpty(rg_coleta_data_2)){ if(!isDate(rg_coleta_data_2)){ show_msgbox('COLETA #2 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_2').focus(); return false; } else { rg_coleta_data_2 = getDataMySQL(rg_coleta_data_2); } }
		//COLETA #2 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_2) && rg_coleta_num_serial_2.length > 6) { show_msgbox('COLETA #2 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_2').focus(); return false; }
		//COLETA #2 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_2) && rg_coleta_num_amostrador_2.length > 10) { show_msgbox('COLETA #2 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_2').focus(); return false; }
		//COLETA #2 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_2) && rg_coleta_num_relat_ensaio_2.length > 3) { show_msgbox('COLETA #2 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_2').focus(); return false; }
		//COLETA #2 - VAZAO
		if (!isEmpty(rg_coleta_vazao_2) && rg_coleta_vazao_2.length > 12) { show_msgbox('COLETA #2 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_2').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_2) && !isDecimal2Regex.test(rg_coleta_vazao_2)) { show_msgbox('COLETA #2 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_2').focus(); return false; }
		//COLETA #2 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_2) && rg_coleta_tempo_amostragem_2.length > 3) { show_msgbox('COLETA #2 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_2').focus(); return false; }
		//COLETA #2 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_2) && rg_coleta_massa1_2.length > 12) { show_msgbox('COLETA #2 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_2) && !isDecimal2Regex.test(rg_coleta_massa1_2)) { show_msgbox('COLETA #2 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_2').focus(); return false; }
		//COLETA #2 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_2) && rg_coleta_massa2_2.length > 12) { show_msgbox('COLETA #2 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_2) && !isDecimal2Regex.test(rg_coleta_massa2_2)) { show_msgbox('COLETA #2 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_2').focus(); return false; }
		//COLETA #2 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_2) && rg_coleta_massa3_2.length > 12) { show_msgbox('COLETA #2 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_2) && !isDecimal2Regex.test(rg_coleta_massa3_2)) { show_msgbox('COLETA #2 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_2').focus(); return false; }
		//COLETA #2 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_2) && rg_coleta_massa4_2.length > 12) { show_msgbox('COLETA #2 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_2) && !isDecimal2Regex.test(rg_coleta_massa4_2)) { show_msgbox('COLETA #2 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_2').focus(); return false; }
		//COLETA #2 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_2) && rg_coleta_massa5_2.length > 12) { show_msgbox('COLETA #2 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_2) && !isDecimal2Regex.test(rg_coleta_massa5_2)) { show_msgbox('COLETA #2 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_2').focus(); return false; }
		//COLETA #3 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_3) && rg_coleta_amostra_3.length > 2) { show_msgbox('COLETA #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_3').focus(); return false; }
		//COLETA #3 - DATA
		if (!isEmpty(rg_coleta_data_3) && (rg_coleta_data_3.length != 10)){ show_msgbox('COLETA #3 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_3').focus(); return false; }
		if (!isEmpty(rg_coleta_data_3)){ if(!isDate(rg_coleta_data_3)){ show_msgbox('COLETA #3 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_3').focus(); return false; } else { rg_coleta_data_3 = getDataMySQL(rg_coleta_data_3); } }
		//COLETA #3 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_3) && rg_coleta_num_serial_3.length > 6) { show_msgbox('COLETA #3 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_3').focus(); return false; }
		//COLETA #3 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_3) && rg_coleta_num_amostrador_3.length > 10) { show_msgbox('COLETA #3 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_3').focus(); return false; }
		//COLETA #3 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_3) && rg_coleta_num_relat_ensaio_3.length > 3) { show_msgbox('COLETA #3 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_3').focus(); return false; }
		//COLETA #3 - VAZAO
		if (!isEmpty(rg_coleta_vazao_3) && rg_coleta_vazao_3.length > 12) { show_msgbox('COLETA #3 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_3').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_3) && !isDecimal2Regex.test(rg_coleta_vazao_3)) { show_msgbox('COLETA #3 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_3').focus(); return false; }
		//COLETA #3 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_3) && rg_coleta_tempo_amostragem_3.length > 3) { show_msgbox('COLETA #3 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_3').focus(); return false; }
		//COLETA #3 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_3) && rg_coleta_massa1_3.length > 12) { show_msgbox('COLETA #3 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_3) && !isDecimal2Regex.test(rg_coleta_massa1_3)) { show_msgbox('COLETA #3 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_3').focus(); return false; }
		//COLETA #3 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_3) && rg_coleta_massa2_3.length > 12) { show_msgbox('COLETA #3 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_3) && !isDecimal2Regex.test(rg_coleta_massa2_3)) { show_msgbox('COLETA #3 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_3').focus(); return false; }
		//COLETA #3 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_3) && rg_coleta_massa3_3.length > 12) { show_msgbox('COLETA #3 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_3) && !isDecimal2Regex.test(rg_coleta_massa3_3)) { show_msgbox('COLETA #3 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_3').focus(); return false; }
		//COLETA #3 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_3) && rg_coleta_massa4_3.length > 12) { show_msgbox('COLETA #3 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_3) && !isDecimal2Regex.test(rg_coleta_massa4_3)) { show_msgbox('COLETA #3 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_3').focus(); return false; }
		//COLETA #3 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_3) && rg_coleta_massa5_3.length > 12) { show_msgbox('COLETA #3 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_3) && !isDecimal2Regex.test(rg_coleta_massa5_3)) { show_msgbox('COLETA #3 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_3').focus(); return false; }
		//COLETA #4 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_4) && rg_coleta_amostra_4.length > 2) { show_msgbox('COLETA #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_4').focus(); return false; }
		//COLETA #4 - DATA
		if (!isEmpty(rg_coleta_data_4) && (rg_coleta_data_4.length != 10)){ show_msgbox('COLETA #4 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_4').focus(); return false; }
		if (!isEmpty(rg_coleta_data_4)){ if(!isDate(rg_coleta_data_4)){ show_msgbox('COLETA #4 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_4').focus(); return false; } else { rg_coleta_data_4 = getDataMySQL(rg_coleta_data_4); } }
		//COLETA #4 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_4) && rg_coleta_num_serial_4.length > 6) { show_msgbox('COLETA #4 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_4').focus(); return false; }
		//COLETA #4 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_4) && rg_coleta_num_amostrador_4.length > 10) { show_msgbox('COLETA #4 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_4').focus(); return false; }
		//COLETA #4 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_4) && rg_coleta_num_relat_ensaio_4.length > 3) { show_msgbox('COLETA #4 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_4').focus(); return false; }
		//COLETA #4 - VAZAO
		if (!isEmpty(rg_coleta_vazao_4) && rg_coleta_vazao_4.length > 12) { show_msgbox('COLETA #4 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_4').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_4) && !isDecimal2Regex.test(rg_coleta_vazao_4)) { show_msgbox('COLETA #4 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_4').focus(); return false; }
		//COLETA #4 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_4) && rg_coleta_tempo_amostragem_4.length > 3) { show_msgbox('COLETA #4 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_4').focus(); return false; }
		//COLETA #4 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_4) && rg_coleta_massa1_4.length > 12) { show_msgbox('COLETA #4 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_4) && !isDecimal2Regex.test(rg_coleta_massa1_4)) { show_msgbox('COLETA #4 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_4').focus(); return false; }
		//COLETA #4 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_4) && rg_coleta_massa2_4.length > 12) { show_msgbox('COLETA #4 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_4) && !isDecimal2Regex.test(rg_coleta_massa2_4)) { show_msgbox('COLETA #4 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_4').focus(); return false; }
		//COLETA #4 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_4) && rg_coleta_massa3_4.length > 12) { show_msgbox('COLETA #4 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_4) && !isDecimal2Regex.test(rg_coleta_massa3_4)) { show_msgbox('COLETA #4 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_4').focus(); return false; }
		//COLETA #4 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_4) && rg_coleta_massa4_4.length > 12) { show_msgbox('COLETA #4 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_4) && !isDecimal2Regex.test(rg_coleta_massa4_4)) { show_msgbox('COLETA #4 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_4').focus(); return false; }
		//COLETA #4 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_4) && rg_coleta_massa5_4.length > 12) { show_msgbox('COLETA #4 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_4) && !isDecimal2Regex.test(rg_coleta_massa5_4)) { show_msgbox('COLETA #4 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_4').focus(); return false; }
		//COLETA #5 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_5) && rg_coleta_amostra_5.length > 2) { show_msgbox('COLETA #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_5').focus(); return false; }
		//COLETA #5 - DATA
		if (!isEmpty(rg_coleta_data_5) && (rg_coleta_data_5.length != 10)){ show_msgbox('COLETA #5 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_5').focus(); return false; }
		if (!isEmpty(rg_coleta_data_5)){ if(!isDate(rg_coleta_data_5)){ show_msgbox('COLETA #5 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_5').focus(); return false; } else { rg_coleta_data_5 = getDataMySQL(rg_coleta_data_5); } }
		//COLETA #5 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_5) && rg_coleta_num_serial_5.length > 6) { show_msgbox('COLETA #5 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_5').focus(); return false; }
		//COLETA #5 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_5) && rg_coleta_num_amostrador_5.length > 10) { show_msgbox('COLETA #5 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_5').focus(); return false; }
		//COLETA #5 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_5) && rg_coleta_num_relat_ensaio_5.length > 3) { show_msgbox('COLETA #5 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_5').focus(); return false; }
		//COLETA #5 - VAZAO
		if (!isEmpty(rg_coleta_vazao_5) && rg_coleta_vazao_5.length > 12) { show_msgbox('COLETA #5 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_5').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_5) && !isDecimal2Regex.test(rg_coleta_vazao_5)) { show_msgbox('COLETA #5 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_5').focus(); return false; }
		//COLETA #5 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_5) && rg_coleta_tempo_amostragem_5.length > 3) { show_msgbox('COLETA #5 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_5').focus(); return false; }
		//COLETA #5 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_5) && rg_coleta_massa1_5.length > 12) { show_msgbox('COLETA #5 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_5) && !isDecimal2Regex.test(rg_coleta_massa1_5)) { show_msgbox('COLETA #5 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_5').focus(); return false; }
		//COLETA #5 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_5) && rg_coleta_massa2_5.length > 12) { show_msgbox('COLETA #5 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_5) && !isDecimal2Regex.test(rg_coleta_massa2_5)) { show_msgbox('COLETA #5 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_5').focus(); return false; }
		//COLETA #5 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_5) && rg_coleta_massa3_5.length > 12) { show_msgbox('COLETA #5 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_5) && !isDecimal2Regex.test(rg_coleta_massa3_5)) { show_msgbox('COLETA #5 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_5').focus(); return false; }
		//COLETA #5 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_5) && rg_coleta_massa4_5.length > 12) { show_msgbox('COLETA #5 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_5) && !isDecimal2Regex.test(rg_coleta_massa4_5)) { show_msgbox('COLETA #5 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_5').focus(); return false; }
		//COLETA #5 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_5) && rg_coleta_massa5_5.length > 12) { show_msgbox('COLETA #5 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_5) && !isDecimal2Regex.test(rg_coleta_massa5_5)) { show_msgbox('COLETA #5 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_5').focus(); return false; }
		//COLETA #6 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_6) && rg_coleta_amostra_6.length > 2) { show_msgbox('COLETA #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_6').focus(); return false; }
		//COLETA #6 - DATA
		if (!isEmpty(rg_coleta_data_6) && (rg_coleta_data_6.length != 10)){ show_msgbox('COLETA #6 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_6').focus(); return false; }
		if (!isEmpty(rg_coleta_data_6)){ if(!isDate(rg_coleta_data_6)){ show_msgbox('COLETA #6 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_6').focus(); return false; } else { rg_coleta_data_6 = getDataMySQL(rg_coleta_data_6); } }
		//COLETA #6 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_6) && rg_coleta_num_serial_6.length > 6) { show_msgbox('COLETA #6 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_6').focus(); return false; }
		//COLETA #6 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_6) && rg_coleta_num_amostrador_6.length > 10) { show_msgbox('COLETA #6 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_6').focus(); return false; }
		//COLETA #6 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_6) && rg_coleta_num_relat_ensaio_6.length > 3) { show_msgbox('COLETA #6 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_6').focus(); return false; }
		//COLETA #6 - VAZAO
		if (!isEmpty(rg_coleta_vazao_6) && rg_coleta_vazao_6.length > 12) { show_msgbox('COLETA #6 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_6').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_6) && !isDecimal2Regex.test(rg_coleta_vazao_6)) { show_msgbox('COLETA #6 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_6').focus(); return false; }
		//COLETA #6 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_6) && rg_coleta_tempo_amostragem_6.length > 3) { show_msgbox('COLETA #6 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_6').focus(); return false; }
		//COLETA #6 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_6) && rg_coleta_massa1_6.length > 12) { show_msgbox('COLETA #6 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_6) && !isDecimal2Regex.test(rg_coleta_massa1_6)) { show_msgbox('COLETA #6 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_6').focus(); return false; }
		//COLETA #6 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_6) && rg_coleta_massa2_6.length > 12) { show_msgbox('COLETA #6 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_6) && !isDecimal2Regex.test(rg_coleta_massa2_6)) { show_msgbox('COLETA #6 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_6').focus(); return false; }
		//COLETA #6 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_6) && rg_coleta_massa3_6.length > 12) { show_msgbox('COLETA #6 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_6) && !isDecimal2Regex.test(rg_coleta_massa3_6)) { show_msgbox('COLETA #6 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_6').focus(); return false; }
		//COLETA #6 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_6) && rg_coleta_massa4_6.length > 12) { show_msgbox('COLETA #6 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_6) && !isDecimal2Regex.test(rg_coleta_massa4_6)) { show_msgbox('COLETA #6 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_6').focus(); return false; }
		//COLETA #6 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_6) && rg_coleta_massa5_6.length > 12) { show_msgbox('COLETA #6 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_6) && !isDecimal2Regex.test(rg_coleta_massa5_6)) { show_msgbox('COLETA #6 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_6').focus(); return false; }
		//RESPIRADOR
		if (!isEmpty(rg_respirador) && rg_respirador.length > 20) { show_msgbox('RESPIRADOR deve ter até 20 dígitos!',' ','alert'); $('#reg_respirador').focus(); return false; }
		//CERTIFICADO DE APROVACAO
		if (!isEmpty(rg_cert_aprov) && rg_cert_aprov.length > 6) { show_msgbox('CERTIFICADO DE APROVAÇÃO deve ter até 6 dígitos!',' ','alert'); $('#reg_cert_aprov').focus(); return false; }
		//RESPONSAVEL CAMPO - REGISTRO
		if (!isEmpty(rg_registro_rc) && rg_registro_rc.length > 17) { show_msgbox('RESPONSÁVEL CAMPO - REGISTRO deve ter até 17 dígitos!',' ','alert'); $('#reg_registro_rc').focus(); return false; }
		//RESPONSAVEL TECNICO - REGISTRO
		if (!isEmpty(rg_registro_rt) && rg_registro_rt.length > 17) { show_msgbox('RESPONSÁVEL TÉCNICO - REGISTRO deve ter até 17 dígitos!',' ','alert'); $('#reg_registro_rt').focus(); return false; }
		//IMAGEM ATIVIDADE
		if(rg_img_ativ_filename_file_upload)
		{
			if ( rg_img_ativ_filename_filename_ext != 'gif' &&
			     rg_img_ativ_filename_filename_ext != 'jpg' &&
			     rg_img_ativ_filename_filename_ext != 'jpeg' &&
			     rg_img_ativ_filename_filename_ext != 'png' &&
			     rg_img_ativ_filename_file_upload.type != "image/gif" &&
			     rg_img_ativ_filename_file_upload.type != "image/jpeg" &&
			     rg_img_ativ_filename_file_upload.type != "image/jpeg" &&
			     rg_img_ativ_filename_file_upload.type != "image/png"){ show_msgbox('IMAGEM ATIVIDADE - Arquivo inválido!',' ','alert'); return false; }
			
			if ( rg_img_ativ_filename_file_upload.size > 2000000 ){ show_msgbox('Tamanho máximo permitido para o arquivo é de 2000000 bytes!',' ','alert'); return false; }
		}
		//IMAGEM LOGOMARCA
		if(rg_logo_filename_file_upload)
		{
			if ( rg_logo_filename_filename_ext != 'gif' &&
			     rg_logo_filename_filename_ext != 'jpg' &&
			     rg_logo_filename_filename_ext != 'jpeg' &&
			     rg_logo_filename_filename_ext != 'png' &&
			     rg_logo_filename_file_upload.type != "image/gif" &&
			     rg_logo_filename_file_upload.type != "image/jpeg" &&
			     rg_logo_filename_file_upload.type != "image/jpeg" &&
			     rg_logo_filename_file_upload.type != "image/png"){ show_msgbox('IMAGEM LOGOMARCA - Arquivo inválido!',' ','alert'); return false; }
			
			if ( rg_logo_filename_file_upload.size > 80000 ){ show_msgbox('Tamanho máximo permitido para o arquivo é de 80000 bytes!',' ','alert'); return false; }
		}
		
		//Formatacao Case
		rg_registro_rc = rg_registro_rc.toUpperCase();
		rg_registro_rt = rg_registro_rt.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "2");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("idcliente", rg_idcliente);
		_postdata.append("ano", rg_ano);
		_postdata.append("mes", rg_mes);
		_postdata.append("planilha_num", rg_planilha_num);
		_postdata.append("unidade_site", rg_unidade_site);
		_postdata.append("data_elaboracao", rg_data_elaboracao);
		_postdata.append("area", rg_area);
		_postdata.append("setor", rg_setor);
		_postdata.append("ges", rg_ges);
		_postdata.append("cargo_funcao", rg_cargo_funcao);
		_postdata.append("cbo", rg_cbo);
		_postdata.append("ativ_macro", rg_ativ_macro);
		_postdata.append("analises", rg_analises);
		_postdata.append("analise_amostra_1", rg_analise_amostra_1);
		_postdata.append("analise_data_amostragem_1", rg_analise_data_amostragem_1);
		_postdata.append("analise_tarefa_exec_1", rg_analise_tarefa_exec_1);
		_postdata.append("analise_proc_prod_1", rg_analise_proc_prod_1);
		_postdata.append("analise_obs_tarefa_1", rg_analise_obs_tarefa_1);
		_postdata.append("analise_amostra_2", rg_analise_amostra_2);
		_postdata.append("analise_data_amostragem_2", rg_analise_data_amostragem_2);
		_postdata.append("analise_tarefa_exec_2", rg_analise_tarefa_exec_2);
		_postdata.append("analise_proc_prod_2", rg_analise_proc_prod_2);
		_postdata.append("analise_obs_tarefa_2", rg_analise_obs_tarefa_2);
		_postdata.append("analise_amostra_3", rg_analise_amostra_3);
		_postdata.append("analise_data_amostragem_3", rg_analise_data_amostragem_3);
		_postdata.append("analise_tarefa_exec_3", rg_analise_tarefa_exec_3);
		_postdata.append("analise_proc_prod_3", rg_analise_proc_prod_3);
		_postdata.append("analise_obs_tarefa_3", rg_analise_obs_tarefa_3);
		_postdata.append("analise_amostra_4", rg_analise_amostra_4);
		_postdata.append("analise_data_amostragem_4", rg_analise_data_amostragem_4);
		_postdata.append("analise_tarefa_exec_4", rg_analise_tarefa_exec_4);
		_postdata.append("analise_proc_prod_4", rg_analise_proc_prod_4);
		_postdata.append("analise_obs_tarefa_4", rg_analise_obs_tarefa_4);
		_postdata.append("analise_amostra_5", rg_analise_amostra_5);
		_postdata.append("analise_data_amostragem_5", rg_analise_data_amostragem_5);
		_postdata.append("analise_tarefa_exec_5", rg_analise_tarefa_exec_5);
		_postdata.append("analise_proc_prod_5", rg_analise_proc_prod_5);
		_postdata.append("analise_obs_tarefa_5", rg_analise_obs_tarefa_5);
		_postdata.append("analise_amostra_6", rg_analise_amostra_6);
		_postdata.append("analise_data_amostragem_6", rg_analise_data_amostragem_6);
		_postdata.append("analise_tarefa_exec_6", rg_analise_tarefa_exec_6);
		_postdata.append("analise_proc_prod_6", rg_analise_proc_prod_6);
		_postdata.append("analise_obs_tarefa_6", rg_analise_obs_tarefa_6);
		_postdata.append("jor_trab", rg_jor_trab);
		_postdata.append("tempo_expo", rg_tempo_expo);
		_postdata.append("tipo_expo", rg_tipo_expo);
		_postdata.append("meio_propag", rg_meio_propag);
		_postdata.append("fonte_geradora", rg_fonte_geradora);
		_postdata.append("mitigacao", rg_mitigacao);
		_postdata.append("coletas", rg_coletas);
		_postdata.append("coleta_amostra_1", rg_coleta_amostra_1);
		_postdata.append("coleta_data_1", rg_coleta_data_1);
		_postdata.append("coleta_num_serial_1", rg_coleta_num_serial_1);
		_postdata.append("coleta_num_amostrador_1", rg_coleta_num_amostrador_1);
		_postdata.append("coleta_num_relat_ensaio_1", rg_coleta_num_relat_ensaio_1);
		_postdata.append("coleta_vazao_1", rg_coleta_vazao_1);
		_postdata.append("coleta_tempo_amostragem_1", rg_coleta_tempo_amostragem_1);
		_postdata.append("coleta_massa1_1", rg_coleta_massa1_1);
		_postdata.append("coleta_massa2_1", rg_coleta_massa2_1);
		_postdata.append("coleta_massa3_1", rg_coleta_massa3_1);
		_postdata.append("coleta_massa4_1", rg_coleta_massa4_1);
		_postdata.append("coleta_massa5_1", rg_coleta_massa5_1);
		_postdata.append("coleta_amostra_2", rg_coleta_amostra_2);
		_postdata.append("coleta_data_2", rg_coleta_data_2);
		_postdata.append("coleta_num_serial_2", rg_coleta_num_serial_2);
		_postdata.append("coleta_num_amostrador_2", rg_coleta_num_amostrador_2);
		_postdata.append("coleta_num_relat_ensaio_2", rg_coleta_num_relat_ensaio_2);
		_postdata.append("coleta_vazao_2", rg_coleta_vazao_2);
		_postdata.append("coleta_tempo_amostragem_2", rg_coleta_tempo_amostragem_2);
		_postdata.append("coleta_massa1_2", rg_coleta_massa1_2);
		_postdata.append("coleta_massa2_2", rg_coleta_massa2_2);
		_postdata.append("coleta_massa3_2", rg_coleta_massa3_2);
		_postdata.append("coleta_massa4_2", rg_coleta_massa4_2);
		_postdata.append("coleta_massa5_2", rg_coleta_massa5_2);
		_postdata.append("coleta_amostra_3", rg_coleta_amostra_3);
		_postdata.append("coleta_data_3", rg_coleta_data_3);
		_postdata.append("coleta_num_serial_3", rg_coleta_num_serial_3);
		_postdata.append("coleta_num_amostrador_3", rg_coleta_num_amostrador_3);
		_postdata.append("coleta_num_relat_ensaio_3", rg_coleta_num_relat_ensaio_3);
		_postdata.append("coleta_vazao_3", rg_coleta_vazao_3);
		_postdata.append("coleta_tempo_amostragem_3", rg_coleta_tempo_amostragem_3);
		_postdata.append("coleta_massa1_3", rg_coleta_massa1_3);
		_postdata.append("coleta_massa2_3", rg_coleta_massa2_3);
		_postdata.append("coleta_massa3_3", rg_coleta_massa3_3);
		_postdata.append("coleta_massa4_3", rg_coleta_massa4_3);
		_postdata.append("coleta_massa5_3", rg_coleta_massa5_3);
		_postdata.append("coleta_amostra_4", rg_coleta_amostra_4);
		_postdata.append("coleta_data_4", rg_coleta_data_4);
		_postdata.append("coleta_num_serial_4", rg_coleta_num_serial_4);
		_postdata.append("coleta_num_amostrador_4", rg_coleta_num_amostrador_4);
		_postdata.append("coleta_num_relat_ensaio_4", rg_coleta_num_relat_ensaio_4);
		_postdata.append("coleta_vazao_4", rg_coleta_vazao_4);
		_postdata.append("coleta_tempo_amostragem_4", rg_coleta_tempo_amostragem_4);
		_postdata.append("coleta_massa1_4", rg_coleta_massa1_4);
		_postdata.append("coleta_massa2_4", rg_coleta_massa2_4);
		_postdata.append("coleta_massa3_4", rg_coleta_massa3_4);
		_postdata.append("coleta_massa4_4", rg_coleta_massa4_4);
		_postdata.append("coleta_massa5_4", rg_coleta_massa5_4);
		_postdata.append("coleta_amostra_5", rg_coleta_amostra_5);
		_postdata.append("coleta_data_5", rg_coleta_data_5);
		_postdata.append("coleta_num_serial_5", rg_coleta_num_serial_5);
		_postdata.append("coleta_num_amostrador_5", rg_coleta_num_amostrador_5);
		_postdata.append("coleta_num_relat_ensaio_5", rg_coleta_num_relat_ensaio_5);
		_postdata.append("coleta_vazao_5", rg_coleta_vazao_5);
		_postdata.append("coleta_tempo_amostragem_5", rg_coleta_tempo_amostragem_5);
		_postdata.append("coleta_massa1_5", rg_coleta_massa1_5);
		_postdata.append("coleta_massa2_5", rg_coleta_massa2_5);
		_postdata.append("coleta_massa3_5", rg_coleta_massa3_5);
		_postdata.append("coleta_massa4_5", rg_coleta_massa4_5);
		_postdata.append("coleta_massa5_5", rg_coleta_massa5_5);
		_postdata.append("coleta_amostra_6", rg_coleta_amostra_6);
		_postdata.append("coleta_data_6", rg_coleta_data_6);
		_postdata.append("coleta_num_serial_6", rg_coleta_num_serial_6);
		_postdata.append("coleta_num_amostrador_6", rg_coleta_num_amostrador_6);
		_postdata.append("coleta_num_relat_ensaio_6", rg_coleta_num_relat_ensaio_6);
		_postdata.append("coleta_vazao_6", rg_coleta_vazao_6);
		_postdata.append("coleta_tempo_amostragem_6", rg_coleta_tempo_amostragem_6);
		_postdata.append("coleta_massa1_6", rg_coleta_massa1_6);
		_postdata.append("coleta_massa2_6", rg_coleta_massa2_6);
		_postdata.append("coleta_massa3_6", rg_coleta_massa3_6);
		_postdata.append("coleta_massa4_6", rg_coleta_massa4_6);
		_postdata.append("coleta_massa5_6", rg_coleta_massa5_6);
		_postdata.append("respirador", rg_respirador);
		_postdata.append("cert_aprov", rg_cert_aprov);
		_postdata.append("resp_campo_idcolaborador", rg_resp_campo_idcolaborador);
		_postdata.append("resp_tecnico_idcolaborador", rg_resp_tecnico_idcolaborador);
		_postdata.append("registro_rc", rg_registro_rc);
		_postdata.append("registro_rt", rg_registro_rt);
		_postdata.append("img_ativ_filename_file_info", rg_img_ativ_filename_file_info);
		_postdata.append("img_ativ_filename_file", rg_img_ativ_filename_file_upload);
		_postdata.append("img_ativ_filename_max_file_size", rg_img_ativ_filename_max_file_size);
		_postdata.append("logo_filename_file_info", rg_logo_filename_file_info);
		_postdata.append("logo_filename_file", rg_logo_filename_file_upload);
		_postdata.append("logo_filename_max_file_size", rg_logo_filename_max_file_size);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-part.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						clear_inputs();
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						$('#cad-modal').scrollTop(0);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});

	// Editar
	$(document).on("f_show_edit", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de executar edição do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "6");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-part.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], idcliente:tmp[3], ano:tmp[4], mes:tmp[5], planilha_num:tmp[6], unidade_site:tmp[7], data_elaboracao:tmp[8], area:tmp[9], setor:tmp[10], ges:tmp[11], cargo_funcao:tmp[12], cbo:tmp[13], ativ_macro:tmp[14], analises:tmp[15], analise_amostra_1:tmp[16], analise_data_amostragem_1:tmp[17], analise_tarefa_exec_1:tmp[18], analise_proc_prod_1:tmp[19], analise_obs_tarefa_1:tmp[20], analise_amostra_2:tmp[21], analise_data_amostragem_2:tmp[22], analise_tarefa_exec_2:tmp[23], analise_proc_prod_2:tmp[24], analise_obs_tarefa_2:tmp[25], analise_amostra_3:tmp[26], analise_data_amostragem_3:tmp[27], analise_tarefa_exec_3:tmp[28], analise_proc_prod_3:tmp[29], analise_obs_tarefa_3:tmp[30], analise_amostra_4:tmp[31], analise_data_amostragem_4:tmp[32], analise_tarefa_exec_4:tmp[33], analise_proc_prod_4:tmp[34], analise_obs_tarefa_4:tmp[35], analise_amostra_5:tmp[36], analise_data_amostragem_5:tmp[37], analise_tarefa_exec_5:tmp[38], analise_proc_prod_5:tmp[39], analise_obs_tarefa_5:tmp[40], analise_amostra_6:tmp[41], analise_data_amostragem_6:tmp[42], analise_tarefa_exec_6:tmp[43], analise_proc_prod_6:tmp[44], analise_obs_tarefa_6:tmp[45], jor_trab:tmp[46], tempo_expo:tmp[47], tipo_expo:tmp[48], meio_propag:tmp[49], fonte_geradora:tmp[50], mitigacao:tmp[51], coletas:tmp[52], coleta_amostra_1:tmp[53], coleta_data_1:tmp[54], coleta_num_serial_1:tmp[55], coleta_num_amostrador_1:tmp[56], coleta_num_relat_ensaio_1:tmp[57], coleta_vazao_1:tmp[58], coleta_tempo_amostragem_1:tmp[59], coleta_massa1_1:tmp[60], coleta_massa2_1:tmp[61], coleta_massa3_1:tmp[62], coleta_massa4_1:tmp[63], coleta_massa5_1:tmp[64], coleta_amostra_2:tmp[65], coleta_data_2:tmp[66], coleta_num_serial_2:tmp[67], coleta_num_amostrador_2:tmp[68], coleta_num_relat_ensaio_2:tmp[69], coleta_vazao_2:tmp[70], coleta_tempo_amostragem_2:tmp[71], coleta_massa1_2:tmp[72], coleta_massa2_2:tmp[73], coleta_massa3_2:tmp[74], coleta_massa4_2:tmp[75], coleta_massa5_2:tmp[76], coleta_amostra_3:tmp[77], coleta_data_3:tmp[78], coleta_num_serial_3:tmp[79], coleta_num_amostrador_3:tmp[80], coleta_num_relat_ensaio_3:tmp[81], coleta_vazao_3:tmp[82], coleta_tempo_amostragem_3:tmp[83], coleta_massa1_3:tmp[84], coleta_massa2_3:tmp[85], coleta_massa3_3:tmp[86], coleta_massa4_3:tmp[87], coleta_massa5_3:tmp[88], coleta_amostra_4:tmp[89], coleta_data_4:tmp[90], coleta_num_serial_4:tmp[91], coleta_num_amostrador_4:tmp[92], coleta_num_relat_ensaio_4:tmp[93], coleta_vazao_4:tmp[94], coleta_tempo_amostragem_4:tmp[95], coleta_massa1_4:tmp[96], coleta_massa2_4:tmp[97], coleta_massa3_4:tmp[98], coleta_massa4_4:tmp[99], coleta_massa5_4:tmp[100], coleta_amostra_5:tmp[101], coleta_data_5:tmp[102], coleta_num_serial_5:tmp[103], coleta_num_amostrador_5:tmp[104], coleta_num_relat_ensaio_5:tmp[105], coleta_vazao_5:tmp[106], coleta_tempo_amostragem_5:tmp[107], coleta_massa1_5:tmp[108], coleta_massa2_5:tmp[109], coleta_massa3_5:tmp[110], coleta_massa4_5:tmp[111], coleta_massa5_5:tmp[112], coleta_amostra_6:tmp[113], coleta_data_6:tmp[114], coleta_num_serial_6:tmp[115], coleta_num_amostrador_6:tmp[116], coleta_num_relat_ensaio_6:tmp[117], coleta_vazao_6:tmp[118], coleta_tempo_amostragem_6:tmp[119], coleta_massa1_6:tmp[120], coleta_massa2_6:tmp[121], coleta_massa3_6:tmp[122], coleta_massa4_6:tmp[123], coleta_massa5_6:tmp[124], respirador:tmp[125], cert_aprov:tmp[126], resp_campo_idcolaborador:tmp[127], resp_tecnico_idcolaborador:tmp[128], registro_rc:tmp[129], registro_rt:tmp[130], img_ativ_filename:tmp[131], logo_filename:tmp[132]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#div-label-type').text('EDITAR REGISTRO');
						$('#bt_register').hide();
						$('#bt_save').show();
						$('#reg_id').val(_id);
						if( $('#reg_idcliente').hasClass('selectpicker') ){ $('#reg_idcliente').selectpicker('val', myRet.idcliente); } else { $('#reg_idcliente').val(myRet.idcliente); }
						$('#reg_ano').val(myRet.ano);
						$('#reg_mes').val(myRet.mes);
						$('#reg_planilha_num').val(myRet.planilha_num);
						$('#reg_unidade_site').val(myRet.unidade_site);
						$('#reg_data_elaboracao').val(myRet.data_elaboracao).datepicker('update');
						$('#reg_area').val(myRet.area);
						$('#reg_setor').val(myRet.setor);
						$('#reg_ges').val(myRet.ges);
						$('#reg_cargo_funcao').val(myRet.cargo_funcao);
						$('#reg_cbo').val(myRet.cbo);
						$('#reg_ativ_macro').val(myRet.ativ_macro);
						$('#reg_analises').val(myRet.analises);
						setAnalises(myRet.analises);
						$('#reg_analise_amostra_1').val(myRet.analise_amostra_1);
						$('#reg_analise_data_amostragem_1').val(myRet.analise_data_amostragem_1).datepicker('update');
						$('#reg_analise_tarefa_exec_1').val(myRet.analise_tarefa_exec_1);
						$('#reg_analise_proc_prod_1').val(myRet.analise_proc_prod_1);
						$('#reg_analise_obs_tarefa_1').val(myRet.analise_obs_tarefa_1);
						$('#reg_analise_amostra_2').val(myRet.analise_amostra_2);
						$('#reg_analise_data_amostragem_2').val(myRet.analise_data_amostragem_2).datepicker('update');
						$('#reg_analise_tarefa_exec_2').val(myRet.analise_tarefa_exec_2);
						$('#reg_analise_proc_prod_2').val(myRet.analise_proc_prod_2);
						$('#reg_analise_obs_tarefa_2').val(myRet.analise_obs_tarefa_2);
						$('#reg_analise_amostra_3').val(myRet.analise_amostra_3);
						$('#reg_analise_data_amostragem_3').val(myRet.analise_data_amostragem_3).datepicker('update');
						$('#reg_analise_tarefa_exec_3').val(myRet.analise_tarefa_exec_3);
						$('#reg_analise_proc_prod_3').val(myRet.analise_proc_prod_3);
						$('#reg_analise_obs_tarefa_3').val(myRet.analise_obs_tarefa_3);
						$('#reg_analise_amostra_4').val(myRet.analise_amostra_4);
						$('#reg_analise_data_amostragem_4').val(myRet.analise_data_amostragem_4).datepicker('update');
						$('#reg_analise_tarefa_exec_4').val(myRet.analise_tarefa_exec_4);
						$('#reg_analise_proc_prod_4').val(myRet.analise_proc_prod_4);
						$('#reg_analise_obs_tarefa_4').val(myRet.analise_obs_tarefa_4);
						$('#reg_analise_amostra_5').val(myRet.analise_amostra_5);
						$('#reg_analise_data_amostragem_5').val(myRet.analise_data_amostragem_5).datepicker('update');
						$('#reg_analise_tarefa_exec_5').val(myRet.analise_tarefa_exec_5);
						$('#reg_analise_proc_prod_5').val(myRet.analise_proc_prod_5);
						$('#reg_analise_obs_tarefa_5').val(myRet.analise_obs_tarefa_5);
						$('#reg_analise_amostra_6').val(myRet.analise_amostra_6);
						$('#reg_analise_data_amostragem_6').val(myRet.analise_data_amostragem_6).datepicker('update');
						$('#reg_analise_tarefa_exec_6').val(myRet.analise_tarefa_exec_6);
						$('#reg_analise_proc_prod_6').val(myRet.analise_proc_prod_6);
						$('#reg_analise_obs_tarefa_6').val(myRet.analise_obs_tarefa_6);
						$('#reg_jor_trab').val(myRet.jor_trab);
						$('#reg_tempo_expo').val(myRet.tempo_expo);
						$('#reg_tipo_expo').val(myRet.tipo_expo);
						$('#reg_meio_propag').val(myRet.meio_propag);
						$('#reg_fonte_geradora').val(myRet.fonte_geradora);
						$('#reg_mitigacao').val(myRet.mitigacao);
						$('#reg_coletas').val(myRet.coletas);
						setColetas(myRet.coletas);
						$('#reg_coleta_amostra_1').val(myRet.coleta_amostra_1);
						$('#reg_coleta_data_1').val(myRet.coleta_data_1).datepicker('update');
						$('#reg_coleta_num_serial_1').val(myRet.coleta_num_serial_1);
						$('#reg_coleta_num_amostrador_1').val(myRet.coleta_num_amostrador_1);
						$('#reg_coleta_num_relat_ensaio_1').val(myRet.coleta_num_relat_ensaio_1);
						$('#reg_coleta_vazao_1').val(myRet.coleta_vazao_1);
						$('#reg_coleta_tempo_amostragem_1').val(myRet.coleta_tempo_amostragem_1);
						$('#reg_coleta_massa1_1').val(myRet.coleta_massa1_1);
						$('#reg_coleta_massa2_1').val(myRet.coleta_massa2_1);
						$('#reg_coleta_massa3_1').val(myRet.coleta_massa3_1);
						$('#reg_coleta_massa4_1').val(myRet.coleta_massa4_1);
						$('#reg_coleta_massa5_1').val(myRet.coleta_massa5_1);
						$('#reg_coleta_amostra_2').val(myRet.coleta_amostra_2);
						$('#reg_coleta_data_2').val(myRet.coleta_data_2).datepicker('update');
						$('#reg_coleta_num_serial_2').val(myRet.coleta_num_serial_2);
						$('#reg_coleta_num_amostrador_2').val(myRet.coleta_num_amostrador_2);
						$('#reg_coleta_num_relat_ensaio_2').val(myRet.coleta_num_relat_ensaio_2);
						$('#reg_coleta_vazao_2').val(myRet.coleta_vazao_2);
						$('#reg_coleta_tempo_amostragem_2').val(myRet.coleta_tempo_amostragem_2);
						$('#reg_coleta_massa1_2').val(myRet.coleta_massa1_2);
						$('#reg_coleta_massa2_2').val(myRet.coleta_massa2_2);
						$('#reg_coleta_massa3_2').val(myRet.coleta_massa3_2);
						$('#reg_coleta_massa4_2').val(myRet.coleta_massa4_2);
						$('#reg_coleta_massa5_2').val(myRet.coleta_massa5_2);
						$('#reg_coleta_amostra_3').val(myRet.coleta_amostra_3);
						$('#reg_coleta_data_3').val(myRet.coleta_data_3).datepicker('update');
						$('#reg_coleta_num_serial_3').val(myRet.coleta_num_serial_3);
						$('#reg_coleta_num_amostrador_3').val(myRet.coleta_num_amostrador_3);
						$('#reg_coleta_num_relat_ensaio_3').val(myRet.coleta_num_relat_ensaio_3);
						$('#reg_coleta_vazao_3').val(myRet.coleta_vazao_3);
						$('#reg_coleta_tempo_amostragem_3').val(myRet.coleta_tempo_amostragem_3);
						$('#reg_coleta_massa1_3').val(myRet.coleta_massa1_3);
						$('#reg_coleta_massa2_3').val(myRet.coleta_massa2_3);
						$('#reg_coleta_massa3_3').val(myRet.coleta_massa3_3);
						$('#reg_coleta_massa4_3').val(myRet.coleta_massa4_3);
						$('#reg_coleta_massa5_3').val(myRet.coleta_massa5_3);
						$('#reg_coleta_amostra_4').val(myRet.coleta_amostra_4);
						$('#reg_coleta_data_4').val(myRet.coleta_data_4).datepicker('update');
						$('#reg_coleta_num_serial_4').val(myRet.coleta_num_serial_4);
						$('#reg_coleta_num_amostrador_4').val(myRet.coleta_num_amostrador_4);
						$('#reg_coleta_num_relat_ensaio_4').val(myRet.coleta_num_relat_ensaio_4);
						$('#reg_coleta_vazao_4').val(myRet.coleta_vazao_4);
						$('#reg_coleta_tempo_amostragem_4').val(myRet.coleta_tempo_amostragem_4);
						$('#reg_coleta_massa1_4').val(myRet.coleta_massa1_4);
						$('#reg_coleta_massa2_4').val(myRet.coleta_massa2_4);
						$('#reg_coleta_massa3_4').val(myRet.coleta_massa3_4);
						$('#reg_coleta_massa4_4').val(myRet.coleta_massa4_4);
						$('#reg_coleta_massa5_4').val(myRet.coleta_massa5_4);
						$('#reg_coleta_amostra_5').val(myRet.coleta_amostra_5);
						$('#reg_coleta_data_5').val(myRet.coleta_data_5).datepicker('update');
						$('#reg_coleta_num_serial_5').val(myRet.coleta_num_serial_5);
						$('#reg_coleta_num_amostrador_5').val(myRet.coleta_num_amostrador_5);
						$('#reg_coleta_num_relat_ensaio_5').val(myRet.coleta_num_relat_ensaio_5);
						$('#reg_coleta_vazao_5').val(myRet.coleta_vazao_5);
						$('#reg_coleta_tempo_amostragem_5').val(myRet.coleta_tempo_amostragem_5);
						$('#reg_coleta_massa1_5').val(myRet.coleta_massa1_5);
						$('#reg_coleta_massa2_5').val(myRet.coleta_massa2_5);
						$('#reg_coleta_massa3_5').val(myRet.coleta_massa3_5);
						$('#reg_coleta_massa4_5').val(myRet.coleta_massa4_5);
						$('#reg_coleta_massa5_5').val(myRet.coleta_massa5_5);
						$('#reg_coleta_amostra_6').val(myRet.coleta_amostra_6);
						$('#reg_coleta_data_6').val(myRet.coleta_data_6).datepicker('update');
						$('#reg_coleta_num_serial_6').val(myRet.coleta_num_serial_6);
						$('#reg_coleta_num_amostrador_6').val(myRet.coleta_num_amostrador_6);
						$('#reg_coleta_num_relat_ensaio_6').val(myRet.coleta_num_relat_ensaio_6);
						$('#reg_coleta_vazao_6').val(myRet.coleta_vazao_6);
						$('#reg_coleta_tempo_amostragem_6').val(myRet.coleta_tempo_amostragem_6);
						$('#reg_coleta_massa1_6').val(myRet.coleta_massa1_6);
						$('#reg_coleta_massa2_6').val(myRet.coleta_massa2_6);
						$('#reg_coleta_massa3_6').val(myRet.coleta_massa3_6);
						$('#reg_coleta_massa4_6').val(myRet.coleta_massa4_6);
						$('#reg_coleta_massa5_6').val(myRet.coleta_massa5_6);
						$('#reg_respirador').val(myRet.respirador);
						$('#reg_cert_aprov').val(myRet.cert_aprov);
						$('#reg_resp_campo_idcolaborador').val(myRet.resp_campo_idcolaborador);
						$('#reg_resp_tecnico_idcolaborador').val(myRet.resp_tecnico_idcolaborador);
						$('#reg_registro_rc').val(myRet.registro_rc);
						$('#reg_registro_rt').val(myRet.registro_rt);
						if(myRet.img_ativ_filename){ $('#reg_img_ativ_filename_file_info').val(myRet.img_ativ_filename); }
						if(myRet.logo_filename){ $('#reg_logo_filename_file_info').val(myRet.logo_filename); }
						hideLoadingBox();
						$('#cad-modal').modal('show');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	$('#bt_save').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_id = $('#reg_id').val();
		var rg_idcliente = $('#reg_idcliente').val();
		var rg_ano = $('#reg_ano').val();
		var rg_mes = $('#reg_mes').val();
		var rg_planilha_num = $('#reg_planilha_num').val();
		var rg_unidade_site = $('#reg_unidade_site').val();
		var rg_data_elaboracao = $('#reg_data_elaboracao').val();
		var rg_area = $('#reg_area').val();
		var rg_setor = $('#reg_setor').val();
		var rg_ges = $('#reg_ges').val();
		var rg_cargo_funcao = $('#reg_cargo_funcao').val();
		var rg_cbo = $('#reg_cbo').val();
		var rg_ativ_macro = $('#reg_ativ_macro').val();
		var rg_analises = $('#reg_analises').val();
		var rg_analise_amostra_1 = $('#reg_analise_amostra_1').val();
		var rg_analise_data_amostragem_1 = $('#reg_analise_data_amostragem_1').val();
		var rg_analise_tarefa_exec_1 = $('#reg_analise_tarefa_exec_1').val();
		var rg_analise_proc_prod_1 = $('#reg_analise_proc_prod_1').val();
		var rg_analise_obs_tarefa_1 = $('#reg_analise_obs_tarefa_1').val();
		var rg_analise_amostra_2 = $('#reg_analise_amostra_2').val();
		var rg_analise_data_amostragem_2 = $('#reg_analise_data_amostragem_2').val();
		var rg_analise_tarefa_exec_2 = $('#reg_analise_tarefa_exec_2').val();
		var rg_analise_proc_prod_2 = $('#reg_analise_proc_prod_2').val();
		var rg_analise_obs_tarefa_2 = $('#reg_analise_obs_tarefa_2').val();
		var rg_analise_amostra_3 = $('#reg_analise_amostra_3').val();
		var rg_analise_data_amostragem_3 = $('#reg_analise_data_amostragem_3').val();
		var rg_analise_tarefa_exec_3 = $('#reg_analise_tarefa_exec_3').val();
		var rg_analise_proc_prod_3 = $('#reg_analise_proc_prod_3').val();
		var rg_analise_obs_tarefa_3 = $('#reg_analise_obs_tarefa_3').val();
		var rg_analise_amostra_4 = $('#reg_analise_amostra_4').val();
		var rg_analise_data_amostragem_4 = $('#reg_analise_data_amostragem_4').val();
		var rg_analise_tarefa_exec_4 = $('#reg_analise_tarefa_exec_4').val();
		var rg_analise_proc_prod_4 = $('#reg_analise_proc_prod_4').val();
		var rg_analise_obs_tarefa_4 = $('#reg_analise_obs_tarefa_4').val();
		var rg_analise_amostra_5 = $('#reg_analise_amostra_5').val();
		var rg_analise_data_amostragem_5 = $('#reg_analise_data_amostragem_5').val();
		var rg_analise_tarefa_exec_5 = $('#reg_analise_tarefa_exec_5').val();
		var rg_analise_proc_prod_5 = $('#reg_analise_proc_prod_5').val();
		var rg_analise_obs_tarefa_5 = $('#reg_analise_obs_tarefa_5').val();
		var rg_analise_amostra_6 = $('#reg_analise_amostra_6').val();
		var rg_analise_data_amostragem_6 = $('#reg_analise_data_amostragem_6').val();
		var rg_analise_tarefa_exec_6 = $('#reg_analise_tarefa_exec_6').val();
		var rg_analise_proc_prod_6 = $('#reg_analise_proc_prod_6').val();
		var rg_analise_obs_tarefa_6 = $('#reg_analise_obs_tarefa_6').val();
		var rg_jor_trab = $('#reg_jor_trab').val();
		var rg_tempo_expo = $('#reg_tempo_expo').val();
		var rg_tipo_expo = $('#reg_tipo_expo').val();
		var rg_meio_propag = $('#reg_meio_propag').val();
		var rg_fonte_geradora = $('#reg_fonte_geradora').val();
		var rg_mitigacao = $('#reg_mitigacao').val();
		var rg_coletas = $('#reg_coletas').val();
		var rg_coleta_amostra_1 = $('#reg_coleta_amostra_1').val();
		var rg_coleta_data_1 = $('#reg_coleta_data_1').val();
		var rg_coleta_num_serial_1 = $('#reg_coleta_num_serial_1').val();
		var rg_coleta_num_amostrador_1 = $('#reg_coleta_num_amostrador_1').val();
		var rg_coleta_num_relat_ensaio_1 = $('#reg_coleta_num_relat_ensaio_1').val();
		var rg_coleta_vazao_1 = $('#reg_coleta_vazao_1').val(); var rg_coleta_vazao_1 = rg_coleta_vazao_1.replace(/,+/gi, "."); var rg_coleta_vazao_1 = sprintf("%0.2f",rg_coleta_vazao_1);
		var rg_coleta_tempo_amostragem_1 = $('#reg_coleta_tempo_amostragem_1').val();
		var rg_coleta_massa1_1 = $('#reg_coleta_massa1_1').val(); var rg_coleta_massa1_1 = rg_coleta_massa1_1.replace(/,+/gi, "."); var rg_coleta_massa1_1 = sprintf("%0.2f",rg_coleta_massa1_1);
		var rg_coleta_massa2_1 = $('#reg_coleta_massa2_1').val(); var rg_coleta_massa2_1 = rg_coleta_massa2_1.replace(/,+/gi, "."); var rg_coleta_massa2_1 = sprintf("%0.2f",rg_coleta_massa2_1);
		var rg_coleta_massa3_1 = $('#reg_coleta_massa3_1').val(); var rg_coleta_massa3_1 = rg_coleta_massa3_1.replace(/,+/gi, "."); var rg_coleta_massa3_1 = sprintf("%0.2f",rg_coleta_massa3_1);
		var rg_coleta_massa4_1 = $('#reg_coleta_massa4_1').val(); var rg_coleta_massa4_1 = rg_coleta_massa4_1.replace(/,+/gi, "."); var rg_coleta_massa4_1 = sprintf("%0.2f",rg_coleta_massa4_1);
		var rg_coleta_massa5_1 = $('#reg_coleta_massa5_1').val(); var rg_coleta_massa5_1 = rg_coleta_massa5_1.replace(/,+/gi, "."); var rg_coleta_massa5_1 = sprintf("%0.2f",rg_coleta_massa5_1);
		var rg_coleta_amostra_2 = $('#reg_coleta_amostra_2').val();
		var rg_coleta_data_2 = $('#reg_coleta_data_2').val();
		var rg_coleta_num_serial_2 = $('#reg_coleta_num_serial_2').val();
		var rg_coleta_num_amostrador_2 = $('#reg_coleta_num_amostrador_2').val();
		var rg_coleta_num_relat_ensaio_2 = $('#reg_coleta_num_relat_ensaio_2').val();
		var rg_coleta_vazao_2 = $('#reg_coleta_vazao_2').val(); var rg_coleta_vazao_2 = rg_coleta_vazao_2.replace(/,+/gi, "."); var rg_coleta_vazao_2 = sprintf("%0.2f",rg_coleta_vazao_2);
		var rg_coleta_tempo_amostragem_2 = $('#reg_coleta_tempo_amostragem_2').val();
		var rg_coleta_massa1_2 = $('#reg_coleta_massa1_2').val(); var rg_coleta_massa1_2 = rg_coleta_massa1_2.replace(/,+/gi, "."); var rg_coleta_massa1_2 = sprintf("%0.2f",rg_coleta_massa1_2);
		var rg_coleta_massa2_2 = $('#reg_coleta_massa2_2').val(); var rg_coleta_massa2_2 = rg_coleta_massa2_2.replace(/,+/gi, "."); var rg_coleta_massa2_2 = sprintf("%0.2f",rg_coleta_massa2_2);
		var rg_coleta_massa3_2 = $('#reg_coleta_massa3_2').val(); var rg_coleta_massa3_2 = rg_coleta_massa3_2.replace(/,+/gi, "."); var rg_coleta_massa3_2 = sprintf("%0.2f",rg_coleta_massa3_2);
		var rg_coleta_massa4_2 = $('#reg_coleta_massa4_2').val(); var rg_coleta_massa4_2 = rg_coleta_massa4_2.replace(/,+/gi, "."); var rg_coleta_massa4_2 = sprintf("%0.2f",rg_coleta_massa4_2);
		var rg_coleta_massa5_2 = $('#reg_coleta_massa5_2').val(); var rg_coleta_massa5_2 = rg_coleta_massa5_2.replace(/,+/gi, "."); var rg_coleta_massa5_2 = sprintf("%0.2f",rg_coleta_massa5_2);
		var rg_coleta_amostra_3 = $('#reg_coleta_amostra_3').val();
		var rg_coleta_data_3 = $('#reg_coleta_data_3').val();
		var rg_coleta_num_serial_3 = $('#reg_coleta_num_serial_3').val();
		var rg_coleta_num_amostrador_3 = $('#reg_coleta_num_amostrador_3').val();
		var rg_coleta_num_relat_ensaio_3 = $('#reg_coleta_num_relat_ensaio_3').val();
		var rg_coleta_vazao_3 = $('#reg_coleta_vazao_3').val(); var rg_coleta_vazao_3 = rg_coleta_vazao_3.replace(/,+/gi, "."); var rg_coleta_vazao_3 = sprintf("%0.2f",rg_coleta_vazao_3);
		var rg_coleta_tempo_amostragem_3 = $('#reg_coleta_tempo_amostragem_3').val();
		var rg_coleta_massa1_3 = $('#reg_coleta_massa1_3').val(); var rg_coleta_massa1_3 = rg_coleta_massa1_3.replace(/,+/gi, "."); var rg_coleta_massa1_3 = sprintf("%0.2f",rg_coleta_massa1_3);
		var rg_coleta_massa2_3 = $('#reg_coleta_massa2_3').val(); var rg_coleta_massa2_3 = rg_coleta_massa2_3.replace(/,+/gi, "."); var rg_coleta_massa2_3 = sprintf("%0.2f",rg_coleta_massa2_3);
		var rg_coleta_massa3_3 = $('#reg_coleta_massa3_3').val(); var rg_coleta_massa3_3 = rg_coleta_massa3_3.replace(/,+/gi, "."); var rg_coleta_massa3_3 = sprintf("%0.2f",rg_coleta_massa3_3);
		var rg_coleta_massa4_3 = $('#reg_coleta_massa4_3').val(); var rg_coleta_massa4_3 = rg_coleta_massa4_3.replace(/,+/gi, "."); var rg_coleta_massa4_3 = sprintf("%0.2f",rg_coleta_massa4_3);
		var rg_coleta_massa5_3 = $('#reg_coleta_massa5_3').val(); var rg_coleta_massa5_3 = rg_coleta_massa5_3.replace(/,+/gi, "."); var rg_coleta_massa5_3 = sprintf("%0.2f",rg_coleta_massa5_3);
		var rg_coleta_amostra_4 = $('#reg_coleta_amostra_4').val();
		var rg_coleta_data_4 = $('#reg_coleta_data_4').val();
		var rg_coleta_num_serial_4 = $('#reg_coleta_num_serial_4').val();
		var rg_coleta_num_amostrador_4 = $('#reg_coleta_num_amostrador_4').val();
		var rg_coleta_num_relat_ensaio_4 = $('#reg_coleta_num_relat_ensaio_4').val();
		var rg_coleta_vazao_4 = $('#reg_coleta_vazao_4').val(); var rg_coleta_vazao_4 = rg_coleta_vazao_4.replace(/,+/gi, "."); var rg_coleta_vazao_4 = sprintf("%0.2f",rg_coleta_vazao_4);
		var rg_coleta_tempo_amostragem_4 = $('#reg_coleta_tempo_amostragem_4').val();
		var rg_coleta_massa1_4 = $('#reg_coleta_massa1_4').val(); var rg_coleta_massa1_4 = rg_coleta_massa1_4.replace(/,+/gi, "."); var rg_coleta_massa1_4 = sprintf("%0.2f",rg_coleta_massa1_4);
		var rg_coleta_massa2_4 = $('#reg_coleta_massa2_4').val(); var rg_coleta_massa2_4 = rg_coleta_massa2_4.replace(/,+/gi, "."); var rg_coleta_massa2_4 = sprintf("%0.2f",rg_coleta_massa2_4);
		var rg_coleta_massa3_4 = $('#reg_coleta_massa3_4').val(); var rg_coleta_massa3_4 = rg_coleta_massa3_4.replace(/,+/gi, "."); var rg_coleta_massa3_4 = sprintf("%0.2f",rg_coleta_massa3_4);
		var rg_coleta_massa4_4 = $('#reg_coleta_massa4_4').val(); var rg_coleta_massa4_4 = rg_coleta_massa4_4.replace(/,+/gi, "."); var rg_coleta_massa4_4 = sprintf("%0.2f",rg_coleta_massa4_4);
		var rg_coleta_massa5_4 = $('#reg_coleta_massa5_4').val(); var rg_coleta_massa5_4 = rg_coleta_massa5_4.replace(/,+/gi, "."); var rg_coleta_massa5_4 = sprintf("%0.2f",rg_coleta_massa5_4);
		var rg_coleta_amostra_5 = $('#reg_coleta_amostra_5').val();
		var rg_coleta_data_5 = $('#reg_coleta_data_5').val();
		var rg_coleta_num_serial_5 = $('#reg_coleta_num_serial_5').val();
		var rg_coleta_num_amostrador_5 = $('#reg_coleta_num_amostrador_5').val();
		var rg_coleta_num_relat_ensaio_5 = $('#reg_coleta_num_relat_ensaio_5').val();
		var rg_coleta_vazao_5 = $('#reg_coleta_vazao_5').val(); var rg_coleta_vazao_5 = rg_coleta_vazao_5.replace(/,+/gi, "."); var rg_coleta_vazao_5 = sprintf("%0.2f",rg_coleta_vazao_5);
		var rg_coleta_tempo_amostragem_5 = $('#reg_coleta_tempo_amostragem_5').val();
		var rg_coleta_massa1_5 = $('#reg_coleta_massa1_5').val(); var rg_coleta_massa1_5 = rg_coleta_massa1_5.replace(/,+/gi, "."); var rg_coleta_massa1_5 = sprintf("%0.2f",rg_coleta_massa1_5);
		var rg_coleta_massa2_5 = $('#reg_coleta_massa2_5').val(); var rg_coleta_massa2_5 = rg_coleta_massa2_5.replace(/,+/gi, "."); var rg_coleta_massa2_5 = sprintf("%0.2f",rg_coleta_massa2_5);
		var rg_coleta_massa3_5 = $('#reg_coleta_massa3_5').val(); var rg_coleta_massa3_5 = rg_coleta_massa3_5.replace(/,+/gi, "."); var rg_coleta_massa3_5 = sprintf("%0.2f",rg_coleta_massa3_5);
		var rg_coleta_massa4_5 = $('#reg_coleta_massa4_5').val(); var rg_coleta_massa4_5 = rg_coleta_massa4_5.replace(/,+/gi, "."); var rg_coleta_massa4_5 = sprintf("%0.2f",rg_coleta_massa4_5);
		var rg_coleta_massa5_5 = $('#reg_coleta_massa5_5').val(); var rg_coleta_massa5_5 = rg_coleta_massa5_5.replace(/,+/gi, "."); var rg_coleta_massa5_5 = sprintf("%0.2f",rg_coleta_massa5_5);
		var rg_coleta_amostra_6 = $('#reg_coleta_amostra_6').val();
		var rg_coleta_data_6 = $('#reg_coleta_data_6').val();
		var rg_coleta_num_serial_6 = $('#reg_coleta_num_serial_6').val();
		var rg_coleta_num_amostrador_6 = $('#reg_coleta_num_amostrador_6').val();
		var rg_coleta_num_relat_ensaio_6 = $('#reg_coleta_num_relat_ensaio_6').val();
		var rg_coleta_vazao_6 = $('#reg_coleta_vazao_6').val(); var rg_coleta_vazao_6 = rg_coleta_vazao_6.replace(/,+/gi, "."); var rg_coleta_vazao_6 = sprintf("%0.2f",rg_coleta_vazao_6);
		var rg_coleta_tempo_amostragem_6 = $('#reg_coleta_tempo_amostragem_6').val();
		var rg_coleta_massa1_6 = $('#reg_coleta_massa1_6').val(); var rg_coleta_massa1_6 = rg_coleta_massa1_6.replace(/,+/gi, "."); var rg_coleta_massa1_6 = sprintf("%0.2f",rg_coleta_massa1_6);
		var rg_coleta_massa2_6 = $('#reg_coleta_massa2_6').val(); var rg_coleta_massa2_6 = rg_coleta_massa2_6.replace(/,+/gi, "."); var rg_coleta_massa2_6 = sprintf("%0.2f",rg_coleta_massa2_6);
		var rg_coleta_massa3_6 = $('#reg_coleta_massa3_6').val(); var rg_coleta_massa3_6 = rg_coleta_massa3_6.replace(/,+/gi, "."); var rg_coleta_massa3_6 = sprintf("%0.2f",rg_coleta_massa3_6);
		var rg_coleta_massa4_6 = $('#reg_coleta_massa4_6').val(); var rg_coleta_massa4_6 = rg_coleta_massa4_6.replace(/,+/gi, "."); var rg_coleta_massa4_6 = sprintf("%0.2f",rg_coleta_massa4_6);
		var rg_coleta_massa5_6 = $('#reg_coleta_massa5_6').val(); var rg_coleta_massa5_6 = rg_coleta_massa5_6.replace(/,+/gi, "."); var rg_coleta_massa5_6 = sprintf("%0.2f",rg_coleta_massa5_6);
		var rg_respirador = $('#reg_respirador').val();
		var rg_cert_aprov = $('#reg_cert_aprov').val();
		var rg_resp_campo_idcolaborador = $('#reg_resp_campo_idcolaborador').val();
		var rg_resp_tecnico_idcolaborador = $('#reg_resp_tecnico_idcolaborador').val();
		var rg_registro_rc = $('#reg_registro_rc').val();
		var rg_registro_rt = $('#reg_registro_rt').val();
		var rg_img_ativ_filename_file_info = $('#reg_img_ativ_filename_file_info').val();
		var rg_img_ativ_filename_filename_ext = rg_img_ativ_filename_file_info.split('.').pop().toLowerCase();
		var rg_img_ativ_filename_file_upload = document.getElementById('reg_img_ativ_filename_file').files[0];
		var rg_img_ativ_filename_max_file_size = $('#reg_img_ativ_filename_max_file_size').val();
		var rg_logo_filename_file_info = $('#reg_logo_filename_file_info').val();
		var rg_logo_filename_filename_ext = rg_logo_filename_file_info.split('.').pop().toLowerCase();
		var rg_logo_filename_file_upload = document.getElementById('reg_logo_filename_file').files[0];
		var rg_logo_filename_max_file_size = $('#reg_logo_filename_max_file_size').val();
		
		//ID
		if (isEmpty(rg_id)) { show_msgbox('Incapaz de determinar qual registro deve ser editado!',' ','error'); return false; }
		//CLIENTE
		if (isEmpty(rg_idcliente)) { show_msgbox('O campo CLIENTE é obrigatório!',' ','alert'); $('#reg_idcliente').focus(); return false; }
		//ANO
		if (isEmpty(rg_ano)) { show_msgbox('O campo ANO é obrigatório!',' ','alert'); $('#reg_ano').focus(); return false; }
		if(!isEmpty(rg_ano) && (rg_ano.length != 4)){ show_msgbox('ANO deve ter 4 dígitos!',' ','alert'); $('#reg_ano').focus(); return false; }
		//MES
		if (isEmpty(rg_mes)) { show_msgbox('O campo MÊS é obrigatório!',' ','alert'); $('#reg_mes').focus(); return false; }
		//NUMERO PLANILHA
		if (isEmpty(rg_planilha_num)) { show_msgbox('O campo NÚMERO PLANILHA é obrigatório!',' ','alert'); $('#reg_planilha_num').focus(); return false; }
		if (!isEmpty(rg_planilha_num) && rg_planilha_num.length > 2) { show_msgbox('NÚMERO PLANILHA deve ter até 2 dígitos!',' ','alert'); $('#reg_planilha_num').focus(); return false; }
		//UNIDADE/SITE
		//if (isEmpty(rg_unidade_site)) { show_msgbox('O campo UNIDADE/SITE é obrigatório!',' ','alert'); $('#reg_unidade_site').focus(); return false; }
		if (!isEmpty(rg_unidade_site) && rg_unidade_site.length > 100) { show_msgbox('UNIDADE/SITE deve ter até 100 dígitos!',' ','alert'); $('#reg_unidade_site').focus(); return false; }
		//DATA ELABORACAO
		if (!isEmpty(rg_data_elaboracao) && (rg_data_elaboracao.length != 10)){ show_msgbox('DATA ELABORAÇÃO deve ter 10 dígitos!',' ','alert'); $('#reg_data_elaboracao').focus(); return false; }
		if (!isEmpty(rg_data_elaboracao)){ if(!isDate(rg_data_elaboracao)){ show_msgbox('DATA ELABORAÇÃO - Informe uma data válida!',' ','alert'); $('#reg_data_elaboracao').focus(); return false; } else { rg_data_elaboracao = getDataMySQL(rg_data_elaboracao); } }
		//AREA
		if (!isEmpty(rg_area) && rg_area.length > 30) { show_msgbox('ÁREA deve ter até 30 dígitos!',' ','alert'); $('#reg_area').focus(); return false; }
		//SETOR
		if (!isEmpty(rg_setor) && rg_setor.length > 20) { show_msgbox('SETOR deve ter até 20 dígitos!',' ','alert'); $('#reg_setor').focus(); return false; }
		//GES
		if (!isEmpty(rg_ges) && rg_ges.length > 7) { show_msgbox('GES deve ter até 7 dígitos!',' ','alert'); $('#reg_ges').focus(); return false; }
		//CARGO/FUNCAO
		if (!isEmpty(rg_cargo_funcao) && rg_cargo_funcao.length > 40) { show_msgbox('CARGO/FUNÇÃO deve ter até 40 dígitos!',' ','alert'); $('#reg_cargo_funcao').focus(); return false; }
		//CBO
		if (!isEmpty(rg_cbo) && rg_cbo.length > 7) { show_msgbox('CBO deve ter até 7 dígitos!',' ','alert'); $('#reg_cbo').focus(); return false; }
		//ATIVIDADE MACRO
		if (!isEmpty(rg_ativ_macro) && rg_ativ_macro.length > 2000) { show_msgbox('ATIVIDADE MACRO deve ter até 2000 dígitos!',' ','alert'); $('#reg_ativ_macro').focus(); return false; }
		//ANALISES
		if (isEmpty(rg_analises)) { show_msgbox('O campo ANÁLISES é obrigatório!',' ','alert'); $('#reg_analises').focus(); return false; }
		//ANALISE #1 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_1) && rg_analise_amostra_1.length > 2) { show_msgbox('ANÁLISE #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_1').focus(); return false; }
		//ANALISE #1 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_1) && (rg_analise_data_amostragem_1.length != 10)){ show_msgbox('ANÁLISE #1 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_1').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_1)){ if(!isDate(rg_analise_data_amostragem_1)){ show_msgbox('ANÁLISE #1 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_1').focus(); return false; } else { rg_analise_data_amostragem_1 = getDataMySQL(rg_analise_data_amostragem_1); } }
		//ANALISE #1 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_1) && rg_analise_tarefa_exec_1.length > 2000) { show_msgbox('ANÁLISE #1 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_1').focus(); return false; }
		//ANALISE #1 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_1) && rg_analise_proc_prod_1.length > 2000) { show_msgbox('ANÁLISE #1 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_1').focus(); return false; }
		//ANALISE #1 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_1) && rg_analise_obs_tarefa_1.length > 2000) { show_msgbox('ANÁLISE #1 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_1').focus(); return false; }
		//ANALISE #2 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_2) && rg_analise_amostra_2.length > 2) { show_msgbox('ANÁLISE #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_2').focus(); return false; }
		//ANALISE #2 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_2) && (rg_analise_data_amostragem_2.length != 10)){ show_msgbox('ANÁLISE #2 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_2').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_2)){ if(!isDate(rg_analise_data_amostragem_2)){ show_msgbox('ANÁLISE #2 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_2').focus(); return false; } else { rg_analise_data_amostragem_2 = getDataMySQL(rg_analise_data_amostragem_2); } }
		//ANALISE #2 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_2) && rg_analise_tarefa_exec_2.length > 2000) { show_msgbox('ANÁLISE #2 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_2').focus(); return false; }
		//ANALISE #2 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_2) && rg_analise_proc_prod_2.length > 2000) { show_msgbox('ANÁLISE #2 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_2').focus(); return false; }
		//ANALISE #2 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_2) && rg_analise_obs_tarefa_2.length > 2000) { show_msgbox('ANÁLISE #2 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_2').focus(); return false; }
		//ANALISE #3 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_3) && rg_analise_amostra_3.length > 2) { show_msgbox('ANÁLISE #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_3').focus(); return false; }
		//ANALISE #3 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_3) && (rg_analise_data_amostragem_3.length != 10)){ show_msgbox('ANÁLISE #3 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_3').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_3)){ if(!isDate(rg_analise_data_amostragem_3)){ show_msgbox('ANÁLISE #3 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_3').focus(); return false; } else { rg_analise_data_amostragem_3 = getDataMySQL(rg_analise_data_amostragem_3); } }
		//ANALISE #3 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_3) && rg_analise_tarefa_exec_3.length > 2000) { show_msgbox('ANÁLISE #3 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_3').focus(); return false; }
		//ANALISE #3 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_3) && rg_analise_proc_prod_3.length > 2000) { show_msgbox('ANÁLISE #3 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_3').focus(); return false; }
		//ANALISE #3 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_3) && rg_analise_obs_tarefa_3.length > 2000) { show_msgbox('ANÁLISE #3 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_3').focus(); return false; }
		//ANALISE #4 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_4) && rg_analise_amostra_4.length > 2) { show_msgbox('ANÁLISE #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_4').focus(); return false; }
		//ANALISE #4 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_4) && (rg_analise_data_amostragem_4.length != 10)){ show_msgbox('ANÁLISE #4 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_4').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_4)){ if(!isDate(rg_analise_data_amostragem_4)){ show_msgbox('ANÁLISE #4 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_4').focus(); return false; } else { rg_analise_data_amostragem_4 = getDataMySQL(rg_analise_data_amostragem_4); } }
		//ANALISE #4 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_4) && rg_analise_tarefa_exec_4.length > 2000) { show_msgbox('ANÁLISE #4 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_4').focus(); return false; }
		//ANALISE #4 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_4) && rg_analise_proc_prod_4.length > 2000) { show_msgbox('ANÁLISE #4 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_4').focus(); return false; }
		//ANALISE #4 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_4) && rg_analise_obs_tarefa_4.length > 2000) { show_msgbox('ANÁLISE #4 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_4').focus(); return false; }
		//ANALISE #5 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_5) && rg_analise_amostra_5.length > 2) { show_msgbox('ANÁLISE #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_5').focus(); return false; }
		//ANALISE #5 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_5) && (rg_analise_data_amostragem_5.length != 10)){ show_msgbox('ANÁLISE #5 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_5').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_5)){ if(!isDate(rg_analise_data_amostragem_5)){ show_msgbox('ANÁLISE #5 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_5').focus(); return false; } else { rg_analise_data_amostragem_5 = getDataMySQL(rg_analise_data_amostragem_5); } }
		//ANALISE #5 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_5) && rg_analise_tarefa_exec_5.length > 2000) { show_msgbox('ANÁLISE #5 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_5').focus(); return false; }
		//ANALISE #5 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_5) && rg_analise_proc_prod_5.length > 2000) { show_msgbox('ANÁLISE #5 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_5').focus(); return false; }
		//ANALISE #5 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_5) && rg_analise_obs_tarefa_5.length > 2000) { show_msgbox('ANÁLISE #5 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_5').focus(); return false; }
		//ANALISE #6 - AMOSTRA
		if (!isEmpty(rg_analise_amostra_6) && rg_analise_amostra_6.length > 2) { show_msgbox('ANÁLISE #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_analise_amostra_6').focus(); return false; }
		//ANALISE #6 - DATA AMOSTRAGEM
		if (!isEmpty(rg_analise_data_amostragem_6) && (rg_analise_data_amostragem_6.length != 10)){ show_msgbox('ANÁLISE #6 - DATA AMOSTRAGEM deve ter 10 dígitos!',' ','alert'); $('#reg_analise_data_amostragem_6').focus(); return false; }
		if (!isEmpty(rg_analise_data_amostragem_6)){ if(!isDate(rg_analise_data_amostragem_6)){ show_msgbox('ANÁLISE #6 - DATA AMOSTRAGEM - Informe uma data válida!',' ','alert'); $('#reg_analise_data_amostragem_6').focus(); return false; } else { rg_analise_data_amostragem_6 = getDataMySQL(rg_analise_data_amostragem_6); } }
		//ANALISE #6 - TAREFA EXECUTADA
		if (!isEmpty(rg_analise_tarefa_exec_6) && rg_analise_tarefa_exec_6.length > 2000) { show_msgbox('ANÁLISE #6 - TAREFA EXECUTADA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_tarefa_exec_6').focus(); return false; }
		//ANALISE #6 - PROCESSO PRODUTIVO
		if (!isEmpty(rg_analise_proc_prod_6) && rg_analise_proc_prod_6.length > 2000) { show_msgbox('ANÁLISE #6 - PROCESSO PRODUTIVO deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_proc_prod_6').focus(); return false; }
		//ANALISE #6 - OBS TAREFA
		if (!isEmpty(rg_analise_obs_tarefa_6) && rg_analise_obs_tarefa_6.length > 2000) { show_msgbox('ANÁLISE #6 - OBS TAREFA deve ter até 2000 dígitos!',' ','alert'); $('#reg_analise_obs_tarefa_6').focus(); return false; }
		//JORNADA DE TRABALHO
		if (!isEmpty(rg_jor_trab) && rg_jor_trab.length > 5) { show_msgbox('JORNADA DE TRABALHO deve ter até 5 dígitos!',' ','alert'); $('#reg_jor_trab').focus(); return false; }
		//TEMPO EXPOSICAO
		if (!isEmpty(rg_tempo_expo) && rg_tempo_expo.length > 3) { show_msgbox('TEMPO EXPOSIÇÃO deve ter até 3 dígitos!',' ','alert'); $('#reg_tempo_expo').focus(); return false; }
		//TIPO EXPOSICAO
		if (!isEmpty(rg_tipo_expo) && rg_tipo_expo.length > 2) { show_msgbox('TIPO EXPOSIÇÃO deve ter até 2 dígitos!',' ','alert'); $('#reg_tipo_expo').focus(); return false; }
		//MEIO DE PROPAGACAO
		if (!isEmpty(rg_meio_propag) && rg_meio_propag.length > 12) { show_msgbox('MEIO DE PROPAGAÇÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_meio_propag').focus(); return false; }
		//FONTE GERADORA
		if (!isEmpty(rg_fonte_geradora) && rg_fonte_geradora.length > 30) { show_msgbox('FONTE GERADORA deve ter até 30 dígitos!',' ','alert'); $('#reg_fonte_geradora').focus(); return false; }
		//MITIGACAO
		if (!isEmpty(rg_mitigacao) && rg_mitigacao.length > 30) { show_msgbox('MITIGAÇÃO deve ter até 30 dígitos!',' ','alert'); $('#reg_mitigacao').focus(); return false; }
		//COLETAS
		if (isEmpty(rg_coletas)) { show_msgbox('O campo COLETAS é obrigatório!',' ','alert'); $('#reg_coletas').focus(); return false; }
		//COLETA #1 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_1) && rg_coleta_amostra_1.length > 2) { show_msgbox('COLETA #1 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_1').focus(); return false; }
		//COLETA #1 - DATA
		if (!isEmpty(rg_coleta_data_1) && (rg_coleta_data_1.length != 10)){ show_msgbox('COLETA #1 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_1').focus(); return false; }
		if (!isEmpty(rg_coleta_data_1)){ if(!isDate(rg_coleta_data_1)){ show_msgbox('COLETA #1 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_1').focus(); return false; } else { rg_coleta_data_1 = getDataMySQL(rg_coleta_data_1); } }
		//COLETA #1 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_1) && rg_coleta_num_serial_1.length > 6) { show_msgbox('COLETA #1 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_1').focus(); return false; }
		//COLETA #1 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_1) && rg_coleta_num_amostrador_1.length > 10) { show_msgbox('COLETA #1 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_1').focus(); return false; }
		//COLETA #1 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_1) && rg_coleta_num_relat_ensaio_1.length > 3) { show_msgbox('COLETA #1 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_1').focus(); return false; }
		//COLETA #1 - VAZAO
		if (!isEmpty(rg_coleta_vazao_1) && rg_coleta_vazao_1.length > 12) { show_msgbox('COLETA #1 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_1').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_1) && !isDecimal2Regex.test(rg_coleta_vazao_1)) { show_msgbox('COLETA #1 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_1').focus(); return false; }
		//COLETA #1 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_1) && rg_coleta_tempo_amostragem_1.length > 3) { show_msgbox('COLETA #1 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_1').focus(); return false; }
		//COLETA #1 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_1) && rg_coleta_massa1_1.length > 12) { show_msgbox('COLETA #1 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_1) && !isDecimal2Regex.test(rg_coleta_massa1_1)) { show_msgbox('COLETA #1 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_1').focus(); return false; }
		//COLETA #1 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_1) && rg_coleta_massa2_1.length > 12) { show_msgbox('COLETA #1 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_1) && !isDecimal2Regex.test(rg_coleta_massa2_1)) { show_msgbox('COLETA #1 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_1').focus(); return false; }
		//COLETA #1 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_1) && rg_coleta_massa3_1.length > 12) { show_msgbox('COLETA #1 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_1) && !isDecimal2Regex.test(rg_coleta_massa3_1)) { show_msgbox('COLETA #1 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_1').focus(); return false; }
		//COLETA #1 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_1) && rg_coleta_massa4_1.length > 12) { show_msgbox('COLETA #1 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_1) && !isDecimal2Regex.test(rg_coleta_massa4_1)) { show_msgbox('COLETA #1 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_1').focus(); return false; }
		//COLETA #1 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_1) && rg_coleta_massa5_1.length > 12) { show_msgbox('COLETA #1 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_1').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_1) && !isDecimal2Regex.test(rg_coleta_massa5_1)) { show_msgbox('COLETA #1 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_1').focus(); return false; }
		//COLETA #2 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_2) && rg_coleta_amostra_2.length > 2) { show_msgbox('COLETA #2 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_2').focus(); return false; }
		//COLETA #2 - DATA
		if (!isEmpty(rg_coleta_data_2) && (rg_coleta_data_2.length != 10)){ show_msgbox('COLETA #2 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_2').focus(); return false; }
		if (!isEmpty(rg_coleta_data_2)){ if(!isDate(rg_coleta_data_2)){ show_msgbox('COLETA #2 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_2').focus(); return false; } else { rg_coleta_data_2 = getDataMySQL(rg_coleta_data_2); } }
		//COLETA #2 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_2) && rg_coleta_num_serial_2.length > 6) { show_msgbox('COLETA #2 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_2').focus(); return false; }
		//COLETA #2 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_2) && rg_coleta_num_amostrador_2.length > 10) { show_msgbox('COLETA #2 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_2').focus(); return false; }
		//COLETA #2 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_2) && rg_coleta_num_relat_ensaio_2.length > 3) { show_msgbox('COLETA #2 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_2').focus(); return false; }
		//COLETA #2 - VAZAO
		if (!isEmpty(rg_coleta_vazao_2) && rg_coleta_vazao_2.length > 12) { show_msgbox('COLETA #2 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_2').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_2) && !isDecimal2Regex.test(rg_coleta_vazao_2)) { show_msgbox('COLETA #2 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_2').focus(); return false; }
		//COLETA #2 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_2) && rg_coleta_tempo_amostragem_2.length > 3) { show_msgbox('COLETA #2 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_2').focus(); return false; }
		//COLETA #2 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_2) && rg_coleta_massa1_2.length > 12) { show_msgbox('COLETA #2 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_2) && !isDecimal2Regex.test(rg_coleta_massa1_2)) { show_msgbox('COLETA #2 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_2').focus(); return false; }
		//COLETA #2 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_2) && rg_coleta_massa2_2.length > 12) { show_msgbox('COLETA #2 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_2) && !isDecimal2Regex.test(rg_coleta_massa2_2)) { show_msgbox('COLETA #2 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_2').focus(); return false; }
		//COLETA #2 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_2) && rg_coleta_massa3_2.length > 12) { show_msgbox('COLETA #2 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_2) && !isDecimal2Regex.test(rg_coleta_massa3_2)) { show_msgbox('COLETA #2 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_2').focus(); return false; }
		//COLETA #2 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_2) && rg_coleta_massa4_2.length > 12) { show_msgbox('COLETA #2 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_2) && !isDecimal2Regex.test(rg_coleta_massa4_2)) { show_msgbox('COLETA #2 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_2').focus(); return false; }
		//COLETA #2 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_2) && rg_coleta_massa5_2.length > 12) { show_msgbox('COLETA #2 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_2').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_2) && !isDecimal2Regex.test(rg_coleta_massa5_2)) { show_msgbox('COLETA #2 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_2').focus(); return false; }
		//COLETA #3 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_3) && rg_coleta_amostra_3.length > 2) { show_msgbox('COLETA #3 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_3').focus(); return false; }
		//COLETA #3 - DATA
		if (!isEmpty(rg_coleta_data_3) && (rg_coleta_data_3.length != 10)){ show_msgbox('COLETA #3 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_3').focus(); return false; }
		if (!isEmpty(rg_coleta_data_3)){ if(!isDate(rg_coleta_data_3)){ show_msgbox('COLETA #3 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_3').focus(); return false; } else { rg_coleta_data_3 = getDataMySQL(rg_coleta_data_3); } }
		//COLETA #3 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_3) && rg_coleta_num_serial_3.length > 6) { show_msgbox('COLETA #3 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_3').focus(); return false; }
		//COLETA #3 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_3) && rg_coleta_num_amostrador_3.length > 10) { show_msgbox('COLETA #3 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_3').focus(); return false; }
		//COLETA #3 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_3) && rg_coleta_num_relat_ensaio_3.length > 3) { show_msgbox('COLETA #3 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_3').focus(); return false; }
		//COLETA #3 - VAZAO
		if (!isEmpty(rg_coleta_vazao_3) && rg_coleta_vazao_3.length > 12) { show_msgbox('COLETA #3 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_3').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_3) && !isDecimal2Regex.test(rg_coleta_vazao_3)) { show_msgbox('COLETA #3 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_3').focus(); return false; }
		//COLETA #3 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_3) && rg_coleta_tempo_amostragem_3.length > 3) { show_msgbox('COLETA #3 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_3').focus(); return false; }
		//COLETA #3 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_3) && rg_coleta_massa1_3.length > 12) { show_msgbox('COLETA #3 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_3) && !isDecimal2Regex.test(rg_coleta_massa1_3)) { show_msgbox('COLETA #3 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_3').focus(); return false; }
		//COLETA #3 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_3) && rg_coleta_massa2_3.length > 12) { show_msgbox('COLETA #3 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_3) && !isDecimal2Regex.test(rg_coleta_massa2_3)) { show_msgbox('COLETA #3 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_3').focus(); return false; }
		//COLETA #3 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_3) && rg_coleta_massa3_3.length > 12) { show_msgbox('COLETA #3 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_3) && !isDecimal2Regex.test(rg_coleta_massa3_3)) { show_msgbox('COLETA #3 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_3').focus(); return false; }
		//COLETA #3 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_3) && rg_coleta_massa4_3.length > 12) { show_msgbox('COLETA #3 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_3) && !isDecimal2Regex.test(rg_coleta_massa4_3)) { show_msgbox('COLETA #3 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_3').focus(); return false; }
		//COLETA #3 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_3) && rg_coleta_massa5_3.length > 12) { show_msgbox('COLETA #3 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_3').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_3) && !isDecimal2Regex.test(rg_coleta_massa5_3)) { show_msgbox('COLETA #3 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_3').focus(); return false; }
		//COLETA #4 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_4) && rg_coleta_amostra_4.length > 2) { show_msgbox('COLETA #4 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_4').focus(); return false; }
		//COLETA #4 - DATA
		if (!isEmpty(rg_coleta_data_4) && (rg_coleta_data_4.length != 10)){ show_msgbox('COLETA #4 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_4').focus(); return false; }
		if (!isEmpty(rg_coleta_data_4)){ if(!isDate(rg_coleta_data_4)){ show_msgbox('COLETA #4 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_4').focus(); return false; } else { rg_coleta_data_4 = getDataMySQL(rg_coleta_data_4); } }
		//COLETA #4 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_4) && rg_coleta_num_serial_4.length > 6) { show_msgbox('COLETA #4 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_4').focus(); return false; }
		//COLETA #4 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_4) && rg_coleta_num_amostrador_4.length > 10) { show_msgbox('COLETA #4 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_4').focus(); return false; }
		//COLETA #4 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_4) && rg_coleta_num_relat_ensaio_4.length > 3) { show_msgbox('COLETA #4 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_4').focus(); return false; }
		//COLETA #4 - VAZAO
		if (!isEmpty(rg_coleta_vazao_4) && rg_coleta_vazao_4.length > 12) { show_msgbox('COLETA #4 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_4').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_4) && !isDecimal2Regex.test(rg_coleta_vazao_4)) { show_msgbox('COLETA #4 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_4').focus(); return false; }
		//COLETA #4 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_4) && rg_coleta_tempo_amostragem_4.length > 3) { show_msgbox('COLETA #4 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_4').focus(); return false; }
		//COLETA #4 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_4) && rg_coleta_massa1_4.length > 12) { show_msgbox('COLETA #4 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_4) && !isDecimal2Regex.test(rg_coleta_massa1_4)) { show_msgbox('COLETA #4 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_4').focus(); return false; }
		//COLETA #4 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_4) && rg_coleta_massa2_4.length > 12) { show_msgbox('COLETA #4 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_4) && !isDecimal2Regex.test(rg_coleta_massa2_4)) { show_msgbox('COLETA #4 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_4').focus(); return false; }
		//COLETA #4 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_4) && rg_coleta_massa3_4.length > 12) { show_msgbox('COLETA #4 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_4) && !isDecimal2Regex.test(rg_coleta_massa3_4)) { show_msgbox('COLETA #4 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_4').focus(); return false; }
		//COLETA #4 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_4) && rg_coleta_massa4_4.length > 12) { show_msgbox('COLETA #4 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_4) && !isDecimal2Regex.test(rg_coleta_massa4_4)) { show_msgbox('COLETA #4 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_4').focus(); return false; }
		//COLETA #4 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_4) && rg_coleta_massa5_4.length > 12) { show_msgbox('COLETA #4 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_4').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_4) && !isDecimal2Regex.test(rg_coleta_massa5_4)) { show_msgbox('COLETA #4 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_4').focus(); return false; }
		//COLETA #5 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_5) && rg_coleta_amostra_5.length > 2) { show_msgbox('COLETA #5 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_5').focus(); return false; }
		//COLETA #5 - DATA
		if (!isEmpty(rg_coleta_data_5) && (rg_coleta_data_5.length != 10)){ show_msgbox('COLETA #5 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_5').focus(); return false; }
		if (!isEmpty(rg_coleta_data_5)){ if(!isDate(rg_coleta_data_5)){ show_msgbox('COLETA #5 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_5').focus(); return false; } else { rg_coleta_data_5 = getDataMySQL(rg_coleta_data_5); } }
		//COLETA #5 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_5) && rg_coleta_num_serial_5.length > 6) { show_msgbox('COLETA #5 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_5').focus(); return false; }
		//COLETA #5 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_5) && rg_coleta_num_amostrador_5.length > 10) { show_msgbox('COLETA #5 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_5').focus(); return false; }
		//COLETA #5 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_5) && rg_coleta_num_relat_ensaio_5.length > 3) { show_msgbox('COLETA #5 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_5').focus(); return false; }
		//COLETA #5 - VAZAO
		if (!isEmpty(rg_coleta_vazao_5) && rg_coleta_vazao_5.length > 12) { show_msgbox('COLETA #5 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_5').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_5) && !isDecimal2Regex.test(rg_coleta_vazao_5)) { show_msgbox('COLETA #5 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_5').focus(); return false; }
		//COLETA #5 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_5) && rg_coleta_tempo_amostragem_5.length > 3) { show_msgbox('COLETA #5 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_5').focus(); return false; }
		//COLETA #5 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_5) && rg_coleta_massa1_5.length > 12) { show_msgbox('COLETA #5 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_5) && !isDecimal2Regex.test(rg_coleta_massa1_5)) { show_msgbox('COLETA #5 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_5').focus(); return false; }
		//COLETA #5 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_5) && rg_coleta_massa2_5.length > 12) { show_msgbox('COLETA #5 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_5) && !isDecimal2Regex.test(rg_coleta_massa2_5)) { show_msgbox('COLETA #5 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_5').focus(); return false; }
		//COLETA #5 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_5) && rg_coleta_massa3_5.length > 12) { show_msgbox('COLETA #5 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_5) && !isDecimal2Regex.test(rg_coleta_massa3_5)) { show_msgbox('COLETA #5 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_5').focus(); return false; }
		//COLETA #5 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_5) && rg_coleta_massa4_5.length > 12) { show_msgbox('COLETA #5 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_5) && !isDecimal2Regex.test(rg_coleta_massa4_5)) { show_msgbox('COLETA #5 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_5').focus(); return false; }
		//COLETA #5 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_5) && rg_coleta_massa5_5.length > 12) { show_msgbox('COLETA #5 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_5').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_5) && !isDecimal2Regex.test(rg_coleta_massa5_5)) { show_msgbox('COLETA #5 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_5').focus(); return false; }
		//COLETA #6 - AMOSTRA
		if (!isEmpty(rg_coleta_amostra_6) && rg_coleta_amostra_6.length > 2) { show_msgbox('COLETA #6 - AMOSTRA deve ter até 2 dígitos!',' ','alert'); $('#reg_coleta_amostra_6').focus(); return false; }
		//COLETA #6 - DATA
		if (!isEmpty(rg_coleta_data_6) && (rg_coleta_data_6.length != 10)){ show_msgbox('COLETA #6 - DATA deve ter 10 dígitos!',' ','alert'); $('#reg_coleta_data_6').focus(); return false; }
		if (!isEmpty(rg_coleta_data_6)){ if(!isDate(rg_coleta_data_6)){ show_msgbox('COLETA #6 - DATA - Informe uma data válida!',' ','alert'); $('#reg_coleta_data_6').focus(); return false; } else { rg_coleta_data_6 = getDataMySQL(rg_coleta_data_6); } }
		//COLETA #6 - Nº SERIAL
		if (!isEmpty(rg_coleta_num_serial_6) && rg_coleta_num_serial_6.length > 6) { show_msgbox('COLETA #6 - Nº SERIAL deve ter até 6 dígitos!',' ','alert'); $('#reg_coleta_num_serial_6').focus(); return false; }
		//COLETA #6 - Nº AMOSTRADOR
		if (!isEmpty(rg_coleta_num_amostrador_6) && rg_coleta_num_amostrador_6.length > 10) { show_msgbox('COLETA #6 - Nº AMOSTRADOR deve ter até 10 dígitos!',' ','alert'); $('#reg_coleta_num_amostrador_6').focus(); return false; }
		//COLETA #6 - Nº RELATORIO DE ENSAIO
		if (!isEmpty(rg_coleta_num_relat_ensaio_6) && rg_coleta_num_relat_ensaio_6.length > 3) { show_msgbox('COLETA #6 - Nº RELATÓRIO DE ENSAIO deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_num_relat_ensaio_6').focus(); return false; }
		//COLETA #6 - VAZAO
		if (!isEmpty(rg_coleta_vazao_6) && rg_coleta_vazao_6.length > 12) { show_msgbox('COLETA #6 - VAZÃO deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_vazao_6').focus(); return false; }
		if (!isEmpty(rg_coleta_vazao_6) && !isDecimal2Regex.test(rg_coleta_vazao_6)) { show_msgbox('COLETA #6 - VAZÃO - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_vazao_6').focus(); return false; }
		//COLETA #6 - TEMPO AMOSTRAGEM
		if (!isEmpty(rg_coleta_tempo_amostragem_6) && rg_coleta_tempo_amostragem_6.length > 3) { show_msgbox('COLETA #6 - TEMPO AMOSTRAGEM deve ter até 3 dígitos!',' ','alert'); $('#reg_coleta_tempo_amostragem_6').focus(); return false; }
		//COLETA #6 - MASSA #1
		if (!isEmpty(rg_coleta_massa1_6) && rg_coleta_massa1_6.length > 12) { show_msgbox('COLETA #6 - MASSA #1 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa1_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa1_6) && !isDecimal2Regex.test(rg_coleta_massa1_6)) { show_msgbox('COLETA #6 - MASSA #1 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa1_6').focus(); return false; }
		//COLETA #6 - MASSA #2
		if (!isEmpty(rg_coleta_massa2_6) && rg_coleta_massa2_6.length > 12) { show_msgbox('COLETA #6 - MASSA #2 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa2_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa2_6) && !isDecimal2Regex.test(rg_coleta_massa2_6)) { show_msgbox('COLETA #6 - MASSA #2 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa2_6').focus(); return false; }
		//COLETA #6 - MASSA #3
		if (!isEmpty(rg_coleta_massa3_6) && rg_coleta_massa3_6.length > 12) { show_msgbox('COLETA #6 - MASSA #3 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa3_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa3_6) && !isDecimal2Regex.test(rg_coleta_massa3_6)) { show_msgbox('COLETA #6 - MASSA #3 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa3_6').focus(); return false; }
		//COLETA #6 - MASSA #4
		if (!isEmpty(rg_coleta_massa4_6) && rg_coleta_massa4_6.length > 12) { show_msgbox('COLETA #6 - MASSA #4 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa4_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa4_6) && !isDecimal2Regex.test(rg_coleta_massa4_6)) { show_msgbox('COLETA #6 - MASSA #4 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa4_6').focus(); return false; }
		//COLETA #6 - MASSA #5
		if (!isEmpty(rg_coleta_massa5_6) && rg_coleta_massa5_6.length > 12) { show_msgbox('COLETA #6 - MASSA #5 deve ter até 12 dígitos!',' ','alert'); $('#reg_coleta_massa5_6').focus(); return false; }
		if (!isEmpty(rg_coleta_massa5_6) && !isDecimal2Regex.test(rg_coleta_massa5_6)) { show_msgbox('COLETA #6 - MASSA #5 - Informe um valor com 2 casas decimais válido!',' ','alert'); $('#reg_coleta_massa5_6').focus(); return false; }
		//RESPIRADOR
		if (!isEmpty(rg_respirador) && rg_respirador.length > 20) { show_msgbox('RESPIRADOR deve ter até 20 dígitos!',' ','alert'); $('#reg_respirador').focus(); return false; }
		//CERTIFICADO DE APROVACAO
		if (!isEmpty(rg_cert_aprov) && rg_cert_aprov.length > 6) { show_msgbox('CERTIFICADO DE APROVAÇÃO deve ter até 6 dígitos!',' ','alert'); $('#reg_cert_aprov').focus(); return false; }
		//RESPONSAVEL CAMPO - REGISTRO
		if (!isEmpty(rg_registro_rc) && rg_registro_rc.length > 17) { show_msgbox('RESPONSÁVEL CAMPO - REGISTRO deve ter até 17 dígitos!',' ','alert'); $('#reg_registro_rc').focus(); return false; }
		//RESPONSAVEL TECNICO - REGISTRO
		if (!isEmpty(rg_registro_rt) && rg_registro_rt.length > 17) { show_msgbox('RESPONSÁVEL TÉCNICO - REGISTRO deve ter até 17 dígitos!',' ','alert'); $('#reg_registro_rt').focus(); return false; }
		//IMAGEM ATIVIDADE
		if(rg_img_ativ_filename_file_upload)
		{
			if ( rg_img_ativ_filename_filename_ext != 'gif' &&
			     rg_img_ativ_filename_filename_ext != 'jpg' &&
			     rg_img_ativ_filename_filename_ext != 'jpeg' &&
			     rg_img_ativ_filename_filename_ext != 'png' &&
			     rg_img_ativ_filename_file_upload.type != "image/gif" &&
			     rg_img_ativ_filename_file_upload.type != "image/jpeg" &&
			     rg_img_ativ_filename_file_upload.type != "image/jpeg" &&
			     rg_img_ativ_filename_file_upload.type != "image/png"){ show_msgbox('IMAGEM ATIVIDADE - Arquivo inválido!',' ','alert'); return false; }
			
			if ( rg_img_ativ_filename_file_upload.size > 2000000 ){ show_msgbox('Tamanho máximo permitido para o arquivo é de 2000000 bytes!',' ','alert'); return false; }
		}
		//IMAGEM LOGOMARCA
		if(rg_logo_filename_file_upload)
		{
			if ( rg_logo_filename_filename_ext != 'gif' &&
			     rg_logo_filename_filename_ext != 'jpg' &&
			     rg_logo_filename_filename_ext != 'jpeg' &&
			     rg_logo_filename_filename_ext != 'png' &&
			     rg_logo_filename_file_upload.type != "image/gif" &&
			     rg_logo_filename_file_upload.type != "image/jpeg" &&
			     rg_logo_filename_file_upload.type != "image/jpeg" &&
			     rg_logo_filename_file_upload.type != "image/png"){ show_msgbox('IMAGEM LOGOMARCA - Arquivo inválido!',' ','alert'); return false; }
			
			if ( rg_logo_filename_file_upload.size > 80000 ){ show_msgbox('Tamanho máximo permitido para o arquivo é de 80000 bytes!',' ','alert'); return false; }
		}
		
		//Formatacao Case
		rg_registro_rc = rg_registro_rc.toUpperCase();
		rg_registro_rt = rg_registro_rt.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "3");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", rg_id);
		_postdata.append("idcliente", rg_idcliente);
		_postdata.append("ano", rg_ano);
		_postdata.append("mes", rg_mes);
		_postdata.append("planilha_num", rg_planilha_num);
		_postdata.append("unidade_site", rg_unidade_site);
		_postdata.append("data_elaboracao", rg_data_elaboracao);
		_postdata.append("area", rg_area);
		_postdata.append("setor", rg_setor);
		_postdata.append("ges", rg_ges);
		_postdata.append("cargo_funcao", rg_cargo_funcao);
		_postdata.append("cbo", rg_cbo);
		_postdata.append("ativ_macro", rg_ativ_macro);
		_postdata.append("analises", rg_analises);
		_postdata.append("analise_amostra_1", rg_analise_amostra_1);
		_postdata.append("analise_data_amostragem_1", rg_analise_data_amostragem_1);
		_postdata.append("analise_tarefa_exec_1", rg_analise_tarefa_exec_1);
		_postdata.append("analise_proc_prod_1", rg_analise_proc_prod_1);
		_postdata.append("analise_obs_tarefa_1", rg_analise_obs_tarefa_1);
		_postdata.append("analise_amostra_2", rg_analise_amostra_2);
		_postdata.append("analise_data_amostragem_2", rg_analise_data_amostragem_2);
		_postdata.append("analise_tarefa_exec_2", rg_analise_tarefa_exec_2);
		_postdata.append("analise_proc_prod_2", rg_analise_proc_prod_2);
		_postdata.append("analise_obs_tarefa_2", rg_analise_obs_tarefa_2);
		_postdata.append("analise_amostra_3", rg_analise_amostra_3);
		_postdata.append("analise_data_amostragem_3", rg_analise_data_amostragem_3);
		_postdata.append("analise_tarefa_exec_3", rg_analise_tarefa_exec_3);
		_postdata.append("analise_proc_prod_3", rg_analise_proc_prod_3);
		_postdata.append("analise_obs_tarefa_3", rg_analise_obs_tarefa_3);
		_postdata.append("analise_amostra_4", rg_analise_amostra_4);
		_postdata.append("analise_data_amostragem_4", rg_analise_data_amostragem_4);
		_postdata.append("analise_tarefa_exec_4", rg_analise_tarefa_exec_4);
		_postdata.append("analise_proc_prod_4", rg_analise_proc_prod_4);
		_postdata.append("analise_obs_tarefa_4", rg_analise_obs_tarefa_4);
		_postdata.append("analise_amostra_5", rg_analise_amostra_5);
		_postdata.append("analise_data_amostragem_5", rg_analise_data_amostragem_5);
		_postdata.append("analise_tarefa_exec_5", rg_analise_tarefa_exec_5);
		_postdata.append("analise_proc_prod_5", rg_analise_proc_prod_5);
		_postdata.append("analise_obs_tarefa_5", rg_analise_obs_tarefa_5);
		_postdata.append("analise_amostra_6", rg_analise_amostra_6);
		_postdata.append("analise_data_amostragem_6", rg_analise_data_amostragem_6);
		_postdata.append("analise_tarefa_exec_6", rg_analise_tarefa_exec_6);
		_postdata.append("analise_proc_prod_6", rg_analise_proc_prod_6);
		_postdata.append("analise_obs_tarefa_6", rg_analise_obs_tarefa_6);
		_postdata.append("jor_trab", rg_jor_trab);
		_postdata.append("tempo_expo", rg_tempo_expo);
		_postdata.append("tipo_expo", rg_tipo_expo);
		_postdata.append("meio_propag", rg_meio_propag);
		_postdata.append("fonte_geradora", rg_fonte_geradora);
		_postdata.append("mitigacao", rg_mitigacao);
		_postdata.append("coletas", rg_coletas);
		_postdata.append("coleta_amostra_1", rg_coleta_amostra_1);
		_postdata.append("coleta_data_1", rg_coleta_data_1);
		_postdata.append("coleta_num_serial_1", rg_coleta_num_serial_1);
		_postdata.append("coleta_num_amostrador_1", rg_coleta_num_amostrador_1);
		_postdata.append("coleta_num_relat_ensaio_1", rg_coleta_num_relat_ensaio_1);
		_postdata.append("coleta_vazao_1", rg_coleta_vazao_1);
		_postdata.append("coleta_tempo_amostragem_1", rg_coleta_tempo_amostragem_1);
		_postdata.append("coleta_massa1_1", rg_coleta_massa1_1);
		_postdata.append("coleta_massa2_1", rg_coleta_massa2_1);
		_postdata.append("coleta_massa3_1", rg_coleta_massa3_1);
		_postdata.append("coleta_massa4_1", rg_coleta_massa4_1);
		_postdata.append("coleta_massa5_1", rg_coleta_massa5_1);
		_postdata.append("coleta_amostra_2", rg_coleta_amostra_2);
		_postdata.append("coleta_data_2", rg_coleta_data_2);
		_postdata.append("coleta_num_serial_2", rg_coleta_num_serial_2);
		_postdata.append("coleta_num_amostrador_2", rg_coleta_num_amostrador_2);
		_postdata.append("coleta_num_relat_ensaio_2", rg_coleta_num_relat_ensaio_2);
		_postdata.append("coleta_vazao_2", rg_coleta_vazao_2);
		_postdata.append("coleta_tempo_amostragem_2", rg_coleta_tempo_amostragem_2);
		_postdata.append("coleta_massa1_2", rg_coleta_massa1_2);
		_postdata.append("coleta_massa2_2", rg_coleta_massa2_2);
		_postdata.append("coleta_massa3_2", rg_coleta_massa3_2);
		_postdata.append("coleta_massa4_2", rg_coleta_massa4_2);
		_postdata.append("coleta_massa5_2", rg_coleta_massa5_2);
		_postdata.append("coleta_amostra_3", rg_coleta_amostra_3);
		_postdata.append("coleta_data_3", rg_coleta_data_3);
		_postdata.append("coleta_num_serial_3", rg_coleta_num_serial_3);
		_postdata.append("coleta_num_amostrador_3", rg_coleta_num_amostrador_3);
		_postdata.append("coleta_num_relat_ensaio_3", rg_coleta_num_relat_ensaio_3);
		_postdata.append("coleta_vazao_3", rg_coleta_vazao_3);
		_postdata.append("coleta_tempo_amostragem_3", rg_coleta_tempo_amostragem_3);
		_postdata.append("coleta_massa1_3", rg_coleta_massa1_3);
		_postdata.append("coleta_massa2_3", rg_coleta_massa2_3);
		_postdata.append("coleta_massa3_3", rg_coleta_massa3_3);
		_postdata.append("coleta_massa4_3", rg_coleta_massa4_3);
		_postdata.append("coleta_massa5_3", rg_coleta_massa5_3);
		_postdata.append("coleta_amostra_4", rg_coleta_amostra_4);
		_postdata.append("coleta_data_4", rg_coleta_data_4);
		_postdata.append("coleta_num_serial_4", rg_coleta_num_serial_4);
		_postdata.append("coleta_num_amostrador_4", rg_coleta_num_amostrador_4);
		_postdata.append("coleta_num_relat_ensaio_4", rg_coleta_num_relat_ensaio_4);
		_postdata.append("coleta_vazao_4", rg_coleta_vazao_4);
		_postdata.append("coleta_tempo_amostragem_4", rg_coleta_tempo_amostragem_4);
		_postdata.append("coleta_massa1_4", rg_coleta_massa1_4);
		_postdata.append("coleta_massa2_4", rg_coleta_massa2_4);
		_postdata.append("coleta_massa3_4", rg_coleta_massa3_4);
		_postdata.append("coleta_massa4_4", rg_coleta_massa4_4);
		_postdata.append("coleta_massa5_4", rg_coleta_massa5_4);
		_postdata.append("coleta_amostra_5", rg_coleta_amostra_5);
		_postdata.append("coleta_data_5", rg_coleta_data_5);
		_postdata.append("coleta_num_serial_5", rg_coleta_num_serial_5);
		_postdata.append("coleta_num_amostrador_5", rg_coleta_num_amostrador_5);
		_postdata.append("coleta_num_relat_ensaio_5", rg_coleta_num_relat_ensaio_5);
		_postdata.append("coleta_vazao_5", rg_coleta_vazao_5);
		_postdata.append("coleta_tempo_amostragem_5", rg_coleta_tempo_amostragem_5);
		_postdata.append("coleta_massa1_5", rg_coleta_massa1_5);
		_postdata.append("coleta_massa2_5", rg_coleta_massa2_5);
		_postdata.append("coleta_massa3_5", rg_coleta_massa3_5);
		_postdata.append("coleta_massa4_5", rg_coleta_massa4_5);
		_postdata.append("coleta_massa5_5", rg_coleta_massa5_5);
		_postdata.append("coleta_amostra_6", rg_coleta_amostra_6);
		_postdata.append("coleta_data_6", rg_coleta_data_6);
		_postdata.append("coleta_num_serial_6", rg_coleta_num_serial_6);
		_postdata.append("coleta_num_amostrador_6", rg_coleta_num_amostrador_6);
		_postdata.append("coleta_num_relat_ensaio_6", rg_coleta_num_relat_ensaio_6);
		_postdata.append("coleta_vazao_6", rg_coleta_vazao_6);
		_postdata.append("coleta_tempo_amostragem_6", rg_coleta_tempo_amostragem_6);
		_postdata.append("coleta_massa1_6", rg_coleta_massa1_6);
		_postdata.append("coleta_massa2_6", rg_coleta_massa2_6);
		_postdata.append("coleta_massa3_6", rg_coleta_massa3_6);
		_postdata.append("coleta_massa4_6", rg_coleta_massa4_6);
		_postdata.append("coleta_massa5_6", rg_coleta_massa5_6);
		_postdata.append("respirador", rg_respirador);
		_postdata.append("cert_aprov", rg_cert_aprov);
		_postdata.append("resp_campo_idcolaborador", rg_resp_campo_idcolaborador);
		_postdata.append("resp_tecnico_idcolaborador", rg_resp_tecnico_idcolaborador);
		_postdata.append("registro_rc", rg_registro_rc);
		_postdata.append("registro_rt", rg_registro_rt);
		_postdata.append("img_ativ_filename_file_info", rg_img_ativ_filename_file_info);
		_postdata.append("img_ativ_filename_file", rg_img_ativ_filename_file_upload);
		_postdata.append("img_ativ_filename_max_file_size", rg_img_ativ_filename_max_file_size);
		_postdata.append("logo_filename_file_info", rg_logo_filename_file_info);
		_postdata.append("logo_filename_file", rg_logo_filename_file_upload);
		_postdata.append("logo_filename_max_file_size", rg_logo_filename_max_file_size);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-part.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						$('#cad-modal').modal('hide');
						clear_inputs();
						//Refresh datatable
						refresh_datatable();
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	// Refresh datatable
	function refresh_datatable()
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "99");
		_postdata.append("l", $.Storage.loadItem('language'));
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-part.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
			 	cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], btn_link:tmp[3], total_recs:tmp[4]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						myTable.clear();
						$("#select-all").prop('checked', false);
						myTable.rows.add( $(myRet.text) ).draw();
						
						//Atualiza add button
						$('#add_button_link').empty().append(myRet.btn_link);
						
						//Atualiza total de registros
						if(isEmpty(myRet.total_recs)){ myRet.total_recs = 0; }
						$('#datatable_total_recs').val(myRet.total_recs);
					}
					else
					{
						return false;
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//Ficha Consolidada
	$(document).on("f_show_ficha", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de carregar ficha do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "7");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-part.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						hideLoadingBox();
						show_msgbox(myRet.text,'Ficha do Registro','ficha');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	//Delete Item
	$(document).on("f_show_del", function(e)
	{
		//Verifica se existe algum checkbox marcado para delecao
		var total_recs = $('#datatable_total_recs').val();
		if(isEmpty(total_recs)){ total_recs=0; }
		
		var allChks = [];
		$(".dt_checkbox:checked").each(function() 
		{ 
			if(!isEmpty($(this).attr('data-id'))){ allChks.push($(this).attr('data-id')); } 
		});
		
		if(allChks.length <= 0)
		{
			show_msgbox('Nenhum registro selecionado!',' ','alert');
			return false;
		}
		
		var confirmDiag = BootstrapDialog.confirm(
		{
			         title: 'ATENÇÃO!',
			       message: 'Os registros selecionados serão apagados. Confirma?',
			          type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			      closable: true, // <-- Default value is false
     //closeByBackdrop: false,
     //closeByKeyboard: false,
           draggable: false, // <-- Default value is false
			btnCancelLabel: 'Não', // <-- Default value is 'Cancel',
			    btnOKLabel: 'Sim', // <-- Default value is 'OK',
			    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
			      callback: function(result) 
			{
				// result will be true if button was click, while it will be false if users close the dialog directly.
				if(result) 
				{ 
					var delIDs = allChks.join(',');
					confirmDiag.close();
					
					//Processa delete dos registros selecionados
					//Formata postdata
					var _postdata = new FormData();
					_postdata.append("dbo", "4");
					_postdata.append("l", $.Storage.loadItem('language'));
					_postdata.append("rids", delIDs);
					
					//Show trobbler
					showLoadingBox('Processando...');
					
					$.ajax({
							 url: base_url + "/lib-bin/mdl-laudos-part.php",
							type: "post",
							//dataType: "json",
							data: _postdata,
							processData: false,
							contentType: false,
							cache: false,
							//scriptCharset: "utf-8",
							success: function(response, textStatus, jqXHR)
							{
								var tmp   = response.split('|');
								var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
								if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
								//
								if(myRet.status == 1)
								{
									refresh_datatable();
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
								else
								{
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
							},
							error: function(jqXHR, textStatus, errorThrown)
							{
									console.log("The following error occured: "+textStatus, errorThrown);
									hideLoadingBox();
									show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
									return false;
							},
							complete: function(){},
							statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
					});
					
					return false;
				}
				else
				{
					confirmDiag.close();
					return false;
				}
			}
		});
		
		return false;
	});
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Processa Form ~ STOP
	

});

