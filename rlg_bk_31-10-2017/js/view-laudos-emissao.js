/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-laudos-emissao.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  # Criacao: 4/10/2017 14:31:57
  ################################################################################# */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	

	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=laudos-emissao');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	

	////////////////////// CRONO ~ START
	// Return today's date and time
	var SYS_currentTime = new Date();
	
	// returns the year (four digits)
	var SYS_year = SYS_currentTime.getFullYear();
	var SYS_aaaa = SYS_year.toString();
	
	// returns the month (from 0 to 11)
	var SYS_month = SYS_currentTime.getMonth() + 1;
	var SYS_mm = SYS_month.toString();
	if(SYS_mm.length == 1){ SYS_mm = "0"+SYS_mm; }
	
	// returns the day of the month (from 1 to 31)
	var SYS_day = SYS_currentTime.getDate();
	var SYS_dd = SYS_day.toString();
	if(SYS_dd.length == 1){ SYS_dd = "0"+SYS_dd; }
	
	Date.prototype.holidays = 
	{
		// Feriados Gerais
		all: [
		'0101', // Jan 01 - Confraternizacao Universal
		'0414', // Abr 14 - Paixao de Cristo
		'0421', // Abr 21 - Tiradentes
		'0501', // Mai 01 - Dia do trabalho
		'0907', // Set 07 - Independencia do Brasil
		'1012', // Out 12 - Nossa Senhora Aparecida
		'1102', // Nov 02 - Finados
		'1115', // Nov 15 - Proclamacao da Republica
		'1225'  // Dez 25 - Natal
		],
		2017: [
		// Feriados especificos de 2017
		]
	};
	
	Date.prototype.addWorkingDays = function(days) 
	{
		while (days > 0) 
		{
			this.setDate(this.getDate() + 1);
			if (!this.isHoliday()) days--;
		}
		return this;
	};
	
	Date.prototype.substractWorkingDays = function(days) 
	{
		while (days > 0) 
		{
			this.setDate(this.getDate() - 1);
			if (!this.isHoliday()) days--;
		}
		return this;
	};
	
	Date.prototype.isHoliday = function() 
	{
		function zeroPad(n) 
		{
			n |= 0;
			return (n < 10 ? '0' : '') + n;
		}
		
		// if weekend return true from here it self;
		if (this.getDay() == 0 || this.getDay() == 6) { return true; } 
		
		var day = zeroPad(this.getMonth() + 1) + zeroPad(this.getDate());
		
		// if date is present in the holiday list return true;
		return !!~this.holidays.all.indexOf(day) || 
		(this.holidays[this.getFullYear()] ?
		!!~this.holidays[this.getFullYear()].indexOf(day) : false);
	};
	Date.prototype.toDDMMAAAA = function() 
	{
		var _ano = this.getFullYear();
		var _mes = this.getMonth() + 1;
		var _dia = this.getDate();
		var _dia = _dia.toString();
		if(_dia.length == 1){ _dia = "0"+_dia; }
		var _mes = _mes.toString();
		if(_mes.length == 1){ _mes = "0"+_mes; }
		return _dia+'/'+_mes+'/'+_ano;
	}
	/*
	// Uasage
	var date = new Date('2015-12-31');
	date.addWorkingDays(10);
	date.substractWorkingDays(10);
	alert(date.toDDMMAAAA());
	*/
	////////////////////// CRONO ~ STOP
	

	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	

	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	

	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	

	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined ||
	   ($.Storage.loadItem('language') != 'pt-br' && $.Storage.loadItem('language') != 'en-us' && $.Storage.loadItem('language') != 'es-es') 
	  )
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	

	////////////////////// FORMATACAO NUMERICA ~ START
	//SPRINTF javascript equivalent
	function sprintf() 
	{
		var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
	  var a = arguments;
	  var i = 0;
	  var format = a[i++];
	
	  // pad()
	  var pad = function(str, len, chr, leftJustify) {
	    if (!chr) {
	      chr = ' ';
	    }
	    var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
	      .join(chr);
	    return leftJustify ? str + padding : padding + str;
	  };
	
	  // justify()
	  var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
	    var diff = minWidth - value.length;
	    if (diff > 0) {
	      if (leftJustify || !zeroPad) {
	        value = pad(value, minWidth, customPadChar, leftJustify);
	      } else {
	        value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
	      }
	    }
	    return value;
	  };
	
	  // formatBaseX()
	  var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
	    // Note: casts negative numbers to positive ones
	    var number = value >>> 0;
	    prefix = prefix && number && {
	      '2': '0b',
	      '8': '0',
	      '16': '0x'
	    }[base] || '';
	    value = prefix + pad(number.toString(base), precision || 0, '0', false);
	    return justify(value, prefix, leftJustify, minWidth, zeroPad);
	  };
	
	  // formatString()
	  var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
	    if (precision != null) {
	      value = value.slice(0, precision);
	    }
	    return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
	  };
	
	  // doFormat()
	  var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
	    var number, prefix, method, textTransform, value;
	
	    if (substring === '%%') {
	      return '%';
	    }
	
	    // parse flags
	    var leftJustify = false;
	    var positivePrefix = '';
	    var zeroPad = false;
	    var prefixBaseX = false;
	    var customPadChar = ' ';
	    var flagsl = flags.length;
	    for (var j = 0; flags && j < flagsl; j++) {
	      switch (flags.charAt(j)) {
	        case ' ':
	          positivePrefix = ' ';
	          break;
	        case '+':
	          positivePrefix = '+';
	          break;
	        case '-':
	          leftJustify = true;
	          break;
	        case "'":
	          customPadChar = flags.charAt(j + 1);
	          break;
	        case '0':
	          zeroPad = true;
	          customPadChar = '0';
	          break;
	        case '#':
	          prefixBaseX = true;
	          break;
	      }
	    }
	
	    // parameters may be null, undefined, empty-string or real valued
	    // we want to ignore null, undefined and empty-string values
	    if (!minWidth) {
	      minWidth = 0;
	    } else if (minWidth === '*') {
	      minWidth = +a[i++];
	    } else if (minWidth.charAt(0) == '*') {
	      minWidth = +a[minWidth.slice(1, -1)];
	    } else {
	      minWidth = +minWidth;
	    }
	
	    // Note: undocumented perl feature:
	    if (minWidth < 0) {
	      minWidth = -minWidth;
	      leftJustify = true;
	    }
	
	    if (!isFinite(minWidth)) {
	      throw new Error('sprintf: (minimum-)width must be finite');
	    }
	
	    if (!precision) {
	      precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
	    } else if (precision === '*') {
	      precision = +a[i++];
	    } else if (precision.charAt(0) == '*') {
	      precision = +a[precision.slice(1, -1)];
	    } else {
	      precision = +precision;
	    }
	
	    // grab value using valueIndex if required?
	    value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];
	
	    switch (type) {
	      case 's':
	        return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
	      case 'c':
	        return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
	      case 'b':
	        return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'o':
	        return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'x':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'X':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
	          .toUpperCase();
	      case 'u':
	        return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'i':
	      case 'd':
	        number = +value || 0;
	        number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
	        prefix = number < 0 ? '-' : positivePrefix;
	        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad);
	      case 'e':
	      case 'E':
	      case 'f': // Should handle locales (as per setlocale)
	      case 'F':
	      case 'g':
	      case 'G':
	        number = +value;
	        prefix = number < 0 ? '-' : positivePrefix;
	        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
	        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
	        value = prefix + Math.abs(number)[method](precision);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
	      default:
	        return substring;
	    }
	  };
	
	  return format.replace(regex, doFormat);
	}
	// Formata valor de USD para BRL
	function currency_format (number, decimals, dec_point, thousands_sep) 
	{
		var n = number, prec = decimals;
		
		var toFixedFix = function (n,prec) {
		    var k = Math.pow(10,prec);
		    return (Math.round(n*k)/k).toString();
		};
		
		n = !isFinite(+n) ? 0 : +n;
		prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		var abs = toFixedFix(Math.abs(n), prec);
		var _, i;
		
		if (abs >= 1000) {
		    _ = abs.split(/\D/);
		    i = _[0].length % 3 || 3;
		
		    _[0] = s.slice(0,i + (n < 0)) +
		          _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		    s = _.join(dec);
		} else {
		    s = s.replace('.', dec);
		}
		
		var decPos = s.indexOf(dec);
		if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
		    s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		}
		else if (prec >= 1 && decPos === -1) {
		    s += dec+new Array(prec).join(0)+'0';
		}
		return s; 
	}
	// Formata currency on type
	function currencyFormat(fld, milSep, decSep, e) 
	{
		var sep = 0;
		var key = '';
		var i = j = 0;
		var len = len2 = 0;
		var strCheck = '0123456789';
		var aux = aux2 = '';
		var whichCode = (window.Event) ? e.which : e.keyCode;
		//
		if (whichCode == 13) return true;  // Enter
		if (whichCode == 8) return true;  // Delete
		key = String.fromCharCode(whichCode);  // Get key value from key code
		if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
		len = fld.value.length;
		for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
		for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
			  aux2 = '';
			  for (j = 0, i = len - 3; i >= 0; i--) {
			    if (j == 3) {
			      aux2 += milSep;
			      j = 0;
			    }//endif
			    aux2 += aux.charAt(i);
			    j++;
			  }//endfor
			  fld.value = '';
			  len2 = aux2.length;
			  for (i = len2 - 1; i >= 0; i--)
			  fld.value += aux2.charAt(i);
			  fld.value += decSep + aux.substr(len - 2, len);
			}//endif
		return false;
	}
	// Formata autoNumeric Defaults
	$.extend($.fn.autoNumeric.defaults, {aSign: 'R$ ', pSign:'p', aSep: '.', aDec: ','});
	// Format datepicker Defaults
	$.fn.datepicker.defaults.container='#cad-modal';
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	////////////////////// FORMATACAO NUMERICA ~ STOP
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric2", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				(45 <= event.which && event.which <= 45) || // -
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(40 <= event.which && event.which <= 41) || // ),(
				(45 <= event.which && event.which <= 45) || // -
				(48 <= event.which && event.which <= 57) || // 1-9
				(32 <= event.which && event.which <= 32) || // space
				(65 <= event.which && event.which <= 90) || // a-z
				(95 <= event.which && event.which <= 95) || // _
				(97 <= event.which && event.which <= 122) || // A-Z
				(192 <= event.which && event.which <= 196) || // A-Z
				(199 <= event.which && event.which <= 207) || // A-Z
				(209 <= event.which && event.which <= 214) || // A-Z
				(217 <= event.which && event.which <= 220) || // A-Z
				(224 <= event.which && event.which <= 228) || // A-Z
				(231 <= event.which && event.which <= 239) || // A-Z
				(241 <= event.which && event.which <= 246) || // A-Z
				(249 <= event.which && event.which <= 252) || // A-Z
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_barra", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(40 <= event.which && event.which <= 41) || // ),(
				(45 <= event.which && event.which <= 45) || // -
				(47 <= event.which && event.which <= 47) || // /
				(48 <= event.which && event.which <= 57) || // 1-9
				(32 <= event.which && event.which <= 32) || // space
				(65 <= event.which && event.which <= 90) || // a-z
				(95 <= event.which && event.which <= 95) || // _
				(97 <= event.which && event.which <= 122) || // A-Z
				(192 <= event.which && event.which <= 196) || // A-Z
				(199 <= event.which && event.which <= 207) || // A-Z
				(209 <= event.which && event.which <= 214) || // A-Z
				(217 <= event.which && event.which <= 220) || // A-Z
				(224 <= event.which && event.which <= 228) || // A-Z
				(231 <= event.which && event.which <= 239) || // A-Z
				(241 <= event.which && event.which <= 246) || // A-Z
				(249 <= event.which && event.which <= 252) || // A-Z
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".banco-digito", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    (88 <= event.which && event.which <= 88) || // X
		    (120 <= event.which && event.which <= 120) || // x
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_comma", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (44 <= event.which && event.which <= 44) || // ,(virgula)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".gps_coord", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	

	////////////////////// MSGBOX ~ START
	$(document).on("f_show_msgbox", function(e,_txt,_title,_type)
	{
		show_msgbox(_txt,_title,_type);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'ficha':
				$("#msgBoxIconFicha").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'pt-br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'en-us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'es-es':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	

	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		LoadingBoxPanel.remove();
	}
	////////////////////// LOADINGBOX ~ STOP
	

	////////////////////// Datatable ~ START
	var myTable = $('#main-datatable').DataTable(
	{
		dom: "lBfrtip",
		buttons: [
			{
				extend: "copy",
				className: "btn-sm",
				text: "<i class='fa fa-clipboard fa-lg'></i>",
				exportOptions: { columns: [  ] }
			},
			{
				extend: "csv",
				className: "btn-sm",
				text: "<i class='fa fa-file-text-o fa-lg'></i>",
				exportOptions: { columns: [  ] }
			},
			{
				extend: "excel",
				className: "btn-sm",
				text: "<i class='fa fa-file-excel-o fa-lg'></i>",
				title: "SEGVIDA - Laudos/Emissão",
				exportOptions: { columns: [  ] }
			},
//			{
//				extend: "pdf",
//				className: "btn-sm",
//				text: "<i class='fa fa-file-pdf-o fa-lg'></i>",
//				title: "SEGVIDA - Laudos/Emissão",
//				exportOptions: { columns: [  ] }
//			},
			{
				extend: "print",
				className: "btn-sm",
				text: "<i class='fa fa-print fa-lg'></i>",
				title: "SEGVIDA - Laudos/Emissão",
				exportOptions: { columns: [  ] }
			},
		],
		responsive: true,
		fixedHeader: true,
		select: true,
		//info:true,
		language: 
		{
			    sEmptyTable: "Nenhum registro encontrado",
			          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			     sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
			  sInfoFiltered: "(Filtrados de _MAX_ registros)",
			   sInfoPostFix: "",
			 sInfoThousands: ".",
			    sLengthMenu: "_MENU_ Resultados por página",
			sLoadingRecords: "Carregando...",
			    sProcessing: "Processando...",
			   sZeroRecords: "Nenhum registro encontrado",
			        sSearch: "Pesquisar",
			      oPaginate: {
													    sNext: "<i class='fa fa-arrow-circle-right fa-lg'></i>",
													sPrevious: "<i class='fa fa-arrow-circle-left fa-lg'></i>",
													   sFirst: "Primeiro",
													    sLast: "Último"
												},
								oAria: {
													sSortAscending: ": Ordenar colunas de forma ascendente",
													sSortDescending: ": Ordenar colunas de forma descendente"
												}
												,
							buttons: {
												  copyTitle: 'Copiado para o clipboard',
												   copyKeys: 'Pressione <i>ctrl</i> ou <i>⌘</i> + <i>C</i> para copiar os dados da tabela para o clipboard. <br><br>Para cancelar, clique sobre esta mensagem ou pressione ESC.',
												copySuccess: {
																				_: 'Copiados %d registros',
																				1: 'Copiado 1 registro'
																		}
												}
		
		},
		
		'order': [[ 2, 'asc' ]],
		'columnDefs': [
			{ 
				'targets': 0,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '10px',
				'render': function (data,type, full, meta){ return '<div class="checkbox checkbox-primary"><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></div>'; }
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '50px'
			}
		]
		
	});

	// Handle click on "Select all" control
	$('#select-all').on('click', function()
	{
		// Get all rows with search applied
		//var rows = myTable.rows({ 'search': 'applied' }).nodes();
		var rows = myTable.rows({ 'page':'current', 'filter': 'applied' }).nodes();
		// Check/uncheck checkboxes for all rows in the table
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
	
	// Handle click on checkbox to set state of "Select all" control
	$('#main-datatable tbody').on('change', 'input[type="checkbox"]', function()
	{
		// If checkbox is not checked
		if(!this.checked)
		{
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if(el && el.checked && ('indeterminate' in el))
			{
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});
	
	$('#main-datatable').on( 'page.dt', function () 
	{
		$('#select-all').prop('checked', false);
	});
	////////////////////// Datatable ~ STOP
	

	////////////////////// VALIDACOES DE FORM ~ START
	var onlyNumbersReg = /^\\d*$/;
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var apenasLetrasNumerosUnderlinesRegex = /^\w+$/;//apenas letras, numeros e underlines
	var apenasNumerosRegex = /^\d*$/;
	// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
	var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	var usernameRegex = /^\w+$/;//apenas letras, numeros e underlines
	var isDecimal2Regex = /^\d+\.\d{0,2}$/;
	var isDecimal3Regex = /^\d+\.\d{0,3}$/;
	var isDecimal4Regex = /^\d+\.\d{0,4}$/;
	var isDecimal5Regex = /^\d+\.\d{0,5}$/;
	var isDecimal6Regex = /^\d+\.\d{0,6}$/;
	var isDecimal7Regex = /^\d+\.\d{0,7}$/;
	var isDecimal8Regex = /^\d+\.\d{0,8}$/;
	var isValidPTBRCurrencyRegex = /\d{1,3}(?:\.\d{3})*?,\d{2}/;
	
	function isDate(str)
	{
		var parms = str.split(/[\.\-\/]/);
		var yyyy = parseInt(parms[2],10);
		var mm   = parseInt(parms[1],10);
		var dd   = parseInt(parms[0],10);
		var date = new Date(yyyy,mm-1,dd,0,0,0,0);
		return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
	}
	
	function getDataMySQL(_data)
	{
		if( isEmpty(_data) ){ return ''; }
		var _ret = '';
		var _tmp = _data.split("/"); 
		_ret = _tmp[2]+'-'+_tmp[1]+'-'+_tmp[0];
		return _ret;
	}
	
	function getPostDataEntries(_in)
	{
		var _entr = "";
		var _cap = "";
		for (var [key, value] of _in.entries()) 
		{
			if(_entr){ _cap = "&"; } else { _cap = ""; }
			_entr = _entr+_cap+key+"="+value;
		}
		return _entr;
	}
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	

	////////////////////// Processa Form ~ START
	//ANO
	$(document).on('blur', '#reg_ano', function()
	{
		if( !frm_valida_ano(this.value,true) ){ return false; }
	});
	function frm_valida_ano(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length != 4 )
		{
			if( ret_msg )
			{
				show_msgbox('ANO deve ter 4 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//on_change validations
	$(document).on("change", "#reg_mes", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_mes(this.value,true) ){ return false; }
		}
	});
	function frm_valida_mes(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo MÊS é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_cliente", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_cliente(this.value,true) ){ return false; }
			//$(this).find(':selected').data('id')
			var set_bio    = $('#reg_cliente').find(':selected').data('ld_bio');
			var set_calor  = $('#reg_cliente').find(':selected').data('ld_calor');
			var set_eletr  = $('#reg_cliente').find(':selected').data('ld_eletr');
			var set_expl   = $('#reg_cliente').find(':selected').data('ld_expl');
			var set_infl   = $('#reg_cliente').find(':selected').data('ld_infl');
			var set_part   = $('#reg_cliente').find(':selected').data('ld_part');
			var set_poei   = $('#reg_cliente').find(':selected').data('ld_poei');
			var set_rad    = $('#reg_cliente').find(':selected').data('ld_rad');
			var set_ris    = $('#reg_cliente').find(':selected').data('ld_ris');
			var set_rui    = $('#reg_cliente').find(':selected').data('ld_rui');
			var set_vap    = $('#reg_cliente').find(':selected').data('ld_vap');
			var set_vbrvci = $('#reg_cliente').find(':selected').data('ld_vbrvci');
			var set_vbrvmb = $('#reg_cliente').find(':selected').data('ld_vbrvmb');
			setLaudoPanel(set_bio, set_calor, set_eletr, set_expl, set_infl, set_part, set_poei, set_rad, set_ris, set_rui, set_vap, set_vbrvci, set_vbrvmb);
		}
		else
		{
			setLaudoPanel(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
		}
	});
	function frm_valida_cliente(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CLIENTE é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_bio_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_bio_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_bio_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO BIOLÓGICO - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_bio_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_bio_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_bio_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO BIOLÓGICO - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_calor_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_calor_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_calor_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO CALOR - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_calor_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_calor_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_calor_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO CALOR - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_eletr_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_eletr_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_eletr_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO ELETRICIDADE - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_eletr_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_eletr_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_eletr_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO ELETRICIDADE - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_expl_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_expl_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_expl_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO EXPLOSIVO - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_expl_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_expl_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_expl_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO EXPLOSIVO - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_infl_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_infl_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_infl_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO INFLAMÁVEL - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_infl_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_infl_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_infl_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO INFLAMÁVEL - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_part_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_part_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_part_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO PARTICULADO - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_part_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_part_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_part_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO PARTICULADO - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_poei_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_poei_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_poei_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO POEIRA - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_poei_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_poei_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_poei_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO POEIRA - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_rad_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_rad_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_rad_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO RADIAÇÃO IONIZANTE - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_rad_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_rad_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_rad_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO RADIAÇÃO IONIZANTE - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_rui_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_rui_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_rui_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO RUÍDO - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_rui_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_rui_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_rui_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO RUÍDO - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vap_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vap_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vap_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VAPOR - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vap_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vap_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vap_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VAPOR - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vbrvci_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vbrvci_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vbrvci_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VIBRAÇÃO VCI - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vbrvci_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vbrvci_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vbrvci_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VIBRAÇÃO VCI - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vbrvmb_plan", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vbrvmb_plan(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vbrvmb_plan(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VIBRAÇÃO VMB - Nº PLANILHA é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	$(document).on("change", "#reg_ld_vbrvmb_mod", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_ld_vbrvmb_mod(this.value,true) ){ return false; }
		}
	});
	function frm_valida_ld_vbrvmb_mod(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo LAUDO VIBRAÇÃO VMB - MODELO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function setLaudoPanel(_bio, _calor, _eletr, _expl, _infl, _part, _poei, _rad, _ris, _rui, _vap, _vbrvci, _vbrvmb)
	{
		//Oculta tudo
		$('#painel_bio_1').hide();
		$('#painel_bio_2').hide();
		$('#painel_calor_1').hide();
		$('#painel_calor_2').hide();
		$('#painel_eletr_1').hide();
		$('#painel_eletr_2').hide();
		$('#painel_expl_1').hide();
		$('#painel_expl_2').hide();
		$('#painel_infl_1').hide();
		$('#painel_infl_2').hide();
		$('#painel_part_1').hide();
		$('#painel_part_2').hide();
		$('#painel_poei_1').hide();
		$('#painel_poei_2').hide();
		$('#painel_rad_1').hide();
		$('#painel_rad_2').hide();
		$('#painel_ris_1').hide();
		$('#painel_ris_2').hide();
		$('#painel_rui_1').hide();
		$('#painel_rui_2').hide();
		$('#painel_vap_1').hide();
		$('#painel_vap_2').hide();
		$('#painel_vbrvci_1').hide();
		$('#painel_vbrvci_2').hide();
		$('#painel_vbrvmb_1').hide();
		$('#painel_vbrvmb_2').hide();
		
		var _option = "<option value=''>Nenhum registro encontrado!</option>";
		//var myRet = {status:0, txt:'', msgbox_type:'error', bio_p:_option, bio_m:_option, calor_p:_option, calor_m:_option, eletr_p:_option, eletr_m:_option, expl_p:_option, expl_m:_option, infl_p:_option, infl_m:_option, part_p:_option, part_m:_option, poei_p:_option, poei_m:_option, rad_p:_option, rad_m:_option, ris_p:_option, ris_m:_option, rui_p:_option, rui_m:_option, vap_p:_option, vap_m:_option, vbrvci_p:_option, vbrvci_m:_option, vbrvmb_p:_option, vbrvmb_m:_option};
		var _go = 0;
		
		//Carrega lista de planilhas e modelos
		if(_bio == 0 && _calor == 0 && _eletr == 0 && _expl == 0 && _infl == 0 && _part == 0 && _poei == 0 && _rad == 0 && _ris == 0 && _rui == 0 && _vap == 0 && _vbrvci == 0 && _vbrvmb == 0)
		{ var _go = 0; } else { var _go = 1; }
			
		if(_go == 0)
		{
			//var myRet = {status:1, txt:'', msgbox_type:'success', bio_p:_option, bio_m:_option, calor_p:_option, calor_m:_option, eletr_p:_option, eletr_m:_option, expl_p:_option, expl_m:_option, infl_p:_option, infl_m:_option, part_p:_option, part_m:_option, poei_p:_option, poei_m:_option, rad_p:_option, rad_m:_option, ris_p:_option, ris_m:_option, rui_p:_option, rui_m:_option, vap_p:_option, vap_m:_option, vbrvci_p:_option, vbrvci_m:_option, vbrvmb_p:_option, vbrvmb_m:_option};
		}
		else 
		{
			//segvida.prodfy.com.br/lib-bin/gera_options_laudos.php?l=pt-br&c=1&a=2017&m=9&bio=1&bio_fb=1&bio_fbt=Teste
			
			var rg_ano = $('#reg_ano').val();
			var rg_mes = $('#reg_mes').val();
			var rg_cliente = $('#reg_cliente').val();
			
			//ANO
			if (isEmpty(rg_ano)) { show_msgbox('O campo ANO é obrigatório!',' ','alert'); $('#reg_ano').focus(); return false; }
			if(!isEmpty(rg_ano) && (rg_ano.length != 4)){ show_msgbox('ANO deve ter 4 dígitos!',' ','alert'); $('#reg_ano').focus(); return false; }
			//MES
			if (isEmpty(rg_mes)) { show_msgbox('O campo MÊS é obrigatório!',' ','alert'); $('#reg_mes').focus(); return false; }
			//CLIENTE
			if (isEmpty(rg_cliente)) { show_msgbox('O campo CLIENTE é obrigatório!',' ','alert'); $('#reg_cliente').focus(); return false; }
			
			//Show trobbler
			showLoadingBox('Carregando Laudos...');
			
			//Formata postdata
			var _postdata = new FormData();
			_postdata.append("l", $.Storage.loadItem('language'));
			_postdata.append("a", rg_ano);
			_postdata.append("m", rg_mes);
			_postdata.append("c", rg_cliente);
			_postdata.append("bio", _bio);
			_postdata.append("bio_fb", '0');
			_postdata.append("bio_fbt", '');
			_postdata.append("calor", _calor);
			_postdata.append("calor_fb", '0');
			_postdata.append("calor_fbt", '');
			_postdata.append("eletr", _eletr);
			_postdata.append("eletr_fb", '0');
			_postdata.append("eletr_fbt", '');
			_postdata.append("expl", _expl);
			_postdata.append("expl_fb", '0');
			_postdata.append("expl_fbt", '');
			_postdata.append("infl", _infl);
			_postdata.append("infl_fb", '0');
			_postdata.append("infl_fbt", '');
			_postdata.append("part", _part);
			_postdata.append("part_fb", '0');
			_postdata.append("part_fbt", '');
			_postdata.append("poei", _poei);
			_postdata.append("poei_fb", '0');
			_postdata.append("poei_fbt", '');
			_postdata.append("rad", _rad);
			_postdata.append("rad_fb", '0');
			_postdata.append("rad_fbt", '');
			//_postdata.append("ris", _ris);
			//_postdata.append("ris_fb", '0');
			//_postdata.append("ris_fbt", '');
			_postdata.append("rui", _rui);
			_postdata.append("rui_fb", '0');
			_postdata.append("rui_fbt", '');
			_postdata.append("vap", _vap);
			_postdata.append("vap_fb", '0');
			_postdata.append("vap_fbt", '');
			_postdata.append("vbrvci", _vbrvci);
			_postdata.append("vbrvci_fb", '0');
			_postdata.append("vbrvci_fbt", '');
			_postdata.append("vbrvmb", _vbrvmb);
			_postdata.append("vbrvmb_fb", '0');
			_postdata.append("vbrvmb_fbt", '');
			
			//console.log(..._postdata);
			
			//Debug
			//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
			//return;
			
			//var _tmp = getPostDataEntries(_postdata);
			//$('#panel_debug').val(base_url + "/lib-bin/gera_options_laudos.php?"+_tmp+"\n\n");
			
			$.ajax({
					 url: base_url + "/lib-bin/gera_options_laudos.php",
					type: "post",
					//dataType: "json",
					data: _postdata,
					processData: false,
					contentType: false,
					cache: false,
					//scriptCharset: "utf-8",
					success: function(response, textStatus, jqXHR)
					{
						/*
						die('1||success|'.
						$LISTA_PLAN_BIO.'|'.$LISTA_MOD_BIO.'|'.
						$LISTA_PLAN_CALOR.'|'.$LISTA_MOD_CALOR.'|'.
						$LISTA_PLAN_ELETR.'|'.$LISTA_MOD_ELETR.'|'.
						$LISTA_PLAN_EXPL.'|'.$LISTA_MOD_EXPL.'|'.
						$LISTA_PLAN_INFL.'|'.$LISTA_MOD_INFL.'|'.
						$LISTA_PLAN_PART.'|'.$LISTA_MOD_PART.'|'.
						$LISTA_PLAN_POEI.'|'.$LISTA_MOD_POEI.'|'.
						$LISTA_PLAN_RAD.'|'.$LISTA_MOD_RAD.'|'.
						$LISTA_PLAN_RIS.'|'.$LISTA_MOD_RIS.'|'.
						$LISTA_PLAN_RUI.'|'.$LISTA_MOD_RUI.'|'.
						$LISTA_PLAN_VAP.'|'.$LISTA_MOD_VAP.'|'.
						$LISTA_PLAN_VBRVCI.'|'.$LISTA_MOD_VBRVCI.'|'.
						$LISTA_PLAN_VBRVMB.'|'.$LISTA_MOD_VBRVMB.'|');
						*/
						//var _tmpp = $('#panel_debug').val();
						//$('#panel_debug').val("\n\n"+response);
						
						var tmp   = response.split('|');
						var myRet = {status:tmp[0], txt:tmp[1], msgbox_type:tmp[2], bio_p:tmp[3], bio_m:tmp[4], calor_p:tmp[5], calor_m:tmp[6], eletr_p:tmp[7], eletr_m:tmp[8], expl_p:tmp[9], expl_m:tmp[10], infl_p:tmp[11], infl_m:tmp[12], part_p:tmp[13], part_m:tmp[14], poei_p:tmp[15], poei_m:tmp[16], rad_p:tmp[17], rad_m:tmp[18], ris_p:tmp[19], ris_m:tmp[20], rui_p:tmp[21], rui_m:tmp[22], vap_p:tmp[23], vap_m:tmp[24], vbrvci_p:tmp[25], vbrvci_m:tmp[26], vbrvmb_p:tmp[27], vbrvmb_m:tmp[28]};
						//_bio, _calor, _eletr, _expl, _infl, _part, _poei, _rad, _ris, _rui, _vap, _vbrvci, _vbrvmb)
						
						//$('#panel_debug').val($('#panel_debug').val()+"\n\nRAD:\n\n(p):"+myRet.rad_p+ "\n\n(m):"+myRet.rad_m);
						
						if(myRet.status == 1)
						{
							if(_bio == 1)
							{
								if(myRet.bio_p){ $('#reg_ld_bio_plan').empty().append(myRet.bio_p); } else { $('#reg_ld_bio_plan').empty().append(_option); }
								if(myRet.bio_m){ $('#reg_ld_bio_mod').empty().append(myRet.bio_m);  } else { $('#reg_ld_bio_mod').empty().append(_option);  }
							}
							if(_calor == 1)
							{
								if(myRet.calor_p){ $('#reg_ld_calor_plan').empty().append(myRet.calor_p); } else { $('#reg_ld_calor_plan').empty().append(_option); }
								if(myRet.calor_m){ $('#reg_ld_calor_mod').empty().append(myRet.calor_m);  } else { $('#reg_ld_calor_mod').empty().append(_option);  }
							}
							if(_eletr == 1)
							{
								if(myRet.eletr_p){ $('#reg_ld_eletr_plan').empty().append(myRet.eletr_p); } else { $('#reg_ld_eletr_plan').empty().append(_option); }
								if(myRet.eletr_m){ $('#reg_ld_eletr_mod').empty().append(myRet.eletr_m);  } else { $('#reg_ld_eletr_mod').empty().append(_option);  }
							}
							if(_expl == 1)
							{
								if(myRet.expl_p){ $('#reg_ld_expl_plan').empty().append(myRet.expl_p); } else { $('#reg_ld_expl_plan').empty().append(_option); }
								if(myRet.expl_m){ $('#reg_ld_expl_mod').empty().append(myRet.expl_m);  } else { $('#reg_ld_expl_mod').empty().append(_option);  }
							}
							if(_infl == 1)
							{
								if(myRet.infl_p){ $('#reg_ld_infl_plan').empty().append(myRet.infl_p); } else { $('#reg_ld_infl_plan').empty().append(_option); }
								if(myRet.infl_m){ $('#reg_ld_infl_mod').empty().append(myRet.infl_m);  } else { $('#reg_ld_infl_mod').empty().append(_option);  }
							}
							if(_part == 1)
							{
								if(myRet.part_p){ $('#reg_ld_part_plan').empty().append(myRet.part_p); } else { $('#reg_ld_part_plan').empty().append(_option); }
								if(myRet.part_m){ $('#reg_ld_part_mod').empty().append(myRet.part_m);  } else { $('#reg_ld_part_mod').empty().append(_option);  }
							}
							if(_poei == 1)
							{
								if(myRet.poei_p){ $('#reg_ld_poei_plan').empty().append(myRet.poei_p); } else { $('#reg_ld_poei_plan').empty().append(_option); }
								if(myRet.poei_m){ $('#reg_ld_poei_mod').empty().append(myRet.poei_m);  } else { $('#reg_ld_poei_mod').empty().append(_option);  }
							}
							if(_rad == 1)
							{
								if(myRet.rad_p){ $('#reg_ld_rad_plan').empty().append(myRet.rad_p); } else { $('#reg_ld_rad_plan').empty().append(_option); }
								if(myRet.rad_m){ $('#reg_ld_rad_mod').empty().append(myRet.rad_m);  } else { $('#reg_ld_rad_mod').empty().append(_option);  }
							}
							if(_rui == 1)
							{
								if(myRet.rui_p){ $('#reg_ld_rui_plan').empty().append(myRet.rui_p); } else { $('#reg_ld_rui_plan').empty().append(_option); }
								if(myRet.rui_m){ $('#reg_ld_rui_mod').empty().append(myRet.rui_m);  } else { $('#reg_ld_rui_mod').empty().append(_option);  }
							}
							if(_vap == 1)
							{
								if(myRet.vap_p){ $('#reg_ld_vap_plan').empty().append(myRet.vap_p); } else { $('#reg_ld_vap_plan').empty().append(_option); }
								if(myRet.vap_m){ $('#reg_ld_vap_mod').empty().append(myRet.vap_m);  } else { $('#reg_ld_vap_mod').empty().append(_option);  }
							}
							if(_vbrvci == 1)
							{
								if(myRet.vbrvci_p){ $('#reg_ld_vbrvci_plan').empty().append(myRet.vbrvci_p); } else { $('#reg_ld_vbrvci_plan').empty().append(_option); }
								if(myRet.vbrvci_m){ $('#reg_ld_vbrvci_mod').empty().append(myRet.vbrvci_m);  } else { $('#reg_ld_vbrvci_mod').empty().append(_option);  }
							}
							if(_vbrvmb == 1)
							{
								if(myRet.vbrvmb_p){ $('#reg_ld_vbrvmb_plan').empty().append(myRet.vbrvmb_p); } else { $('#reg_ld_vbrvmb_plan').empty().append(_option); }
								if(myRet.vbrvmb_m){ $('#reg_ld_vbrvmb_mod').empty().append(myRet.vbrvmb_m);  } else { $('#reg_ld_vbrvmb_mod').empty().append(_option);  }
							}
							
							hideLoadingBox();
							//return true;
						}
						else
						{
							hideLoadingBox();
							//show_msgbox(myRet.text,' ',myRet.msgbox_type);
							//return 'nao_existe';
						}
						
					},
					error: function(jqXHR, textStatus, errorThrown)
					{
							console.log("The following error occured: "+textStatus, errorThrown);
							hideLoadingBox();
							show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
							return false;
					},
					complete: function(){},
					statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
			});
		}
		
		
		
		//Mostra laudos indicados
		if(_bio == 1)
		{
			$('#painel_bio_1').show();
			$('#painel_bio_2').show();
		}
		if(_calor == 1)
		{
			$('#painel_calor_1').show();
			$('#painel_calor_2').show();
		}
		if(_eletr  == 1)
		{
			$('#painel_eletr_1').show();
			$('#painel_eletr_2').show();
		}
		if(_expl == 1)
		{
			$('#painel_expl_1').show();
			$('#painel_expl_2').show();
		}
		if(_infl == 1)
		{
			$('#painel_infl_1').show();
			$('#painel_infl_2').show();
		}
		if(_part == 1)
		{
			$('#painel_part_1').show();
			$('#painel_part_2').show();
		}
		if(_poei == 1)
		{
			$('#painel_poei_1').show();
			$('#painel_poei_2').show();
		}
		if(_rad == 1)
		{
			$('#painel_rad_1').show();
			$('#painel_rad_2').show();
		}
		if(_rui == 1)
		{
			$('#painel_rui_1').show();
			$('#painel_rui_2').show();
		}
		if(_vap == 1)
		{
			$('#painel_vap_1').show();
			$('#painel_vap_2').show();
		}
		if(_vbrvci == 1)
		{
			$('#painel_vbrvci_1').show();
			$('#painel_vbrvci_2').show();
		}
		if(_vbrvmb == 1)
		{
			$('#painel_vbrvmb_1').show();
			$('#painel_vbrvmb_2').show();
		}
		
	}
	setLaudoPanel(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	////////////////////// Processa Form ~ STOP
	

	////////////////////// Form ~ START
	var $formRegister = $('#cad-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	//Clear Inputs
	function clear_inputs()
	{
		$('#reg_ano').val(SYS_aaaa);
		$('#reg_mes').val(SYS_month);
		$('#reg_cliente').val('');
		$('#reg_ld_bio_plan').val('');
		$('#reg_ld_bio_mod').val('');
		$('#reg_ld_calor_plan').val('');
		$('#reg_ld_calor_mod').val('');
		$('#reg_ld_eletr_plan').val('');
		$('#reg_ld_eletr_mod').val('');
		$('#reg_ld_expl_plan').val('');
		$('#reg_ld_expl_mod').val('');
		$('#reg_ld_infl_plan').val('');
		$('#reg_ld_infl_mod').val('');
		$('#reg_ld_part_plan').val('');
		$('#reg_ld_part_mod').val('');
		$('#reg_ld_poei_plan').val('');
		$('#reg_ld_poei_mod').val('');
		$('#reg_ld_rad_plan').val('');
		$('#reg_ld_rad_mod').val('');
		//$('#reg_ld_ris_plan').val('');
		//$('#reg_ld_ris_mod').val('');
		$('#reg_ld_rui_plan').val('');
		$('#reg_ld_rui_mod').val('');
		$('#reg_ld_vap_plan').val('');
		$('#reg_ld_vap_mod').val('');
		$('#reg_ld_vbrvci_plan').val('');
		$('#reg_ld_vbrvci_mod').val('');
		$('#reg_ld_vbrvmb_plan').val('');
		$('#reg_ld_vbrvmb_mod').val('');
		setLaudoPanel(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
	}
	
	$('#reg_ano').val(SYS_aaaa);
	$('#reg_mes').val(SYS_month);
	
	// Help Info Popups
	
	// Gerar
	$('#bt_gerar').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_ano = $('#reg_ano').val();
		var rg_mes = $('#reg_mes').val();
		var rg_cliente = $('#reg_cliente').val();
		
		var set_bio    = $('#reg_cliente').find(':selected').data('ld_bio');
		var set_calor  = $('#reg_cliente').find(':selected').data('ld_calor');
		var set_eletr  = $('#reg_cliente').find(':selected').data('ld_eletr');
		var set_expl   = $('#reg_cliente').find(':selected').data('ld_expl');
		var set_infl   = $('#reg_cliente').find(':selected').data('ld_infl');
		var set_part   = $('#reg_cliente').find(':selected').data('ld_part');
		var set_poei   = $('#reg_cliente').find(':selected').data('ld_poei');
		var set_rad    = $('#reg_cliente').find(':selected').data('ld_rad');
		var set_ris    = $('#reg_cliente').find(':selected').data('ld_ris');
		var set_rui    = $('#reg_cliente').find(':selected').data('ld_rui');
		var set_vap    = $('#reg_cliente').find(':selected').data('ld_vap');
		var set_vbrvci = $('#reg_cliente').find(':selected').data('ld_vbrvci');
		var set_vbrvmb = $('#reg_cliente').find(':selected').data('ld_vbrvmb');
		
		
		if(set_bio == 1)
		{
			var rg_ld_bio_plan = $('#reg_ld_bio_plan').val();
			var rg_ld_bio_mod  = $('#reg_ld_bio_mod').val();
		}
		if(set_calor == 1)
		{
			var rg_ld_calor_plan = $('#reg_ld_calor_plan').val();
			var rg_ld_calor_mod  = $('#reg_ld_calor_mod').val();
		}
		if(set_eletr == 1)
		{
			var rg_ld_eletr_plan = $('#reg_ld_eletr_plan').val();
			var rg_ld_eletr_mod  = $('#reg_ld_eletr_mod').val();
		}
		if(set_expl == 1)
		{
			var rg_ld_expl_plan  = $('#reg_ld_expl_plan').val();
			var rg_ld_expl_mod   = $('#reg_ld_expl_mod').val();
		}
		if(set_infl == 1)
		{
			var rg_ld_infl_plan  = $('#reg_ld_infl_plan').val();
			var rg_ld_infl_mod   = $('#reg_ld_infl_mod').val();
		}
		if(set_part == 1)
		{
			var rg_ld_part_plan  = $('#reg_ld_part_plan').val();
			var rg_ld_part_mod   = $('#reg_ld_part_mod').val();
		}
		if(set_poei == 1)
		{
			var rg_ld_poei_plan  = $('#reg_ld_poei_plan').val();
			var rg_ld_poei_mod   = $('#reg_ld_poei_mod').val();
		}
		if(set_rad == 1)
		{
			var rg_ld_rad_plan   = $('#reg_ld_rad_plan').val();
			var rg_ld_rad_mod    = $('#reg_ld_rad_mod').val();
		}
		if(set_ris == 1)
		{
			//var rg_ld_ris_plan = $('#reg_ld_ris_plan').val();
			//var rg_ld_ris_mod  = $('#reg_ld_ris_mod').val();
		}
		if(set_rui == 1)
		{
			var rg_ld_rui_plan = $('#reg_ld_rui_plan').val();
			var rg_ld_rui_mod  = $('#reg_ld_rui_mod').val();
		}
		if(set_vap == 1)
		{
			var rg_ld_vap_plan = $('#reg_ld_vap_plan').val();
			var rg_ld_vap_mod  = $('#reg_ld_vap_mod').val();
		}
		if(set_vbrvci == 1)
		{
			var rg_ld_vbrvci_plan = $('#reg_ld_vbrvci_plan').val();
			var rg_ld_vbrvci_mod  = $('#reg_ld_vbrvci_mod').val();
		}
		if(set_vbrvmb == 1)
		{
			var rg_ld_vbrvmb_plan = $('#reg_ld_vbrvmb_plan').val();
			var rg_ld_vbrvmb_mod  = $('#reg_ld_vbrvmb_mod').val();
		}
		
		//ANO
		if (isEmpty(rg_ano)) { show_msgbox('O campo ANO é obrigatório!',' ','alert'); $('#reg_ano').focus(); return false; }
		if(!isEmpty(rg_ano) && (rg_ano.length != 4)){ show_msgbox('ANO deve ter 4 dígitos!',' ','alert'); $('#reg_ano').focus(); return false; }
		//MES
		if (isEmpty(rg_mes)) { show_msgbox('O campo MÊS é obrigatório!',' ','alert'); $('#reg_mes').focus(); return false; }
		//CLIENTE
		if (isEmpty(rg_cliente)) { show_msgbox('O campo CLIENTE é obrigatório!',' ','alert'); $('#reg_cliente').focus(); return false; }
		
		if(set_bio == 1)
		{
			//LAUDO BIOLOGICO - Nº PLANILHA
			if (isEmpty(rg_ld_bio_plan)) { show_msgbox('O campo LAUDO BIOLÓGICO - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_bio_plan').focus(); return false; }
			//LAUDO BIOLOGICO - MODELO
			if (isEmpty(rg_ld_bio_mod)) { show_msgbox('O campo LAUDO BIOLÓGICO - MODELO é obrigatório!',' ','alert'); $('#reg_ld_bio_mod').focus(); return false; }
		}
		if(set_calor == 1)
		{
			//LAUDO CALOR - Nº PLANILHA
			if (isEmpty(rg_ld_calor_plan)) { show_msgbox('O campo LAUDO CALOR - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_calor_plan').focus(); return false; }
			//LAUDO CALOR - MODELO
			if (isEmpty(rg_ld_calor_mod)) { show_msgbox('O campo LAUDO CALOR - MODELO é obrigatório!',' ','alert'); $('#reg_ld_calor_mod').focus(); return false; }
		}
		if(set_eletr == 1)
		{
			//LAUDO ELETRICIDADE - Nº PLANILHA
			if (isEmpty(rg_ld_eletr_plan)) { show_msgbox('O campo LAUDO ELETRICIDADE - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_eletr_plan').focus(); return false; }
			//LAUDO ELETRICIDADE - MODELO
			if (isEmpty(rg_ld_eletr_mod)) { show_msgbox('O campo LAUDO ELETRICIDADE - MODELO é obrigatório!',' ','alert'); $('#reg_ld_eletr_mod').focus(); return false; }
		}
		if(set_expl == 1)
		{
			//LAUDO EXPLOSIVO - Nº PLANILHA
			if (isEmpty(rg_ld_expl_plan)) { show_msgbox('O campo LAUDO EXPLOSIVO - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_expl_plan').focus(); return false; }
			//LAUDO EXPLOSIVO - MODELO
			if (isEmpty(rg_ld_expl_mod)) { show_msgbox('O campo LAUDO EXPLOSIVO - MODELO é obrigatório!',' ','alert'); $('#reg_ld_expl_mod').focus(); return false; }
		}
		if(set_infl == 1)
		{
			//LAUDO INFLAMAVEL - Nº PLANILHA
			if (isEmpty(rg_ld_infl_plan)) { show_msgbox('O campo LAUDO INFLAMÁVEL - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_infl_plan').focus(); return false; }
			//LAUDO INFLAMAVEL - MODELO
			if (isEmpty(rg_ld_infl_mod)) { show_msgbox('O campo LAUDO INFLAMÁVEL - MODELO é obrigatório!',' ','alert'); $('#reg_ld_infl_mod').focus(); return false; }
		}
		if(set_part == 1)
		{
			//LAUDO PARTICULADO - Nº PLANILHA
			if (isEmpty(rg_ld_part_plan)) { show_msgbox('O campo LAUDO PARTICULADO - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_part_plan').focus(); return false; }
			//LAUDO PARTICULADO - MODELO
			if (isEmpty(rg_ld_part_mod)) { show_msgbox('O campo LAUDO PARTICULADO - MODELO é obrigatório!',' ','alert'); $('#reg_ld_part_mod').focus(); return false; }
		}
		if(set_poei == 1)
		{
			//LAUDO POEIRA - Nº PLANILHA
			if (isEmpty(rg_ld_poei_plan)) { show_msgbox('O campo LAUDO POEIRA - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_poei_plan').focus(); return false; }
			//LAUDO POEIRA - MODELO
			if (isEmpty(rg_ld_poei_mod)) { show_msgbox('O campo LAUDO POEIRA - MODELO é obrigatório!',' ','alert'); $('#reg_ld_poei_mod').focus(); return false; }
		}
		if(set_rad == 1)
		{
			//LAUDO RADIACAO IONIZANTE - Nº PLANILHA
			if (isEmpty(rg_ld_rad_plan)) { show_msgbox('O campo LAUDO RADIAÇÃO IONIZANTE - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_rad_plan').focus(); return false; }
			//LAUDO RADIACAO IONIZANTE - MODELO
			if (isEmpty(rg_ld_rad_mod)) { show_msgbox('O campo LAUDO RADIAÇÃO IONIZANTE - MODELO é obrigatório!',' ','alert'); $('#reg_ld_rad_mod').focus(); return false; }
		}
		if(set_rui == 1)
		{
			//LAUDO RUIDO - Nº PLANILHA
			if (isEmpty(rg_ld_rui_plan)) { show_msgbox('O campo LAUDO RUÍDO - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_rui_plan').focus(); return false; }
			//LAUDO RUIDO - MODELO
			if (isEmpty(rg_ld_rui_mod)) { show_msgbox('O campo LAUDO RUÍDO - MODELO é obrigatório!',' ','alert'); $('#reg_ld_rui_mod').focus(); return false; }
		}
		if(set_vap == 1)
		{
			//LAUDO VAPOR - Nº PLANILHA
			if (isEmpty(rg_ld_vap_plan)) { show_msgbox('O campo LAUDO VAPOR - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_vap_plan').focus(); return false; }
			//LAUDO VAPOR - MODELO
			if (isEmpty(rg_ld_vap_mod)) { show_msgbox('O campo LAUDO VAPOR - MODELO é obrigatório!',' ','alert'); $('#reg_ld_vap_mod').focus(); return false; }
		}
		if(set_vbrvci == 1)
		{
			//LAUDO VIBRACAO VCI - Nº PLANILHA
			if (isEmpty(rg_ld_vbrvci_plan)) { show_msgbox('O campo LAUDO VIBRAÇÃO VCI - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_vbrvci_plan').focus(); return false; }
			//LAUDO VIBRACAO VCI - MODELO
			if (isEmpty(rg_ld_vbrvci_mod)) { show_msgbox('O campo LAUDO VIBRAÇÃO VCI - MODELO é obrigatório!',' ','alert'); $('#reg_ld_vbrvci_mod').focus(); return false; }
		}
		if(set_vbrvmb == 1)
		{
			//LAUDO VIBRACAO VMB - Nº PLANILHA
			if (isEmpty(rg_ld_vbrvmb_plan)) { show_msgbox('O campo LAUDO VIBRAÇÃO VMB - Nº PLANILHA é obrigatório!',' ','alert'); $('#reg_ld_vbrvmb_plan').focus(); return false; }
			//LAUDO VIBRACAO VMB - MODELO
			if (isEmpty(rg_ld_vbrvmb_mod)) { show_msgbox('O campo LAUDO VIBRAÇÃO VMB - MODELO é obrigatório!',' ','alert'); $('#reg_ld_vbrvmb_mod').focus(); return false; }
		}
		
		//Formatacao Case
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "8");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("ano", rg_ano);
		_postdata.append("mes", rg_mes);
		_postdata.append("cliente", rg_cliente);
		
		if(set_bio == 1)
		{
			_postdata.append("ld_bio_set", '1');
			_postdata.append("ld_bio_plan", rg_ld_bio_plan);
			_postdata.append("ld_bio_mod", rg_ld_bio_mod);
		}
		if(set_calor == 1)
		{
			_postdata.append("ld_calor_set", '1');
			_postdata.append("ld_calor_plan", rg_ld_calor_plan);
			_postdata.append("ld_calor_mod", rg_ld_calor_mod);
		}
		if(set_eletr == 1)
		{
			_postdata.append("ld_eletr_set", '1');
			_postdata.append("ld_eletr_plan", rg_ld_eletr_plan);
			_postdata.append("ld_eletr_mod", rg_ld_eletr_mod);
		}
		if(set_expl == 1)
		{
			_postdata.append("ld_expl_set", '1');
			_postdata.append("ld_expl_plan", rg_ld_expl_plan);
			_postdata.append("ld_expl_mod", rg_ld_expl_mod);
		}
		if(set_infl == 1)
		{
			_postdata.append("ld_infl_set", '1');
			_postdata.append("ld_infl_plan", rg_ld_infl_plan);
			_postdata.append("ld_infl_mod", rg_ld_infl_mod);
		}
		if(set_part == 1)
		{
			_postdata.append("ld_part_set", '1');
			_postdata.append("ld_part_plan", rg_ld_part_plan);
			_postdata.append("ld_part_mod", rg_ld_part_mod);
		}
		if(set_poei == 1)
		{
			_postdata.append("ld_poei_set", '1');
			_postdata.append("ld_poei_plan", rg_ld_poei_plan);
			_postdata.append("ld_poei_mod", rg_ld_poei_mod);
		}
		if(set_rad == 1)
		{
			_postdata.append("ld_rad_set", '1');
			_postdata.append("ld_rad_plan", rg_ld_rad_plan);
			_postdata.append("ld_rad_mod", rg_ld_rad_mod);
		}
		//if(set_ris == 1)
		//{
		//	_postdata.append("ld_ris_set", '1');
		//	_postdata.append("ld_ris_plan", rg_ld_ris_plan);
		//	_postdata.append("ld_ris_mod", rg_ld_ris_mod);
		//}
		if(set_rui == 1)
		{
			_postdata.append("ld_rui_set", '1');
			_postdata.append("ld_rui_plan", rg_ld_rui_plan);
			_postdata.append("ld_rui_mod", rg_ld_rui_mod);
		}
		if(set_vap == 1)
		{
			_postdata.append("ld_vap_set", '1');
			_postdata.append("ld_vap_plan", rg_ld_vap_plan);
			_postdata.append("ld_vap_mod", rg_ld_vap_mod);
		}
		if(set_vbrvci == 1)
		{
			_postdata.append("ld_vbrvci_set", '1');
			_postdata.append("ld_vbrvci_plan", rg_ld_vbrvci_plan);
			_postdata.append("ld_vbrvci_mod", rg_ld_vbrvci_mod);
		}
		if(set_vbrvmb == 1)
		{
			_postdata.append("ld_vbrvmb_set", '1');
			_postdata.append("ld_vbrvmb_plan", rg_ld_vbrvmb_plan);
			_postdata.append("ld_vbrvmb_mod", rg_ld_vbrvmb_mod);
		}
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-emissao.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						//clear_inputs();
						//refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						$('#cad-modal').scrollTop(0);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						//refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	// Refresh datatable
	function refresh_datatable()
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "99");
		_postdata.append("l", $.Storage.loadItem('language'));
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-laudos-emissao.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
			 	cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], btn_link:tmp[3], total_recs:tmp[4]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						myTable.clear();
						$("#select-all").prop('checked', false);
						myTable.rows.add( $(myRet.text) ).draw();
						
						//Atualiza add button
						$('#add_button_link').empty().append(myRet.btn_link);
						
						//Atualiza total de registros
						if(isEmpty(myRet.total_recs)){ myRet.total_recs = 0; }
						$('#datatable_total_recs').val(myRet.total_recs);
					}
					else
					{
						return false;
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Processa Form ~ STOP
	

});

