<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-relat-etiquetas.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 450px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	
	/*##################
	  # confirm-dialog #
	  ################## */
	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
	
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	.input-group-addon {
	     min-width:170px;
	         width:170px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Ficha
	.ficha-table {
		with:100%;
	}
	
	.ficha-table th {
		width: 100px;
		text-align: right;
		vertical-align: top;
		padding: 2px;
		color:black;
		white-space: nowrap;
	}
	.ficha-table td {
		width: *;
		text-align: left;
		vertical-align: top;
		padding: 2px;
		color:#0000ff;
	}
	.datepicker {
			z-index:1051 !important;
		}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		//global $user_username, $user_prodfy_cliente_id, $user_plano_codigo
		//;
		
		
		####
		# Carrega dados do modulo
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			####
			# INPUT-SELECT-LIST :: LOTE
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idlote` as value, concat(upper(AA.`codigo`),' - ', upper(M.`nome_interno`)) as txt
	       FROM `LOTE` AA
	 INNER JOIN `SYSTEM_CLIENTE` C
	         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
	  LEFT JOIN `MUDA` M
	         ON M.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
	        AND M.`idMUDA` = AA.`idMUDA`
	      WHERE AA.`idSYSTEM_CLIENTE` = ?
	   ORDER BY 1"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_INPUTSELECTLIST_VALUE_LOTE, $o_INPUTSELECTLIST_TXT_LOTE);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_LOTE = "";
				}
				else
				{
					$INPUT_SELECT_LIST_LOTE = "";
					while($stmt->fetch())
					{
						$INPUT_SELECT_LIST_LOTE .= "<option value='".$o_INPUTSELECTLIST_VALUE_LOTE."'>".$o_INPUTSELECTLIST_TXT_LOTE."</option>";
					}
					
				}
				
			}
			
			####
			# INPUT-SELECT-LIST :: ETIQUETA
			####
			
			$tmp = explode("-", $SID_CTRL->getIDIOMA());
			$PAIS = $tmp[1];
			mb_strtoupper($PAIS,"UTF-8");
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT upper(AA.`codigo`) as codigo, 
              AA.`descr_ptbr` as descr_ptbr,
              AA.`descr_enus` as descr_enus,
              AA.`descr_eses` as descr_eses,
              AA.`colunas` as cols,
              AA.`linhas` as rows,
              AA.`unidades` as unid
         FROM `ETIQUETAS` AA
        WHERE AA.`pais` = ?
	   ORDER BY 1"
			)) 
			{
				//$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				$sql_PAIS   = $mysqli->escape_String($PAIS);
				
				//
				$stmt->bind_param('s', $sql_PAIS);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_ETQ_COD, $o_ETQ_DESCR_PTBR, $o_ETQ_ENUS, $o_ETQ_ESES, $o_ETQ_COLS, $o_ETQ_ROWS, $o_ETQ_UNITS);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$INPUT_SELECT_LIST_ETIQUETA = "";
				}
				else
				{
					$INPUT_SELECT_LIST_ETIQUETA = "";
					while($stmt->fetch())
					{
						switch($SID_CTRL->getIDIOMA())
						{
							case 'en-us':
								$o_DESCR = $o_ETQ_DESCR_ENUS." (".$o_ETQ_COLS."C x ".$o_ETQ_ROWS."R) - ".$o_ETQ_UNITS." labels";
								break;
							case 'es-es':
								$o_DESCR = $o_ETQ_DESCR_ESES." (".$o_ETQ_COLS."C x ".$o_ETQ_ROWS."L) - ".$o_ETQ_UNITS." etiquetas";
								break;
							case 'pt-br':
							default:
								$o_DESCR = $o_ETQ_DESCR_PTBR." (".$o_ETQ_COLS."C x ".$o_ETQ_ROWS."L) - ".$o_ETQ_UNITS." etiquetas";
								break;
						}
						$INPUT_SELECT_LIST_ETIQUETA .= "<option value='".$o_ETQ_COD."'>".$o_DESCR."</option>";
					}
					
				}
				
			}
		}
		
		
		//$link_relat_prod_guia_inventario = '<li><a id="relat_prod_1" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
//		$link_relat_prod_guia_inventario = '<li><a href="javascript:$(this).trigger(\'f_show_relat\',[\'1\']);" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
		
		//$link_relat_prod_guia_inventario = '<li><a id="lnk_relat1" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_RELAT_ETIQUETAS_RELATORIOS; ?></a></li>
								<li class="active"><span><?php echo TXT_RELAT_ETIQUETAS_ETIQUETAS; ?></span></li>
							</ol></h2>
						<ul class="nav navbar-right panel_toolbox">
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
					<!-- /main_tabe -->
					<div class="col-xs-12 col-sm-9 col-centered">
						<h3 style="color:#434a54;"><span class="fa fa-qrcode" aria-hidden="true"></span> Impressão de Etiquetas QR</h3>
						<div class="clearfix"></div>
						<p align="justify" style="color:#434a54; font-size: 120%;">Preencha o formulário abaixo e clique em GERAR.</p>
						<p align="justify" style="color:#434a54; font-size: 120%;">Obs.: Campos com um <span style="color:#ff0000;">*</span> são obrigatórios</p>
						
						<!-- Begin | Register Form -->
						<form id="cad-form">
							<!-- <input id="cad_username" value="<?php echo $o_USERNAME; ?>" type=hidden> -->
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_LOTE; ?>:</b>
								</span>
								<select id="reg_lote" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value=""></option>
									<?php echo $INPUT_SELECT_LIST_LOTE; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_ETIQUETA; ?>:</b>
								</span>
								<select id="reg_etiqueta" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value=""></option>
									<?php echo $INPUT_SELECT_LIST_ETIQUETA; ?>
									<!-- <option value="PIMACO-A4251">Pimaco A4251 (5C x 13L) - 65 unidades</option> -->
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_IMPRESSAO; ?>:</b>
								</span>
								<select id="reg_impressao" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="1">Padrão</option>
									<option value="2">Posição Específica</option>
								</select>
							</div>
							
							<div class="input-group" id="impr_modo2_1">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_IMPRESSAO_COLUNA; ?>:</b>
								</span>
								<input id="reg_impr_col" type="text" maxlength=45 class="form-control numeric" 
								value="" 
								data-toggle="tooltip" data-placement="auto right" title="<?php echo TXT_RELAT_ETIQUETAS_IMPRESSAO_COLUNA_TOOLTIP; ?>">
							</div>
							<div class="input-group" id="impr_modo2_2">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_IMPRESSAO_LINHA; ?>:</b>
								</span>
								<input id="reg_impr_row" type="text" maxlength=45 class="form-control numeric" 
								value="" 
								data-toggle="tooltip" data-placement="auto right" title="<?php echo TXT_RELAT_ETIQUETAS_IMPRESSAO_LINHA_TOOLTIP; ?>">
							</div>
							
							
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_QTDE_IMPRESSAO; ?>:</b>
								</span>
								<input id="reg_qtde_impr" type="text" maxlength=45 class="form-control numeric" required
								value="" 
								data-toggle="tooltip" data-placement="auto right" title="<?php echo TXT_RELAT_ETIQUETAS_QTDE_IMPRESSAO_TOOLTIP; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_QTDE_CODIFICADA; ?>:</b>
								</span>
								<input id="reg_qtde" type="text" maxlength=45 class="form-control numeric" required
								value="1"
								data-toggle="tooltip" data-placement="auto right" title="<?php echo TXT_RELAT_ETIQUETAS_QTDE_CODIFICADA_TOOLTIP; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_RELAT_ETIQUETAS_BORDA; ?>:</b>
								</span>
								<select id="reg_borda" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="0">Não</option>
									<option value="1">Sim</option>
								</select>
							</div>
							<div>
								<button id="bt_gerar" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_GENERATE; ?></button>
							</div>
						</form>
						<!-- End | cad Form -->
						
					</div><!--/.col-xs-12.col-sm-9-->
					
					
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->



<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<!-- <script src="js/jspdf.min.js"></script>
		<script src="js/jspdf.plugin.autotable.prodfy.js"></script> -->
		<script src="js/view-relat-etiquetas.js" charset="UTF-8"></script>
		

<?php
	} 
?>
