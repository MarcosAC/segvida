<?php
#################################################################################
## KONDOO
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
	####
	# Obs
	####
	# - Colaboradores tem acesso restrito ao sistema, meramente para servir de login ao sistema para colaboracao;
	# - Geralmente são os funcionários da empresa que devem ter acesso limitado ao sistema para desempenharem atividades específicas;
	# - Colaboradores só têm acesso ao sistema depois de serem homologados pelo cliente Prodfy;
	# - O acesso dos colaboradores ao sistema é condicionado pelo cliente Prodfy, que determina que setores do sistema o username tem acesso;
	# - Apenas usernames de colaboradores podem ser homologados como colaboradores de um cliente Prodfy;
	
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Planos de Aquisição" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="css/offcanvas.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="css/usuarios.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			* {
			    box-sizing: border-box;
			}
			
			.columns {
			    float: left;
			    width: 33.3%;
			    padding: 8px;
			}
			
			.price {
			    list-style-type: none;
			    border: 1px solid #eee;
			    margin: 0;
			    padding: 0;
			    -webkit-transition: 0.3s;
			    transition: 0.3s;
			}
			
			.price:hover {
			    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
			}
			
			.price .header {
			    background-color: #111;
			    color: white;
			    font-size: 20px;
			}
			
			.price li {
			    border-bottom: 1px solid #eee;
			    padding: 10px;
			    text-align: center;
			}
			
			.price .grey {
			    background-color: #eee;
			    font-size: 35px;
			}
			
			.button {
			    background-color: #2b71c4;
			    border: none;
			    color: white;
			    padding: 10px 25px;
			    text-align: center;
			    text-decoration: none;
			    font-size: 18px;
			}
			
			@media only screen and (max-width: 600px) {
			    .columns {
			        width: 100%;
			    }
			}
			
			select:not([multiple]) {
			    -webkit-appearance: none;
			    -moz-appearance: none;
			    background-position: right 50%;
			    background-repeat: no-repeat;
			    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
			    padding: .5em;
			    padding-right: 1.5em;
			}

			
		</style>
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO; ?></a></li>
						<li><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO; ?></a></a></li>
						<li><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS; ?></a></a></li>
						<li class="active"><a href="usuarios.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_USERS; ?></a></a></li>
						<li><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE; ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO; ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">
				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-users" aria-hidden="true"></span> Usuários</h3>
					</div>
					
					<p class="text-justify">O sistema Prodfy Mudas foi criado de forma a permitir que outros usuários possam ter acesso ao sistema. Um usuário pode ser configurado como um <b>Colaborador</b>, aquele que terá acesso limitado ao sistema administrativo, alimentando informações, realizando atividades, etc. Um usuário também pode ser configurado como um <b>Cliente Final</b>, que terá acesso a um painel especial onde informações oficiais serão disponibilizadas para aceso, como Relatórios Oficiais, Estatísticas, Indicadores, Mensagens e até mesmo Cobranças On-Line.</p>
					
					<br/>
					<center>
					<button type="button" class="button" 
					role="button" data-toggle="modal" data-target="#user-modal"
					onclick="javascript:$('#reg_captcha_refresh_btn').trigger('click');"
					><?php echo TXT_PG_USUARIOS_BT_CRIAR_CONTA; ?></button>
					</center>
					<br/>
					<br/>
					
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
		</div><!--/container-->
		
		<!-- BEGIN # MODAL USERS -->
		<div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" align="center">
						<img src="img/logo_login_200x59.png">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
					</div>
					
					<!-- Begin # DIV Form -->
					<div id="div-forms">
						<!-- Begin | Form -->
						<form id="reg-form">
							<div class="modal-body">
								<div id="div-reg-msg">
									<div id="div-label-type">CRIAR CONTA USUÁRIO</div>
									<div id="icon-reg-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-reg-msg">Campos com * são obrigatórios</span>
									</div>
									<input id="reg_nome"        type="text" maxlength=45  class="form-control textonly_az input-upper" placeholder="*Nome" required>
									<input id="reg_sobrenome"   type="text" maxlength=45  class="form-control textonly_az input-upper" placeholder="*Sobrenome" required>
									<input id="reg_email"       type="text" maxlength=180 class="form-control input-lower"             placeholder="*E-Mail" required>
									<input id="reg_cpf"         type="text" maxlength=14  class="form-control numeric"     placeholder="*CPF" required>
									<input id="reg_cnpj"        type="text" maxlength=18  class="form-control numeric"     placeholder="CNPJ">
									<input id="reg_razaosocial" type="text" maxlength=180 class="form-control textonly input-upper"    placeholder="Razão Social">
									<input id="reg_fonefixo1"   type="text" maxlength=15  class="form-control numeric"     placeholder="*Telefone Fixo" required
									data-toggle="tooltip" data-placement="auto right" title="digite o número do seu telefone fixo com o código de área Ex.: 3138852424">
									<input id="reg_celular1"    type="text" maxlength=15  class="form-control numeric" style="margin-bottom: 10px;" placeholder="Celular"
									data-toggle="tooltip" data-placement="auto left" title="Digite o número do seu celular com o código de área Ex.: 31999474426">
									<select id="reg_pais" class="form-control" style="margin-top: 0px; margin-bottom: 10px;" required>
										<!-- <option value="" selected hidden>*País</option> -->
										<option value="BR" selected>Brasil</option>
									</select>
									<select id="reg_idioma" class="form-control" style="margin-top: 0px; margin-bottom: 10px;" required>
										<!-- <option value="" selected hidden>*Idioma</option> -->
										<option value="pt-br" selected>Português Brasil</option>
										<!--<option value="en-us" >US English</option>
										<option value="es-es" >Español</option>-->
									</select>
									<input id="reg_cep" type="text" maxlength=10 class="form-control numeric" style="margin-top: 0px;" placeholder="*CEP" required
									data-toggle="tooltip" data-placement="auto bottom" title="Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.">
									<input id="reg_cep_uf" type="hidden">
									<input id="reg_cep_cidade_codigo" type="hidden">
									<select id="reg_uf" class="form-control" required>
										<option value="" selected hidden>*Estado</option><option value="AC">ACRE</option><option value="AL">ALAGOAS</option><option value="AM">AMAZONAS</option><option value="AP">AMAPÁ</option><option value="BA">BAHIA</option><option value="CE">CEARÁ</option><option value="DF">DISTRITO FEDERAL</option><option value="ES">ESPÍRITO SANTO</option><option value="GO">GOIÁS</option><option value="MA">MARANHÃO</option><option value="MG">MINAS GERAIS</option><option value="MS">MATO GROSSO DO SUL</option><option value="MT">MATO GROSSO</option><option value="PA">PARÁ</option><option value="PB">PARAÍBA</option><option value="PE">PERNAMBUCO</option><option value="PI">PIAUÍ</option><option value="PR">PARANÁ</option><option value="RJ">RIO DE JANEIRO</option><option value="RN">RIO GRANDE DO NORTE</option><option value="RO">RONDÔNIA</option><option value="RR">RORAIMA</option><option value="RS">RIO GRANDE DO SUL</option><option value="SC">SANTA CATARINA</option><option value="SE">SERGIPE</option><option value="SP">SÃO PAULO</option><option value="TO">TOCANTINS</option>
									</select>
									<span id='cidade_panel'>
										<select id="reg_cidade" class="form-control" required>
											<option value="" selected hidden>*Cidade (Escolha Um Estado Primeiro)</option>
										</select>
									</span>
									<input id="reg_bairro"     type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="*Bairro" required>
									<input id="reg_end"        type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="*Endereço" required>
									<input id="reg_num"        type="text"     maxlength=30  class="form-control textonly input-upper"    placeholder="*Número" required>
									<input id="reg_compl"      type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="Complemento">
									<input id="reg_username"   type="text"     maxlength=30  class="form-control logintext input-lower"   placeholder="*Usuário (de 6 a 30 dígitos)" required 
									data-toggle="tooltip" data-placement="auto top" title="O campo USUÁRIO deve ter de 6 a 30 dígitos. Deve conter apenas letras, números e underlines.">
									<input id="reg_password"   type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="*Senha" required
									data-toggle="tooltip" data-placement="auto left" title="A senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula.">
									<input id="reg_password2"  type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="*Senha para confirmação" required
									data-toggle="tooltip" data-placement="auto right" title="Entre exatamente com a mesma senha digitada para confirmação.">
									<div class="input-group" style="margin-bottom: 10px;">
										<span class="input-group-addon">
											<img id='reg_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="reg_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="reg_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="Digite o Código" required>
									</div>
									<div class="checkbox" style="margin-top: 1px;">
										<label>
										<input id="reg_termos" type="checkbox" required> Li e aceito o <a href="docs/termo-de-servico.pdf" target="_blank">termo de serviço</a>.
										</label>
									</div>
								</div>
							<div class="modal-footer">
								<div>
									<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block">Registrar</button>
								</div>
							</div>
						</form>
						<!-- End | Form -->
						
					</div>
					<!-- End # DIV Form -->
					
				</div>
			</div>
		</div>
		<!-- END # MODAL USERS -->
		
		
		
		
		
		
		
		
<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" data-backdrop="static" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<!--<div class="modal-header">
			</div>-->
			<div class="modal-body">
				<p class="text-center center-block"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color=#ffffff><b id='loadingBoxText'><?php echo TXT_MODAL_MSG_CARREGANDO ?></b></font></p>
			</div>
			<!--<div class="modal-footer" id="msgBoxFooter">
			</div>-->
		</div>
	</div>
</div>
<!-- /msgBox -->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='js/jquery.storage.js'></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script src="js/sha512.js"></script>
		<script src="js/usuarios.js"></script>
		
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>