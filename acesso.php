<?php
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
	
#	<div class="columns">
#							<ul class="price">
#								<li class="header" style="background-color:#64a5de">USUÁRIO</li>
#								<li class="grey"><i class="fa fa-user-circle-o fa-5x"></i></li>
#								<li class="grey"><small>(Cliente Final)</small></li>
#								<!-- <li class="grey">Acesso ao painel do Cliente Final.</li> -->
#								<!-- <li class="grey"><small>(Informações, Lotes de Produção, Relatórios, Cobranças, etc)</small></li> -->
#								<li class="grey">
#									<button id='btn_show_login_user' type="button" class="btn btn-yellow" style="padding:10px 25px;"
#									role="button" data-toggle="modal" data-target="#usr-login-modal"
#									onclick="javascript:$(this).trigger('f_show_login',['usr']);"
#									><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
#								</li>
#							</ul>
#						</div>
#						
	
	
	
?>	

<?php 
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Acesso ao Sistema" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="css/offcanvas.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="css/acesso_form.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		
		<style>
			* {
			    box-sizing: border-box;
			}
			
			.columns {
			    float: left;
			    width: 33.3%;
			    padding: 8px;
			}
			
			.price {
			    list-style-type: none;
			    border: 1px solid #eee;
			    margin: 0;
			    padding: 0;
			    -webkit-transition: 0.3s;
			    transition: 0.3s;
			    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2);
			}
			
			//.price:hover {
			//    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
			//}
			
			.price .header {
			    background-color: #111;
			    color: white;
			    font-size: 20px;
			}
			
			.price li {
			    border-bottom: 1px solid #eee;
			    padding: 10px;
			    text-align: center;
			}
			
			.price .grey {
			    background-color: #eee;
			    font-size: 18px;
			}
			
			@media only screen and (max-width: 600px) {
			    .columns {
			        width: 100%;
			    }
			}
			
		</style>
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO; ?></a></li>
						<li class="active"><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO; ?></a></a></li>
						<li><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS; ?></a></a></li>
						<li><a href="usuarios.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_USERS; ?></a></a></li>
						<li><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE; ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO; ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">
				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-users" aria-hidden="true"></span> Acesso ao Sistema</h3>
					</div>
					
					<div class="row text-center center-block">
						<div class="columns">
							<p class="text-justify">O Prodfy Mudas é uma plataforma de controle de produção integrada, permitindo que Administradores, Colaboradores e Clientes Finais possam acessar suas respectivas ferramentas e recursos com toda comodidade. Veja qual tipo de acesso desejado e efetue seu login.</p>
						</div>
						<div class="columns">
							<ul class="price">
								<li class="header" style="background-color:#de9846">ADMINISTRATIVO</li>
								<li class="grey"><i class="fa fa-id-badge fa-5x"></i></li>
								<!-- <li class="grey">Acesso administrativo ao sistema de controle de produção.</li> -->
								<li class="grey"><small>(Admin/Colaborador)</small></li>
								<li class="grey">
									<button id='btn_show_login_adm' type="button" class="btn btn-yellow" style="padding:10px 25px;"
									role="button" data-toggle="modal" data-target="#adm-login-modal"
									onclick="javascript:$(this).trigger('f_show_login',['adm']);"
									><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
								</li>
							</ul>
						</div>
						
						
					</div><!--/row-->
					
					
					
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
		</div><!--/container-->
		
		
		<!-- BEGIN # MODAL LOGIN ADM -->
		<div class="modal fade" id="adm-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" align="center">
						<!-- <img class="img-circle" id="img_logo" src="img/login_logo_200x200.jpg"> -->
						<img src="img/logo_login_200x59.png">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
					</div>
					
					<!-- Begin # DIV Form -->
					<div id="div-forms-adm">
						
						<!-- Begin # Login Form -->
						<form id="adm-login-form">
							<div class="modal-body">
								<div id="div-login-msg">
									<div id="div-label-login-type">ACESSO ADMINISTRADOR</div>
									<div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-login-msg"><span class="fa fa-cog" aria-hidden="true"></span> Entre com o seu usuário e senha</span>
									</div>
									<input id="adm_login_username" type="text"     maxlength=30 class="form-control logintext input-lower" placeholder="*Usuário" >
									<input id="adm_login_password" type="password" maxlength=30 class="form-control"           placeholder="*Senha" required>
									<div class="checkbox" style="margin-bottom: 10px;">
										<label>
											<input id="adm_login_remember" type="checkbox"> Lembrar
										</label>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<img id='adm_login_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="adm_login_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="adm_login_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="*Digite o Código" required>
									</div>
							</div>
							<div class="modal-footer">
								<div>
									<button id="bt_login_adm" type="button" class="btn btn-yellow btn-lg btn-block">Entrar</button>
								</div>
								<div class="text-center">
									<button id="adm_login_lost_btn" type="button" class="btn btn-link"><span class="fa fa-question-circle" aria-hidden="true"></span>&nbsp;Esqueceu a Senha?</button>
								</div>
							</div>
						</form>
						<!-- End # Login Form -->
						
						<!-- Begin | Lost Password Form -->
						<form id="adm-lost-form" style="display:none;">
							<div class="modal-body">
								<div id="div-lost-msg">
									<div id="div-label-login-type">RECUPERA SENHA ADMINISTRADOR</div>
									<div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-lost-msg"><span class="fa fa-cog" aria-hidden="true"></span> Digite seu username e e-mail.</span>
									</div>
									<input id="adm_lost_username" type="text" maxlength=30  class="form-control logintext input-lower" placeholder="*Usuário" >
									<input id="adm_lost_email" type="text"    maxlength=180 class="form-control input-lower" style="margin-bottom: 10px;" placeholder="*E-Mail" required>
									<div class="input-group">
										<span class="input-group-addon">
											<img id='adm_lost_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="adm_lost_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="adm_lost_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="*Digite o Código" required>
									</div>
								</div>
								<div class="modal-footer">
									<div>
										<button id="bt_adm_lost" type="button" class="btn btn-yellow btn-lg btn-block">Enviar</button>
									</div>
								<div class="text-center">
									<button id="adm_lost_login_btn" type="button" class="btn btn-link"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log In</button>
								</div>
							</div>
						</form>
						<!-- End | Lost Password Form -->
						
					</div>
					<!-- End # DIV Form -->
					
				</div>
			</div>
		</div>
		<!-- END # MODAL LOGIN ADM -->
		
		
		<!-- BEGIN # MODAL LOGIN USR -->
		<div class="modal fade" id="usr-login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" align="center">
						<img src="img/logo_login_200x59.png">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
					</div>
					
					<!-- Begin # DIV Form -->
					<div id="div-forms-usr">
						
						<!-- Begin # Login Form -->
						<form id="usr-login-form">
							<div class="modal-body">
								<div id="div-login-msg">
									<div id="div-label-login-type">ACESSO CLINTE FINAL</div>
									<div id="icon-login-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-login-msg"><span class="fa fa-user" aria-hidden="true"></span> Entre com o seu usuário e senha</span>
									</div>
									<input id="usr_login_username" type="text"     maxlength=30 class="form-control logintext" placeholder="*Usuário" >
									<input id="usr_login_password" type="password" maxlength=30 class="form-control"           placeholder="*Senha" required>
									<div class="checkbox" style="margin-bottom: 10px;">
										<label>
											<input id="usr_login_remember" type="checkbox"> Lembrar
										</label>
									</div>
									<div class="input-group">
										<span class="input-group-addon">
											<img id='usr_login_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="usr_login_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="usr_login_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="*Digite o Código" required>
									</div>
							</div>
							<div class="modal-footer">
								<div>
									<button id="bt_login_usr" type="button" class="btn btn-yellow btn-lg btn-block">Entrar</button>
								</div>
								<div class="text-center">
									<button id="usr_login_lost_btn" type="button" class="btn btn-link"><span class="fa fa-question-circle" aria-hidden="true"></span>&nbsp;Esqueceu a Senha?</button>
								</div>
							</div>
						</form>
						<!-- End # Login Form -->
						
						<!-- Begin | Lost Password Form -->
						<form id="usr-lost-form" style="display:none;">
							<div class="modal-body">
								<div id="div-lost-msg">
									<div id="div-label-login-type">RECUPERA SENHA USUÁRIO</div>
									<div id="icon-lost-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-lost-msg"><span class="fa fa-user" aria-hidden="true"></span> Digite seu usuário e e-mail.</span>
									</div>
									<input id="usr_lost_username" type="text" maxlength=30  class="form-control logintext input-lower" placeholder="*Usuário" >
									<input id="usr_lost_email" type="text"    maxlength=180 class="form-control input-lower" style="margin-bottom: 10px;" placeholder="*E-Mail" required>
									<div class="input-group">
										<span class="input-group-addon">
											<img id='usr_lost_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="usr_lost_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="usr_lost_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="*Digite o Código" required>
									</div>
								</div>
								<div class="modal-footer">
									<div>
										<button id="bt_usr_lost" type="button" class="btn btn-yellow btn-lg btn-block">Enviar</button>
									</div>
								<div class="text-center">
									<button id="usr_lost_login_btn" type="button" class="btn btn-link"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log In</button>
								</div>
							</div>
						</form>
						<!-- End | Lost Password Form -->
						
					</div>
					<!-- End # DIV Form -->
					
				</div>
			</div>
		</div>
		<!-- END # MODAL LOGIN USR -->
		
		
<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal"><?php echo TXT_MODAL_BTN_FECHAR ?></button>
				<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" data-backdrop="static" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<!--<div class="modal-header">
			</div>-->
			<div class="modal-body">
				<p class="text-center center-block"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color=#ffffff><b id='loadingBoxText'><?php echo TXT_MODAL_MSG_CARREGANDO ?></b></font></p>
			</div>
			<!--<div class="modal-footer" id="msgBoxFooter">
			</div>-->
		</div>
	</div>
</div>
<!-- /msgBox -->

		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='js/jquery.storage.js'></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script src="js/offcanvas.js"></script>
		<script src="js/sha512.js"></script>
		<script src="js/acesso_form.js"></script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>