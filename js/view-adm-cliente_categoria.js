/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-adm-cliente_categoria.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  # Criacao: 01/09/2017 18:00:00
  ################################################################################# */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	

	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=adm-cliente_categoria');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	

	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	

	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	

	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	

	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	

	////////////////////// FORMATACAO NUMERICA ~ START
	//SPRINTF javascript equivalent
	function sprintf() 
	{
		var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
	  var a = arguments;
	  var i = 0;
	  var format = a[i++];
	
	  // pad()
	  var pad = function(str, len, chr, leftJustify) {
	    if (!chr) {
	      chr = ' ';
	    }
	    var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
	      .join(chr);
	    return leftJustify ? str + padding : padding + str;
	  };
	
	  // justify()
	  var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
	    var diff = minWidth - value.length;
	    if (diff > 0) {
	      if (leftJustify || !zeroPad) {
	        value = pad(value, minWidth, customPadChar, leftJustify);
	      } else {
	        value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
	      }
	    }
	    return value;
	  };
	
	  // formatBaseX()
	  var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
	    // Note: casts negative numbers to positive ones
	    var number = value >>> 0;
	    prefix = prefix && number && {
	      '2': '0b',
	      '8': '0',
	      '16': '0x'
	    }[base] || '';
	    value = prefix + pad(number.toString(base), precision || 0, '0', false);
	    return justify(value, prefix, leftJustify, minWidth, zeroPad);
	  };
	
	  // formatString()
	  var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
	    if (precision != null) {
	      value = value.slice(0, precision);
	    }
	    return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
	  };
	
	  // doFormat()
	  var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
	    var number, prefix, method, textTransform, value;
	
	    if (substring === '%%') {
	      return '%';
	    }
	
	    // parse flags
	    var leftJustify = false;
	    var positivePrefix = '';
	    var zeroPad = false;
	    var prefixBaseX = false;
	    var customPadChar = ' ';
	    var flagsl = flags.length;
	    for (var j = 0; flags && j < flagsl; j++) {
	      switch (flags.charAt(j)) {
	        case ' ':
	          positivePrefix = ' ';
	          break;
	        case '+':
	          positivePrefix = '+';
	          break;
	        case '-':
	          leftJustify = true;
	          break;
	        case "'":
	          customPadChar = flags.charAt(j + 1);
	          break;
	        case '0':
	          zeroPad = true;
	          customPadChar = '0';
	          break;
	        case '#':
	          prefixBaseX = true;
	          break;
	      }
	    }
	
	    // parameters may be null, undefined, empty-string or real valued
	    // we want to ignore null, undefined and empty-string values
	    if (!minWidth) {
	      minWidth = 0;
	    } else if (minWidth === '*') {
	      minWidth = +a[i++];
	    } else if (minWidth.charAt(0) == '*') {
	      minWidth = +a[minWidth.slice(1, -1)];
	    } else {
	      minWidth = +minWidth;
	    }
	
	    // Note: undocumented perl feature:
	    if (minWidth < 0) {
	      minWidth = -minWidth;
	      leftJustify = true;
	    }
	
	    if (!isFinite(minWidth)) {
	      throw new Error('sprintf: (minimum-)width must be finite');
	    }
	
	    if (!precision) {
	      precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
	    } else if (precision === '*') {
	      precision = +a[i++];
	    } else if (precision.charAt(0) == '*') {
	      precision = +a[precision.slice(1, -1)];
	    } else {
	      precision = +precision;
	    }
	
	    // grab value using valueIndex if required?
	    value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];
	
	    switch (type) {
	      case 's':
	        return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
	      case 'c':
	        return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
	      case 'b':
	        return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'o':
	        return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'x':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'X':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
	          .toUpperCase();
	      case 'u':
	        return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'i':
	      case 'd':
	        number = +value || 0;
	        number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
	        prefix = number < 0 ? '-' : positivePrefix;
	        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad);
	      case 'e':
	      case 'E':
	      case 'f': // Should handle locales (as per setlocale)
	      case 'F':
	      case 'g':
	      case 'G':
	        number = +value;
	        prefix = number < 0 ? '-' : positivePrefix;
	        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
	        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
	        value = prefix + Math.abs(number)[method](precision);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
	      default:
	        return substring;
	    }
	  };
	
	  return format.replace(regex, doFormat);
	}
	// Formata valor de USD para BRL
	function currency_format (number, decimals, dec_point, thousands_sep) 
	{
		var n = number, prec = decimals;
		
		var toFixedFix = function (n,prec) {
		    var k = Math.pow(10,prec);
		    return (Math.round(n*k)/k).toString();
		};
		
		n = !isFinite(+n) ? 0 : +n;
		prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		var abs = toFixedFix(Math.abs(n), prec);
		var _, i;
		
		if (abs >= 1000) {
		    _ = abs.split(/\D/);
		    i = _[0].length % 3 || 3;
		
		    _[0] = s.slice(0,i + (n < 0)) +
		          _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		    s = _.join(dec);
		} else {
		    s = s.replace('.', dec);
		}
		
		var decPos = s.indexOf(dec);
		if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
		    s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		}
		else if (prec >= 1 && decPos === -1) {
		    s += dec+new Array(prec).join(0)+'0';
		}
		return s; 
	}
	// Formata currency on type
	function currencyFormat(fld, milSep, decSep, e) 
	{
		var sep = 0;
		var key = '';
		var i = j = 0;
		var len = len2 = 0;
		var strCheck = '0123456789';
		var aux = aux2 = '';
		var whichCode = (window.Event) ? e.which : e.keyCode;
		//
		if (whichCode == 13) return true;  // Enter
		if (whichCode == 8) return true;  // Delete
		key = String.fromCharCode(whichCode);  // Get key value from key code
		if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
		len = fld.value.length;
		for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
		for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
			  aux2 = '';
			  for (j = 0, i = len - 3; i >= 0; i--) {
			    if (j == 3) {
			      aux2 += milSep;
			      j = 0;
			    }//endif
			    aux2 += aux.charAt(i);
			    j++;
			  }//endfor
			  fld.value = '';
			  len2 = aux2.length;
			  for (i = len2 - 1; i >= 0; i--)
			  fld.value += aux2.charAt(i);
			  fld.value += decSep + aux.substr(len - 2, len);
			}//endif
		return false;
	}
	// Formata autoNumeric Defaults
	$.extend($.fn.autoNumeric.defaults, {aSign: 'R$ ', pSign:'p', aSep: '.', aDec: ','});
	// Format datepicker Defaults
	$.fn.datepicker.defaults.container='#cad-modal';
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	////////////////////// FORMATACAO NUMERICA ~ STOP
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_comma", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (44 <= event.which && event.which <= 44) || // ,(virgula)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	

	////////////////////// MSGBOX ~ START
	$(document).on("f_show_msgbox", function(e,_txt,_title,_type)
	{
		show_msgbox(_txt,_title,_type);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'ficha':
				$("#msgBoxIconFicha").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'pt-br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'en-us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'es-es':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
	}
	////////////////////// LOADINGBOX ~ STOP
	
	
	////////////////////// Datatable ~ START
	var myTable = $('#main-datatable').DataTable(
	{
		dom: "lBfrtip",
		buttons: [
			{
				extend: "copy",
				className: "btn-sm",
				text: "<i class='fa fa-clipboard fa-lg'></i>",
				exportOptions: { columns: [ 2 ] }
			},
			{
				extend: "csv",
				className: "btn-sm",
				text: "<i class='fa fa-file-text-o fa-lg'></i>",
				exportOptions: { columns: [ 2 ] }
			},
			{
				extend: "excel",
				className: "btn-sm",
				text: "<i class='fa fa-file-excel-o fa-lg'></i>",
				title: "Prodfy Mudas - Administrativo",
				exportOptions: { columns: [ 2 ] }
			},
//			{
//				extend: "pdf",
//				className: "btn-sm",
//				text: "<i class='fa fa-file-pdf-o fa-lg'></i>",
//				title: "Prodfy Mudas - Administrativo",
//				exportOptions: { columns: [ 2 ] }
//			},
			{
				extend: "print",
				className: "btn-sm",
				text: "<i class='fa fa-print fa-lg'></i>",
				title: "Prodfy Mudas - Administrativo",
				exportOptions: { columns: [ 2 ] }
			},
		],
		responsive: true,
		fixedHeader: true,
		select: true,
		//info:true,
		language: 
		{
			    sEmptyTable: "Nenhum registro encontrado",
			          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			     sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
			  sInfoFiltered: "(Filtrados de _MAX_ registros)",
			   sInfoPostFix: "",
			 sInfoThousands: ".",
			    sLengthMenu: "_MENU_ Resultados por página",
			sLoadingRecords: "Carregando...",
			    sProcessing: "Processando...",
			   sZeroRecords: "Nenhum registro encontrado",
			        sSearch: "Pesquisar",
			      oPaginate: {
													    sNext: "<i class='fa fa-arrow-circle-right fa-lg'></i>",
													sPrevious: "<i class='fa fa-arrow-circle-left fa-lg'></i>",
													   sFirst: "Primeiro",
													    sLast: "Último"
												},
								oAria: {
													sSortAscending: ": Ordenar colunas de forma ascendente",
													sSortDescending: ": Ordenar colunas de forma descendente"
												}
												,
							buttons: {
												  copyTitle: 'Copiado para o clipboard',
												   copyKeys: 'Pressione <i>ctrl</i> ou <i>⌘</i> + <i>C</i> para copiar os dados da tabela para o clipboard. <br><br>Para cancelar, clique sobre esta mensagem ou pressione ESC.',
												copySuccess: {
																				_: 'Copiados %d registros',
																				1: 'Copiado 1 registro'
																		}
												}
		
		},
		
		'order': [[ 2, 'asc' ]],
		'columnDefs': [
			{ 
				'targets': 0,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '10px',
				'render': function (data,type, full, meta){ return '<div class="checkbox checkbox-primary"><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></div>'; }
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '50px'
			}
		]
		
	});

	// Handle click on "Select all" control
	$('#select-all').on('click', function()
	{
		// Get all rows with search applied
		//var rows = myTable.rows({ 'search': 'applied' }).nodes();
		var rows = myTable.rows({ 'page':'current', 'filter': 'applied' }).nodes();
		// Check/uncheck checkboxes for all rows in the table
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});
	
	// Handle click on checkbox to set state of "Select all" control
	$('#main-datatable tbody').on('change', 'input[type="checkbox"]', function()
	{
		// If checkbox is not checked
		if(!this.checked)
		{
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if(el && el.checked && ('indeterminate' in el))
			{
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});
	
	$('#main-datatable').on( 'page.dt', function () 
	{
		$('#select-all').prop('checked', false);
	});
	////////////////////// Datatable ~ STOP
	

	////////////////////// VALIDACOES DE FORM ~ START
	var onlyNumbersReg = /^\\d*$/;
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var apenasLetrasNumerosUnderlinesRegex = /^\w+$/;//apenas letras, numeros e underlines
	var apenasNumerosRegex = /^\d*$/;
	// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
	var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	var usernameRegex = /^\w+$/;//apenas letras, numeros e underlines
	var isDecimal2Regex = /^\d+\.\d{0,2}$/;
	var isDecimal3Regex = /^\d+\.\d{0,3}$/;
	var isDecimal4Regex = /^\d+\.\d{0,4}$/;
	var isValidPTBRCurrencyRegex = /\d{1,3}(?:\.\d{3})*?,\d{2}/;
	
	function isDate(str)
	{
		var parms = str.split(/[\.\-\/]/);
		var yyyy = parseInt(parms[2],10);
		var mm   = parseInt(parms[1],10);
		var dd   = parseInt(parms[0],10);
		var date = new Date(yyyy,mm-1,dd,0,0,0,0);
		return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
	}
	
	function getDataMySQL(_data)
	{
		if( isEmpty(_data) ){ return ''; }
		var _ret = '';
		var _tmp = _data.split("/"); 
		_ret = _tmp[2]+'-'+_tmp[1]+'-'+_tmp[0];
		return _ret;
	}
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	

	////////////////////// Processa Form ~ START
	//CATEGORIA
	$(document).on('blur', '#reg_categoria', function()
	{
		if( !frm_valida_categoria(this.value,true) ){ return false; }
	});
	function frm_valida_categoria(value,ret_msg)
	{
		var _tmp = value;
		if( _tmp.length > 45 )
		{
			if( ret_msg )
			{
				show_msgbox('CATEGORIA deve ter até 45 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}

	//on_change validations
	////////////////////// Processa Form ~ STOP
	

	////////////////////// Form ~ START
	var $formRegister = $('#cad-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	//Clear Inputs
	function clear_inputs()
	{
		$('#reg_categoria').val('');
	}
	// Registrar
	$(document).on("f_show_register", function(e)
	{
		$('#div-label-type').text('NOVO REGISTRO');
		$('#bt_register').show();
		$('#bt_save').hide();
		clear_inputs();
		$('#cad-modal').modal('show');
		return false;
	});
	$('#bt_register').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_categoria = $('#reg_categoria').val();
		
		//CATEGORIA
		if (isEmpty(rg_categoria)) { show_msgbox('O campo CATEGORIA é obrigatório!',' ','alert'); $('#reg_categoria').focus(); return false; }
		if (!isEmpty(rg_categoria) && rg_categoria.length > 45) { show_msgbox('CATEGORIA deve ter até 45 dígitos!',' ','alert'); $('#reg_categoria').focus(); return false; }
		
		//Formatacao Case
		rg_categoria = rg_categoria.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "2");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("categoria", rg_categoria);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						clear_inputs();
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						$('#cad-modal').scrollTop(0);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});


	// Editar
	$(document).on("f_show_edit", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de executar edição do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "6");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		$.ajax({
				 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], categoria:tmp[3]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#div-label-type').text('EDITAR REGISTRO');
						$('#bt_register').hide();
						$('#bt_save').show();
						$('#reg_id').val(_id);
						$('#reg_categoria').val(myRet.categoria);
						hideLoadingBox();
						$('#cad-modal').modal('show');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	$('#bt_save').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_id = $('#reg_id').val();
		var rg_categoria = $('#reg_categoria').val();
		
		//ID
		if (isEmpty(rg_id)) { show_msgbox('Incapaz de determinar qual registro deve ser editado!',' ','error'); return false; }
		//CATEGORIA
		if (isEmpty(rg_categoria)) { show_msgbox('O campo CATEGORIA é obrigatório!',' ','alert'); $('#reg_categoria').focus(); return false; }
		if (!isEmpty(rg_categoria) && rg_categoria.length > 45) { show_msgbox('CATEGORIA deve ter até 45 dígitos!',' ','alert'); $('#reg_categoria').focus(); return false; }
		
		//Formatacao Case
		rg_categoria = rg_categoria.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "3");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", rg_id);
		_postdata.append("categoria", rg_categoria);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						$('#cad-modal').modal('hide');
						clear_inputs();
						//Refresh datatable
						refresh_datatable();
						
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	// Refresh datatable
	function refresh_datatable()
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "99");
		_postdata.append("l", $.Storage.loadItem('language'));
		
		$.ajax({
				 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], btn_link:tmp[3], total_recs:tmp[4]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						myTable.clear();
						$("#select-all").prop('checked', false);
						myTable.rows.add( $(myRet.text) ).draw();
						
						//Atualiza add button
						$('#add_button_link').empty().append(myRet.btn_link);
						
						//Atualiza total de registros
						if(isEmpty(myRet.total_recs)){ myRet.total_recs = 0; }
						$('#datatable_total_recs').val(myRet.total_recs);
					}
					else
					{
						return false;
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//Ficha Consolidada
	$(document).on("f_show_ficha", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de carregar ficha do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "7");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		$.ajax({
				 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						hideLoadingBox();
						show_msgbox(myRet.text,'Ficha do Registro','ficha');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	//Delete Item
	$(document).on("f_show_del", function(e)
	{
		//Verifica se existe algum checkbox marcado para delecao
		var total_recs = $('#datatable_total_recs').val();
		if(isEmpty(total_recs)){ total_recs=0; }
		
		var allChks = [];
		$(".dt_checkbox:checked").each(function() 
		{ 
			if(!isEmpty($(this).attr('data-id'))){ allChks.push($(this).attr('data-id')); } 
		});
		
		if(allChks.length <= 0)
		{
			show_msgbox('Nenhum registro selecionado!',' ','alert');
			return false;
		}
		
		var confirmDiag = BootstrapDialog.confirm(
		{
			         title: 'ATENÇÃO!',
			       message: 'Os registros selecionados serão apagados. Confirma?',
			          type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			      closable: true, // <-- Default value is false
     //closeByBackdrop: false,
     //closeByKeyboard: false,
           draggable: false, // <-- Default value is false
			btnCancelLabel: 'Não', // <-- Default value is 'Cancel',
			    btnOKLabel: 'Sim', // <-- Default value is 'OK',
			    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
			      callback: function(result) 
			{
				// result will be true if button was click, while it will be false if users close the dialog directly.
				if(result) 
				{ 
					var delIDs = allChks.join(',');
					confirmDiag.close();
					
					//Processa delete dos registros selecionados
					//Formata postdata
					var _postdata = new FormData();
					_postdata.append("dbo", "4");
					_postdata.append("l", $.Storage.loadItem('language'));
					_postdata.append("rids", delIDs);
					
					//Show trobbler
					showLoadingBox('Processando...');
					
					$.ajax({
							 url: base_url + "/cgi-bin/mdl-adm-cliente_categoria.php",
							type: "post",
							//dataType: "json",
							data: _postdata,
							processData: false,
							contentType: false,
							cache: false,
							//scriptCharset: "utf-8",
							success: function(response, textStatus, jqXHR)
							{
								var tmp   = response.split('|');
								var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
								if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
								//
								if(myRet.status == 1)
								{
									refresh_datatable();
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
								else
								{
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
							},
							error: function(jqXHR, textStatus, errorThrown)
							{
									console.log("The following error occured: "+textStatus, errorThrown);
									hideLoadingBox();
									show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
									return false;
							},
							complete: function(){},
							statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
					});
					
					return false;
				}
				else
				{
					confirmDiag.close();
					return false;
				}
			}
		});
		
		return false;
	});
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Processa Form ~ STOP
	

});

