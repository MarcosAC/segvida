/* #####################################################################
   # SEGVIDA - Planos Form
   ##################################################################### */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		//var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	//alert(tmp0[1]+' = '+tmp1[1]);
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/planos.htm');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	
	
	
		
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() 
	{
		$('#loadingBox').modal('show');
	}).ajaxStop(function() {
		$('#loadingBox').modal('hide');
	});
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	//alert("language => "+$.Storage.loadItem('language'));
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#login_captcha_refresh_btn", function()
	{
		$("#login_captcha_code").val('');
		$('#login_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#login_captcha_img').attr('src', captcha_src);
		return false;
	});
	$(document).on("click", "#lost_captcha_refresh_btn", function()
	{
		$("#lost_captcha_code").val('');
		$('#lost_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#lost_captcha_img').attr('src', captcha_src);
		return false;
	});
	$(document).on("click", "#register_captcha_refresh_btn", function()
	{
		$("#register_captcha_code").val('');
		$('#register_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#register_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	
	// checa cpf
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}//endfunc
	
	// Checa CNPJ
	function chk_cnpj(s)
	{
		var i;
		var c = s.substr(0,12);
		var dv = s.substr(12,2);
		var d1 = 0;
		//
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+(i % 8)); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+((i+1) % 8)); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}//endfunc
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Login Form ~ START
	var $formRegister = $('#register-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	$(document).on("f_comprar_plano", function(e,_plano)
	{
		//alert('Plano: '+_plano);
		$('#register_captcha_refresh_btn').trigger('click'); 
		$('#register_plano').val(_plano);
		
	});
	
	//Validacao dos campos de registro
	$(document).on("change", "#register_uf", function()
	{
		//Forca revalidacao do CEP por sido mudado o estado
		$('#register_cep_ok').val('');
		
		if(!isEmpty(this.value))
		{ 
			//_estado,_target_panel,_field_id,_required,_setfirstblank,_firstblanktext,_cidade,_codigo)
			frm_mostra_cidades(this.value,'cidade_panel','register_cidade',1,1,'*Cidade');
		}
	});
	//$(document).on("blur", "#cep", function()
	//{
	//	if( !frm_valida_cep(this.value,true,false) ){ return false; } }
	//});
	//$(document).on("blur", "#register_cep", function()
	//{
	//	frm_processa_cep(this.value);
	//});
	$(document).on("click", "#register_search_cep_btn", function()
	{
		var _cep = $('#register_cep').val();
		frm_processa_cep(_cep);
	});
	
	
	
	//CEP
	$(document).on("blur", "#register_cep", function(){
		setContent($('#register_cep'), 'CEP');
		var _cep = $('#register_cep').val();
		if( !frm_valida_cep(_cep,true) ){ return false; }
	});
	function frm_valida_cep(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CEP é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 8 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CEP válido!',' ','alert');
			}
			return false;
		}
		else if( checa_existe_cep(_tmp) == 'nao_existe')
		{
			if( ret_msg )
			{
				show_msgbox('O CEP <b>'+_tmp+'</b> não existe!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function checa_existe_cep(_cep)
	{
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//Formata postdata
		//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
		var _postdata = "cep=" + encodeURIComponent(_cep);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/cep.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], uf:tmp[1], cidade:tmp[2], bairro:tmp[3], logradouro:tmp[4], codigo:tmp[5]};
					//
					if(myRet.status == 1)
					{
						$('#register_cep_uf').val(myRet.uf);
						$('#register_uf').val(myRet.uf);
						
						$('#register_cep_cidade_codigo').val(myRet.codigo); 
						//frm_gera_options_cidades(_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo)
						frm_gera_options_cidades(myRet.uf,'register_cidade',1,'Selecione a cidade','',myRet.codigo);
						
						$('#register_bairro').val(myRet.bairro);
						$('#register_end').val(myRet.logradouro);
						//
						$('#loadingBox').modal('hide');
						return true;
					}
					else
					{
						$('#loadingBox').modal('hide');
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return 'nao_existe';
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	
	
	
	//$(document).on("keyup", "#register_nome", function(){
	//	this.value = this.value.toUpperCase();
	//});
	$(document).on("blur", "#register_nome", function(){
		if( !frm_valida_nome(this.value,true) ){ return false; }
	});
	//$(document).on("keyup", "#register_sobrenome", function(){
	//	this.value = this.value.toUpperCase();
	//});
	$(document).on("blur", "#register_sobrenome", function(){
		if( !frm_valida_sobrenome(this.value,true) ){ return false; }
	});
	//$(document).on("keyup", "#register_email", function(){
	//	this.value = this.value.toLowerCase();
	//});
	$(document).on("blur", "#register_email", function(){
		if( !frm_valida_email(this.value,true) ){ return false; }
	});
	$(document).on("blur", "#register_cpf", function(){
		setContent($('#register_cpf'), 'CPF');
		if( !frm_valida_cpf(this.value,true) ){ return false; }
	});
	$(document).on("blur", "#register_cnpj", function(){
		setContent($('#register_cnpj'), 'CNPJ');
		//if( !frm_valida_cnpj(this.value,true) ){ return false; }
	});
	//$(document).on("keyup", "#register_razaosocial", function(){
	//	this.value = this.value.toUpperCase();
	//});
	$(document).on("blur", "#register_fonefixo1", function(){
		setContent($('#register_fonefixo1'), 'FONE');
		//if( !frm_valida_tel(this.value,true,false) ){ return false; }
	});
	$(document).on("blur", "#register_celular", function(){
		setContent($('#register_celular'), 'FONE');
		//if( !frm_valida_cel(this.value,true,false) ){ return false; }
	});
	//$(document).on("blur", "#register_cep", function(){
	//	setContent($('#register_cep'), 'CEP');
	//	if( !frm_valida_cep(this.value,true) ){ return false; }
	//});
	//$(document).on("keyup", "#register_end", function(){
	//	this.value = this.value.toUpperCase();
	//});
	//$(document).on("blur", "#register_end", function(){
	//	if( !frm_valida_end1(this.value,true,false) ){ return false; }
	//});
	//$(document).on("keyup", "#register_bairro", function(){
	//	this.value = this.value.toUpperCase();
	//});
	//$(document).on("keyup", "#register_num", function(){
	//	this.value = this.value.toUpperCase();
	//});
	//$(document).on("blur", "#num", function(){
	//	if( !frm_valida_numero(this.value,true,false) ){ return false; }
	//});
	//$(document).on("keyup", "#register_compl", function(){
	//	this.value = this.value.toUpperCase();
	//});
	$(document).on("blur", "#register_username", function(){
		if( !frm_valida_username(this.value,true) ){ return false; }
	});
	$(document).on("blur", "#register_password", function(){
		if( !frm_valida_senha1(this.value,true) ){ return false; }
	});
	$(document).on("blur", "#register_password2", function(){
		if( !frm_valida_senha2(this.value,true) ){ return false; }
		if( !frm_valida_senhas($('#register_password').val(),this.value,true) ){ return false; }
		$('#register_captcha_refresh_btn').trigger('click');
	});
	
	function frm_valida_nome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo NOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('NOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_sobrenome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('SOBRENOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_email(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( !emailReg.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um E-MAIL válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_tel(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE TELEFONE válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_cel(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CELULAR é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE CELULAR válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_senha1(value,ret_msg)
	{
		// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		else if (!re.test(_tmp))
		{
			show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert');
			return false;
		}
		return true;
	}
	function frm_valida_senha2(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CONFIRMAÇÃO DE SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma CONFIRMAÇÃO DE SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_senhas(s1,s2,ret_msg)
	{
		var _tmp1 = s1;
		var _tmp2 = s2;
		if( _tmp1 != _tmp2 )
		{
			if( ret_msg )
			{
				show_msgbox('Os campos SENHA e CONFIRMAÇÃO DE SENHA não conferem!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_cpf(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CPF é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cpf(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CPF válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	// checa cpf
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	function frm_valida_username(value,ret_msg)
	{
		re = /^\w+$/;//apenas letras, numeros e underlines
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( (_tmp.length < 6) || (_tmp.length > 30) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um USUÁRIO válido!',' ','alert');
			}
			return false;
		}
		else if(!re.test(_tmp))
		{ 
			show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert');
			return false;
		}
		else if( checa_existe_username(_tmp) == "ja_em_uso" )
		{
			if( ret_msg )
			{
				show_msgbox('O usuário <b>'+_tmp+'</b> já está sendo usado!',' ','alert');
			}
			return false;
		}
		
		return true;
	}
	function checa_existe_username(_username)
	{
		//Verifica captcha
		var _postdata = "lang="   + encodeURIComponent($.Storage.loadItem('language'))
		              + "&username=" + encodeURIComponent(_username);
		
		$.ajax({
				 url: base_url + "/lib-bin/existe_username.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						return "ja_em_uso";
					}
					else
					{
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	$('#bt_register').click( function () 
	{
		var rg_nome              = $('#register_nome').val();
		var rg_sobrenome         = $('#register_sobrenome').val();
		var rg_email             = $('#register_email').val();
		var rg_cpf               = $('#register_cpf').val();
		var rg_cnpj              = $('#register_cnpj').val();
		var rg_razao             = $('#register_razaosocial').val();
		var rg_fonefixo1         = $('#register_fonefixo1').val();
		var rg_celular           = $('#register_celular').val();
		var rg_pais              = $('#register_pais').val();
		var rg_idioma            = $('#register_idioma').val();
		var rg_cep               = $('#register_cep').val();
		var rg_cep_uf            = $('#register_cep_uf').val();
		var rg_cep_cidade_codigo = $('#register_cep_cidade_codigo').val();
		var rg_estado            = $('#register_uf').val();
		var rg_cidade            = $('#register_cidade').val();
		var rg_bairro            = $('#register_bairro').val();
		var rg_end               = $('#register_end').val();
		var rg_num               = $('#register_num').val();
		var rg_compl             = $('#register_compl').val();
		var rg_username          = $('#register_username').val();
		var rg_password          = $('#register_password').val();
		var rg_password2         = $('#register_password2').val();
		var rg_plano             = $('#register_plano').val();
		var rg_captcha_code      = $('#register_captcha_code').val();
		var rg_termos            = $('#register_termos').is(':checked');
		
		setContent($('#register_cpf'), 'CPF');
		setContent($('#register_cnpj'), 'CNPJ');
		setContent($('#register_fonefixo1'), 'FONE');
		setContent($('#register_celular'), 'FONE');
		setContent($('#register_cep'), 'CEP');
		
		if (isEmpty(rg_nome))         { show_msgbox('O campo NOME é obrigatório!',' ','alert');               $('#register_nome').focus();         return false; }
		if (isEmpty(rg_sobrenome))    { show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');          $('#register_sobrenome').focus();    return false; }
		if (isEmpty(rg_email))        { show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');             $('#register_email').focus();        return false; }
		if (!emailReg.test(rg_email)) { show_msgbox('Informe um e-Mail válido!',' ','alert');                 $('#register_email').focus();        return false; }
		
		if (isEmpty(rg_cpf))          { show_msgbox('O campo CPF é obrigatório!',' ','alert');                $('#register_cpf').focus();          return false; }
		//if (isEmpty(rg_cnpj))       { show_msgbox('O campo CNPJ é obrigatório!',' ','alert');               $('#register_cnpj').focus();         return false; }
		//if (isEmpty(rg_razao))      { show_msgbox('O campo RAZÃO SOCIAL é obrigatório!',' ','alert');       $('#register_razaosocial').focus();        return false; }
		//if (isEmpty(rg_fonefixo1))  { show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');      $('#register_fonefixo1').focus();    return false; }
		//if (isEmpty(rg_celular))    { show_msgbox('O campo CELULAR é obrigatório!',' ','alert');            $('#register_celular').focus();      return false; }
		if (isEmpty(rg_pais))         { show_msgbox('O campo PAÍS é obrigatório!',' ','alert');                $('#register_pais').focus();          return false; }
		if (isEmpty(rg_cep))          { show_msgbox('O campo CEP é obrigatório!',' ','alert');                $('#register_cep').focus();          return false; }
		
		if (isEmpty(rg_estado))       { show_msgbox('O campo ESTADO é obrigatório!',' ','alert');             $('#register_uf').focus();       return false; }
		if(!isEmpty(rg_cep_uf))
		{
			if(rg_estado != rg_cep_uf){ show_msgbox('O ESTADO informado não confere com o ESTADO do CEP informado!',' ','alert'); $('#register_uf').focus(); return false; }
		}
		
		if (isEmpty(rg_cidade))       { show_msgbox('O campo CIDADE é obrigatório!',' ','alert'); $('#register_cidade').focus(); return false; }
		if(!isEmpty(rg_cep_cidade_codigo))
		{
			if(rg_cidade != rg_cep_cidade_codigo){ show_msgbox('A CIDADE informada não confere com a CIDADE do CEP informado!',' ','alert'); $('#register_cidade').focus(); return false; }
		}
		
		if (isEmpty(rg_bairro))       { show_msgbox('O campo BAIRRO é obrigatório!',' ','alert');             $('#register_bairro').focus();       return false; }
		if (isEmpty(rg_end))          { show_msgbox('O campo ENDEREÇO é obrigatório!',' ','alert');           $('#register_end').focus();          return false; }
		if (isEmpty(rg_num))          { show_msgbox('O campo NÚMERO DO ENDEREÇO é obrigatório!',' ','alert'); $('#register_num').focus();          return false; }
		//if (isEmpty(rg_compl))      { show_msgbox('O campo COMPLEMENTO é obrigatório!',' ','alert');        $('#register_compl').focus();        return false; }
		
		
		//Username
		if (isEmpty(rg_username))     { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');           $('#register_username').focus();     return false; }
		re = /^\w+$/; 
		if(!re.test(rg_username))     { show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert'); $('#register_username').focus();     return false; }
		if( checa_existe_username(rg_username) == "ja_em_uso" ){ show_msgbox('O usuário <b>'+rg_username+'</b> já está sendo usado!',' ','alert'); $('#register_username').focus();     return false; }
		
		//Password
		if (isEmpty(rg_password))     { show_msgbox('O campo SENHA é obrigatório!',' ','alert');              $('#register_password').focus();     return false; }
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		if (!re.test(rg_password))    { show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert'); $('#register_password').focus(); return false; }
		if (isEmpty(rg_password2))    { show_msgbox('O campo SENHA PARA CONFIRMAÇÃO é obrigatório!',' ','alert'); $('#register_password2').focus(); return false; }
		if (rg_password != rg_password2) { show_msgbox('SENHA e CONFIRMAÇÃO não conferem!',' ','alert');     $('#register_password').focus();  return false; }
		
		if (isEmpty(rg_plano))        { show_msgbox('O campo PLANO DE PAGAMENTO é obrigatório!',' ','alert'); $('#register_plano').focus();         return false; }
		if (isEmpty(rg_captcha_code)) { show_msgbox('Informe o CÓDIGO DE SEGURANÇA!',' ','alert');            $('#register_captcha_code').focus(); return false; }
		if (rg_termos != true)        { show_msgbox('Para se cadastrar, leia e concorde com o termo de serviço!',' ','alert');           $('#register_termo').focus();          return false; }
		
		//Formatacao
		rg_idioma    = rg_idioma.toLowerCase();
		rg_nome      = rg_nome.toUpperCase();
		rg_sobrenome = rg_sobrenome.toUpperCase();
		rg_email     = rg_email.toLowerCase();
		rg_razao     = rg_razao.toUpperCase();
		rg_bairro    = rg_bairro.toUpperCase();
		rg_end       = rg_end.toUpperCase();
		rg_num       = rg_num.toUpperCase();
		rg_compl     = rg_compl.toUpperCase();
		rg_username  = rg_username.toUpperCase();
		
		//Criptografa senha
		var rg_password_crypted = hex_sha512(rg_password);
		
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		
		//Verifica captcha
		var _postdata = "lang=" + encodeURIComponent($.Storage.loadItem('language'))
		              + "&code=" + encodeURIComponent(rg_captcha_code);
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		
		$.ajax({
				 url: base_url + "/lib-bin/captcha/chk_code.cgi",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 0)
					{
						$('#register_captcha_refresh_btn').trigger('click');
						$('#register_captcha_code').val('');
						$('#register_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox('CÓDIGO DE SEGURANÇA inválido. Tente novamente!',' ','alert');
						return false;
					}
					else
					{
						//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
						var _postdata = "l="          + encodeURIComponent($.Storage.loadItem('language'))
							              "&plano="     + encodeURIComponent(rg_plano)
							            + "&nome="      + encodeURIComponent(rg_nome)
							            + "&sobrenome=" + encodeURIComponent(rg_sobrenome)
							            + "&email="     + encodeURIComponent(rg_email)
							            + "&cpf="       + encodeURIComponent(rg_cpf)
							            + "&cnpj="      + encodeURIComponent(rg_cnpj)
							            + "&razao="     + encodeURIComponent(rg_razao)
							            + "&fonefixo1=" + encodeURIComponent(rg_fonefixo1)
							            + "&celular="   + encodeURIComponent(rg_celular)
							            + "&pais="      + encodeURIComponent(rg_pais)
							            + "&idioma="    + encodeURIComponent(rg_idioma)
							            + "&cep="       + encodeURIComponent(rg_cep)
							            + "&uf="        + encodeURIComponent(rg_estado)
							            + "&cidade="    + encodeURIComponent(rg_cidade)
							            + "&bairro="    + encodeURIComponent(rg_bairro)
							            + "&end="       + encodeURIComponent(rg_end)
							            + "&num="       + encodeURIComponent(rg_num)
							            + "&compl="     + encodeURIComponent(rg_compl)
							            + "&username="  + encodeURIComponent(rg_username)
							            + "&p="         + encodeURIComponent(rg_password_crypted)
							            ;
						
						//$('#loadingBox').modal('hide');
						do_cad(_postdata);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	function do_cad(_data)
	{
		//alert(base_url + "/lib-bin/registro.php?"+_data);
		//return false;
		
		$.ajax({
				 url: base_url + "/lib-bin/registro.php",
				type: "post",
		//dataType: "json",
				data: _data,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						$('#register_nome').val('');
						$('#register_sobrenome').val('');
						$('#register_email').val('');
						$('#register_cpf').val('');
						$('#register_cnpj').val('');
						$('#register_razaosocial').val('');
						$('#register_fonefixo1').val('');
						$('#register_celular').val('');
						$('#register_cep').val('');
						$('#register_uf').val('');
						$('#register_cidade').val('');
						$('#register_bairro').val('');
						$('#register_end').val('');
						$('#register_num').val('');
						$('#register_compl').val('');
						$('#register_username').val('');
						$('#register_password').val('');
						$('#register_password2').val('');
						$('#register_plano').val('');
						$('#register_captcha_code').val('');
						$('#register_captcha_refresh_btn').trigger('click');
						$("#register_termos").prop('checked', false);
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$('#register_captcha_refresh_btn').trigger('click');
						$('#register_captcha_code').val('');
						$('#register_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	$('#bt_lost').click( function () 
	{
		var $ls_email        = $('#lost_email').val();
		var $ls_captcha_code = $('#lost_captcha_code').val();
		//if ($ls_email == "ERROR") 
		//{
		//	msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "error", "glyphicon-remove", "Send error");
		//} else {
		//	msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'), "success", "glyphicon-ok", "Send OK");
		//}
		if (isEmpty($ls_email))        { show_msgbox('O campo E-MAIL é obrigatório!',' ','alert'); $('#lost_email').focus(); return false; }
		if (!emailReg.test($ls_email)) { show_msgbox('Informe um e-Mail válido!',' ','alert');     $('#register_email').focus();        return false; }
		if (isEmpty($lg_password))     { show_msgbox('O campo SENHA é obrigatório!',' ','alert');  $('#lost_captcha_code').focus(); return false; }
		
		show_msgbox('Um link para renovação da senha de acesso acaba de ser enviado ao e-mail!',' ','success');
		
		//Clear
		$('#lost_email').val('');
		$('#lost_captcha_code').val('');
		$('#lost_captcha_refresh_btn').trigger('click');
		
		return false;
	});
	
	$('#login_register_btn').click( function () 
	{ 
		$('#register_captcha_refresh_btn').trigger('click');
		modalAnimate($formLogin, $formRegister);
	});
	
	$('#lost_login_btn').click( function () 
	{ 
		$('#login_captcha_refresh_btn').trigger('click');
		modalAnimate($formLost, $formLogin); 
	});
	
	$('#lost_register_btn').click( function () 
	{ 
		$('#register_captcha_refresh_btn').trigger('click');
		modalAnimate($formLost, $formRegister); 
	});
	
	$('#register_lost_btn').click( function () 
	{ 
		$('#lost_captcha_refresh_btn').trigger('click');
		modalAnimate($formRegister, $formLost); 
	});
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Login Form ~ STOP
	
	
	
	
	////////////////////// Validacoes de Form ~ START
	function frm_mostra_cidades(_estado,_target_panel,_field_id,_required,_setfirstblank,_firstblanktext,_cidade,_codigo)
	{
		if(isEmpty(_estado))
		{
			show_msgbox('Selecione um Estado para exibir a lista de cidades!',' ','alert');
			return false;
		}
		if(isEmpty(_target_panel))
		{
			show_msgbox('TARGET não indicado!',' ','error');
			return false;
		}
		if(isEmpty(_field_id))
		{
			show_msgbox('FIELD_ID não indicado!',' ','error');
			return false;
		}
		
		if(!isEmpty(_required)) { var _required_set=1; } else { var _required_set=0; }
		if(!isEmpty(_setfirstblank)) { var _setfirstblank=1; } else { var _setfirstblank=0; }
		
		
		//var _field_id = _target;
		var _css = 'form-control';
		
		//Forca revalidacao do CEP por ter sido mudada a cidade
		$('#register_cep_ok').val('');
		
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//Formata postdata
		//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
		var _postdata = 
			              "field_id="  + encodeURIComponent(_field_id)
			            + "&estado="   + encodeURIComponent(_estado)
			            + "&css="      + encodeURIComponent(_css)
			            + "&required=" + encodeURIComponent(_required_set);
		
		if(!isEmpty(_cidade)){ _postdata = _postdata + "&cidade="   + encodeURIComponent(_cidade); }
		if(!isEmpty(_codigo)){ _postdata = _postdata + "&codigo="   + encodeURIComponent(_codigo); }
		if(_setfirstblank == 1){ _postdata = _postdata + "&firstblank="   + encodeURIComponent(_setfirstblank); }
		if(!isEmpty(_firstblanktext)){ _postdata = _postdata + "&firstblanktext="   + encodeURIComponent(_firstblanktext); }
		
		//alert('frm_mostra_cidades POSTDATA = '+base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		
		$.ajax({
				 url: base_url + "/lib-bin/gera_lista_cidades.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					//
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					if(myRet.status == 1)
					{
						$('#'+_target_panel).html(myRet.text);
						$('#loadingBox').modal('hide');
						return false;
					}
					else
					{
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	function frm_processa_cep(_cep)
	{
		if(isEmpty(_cep))
		{
			show_msgbox('Informe um CEP para busca.',' ','alert');
			return false;
		}
		if( _cep.length < 8 )
		{
			show_msgbox('Entre com um CEP válido.',' ','alert');
			return false;
		}
		if( $('#register_cep_ok').val() == 1)
		{
			return true;
		}
		
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//Formata postdata
		//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
		var _postdata = "cep=" + encodeURIComponent(_cep);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/cep.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					//alert("response => "+response);
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], uf:tmp[1], cidade:tmp[2], bairro:tmp[3], logradouro:tmp[4], codigo:tmp[5]};
					//
					if(myRet.status == 1)
					{
						if(!isEmpty(myRet.uf))
						{
							$('#register_cep_uf').val(myRet.uf);
							$('#register_uf').val(myRet.uf);
						}
						if(!isEmpty(myRet.cidade))
						{ 
							$('#register_cep_cidade_codigo').val(myRet.codigo); 
							//_estado,_target_panel,_field_id,_required,_setfirstblank,_firstblanktext,_cidade,_codigo)
							frm_mostra_cidades(myRet.uf,'cidade_panel','register_cidade',1,1,'*Cidade',myRet.cidade);
						}
						if(!isEmpty(myRet.bairro)){ $('#register_bairro').val(myRet.bairro); }
						if(!isEmpty(myRet.logradouro)){ $('#register_end').val(myRet.logradouro); }
						$('#register_cep_ok').val('1');
						//
						$('#loadingBox').modal('hide');
						return false;
					}
					else
					{
						$('#register_cep_ok').val('');
						$('#loadingBox').modal('hide');
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	////////////////////// Validacoes de Form ~ START
	
	
	
});