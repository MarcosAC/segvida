/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-relat-prod.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  ################################################################################# */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	

	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=relat-prod');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	

	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	

	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	

	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	

	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	

	////////////////////// FORMATACAO NUMERICA ~ START
	//SPRINTF javascript equivalent
	function sprintf() 
	{
		var regex = /%%|%(\d+\$)?([-+\'#0 ]*)(\*\d+\$|\*|\d+)?(\.(\*\d+\$|\*|\d+))?([scboxXuideEfFgG])/g;
	  var a = arguments;
	  var i = 0;
	  var format = a[i++];
	
	  // pad()
	  var pad = function(str, len, chr, leftJustify) {
	    if (!chr) {
	      chr = ' ';
	    }
	    var padding = (str.length >= len) ? '' : new Array(1 + len - str.length >>> 0)
	      .join(chr);
	    return leftJustify ? str + padding : padding + str;
	  };
	
	  // justify()
	  var justify = function(value, prefix, leftJustify, minWidth, zeroPad, customPadChar) {
	    var diff = minWidth - value.length;
	    if (diff > 0) {
	      if (leftJustify || !zeroPad) {
	        value = pad(value, minWidth, customPadChar, leftJustify);
	      } else {
	        value = value.slice(0, prefix.length) + pad('', diff, '0', true) + value.slice(prefix.length);
	      }
	    }
	    return value;
	  };
	
	  // formatBaseX()
	  var formatBaseX = function(value, base, prefix, leftJustify, minWidth, precision, zeroPad) {
	    // Note: casts negative numbers to positive ones
	    var number = value >>> 0;
	    prefix = prefix && number && {
	      '2': '0b',
	      '8': '0',
	      '16': '0x'
	    }[base] || '';
	    value = prefix + pad(number.toString(base), precision || 0, '0', false);
	    return justify(value, prefix, leftJustify, minWidth, zeroPad);
	  };
	
	  // formatString()
	  var formatString = function(value, leftJustify, minWidth, precision, zeroPad, customPadChar) {
	    if (precision != null) {
	      value = value.slice(0, precision);
	    }
	    return justify(value, '', leftJustify, minWidth, zeroPad, customPadChar);
	  };
	
	  // doFormat()
	  var doFormat = function(substring, valueIndex, flags, minWidth, _, precision, type) {
	    var number, prefix, method, textTransform, value;
	
	    if (substring === '%%') {
	      return '%';
	    }
	
	    // parse flags
	    var leftJustify = false;
	    var positivePrefix = '';
	    var zeroPad = false;
	    var prefixBaseX = false;
	    var customPadChar = ' ';
	    var flagsl = flags.length;
	    for (var j = 0; flags && j < flagsl; j++) {
	      switch (flags.charAt(j)) {
	        case ' ':
	          positivePrefix = ' ';
	          break;
	        case '+':
	          positivePrefix = '+';
	          break;
	        case '-':
	          leftJustify = true;
	          break;
	        case "'":
	          customPadChar = flags.charAt(j + 1);
	          break;
	        case '0':
	          zeroPad = true;
	          customPadChar = '0';
	          break;
	        case '#':
	          prefixBaseX = true;
	          break;
	      }
	    }
	
	    // parameters may be null, undefined, empty-string or real valued
	    // we want to ignore null, undefined and empty-string values
	    if (!minWidth) {
	      minWidth = 0;
	    } else if (minWidth === '*') {
	      minWidth = +a[i++];
	    } else if (minWidth.charAt(0) == '*') {
	      minWidth = +a[minWidth.slice(1, -1)];
	    } else {
	      minWidth = +minWidth;
	    }
	
	    // Note: undocumented perl feature:
	    if (minWidth < 0) {
	      minWidth = -minWidth;
	      leftJustify = true;
	    }
	
	    if (!isFinite(minWidth)) {
	      throw new Error('sprintf: (minimum-)width must be finite');
	    }
	
	    if (!precision) {
	      precision = 'fFeE'.indexOf(type) > -1 ? 6 : (type === 'd') ? 0 : undefined;
	    } else if (precision === '*') {
	      precision = +a[i++];
	    } else if (precision.charAt(0) == '*') {
	      precision = +a[precision.slice(1, -1)];
	    } else {
	      precision = +precision;
	    }
	
	    // grab value using valueIndex if required?
	    value = valueIndex ? a[valueIndex.slice(0, -1)] : a[i++];
	
	    switch (type) {
	      case 's':
	        return formatString(String(value), leftJustify, minWidth, precision, zeroPad, customPadChar);
	      case 'c':
	        return formatString(String.fromCharCode(+value), leftJustify, minWidth, precision, zeroPad);
	      case 'b':
	        return formatBaseX(value, 2, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'o':
	        return formatBaseX(value, 8, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'x':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'X':
	        return formatBaseX(value, 16, prefixBaseX, leftJustify, minWidth, precision, zeroPad)
	          .toUpperCase();
	      case 'u':
	        return formatBaseX(value, 10, prefixBaseX, leftJustify, minWidth, precision, zeroPad);
	      case 'i':
	      case 'd':
	        number = +value || 0;
	        number = Math.round(number - number % 1); // Plain Math.round doesn't just truncate
	        prefix = number < 0 ? '-' : positivePrefix;
	        value = prefix + pad(String(Math.abs(number)), precision, '0', false);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad);
	      case 'e':
	      case 'E':
	      case 'f': // Should handle locales (as per setlocale)
	      case 'F':
	      case 'g':
	      case 'G':
	        number = +value;
	        prefix = number < 0 ? '-' : positivePrefix;
	        method = ['toExponential', 'toFixed', 'toPrecision']['efg'.indexOf(type.toLowerCase())];
	        textTransform = ['toString', 'toUpperCase']['eEfFgG'.indexOf(type) % 2];
	        value = prefix + Math.abs(number)[method](precision);
	        return justify(value, prefix, leftJustify, minWidth, zeroPad)[textTransform]();
	      default:
	        return substring;
	    }
	  };
	
	  return format.replace(regex, doFormat);
	}
	// Formata valor de USD para BRL
	function currency_format (number, decimals, dec_point, thousands_sep) 
	{
		var n = number, prec = decimals;
		
		var toFixedFix = function (n,prec) {
		    var k = Math.pow(10,prec);
		    return (Math.round(n*k)/k).toString();
		};
		
		n = !isFinite(+n) ? 0 : +n;
		prec = !isFinite(+prec) ? 0 : Math.abs(prec);
		var sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep;
		var dec = (typeof dec_point === 'undefined') ? '.' : dec_point;
		
		var s = (prec > 0) ? toFixedFix(n, prec) : toFixedFix(Math.round(n), prec); //fix for IE parseFloat(0.55).toFixed(0) = 0;
		
		var abs = toFixedFix(Math.abs(n), prec);
		var _, i;
		
		if (abs >= 1000) {
		    _ = abs.split(/\D/);
		    i = _[0].length % 3 || 3;
		
		    _[0] = s.slice(0,i + (n < 0)) +
		          _[0].slice(i).replace(/(\d{3})/g, sep+'$1');
		    s = _.join(dec);
		} else {
		    s = s.replace('.', dec);
		}
		
		var decPos = s.indexOf(dec);
		if (prec >= 1 && decPos !== -1 && (s.length-decPos-1) < prec) {
		    s += new Array(prec-(s.length-decPos-1)).join(0)+'0';
		}
		else if (prec >= 1 && decPos === -1) {
		    s += dec+new Array(prec).join(0)+'0';
		}
		return s; 
	}
	// Formata currency on type
	function currencyFormat(fld, milSep, decSep, e) 
	{
		var sep = 0;
		var key = '';
		var i = j = 0;
		var len = len2 = 0;
		var strCheck = '0123456789';
		var aux = aux2 = '';
		var whichCode = (window.Event) ? e.which : e.keyCode;
		//
		if (whichCode == 13) return true;  // Enter
		if (whichCode == 8) return true;  // Delete
		key = String.fromCharCode(whichCode);  // Get key value from key code
		if (strCheck.indexOf(key) == -1) return false;  // Not a valid key
		len = fld.value.length;
		for(i = 0; i < len; i++)
			if ((fld.value.charAt(i) != '0') && (fld.value.charAt(i) != decSep)) break;
			aux = '';
		for(; i < len; i++)
			if (strCheck.indexOf(fld.value.charAt(i))!=-1) aux += fld.value.charAt(i);
			aux += key;
			len = aux.length;
			if (len == 0) fld.value = '';
			if (len == 1) fld.value = '0'+ decSep + '0' + aux;
			if (len == 2) fld.value = '0'+ decSep + aux;
			if (len > 2) {
			  aux2 = '';
			  for (j = 0, i = len - 3; i >= 0; i--) {
			    if (j == 3) {
			      aux2 += milSep;
			      j = 0;
			    }//endif
			    aux2 += aux.charAt(i);
			    j++;
			  }//endfor
			  fld.value = '';
			  len2 = aux2.length;
			  for (i = len2 - 1; i >= 0; i--)
			  fld.value += aux2.charAt(i);
			  fld.value += decSep + aux.substr(len - 2, len);
			}//endif
		return false;
	}
	// Formata autoNumeric Defaults
	$.extend($.fn.autoNumeric.defaults, {aSign: 'R$ ', pSign:'p', aSep: '.', aDec: ','});
	// Format datepicker Defaults
	$.fn.datepicker.defaults.container='#cad-modal';
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
	////////////////////// FORMATACAO NUMERICA ~ STOP
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	

	////////////////////// MSGBOX ~ START
	$(document).on("f_show_msgbox", function(e,_txt,_title,_type)
	{
		show_msgbox(_txt,_title,_type);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'ficha':
				$("#msgBoxIconFicha").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'pt-br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'en-us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'es-es':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
	}
	////////////////////// LOADINGBOX ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var onlyNumbersReg = /^\\d*$/;
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var apenasLetrasNumerosUnderlinesRegex = /^\w+$/;//apenas letras, numeros e underlines
	var apenasNumerosRegex = /^\d*$/;
	// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
	var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	var usernameRegex = /^\w+$/;//apenas letras, numeros e underlines
	var isDecimal2Regex = /^\d+\.\d{0,2}$/;
	var isDecimal3Regex = /^\d+\.\d{0,3}$/;
	var isDecimal4Regex = /^\d+\.\d{0,4}$/;
	var isValidPTBRCurrencyRegex = /\d{1,3}(?:\.\d{3})*?,\d{2}/;
	
	function isDate(str)
	{
		var parms = str.split(/[\.\-\/]/);
		var yyyy = parseInt(parms[2],10);
		var mm   = parseInt(parms[1],10);
		var dd   = parseInt(parms[0],10);
		var date = new Date(yyyy,mm-1,dd,0,0,0,0);
		return mm === (date.getMonth()+1) && dd === date.getDate() && yyyy === date.getFullYear();
	}
	
	function getDataMySQL(_data)
	{
		if( isEmpty(_data) ){ return ''; }
		var _ret = '';
		var _tmp = _data.split("/"); 
		_ret = _tmp[2]+'-'+_tmp[1]+'-'+_tmp[0];
		return _ret;
	}
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	

	////////////////////// Processa Form ~ START
	//on_change validations
	////////////////////// Processa Form ~ STOP
	

	////////////////////// Form ~ START
	var $formRegister = $('#cad-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	var prodfy_logo_170x50_base64tmp = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6OEI4NjlCNzNENDQ3MTFFNjgyNTQ4MjQ4OENCNzgxQTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6OEI4NjlCNzRENDQ3MTFFNjgyNTQ4MjQ4OENCNzgxQTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4Qjg2OUI3MUQ0NDcxMUU2ODI1NDgyNDg4Q0I3ODFBMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4Qjg2OUI3MkQ0NDcxMUU2ODI1NDgyNDg4Q0I3ODFBMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIADIAqgMBEQACEQEDEQH/xACLAAACAwEAAwEAAAAAAAAAAAAABwUGCAQBAgMJAQEAAAAAAAAAAAAAAAAAAAAAEAABAwIEAwMGBwwHCQAAAAACAQMEEQUAEgYHIRMIMUEiUXGBMjMUYVJiIzQVNZGhcoKS0yREVRY3GPBCQ6OURTbBouJjk2SEJRcRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/ANRTrhAgMLInSWokdFRFefMWwRV7EzEqJgPeNJjSmAkRnQfYcSrbzZIYEnlQhqi4D6YAwBgDAcaXmzrPW3pOjrPTtiI6HO/6dc33sB2YAwBgDAGAMAYAwBgMedZGrri1r61Wm3Tn44wrcjj4sum2PMkOmvFBVOORsV9OAeumL1F262Btl6vLjkj6utTUyRnMjcdkyqOo0hnmWpvPICeTzYDKkBreHf3Vc1G5nMGOPPcbddNi3xGyKjbYiKHRV7B4KZUVVVaKuA2Fsvoa4aJ27tlgubov3NlXXZroGTgK464RIgKVFoIZR7O7AXfAGAxzpLS146jdc3++agu78DTlpcEIUBhUIwB5TRltoTzNhQGszh5fEXd8UI6/3DWnTbr87XYbkN3sdyipLZhTkLlEJmTdTbbMcroE0vjBUzJ9xA6v52Nxv2LZ/wAiV+fwB/OxuN+xbP8AkSvz+AP52Nxv2LZ/yJX5/ARWqOr3c2+WKVamY8G0FLHllPgi+MkAVfEjZm6aApJwzIlU7qLxwFwf6QH2NCFfh1A/++jUdbgQeFI3NQearSOV5mevDm5u3jlwDI6ct1blqPaidcNQOlIn6aJ1mTNcWpvsNMo8Djhd5oKqJL2rlqvFVwCe0h1d7h+9zPrhpm6PHHJuz2yNGyK7OcMRazkFT5YipKoj4iWiJ21QOPTvVfutbdZMjqtxpy1pJFq52w4gRzYbzZXMiogOiYJxoZL8OAe/Urutf9vtK2qXp1xkLlcJvKzPAjo+7g0ZHQV786hxwCTvHV7rgdH2uJb3o66nf5rt2uXu48tkVcIWWGWyqBHkRDMiRU408uAbm7G/57caSs1uXJdtdT4DLrqOogNNKraIch8G8vruZsjY07+KInEEnL6keoixuW+8XgRbtl1D3i3MSoDbUaQylFq0YiDpDQk4o52Ki96YDWmidwbPqfb+FrWqQbc/Gcky+cSZY/u6kMhCPh4WybLxUTglcBm7VXVHuVqvVRWLa2CoR1IwhuDHSTMkIFVV5QcQgbDKlaKNUTtXuQErqCVrLW+5YR9TKX7zXCZHtkoCbFkgdEgioKtCgiKjTiiJ24Bu9UGstyYz8/Sb0Bbft8brEe0mbCCr/ujbZLldqqkPMSqfBTAV7YadvnbYfK0Ja/8A094nNtzLucMXwAhVG1InFUfA0hKSp3ccBedrOobc++7u23SV4mRJFtelSI0gmowtEaMtuKhCqLUak2i4Ca6juoXV+idaRtO6VdjAjMQH7gbrQvFzXiJQDivhytiJfjYC05Opf9pwfsL6x+gt/an7O9f+8+9gKRf9n949ttb3PUe0qhLs11JTetVWlVtCJT5RsvKImDZEvLMCzIi08qqHjTXTvr3cLUz+qt5ZJsobSNx7dHcaF5USuQfmkNtltutUFKkqrx76gqeoTZJvbO9Qjtj70vT91E1iuvoiuNOtKmdkyBEEvCQkJUSvHh4a4C09Nu0u2G49jubV7ZmDfLS8HNJiRy23I8hFVokHKviQmzFfR5cA4v5PtnviXH/Ff8GAitT9GugJFilt6cky4V6yZoT8p7msZ045XRQEXKXZVOKdtF7FCmHp3q+esH/z42RS0cr3MrnzIiViIOTlrJQuZkycPV5lOC4Bg3TR0LZ3pr1FbgfGRcJER0Z8wUVEdl3DLF8CLxygJiI/ANacVwCt6LNKQrhq+86hlMc1yyx224RknhbelqYkY/K5bZD5iXAUDfRkrzv1qCHBo47KuLUNoR41dQG2FTh350pgGT1sXgPrnSunQKqwIb0tzy/pBi0Ff8MWAZ3TBtRYbDoCBqOVEak3+/NJKOU6CETUZziyy2peqKgiGVO1V70RMBm+F7xut1Dt+/UdYu92VXG+1EgRaly+HkjMZa+nAN3rfmxWrVpK1ggo6rsp8QFETK22DQJ5kVT4ebAU68akm6f6R9O2hp0mn9Tz5QqgrRfc2ZDhOpVOPiMQRU70VcAxejXSUC26IumsXx/TLi+ccHVT1IkREVUFfluqWb8EfJgElsbHk6w6gbXcH0VScuEi8yiXjRW88lFXzuZU9OAv3W7fhe1JpuwiSL7lEemOInllOI2NfMkZfu4B8dPNlC0bNaXYEVEpEX31xVSiqssyfqvocSnwYDLmgLctp6rUhLQAh3u4pmVaIjQi+qEqr3ZOOA6tsLYe7fUZL1BJbV20MTHbxIF3jSMwaDDZVF4Lx5QqPxUXAbewBgDAKfqg0eWpdormTKVlWQhuzCeUY4kj39wZr50TAZf6XtdNaU3UhtzH0Ztl8ArbKI18Am4qFHJe5PnhEcy9iEuA3xgDAGAQXWdem4m2cG2Ivz1zuTfhr/ZsNm4S+gsmAWWwW8ugtttsL65LfOTquVMJyPaQacRXBFoQYq7l5aAhZ1Jc1UTsRVpUIrpl0Fd9cbnrrG6obtus8krjLmlwR64kXMaBF7yzlzSp2IlFpmTARPU1cFvu+d0iRVzrHWLbmarwzi2OZPgo64SYDc1qtDNtsMOzx1ysworcRkk4UFptGxVPQOAwLs1qG37d7zQ5ep88aNbHpcK4GgERMmTbkdSUE8VBNfFTjSvb2YCZ3Z1PN3t3giwdJsG/GQQttozoQIbYETjspwVSrYVIiWqVQBSqV4YC7dW+jU03pDb63W4SWz2ZqTb+ZSnzqgwQkdOGd3lGS+nARWit+7dZNgz0NaYsl7WLiTIkcW28zaNSjN05OdF7W23CREp6yVXw8cB9OiizrI19ersqVbgW3kp8ByXgUf8AdZLAVLqSuC6h32usSKteS5FtjKrx8YNgJ/cdIkwG7rVbmLZa4dtjpSPCYbjMp8hoEAfvDgMJ7+SZul9/tRzbeXLkEouNH5PfYIi4vn+dLAaD6RtAFp7bsr9MZ5dy1I4kgCX1khNpljov4SqbnDtQkwDzwBgDAerrTbrZtOgLjTgqLjZIiiQqlFRUXgqKmA/PvffZ65bdare5TRnpm4OE5Z5qIqigr4ljmvGjjXZx9YfF5UQLXofq/wBfaftLVsu8NjULcdEBmXIM2pWREoguOChI5RP6yjm8qrgJbSmn9xOou83W+Xe/nZNO251GosNlDdbbMxzC200hNCqiNFccJcy1T8ULJtPf9e7Z7wt7UanuJXiy3EFW1SCIi5eYCcZdaz5jAD5ZAbdaIXFOypBBdbt4R3VGm7MLlVhwnpZt9yLJdQEXz/o+Au23HS3thddEaavF7hyHLlMgMSpoNyXQbcJ8EdTMKLUaCaD4FTswD4sGnrJp60sWiyQmoFtjJRmMyOUUrxVV71JV4qS8V78BnTVtw6SLPrh+63A3rlqIZqz5T0R2ZJaSVzeauZRPkL4+0RqidmAtuneqjSepdxbNpWyQJDkC5523LnIo0QP5SJsAZTOpCuWiqqp29nDAUHWmqunTXjWo9U3jTt0ZuNhcjM3B+MTTEiTznCjgoijytnk5fiU0QqUwFk0ZrrZLblhj93dNzo790sH7wuTCRt6QcQSNOUbjjyqh1aVco0DswHRr7fbazUFih2bUOl7vd7ZeLaF6Jphlozjx0ccDnEQPiTZNcolUhWiIvbxXAVV279NO29ohyIlkuVyb1na3lSYJIb7cKRVh1qrjrPKJfGCq2leC+LAWJnXezmymorzZNOacubiAEJ7Uk+MpSGmG3URY1SkO17JSdlEqVKquAu8HYXZ+53ePrZm2uyJ82QF5alnJkqhuumkkXCbU8tFJa5VSndgLdrrcPSWh7QV01HPCK1ReRHRUJ98h/qMtVzGvHzJ3qiYDGlqt933/AN73p5xfcrU6bbtwUKqkeBHEWxBXO910RyovxlrTKnAN0xIkaHEZiRWhYixmxaYZBMog2CIIiKJ2IiJRMB9cAYAwBgOO8Wa03q3PW27Q2Z8CQlHosgBcbKnFKiSKlUXii92ATF86Otp7hIcfhOXG051zIxGfA2hr3Ij7bp0/HwC7tumt9ti77dIukrOuqdMXExNoxYckIuVFQCJuOQutOii5T4ZS+5QLPtDtpuTqXc8t09ymFt8mMKpa7Yqcss6tq2FGakrbTQGVEJcynxXvqFk3Q6Y7buBrCTqSdqGTFN5tplqI2y2YNA0CDlElVFWpVLj3rgHJbYEe3W6Lb46Ujw2W2GU8gNCgD95MAt+pa/zbLs3fX4MsIkuUjMMDI0BwgfdEHgar6xq1n4Jxy1XuwGQ3dU6G0YIW/S9rt+q7ly0+sdSXmMUiMTpIiq3AhPI2INt+rzHhIjXiiClMAxul/bS+6j3ATcW6QRg2WEb0iGjTIx2H5bqKAjHZFBFGms6lUeCKiCnfQFwFiCSYOg4vulykX0ri2JUE0tDPvgKX4KOVwFj1NNhw4umnpbwsNStulixnCrlcfJ2SKNCqIvirwwHmZcIdlW0JdXkhFK24fjRxcQqm7L965DaIKL7TMlK8KLgIzUAvy4NojN212f8AVmhmzcFpRH3VHJRvLKdQkXwIL6dnFcyYCV1VrWC05btVWp2TYtax7Ha3Zb0ggehXptG2YxijBCXiHLxzVBUbXghIiqG1NLzn5+mbRPkMjHkS4Ud95gRURbNxoTIEFeKIKrSmApm9+z0Lc7TkW3rICBc4MgXodxJvmKAF4X21SokomHioipUhHATW2u2Wl9vdPjZ7EytTVDmzncqyJDiV8ThIicBrQRTgiemoWzAGAMAYAwBgDAGAMAYAwCZ30/iBtX9B+13vtD1Oxn1Pl/E+XlwC6vH8UD/hh9JX23r+t/bf9x5flYDU0P6Ix7P2Y+x9n6qep8nyfBgK6x7P/Jf1v2fZ7NP6PfJwHFqT7FtP+nPaf5l9F/8AE+VgOy/fTx+xPZh9P9t6Pk/FwH2d+lzPsj6Gnre09UPbf8j/AGUwEVdva2L/AE12/rfret+of07cBd8AYAwBgDAf/9k=";
	var prodfy_logo_85x25_base64tmp = "data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAAA8AAD/4QMtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjMtYzAxMSA2Ni4xNDU2NjEsIDIwMTIvMDIvMDYtMTQ6NTY6MjcgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDUzYgKE1hY2ludG9zaCkiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NEYwQzBERDBENDVBMTFFNjgyNTQ4MjQ4OENCNzgxQTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NEYwQzBERDFENDVBMTFFNjgyNTQ4MjQ4OENCNzgxQTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo4Qjg2OUI3NUQ0NDcxMUU2ODI1NDgyNDg4Q0I3ODFBMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo4Qjg2OUI3NkQ0NDcxMUU2ODI1NDgyNDg4Q0I3ODFBMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pv/uAA5BZG9iZQBkwAAAAAH/2wCEAAYEBAQFBAYFBQYJBgUGCQsIBgYICwwKCgsKCgwQDAwMDAwMEAwODxAPDgwTExQUExMcGxsbHB8fHx8fHx8fHx8BBwcHDQwNGBAQGBoVERUaHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fH//AABEIABkAVQMBEQACEQEDEQH/xAB5AAACAgMBAAAAAAAAAAAAAAAFBwAGAwQIAgEBAAAAAAAAAAAAAAAAAAAAABAAAQMDAwMCAwQEDwAAAAAAAgEDBBEFBgASByExEyIIQTIUQnQVNVFxgWKhsVLCM3Ozw1SElLS1NxgRAQAAAAAAAAAAAAAAAAAAAAD/2gAMAwEAAhEDEQA/AL8vuKnMwRy6Vi7ocZPyihxshCQByS2uK0kgodEJGicBR71/gTQZP/XPDH+Nmf6RzQYpXu94eaivOsPzZDwARNRximKuEiVQEIqClV6VXQWLFuVchdySNj+bY0uMy7pEeuFmcGSEpt1qOiE8y6oiCtvNAW4k7U/R0qAIfdpxKTcB1Dno3NeVl01jIgxvVRDfLftRCpuRAUip1VNAdzT3B8eYdfrjYry5JG4W6K3KIGmkMXVd2+NlpdybnSQ91FRBpVVXQa+Y+4zAMUua2ye1cJMyODTl1GHHR0YCPom0ZRqYiJepEVBVevTvoDWZcy4NitntVzlSHZ/46IHZIVvbV+TLBwRISabVQ6Khj1JU7076DnvGeVXIF45R5T+lmr5yS1WFk2yPwPOiZNLKb3KDQh9M15Kr3XanfQbfBWW4Xj8215JkjeQTc1yt1bct2ltVhGct8FEWSMxU0RNikdF7rTp00DvDnPCTzZ7EBGWs6PPatT0xGh+kGY+Dhtsq5v3bi8Binp7ougHp7dcHW41ek3F/HBknOaxFyTW0BJNVVTGOgou2pKqBup+zpoEXlGCYXx/7krXBuFqjPYTkSB44ksEcYZWVVldqmvp8UkUL90Fp20HRMvgfh6TFejnilvbB4CbJxppG3BQkpUDGhCSfBU7aCpZRx1ZeO8JyfLfxW5Xu7w7NIgWiVeJP1H0bLw+MGmEQQQamQ1Xuvb4roEc3BtWQca8acX2B1qde7xcnLxfUiEJrGElNvdI21oYRzWqL6kQOqdU0DP4ptsHLPcPyJlsmM3LGwuhAtikiEIOhWMJt16bvHEVN372gU9nyeCnCuYwDdSVyDm2QNxXbb88wkRxt5CVpPWqK4TootPmKnfQW6DdbPhfObbOXPtthgmJRYdnQqr5pbcJlVFhPtG4ch7b+nQVcX37d7V5T5GR3HNsi2uEq+t0GV3L+zyxlr+vQNX3B25nD8N4wuhteRrD7nAZMW0TqLDCHRK9PUsRNAHHjW+j7e3MuESTNHboGduKieojbIjAV+16YzhOIP8paaDqHQLrmzhu18m461DceSFeIBE7a7ht3oCmiIbTg9FVtzaladUVEX4UUEhJwzl2yOWuBy2/cMk40iOGElm0unIJVRtRYKR4kalmzup8y9P4wOSMK5IyLgm745Y4Ew4Fxve7HYN3eFmWxZGyF5vyK8SLTyt+gCXdtWvVKaBtwcawri/DJt+hWSJHl2y2eS4PRW2235CxmUUg8qolVcMPj3Lr30CKxHlHMp+bWqwYZHsMJ+6XJmTkMOwwFUW4oGjjqyZ5KTbyo2pCZgPzdBLroA1u5WvTbtyydLRY4V7j+I/x1LcyjzyyJTgm+p1TbVo2wVa/N8eug2b7y1kTsqddZ0O0T8liMstW+S9bAOXCqrauB0UjIXmjdVBIfT9npu0DK4huOO5pervYrjCs86y2NWp2N2qPbmhjwWZBmSKBr1F6v9KJtou9FISUS0BbkTjzOuQ+QoVnvSR43F1pNm4qjRVfnvoKorLiV3DtXci0omxa1UlTaDg8TXi8WwfFt2eOibdtKUp2pTQetBNBNBNBR+av+vJn3y1/8nG0Fe4N/M8s++/zi0B+5fkUn7r/ft6Ak3+fTf68P7BzQZsY/M7v/AJT/AGwaCxaCaD//2Q==";
	
	function PixelToPoint(units)
	{
		return ((units * 72) / 96);
	}
	
	// Relatorios
	$('#lnk_relat1_').click( function () 
	{
		var columns = ["Código", "Objetivo", "Muda", "Qtde"];
		var data = [
		    ["L001", "TESTE CLONAL", "CLR400", ""],
		    ["L002", "TESTE CLONAL", "CLR401", ""],
		    ["L003", "TESTE CLONAL", "CLR402", ""],
		    ["L004", "TESTE CLONAL", "CLR403", ""],
		    ["L005", "TESTE CLONAL", "CLR404", ""]
		];
		
		var totalPagesExp = "{total_pages_count_string}";
		
		//var doc = new jsPDF('p', 'pt');
		var doc = new jsPDF('p','pt','a4');
		var totalPagesExp = "{total_pages_count_string}";
		
		var pageContent = function (data) 
		{
			// HEADER
			doc.setFontSize(20);
			doc.setTextColor('#ffffff');
			doc.setFontStyle('normal');
			doc.addImage(prodfy_logo_85x25_base64tmp, 'JPEG', data.settings.margin.left, 10, PixelToPoint(85), PixelToPoint(25));
			doc.text("Guia de Inventário de Lotes", data.settings.margin.left + 10 + PixelToPoint(85), 26);
			
			// FOOTER
			var str = "Página " + data.pageCount;
			// Total page number plugin only available in jspdf v1.0+
			if (typeof doc.putTotalPages === 'function') 
			{
				str = str + " de " + totalPagesExp;
			}
			doc.setFontSize(10);
			doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 10);
		};

		doc.autoTable(columns, data, {
				addPageContent: pageContent,
				margin: {top: 35},
				theme: 'grid'
		});

		// Total page number plugin only available in jspdf v1.0+
		if (typeof doc.putTotalPages === 'function') 
		{
			doc.putTotalPages(totalPagesExp);
		}
		
		doc.setProperties({
				title: 'Prodfy Mudas - Guia de Inventário',
				subject: 'Guia de Inventário'
		});
		//doc.autoTable(columns, data);
		doc.save("table.pdf");
		
		//alert('lnk_relat1 - DONE!');
		
	});
	
	
	$('#lnk_relat1').click( function () 
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "8");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rltid", "1");
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-relat-prod.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#tpl_relat').empty().append(myRet.text);
						hideLoadingBox();
						geraPDF(1);
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	
	function geraPDF(_relat)
	{
		switch(_relat)
		{
			case 1:
				{
					//Guia de Inventario
					var doc = new jsPDF('p','pt','a4');
					var totalPagesExp = "{total_pages_count_string}";
					
					var elem = document.getElementById("tpl_relat1");
					var res = doc.autoTableHtmlToJson(elem);
					var columns = res.columns;
					var data = res.data;
					
					//if(!data){ return false; }
					
					// Document defaults
					doc.autoTableSetDefaults(
					{
						//headerStyles: {fillColor: [80, 80, 80]},
						margin: {top: 35},
						addPageContent: function (data) 
						{
							// HEADER
							doc.setFontSize(20);
							doc.setTextColor('#000000');
							doc.setFontStyle('normal');
							doc.addImage(prodfy_logo_85x25_base64tmp, 'JPEG', data.settings.margin.left, 10, PixelToPoint(85), PixelToPoint(25));
							doc.text("Guia de Inventário de Lotes", data.settings.margin.left + 10 + PixelToPoint(85), 26);
							
							// FOOTER
							var str = "Página " + data.pageCount;
							// Total page number plugin only available in jspdf v1.0+
							if (typeof doc.putTotalPages === 'function') 
							{
								str = str + " de " + totalPagesExp;
							}
							doc.setFontSize(10);
							doc.text(str, data.settings.margin.left, doc.internal.pageSize.height - 10);
						}
					});
			
					doc.autoTable(columns, data, 
					{
			//			        startY: doc.autoTable.previous.finalY + 15,
			//			addPageContent: pageContent,
			//			        margin: {top: 35},
						         theme: 'prodfy1'
			//			    bodyStyles: {valign: 'top'},
			//			        styles: {overflow: 'linebreak', columnWidth: 'wrap'}
			//			  columnStyles: {text: {columnWidth: 'auto'}}
					});
					
					
					// Total page number plugin only available in jspdf v1.0+
					if (typeof doc.putTotalPages === 'function') 
					{
						doc.putTotalPages(totalPagesExp);
					}
					
					doc.setProperties(
					{
						  title: 'Prodfy Mudas - Guia de Inventário',
						subject: 'Guia de Inventário'
					});
					
					
					
					doc.save("guia_de_inventario.pdf");
					
					//alert('lnk_relat1 - DONE!');
					return false;
				}
				break;
			default:
				break;
		}
	}
	
	
	$(document).on("f_show_relat", function(e,_id)
	{
		alert('_id = '+_id);
		
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de identificar qual relatório deve ser gerado!',' ','error');
			return false;
		}
		
		
		switch(_id)
		{
			case 1:
				alert('Relat: '+_id);
				var columns = ["ID", "Name", "Age", "City"];
				var data = [
				    [1, "Jonathan", 25, "Gothenburg"],
				    [2, "Simon", 23, "Gothenburg"],
				    [3, "Hanna", 21, "Stockholm"]
				];
				
				var doc = new jsPDF('p', 'pt');
				doc.setProperties({
            title: 'Example: ' + funcStr,
            subject: 'A jspdf-autotable example pdf (' + _id + ')'
        });
				doc.autoTable(columns, data);
				doc.save("table.pdf");
				alert('Done!');
				break;
			default:
				break;
		}
		return false;
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "6");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-relat-prod.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#div-label-type').text('EDITAR REGISTRO');
						$('#bt_register').hide();
						$('#bt_save').show();
						$('#reg_id').val(_id);
						hideLoadingBox();
						$('#cad-modal').modal('show');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	
	
	
	//Clear Inputs
	function clear_inputs()
	{
	}
	// Registrar
	$(document).on("f_show_register", function(e)
	{
		$('#div-label-type').text('NOVO REGISTRO');
		$('#bt_register').show();
		$('#bt_save').hide();
		clear_inputs();
		$('#cad-modal').modal('show');
		return false;
	});
	


	// Editar
	$(document).on("f_show_edit", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de executar edição do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "6");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-relat-prod.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#div-label-type').text('EDITAR REGISTRO');
						$('#bt_register').hide();
						$('#bt_save').show();
						$('#reg_id').val(_id);
						hideLoadingBox();
						$('#cad-modal').modal('show');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	$('#bt_save').click( function () 
	{
		//Ajuste de conteudo
		
		var rg_id = $('#reg_id').val();
		
		//ID
		if (isEmpty(rg_id)) { show_msgbox('Incapaz de determinar qual registro deve ser editado!',' ','error'); return false; }
		
		//Formatacao Case
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "3");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", rg_id);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-relat-prod.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						$('#cad-modal').modal('hide');
						clear_inputs();
						//Refresh datatable
						refresh_datatable();
						
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	
	//Ficha Consolidada
	$(document).on("f_show_ficha", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de carregar ficha do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "7");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-relat-prod.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						hideLoadingBox();
						show_msgbox(myRet.text,'Ficha do Registro','ficha');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Processa Form ~ STOP
	

});

