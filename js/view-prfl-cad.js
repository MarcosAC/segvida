/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-relat-etiquetas.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  ################################################################################# */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=relat-etiquetas');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	
	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	/*$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});*/
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
	}
	////////////////////// LOADINGBOX ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Processa Form ~ START
	
	//NOME
	$(document).on("blur", "#cad_nome", function(){
		if( !frm_valida_nome(this.value,true) ){ return false; }
	});
	function frm_valida_nome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo NOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('NOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//SOBRENOME
	$(document).on("blur", "#cad_sobrenome", function(){
		if( !frm_valida_sobrenome(this.value,true) ){ return false; }
	});
	function frm_valida_sobrenome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('SOBRENOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//EMAIL
	$(document).on("blur", "#cad_email", function(){
		if( !frm_valida_email(this.value,true) ){ return false; }
	});
	function frm_valida_email(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( !emailReg.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um E-MAIL válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CPF
	$(document).on("blur", "#cad_cpf", function(){
		setContent($('#cad_cpf'), 'CPF');
		if( !frm_valida_cpf(this.value,true) ){ return false; }
	});
	function frm_valida_cpf(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CPF é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cpf(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CPF válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	
	//CNPJ
	$(document).on("blur", "#cad_cnpj", function(){
		setContent($('#cad_cnpj'), 'CNPJ');
		if(!isEmpty(this.value))
		{
			if( !frm_valida_cnpj(this.value,true) ){ return false; }
		}
	});
	function frm_valida_cnpj(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CNPJ é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cnpj(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CNPJ válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function chk_cnpj(s)
	{
		var i;
		var c = s.substr(0,12);
		var dv = s.substr(12,2);
		var d1 = 0;
		//
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+(i % 8)); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+((i+1) % 8)); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	
	//FONE_FIXO1
	$(document).on("blur", "#cad_fonefixo1", function(){
		setContent($('#cad_fonefixo1'), 'FONE');
		//if(!isEmpty(this.value))
		{
			if( !frm_valida_fonefixo1(this.value,true,false) ){ return false; }
		}
	});
	function frm_valida_fonefixo1(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE TELEFONE válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CELULAR1
	$(document).on("blur", "#cad_celular1", function(){
		setContent($('#cad_celular1'), 'FONE');
		if(!isEmpty(this.value))
		{
			if( !frm_valida_celular1(this.value,true,false) ){ return false; }
		}
	});
	function frm_valida_celular1(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CELULAR é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE CELULAR válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CEP
	$(document).on("blur", "#cad_cep", function(){
		setContent($('#cad_cep'), 'CEP');
		var _cep = $('#cad_cep').val();
		if( !frm_valida_cep(_cep,true) ){ return false; }
	});
	function frm_valida_cep(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CEP é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 8 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CEP válido!',' ','alert');
			}
			return false;
		}
		else if( checa_existe_cep(_tmp) == 'nao_existe')
		{
			if( ret_msg )
			{
				show_msgbox('O CEP <b>'+_tmp+'</b> não existe!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function checa_existe_cep(_cep)
	{
		//Show trobbler
		showLoadingBox('Verificando CEP...');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("cep", _cep);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/cep.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], uf:tmp[1], cidade:tmp[2], bairro:tmp[3], logradouro:tmp[4], codigo:tmp[5]};
					//
					if(myRet.status == 1)
					{
						$('#cad_cep_uf').val(myRet.uf);
						$('#cad_uf').val(myRet.uf);
						
						$('#cad_cep_cidade_codigo').val(myRet.codigo); 
						//frm_gera_options_cidades(_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo)
						frm_gera_options_cidades(myRet.uf,'cad_cidade',1,'Selecione a cidade','',myRet.codigo);
						
						$('#cad_bairro').val(myRet.bairro);
						$('#cad_end').val(myRet.logradouro);
						//
						hideLoadingBox();
						return true;
					}
					else
					{
						hideLoadingBox();
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return 'nao_existe';
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//UF
	$(document).on("change", "#cad_uf", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_uf(this.value,true) ){ return false; }
			//_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo
			frm_gera_options_cidades(this.value,'cad_cidade',1,'Selecione a cidade','','');
		}
	});
	function frm_valida_uf(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo ESTADO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CIDADE
	$(document).on("change", "#cad_cidade", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_cidade(this.value,true) ){ return false; }
		}
	});
	function frm_valida_cidade(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CIDADE é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//BAIRRO
	$(document).on("blur", "#cad_bairro", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_bairro(this.value,true) ){ return false; }
		}
	});
	function frm_valida_bairro(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo BAIRRO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//ENDERECO
	$(document).on("blur", "#cad_end", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_endereco(this.value,true) ){ return false; }
		}
	});
	function frm_valida_endereco(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo ENDEREÇO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//NUMERO
	$(document).on("blur", "#cad_numero", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_numero(this.value,true) ){ return false; }
		}
	});
	function frm_valida_numero(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo NÚMERO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//COMPLEMENTO
	//$(document).on("blur", "#cad_compl", function()
	//{
	//	if(!isEmpty(this.value))
	//	{ 
	//		if( !frm_valida_complemento(this.value,true) ){ return false; }
	//	}
	//});
	//function frm_valida_complemento(value,ret_msg)
	//{
	//	var _tmp = value;
	//	if( isEmpty(_tmp) )
	//	{
	//		if( ret_msg )
	//		{
	//			show_msgbox('O campo COMPLEMENTO é obrigatório!',' ','alert');
	//		}
	//		return false;
	//	}
	//	return true;
	//}
	
	//SENHA_ATUAL
	//$(document).on("blur", "#cad_password", function(){
	//	if( !frm_valida_senha_atual(this.value,true) ){ return false; }
	//});
	//function frm_valida_senha_atual(value,ret_msg)
	//{
	//	var _tmp = value;
	//	if( isEmpty(_tmp) )
	//	{
	//		if( ret_msg )
	//		{
	//			show_msgbox('O campo SENHA ATUAL é obrigatório!',' ','alert');
	//		}
	//		return false;
	//	}
	//	return true;
	//}
	
	//BT_CAD_SALVAR
	$('#bt_cad_salvar').click( function () 
	{
		//var rg_username          = $('#cad_username').val();
		var rg_nome              = $('#cad_nome').val();
		var rg_sobrenome         = $('#cad_sobrenome').val();
		var rg_email             = $('#cad_email').val();
		var rg_cpf               = $('#cad_cpf').val();
		var rg_cnpj              = $('#cad_cnpj').val();
		var rg_razao             = $('#cad_razaosocial').val();
		var rg_fonefixo1         = $('#cad_fonefixo1').val();
		var rg_celular1          = $('#cad_celular1').val();
		var rg_pais              = $('#cad_pais').val();
		var rg_idioma            = $('#cad_idioma').val();
		var rg_cep               = $('#cad_cep').val();
		var rg_cep_uf            = $('#cad_cep_uf').val();
		var rg_cep_cidade_codigo = $('#cad_cep_cidade_codigo').val();
		var rg_uf                = $('#cad_uf').val();
		var rg_cidade            = $('#cad_cidade').val();
		var rg_bairro            = $('#cad_bairro').val();
		var rg_end               = $('#cad_end').val();
		var rg_num               = $('#cad_numero').val();
		var rg_compl             = $('#cad_compl').val();
		var rg_password          = $('#cad_password').val();
		
		setContent($('#cad_cpf'), 'CPF');
		setContent($('#cad_cnpj'), 'CNPJ');
		setContent($('#cad_fonefixo1'), 'FONE');
		setContent($('#cad_celular1'), 'FONE');
		setContent($('#cad_cep'), 'CEP');
		
		if (isEmpty(rg_nome))         { show_msgbox('O campo NOME é obrigatório!',' ','alert');               $('#cad_nome').focus();         return false; }
		if (isEmpty(rg_sobrenome))    { show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');          $('#cad_sobrenome').focus();    return false; }
		if (isEmpty(rg_email))        { show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');             $('#cad_email').focus();        return false; }
		if (!emailReg.test(rg_email)) { show_msgbox('Informe um e-Mail válido!',' ','alert');                 $('#cad_email').focus();        return false; }
		
		if (isEmpty(rg_cpf))          { show_msgbox('O campo CPF é obrigatório!',' ','alert');                $('#cad_cpf').focus();          return false; }
		//if (isEmpty(rg_cnpj))       { show_msgbox('O campo CNPJ é obrigatório!',' ','alert');               $('#cad_cnpj').focus();         return false; }
		//if (isEmpty(rg_razao))      { show_msgbox('O campo RAZÃO SOCIAL é obrigatório!',' ','alert');       $('#cad_razaosocial').focus();        return false; }
		if (isEmpty(rg_fonefixo1))    { show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');      $('#cad_fonefixo1').focus();    return false; }
		//if (isEmpty(rg_celular1))   { show_msgbox('O campo CELULAR é obrigatório!',' ','alert');            $('#cad_celular1').focus();     return false; }
		if (isEmpty(rg_pais))         { show_msgbox('O campo PAÍS é obrigatório!',' ','alert');                $('#cad_pais').focus();          return false; }
		if (isEmpty(rg_idioma))       { show_msgbox('O campo IDIOMA é obrigatório!',' ','alert');              $('#cad_idioma').focus();        return false; }
		if (isEmpty(rg_cep))          { show_msgbox('O campo CEP é obrigatório!',' ','alert');                $('#cad_cep').focus();          return false; }
		
		if (isEmpty(rg_uf))       { show_msgbox('O campo ESTADO é obrigatório!',' ','alert');             $('#cad_uf').focus();       return false; }
		if(!isEmpty(rg_cep_uf))
		{
			if(rg_uf != rg_cep_uf){ show_msgbox('O ESTADO informado não confere com o ESTADO do CEP informado!',' ','alert'); $('#cad_uf').focus(); return false; }
		}
		
		if (isEmpty(rg_cidade))       { show_msgbox('O campo CIDADE é obrigatório!',' ','alert'); $('#cad_cidade').focus(); return false; }
		if(!isEmpty(rg_cep_cidade_codigo))
		{
			if(rg_cidade != rg_cep_cidade_codigo){ show_msgbox('A CIDADE informada não confere com a CIDADE do CEP informado!',' ','alert'); $('#cad_cidade').focus(); return false; }
		}
		
		if (isEmpty(rg_bairro))       { show_msgbox('O campo BAIRRO é obrigatório!',' ','alert');             $('#cad_bairro').focus();       return false; }
		if (isEmpty(rg_end))          { show_msgbox('O campo ENDEREÇO é obrigatório!',' ','alert');           $('#cad_end').focus();          return false; }
		if (isEmpty(rg_num))          { show_msgbox('O campo NÚMERO DO ENDEREÇO é obrigatório!',' ','alert'); $('#cad_num').focus();          return false; }
		//if (isEmpty(rg_compl))      { show_msgbox('O campo COMPLEMENTO é obrigatório!',' ','alert');        $('#cad_compl').focus();        return false; }
		
		
		//Username
		//if (isEmpty(rg_username))     { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert'); $('#cad_username').focus(); return false; }
		
		//Password
		//if (isEmpty(rg_password))     { show_msgbox('O campo SENHA ATUAL é obrigatório!',' ','alert'); $('#cad_password').focus(); return false; }
		
		//Formatacao
		rg_nome      = rg_nome.toUpperCase();
		rg_sobrenome = rg_sobrenome.toUpperCase();
		rg_email     = rg_email.toLowerCase();
		rg_razao     = rg_razao.toUpperCase();
		rg_bairro    = rg_bairro.toUpperCase();
		rg_end       = rg_end.toUpperCase();
		rg_num       = rg_num.toUpperCase();
		rg_compl     = rg_compl.toUpperCase();
		//rg_username  = rg_username.toUpperCase();
		
		//Criptografa senha
		//var rg_password_crypted = hex_sha512(rg_password);
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "2");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("nome", rg_nome);
		_postdata.append("sobrenome", rg_sobrenome);
		_postdata.append("email", rg_email);
		_postdata.append("cpf", rg_cpf);
		_postdata.append("cnpj", rg_cnpj);
		_postdata.append("razao", rg_razao);
		_postdata.append("fonefixo1", rg_fonefixo1);
		_postdata.append("celular1", rg_celular1);
		_postdata.append("pais", rg_pais);
		_postdata.append("cep", rg_cep);
		_postdata.append("uf", rg_uf);
		_postdata.append("cidade", rg_cidade);
		_postdata.append("bairro", rg_bairro);
		_postdata.append("end", rg_end);
		_postdata.append("num", rg_num);
		_postdata.append("compl", rg_compl);
		
		do_cad(_postdata);
		
		$(".form-control").removeAttr("disabled");
		
		return false;
	});
	function do_cad(_data)
	{
		//alert(base_url + "/lib-bin/mdl-prfl-cad.php?"+_data);
		//return false;
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-prfl-cad.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Atualiza cabecalho da pagina
						var tmp_info = $('#cad_nome').val().toUpperCase()+' '+$('#cad_sobrenome').val().toUpperCase()+' &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp; CPF: '+$('#cad_cpf').val();
						$('#panel_dados_basicos_identificacao').empty().append(tmp_info);
						
						//Clear
//						$('#cad_nome').val('');
//						$('#cad_sobrenome').val('');
//						$('#cad_email').val('');
//						$('#cad_cpf').val('');
//						$('#cad_cnpj').val('');
//						$('#cad_razaosocial').val('');
//						$('#cad_fonefixo1').val('');
//						$('#cad_celular1').val('');
//						$('#cad_cep').val('');
//						$('#cad_uf').val('');
//						$('#cad_cidade').val('');
//						$('#cad_bairro').val('');
//						$('#cad_end').val('');
//						$('#cad_num').val('');
//						$('#cad_compl').val('');
//						//$('#cad_password').val('');
						hideLoadingBox();
						$(".form-control").removeAttr("disabled");
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						
						return false;
					}
					else
					{
						hideLoadingBox();
						$(".form-control").removeAttr("disabled");
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	////////////////////// Processa Form ~ STOP
	
	
	
	
	
	
	////////////////////// Processa Foto ~ START
	//BT_ENVIAR_FOTO
	$('#bt_enviar_foto').click( function () 
	{
		//Fecha modal do upload
		$('#alterafoto-modal').modal('hide');
		
		var _postdata = new FormData(document.getElementById("alterafoto-form"));
		_postdata.append("dbo", "4");
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		//$('.form-control').attr('disabled','true');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-prfl-cad.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], filename:tmp[3]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						$('#file_info').val('');
						var control = $('#file_upload');
						control.replaceWith( control = control.clone( true ) );
						//
						if(myRet.filename)
						{
							$('#profile_pic').attr('src', 'img/profile_pics/'+myRet.filename);
						}
						//
						hideLoadingBox();
						//$(".form-control").removeAttr("disabled");
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$('#file_info').val('');
						var control = $('#file_upload');
						control.replaceWith( control = control.clone( true ) );
						//
						hideLoadingBox();
						//$(".form-control").removeAttr("disabled");
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	////////////////////// Processa Foto ~ STOP
	
	
	
	
	
	////////////////////// Automacao de Form ~ START
	function frm_gera_options_cidades(_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo)
	{
		if(isEmpty(_uf))
		{
			show_msgbox('Selecione um Estado para exibir a lista de cidades!',' ','alert');
			return false;
		}
		if(isEmpty(_target_panel))
		{
			show_msgbox('TARGET não indicado!',' ','error');
			return false;
		}
		
		//if(!isEmpty(_setfirstblank)) { var _setfirstblank=1; } else { var _setfirstblank=0; }
		
		//Show trobbler
		showLoadingBox('Carregando cidades...');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("uf", _uf);
		
		if(!isEmpty(_codigo)){ _postdata.append("c", _codigo); }
		if(!isEmpty(_cidade)){ _postdata.append("cdd", _cidade); }
		if(_setfirstblank == 1)
		{
			_postdata.append("fb", _setfirstblank);
			if(!isEmpty(_firstblanktext)){ _postdata.append("fbt", _firstblanktext); }
		}
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_options_cidades.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/gera_options_cidades.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					//
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					if(myRet.status == 1)
					{
						$('#'+_target_panel).empty().append(myRet.text);
						hideLoadingBox();
						return false;
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	function frm_processa_cep(_cep)
	{
		if(isEmpty(_cep))
		{
			show_msgbox('Informe um CEP para busca.',' ','alert');
			return false;
		}
		if( _cep.length < 8 )
		{
			show_msgbox('Entre com um CEP válido.',' ','alert');
			return false;
		}
		
		//Show trobbler
		showLoadingBox('Verificando CEP...');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("cep", _cep);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/cep.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					//alert("response => "+response);
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], uf:tmp[1], cidade:tmp[2], bairro:tmp[3], logradouro:tmp[4], codigo:tmp[5]};
					//
					if(myRet.status == 1)
					{
						if(!isEmpty(myRet.uf))
						{
							$('#cad_cep_uf').val(myRet.uf);
							$('#cad_uf').val(myRet.uf);
						}
						if(!isEmpty(myRet.cidade))
						{ 
							$('#cad_cep_cidade_codigo').val(myRet.codigo); 
							//_estado,_target_panel,_field_id,_required,_setfirstblank,_firstblanktext,_cidade,_codigo)
							frm_mostra_cidades(myRet.uf,'cidade_panel','cad_cidade',1,1,'*Cidade',myRet.cidade);
						}
						if(!isEmpty(myRet.bairro)){ $('#cad_bairro').val(myRet.bairro); }
						if(!isEmpty(myRet.logradouro)){ $('#cad_end').val(myRet.logradouro); }
						//
						hideLoadingBox();
						return false;
					}
					else
					{
						hideLoadingBox();
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	////////////////////// Automacao de Form ~ STOP
	
	
	
});