/* #####################################################################
   # SEGVIDA - USUARIOS
   ##################################################################### */
$(document).ready(function() 
{
	
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		//var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	//alert(tmp0[1]+' = '+tmp1[1]);
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/usuarios.php');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	
	
	
		
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() 
	{
		$('#loadingBoxText').text('Processando...');
		$('#loadingBox').modal('show');
	}).ajaxStop(function() {
		$('#loadingBox').modal('hide');
	});
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	//alert("language => "+$.Storage.loadItem('language'));
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#reg_captcha_refresh_btn", function()
	{
		$("#reg_captcha_code").val('');
		$('#reg_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#reg_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Processa Form ~ START
	
	//NOME
	$(document).on("blur", "#reg_nome", function()
	{
		if( !frm_valida_nome(this.value,true) ){ return false; }
	});
	function frm_valida_nome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo NOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('NOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//SOBRENOME
	$(document).on("blur", "#reg_sobrenome", function()
	{
		if( !frm_valida_sobrenome(this.value,true) ){ return false; }
	});
	function frm_valida_sobrenome(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 3 || _tmp.length > 30 )
		{
			if( ret_msg )
			{
				show_msgbox('SOBRENOME deve ter de 3 a 32 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//EMAIL
	$(document).on("blur", "#reg_email", function()
	{
		if( !frm_valida_email(this.value,true) ){ return false; }
	});
	function frm_valida_email(value,ret_msg)
	{
		var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( !re.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um E-MAIL válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CPF
	$(document).on("blur", "#reg_cpf", function()
	{
		setContent($('#reg_cpf'), 'CPF');
		if( !frm_valida_cpf(this.value,true) ){ return false; }
	});
	function frm_valida_cpf(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CPF é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cpf(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CPF válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	
	//CNPJ
	$(document).on("blur", "#reg_cnpj", function()
	{
		setContent($('#reg_cnpj'), 'CNPJ');
		if(!isEmpty(this.value))
		{
			if( !frm_valida_cnpj(this.value,true) ){ return false; }
		}
	});
	function frm_valida_cnpj(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CNPJ é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cnpj(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CNPJ válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function chk_cnpj(s)
	{
		var i;
		var c = s.substr(0,12);
		var dv = s.substr(12,2);
		var d1 = 0;
		//
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+(i % 8)); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+((i+1) % 8)); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	
	//FONE_FIXO1
	$(document).on("blur", "#reg_fonefixo1", function()
	{
		setContent($('#reg_fonefixo1'), 'FONE');
		//if(!isEmpty(this.value))
		{
			if( !frm_valida_fonefixo1(this.value,true,false) ){ return false; }
		}
	});
	function frm_valida_fonefixo1(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE TELEFONE válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CELULAR1
	$(document).on("blur", "#reg_celular1", function()
	{
		setContent($('#reg_celular1'), 'FONE');
		if(!isEmpty(this.value))
		{
			if( !frm_valida_celular1(this.value,true,false) ){ return false; }
		}
	});
	function frm_valida_celular1(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CELULAR é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 10 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um NÚMERO DE CELULAR válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CEP
	$(document).on("blur", "#reg_cep", function()
	{
		setContent($('#reg_cep'), 'CEP');
		var _cep = $('#reg_cep').val();
		if( !frm_valida_cep(_cep,true) ){ return false; }
	});
	function frm_valida_cep(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CEP é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 8 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CEP válido!',' ','alert');
			}
			return false;
		}
		else if( checa_existe_cep(_tmp) == 'nao_existe')
		{
			if( ret_msg )
			{
				show_msgbox('O CEP <b>'+_tmp+'</b> não existe!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function checa_existe_cep(_cep)
	{
		//Show trobbler
		$('#loadingBoxText').text('Validando CEP...');
		$('#loadingBox').modal('show');
		
		//Formata postdata
		var _postdata = "cep=" + encodeURIComponent(_cep);
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/cep.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/cep.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], uf:tmp[1], cidade:tmp[2], bairro:tmp[3], logradouro:tmp[4], codigo:tmp[5]};
					//
					if(myRet.status == 1)
					{
						$('#reg_cep_uf').val(myRet.uf);
						$('#reg_uf').val(myRet.uf);
						
						$('#reg_cep_cidade_codigo').val(myRet.codigo); 
						frm_gera_options_cidades(myRet.uf,'reg_cidade',1,'Selecione a cidade','',myRet.codigo);
						
						$('#reg_bairro').val(myRet.bairro);
						$('#reg_end').val(myRet.logradouro);
						//
						$('#loadingBox').modal('hide');
						return true;
					}
					else
					{
						$('#loadingBox').modal('hide');
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return 'nao_existe';
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//UF
	$(document).on("change", "#reg_uf", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_uf(this.value,true) ){ return false; }
			//_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo
			frm_gera_options_cidades(this.value,'reg_cidade',1,'Selecione a cidade','','');
		}
	});
	function frm_valida_uf(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo ESTADO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//CIDADE
	$(document).on("change", "#reg_cidade", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_cidade(this.value,true) ){ return false; }
		}
	});
	function frm_valida_cidade(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CIDADE é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//BAIRRO
	$(document).on("blur", "#reg_bairro", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_bairro(this.value,true) ){ return false; }
		}
	});
	function frm_valida_bairro(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo BAIRRO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//ENDERECO
	$(document).on("blur", "#reg_end", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_endereco(this.value,true) ){ return false; }
		}
	});
	function frm_valida_endereco(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo ENDEREÇO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//NUMERO
	$(document).on("blur", "#reg_numero", function()
	{
		if(!isEmpty(this.value))
		{ 
			if( !frm_valida_numero(this.value,true) ){ return false; }
		}
	});
	function frm_valida_numero(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo NÚMERO é obrigatório!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	//COMPLEMENTO
	//$(document).on("blur", "#reg_compl", function()
	//{
	//	if(!isEmpty(this.value))
	//	{ 
	//		if( !frm_valida_complemento(this.value,true) ){ return false; }
	//	}
	//});
	//function frm_valida_complemento(value,ret_msg)
	//{
	//	var _tmp = value;
	//	if( isEmpty(_tmp) )
	//	{
	//		if( ret_msg )
	//		{
	//			show_msgbox('O campo COMPLEMENTO é obrigatório!',' ','alert');
	//		}
	//		return false;
	//	}
	//	return true;
	//}
	
	//USERNAME
	$(document).on("blur", "#reg_username", function()
	{
		if( !frm_valida_username(this.value,true) ){ return false; }
	});
	function frm_valida_username(value,ret_msg)
	{
		re = /^\w+$/;//apenas letras, numeros e underlines
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( (_tmp.length < 6) || (_tmp.length > 30) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um USUÁRIO válido!',' ','alert');
			}
			return false;
		}
		else if(!re.test(_tmp))
		{ 
			show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert');
			return false;
		}
		else if( checa_existe_username(_tmp) == "ja_em_uso" )
		{
			if( ret_msg )
			{
				show_msgbox('O usuário <b>'+_tmp+'</b> já está sendo usado!',' ','alert');
			}
			return false;
		}
		
		return true;
	}
	function checa_existe_username(_username)
	{
		//Verifica captcha
		var _postdata = "l="   + encodeURIComponent($.Storage.loadItem('language'))
		              + "&u=" + encodeURIComponent(_username);
		
		$.ajax({
				 url: base_url + "/lib-bin/existe_username.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						return "ja_em_uso";
					}
					else
					{
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//SENHA1
	$(document).on("blur", "#reg_password", function()
	{
		if( !frm_valida_senha1(this.value,true) ){ return false; }
	});
	function frm_valida_senha1(value,ret_msg)
	{
		// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		else if (!re.test(_tmp))
		{
			show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert');
			return false;
		}
		return true;
	}
	
	//SENHA2
	$(document).on("blur", "#reg_password2", function()
	{
		if( !frm_valida_senha2(this.value,true) ){ return false; }
		if( !frm_valida_senhas($('#reg_password').val(),this.value,true) ){ return false; }
		$('#reg_captcha_refresh_btn').trigger('click');
	});
	function frm_valida_senha2(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CONFIRMAÇÃO DE SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma CONFIRMAÇÃO DE SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_senhas(s1,s2,ret_msg)
	{
		var _tmp1 = s1;
		var _tmp2 = s2;
		if( _tmp1 != _tmp2 )
		{
			if( ret_msg )
			{
				show_msgbox('Os campos SENHA e CONFIRMAÇÃO DE SENHA não conferem!',' ','alert');
			}
			return false;
		}
		return true;
	}
	////////////////////// Processa Form ~ STOP
	
	
	////////////////////// Login Form ~ START
	var $formRegister = $('#reg-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	
	$('#bt_register').click( function () 
	{
		var rg_nome              = $('#reg_nome').val();
		var rg_sobrenome         = $('#reg_sobrenome').val();
		var rg_email             = $('#reg_email').val();
		var rg_cpf               = $('#reg_cpf').val();
		var rg_cnpj              = $('#reg_cnpj').val();
		var rg_razao             = $('#reg_razaosocial').val();
		var rg_fonefixo1         = $('#reg_fonefixo1').val();
		var rg_celular1          = $('#reg_celular1').val();
		var rg_pais              = $('#reg_pais').val();
		var rg_idioma            = $('#reg_idioma').val();
		var rg_cep               = $('#reg_cep').val();
		var rg_cep_uf            = $('#reg_cep_uf').val();
		var rg_cep_cidade_codigo = $('#reg_cep_cidade_codigo').val();
		var rg_estado            = $('#reg_uf').val();
		var rg_cidade            = $('#reg_cidade').val();
		var rg_bairro            = $('#reg_bairro').val();
		var rg_end               = $('#reg_end').val();
		var rg_num               = $('#reg_num').val();
		var rg_compl             = $('#reg_compl').val();
		var rg_username          = $('#reg_username').val();
		var rg_password          = $('#reg_password').val();
		var rg_password2         = $('#reg_password2').val();
		var rg_captcha_code      = $('#reg_captcha_code').val();
		var rg_termos            = $('#reg_termos').is(':checked');
		
		setContent($('#reg_cpf'), 'CPF');
		setContent($('#reg_cnpj'), 'CNPJ');
		setContent($('#reg_fonefixo1'), 'FONE');
		setContent($('#reg_celular1'), 'FONE');
		setContent($('#reg_cep'), 'CEP');
		
		if (isEmpty(rg_nome))         { show_msgbox('O campo NOME é obrigatório!',' ','alert');               $('#reg_nome').focus();         return false; }
		if (isEmpty(rg_sobrenome))    { show_msgbox('O campo SOBRENOME é obrigatório!',' ','alert');          $('#reg_sobrenome').focus();    return false; }
		if (isEmpty(rg_email))        { show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');             $('#reg_email').focus();        return false; }
		if (!emailReg.test(rg_email)) { show_msgbox('Informe um e-Mail válido!',' ','alert');                 $('#reg_email').focus();        return false; }
		
		if (isEmpty(rg_cpf))          { show_msgbox('O campo CPF é obrigatório!',' ','alert');                $('#reg_cpf').focus();          return false; }
		//if (isEmpty(rg_cnpj))       { show_msgbox('O campo CNPJ é obrigatório!',' ','alert');               $('#reg_cnpj').focus();         return false; }
		//if (isEmpty(rg_razao))      { show_msgbox('O campo RAZÃO SOCIAL é obrigatório!',' ','alert');       $('#reg_razaosocial').focus();        return false; }
		if (isEmpty(rg_fonefixo1))    { show_msgbox('O campo TELEFONE FIXO é obrigatório!',' ','alert');      $('#reg_fonefixo1').focus();    return false; }
		//if (isEmpty(rg_celular1))   { show_msgbox('O campo CELULAR é obrigatório!',' ','alert');            $('#reg_celular1').focus();     return false; }
		if (isEmpty(rg_pais))         { show_msgbox('O campo PAÍS é obrigatório!',' ','alert');               $('#reg_pais').focus();         return false; }
		if (isEmpty(rg_idioma))       { show_msgbox('O campo IDIOMA é obrigatório!',' ','alert');             $('#reg_idioma').focus();       return false; }
		if (isEmpty(rg_cep))          { show_msgbox('O campo CEP é obrigatório!',' ','alert');                $('#reg_cep').focus();          return false; }
		
		if (isEmpty(rg_estado))       { show_msgbox('O campo ESTADO é obrigatório!',' ','alert');             $('#reg_uf').focus();       return false; }
		if(!isEmpty(rg_cep_uf))
		{
			if(rg_estado != rg_cep_uf){ show_msgbox('O ESTADO informado não confere com o ESTADO do CEP informado!',' ','alert'); $('#reg_uf').focus(); return false; }
		}
		
		if (isEmpty(rg_cidade))       { show_msgbox('O campo CIDADE é obrigatório!',' ','alert'); $('#reg_cidade').focus(); return false; }
		if(!isEmpty(rg_cep_cidade_codigo))
		{
			if(rg_cidade != rg_cep_cidade_codigo){ show_msgbox('A CIDADE informada não confere com a CIDADE do CEP informado!',' ','alert'); $('#reg_cidade').focus(); return false; }
		}
		
		if (isEmpty(rg_bairro))       { show_msgbox('O campo BAIRRO é obrigatório!',' ','alert');             $('#reg_bairro').focus();       return false; }
		if (isEmpty(rg_end))          { show_msgbox('O campo ENDEREÇO é obrigatório!',' ','alert');           $('#reg_end').focus();          return false; }
		if (isEmpty(rg_num))          { show_msgbox('O campo NÚMERO DO ENDEREÇO é obrigatório!',' ','alert'); $('#reg_num').focus();          return false; }
		//if (isEmpty(rg_compl))      { show_msgbox('O campo COMPLEMENTO é obrigatório!',' ','alert');        $('#reg_compl').focus();        return false; }
		
		
		//Username
		if (isEmpty(rg_username))     { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');           $('#reg_username').focus();     return false; }
		re = /^\w+$/; 
		if(!re.test(rg_username))     { show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert'); $('#reg_username').focus();     return false; }
		if( checa_existe_username(rg_username) == "ja_em_uso" ){ show_msgbox('O usuário <b>'+rg_username+'</b> já está sendo usado!',' ','alert'); $('#reg_username').focus();     return false; }
		
		//Password
		if (isEmpty(rg_password))     { show_msgbox('O campo SENHA é obrigatório!',' ','alert');              $('#reg_password').focus();     return false; }
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		if (!re.test(rg_password))    { show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert'); $('#reg_password').focus(); return false; }
		if (isEmpty(rg_password2))    { show_msgbox('O campo SENHA PARA CONFIRMAÇÃO é obrigatório!',' ','alert'); $('#reg_password2').focus(); return false; }
		if (rg_password != rg_password2) { show_msgbox('SENHA e CONFIRMAÇÃO não conferem!',' ','alert');     $('#reg_password').focus();  return false; }
		
		if (isEmpty(rg_captcha_code)) { show_msgbox('Informe o CÓDIGO DE SEGURANÇA!',' ','alert');            $('#reg_captcha_code').focus(); return false; }
		if (rg_termos != true)        { show_msgbox('Para se cadastrar, leia e concorde com o termo de serviço!',' ','alert');           $('#reg_termo').focus();          return false; }
		
		//Formatacao
		rg_idioma    = rg_idioma.toLowerCase();
		rg_nome      = rg_nome.toUpperCase();
		rg_sobrenome = rg_sobrenome.toUpperCase();
		rg_email     = rg_email.toLowerCase();
		rg_razao     = rg_razao.toUpperCase();
		rg_bairro    = rg_bairro.toUpperCase();
		rg_end       = rg_end.toUpperCase();
		rg_num       = rg_num.toUpperCase();
		rg_compl     = rg_compl.toUpperCase();
		rg_username  = rg_username.toUpperCase();
		
		//Criptografa senha
		var rg_password_crypted = hex_sha512(rg_password);
		
		//Show trobbler
		$('#loadingBoxText').text('Processando...');
		$('#loadingBox').modal('show');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		
		//Verifica captcha
		var _postdata = "lang=" + encodeURIComponent($.Storage.loadItem('language'))
		              + "&code=" + encodeURIComponent(rg_captcha_code);
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		
		$.ajax({
				 url: base_url + "/lib-bin/captcha/chk_code.cgi",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 0)
					{
						$('#reg_captcha_refresh_btn').trigger('click');
						$('#reg_captcha_code').val('');
						$('#reg_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox('CÓDIGO DE SEGURANÇA inválido. Tente novamente!',' ','alert');
						return false;
					}
					else
					{
						//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
						var _postdata = "l="          + encodeURIComponent($.Storage.loadItem('language'))
							            + "&nome="      + encodeURIComponent(rg_nome)
							            + "&sobrenome=" + encodeURIComponent(rg_sobrenome)
							            + "&email="     + encodeURIComponent(rg_email)
							            + "&cpf="       + encodeURIComponent(rg_cpf)
							            + "&cnpj="      + encodeURIComponent(rg_cnpj)
							            + "&razao="     + encodeURIComponent(rg_razao)
							            + "&fonefixo1=" + encodeURIComponent(rg_fonefixo1)
							            + "&celular1="  + encodeURIComponent(rg_celular1)
							            + "&pais="      + encodeURIComponent(rg_pais)
							            + "&idioma="    + encodeURIComponent(rg_idioma)
							            + "&cep="       + encodeURIComponent(rg_cep)
							            + "&uf="        + encodeURIComponent(rg_estado)
							            + "&cidade="    + encodeURIComponent(rg_cidade)
							            + "&bairro="    + encodeURIComponent(rg_bairro)
							            + "&end="       + encodeURIComponent(rg_end)
							            + "&num="       + encodeURIComponent(rg_num)
							            + "&compl="     + encodeURIComponent(rg_compl)
							            + "&username="  + encodeURIComponent(rg_username)
							            + "&p="         + encodeURIComponent(rg_password_crypted)
							            ;
						
						//$('#loadingBox').modal('hide');
						do_cad(_postdata);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	function do_cad(_data)
	{
		//alert(base_url + "/lib-bin/registro.php?"+_data);
		//return false;
		
		$.ajax({
				 url: base_url + "/lib-bin/registro_user.php",
				type: "post",
		//dataType: "json",
				data: _data,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						$('#reg_nome').val('');
						$('#reg_sobrenome').val('');
						$('#reg_email').val('');
						$('#reg_cpf').val('');
						$('#reg_cnpj').val('');
						$('#reg_razaosocial').val('');
						$('#reg_fonefixo1').val('');
						$('#reg_celular1').val('');
						$('#reg_cep').val('');
						$('#reg_uf').val('');
						$('#reg_cidade').val('');
						$('#reg_bairro').val('');
						$('#reg_end').val('');
						$('#reg_num').val('');
						$('#reg_compl').val('');
						$('#reg_username').val('');
						$('#reg_password').val('');
						$('#reg_password2').val('');
						$('#reg_captcha_code').val('');
						$('#reg_captcha_refresh_btn').trigger('click');
						$("#reg_termos").prop('checked', false);
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$('#reg_captcha_refresh_btn').trigger('click');
						$('#reg_captcha_code').val('');
						$('#reg_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Login Form ~ STOP
	
	
	
	////////////////////// Automacao de Form ~ START
	function frm_gera_options_cidades(_uf,_target_panel,_setfirstblank,_firstblanktext,_cidade,_codigo)
	{
		if(isEmpty(_uf))
		{
			show_msgbox('Selecione um Estado para exibir a lista de cidades!',' ','alert');
			return false;
		}
		if(isEmpty(_target_panel))
		{
			show_msgbox('TARGET não indicado!',' ','error');
			return false;
		}
		
		//if(!isEmpty(_setfirstblank)) { var _setfirstblank=1; } else { var _setfirstblank=0; }
		
		
		//Show trobbler
		$('#loadingBoxText').text('Carregando cidades...');
		$('#loadingBox').modal('show');
		
		//Formata postdata
		var _postdata = "l="   + encodeURIComponent($.Storage.loadItem('language'))
			            + "&uf=" + encodeURIComponent(_uf)
			            ;
		
		if(!isEmpty(_codigo)){ _postdata = _postdata + "&c="   + encodeURIComponent(_codigo); }
		if(!isEmpty(_cidade)){ _postdata = _postdata + "&cdd="   + encodeURIComponent(_cidade); }
		if(_setfirstblank == 1)
		{
			_postdata = _postdata + "&fb="   + encodeURIComponent(_setfirstblank);
			if(!isEmpty(_firstblanktext)){ _postdata = _postdata + "&fbt="   + encodeURIComponent(_firstblanktext); }
		}
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_options_cidades.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/gera_options_cidades.php",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					//
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					if(myRet.status == 1)
					{
						$('#'+_target_panel).empty().append(myRet.text);
						$('#loadingBox').modal('hide');
						return false;
					}
					else
					{
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	////////////////////// Automacao de Form ~ STOP
	
	
	
});