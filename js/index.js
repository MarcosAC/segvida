/*#####################################################################
  # SEGVIDA - INDEX
  ##################################################################### */
$(document).ready(function() 
{
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		//var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	//alert(tmp0[1]+' = '+tmp1[1]);
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/index.php');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() 
	{
		$('#loadingBox').modal('show');
	}).ajaxStop(function() {
		$('#loadingBox').modal('hide');
	});
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	//alert("language => "+$.Storage.loadItem('language'));
	////////////////////// IDIOMA INICIAL ~ STOP
	
	////////////////////// CARREGA DADOS DE LOGIN ~ START
	//Se houver dados de login lembrados, carrega e popula os campos
	if($.Storage.loadItem('adm_login_remember') != '' && 
	   $.Storage.loadItem('adm_login_remember') != undefined)
	{
		$("#adm_login_remember").prop('checked', true);
		
		if($.Storage.loadItem('adm_login_username') != '' && 
		   $.Storage.loadItem('adm_login_username') != undefined)
		{
			$('#adm_login_username').val($.Storage.loadItem('adm_login_username'));
		}
		if($.Storage.loadItem('adm_login_password') != '' && 
		   $.Storage.loadItem('adm_login_password') != undefined)
		{
			$('#adm_login_password').val($.Storage.loadItem('adm_login_password'));
		}
		
	}
	//
	if($.Storage.loadItem('usr_login_remember') != '' && 
	   $.Storage.loadItem('usr_login_remember') != undefined)
	{
		$("#usr_login_remember").prop('checked', true);
		
		if($.Storage.loadItem('usr_login_username') != '' && 
		   $.Storage.loadItem('usr_login_username') != undefined)
		{
			$('#usr_login_username').val($.Storage.loadItem('usr_login_username'));
		}
		if($.Storage.loadItem('usr_login_password') != '' && 
		   $.Storage.loadItem('usr_login_password') != undefined)
		{
			$('#usr_login_password').val($.Storage.loadItem('usr_login_password'));
		}
		
	}
	////////////////////// CARREGA DADOS DE LOGIN ~ STOP
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').text(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').text(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#adm_login_captcha_refresh_btn", function()
	{
		$("#adm_login_captcha_code").val('');
		$('#adm_login_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		//var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		var captcha_src = base_url + '/securimage/securimage_show.php?'+numRand;
		$('#adm_login_captcha_img').attr('src', captcha_src);
		return false;
	});
	$(document).on("click", "#adm_lost_captcha_refresh_btn", function()
	{
		$("#adm_lost_captcha_code").val('');
		$('#adm_lost_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		//var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		var captcha_src = base_url + '/securimage/securimage_show.php?'+numRand;
		$('#adm_lost_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	
	// checa cpf
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}//endfunc
	
	// Checa CNPJ
	function chk_cnpj(s)
	{
		var i;
		var c = s.substr(0,12);
		var dv = s.substr(12,2);
		var d1 = 0;
		//
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+(i % 8)); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 12; i++){ d1 += c.charAt(11-i)*(2+((i+1) % 8)); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}//endfunc
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Login Form ~ START
	var $formLoginADM = $('#adm-login-form');
	var $formLostADM = $('#adm-lost-form');
	var $divFormsADM = $('#div-forms-adm');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	$(document).on("f_show_modal", function(e,_target_form)
	{
		if(_target_form == "adm")
		{
			$('#adm_login_captcha_refresh_btn').trigger('click');
		}
	});
	
	$('#adm_login_captcha_refresh_btn').trigger('click');
	
	
	
	function frm_valida_username(value,ret_msg)
	{
		re = /^\w+$/;//apenas letras, numeros e underlines
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( (_tmp.length < 6) || (_tmp.length > 30) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um USUÁRIO válido!',' ','alert');
			}
			return false;
		}
		else if(!re.test(_tmp))
		{ 
			show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert');
			return false;
		}
		else if( checa_existe_username(_tmp) == "ja_em_uso" )
		{
			if( ret_msg )
			{
				show_msgbox('O usuário <b>'+_tmp+'</b> já está sendo usado!',' ','alert');
			}
			return false;
		}
		
		return true;
	}
	function checa_existe_username(_username)
	{
		var _postdata = new FormData();
		_postdata.append("lang", $.Storage.loadItem('language'));
		_postdata.append("username", _username);
		
		$.ajax({
				 url: base_url + "/lib-bin/existe_username.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						return "ja_em_uso";
					}
					else
					{
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	
	$('#bt_adm_lost').click( function () 
	{
		var ls_adm_lost_username     = $('#adm_lost_username').val();
		var ls_adm_lost_email        = $('#adm_lost_email').val();
		var ls_adm_lost_captcha_code = $('#adm_lost_captcha_code').val();
		
		if (isEmpty(ls_adm_lost_username))     { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');            $('#adm_lost_username').focus();     return false; }
		if (isEmpty(ls_adm_lost_email))        { show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');             $('#adm_lost_email').focus();        return false; }
		if (!emailReg.test(ls_adm_lost_email)) { show_msgbox('Informe um e-Mail válido!',' ','alert');                 $('#adm_lost_email').focus();        return false; }
		if (isEmpty(ls_adm_lost_captcha_code)) { show_msgbox('Informe o CÓDIGO DE SEGURANÇA!',' ','alert');            $('#adm_lost_captcha_code').focus(); return false; }
		
		//Formatacao
		ls_adm_lost_username  = ls_adm_lost_username.toUpperCase();
		ls_adm_lost_email     = ls_adm_lost_email.toLowerCase();
		
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		
		var _postdata = new FormData();
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("c", ls_adm_lost_captcha_code);
		
		$(".form-control").removeAttr("disabled");
		
		$.ajax({
				 url: base_url + "/securimage/chk_code.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 0)
					{
						$('#adm_lost_captcha_refresh_btn').trigger('click');
						$('#adm_lost_captcha_code').val('');
						$('#adm_lost_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox('CÓDIGO DE SEGURANÇA inválido. Tente novamente!',' ','alert');
						return false;
					}
					else
					{
						var _postdata = new FormData();
						_postdata.append("l", $.Storage.loadItem('language'));
						_postdata.append("u", ls_adm_lost_username);
						_postdata.append("e", ls_adm_lost_email);
						
						//$('#loadingBox').modal('hide');
						do_lost_password('adm',_postdata);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					console.log("The following error occured: "+textStatus, errorThrown);
					$('#loadingBox').modal('hide');
					show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
					return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	function do_lost_password(_tipo,_data)
	{
		if(_tipo == 'adm'){ _url = base_url + "/lib-bin/lostpwd.php"; }
		if(_tipo == 'usr'){ _url = base_url + "/lib-bin/lostpwd_user.php"; }
		
		$.ajax({
				 url: _url,
				type: "post",
				//dataType: "json",
				data: _data,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						
						//Clear
						if(_tipo == 'adm')
						{
							$('#adm_lost_username').val('');
							$('#adm_lost_email').val('');
							$('#adm_lost_captcha_code').val('');
							$('#adm_lost_captcha_refresh_btn').trigger('click');
						}
						if(_tipo == 'usr')
						{
							$('#usr_lost_username').val('');
							$('#usr_lost_email').val('');
							$('#usr_lost_captcha_code').val('');
							$('#usr_lost_captcha_refresh_btn').trigger('click');
						}
						return false;
					}
					else
					{
						$('#loadingBox').modal('hide');
						
						if(_tipo == 'adm')
						{
							$('#adm_lost_captcha_refresh_btn').trigger('click');
							$('#adm_lost_captcha_code').val('');
							$('#adm_lost_captcha_code').focus();
						}
						if(_tipo == 'usr')
						{
							$('#usr_lost_captcha_refresh_btn').trigger('click');
							$('#usr_lost_captcha_code').val('');
							$('#usr_lost_captcha_code').focus();
						}
						
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					console.log("The following error occured: "+textStatus, errorThrown);
					$('#loadingBox').modal('hide');
					show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
					return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	$('#bt_login_adm').click( function () 
	{
		var lg_adm_login_username         = $('#adm_login_username').val();
		var lg_adm_login_password         = $('#adm_login_password').val();
		var lg_adm_login_remember         = $('#adm_login_remember').is(':checked');
		var lg_adm_login_captcha_code     = $('#adm_login_captcha_code').val();
		var lg_adm_login_password_crypted = hex_sha512(lg_adm_login_password);
		//
		if (isEmpty(lg_adm_login_username))     { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');  $('#adm_login_username').focus(); return false; }
		if (isEmpty(lg_adm_login_password))     { show_msgbox('O campo SENHA é obrigatório!',' ','alert');    $('#adm_login_password').focus(); return false; }
		if (isEmpty(lg_adm_login_captcha_code)) { show_msgbox('Informe o CÓDIGO DE SEGURANÇA!',' ','alert');  $('#adm_login_captcha_code').focus(); return false; }
		//
		// grava dados de login se solicitado
		if(lg_adm_login_remember == true)
		{
			$.Storage.saveItem('adm_login_username', lg_adm_login_username);
			$.Storage.saveItem('adm_login_password', lg_adm_login_password);
			$.Storage.saveItem('adm_login_remember', lg_adm_login_remember);
		}
		//
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		
		var _postdata = new FormData();
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("c", lg_adm_login_captcha_code);
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/securimage/chk_code.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 0)
					{
						$('#adm_login_captcha_refresh_btn').trigger('click');
						$('#adm_login_captcha_code').val('');
						$('#adm_login_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox('CÓDIGO DE SEGURANÇA inválido. Tente novamente!',' ','alert');
						return false;
					}
					else
					{
						var _postdata = new FormData();
						_postdata.append("l", $.Storage.loadItem('language'));
						_postdata.append("u", lg_adm_login_username);
						_postdata.append("p", lg_adm_login_password_crypted);
						
						//$('#loadingBox').modal('hide');
						do_login('adm',_postdata);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					console.log("The following error occured: "+textStatus, errorThrown);
					$('#loadingBox').modal('hide');
					show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
					return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	function do_login(_tipo,_data)
	{
		if(_tipo == 'adm'){ _url = base_url + "/lib-bin/login.php"; }
		if(_tipo == 'usr'){ _url = base_url + "/lib-bin/login_user.php"; }
		
		//alert(_url+'?'+_data);
		//return false;
		
		$.ajax({
				 url: _url,
				type: "post",
				//dataType: "json",
				data: _data,
				processData: false,
				contentType: false,
				cache: false,
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						$('#loadingBox').modal('hide');
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						document.location.href = 'main.php';
						
						//Clear
						//if(_tipo == 'adm')
						//{
						//	$('#login_adm_username').val('');
						//	$('#login_adm_password').val('');
						//	$('#login_adm_captcha_code').val('');
						//	$('#login_adm_captcha_refresh_btn').trigger('click');
						//}
						//if(_tipo == 'usr')
						//{
						//	$('#login_usr_username').val('');
						//	$('#login_usr_password').val('');
						//	$('#login_usr_captcha_code').val('');
						//	$('#login_usr_captcha_refresh_btn').trigger('click');
						//}
						
						return false;
					}
					else
					{
						$('#loadingBox').modal('hide');
							
						if(_tipo == 'adm')
						{
							$('#adm_login_captcha_refresh_btn').trigger('click');
							$('#adm_login_captcha_code').val('');
							$('#adm_login_captcha_code').focus();
						}
						if(_tipo == 'usr')
						{
							$('#usr_login_captcha_refresh_btn').trigger('click');
							$('#usr_login_captcha_code').val('');
							$('#usr_login_captcha_code').focus();
						}
						
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						$('#loadingBox').modal('hide');
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	//ADMIN
	$('#adm_login_lost_btn').click( function () 
	{ 
		$('#adm_lost_captcha_refresh_btn').trigger('click');
		//modalAnimate('adm',$formLoginADM, $formLostADM); 
	});
	
	
	function modalAnimate (_tipo, $oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		if(_tipo == 'adm')
		{
			$divFormsADM.css("height",$oldH);
			$oldForm.fadeToggle($modalAnimateTime, function()
			{
				$divFormsADM.animate({height: $newH}, $modalAnimateTime, function()
				{
					$newForm.fadeToggle($modalAnimateTime);
				});
			});
		}
	}
	
	//function modalAnimate ($oldForm, $newForm) 
	//{
	//	var $oldH = $oldForm.height();
	//	var $newH = $newForm.height();
	//	$divForms.css("height",$oldH);
	//	$oldForm.fadeToggle($modalAnimateTime, function()
	//	{
	//		$divForms.animate({height: $newH}, $modalAnimateTime, function()
	//		{
	//			$newForm.fadeToggle($modalAnimateTime);
	//		});
	//	});
	//}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Login Form ~ STOP
	
	
	
	
	
	
});