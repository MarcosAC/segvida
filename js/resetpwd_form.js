/* #####################################################################
   # SEGVIDA - ResetPWD Form
   ##################################################################### */
$(document).ready(function() 
{
	
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	//function getAbsolutePath() 
	//{
	//	var loc = window.location;
	//	//var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
	//	var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
	//	return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	//}
	//
	//tmp_site_url = getAbsolutePath();
	//var tmp0 = base_url.split('//');
	//var tmp1 = tmp_site_url.split('//');
	////alert(tmp0[1]+' = '+tmp1[1]);
	//
	////Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	//if(tmp0[1] != tmp1[1])
	//{
	//	window.location.replace(base_url+'/acesso.htm');
	//}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() 
	{
		$('#loadingBox').modal('show');
	}).ajaxStop(function() {
		$('#loadingBox').modal('hide');
	});
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	//alert("language => "+$.Storage.loadItem('language'));
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#reset_captcha_refresh_btn", function()
	{
		$("#reset_captcha_code").val('');
		$('#reset_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#reset_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Reset Form ~ START
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	//Atualiza Captcha
	$('#reset_captcha_refresh_btn').trigger('click');
	
	//$(document).on("blur", "#register_username", function(){
	//	if( !frm_valida_username(this.value,true) ){ return false; }
	//});
	$(document).on("blur", "#reset_password", function(){
		if( !frm_valida_senha1(this.value,true) ){ return false; }
	});
	$(document).on("blur", "#reset_password2", function(){
		if( !frm_valida_senha2(this.value,true) ){ return false; }
		if( !frm_valida_senhas($('#reset_password').val(),this.value,true) ){ return false; }
		//$('#reset_captcha_refresh_btn').trigger('click');
	});
	
	function frm_valida_email(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( !emailReg.test( _tmp ) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um E-MAIL válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_senha1(value,ret_msg)
	{
		// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		//
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		else if (!re.test(_tmp))
		{
			show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert');
			return false;
		}
		return true;
	}
	function frm_valida_senha2(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CONFIRMAÇÃO DE SENHA é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( _tmp.length < 6 )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com uma CONFIRMAÇÃO DE SENHA com no mínimo 6 dígitos!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function frm_valida_senhas(s1,s2,ret_msg)
	{
		var _tmp1 = s1;
		var _tmp2 = s2;
		if( _tmp1 != _tmp2 )
		{
			if( ret_msg )
			{
				show_msgbox('Os campos SENHA e CONFIRMAÇÃO DE SENHA não conferem!',' ','alert');
			}
			return false;
		}
		return true;
	}
	
	
	$('#bt_reset').click( function () 
	{
		var rs_token            = $('#reset_token').val();
		var rs_email            = $('#reset_email').val();
		var rs_username         = $('#reset_username').val();
		var rs_password         = $('#reset_password').val();
		var rs_password2        = $('#reset_password2').val();
		var rs_captcha_code     = $('#reset_captcha_code').val();
		
		//Password
		if (isEmpty(rs_password))     { show_msgbox('O campo SENHA é obrigatório!',' ','alert');              $('#reset_password').focus();     return false; }
		var re = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
		if (!re.test(rs_password))    { show_msgbox('Senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula. Favor tentar novamente!',' ','alert'); $('#reset_password').focus(); return false; }
		if (isEmpty(rs_password2))    { show_msgbox('O campo SENHA PARA CONFIRMAÇÃO é obrigatório!',' ','alert'); $('#rs_password2').focus(); return false; }
		if (rs_password != rs_password2) { show_msgbox('SENHA e CONFIRMAÇÃO não conferem!',' ','alert');     $('#rs_password').focus();  return false; }
		
		//Criptografa a senha
		var rs_password_crypted = hex_sha512(rs_password);
		
		//Show trobbler
		$('#loadingBox').modal('show');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		
		//Verifica captcha
		var _postdata = "lang=" + encodeURIComponent($.Storage.loadItem('language'))
		              + "&code=" + encodeURIComponent(rs_captcha_code);
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		
		$.ajax({
				 url: base_url + "/lib-bin/captcha/chk_code.cgi",
				type: "post",
		//dataType: "json",
				data: _postdata,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 0)
					{
						$('#reset_captcha_refresh_btn').trigger('click');
						$('#reset_captcha_code').val('');
						$('#reset_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox('CÓDIGO DE SEGURANÇA inválido. Tente novamente!',' ','alert');
						return false;
					}
					else
					{
						//var _postdata = "lang="      + encodeURIComponent($.Storage.loadItem('language'))
						var _postdata = 
							              "l="   + encodeURIComponent($.Storage.loadItem('language'))
							            + "&t="  + encodeURIComponent(rs_token)
							            + "&p="  + encodeURIComponent(rs_password_crypted)
							            ;
						
						//$('#loadingBox').modal('hide');
						do_reset_password(_postdata);
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					console.log("The following error occured: "+textStatus, errorThrown);
					$('#loadingBox').modal('hide');
					show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
					return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	function do_reset_password(_data)
	{
		//alert(base_url + "/lib-bin/do_newpwd.php?"+_data);
		//return false;
		
		$.ajax({
				 url: base_url + "/lib-bin/do_newpwd.php",
				type: "post",
		//dataType: "json",
				data: _data,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						$('#div-forms').hide();
						$('#msg_reset_ok').show();
						
						$('#loadingBox').modal('hide');
						//show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$('#reset_captcha_refresh_btn').trigger('click');
						$('#reset_captcha_code').val('');
						$('#reset_captcha_code').focus();
						$(".form-control").removeAttr("disabled");
						$('#loadingBox').modal('hide');
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
					console.log("The following error occured: "+textStatus, errorThrown);
					$('#loadingBox').modal('hide');
					show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
					return false;
				},
				complete: function(){},
				statusCode: { 404: function() { $('#loadingBox').modal('hide'); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	////////////////////// Reset Form ~ STOP
	
	
});