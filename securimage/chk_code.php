<?php
#################################################################################
## KONDOO - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: chk_code.php
## Função..........: Checa se o captcha informado e valido
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "../cgi-bin/includes/config.php";
	include_once "../cgi-bin/includes/aux_lib.php";
	include_once "./securimage.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_CODE = "";
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG           = test_input($_GET["l"]);
		$IN_CODE           = test_input($_GET["c"]);
	}
	elseif($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		$IN_CODE           = test_input($_POST["c"]);
	}
	
	$securimage = new Securimage();
	
	if ($securimage->check($IN_CODE) == true) 
	{
		die("1|ok|");
		exit;
	}
	else
	{
		switch($IN_LANG)
		{
			case 'en-us':
			case 'EN-US':
				$TXT = "Incorrect code!";
				break;
			case 'es-es':
			case 'ES-ES':
				$TXT = "Código incorrecto!";
				break;
			case 'pt-br':
			case 'PT-BR':
			default:
				$TXT = "Código incorreto!";
				break;
		}
		die("0|".$TXT."|");
		exit;
	}
		
#################################################################################
###########
#####
##
?>
