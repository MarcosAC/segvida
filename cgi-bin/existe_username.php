<?php
#################################################################################
## ME DEI BEM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: existe_username.php
## Função..........: Verifica se o username informado ja esta em uso
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_USERNAME = "";
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_USERNAME   = test_input($_GET["username"]);
		
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		$IN_USERNAME       = test_input($_POST["u"]);
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_USERNAME)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT `username` as username 
			   FROM `SYSTEM_USER_ACCOUNT` 
			  WHERE upper(`username`) = upper(?)"
			)) 
			{
				$sql_USERNAME = $mysqli->escape_string($IN_USERNAME);
				//
				$stmt->bind_param('s', $sql_USERNAME);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_username);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|".TXT_USERNAME_LIBERADO."|success|");
					exit; 
				}
				else
				{
					die("1|".TXT_USERNAME_JA_EM_USO."|alert|");
					exit; 
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
