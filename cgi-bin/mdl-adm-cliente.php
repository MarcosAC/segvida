<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-adm-cliente.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 02/09/2017 17:45:01
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_NOME_INTERNO = $IN_EMPRESA = $IN_CNPJ = $IN_FONE1 = $IN_FONE2 = $IN_EMAIL = $IN_CEP = $IN_UF = $IN_CIDADE = $IN_BAIRRO = $IN_END = $IN_NUMERO = $IN_COMPL = $IN_OBS = $IN_CONTATO_NOME = $IN_CONTATO_FONE1 = $IN_CONTATO_CEL1 = $IN_CONTATO_WHATSAPP = $IN_CONTATO_EMAIL = $IN_LD_RUIDO = $IN_LD_PART = $IN_LD_POEI = $IN_LD_VAP = $IN_LD_VIBR_VCI = $IN_LD_VIBR_VMB = $IN_LD_CALOR = $IN_LD_BIO = $IN_LD_ELETR = $IN_LD_EXP = $IN_LD_INFL = $IN_LD_RAD = $IN_LD_RIS = $IN_SITUACAO = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_NOME_INTERNO = test_input($_GET["nome_interno"]);
		$IN_EMPRESA = test_input($_GET["empresa"]);
		$IN_CNPJ = test_input($_GET["cnpj"]);
		$IN_FONE1 = test_input($_GET["fone1"]);
		$IN_FONE2 = test_input($_GET["fone2"]);
		$IN_EMAIL = test_input($_GET["email"]);
		$IN_CEP = test_input($_GET["cep"]);
		$IN_UF = test_input($_GET["uf"]);
		$IN_CIDADE = test_input($_GET["cidade"]);
		$IN_BAIRRO = test_input($_GET["bairro"]);
		$IN_END = test_input($_GET["end"]);
		$IN_NUMERO = test_input($_GET["numero"]);
		$IN_COMPL = test_input($_GET["compl"]);
		$IN_OBS = test_input($_GET["obs"]);
		$IN_CONTATO_NOME = test_input($_GET["contato_nome"]);
		$IN_CONTATO_FONE1 = test_input($_GET["contato_fone1"]);
		$IN_CONTATO_CEL1 = test_input($_GET["contato_cel1"]);
		$IN_CONTATO_WHATSAPP = test_input($_GET["contato_whatsapp"]);
		$IN_SITUACAO = test_input($_GET["situacao"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_NOME_INTERNO = test_input($_POST["nome_interno"]);
		$IN_EMPRESA = test_input($_POST["empresa"]);
		$IN_CNPJ = test_input($_POST["cnpj"]);
		$IN_FONE1 = test_input($_POST["fone1"]);
		$IN_FONE2 = test_input($_POST["fone2"]);
		$IN_EMAIL = test_input($_POST["email"]);
		$IN_CEP = test_input($_POST["cep"]);
		$IN_UF = test_input($_POST["uf"]);
		$IN_CIDADE = test_input($_POST["cidade"]);
		$IN_BAIRRO = test_input($_POST["bairro"]);
		$IN_END = test_input($_POST["end"]);
		$IN_NUMERO = test_input($_POST["numero"]);
		$IN_COMPL = test_input($_POST["compl"]);
		$IN_OBS = test_input($_POST["obs"]);
		$IN_CONTATO_NOME = test_input($_POST["contato_nome"]);
		$IN_CONTATO_FONE1 = test_input($_POST["contato_fone1"]);
		$IN_CONTATO_CEL1 = test_input($_POST["contato_cel1"]);
		$IN_CONTATO_WHATSAPP = test_input($_POST["contato_whatsapp"]);
		$IN_CONTATO_EMAIL = test_input($_POST["contato_email"]);
		$IN_LD_RUIDO = test_input($_POST["ld_ruido"]);
		$IN_LD_PART = test_input($_POST["ld_part"]);
		$IN_LD_POEI = test_input($_POST["ld_poei"]);
		$IN_LD_VAP = test_input($_POST["ld_vap"]);
		$IN_LD_VIBR_VCI = test_input($_POST["ld_vibr_vci"]);
		$IN_LD_VIBR_VMB = test_input($_POST["ld_vibr_vmb"]);
		$IN_LD_CALOR = test_input($_POST["ld_calor"]);
		$IN_LD_BIO = test_input($_POST["ld_bio"]);
		$IN_LD_ELETR = test_input($_POST["ld_eletr"]);
		$IN_LD_EXP = test_input($_POST["ld_exp"]);
		$IN_LD_INFL = test_input($_POST["ld_infl"]);
		$IN_LD_RAD = test_input($_POST["ld_rad"]);
		$IN_LD_RIS = test_input($_POST["ld_ris"]);
		$IN_SITUACAO = test_input($_POST["situacao"]);
		
		## case convertion
		mb_strtoupper($IN_NOME_INTERNO,"UTF-8");
		mb_strtoupper($IN_EMPRESA,"UTF-8");
		mb_strtolower($IN_EMAIL,"UTF-8");
		mb_strtoupper($IN_BAIRRO,"UTF-8");
		mb_strtoupper($IN_END,"UTF-8");
		mb_strtoupper($IN_COMPL,"UTF-8");
		mb_strtoupper($IN_CONTATO_NOME,"UTF-8");
		mb_strtolower($IN_CONTATO_EMAIL,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_NOME_INTERNO, $IN_EMPRESA, $IN_CNPJ, $IN_FONE1, $IN_FONE2, $IN_EMAIL, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUMERO, $IN_COMPL, $IN_OBS, $IN_CONTATO_NOME, $IN_CONTATO_FONE1, $IN_CONTATO_CEL1, $IN_CONTATO_WHATSAPP, $IN_CONTATO_EMAIL, $IN_LD_RUIDO, $IN_LD_PART, $IN_LD_POEI, $IN_LD_VAP, $IN_LD_VIBR_VCI, $IN_LD_VIBR_VMB, $IN_LD_CALOR, $IN_LD_BIO, $IN_LD_ELETR, $IN_LD_EXP, $IN_LD_INFL, $IN_LD_RAD, $IN_LD_RIS, $IN_SITUACAO);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_NOME_INTERNO) || isEmpty($IN_EMPRESA) || isEmpty($IN_CNPJ) || isEmpty($IN_FONE1) || isEmpty($IN_EMAIL) || isEmpty($IN_CEP) || isEmpty($IN_UF) || isEmpty($IN_CIDADE) || isEmpty($IN_BAIRRO) || isEmpty($IN_END) || isEmpty($IN_NUMERO) || isEmpty($IN_SITUACAO) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_NOME_INTERNO, $IN_EMPRESA, $IN_CNPJ, $IN_FONE1, $IN_FONE2, $IN_EMAIL, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUMERO, $IN_COMPL, $IN_OBS, $IN_CONTATO_NOME, $IN_CONTATO_FONE1, $IN_CONTATO_CEL1, $IN_CONTATO_WHATSAPP, $IN_CONTATO_EMAIL, $IN_LD_RUIDO, $IN_LD_PART, $IN_LD_POEI, $IN_LD_VAP, $IN_LD_VIBR_VCI, $IN_LD_VIBR_VMB, $IN_LD_CALOR, $IN_LD_BIO, $IN_LD_ELETR, $IN_LD_EXP, $IN_LD_INFL, $IN_LD_RAD, $IN_LD_RIS, $IN_SITUACAO);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_NOME_INTERNO) || isEmpty($IN_EMPRESA) || isEmpty($IN_CNPJ) || isEmpty($IN_FONE1) || isEmpty($IN_EMAIL) || isEmpty($IN_CEP) || isEmpty($IN_UF) || isEmpty($IN_CIDADE) || isEmpty($IN_BAIRRO) || isEmpty($IN_END) || isEmpty($IN_NUMERO) || isEmpty($IN_SITUACAO) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_NOME_INTERNO, $IN_EMPRESA, $IN_CNPJ, $IN_FONE1, $IN_FONE2, $IN_EMAIL, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUMERO, $IN_COMPL, $IN_OBS, $IN_CONTATO_NOME, $IN_CONTATO_FONE1, $IN_CONTATO_CEL1, $IN_CONTATO_WHATSAPP, $IN_CONTATO_EMAIL, $IN_LD_RUIDO, $IN_LD_PART, $IN_LD_POEI, $IN_LD_VAP, $IN_LD_VIBR_VCI, $IN_LD_VIBR_VMB, $IN_LD_CALOR, $IN_LD_BIO, $IN_LD_ELETR, $IN_LD_EXP, $IN_LD_INFL, $IN_LD_RAD, $IN_LD_RIS, $IN_SITUACAO);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## FILE UPLOAD
			case '5':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPLOAD($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id, 
              upper(AA.`nome_interno`) as nome_interno, 
              upper(AA.`empresa`) as empresa, 
              concat(upper(M.`cidade`),'-',upper(AA.`uf`)) as cidade_uf,
              AA.`ld_ruido` as ld_ruido,
              AA.`ld_part` as ld_part,
              AA.`ld_poei` as ld_poei,
              AA.`ld_vap` as ld_vap,
              AA.`ld_vibr_vci` as ld_vibr_vci,
              AA.`ld_vibr_vmb` as ld_vibr_vmb,
              AA.`ld_calor` as ld_calor,
              AA.`ld_bio` as ld_bio,
              AA.`ld_eletr` as ld_eletr,
              AA.`ld_exp` as ld_exp,
              AA.`ld_infl` as ld_infl,
              AA.`ld_rad` as ld_rad,
              AA.`ld_ris` as ld_ris,
              AA.`situacao` as situacao,
              upper(AA.`contato_nome`) as contato_nome
         FROM `CLIENTE` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `MUNICIPIO_BR` M
           ON M.`codigo_ibge` = AA.`cidade_codigo`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 2"
		)) 
		{
			$stmt->bind_param('s', $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_NOME_INTERNO, $o_EMPRESA, $o_CIDADE_UF, 
			$o_LD_RUIDO, $o_LD_PART, $o_LD_POEI, $o_LD_VAP, $o_LD_VIBR_VCI, $o_LD_VIBR_VMB, $o_LD_CALOR, $o_LD_BIO, $o_LD_ELETR, $o_LD_EXP, 
			$o_LD_INFL, $o_LD_RAD, $o_LD_RIS, $o_SITUACAO, $o_CONTATO_NOME);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_SITUACAO == "A"){ $o_SITUACAO_TXT = "<div class=\"grid_color_green\">".TXT_ADM_CLIENTE_ATIVO."</div>"; }
					if( $o_SITUACAO == "I"){ $o_SITUACAO_TXT = "<div class=\"grid_color_red\">".TXT_ADM_CLIENTE_INATIVO."</div>"; }
					
					# Formata Laudos
					$o_LAUDOS_TOTAL = $o_LD_RUIDO + $o_LD_PART + $o_LD_POEI + $o_LD_VAP + $o_LD_VIBR_VCI + $o_LD_VIBR_VMB + $o_LD_CALOR + $o_LD_BIO + $o_LD_ELETR + $o_LD_EXP + $o_LD_INFL + $o_LD_RAD + $o_LD_RIS;
					if(isEmpty($o_LAUDOS_TOTAL)){ $o_LAUDOS_TOTAL = 0; }
					
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td><small>'.$o_NOME_INTERNO.'</small></td>';
					$TBODY_LIST .= '<td><small>'.$o_EMPRESA.'</small></td>';
					$TBODY_LIST .= '<td><small>'.$o_CIDADE_UF.'</small></td>';
					$TBODY_LIST .= '<td><small>'.$o_CONTATO_NOME.'</small></td>';
					$TBODY_LIST .= '<td>'.$o_LAUDOS_TOTAL.'</td>';
					$TBODY_LIST .= '<td>'.$o_SITUACAO_TXT.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_NOME_INTERNO, $_EMPRESA, $_CNPJ, $_FONE1, $_FONE2, $_EMAIL, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUMERO, $_COMPL, $_OBS, $_CONTATO_NOME, $_CONTATO_FONE1, $_CONTATO_CEL1, $_CONTATO_WHATSAPP, $_CONTATO_EMAIL, $_LD_RUIDO, $_LD_PART, $_LD_POEI, $_LD_VAP, $_LD_VIBR_VCI, $_LD_VIBR_VMB, $_LD_CALOR, $_LD_BIO, $_LD_ELETR, $_LD_EXP, $_LD_INFL, $_LD_RAD, $_LD_RIS, $_SITUACAO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_NOME_INTERNO = $mysqli->escape_String($_NOME_INTERNO);
		$sql_EMPRESA = $mysqli->escape_String($_EMPRESA);
		$sql_CNPJ = $mysqli->escape_String($_CNPJ);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id
       FROM `CLIENTE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`nome_interno` = ?
        AND AA.`empresa` = ?
        AND AA.`cnpj` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_SID_idSYSTEM_CLIENTE, $sql_NOME_INTERNO, $sql_EMPRESA, $sql_CNPJ);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_NOME_INTERNO, $_EMPRESA, $_CNPJ, $_FONE1, $_FONE2, $_EMAIL, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUMERO, $_COMPL, $_OBS, $_CONTATO_NOME, $_CONTATO_FONE1, $_CONTATO_CEL1, $_CONTATO_WHATSAPP, $_CONTATO_EMAIL, $_LD_RUIDO, $_LD_PART, $_LD_POEI, $_LD_VAP, $_LD_VIBR_VCI, $_LD_VIBR_VMB, $_LD_CALOR, $_LD_BIO, $_LD_ELETR, $_LD_EXP, $_LD_INFL, $_LD_RAD, $_LD_RIS, $_SITUACAO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_NOME_INTERNO = $mysqli->escape_String($_NOME_INTERNO);
		$sql_EMPRESA = $mysqli->escape_String($_EMPRESA);
		$sql_CNPJ = $mysqli->escape_String($_CNPJ);
		$sql_FONE1 = $mysqli->escape_String($_FONE1);
		$sql_FONE2 = $mysqli->escape_String($_FONE2);
		$sql_EMAIL = $mysqli->escape_String($_EMAIL);
		$sql_CEP = $mysqli->escape_String($_CEP);
		$sql_UF = $mysqli->escape_String($_UF);
		$sql_CIDADE = $mysqli->escape_String($_CIDADE);
		$sql_BAIRRO = $mysqli->escape_String($_BAIRRO);
		$sql_END = $mysqli->escape_String($_END);
		$sql_NUMERO = $mysqli->escape_String($_NUMERO);
		$sql_COMPL = $mysqli->escape_String($_COMPL);
		$sql_OBS = $mysqli->escape_String($_OBS);
		$sql_CONTATO_NOME = $mysqli->escape_String($_CONTATO_NOME);
		$sql_CONTATO_FONE1 = $mysqli->escape_String($_CONTATO_FONE1);
		$sql_CONTATO_CEL1 = $mysqli->escape_String($_CONTATO_CEL1);
		$sql_CONTATO_WHATSAPP = $mysqli->escape_String($_CONTATO_WHATSAPP);
		$sql_CONTATO_EMAIL = $mysqli->escape_String($_CONTATO_EMAIL);
		$sql_LD_RUIDO    = $mysqli->escape_String($_LD_RUIDO);
		$sql_LD_PART     = $mysqli->escape_String($_LD_PART);
		$sql_LD_POEI     = $mysqli->escape_String($_LD_POEI);
		$sql_LD_VAP      = $mysqli->escape_String($_LD_VAP);
		$sql_LD_VIBR_VCI = $mysqli->escape_String($_LD_VIBR_VCI);
		$sql_LD_VIBR_VMB = $mysqli->escape_String($_LD_VIBR_VMB);
		$sql_LD_CALOR    = $mysqli->escape_String($_LD_CALOR);
		$sql_LD_BIO      = $mysqli->escape_String($_LD_BIO);
		$sql_LD_ELETR    = $mysqli->escape_String($_LD_ELETR);
		$sql_LD_EXP      = $mysqli->escape_String($_LD_EXP);
		$sql_LD_INFL     = $mysqli->escape_String($_LD_INFL);
		$sql_LD_RAD      = $mysqli->escape_String($_LD_RAD);
		$sql_LD_RIS      = $mysqli->escape_String($_LD_RIS);
		$sql_SITUACAO = $mysqli->escape_String($_SITUACAO);
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id
       FROM `CLIENTE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`nome_interno` = ?
        AND AA.`empresa` = ?
        AND AA.`cnpj` = ?"
		)) 
		{
			$stmt->bind_param('ssss', $sql_SID_idSYSTEM_CLIENTE, $sql_NOME_INTERNO, $sql_EMPRESA, $sql_CNPJ);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `CLIENTE` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `nome_interno` = ?
		        ,`empresa` = ?
		        ,`cnpj` = ?
		        ,`fone1` = ?
		        ,`fone2` = ?
		        ,`email` = ?
		        ,`cep` = ?
		        ,`uf` = ?
		        ,`cidade_codigo` = ?
		        ,`bairro` = ?
		        ,`end` = ?
		        ,`numero` = ?
		        ,`compl` = ?
		        ,`obs` = ?
		        ,`contato_nome` = ?
		        ,`contato_fone1` = ?
		        ,`contato_cel1` = ?
		        ,`contato_whatsapp` = ?
		        ,`contato_email` = ?
		        ,`ld_ruido`= ?
		        ,`ld_part`= ?
		        ,`ld_poei`= ?
		        ,`ld_vap`= ?
		        ,`ld_vibr_vci`= ?
		        ,`ld_vibr_vmb`= ?
		        ,`ld_calor`= ?
		        ,`ld_bio`= ?
		        ,`ld_eletr`= ?
		        ,`ld_exp`= ?
		        ,`ld_infl`= ?
		        ,`ld_rad`= ?
		        ,`ld_ris`= ?
		        ,`situacao` = ?"
		)) 
		{
			$stmt->bind_param('ssssssssssdssssssssssssssssssssssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_NOME_INTERNO, $sql_EMPRESA, $sql_CNPJ, $sql_FONE1, $sql_FONE2, $sql_EMAIL, $sql_CEP, $sql_UF, $sql_CIDADE, $sql_BAIRRO, $sql_END, $sql_NUMERO, $sql_COMPL, $sql_OBS, $sql_CONTATO_NOME, $sql_CONTATO_FONE1, $sql_CONTATO_CEL1, $sql_CONTATO_WHATSAPP, $sql_CONTATO_EMAIL, $sql_LD_RUIDO, $sql_LD_PART, $sql_LD_POEI, $sql_LD_VAP, $sql_LD_VIBR_VCI, $sql_LD_VIBR_VMB, $sql_LD_CALOR, $sql_LD_BIO, $sql_LD_ELETR, $sql_LD_EXP, $sql_LD_INFL, $sql_LD_RAD, $sql_LD_RIS, $sql_SITUACAO);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_NOME_INTERNO, $_EMPRESA, $_CNPJ, $_FONE1, $_FONE2, $_EMAIL, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUMERO, $_COMPL, $_OBS, $_CONTATO_NOME, $_CONTATO_FONE1, $_CONTATO_CEL1, $_CONTATO_WHATSAPP, $_CONTATO_EMAIL, $_LD_RUIDO, $_LD_PART, $_LD_POEI, $_LD_VAP, $_LD_VIBR_VCI, $_LD_VIBR_VMB, $_LD_CALOR, $_LD_BIO, $_LD_ELETR, $_LD_EXP, $_LD_INFL, $_LD_RAD, $_LD_RIS, $_SITUACAO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_NOME_INTERNO = $mysqli->escape_String($_NOME_INTERNO);
		$sql_EMPRESA = $mysqli->escape_String($_EMPRESA);
		$sql_CNPJ = $mysqli->escape_String($_CNPJ);
		$sql_FONE1 = $mysqli->escape_String($_FONE1);
		$sql_FONE2 = $mysqli->escape_String($_FONE2);
		$sql_EMAIL = $mysqli->escape_String($_EMAIL);
		$sql_CEP = $mysqli->escape_String($_CEP);
		$sql_UF = $mysqli->escape_String($_UF);
		$sql_CIDADE = $mysqli->escape_String($_CIDADE);
		$sql_BAIRRO = $mysqli->escape_String($_BAIRRO);
		$sql_END = $mysqli->escape_String($_END);
		$sql_NUMERO = $mysqli->escape_String($_NUMERO);
		$sql_COMPL = $mysqli->escape_String($_COMPL);
		$sql_OBS = $mysqli->escape_String($_OBS);
		$sql_CONTATO_NOME = $mysqli->escape_String($_CONTATO_NOME);
		$sql_CONTATO_FONE1 = $mysqli->escape_String($_CONTATO_FONE1);
		$sql_CONTATO_CEL1 = $mysqli->escape_String($_CONTATO_CEL1);
		$sql_CONTATO_WHATSAPP = $mysqli->escape_String($_CONTATO_WHATSAPP);
		$sql_CONTATO_EMAIL = $mysqli->escape_String($_CONTATO_EMAIL);
		$sql_LD_RUIDO    = $mysqli->escape_String($_LD_RUIDO);
		$sql_LD_PART     = $mysqli->escape_String($_LD_PART);
		$sql_LD_POEI     = $mysqli->escape_String($_LD_POEI);
		$sql_LD_VAP      = $mysqli->escape_String($_LD_VAP);
		$sql_LD_VIBR_VCI = $mysqli->escape_String($_LD_VIBR_VCI);
		$sql_LD_VIBR_VMB = $mysqli->escape_String($_LD_VIBR_VMB);
		$sql_LD_CALOR    = $mysqli->escape_String($_LD_CALOR);
		$sql_LD_BIO      = $mysqli->escape_String($_LD_BIO);
		$sql_LD_ELETR    = $mysqli->escape_String($_LD_ELETR);
		$sql_LD_EXP      = $mysqli->escape_String($_LD_EXP);
		$sql_LD_INFL     = $mysqli->escape_String($_LD_INFL);
		$sql_LD_RAD      = $mysqli->escape_String($_LD_RAD);
		$sql_LD_RIS      = $mysqli->escape_String($_LD_RIS);
		$sql_SITUACAO = $mysqli->escape_String($_SITUACAO);
		
		## Verifica se o registro do ID informado existe e se é do cliente prodfy indicado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id
       FROM `CLIENTE` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idCLIENTE);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `CLIENTE` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `nome_interno` = ?
		        ,`empresa` = ?
		        ,`cnpj` = ?
		        ,`fone1` = ?
		        ,`fone2` = ?
		        ,`email` = ?
		        ,`cep` = ?
		        ,`uf` = ?
		        ,`cidade_codigo` = ?
		        ,`bairro` = ?
		        ,`end` = ?
		        ,`numero` = ?
		        ,`compl` = ?
		        ,`obs` = ?
		        ,`contato_nome` = ?
		        ,`contato_fone1` = ?
		        ,`contato_cel1` = ?
		        ,`contato_whatsapp` = ?
		        ,`contato_email` = ?
		        ,`ld_ruido`= ?
		        ,`ld_part`= ?
		        ,`ld_poei`= ?
		        ,`ld_vap`= ?
		        ,`ld_vibr_vci`= ?
		        ,`ld_vibr_vmb`= ?
		        ,`ld_calor`= ?
		        ,`ld_bio`= ?
		        ,`ld_eletr`= ?
		        ,`ld_exp`= ?
		        ,`ld_infl`= ?
		        ,`ld_rad`= ?
		        ,`ld_ris`= ?
		        ,`situacao` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idCLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('sssssssssdssssssssssssssssssssssssss', $sql_SID_USERNAME, $sql_NOME_INTERNO, $sql_EMPRESA, $sql_CNPJ, $sql_FONE1, $sql_FONE2, $sql_EMAIL, $sql_CEP, $sql_UF, $sql_CIDADE, $sql_BAIRRO, $sql_END, $sql_NUMERO, $sql_COMPL, $sql_OBS, $sql_CONTATO_NOME, $sql_CONTATO_FONE1, $sql_CONTATO_CEL1, $sql_CONTATO_WHATSAPP, $sql_CONTATO_EMAIL, $sql_LD_RUIDO, $sql_LD_PART, $sql_LD_POEI, $sql_LD_VAP, $sql_LD_VIBR_VCI, $sql_LD_VIBR_VMB, $sql_LD_CALOR, $sql_LD_BIO, $sql_LD_ELETR, $sql_LD_EXP, $sql_LD_INFL, $sql_LD_RAD, $sql_LD_RIS, $sql_SITUACAO, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente prodfy indicado
		
		$_QUERY = "DELETE FROM `CLIENTE` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idCLIENTE` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPLOAD
	####
	function executa_UPLOAD($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		#Extensoes permitidas para a foto
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (
				(($_FILES["file"]["type"] == "image/gif")   || 
				 ($_FILES["file"]["type"] == "image/jpeg")  || 
				 ($_FILES["file"]["type"] == "image/jpg")   || 
				 ($_FILES["file"]["type"] == "image/pjpeg") || 
				 ($_FILES["file"]["type"] == "image/x-png") || 
				 ($_FILES["file"]["type"] == "image/png")) && 
				 ($_FILES["file"]["size"] < 80000) &&
				 in_array($extension, $allowedExts)
			) 
		{
			if ($_FILES["file"]["error"] > 0) 
			{
				//return "Return Code: " . $_FILES["file"]["error"] . "<br>";
				return "0|".$_FILES["file"]["error"]."|error|";
				exit;
			}
			else
			{
				## Carrega nome existente da foto de perfil do usuario
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT U.`profile_pic` as foto
           FROM `SYSTEM_USER_ACCOUNT` U
          WHERE U.`idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_REG_ID = $mysqli->escape_String($_REG_ID);
					$stmt->bind_param('s', $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_NOME_FOTO_ATUAL);
					
					$stmt->fetch();
					
				}
				else
				{
					return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|";
					exit;
				}
				
				
				## Gera nome unico para o arquivo e
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(PROFILE_PICS_PATH."/" . $filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["file"]["tmp_name"],PROFILE_PICS_PATH."/" . $filename);
				
				## Atualiza base de dados com o nome novo da foto
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"UPDATE `SYSTEM_USER_ACCOUNT`
				    SET  `profile_pic` = ?
				  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_PROFILE_PIC = $mysqli->escape_String($filename);
					$sql_REG_ID      = $mysqli->escape_String($_REG_ID);
					//
					$stmt->bind_param('ss', $sql_PROFILE_PIC, $sql_REG_ID);
					
					if($stmt->execute())
					{
						## Apaga foto antiga do repositorio
						if( !isEmpty($o_NOME_FOTO_ATUAL) && 
						    ($o_NOME_FOTO_ATUAL != "no_profile_pic.jpg") && 
						    file_exists(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL)
						  )
						{ unlink(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL); }
						
						## Retorna OK com o nome da foto nova
						return "1|".TXT_MOD_PERFIL_FOTO_ATUALIZADA."|success|".$filename."|";
						exit;
					}
					else
					{
						return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
						exit;
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
			}
		}
		else
		{
			//echo "Invalid file";
			return "0|".TXT_UPLOAD_ARQUIVO_INVALIDO."|error|";
			exit;
		}
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		/*
		$sql_NOME_INTERNO = $mysqli->escape_String($_NOME_INTERNO);
		$sql_EMPRESA = $mysqli->escape_String($_EMPRESA);
		$sql_CNPJ = $mysqli->escape_String($_CNPJ);
		$sql_FONE1 = $mysqli->escape_String($_FONE1);
		$sql_FONE2 = $mysqli->escape_String($_FONE2);
		$sql_EMAIL = $mysqli->escape_String($_EMAIL);
		$sql_CEP = $mysqli->escape_String($_CEP);
		$sql_UF = $mysqli->escape_String($_UF);
		$sql_CIDADE = $mysqli->escape_String($_CIDADE);
		$sql_BAIRRO = $mysqli->escape_String($_BAIRRO);
		$sql_END = $mysqli->escape_String($_END);
		$sql_NUMERO = $mysqli->escape_String($_NUMERO);
		$sql_COMPL = $mysqli->escape_String($_COMPL);
		$sql_OBS = $mysqli->escape_String($_OBS);
		$sql_CONTATO_NOME = $mysqli->escape_String($_CONTATO_NOME);
		$sql_CONTATO_FONE1 = $mysqli->escape_String($_CONTATO_FONE1);
		$sql_CONTATO_CEL1 = $mysqli->escape_String($_CONTATO_CEL1);
		$sql_CONTATO_WHATSAPP = $mysqli->escape_String($_CONTATO_WHATSAPP);
		$sql_LD_RUIDO    = $mysqli->escape_String($_LD_RUIDO);
		$sql_LD_PART     = $mysqli->escape_String($_LD_PART);
		$sql_LD_POEI     = $mysqli->escape_String($_LD_POEI);
		$sql_LD_VAP      = $mysqli->escape_String($_LD_VAP);
		$sql_LD_VIBR_VCI = $mysqli->escape_String($_LD_VIBR_VCI);
		$sql_LD_VIBR_VMB = $mysqli->escape_String($_LD_VIBR_VMB);
		$sql_LD_CALOR    = $mysqli->escape_String($_LD_CALOR);
		$sql_LD_BIO      = $mysqli->escape_String($_LD_BIO);
		$sql_LD_ELETR    = $mysqli->escape_String($_LD_ELETR);
		$sql_LD_EXP      = $mysqli->escape_String($_LD_EXP);
		$sql_LD_INFL     = $mysqli->escape_String($_LD_INFL);
		$sql_LD_RAD      = $mysqli->escape_String($_LD_RAD);
		$sql_LD_RIS      = $mysqli->escape_String($_LD_RIS);
		$sql_SITUACAO = $mysqli->escape_String($_SITUACAO);
		$sql_SITUACAO = $mysqli->escape_String($_SITUACAO);
		*/
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id, upper(AA.`nome_interno`) as nome_interno, upper(AA.`empresa`) as empresa, AA.`cnpj` as cnpj, AA.`fone1` as fone1, AA.`fone2` as fone2, lower(AA.`email`) as email, AA.`cep` as cep, AA.`uf` as uf, AA.`cidade_codigo` as cidade_codigo, upper(AA.`bairro`) as bairro, upper(AA.`end`) as end, AA.`numero` as numero, upper(AA.`compl`) as compl, AA.`obs` as obs, upper(AA.`contato_nome`) as contato_nome, AA.`contato_fone1` as contato_fone1, AA.`contato_cel1` as contato_cel1, AA.`contato_whatsapp` as contato_whatsapp, lower(AA.`contato_email`) as contato_email, 
            AA.`ld_ruido` as ld_ruido,
            AA.`ld_part` as ld_part,
            AA.`ld_poei` as ld_poei,
            AA.`ld_vap` as ld_vap,
            AA.`ld_vibr_vci` as ld_vibr_vci,
            AA.`ld_vibr_vmb` as ld_vibr_vmb,
            AA.`ld_calor` as ld_calor,
            AA.`ld_bio` as ld_bio,
            AA.`ld_eletr` as ld_eletr,
            AA.`ld_exp` as ld_exp,
            AA.`ld_infl` as ld_infl,
            AA.`ld_rad` as ld_rad,
            AA.`ld_ris` as ld_ris,
            AA.`situacao` as situacao
       FROM `CLIENTE` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCLIENTE` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_NOME_INTERNO, $o_EMPRESA, $o_CNPJ, $o_FONE1, $o_FONE2, $o_EMAIL, $o_CEP, $o_UF, $o_CIDADE, $o_BAIRRO, $o_END, $o_NUMERO, $o_COMPL, $o_OBS, $o_CONTATO_NOME, $o_CONTATO_FONE1, $o_CONTATO_CEL1, $o_CONTATO_WHATSAPP, $o_CONTATO_EMAIL, $o_LD_RUIDO, $o_LD_PART, $o_LD_POEI, $o_LD_VAP, $o_LD_VIBR_VCI, $o_LD_VIBR_VMB, $o_LD_CALOR, $o_LD_BIO, $o_LD_ELETR, $o_LD_EXP, $o_LD_INFL, $o_LD_RAD, $o_LD_RIS, $o_SITUACAO);
			$stmt->fetch();
			
			$o_OBS = unescape_string($o_OBS);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_NOME_INTERNO."|".$o_EMPRESA."|".$o_CNPJ."|".$o_FONE1."|".$o_FONE2."|".$o_EMAIL."|".$o_CEP."|".$o_UF."|".$o_CIDADE."|".$o_BAIRRO."|".$o_END."|".$o_NUMERO."|".$o_COMPL."|".$o_OBS."|".$o_CONTATO_NOME."|".$o_CONTATO_FONE1."|".$o_CONTATO_CEL1."|".$o_CONTATO_WHATSAPP."|".$o_CONTATO_EMAIL."|".$o_LD_RUIDO."|".$o_LD_PART."|".$o_LD_POEI."|".$o_LD_VAP."|".$o_LD_VIBR_VCI."|".$o_LD_VIBR_VMB."|".$o_LD_CALOR."|".$o_LD_BIO."|".$o_LD_ELETR."|".$o_LD_EXP."|".$o_LD_INFL."|".$o_LD_RAD."|".$o_LD_RIS."|".$o_SITUACAO."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idCLIENTE` as id, upper(AA.`nome_interno`) as nome_interno, upper(AA.`empresa`) as empresa, AA.`cnpj` as cnpj, AA.`fone1` as fone1, AA.`fone2` as fone2, lower(AA.`email`) as email, AA.`cep` as cep, AA.`uf` as uf, upper(MB.`cidade`) as cidade_codigo, upper(AA.`bairro`) as bairro, upper(AA.`end`) as end, AA.`numero` as numero, upper(AA.`compl`) as compl, AA.`obs` as obs, upper(AA.`contato_nome`) as contato_nome, AA.`contato_fone1` as contato_fone1, AA.`contato_cel1` as contato_cel1, AA.`contato_whatsapp` as contato_whatsapp, lower(AA.`contato_email`) as contato_email, 
            AA.`ld_ruido` as ld_ruido,
            AA.`ld_part` as ld_part,
            AA.`ld_poei` as ld_poei,
            AA.`ld_vap` as ld_vap,
            AA.`ld_vibr_vci` as ld_vibr_vci,
            AA.`ld_vibr_vmb` as ld_vibr_vmb,
            AA.`ld_calor` as ld_calor,
            AA.`ld_bio` as ld_bio,
            AA.`ld_eletr` as ld_eletr,
            AA.`ld_exp` as ld_exp,
            AA.`ld_infl` as ld_infl,
            AA.`ld_rad` as ld_rad,
            AA.`ld_ris` as ld_ris,
            AA.`situacao` as situacao,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `CLIENTE` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `MUNICIPIO_BR` as MB
           ON MB.`codigo_ibge` = AA.`cidade_codigo`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCLIENTE` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_NOME_INTERNO, $o_EMPRESA, $o_CNPJ, $o_FONE1, $o_FONE2, $o_EMAIL, $o_CEP, $o_UF, $o_CIDADE, $o_BAIRRO, $o_END, $o_NUMERO, $o_COMPL, $o_OBS, $o_CONTATO_NOME, $o_CONTATO_FONE1, $o_CONTATO_CEL1, $o_CONTATO_WHATSAPP, $o_CONTATO_EMAIL, $o_LD_RUIDO, $o_LD_PART, $o_LD_POEI, $o_LD_VAP, $o_LD_VIBR_VCI, $o_LD_VIBR_VMB, $o_LD_CALOR, $o_LD_BIO, $o_LD_ELETR, $o_LD_EXP, $o_LD_INFL, $o_LD_RAD, $o_LD_RIS, $o_SITUACAO, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			$o_OBS = unescape_string($o_OBS);
			$o_OBS = nl2br($o_OBS);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_UF == "AC"){ $o_UF_TXT = TXT_ADM_CLIENTE_ACRE; }
				if( $o_UF == "AL"){ $o_UF_TXT = TXT_ADM_CLIENTE_ALAGOAS; }
				if( $o_UF == "AM"){ $o_UF_TXT = TXT_ADM_CLIENTE_AMAZONAS; }
				if( $o_UF == "AP"){ $o_UF_TXT = TXT_ADM_CLIENTE_AMAPA; }
				if( $o_UF == "BA"){ $o_UF_TXT = TXT_ADM_CLIENTE_BAHIA; }
				if( $o_UF == "CE"){ $o_UF_TXT = TXT_ADM_CLIENTE_CEARA; }
				if( $o_UF == "DF"){ $o_UF_TXT = TXT_ADM_CLIENTE_DISTRITO_FEDERAL; }
				if( $o_UF == "ES"){ $o_UF_TXT = TXT_ADM_CLIENTE_ESPIRITO_SANTO; }
				if( $o_UF == "GO"){ $o_UF_TXT = TXT_ADM_CLIENTE_GOIAS; }
				if( $o_UF == "MA"){ $o_UF_TXT = TXT_ADM_CLIENTE_MARANHAO; }
				if( $o_UF == "MG"){ $o_UF_TXT = TXT_ADM_CLIENTE_MINAS_GERAIS; }
				if( $o_UF == "MS"){ $o_UF_TXT = TXT_ADM_CLIENTE_MATO_GROSSO_DO_SUL; }
				if( $o_UF == "MT"){ $o_UF_TXT = TXT_ADM_CLIENTE_MATO_GROSSO; }
				if( $o_UF == "PA"){ $o_UF_TXT = TXT_ADM_CLIENTE_PARA; }
				if( $o_UF == "PB"){ $o_UF_TXT = TXT_ADM_CLIENTE_PARAIBA; }
				if( $o_UF == "PE"){ $o_UF_TXT = TXT_ADM_CLIENTE_PERNAMBUCO; }
				if( $o_UF == "PI"){ $o_UF_TXT = TXT_ADM_CLIENTE_PIAUI; }
				if( $o_UF == "PR"){ $o_UF_TXT = TXT_ADM_CLIENTE_PARANA; }
				if( $o_UF == "RJ"){ $o_UF_TXT = TXT_ADM_CLIENTE_RIO_DE_JANEIRO; }
				if( $o_UF == "RN"){ $o_UF_TXT = TXT_ADM_CLIENTE_RIO_GRANDE_DO_NORTE; }
				if( $o_UF == "RO"){ $o_UF_TXT = TXT_ADM_CLIENTE_RONDONIA; }
				if( $o_UF == "RR"){ $o_UF_TXT = TXT_ADM_CLIENTE_RORAIMA; }
				if( $o_UF == "RS"){ $o_UF_TXT = TXT_ADM_CLIENTE_RIO_GRANDE_DO_SUL; }
				if( $o_UF == "SC"){ $o_UF_TXT = TXT_ADM_CLIENTE_SANTA_CATARINA; }
				if( $o_UF == "SE"){ $o_UF_TXT = TXT_ADM_CLIENTE_SERGIPE; }
				if( $o_UF == "SP"){ $o_UF_TXT = TXT_ADM_CLIENTE_SAO_PAULO; }
				if( $o_UF == "TO"){ $o_UF_TXT = TXT_ADM_CLIENTE_TOCANTINS; }
				$o_CIDADE_TXT = $o_CIDADE;
				if( $o_SITUACAO == "A"){ $o_SITUACAO_TXT = "<div class=\"grid_color_green\">".TXT_ADM_CLIENTE_ATIVO."</div>"; }
				if( $o_SITUACAO == "I"){ $o_SITUACAO_TXT = "<div class=\"grid_color_red\">".TXT_ADM_CLIENTE_INATIVO."</div>"; }
				
				# Formata endereco
				if(!isEmpty($o_END))
				{
					$END_TXT = $o_END;
					if($o_NUMERO){ $END_TXT .= ", ".$o_NUMERO; }
					if($o_COMPL){ $END_TXT .= ", ".$o_COMPL; }
					if($o_BAIRRO){ $END_TXT .= "<br/>".$o_BAIRRO; }
					if($o_CIDADE){ $END_TXT .= "<br/>".$o_CIDADE; }
					if($o_UF){ $END_TXT .= " - ".$o_UF; }
					if($o_CEP){ $END_TXT .= " - CEP ".$o_CEP; }
					$o_END_TXT = "<small>".$END_TXT."</small>";
				}
				
				# Fromata Laudos
				if($o_LD_RUIDO    == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_RUIDO.'<br/>'; }
				if($o_LD_PART     == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_PART.'<br/>'; }
				if($o_LD_POEI     == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_POEI.'<br/>'; }
				if($o_LD_VAP      == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_VAP.'<br/>'; }
				if($o_LD_VIBR_VCI == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_VIBR_VCI.'<br/>'; }
				if($o_LD_VIBR_VMB == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_VIBR_VMB.'<br/>'; }
				if($o_LD_CALOR    == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_CALOR.'<br/>'; }
				if($o_LD_BIO      == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_BIO.'<br/>'; }
				if($o_LD_ELETR    == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_ELETR.'<br/>'; }
				if($o_LD_EXP      == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_EXP.'<br/>'; }
				if($o_LD_INFL     == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_INFL.'<br/>'; }
				if($o_LD_RAD      == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_RAD.'<br/>'; }
				if($o_LD_RIS      == 1){ $o_LAUDOS_TXT .= '<i class="fa fa-check-circle"></i>&nbsp;'.TXT_ADM_CLIENTE_LD_TIT_RIS.'<br/>'; }
				if($o_LAUDOS_TXT){} else { $o_LAUDOS_TXT = "Nenhum laudo selecionado!"; }
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_NOME_INTERNO.':</b></th><td>'.$o_NOME_INTERNO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_EMPRESA.':</b></th><td>'.$o_EMPRESA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CNPJ.':</b></th><td>'.$o_CNPJ.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_FONE_1.':</b></th><td>'.$o_FONE1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_FONE_2.':</b></th><td>'.$o_FONE2.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_E_MAIL.':</b></th><td>'.$o_EMAIL.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_ENDERECO.':</b></th><td>'.$o_END_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_OBSERVACAO.':</b></th><td>'.$o_OBS.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CONTATO_NOME.':</b></th><td>'.$o_CONTATO_NOME.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CONTATO_FONE.':</b></th><td>'.$o_CONTATO_FONE1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CONTATO_CELULAR.':</b></th><td>'.$o_CONTATO_CEL1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CONTATO_WHATSAPP.':</b></th><td>'.$o_CONTATO_WHATSAPP.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_CONTATO_EMAIL.':</b></th><td>'.$o_CONTATO_EMAIL.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_LAUDOS.':</b></th><td>'.$o_LAUDOS_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_ADM_CLIENTE_SITUACAO.':</b></th><td>'.$o_SITUACAO_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
