<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: conf_pagto.php
## Função..........: Efetua a confirmacao do pagamento e liberacao da conta do cliente
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_TOKEN = $IN_VALOR_ATIVACAO = "";
	
	#http://segvida.prodfy.com.br/cgi-bin/conf_pagto.php?l=pt-br&va=0&t=N2O4H4I9J1A2H7A2C6E5X5I9C8A4B9I91501863468
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG           = test_input($_GET["l"]);
		$IN_TOKEN          = test_input($_GET["t"]);
		$IN_VALOR_ATIVACAO = test_input($_GET["va"]);
		
		
/*	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_TOKEN      = test_input($_POST["t"]);
*/
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !isEmpty($IN_TOKEN) &&
		    !isEmpty($IN_VALOR_ATIVACAO)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			####
			# Verifica se o token informado existe
			####
			
			/*
			$QUERY = "SELECT U.`idSYSTEM_USER_ACCOUNT` as idprodfy_user_account,
              C.`idSYSTEM_CLIENTE` as idprodfy_cliente,
              C.`pagto_plano_codigo` as plano_codigo,
              CASE WHEN upper('".$IN_LANG."') = 'PT-BR' THEN `titulo_br`
                   WHEN upper('".$IN_LANG."') = 'EN-US' THEN `titulo_us`
                   WHEN upper('".$IN_LANG."') = 'ES-ES' THEN `titulo_sp`
               END AS plano_titulo,
              P.`tipo` as tipo,
              U.`nome` as nome,
              U.`sobrenome` as sobrenome,
              lower(U.`email`) as email
         FROM `SYSTEM_USER_ACCOUNT` U 
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_USER_ACCOUNT` = U.`idSYSTEM_USER_ACCOUNT`
   INNER JOIN `SYSTEM_PLANO_PAGTO` as P
           ON P.`codigo` = C.`pagto_plano_codigo`
        WHERE upper(C.`pagto_token`) = upper('".$IN_TOKEN."')";
			*/
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT U.`idSYSTEM_USER_ACCOUNT` as idsystem_user_account,
              C.`idSYSTEM_CLIENTE` as idsystem_cliente,
              C.`pagto_plano_codigo` as plano_codigo,
              CASE WHEN upper('".$IN_LANG."') = 'PT-BR' THEN `titulo_br`
                   WHEN upper('".$IN_LANG."') = 'EN-US' THEN `titulo_us`
                   WHEN upper('".$IN_LANG."') = 'ES-ES' THEN `titulo_sp`
               END AS plano_titulo,
              P.`tipo` as tipo,
              U.`nome` as nome,
              U.`sobrenome` as sobrenome,
              lower(U.`email`) as email
         FROM `SYSTEM_USER_ACCOUNT` U 
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_USER_ACCOUNT` = U.`idSYSTEM_USER_ACCOUNT`
   INNER JOIN `SYSTEM_PLANO_PAGTO` as P
           ON P.`codigo` = C.`pagto_plano_codigo`
        WHERE upper(C.`pagto_token`) = upper(?)")) 
			{
				$sql_TOKEN = $mysqli->escape_String($IN_TOKEN);
				//
				$stmt->bind_param('s', $sql_TOKEN);
				
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_idSYSTEM_USER_ACCOUNT, $o_idSYSTEM_CLIENTE, $o_PLANO_CODIGO, $o_PLANO_TITULO, $o_PLANO_TIPO, $o_NOME, $o_SOBRENOME, $o_EMAIL);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|".TXT_CONF_PAGTO_TOKEN_INEXISTENTE."|alert|");
					exit;
				}
				else
				{
					####
					# Encontrou o token, processa liberacao do plano contratado
					####
					
					## Calcula tempo de vigencia do plano contratado
					if($o_PLANO_TIPO == "M")
					{
						$prazo_plano = 30;
						$TIPO_TXT = TXT_MENSAL;
					}
					else if ($o_PLANO_TIPO == "A")
					{
						$prazo_plano = 365;
						$TIPO_TXT = TXT_ANUAL;
					}
					
					## Libera Plano Contratado Pelo Cliente
					liberaPlanoContratoDoCliente($mysqli, $_DEBUG, $o_idSYSTEM_CLIENTE, $prazo_plano, $IN_VALOR_ATIVACAO);
					
					####
					# Plano contratado liberado, avisa ao cliente por email
					####
					
					#Formata URL de acesso
					//$CONF_URL = WEB_ADDRESS."/cgi-bin/conf_registro.php?lang=".$IN_LANG."&token=".$TOKEN_TMP;
					$ACESSO_URL = WEB_ADDRESS."/index.php";
					
					
					#Formata tipo do plano
					if($o_PLANO_TIPO == "M"){ $PLANO_TIPO_TEXT = TXT_PLANO_TIPO_M; }
					                   else { $PLANO_TIPO_TEXT = TXT_PLANO_TIPO_A; }
					
					#Formata Valor do Plano
					//$MOEDA_LOCALE = "pt_BR";
					//if($IN_LANG == "US"){ $MOEDA_LOCALE = "en_US"; }
					//if($IN_LANG == "SP"){ $MOEDA_LOCALE = "sp_SP"; }
					//$PLANO_VALOR = formata_moeda($MOEDA_LOCALE,$PLANO_VALOR_TMP);
					
					
					## Create a new PHPMailer instance
					$mail = new PHPMailer(true);
					
					##Formata array com os dados
					$template_map = array(
						'{{IMG_URL}}'        => IMG_URL,
						'{{NOME}}'           => mb_strtoupper($o_NOME,"UTF-8"),
						'{{SOBRENOME}}'      => mb_strtoupper($o_SOBRENOME,"UTF-8"),
						'{{EMAIL}}'          => mb_strtolower($o_EMAIL,"UTF-8"),
						'{{PLANO}}'          => mb_strtoupper($o_PLANO_TITULO." - ".$PLANO_TIPO_TEXT,"UTF-8"),
						'{{LOGIN_URL}}'      => $ACESSO_URL
					);
					
					
					
					##Carrega texto do email via template ja processado
					#$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_REGISTRO, $template_map,"pt-br");
					$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_CONF_PAGTO, $template_map, $IN_LANG);
					
					$isMailSent = 0;
					
					try 
					{
						ini_set("sendmail_from", "");
						$mail->CharSet = 'UTF-8';
						$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
						
						$mail->AddAddress($o_EMAIL);//Set who the message is to be sent to
						
						//if(isset(EMAIL_CONTATO_BCC))
						//{
						//	$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
						//}
						
						$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
						
						$mail->Subject = TXT_EMAIL_SUBJECT_CONFIRMACAO_PAGTO;//Set the subject line
						
						//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
						//$mail->MsgHTML(file_get_contents('contents.html'));
						$mail->MsgHTML($body);
						//$mail->AddAttachment('images/phpmailer.gif');      // attachment
						//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
						//if($mail->Send();
						if ($mail->send()) { $isMailSent = 1; }
					} 
					catch (phpmailerException $e) { $MailError = $e->errorMessage(); } 
					catch (Exception $e) { $MailException = $e->getMessage(); }
					
					//send the message, check for errors
					if (!$isMailSent) 
					{
						error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_CONF_PAGTO."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
					}
					
					die("1|".TXT_CONF_PAGTO_MSG_PLANO_ATIVADO."|success|");
					exit; 
				}
			}
			else
			{
				if($_DEBUG){ die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")A|error|"); }
				      else { die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"); }
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# FUNCOES DE APOIO
	####
	function liberaPlanoContratoDoCliente($mysqli, $_DEBUG, $_idSYSTEM_CLIENTE, $_PRAZO_PLANO, $_VALOR_ATIVACAO)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_idSYSTEM_CLIENTE);
		$sql_PRAZO_PLANO      = $mysqli->escape_String($_PRAZO_PLANO);
		$sql_VALOR_ATIVACAO   = $mysqli->escape_String($_VALOR_ATIVACAO);
		
		$RET = 0;
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `SYSTEM_CLIENTE` 
        SET `data_ativacao`  = now(),
            `data_expiracao` = now()+interval ? day,
            `valor_ativacao` = ?,
            `pagto_token`    = null
      WHERE `idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('isd', $sql_PRAZO_PLANO, $sql_VALOR_ATIVACAO, $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			$RET = 1;
		}
		//else
		//{
		//	if($_DEBUG){ die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")C|error|"); }
		//	      else { die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(C)|error|"); }
		//	exit;
		//}
		
		return $RET;
	}
	
#################################################################################
###########
#####
##
?>
