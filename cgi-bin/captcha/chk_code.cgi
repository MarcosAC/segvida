#!/usr/bin/perl -w
##############################################################################
# By BumbleBeeWare.com 2006
# Simple CAPTCHA using static premade images
# check-captcha.cgi
##############################################################################

	BEGIN {
    my $b__dir = (-d '/home/rlgom197/perl'?'/home/rlgom197/perl':( getpwuid($>) )[7].'/perl');
    unshift @INC,$b__dir.'5/lib/perl5',$b__dir.'5/lib/perl5/i686-linux',map { $b__dir . $_ } @INC;
	}

	## Carrega charset UTF8 como padrão interno
	#use utf8;
	
	use CGI qw(:cgi-lib);## Carrega CGI.pm
	ReadParse(*req);
	use CGI::Carp qw/fatalsToBrowser/;
	$CGI::POST_MAX=1024 * 20000;  # max 100K posts
	#$CGI::DISABLE_UPLOADS = 0;  # no uploads
	#$TempFile::TMPDIRECTORY = '/srv/www/htdocs/cgi-bin/incoming';
	
	# lets block direct access that is not via the form post
	if ($ENV{"REQUEST_METHOD"} ne "POST"){&nopost;}

	
	## Inputs
	$IN_LANG = $req{'lang'};
	$IN_CODE = lc($req{'code'});
	
	if($IN_LANG eq ''){ $IN_LANG = 'pt-br'; }
	
	##########################
	# configuration
	$tempdir = "/home/rlgom197/public_html/cgi-bin/captcha/temp";
	
	# use this program to remove all old temp files
	# this keeps the director clean without setting up a cron job
	opendir TMPDIR, "$tempdir"; 
	@alltmpfiles = readdir TMPDIR;
	
	foreach $oldtemp (@alltmpfiles) 
	{
		$age = 0;
		$age = (stat("$tempdir/$oldtemp"))[9];
		# if age is more than 300 seconds or 5 minutes	
		if ((time - $age) > 300){unlink "$tempdir/$oldtemp";}
	}
	
	# open the temp datafile for current user based on ip
	$tempfile = "$tempdir/$ENV{'REMOTE_ADDR'}";
	open (TMPFILE, "<$tempfile")|| ($nofile = 1);
	(@tmpfile) = <TMPFILE>;
	close TMPFILE;
	
	# if no matching ip file check for a cookie match
	# this will compensate for AOL proxy servers accessing images
	if ($nofile == 1)
	{
		$cookieip = $ENV{HTTP_COOKIE};
		$cookieip =~ /checkme=([^;]*)/;
		$cookieip = $1;
		
		if ($cookieip ne "")
		{
			$tempfile = "$tempdir/$cookieip";
			open (TMPFILE, "<$tempdir/$cookieip")|| &nofile;
			(@tmpfile) = <TMPFILE>;
			close TMPFILE;
		}
	}
	
	$imagetext = $tmpfile[0];
	chomp $imagetext;
	
	# compare the form input with the file text
	if ($IN_CODE ne "$imagetext"){&error;}
	
	# now delete the temp file so it cannot be used again by the same user
	unlink "$tempfile";
	
	# if no error continue with the program
	print "Content-type: text/html; charset=utf-8\n\n";
	print "1|ok|";
	exit;



sub error 
{
	print "Content-type: text/html; charset=utf-8\n\n";
	#print "input verification code does not match the text on the image.";
	if($IN_LANG eq 'pt-br'){ $TXT = "Código incorreto!";  }
	if($IN_LANG eq 'en-us'){ $TXT = "Incorrect code!";    }
	if($IN_LANG eq 'es-es'){ $TXT = "Código incorrecto!"; }
	print "0|".$TXT."|";
	# now delete the temp file so it cannot be used again by the same user
	unlink "$tempdir/$ENV{'REMOTE_ADDR'}";
	exit;	
}

sub nofile 
{
	print "Content-type: text/html; charset=utf-8\n\n";
	#print "no file found for verification.";	
	if($IN_LANG eq 'pt-br'){ $TXT = "Código incorreto!";  }
	if($IN_LANG eq 'en-us'){ $TXT = "Incorrect code!";    }
	if($IN_LANG eq 'es-es'){ $TXT = "Código incorrecto!"; }
	print "0|".$TXT."|";
	exit;	
}


sub nopost 
{
	print "Content-type: text/html; charset=utf-8\n\n";
	#print "method not allowed, input must be via a form post.";	
	if($IN_LANG eq 'pt-br'){ $TXT = "Código incorreto!";  }
	if($IN_LANG eq 'en-us'){ $TXT = "Incorrect code!";    }
	if($IN_LANG eq 'es-es'){ $TXT = "Código incorrecto!"; }
	print "0|".$TXT."|";
	exit;	
}



#sub form_parse  
#{
#	read (STDIN, $buffer, $ENV{'CONTENT_LENGTH'});
#	@pairs = split(/&/, $buffer);
#
#	foreach $pair (@pairs)
#	{
#		($name, $value) = split(/=/, $pair);
#		$value =~ tr/+/ /;
#		$value =~ s/%([a-fA-F0-9][a-fA-F0-9])/pack("C", hex($1))/eg;
#		$FORM{$name} = $value;
#	}
#}


