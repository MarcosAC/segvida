<?php
#################################################################################
## SEGVIDA
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: lostpwd.php
## Função..........: Gera um token para redefinicao de senha de acesso
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_USERNAME = $IN_EMAIL = "";
	
	//www.rlgomide.com/cgi-bin/registro.php?plano=G&nome=rodrigo&sobrenome=gomide&email=rlgomide%40gmail.com&cpf=03104925640&cnpj=123123123000145&razao=teste%20de%20razao&fonefixo1=031988891655&celular=031988891766&pais=BR&cep=36570000&uf=MG&cidade=3171303&bairro=joao%20braz&end=rua%20pedra%20do%20anta&num=65&compl=casa%20b%20fundos&username=rlgomide&password=123123
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["l"]);
		$IN_USERNAME   = test_input($_GET["u"]);
		$IN_EMAIL      = test_input($_GET["e"]);
	*/	
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_USERNAME   = test_input($_POST["u"]);
		$IN_EMAIL      = test_input($_POST["e"]);
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_USERNAME) &&
		    !empty($IN_EMAIL) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## GERA TOKEN
			$TOKEN_TMP = strtoupper(gera_token());
			
			## uppercase
			mb_strtolower($IN_EMAIL,"UTF-8");
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT F_RecuperaSenha_UPDATE(?,?,?,?) as ret"
			)) 
			{
				$sql_LANG      = $mysqli->escape_string($IN_LANG);
				$sql_TOKEN_TMP = $mysqli->escape_string($TOKEN_TMP);
				$sql_USERNAME  = $mysqli->escape_string($IN_USERNAME);
				$sql_EMAIL     = $mysqli->escape_string($IN_EMAIL);
				//
				$stmt->bind_param('ssss', $sql_LANG,$sql_TOKEN_TMP,$sql_USERNAME,$sql_EMAIL);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_RESULT);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|".TXT_ERRO_SOLICITACAO."|alert|");
					exit;
				}
				else
				{
					//status|msg|user_account_id|cliente_gescond_id|plano_dias_gratis|plano_tipo|plano_tit|plano_valor|
					list($STATUS, 
					     $MSG, 
					     $EMAIL
					    ) = explode("|", $o_RESULT);
					
					if($STATUS == 0)
					{
						die($STATUS."|".$MSG."|alert|");
						exit;
					}
					
					//echo "\n\n($STATUS)($MSG)($USER_ACCOUNT_ID)($CLIENTE_GESCOND_ID)($PLANO_DIAS_GRATIS)($PLANO_TIPO)($PLANO_TITULO)($PLANO_VALOR_TMP)\n\n";
					
					#Formata URL de confirmacao
					$RESET_URL = WEB_ADDRESS."/cgi-bin/newpwd.php?l=".$IN_LANG."&t=".$TOKEN_TMP;
					
					## Create a new PHPMailer instance
					$mail = new PHPMailer(true);
					
					##Formata array com os dados
					$template_map = array(
						'{{IMG_URL}}'         => IMG_URL,
						'{{EMAIL}}'           => mb_strtolower($EMAIL,"UTF-8"),
						'{{RESET_URL}}'       => $RESET_URL
					);
					
					
					
					##Carrega texto do email via template ja processado
					$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_NOVA_SENHA,$template_map,$IN_LANG);
					
					$isMailSent = 0;
					
					try 
					{
						$mail->CharSet = 'UTF-8';
						$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
						$mail->AddAddress($EMAIL);//Set who the message is to be sent to
						
						//if(!empty(EMAIL_CONTATO_BCC))
						//{
						//	$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
						//}
						
						$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
						
						$mail->Subject = TXT_EMAIL_SUBJECT_REDEFINICAO_SENHA;//Set the subject line
						
						$mail->MsgHTML($body);
						
						if ($mail->send()) { $isMailSent = 1; }
					} 
					catch (phpmailerException $e) 
					{
						$MailError = $e->errorMessage();//Pretty error messages from PHPMailer //echo "\nerrorMessage => ".$e->errorMessage();
					} 
					catch (Exception $e) 
					{
						$MailException = $e->getMessage();//Boring error messages from anything else! //echo "\Exception => ".$e->getMessage();
					}
					
									//send the message, check for errors
					if (!$isMailSent) 
					{
						error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_NOVA_SENHA."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
					}
					
					$MSG = TXT_MSG_REDEFINICAO_SENHA_INSTRUCOES;
					
					die($STATUS."|".$MSG."|success|");
					exit; 
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
