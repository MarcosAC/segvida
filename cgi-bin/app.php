<?php
#################################################################################
## Prodfy Plantas
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: app.php
## Função..........: controler de comunicacao entre o APP e o PORTAL Prodfy
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	//header('Content-Type: text/html; charset=utf-8');
	header('Content-Type: application/json; charset=UTF-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_ACTION = $IN_KEY = $IN_DATA = $IN_USERNAME = $IN_CPF = $IN_SITUACAO = "";
	
	/*
	Config inicial
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=gsd&k=096b3566653a8a8e58c7d0cc9cd58199a1b7c3ca076301ef7fd297ce11e5932153a96f3b6f74eb18
	
	Sincronia
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=s&k=bb07d2e3e0f27a644664be2760ca22a875ba175de9a1d16d9f615f3a7b3a6c20f3223638cd3b9525
	
	Upload Contagem
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=u-cntgm&k=bb07d2e3e0f27a644664be2760ca22a875ba175de9a1d16d9f615f3a7b3a6c20f3223638cd3b9525&d=teste
	
	Upload Data
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=u&k=bb07d2e3e0f27a644664be2760ca22a875ba175de9a1d16d9f615f3a7b3a6c20f3223638cd3b9525&d={"contagem" : [{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:35","qtde" : "1","lote_id" : "1","data_fim" : "2017-03-03 23:27:58"},{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:38","qtde" : "5","lote_id" : "2","data_fim" : "2017-03-03 23:27:58"},{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:41","qtde" : "20","lote_id" : "3","data_fim" : "2017-03-03 23:27:58"}]}
	
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=u&k=bb07d2e3e0f27a644664be2760ca22a875ba175de9a1d16d9f615f3a7b3a6c20f3223638cd3b9525&d={"hist" : [],"contagem" : [{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:35","qtde" : "1","lote_id" : "1","data_fim" : "2017-03-03 23:27:58"},{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:38","qtde" : "5","lote_id" : "2","data_fim" : "2017-03-03 23:27:58"},{"disp_id" : "5","data_inicio" : "2017-03-03 23:27:41","qtde" : "20","lote_id" : "3","data_fim" : "2017-03-03 23:27:58"}],"perda" : []}
	
	http://mudas.prodfy.com.br/cgi-bin/app.php?l=pt-br&a=u&k=53ce581ed2dc884bbef3b2d84536cad869984b1ec00150d8d7d0e29dc2630bb818427d0591ce0956&d={"hist" : [{"disp_id" : "7","lote_id" : "1","data" : "2017-03-11","titulo" : "teste","texto" : "isso é um teste"},{"disp_id" : "7","lote_id" : "1","data" : "2017-03-10","titulo" : "teste 2","texto" : "Este é um teste de histórico informado no smartphone"}],"contagem" : [],"perda" : []}
	
	
	*/
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = getRequestInput("l");
		$IN_KEY        = getRequestInput("k");
		$IN_ACTION     = getRequestInput("a");
		$IN_DATA       = getRequestInput("d");
		
		
		
/ */
		//$_GET = json_decode(file_get_contents('php://input'), true);
		//$_POST = json_decode(file_get_contents('php://input'), true);
		
	if($_SERVER["REQUEST_METHOD"] == "POST" || $_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = getRequestInputRaw("l");
		$IN_KEY        = getRequestInputRaw("k");
		$IN_ACTION     = getRequestInputRaw("a");
		$IN_DATA       = getRequestInputRaw("d");
		
		
		
		
		####
		# Set Debug Mode
		####
		$_DEBUG=1;
		
		## Define idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_ACTION) || isEmpty($IN_KEY) ) { die(geraAppErrorMsgJSON(TXT_FALHA_TRANSF_DADOS)); exit; }
		
		####
		# Conecta ao Banco de Dados
		####
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die(geraAppErrorMsgJSON(TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error())); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		####
		# Valida app_key
		####
		
		$sql_APP_KEY = $mysqli->escape_String($IN_KEY);
		
		##Prepara query
		#        AND AA.`uso_liberado` = 1
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idPRODFY_CLIENTE` as idprodfy_cliente, 
            AA.`idDISPOSITIVO` as id,
            AA.`numero` as numero,
            AA.`username` as username,
            AA.`app_key` as app_key
       FROM `DISPOSITIVO` AA
 INNER JOIN `PRODFY_CLIENTE` C
         ON C.`idPRODFY_CLIENTE` = AA.`idPRODFY_CLIENTE`
  LEFT JOIN `COLABORADOR` as CLBRDR
         ON CLBRDR.`username` = AA.`username`
  LEFT JOIN `PRODFY_USER_ACCOUNT` U
         ON U.`username` = AA.`username`
      WHERE AA.`app_key` = ?
   ORDER BY 1,2"
		)) 
		{
			$stmt->bind_param('s', $sql_APP_KEY);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idPRODFY_CLIENTE, $o_idDISPOSITIVO, $o_DISP_NUM, $o_USERNAME, $o_APP_KEY);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$o_APP_OK = 0;
				return geraAppErrorMsgJSON(TXT_APP_KEY_NOT_FOUND);
			}
			else
			{
				$o_APP_OK = 1;
			}
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		####
		# Processa acao solicitada
		####
		switch($IN_ACTION)
		{
			## GET SETUP DATA
			case 'gsd':
				## Executa
				$tmp_RESULT = executa_GetSetupData($mysqli, $_DEBUG, $o_idPRODFY_CLIENTE, $o_idDISPOSITIVO, $o_APP_KEY, $IN_LANG);
				die($tmp_RESULT);
				exit;
				break;
			## GET SINC DATA
			case 's':
				## Executa
				$tmp_RESULT = executa_GetSincData($mysqli, $_DEBUG, $o_idPRODFY_CLIENTE, $o_idDISPOSITIVO, $o_APP_KEY, $IN_LANG);
				die($tmp_RESULT);
				exit;
				break;
			## POST UPLOAD DATA
			case 'u':
				## Executa
				$tmp_RESULT = executa_PostUploadData($mysqli, $_DEBUG, $o_idPRODFY_CLIENTE, $o_idDISPOSITIVO, $o_DISP_NUM, $o_USERNAME, $o_APP_KEY, $IN_LANG, $IN_DATA);
				die($tmp_RESULT);
				exit;
				break;
			/*## POST CONTAGEM DATA
			case 'u-cntgm':
				## Executa
				$tmp_RESULT = executa_PostContagemData($mysqli, $_DEBUG, $o_idPRODFY_CLIENTE, $o_idDISPOSITIVO, $o_APP_KEY, $IN_LANG, $IN_DATA);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_ACTION) || isEmpty($IN_NUMERO) || isEmpty($IN_USO_LIBERADO) ) 
				{ $GO=0; die(geraAppErrorMsgJSON(TXT_FALHA_TRANSF_DADOS)); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idPRODFY_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_NUMERO, $IN_SENHA, $IN_COLAB, $IN_USO_LIBERADO);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_ACTION) || isEmpty($IN_REG_ID) || isEmpty($IN_NUMERO) || isEmpty($IN_USO_LIBERADO) ) 
				{ $GO=0; die(geraAppErrorMsgJSON(TXT_FALHA_TRANSF_DADOS)); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idPRODFY_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_NUMERO, $IN_SENHA, $IN_COLAB, $IN_USO_LIBERADO);
				die($tmp_RESULT);
				exit;
				break;*/
			## DEFAULT
			default:
				$GO=0; die(geraAppErrorMsgJSON(TXT_FALHA_TRANSF_DADOS."(B)")); exit;
				break;
		}
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die(geraAppErrorMsgJSON("O.o?"));
	}
	
	
	####
	# GET SETUP DATA
	####
	function executa_GetSetupData($mysqli, $_DEBUG, $_idPRODFY_CLIENTE, $_idDISPOSITIVO, $_APP_KEY, $_LANG)
	{
		## Gera escapes das variaveis
		$sql_DDMMYYYY         = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS   = '%d/%m/%Y %T';
		$sql_idPRODFY_CLIENTE = $mysqli->escape_String($_idPRODFY_CLIENTE);
		$sql_idDISPOSITIVO    = $mysqli->escape_String($_idDISPOSITIVO);
		$sql_APP_KEY          = $mysqli->escape_String($_APP_KEY);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT   AA.`idDISPOSITIVO` as id, 
              AA.`numero` as numero, 
              AA.`senha` as senha, 
              upper(U.`nome`) as nome, upper(U.`sobrenome`) as sobrenome,
              AA.`app_key` as app_key,
              upper(U2.`razao_social`) as razao
         FROM `DISPOSITIVO` AA
   INNER JOIN `PRODFY_CLIENTE` C
           ON C.`idPRODFY_CLIENTE` = AA.`idPRODFY_CLIENTE`
    LEFT JOIN `COLABORADOR` as CLBRDR
           ON CLBRDR.`username` = AA.`username`
    LEFT JOIN `PRODFY_USER_ACCOUNT` U
           ON U.`username` = AA.`username`
    LEFT JOIN `PRODFY_USER_ACCOUNT` U2
           ON U2.`idPRODFY_USER_ACCOUNT` = C.`idPRODFY_USER_ACCOUNT`
        WHERE AA.`idPRODFY_CLIENTE` = ?
          AND AA.`idDISPOSITIVO` = ?
     ORDER BY 1,2"
		)) 
		{
			$stmt->bind_param('ss', $sql_idPRODFY_CLIENTE, $sql_idDISPOSITIVO);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_NUMERO, $o_SENHA, $o_NOME, $o_SOBRENOME, $o_APP_KEY, $o_EMPRESA);
			
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return geraAppErrorMsgJSON(TXT_APP_SETUP_DATA_NOT_FOUND);
			}
			
			$o_AUTOSINC = 0;
			$o_AUTOSINC_TIME = 0;
			
			##Prepara JSON
			#$INFO = array("diretor"=>"Romulo Balga","CTO"=>"Rodrigo Gomide");
			
			$TMP = array(
				"sinc_stat"     => 1, 
				"sinc_msg"      => "",
				"lang"          => "$_LANG", 
				"app_key"       => "$o_APP_KEY", 
				"disp_id"       => $o_ID,
				"disp_num"      => $o_NUMERO,
				"senha"         => "$o_SENHA",
				"nome"          => "$o_NOME",
				"sobrenome"     => "$o_SOBRENOME",
				"empresa"       => "$o_EMPRESA",
				"autosinc"      => $o_AUTOSINC,
				"autosinc_time" => $o_AUTOSINC_TIME
			);
			
			$JSON = json_encode($TMP);
			return $JSON;
			exit;
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error)); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
	}
	
	####
	# GET SINC DATA
	####
	function executa_GetSincData($mysqli, $_DEBUG, $_idPRODFY_CLIENTE, $_idDISPOSITIVO, $_APP_KEY, $_LANG)
	{
		/****
		 * Formata dados para envio ao APP
		 * - PONTO_CONTROLE
		 * - PONTO_CONTROLE_ESTAGIO
		 * - LOTE
		 * - LOTE_PERDA_MOTIVO
		 ****/
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY         = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS   = '%d/%m/%Y %T';
		$sql_idPRODFY_CLIENTE = $mysqli->escape_String($_idPRODFY_CLIENTE);
		$sql_idDISPOSITIVO    = $mysqli->escape_String($_idDISPOSITIVO);
		$sql_APP_KEY          = $mysqli->escape_String($_APP_KEY);
		
		## Carrega os dados dentro do critério especificado
		
		#############################
		## PONTO_CONTROLE
		if ($stmt = $mysqli->prepare(
		"SELECT A.`idPONTO_CONTROLE`,
            upper(A.`codigo`), 
            upper(A.`titulo`),
            A.`maturacao`,
            A.`unidade`,
            CASE WHEN upper(A.`unidade`) = 'SG' THEN A.`maturacao`
                 WHEN upper(A.`unidade`) = 'MN' THEN (A.`maturacao` * 60)
                 WHEN upper(A.`unidade`) = 'HR' THEN (A.`maturacao` * 3600)
                 WHEN upper(A.`unidade`) = 'DD' THEN (A.`maturacao` * 86400)
                 WHEN upper(A.`unidade`) = 'SM' THEN (A.`maturacao` * 604800)
                 WHEN upper(A.`unidade`) = 'MM' THEN (A.`maturacao` * 18144000)
                 WHEN upper(A.`unidade`) = 'AA' THEN (A.`maturacao` * 217728000)
             END AS maturacao_segundos
       FROM `PONTO_CONTROLE` A
      WHERE A.`idPRODFY_CLIENTE` = ?
      ORDER BY 2"
		)) 
		{
			$stmt->bind_param('s', $sql_idPRODFY_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idPONTO_CONTROLE, $o_CODIGO, $o_TITULO, $o_MAT, $o_UNID, $o_MAT_SEG);
			
			$TMP_PONTO_CONTROLE_ITENS = array();
			
			while($stmt->fetch())
			{
				//$TMP = "";
				$TMP = array(
					"ponto_controle_id" => $o_idPONTO_CONTROLE, 
					"codigo"            => "$o_CODIGO",
					"titulo"            => "$o_TITULO", 
					"maturacao"         => $o_MAT, 
					"unidade"           => "$o_UNID",
					"maturacao_seg"     => $o_MAT_SEG
				);
				
				//$TMP_ITENS = array_merge($TMP_ITENS, $TMP);
				array_push($TMP_PONTO_CONTROLE_ITENS, $TMP);
				
			}
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error)); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		#############################
		## PONTO_CONTROLE_ESTAGIO
		if ($stmt = $mysqli->prepare(
		"SELECT A.`idPONTO_CONTROLE_ESTAGIO`,
            A.`idPONTO_CONTROLE`,
            upper(A.`codigo`), 
            upper(A.`titulo`),
            A.`maturacao`,
            A.`unidade`,
            CASE WHEN upper(A.`unidade`) = 'SG' THEN A.`maturacao`
                 WHEN upper(A.`unidade`) = 'MN' THEN (A.`maturacao` * 60)
                 WHEN upper(A.`unidade`) = 'HR' THEN (A.`maturacao` * 3600)
                 WHEN upper(A.`unidade`) = 'DD' THEN (A.`maturacao` * 86400)
                 WHEN upper(A.`unidade`) = 'SM' THEN (A.`maturacao` * 604800)
                 WHEN upper(A.`unidade`) = 'MM' THEN (A.`maturacao` * 18144000)
                 WHEN upper(A.`unidade`) = 'AA' THEN (A.`maturacao` * 217728000)
             END AS maturacao_segundos
       FROM `PONTO_CONTROLE_ESTAGIO` A
      WHERE A.`idPRODFY_CLIENTE` = ?
      ORDER BY 2"
		)) 
		{
			$stmt->bind_param('s', $sql_idPRODFY_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idPONTO_CONTROLE_ESTAGIO, $o_idPONTO_CONTROLE, $o_CODIGO, $o_TITULO, $o_MAT, $o_UNID, $o_MAT_SEG);
			
			$TMP_ESTAGIO_ITENS = array();
			
			while($stmt->fetch())
			{
				//$TMP = "";
				$TMP = array(
					"estagio_id"        => $o_idPONTO_CONTROLE_ESTAGIO, 
					"ponto_controle_id" => $o_idPONTO_CONTROLE, 
					"codigo"            => "$o_CODIGO",
					"titulo"            => "$o_TITULO", 
					"maturacao"         => $o_MAT, 
					"unidade"           => "$o_UNID",
					"maturacao_seg"     => $o_MAT_SEG
				);
				
				array_push($TMP_ESTAGIO_ITENS, $TMP);
				
			}
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error)); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		#############################
		## LOTE
		if ($stmt = $mysqli->prepare(
		"SELECT A.`idLOTE`,
            upper(A.`codigo`) as codigo, 
            upper(O.`titulo`) as objetivo,
            upper(M.`nome_interno`) as muda,
            upper(C.`nome_interno`) as cliente
       FROM `LOTE` A
       LEFT JOIN `OBJETIVO` O
              ON O.`idOBJETIVO` = A.`idOBJETIVO`
             AND O.`idPRODFY_CLIENTE` = A.`idPRODFY_CLIENTE`
       LEFT JOIN `MUDA` M
              ON M.`idMUDA` = A.`idMUDA`
             AND M.`idPRODFY_CLIENTE` = A.`idPRODFY_CLIENTE`
       LEFT JOIN `CLIENTE` C
              ON C.`idCLIENTE` = A.`idCLIENTE`
             AND C.`idPRODFY_CLIENTE` = A.`idPRODFY_CLIENTE`
      WHERE A.`idPRODFY_CLIENTE` = ?
      ORDER BY 2"
		)) 
		{
			$stmt->bind_param('s', $sql_idPRODFY_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idLOTE, $o_CODIGO, $o_OBJETIVO, $o_MUDA, $o_CLIENTE);
			
			$TMP_LOTE_ITENS = array();
			
			while($stmt->fetch())
			{
				//$TMP = "";
				$TMP = array(
					"lote_id"        => $o_idLOTE, 
					"codigo"         => "$o_CODIGO",
					"objetivo"       => "$o_OBJETIVO", 
					"clone"          => "$o_MUDA", 
					"cliente"        => "$o_CLIENTE"
				);
				
				array_push($TMP_LOTE_ITENS, $TMP);
				
			}
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error)); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		#############################
		## LOTE_PERDA_MOTIVO
		if ($stmt = $mysqli->prepare(
		"SELECT A.`idLOTE_PERDA_MOTIVO`,
            upper(A.`motivo`)
       FROM `LOTE_PERDA_MOTIVO` A
      WHERE A.`idPRODFY_CLIENTE` = ?
      ORDER BY 2"
		)) 
		{
			$stmt->bind_param('s', $sql_idPRODFY_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idLOTE_PERDA_MOTIVO, $o_MOTIVO);
			
			$TMP_PERDA_MOTIVO_ITENS = array();
			
			while($stmt->fetch())
			{
				//$TMP = "";
				$TMP = array(
					"perda_motivo_id"  => $o_idLOTE_PERDA_MOTIVO, 
					"motivo"           => "$o_MOTIVO"
				);
				
				array_push($TMP_PERDA_MOTIVO_ITENS, $TMP);
				
			}
			
		}
		else
		{
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error)); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		
		##Prepara JSON
		#$INFO = array("diretor"=>"Romulo Balga","CTO"=>"Rodrigo Gomide");
		
		$TMP_RET = array(
			"sinc_stat"          => 1, 
			"sinc_msg"           => "",
			"ponto_controle"     => $TMP_PONTO_CONTROLE_ITENS,
			"estagio"            => $TMP_ESTAGIO_ITENS,
			"lote"               => $TMP_LOTE_ITENS,
			"perda_motivo"       => $TMP_PERDA_MOTIVO_ITENS
		);
		
		$JSON = json_encode($TMP_RET);
		return $JSON;
		exit;
			
	}
	
	####
	# POST UPLOAD DATA
	####
	function executa_PostUploadData($mysqli, $_DEBUG, $_idPRODFY_CLIENTE, $_idDISPOSITIVO, $_DISP_NUM, $_USERNAME, $_APP_KEY, $_LANG, $_DATA)
	{
		/****
		 * Prepara dados para inclusao no banco de dados prodfy
		 * - CONTAGEM
		 * - PERDA
		 * - HISTORICO
		 * - Se existir dados, processa os dados e insere na base do cliente
		 ****/
		
		//error_log("DATA -> ".$_DATA, 0);
		
		$sql_idPRODFY_CLIENTE = $mysqli->escape_String($_idPRODFY_CLIENTE);
		$sql_APP_KEY          = $mysqli->escape_String($_APP_KEY);
		$sql_DISP_NUM         = $mysqli->escape_String($_DISP_NUM);
		$sql_USERNAME         = $mysqli->escape_String($_USERNAME);
		
		//$oDISP_NUM = $_DISP_NUM;
		//$oUSERNAME = $_USERNAME;
		
		$oSINC_STAT = 1;
		$oSINC_MSG  = TXT_APP_SINCRONIA_CONCLUIDA_COM_SUCESSO;
		
		$oSINC_CONTAGEM_STAT = 0;
		$oSINC_PERDA_STAT = 0;
		$oSINC_HIST_STAT = 0;
		
		//print_r("APP_KEY: ".$_APP_KEY."\n");
		//print_r("DISP_NUM: ".$_DISP_NUM."\n");
		//print_r("USERNAME: ".$_USERNAME."\n");
		//print_r("S_DATA:\n\n".$_DATA."\n");
		//exit;
		
		##Prepara JSON
		$DATA = json_decode($_DATA);
		$DATA = (array) $DATA;
		
		//print_r("JSON_DATA:\n\n");
		//print_r("contagem  -> ".count($DATA["contagem"])."\n");
		//print_r("perda     -> ".count($DATA["perda"])."\n");
		//print_r("historico -> ".count($DATA["hist"])."\n");
		//exit;
		
		
		################
		## CONTAGEM
		$oCONTAGEM_TOTAL = count($DATA["contagem"]);
		
		if($oCONTAGEM_TOTAL > 0)
		{
			//$oSINC_CONTAGEM_STAT = 1;
			
			//print_r("Iniciando contagem:\n\n");
			
			for($i=0; $i<$oCONTAGEM_TOTAL;$i++)
			{
				$ITEM = (array) $DATA["contagem"][$i];
				
				//print_r("[".$i."] -> disp_id(".$ITEM["disp_id"]."), lote_id(".$ITEM["lote_id"]."), data_inicio(".$ITEM["data_inicio"]."), data_fim(".$ITEM["data_fim"]."), qtde(".$ITEM["qtde"].");\n");
				
				//$oDISP_ID     = $ITEM["disp_id"];
				$oLOTE_ID     = $ITEM["lote_id"];
				$oDATA_INICIO = $ITEM["data_inicio"];
				$oDATA_FIM    = $ITEM["data_fim"];
				$oQTDE        = $ITEM["qtde"];
				$oPROC        = $ITEM["proc"];
				
				## Valida Dados Recebidos
				if(!isEmpty($oLOTE_ID) || 
					 !isEmpty($oDATA_INICIO) || 
					 !isEmpty($oDATA_FIM)
					)
				{
					## Verifica se o idLOTE e valido
					if( isValid_idLOTE($mysqli, $_idPRODFY_CLIENTE, $oLOTE_ID))
					{
						## Gera escapes
						$sql_idLOTE      = $mysqli->escape_String($oLOTE_ID);
						$sql_DATA_INICIO = $mysqli->escape_String($oDATA_INICIO);
						$sql_DATA_FIM    = $mysqli->escape_String($oDATA_FIM);
						
						## Processa QTDE
						$oQTDE_ATUAL = 0;
						
						if($oPROC == 2)
						{
							if ($stmt = $mysqli->prepare(
							"SELECT LI.`qtde` as qtde
					       FROM `LOTE_INVENTARIO` LI
					      WHERE LI.`idPRODFY_CLIENTE` = ?
					        AND LI.`idlote` = ?
					        AND LI.`data_fim` = (SELECT max(LI2.`data_fim`)
					                               FROM `LOTE_INVENTARIO` LI2
					                              WHERE LI2.`idLOTE_INVENTARIO` = LI.`idLOTE_INVENTARIO`
					                                AND LI2.`idLOTE` = LI.`idLOTE`
					                              LIMIT 1)
					      ORDER BY LI.`data_fim` desc
					      LIMIT 1"
							)) 
							{
								$stmt->bind_param('ss', $sql_idPRODFY_CLIENTE, $sql_idLOTE);
								$stmt->execute();
								$stmt->store_result();
								
								// obtém variáveis a partir dos resultados.
								$stmt->bind_result($oQTDE_ATUAL);
								$stmt->fetch();
								
							}
							else
							{
								error_log("APP.PHP -> ERROR -> CONTAGEM -> SELECT oQTDE_ATUAL -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
								if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
								      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
								exit;
							}
							
						}
						
						$oQTDE_ATUAL = $oQTDE_ATUAL + $oQTDE;
						
						$sql_QTDE = $mysqli->escape_String($oQTDE_ATUAL);
						
						##Prepara query
						if ($stmt = $mysqli->prepare(
						"INSERT INTO `LOTE_INVENTARIO` (`idPRODFY_CLIENTE`, `idLOTE`, `data_inicio`, `data_fim`, `qtde`, `origem_contagem`, `cad_num_dispositivo`, `cad_username`, `cad_date`)
             SELECT ?, ?, ?, ?, ?, 'D', ?, ?, now() FROM DUAL
              WHERE EXISTS(SELECT 1 
                             FROM `LOTE` L
                            WHERE L.`idPRODFY_CLIENTE` = ?
                              AND L.`idLOTE` = ?
                            LIMIT 1)
                AND NOT EXISTS (SELECT 1 
                                  FROM `LOTE_INVENTARIO` LI
                                 WHERE LI.`idPRODFY_CLIENTE` = ?
                                   AND LI.`idLOTE` = ?
                                   AND LI.`data_inicio` = ?
                                   AND LI.`data_fim` = ?
                                   AND LI.`qtde` = ?
                                   AND LI.`origem_contagem` = 'D'
                                   AND LI.`cad_num_dispositivo` = ?
                                   AND LI.`cad_username` = ?
                                 LIMIT 1)"
						)) 
						{
							$stmt->bind_param('ssssssssssssssss', $sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_DATA_INICIO, $sql_DATA_FIM, $sql_QTDE, $sql_DISP_NUM, $sql_USERNAME, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_DATA_INICIO, $sql_DATA_FIM, $sql_QTDE, $sql_DISP_NUM, $sql_USERNAME);
							
							if($stmt->execute()){} else
							{
								error_log("APP.PHP -> ERROR -> CONTAGEM -> INSERT INTO `LOTE_INVENTARIO` -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
								if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
								      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
								exit;
							}
							
						}
						else
						{
							error_log("APP.PHP -> ERROR -> CONTAGEM -> INSERT INTO `LOTE_INVENTARIO` -> STMT -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
							if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
							      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
							exit;
						}
						
					}
					
				}//endIf
				
			}//endFor
			
			$oSINC_CONTAGEM_STAT = 1;
			
		}
		
		################
		## PERDA
		$oPERDA_TOTAL = count($DATA["perda"]);
		
		if($oPERDA_TOTAL > 0)
		{
			//$oSINC_CONTAGEM_STAT = 1;
			
			//print_r("Iniciando contagem:\n\n");
			
			for($i=0; $i<$oPERDA_TOTAL;$i++)
			{
				$ITEM = (array) $DATA["perda"][$i];
				
				//print_r("[".$i."] -> disp_id(".$ITEM["disp_id"]."), lote_id(".$ITEM["lote_id"]."), data_inicio(".$ITEM["data_inicio"]."), data_fim(".$ITEM["data_fim"]."), qtde(".$ITEM["qtde"].");\n");
				
				//$oDISP_ID     = $ITEM["disp_id"];
				$oLOTE_ID           = $ITEM["lote_id"];
				$oDATA              = $ITEM["data"];
				$oPONTO_CONTROLE_ID = $ITEM["ponto_controle_id"];
				$oESTAGIO_ID        = $ITEM["estagio_id"];
				$oMOTIVO_ID         = $ITEM["motivo_id"];
				$oQTDE              = $ITEM["qtde"];
				
				## Valida Dados Recebidos
				if(!isEmpty($oLOTE_ID) || 
					 !isEmpty($oDATA) || 
					 !isEmpty($oPONTO_CONTROLE_ID) || 
					 !isEmpty($oESTAGIO_ID) || 
					 !isEmpty($oMOTIVO_ID) || 
					 !isEmpty($oQTDE)
					)
				{
					## Verifica se o idLOTE e valido
					if( isValid_idLOTE($mysqli, $_idPRODFY_CLIENTE, $oLOTE_ID) && $oQTDE > 0)
					{
						## Gera escapes
						$sql_idLOTE            = $mysqli->escape_String($oLOTE_ID);
						$sql_DATA              = $mysqli->escape_String($oDATA);
						$sql_PONTO_CONTROLE_ID = $mysqli->escape_String($oPONTO_CONTROLE_ID);
						$sql_ESTAGIO_ID        = $mysqli->escape_String($oESTAGIO_ID);
						$sql_MOTIVO_ID         = $mysqli->escape_String($oMOTIVO_ID);
						$sql_QTDE              = $mysqli->escape_String($oQTDE);
						
						##Prepara query
						if ($stmt = $mysqli->prepare(
						"INSERT INTO `LOTE_PERDA` 
             (`idPRODFY_CLIENTE`, `idLOTE`, `idPONTO_CONTROLE`, `idPONTO_CONTROLE_ESTAGIO`, `idLOTE_PERDA_MOTIVO`, 
              `data_perda`, `qtde`, `cad_username`, `cad_date`)
             SELECT ?, ?, ?, ?, ?, ?, ?, ?, now() FROM DUAL
              WHERE EXISTS(SELECT 1 
                             FROM `LOTE` L
                            WHERE L.`idPRODFY_CLIENTE` = ?
                              AND L.`idLOTE` = ?
                            LIMIT 1)
                AND NOT EXISTS (SELECT 1 
                                  FROM `LOTE_PERDA` LI
                                 WHERE LI.`idPRODFY_CLIENTE` = ?
                                   AND LI.`idLOTE` = ?
                                   AND LI.`idPONTO_CONTROLE` = ?
                                   AND LI.`idPONTO_CONTROLE_ESTAGIO` = ?
                                   AND LI.`idLOTE_PERDA_MOTIVO` = ?
                                   AND LI.`data_perda` = ?
                                   AND LI.`qtde` = ?
                                   AND LI.`cad_username` = ?
                                 LIMIT 1)"
						)) 
						{
							$stmt->bind_param('ssssssssssssssssss', 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_PONTO_CONTROLE_ID, $sql_ESTAGIO_ID, $sql_MOTIVO_ID, $sql_DATA, $sql_QTDE, $sql_USERNAME, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_PONTO_CONTROLE_ID, $sql_ESTAGIO_ID, $sql_MOTIVO_ID, $sql_DATA, $sql_QTDE, $sql_USERNAME);
							
							if($stmt->execute()){} else
							{
								error_log("APP.PHP -> ERROR -> PERDA -> INSERT INTO `LOTE_PERDA` -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
								if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
								      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
								exit;
							}
							
						}
						else
						{
							error_log("APP.PHP -> ERROR -> PERDA -> INSERT INTO `LOTE_PERDA` -> STMT -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
							if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
							      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
							exit;
						}
						
					}
					
				}//endIf
				
			}//endFor
			
			$oSINC_PERDA_STAT = 1;
			
		}
		
		################
		## HISTORICO
		$oHIST_TOTAL = count($DATA["hist"]);
		
		if($oHIST_TOTAL > 0)
		{
			//$oSINC_CONTAGEM_STAT = 1;
			
			//print_r("Iniciando contagem:\n\n");
			
			for($i=0; $i<$oHIST_TOTAL;$i++)
			{
				$ITEM = (array) $DATA["hist"][$i];
				
				//print_r("[".$i."] -> disp_id(".$ITEM["disp_id"]."), lote_id(".$ITEM["lote_id"]."), data_inicio(".$ITEM["data_inicio"]."), data_fim(".$ITEM["data_fim"]."), qtde(".$ITEM["qtde"].");\n");
				
				//$oDISP_ID     = $ITEM["disp_id"];
				$oLOTE_ID = $ITEM["lote_id"];
				$oDATA    = $ITEM["data"];
				$oTITULO  = $ITEM["titulo"];
				$oTEXTO   = $ITEM["texto"];
				
				## Valida Dados Recebidos
				if(!isEmpty($oLOTE_ID) || 
					 !isEmpty($oDATA) || 
					 !isEmpty($oTITULO) || 
					 !isEmpty($oTEXTO)
					)
				{
					## Verifica se o idLOTE e valido
					if( isValid_idLOTE($mysqli, $_idPRODFY_CLIENTE, $oLOTE_ID) )
					{
						## Gera escapes
						$sql_idLOTE   = $mysqli->escape_String($oLOTE_ID);
						$sql_DATA     = $mysqli->escape_String($oDATA);
						$sql_TITULO   = $mysqli->escape_String($oTITULO);
						$sql_TEXTO    = $mysqli->escape_String($oTEXTO);
						
						##Prepara query
						if ($stmt = $mysqli->prepare(
						"INSERT INTO `LOTE_HISTORICO` 
             (`idPRODFY_CLIENTE`, `idLOTE`, `data`, `titulo`, `texto`, `cad_username`, `cad_date`)
             SELECT ?, ?, ?, ?, ?, ?, now() FROM DUAL
              WHERE EXISTS(SELECT 1 
                             FROM `LOTE` L
                            WHERE L.`idPRODFY_CLIENTE` = ?
                              AND L.`idLOTE` = ?
                            LIMIT 1)
                AND NOT EXISTS (SELECT 1 
                                  FROM `LOTE_HISTORICO` LI
                                 WHERE LI.`idPRODFY_CLIENTE` = ?
                                   AND LI.`idLOTE` = ?
                                   AND LI.`data` = ?
                                   AND LI.`titulo` = ?
                                   AND LI.`texto` = ?
                                   AND LI.`cad_username` = ?
                                 LIMIT 1)"
						)) 
						{
							$stmt->bind_param('ssssssssssssss', 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_DATA, $sql_TITULO, $sql_TEXTO, $sql_USERNAME, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, 
							$sql_idPRODFY_CLIENTE, $sql_idLOTE, $sql_DATA, $sql_TITULO, $sql_TEXTO, $sql_USERNAME);
							
							if($stmt->execute()){} else
							{
								error_log("APP.PHP -> ERROR -> HISTORICO -> INSERT INTO `LOTE_HISTORICO` -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
								if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
								      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
								exit;
							}
							
						}
						else
						{
							error_log("APP.PHP -> ERROR -> HISTORICO -> INSERT INTO `LOTE_HISTORICO` -> STMT -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
							if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
							      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
							exit;
						}
						
					}
					
				}//endIf
				
			}//endFor
			
			$oSINC_HIST_STAT = 1;
			
		}
		
		$TMP_RET = array(
			"sinc_stat"           => $oSINC_STAT, 
			"sinc_msg"            => "$oSINC_MSG",
			"sinc_contagem_stat"  => $oSINC_CONTAGEM_STAT, 
			"sinc_perda_stat"     => $oSINC_PERDA_STAT, 
			"sinc_hist_stat"      => $oSINC_HIST_STAT
		);
		
		$JSON = json_encode($TMP_RET);
		return $JSON;
		exit;
			
	}
	
	####
	# Checa se o idLOTE e valido
	####
	function isValid_idLOTE($mysqli, $_idPRODFY_CLIENTE, $_idLOTE) 
	{
		$sql_idPRODFY_CLIENTE = $mysqli->escape_String($_idPRODFY_CLIENTE);
		$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		$oRET = 0;
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT 1 
       FROM `LOTE` L
      WHERE L.`idLOTE` = ?
        AND L.`idPRODFY_CLIENTE` = ?
      LIMIT 1"
		))
		{
			$stmt->bind_param('ss', $sql_idLOTE, $sql_idPRODFY_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CHK);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows > 0) 
			{
				$o_CHK = $o_CHK + 0;
				$oRET  = $o_CHK;
			}
			
		}
		else
		{
			error_log("APP.PHP -> ERROR -> isValid_idLOTE() -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
			if($_DEBUG){ return geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")"); }
			      else { geraAppErrorMsgJSON(TXT_INCAPAZ_DE_PREPARAR_SQL_STMT); }
			exit;
		}
		
		return $oRET;
	}

#################################################################################
###########
#####
##
?>
