<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-infl.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Marcos Aurelio (marcos.aucorrea@gmail.com)
## Criacao: 10/9/2017 16:14:55
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_IDCLIENTE = $IN_ANO = $IN_MES = $IN_PLANILHA_NUM = $IN_UNIDADE_SITE = $IN_DATA_ELABORACAO = $IN_AREA = $IN_SETOR = $IN_GES = $IN_CARGO_FUNCAO = $IN_CBO = $IN_ATIV_MACRO = $IN_TAREFA_EXEC = $IN_JOR_TRAB = $IN_TEMPO_EXPO = $IN_TIPO_EXPO = $IN_MEIO_PROPAG = $IN_FONTE_GERADORA = $IN_ATIVIDADE = $IN_COMBUSTIVEL = $IN_QTDE = $IN_FULGOR = $IN_INFLAMABILIDADE = $IN_EXPLOSIVIDADE = $IN_ITEM_4Q1 = $IN_ITEM_1Q3 = $IN_ITEM_2Q3 = $IN_ITEM_3Q3 = $IN_EPI = $IN_EPC = $IN_ENQ_NR16 = $IN_RESP_CAMPO_IDCOLABORADOR = $IN_RESP_TECNICO_IDCOLABORADOR = $IN_REGISTRO_RC = $IN_REGISTRO_RT = "";
	
	####
	# Set Debug Mode
	####
	#$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_IDCLIENTE = test_input($_GET["idcliente"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_PLANILHA_NUM = test_input($_GET["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_GET["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_GET["data_elaboracao"]);
		$IN_AREA = test_input($_GET["area"]);
		$IN_SETOR = test_input($_GET["setor"]);
		$IN_GES = test_input($_GET["ges"]);
		$IN_CARGO_FUNCAO = test_input($_GET["cargo_funcao"]);
		$IN_CBO = test_input($_GET["cbo"]);
		$IN_ATIV_MACRO = test_input($_GET["ativ_macro"]);
		$IN_TAREFA_EXEC = test_input($_GET["tarefa_exec"]);
		$IN_JOR_TRAB = test_input($_GET["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_GET["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_GET["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_GET["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_GET["fonte_geradora"]);
		$IN_ATIVIDADE = test_input($_GET["atividade"]);
		$IN_COMBUSTIVEL = test_input($_GET["combustivel"]);
		$IN_QTDE = test_input($_GET["qtde"]);
		$IN_FULGOR = test_input($_GET["fulgor"]);
		$IN_INFLAMABILIDADE = test_input($_GET["inflamabilidade"]);
		$IN_EXPLOSIVIDADE = test_input($_GET["explosividade"]);
		$IN_ITEM_4Q1 = test_input($_GET["item_4q1"]);
		$IN_ITEM_1Q3 = test_input($_GET["item_1q3"]);
		$IN_ITEM_2Q3 = test_input($_GET["item_2q3"]);
		$IN_ITEM_3Q3 = test_input($_GET["item_3q3"]);
		$IN_EPI = test_input($_GET["epi"]);
		$IN_EPC = test_input($_GET["epc"]);
		$IN_ENQ_NR16 = test_input($_GET["enq_nr16"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_GET["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_GET["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_GET["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_GET["registro_rt"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_IDCLIENTE = test_input($_POST["idcliente"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_PLANILHA_NUM = test_input($_POST["planilha_num"]);
		$IN_UNIDADE_SITE = test_input($_POST["unidade_site"]);
		$IN_DATA_ELABORACAO = test_input($_POST["data_elaboracao"]);
		$IN_AREA = test_input($_POST["area"]);
		$IN_SETOR = test_input($_POST["setor"]);
		$IN_GES = test_input($_POST["ges"]);
		$IN_CARGO_FUNCAO = test_input($_POST["cargo_funcao"]);
		$IN_CBO = test_input($_POST["cbo"]);
		$IN_ATIV_MACRO = test_input($_POST["ativ_macro"]);
		$IN_TAREFA_EXEC = test_input($_POST["tarefa_exec"]);
		$IN_JOR_TRAB = test_input($_POST["jor_trab"]);
		$IN_TEMPO_EXPO = test_input($_POST["tempo_expo"]);
		$IN_TIPO_EXPO = test_input($_POST["tipo_expo"]);
		$IN_MEIO_PROPAG = test_input($_POST["meio_propag"]);
		$IN_FONTE_GERADORA = test_input($_POST["fonte_geradora"]);
		$IN_ATIVIDADE = test_input($_POST["atividade"]);
		$IN_COMBUSTIVEL = test_input($_POST["combustivel"]);
		$IN_QTDE = test_input($_POST["qtde"]);
		$IN_FULGOR = test_input($_POST["fulgor"]);
		$IN_INFLAMABILIDADE = test_input($_POST["inflamabilidade"]);
		$IN_EXPLOSIVIDADE = test_input($_POST["explosividade"]);
		$IN_ITEM_4Q1 = test_input($_POST["item_4q1"]);
		$IN_ITEM_1Q3 = test_input($_POST["item_1q3"]);
		$IN_ITEM_2Q3 = test_input($_POST["item_2q3"]);
		$IN_ITEM_3Q3 = test_input($_POST["item_3q3"]);
		$IN_EPI = test_input($_POST["epi"]);
		$IN_EPC = test_input($_POST["epc"]);
		$IN_ENQ_NR16 = test_input($_POST["enq_nr16"]);
		$IN_RESP_CAMPO_IDCOLABORADOR = test_input($_POST["resp_campo_idcolaborador"]);
		$IN_RESP_TECNICO_IDCOLABORADOR = test_input($_POST["resp_tecnico_idcolaborador"]);
		$IN_REGISTRO_RC = test_input($_POST["registro_rc"]);
		$IN_REGISTRO_RT = test_input($_POST["registro_rt"]);
		
		## case convertion
		mb_strtoupper($IN_REGISTRO_RC,"UTF-8");
		mb_strtoupper($IN_REGISTRO_RT,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_TAREFA_EXEC, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_ATIVIDADE, $IN_COMBUSTIVEL, $IN_QTDE, $IN_FULGOR, $IN_INFLAMABILIDADE, $IN_EXPLOSIVIDADE, $IN_ITEM_4Q1, $IN_ITEM_1Q3, $IN_ITEM_2Q3, $IN_ITEM_3Q3, $IN_EPI, $IN_EPC, $IN_ENQ_NR16, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_TAREFA_EXEC, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_ATIVIDADE, $IN_COMBUSTIVEL, $IN_QTDE, $IN_FULGOR, $IN_INFLAMABILIDADE, $IN_EXPLOSIVIDADE, $IN_ITEM_4Q1, $IN_ITEM_1Q3, $IN_ITEM_2Q3, $IN_ITEM_3Q3, $IN_EPI, $IN_EPC, $IN_ENQ_NR16, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_IDCLIENTE) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_PLANILHA_NUM) || isEmpty($IN_UNIDADE_SITE) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_IDCLIENTE, $IN_ANO, $IN_MES, $IN_PLANILHA_NUM, $IN_UNIDADE_SITE, $IN_DATA_ELABORACAO, $IN_AREA, $IN_SETOR, $IN_GES, $IN_CARGO_FUNCAO, $IN_CBO, $IN_ATIV_MACRO, $IN_TAREFA_EXEC, $IN_JOR_TRAB, $IN_TEMPO_EXPO, $IN_TIPO_EXPO, $IN_MEIO_PROPAG, $IN_FONTE_GERADORA, $IN_ATIVIDADE, $IN_COMBUSTIVEL, $IN_QTDE, $IN_FULGOR, $IN_INFLAMABILIDADE, $IN_EXPLOSIVIDADE, $IN_ITEM_4Q1, $IN_ITEM_1Q3, $IN_ITEM_2Q3, $IN_ITEM_3Q3, $IN_EPI, $IN_EPC, $IN_ENQ_NR16, $IN_RESP_CAMPO_IDCOLABORADOR, $IN_RESP_TECNICO_IDCOLABORADOR, $IN_REGISTRO_RC, $IN_REGISTRO_RT);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_PLANO_CODIGO = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
				
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id, CLNT.`nome_interno` as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, date_format(AA.`data_elaboracao`,?) as data_elaboracao
       FROM `INFLAMAVEL` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
   ORDER BY 1,2,3,4,5"
		)) 
		{
			$stmt->bind_param('ss', $sql_DATA_ELABORACAO, $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_DATA_ELABORACAO);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata select options formats
					if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_INFL_JAN; }
					if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_INFL_FEV; }
					if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_INFL_MAR; }
					if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_INFL_ABR; }
					if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_INFL_MAI; }
					if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_INFL_JUN; }
					if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_INFL_JUL; }
					if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_INFL_AGO; }
					if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_INFL_SET; }
					if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_INFL_OUT; }
					if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_INFL_NOV; }
					if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_INFL_DEZ; }
				
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td>'.$o_IDCLIENTE.'</td>';
					$TBODY_LIST .= '<td>'.$o_ANO.'</td>';
					$TBODY_LIST .= '<td>'.$o_MES_TXT.'</td>';
					$TBODY_LIST .= '<td>'.$o_PLANILHA_NUM.'</td>';
					$TBODY_LIST .= '<td>'.$o_DATA_ELABORACAO.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Carrega limitacoes do plano contratado
			$o_LIMITE_REGS = -1;//(-1) = Sem limitacao de plano
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1){ $IND_EXIBE_ADD_BTN = 1; } else 
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_TAREFA_EXEC, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_ATIVIDADE, $_COMBUSTIVEL, $_QTDE, $_FULGOR, $_INFLAMABILIDADE, $_EXPLOSIVIDADE, $_ITEM_4Q1, $_ITEM_1Q3, $_ITEM_2Q3, $_ITEM_3Q3, $_EPI, $_EPC, $_ENQ_NR16, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id
       FROM `INFLAMAVEL` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
   ORDER BY 1"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_TAREFA_EXEC, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_ATIVIDADE, $_COMBUSTIVEL, $_QTDE, $_FULGOR, $_INFLAMABILIDADE, $_EXPLOSIVIDADE, $_ITEM_4Q1, $_ITEM_1Q3, $_ITEM_2Q3, $_ITEM_3Q3, $_EPI, $_EPC, $_ENQ_NR16, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_filename = "";
		if($_FILES["img_ativ_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE - ".$_FILES["img_ativ_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_TAREFA_EXEC = $mysqli->escape_String($_TAREFA_EXEC);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_ATIVIDADE = $mysqli->escape_String($_ATIVIDADE);
		$sql_COMBUSTIVEL = $mysqli->escape_String($_COMBUSTIVEL);
		$sql_QTDE = $mysqli->escape_String($_QTDE);
		$sql_FULGOR = $mysqli->escape_String($_FULGOR);
		$sql_INFLAMABILIDADE = $mysqli->escape_String($_INFLAMABILIDADE);
		$sql_EXPLOSIVIDADE = $mysqli->escape_String($_EXPLOSIVIDADE);
		$sql_ITEM_4Q1 = $mysqli->escape_String($_ITEM_4Q1);
		$sql_ITEM_1Q3 = $mysqli->escape_String($_ITEM_1Q3);
		$sql_ITEM_2Q3 = $mysqli->escape_String($_ITEM_2Q3);
		$sql_ITEM_3Q3 = $mysqli->escape_String($_ITEM_3Q3);
		$sql_EPI = $mysqli->escape_String($_EPI);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_ENQ_NR16 = $mysqli->escape_String($_ENQ_NR16);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id
       FROM `INFLAMAVEL` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idcliente` = ?
        AND AA.`ano` = ?
        AND AA.`mes` = ?
        AND AA.`planilha_num` = ?
"
		)) 
		{
			$stmt->bind_param('sdddd', $sql_SID_idSYSTEM_CLIENTE, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `INFLAMAVEL` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`tarefa_exec` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`atividade` = ?
		        ,`combustivel` = ?
		        ,`qtde` = ?
		        ,`fulgor` = ?
		        ,`inflamabilidade` = ?
		        ,`explosividade` = ?
		        ,`item_4q1` = ?
		        ,`item_1q3` = ?
		        ,`item_2q3` = ?
		        ,`item_3q3` = ?
		        ,`epi` = ?
		        ,`epc` = ?
		        ,`enq_nr16` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		        ,`img_ativ_filename` = ?
		        ,`logo_filename` = ?
"
		)) 
		{
			$stmt->bind_param('ssddddsssssssssssssssssssssssssssddssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_TAREFA_EXEC, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_ATIVIDADE, $sql_COMBUSTIVEL, $sql_QTDE, $sql_FULGOR, $sql_INFLAMABILIDADE, $sql_EXPLOSIVIDADE, $sql_ITEM_4Q1, $sql_ITEM_1Q3, $sql_ITEM_2Q3, $sql_ITEM_3Q3, $sql_EPI, $sql_EPC, $sql_ENQ_NR16, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_IMG_ATIV_FILENAME_filename, $sql_LOGO_FILENAME_filename);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_IDCLIENTE, $_ANO, $_MES, $_PLANILHA_NUM, $_UNIDADE_SITE, $_DATA_ELABORACAO, $_AREA, $_SETOR, $_GES, $_CARGO_FUNCAO, $_CBO, $_ATIV_MACRO, $_TAREFA_EXEC, $_JOR_TRAB, $_TEMPO_EXPO, $_TIPO_EXPO, $_MEIO_PROPAG, $_FONTE_GERADORA, $_ATIVIDADE, $_COMBUSTIVEL, $_QTDE, $_FULGOR, $_INFLAMABILIDADE, $_EXPLOSIVIDADE, $_ITEM_4Q1, $_ITEM_1Q3, $_ITEM_2Q3, $_ITEM_3Q3, $_EPI, $_EPC, $_ENQ_NR16, $_RESP_CAMPO_IDCOLABORADOR, $_RESP_TECNICO_IDCOLABORADOR, $_REGISTRO_RC, $_REGISTRO_RT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_IMG_ATIV_FILENAME_filename = "";
		if($_FILES["img_ativ_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["img_ativ_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["img_ativ_filename_file"]["size"] < 2000000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["img_ativ_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM ATIVIDADE - ".$_FILES["img_ativ_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_IMG_ATIV_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["img_ativ_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_IMG_ATIV_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM ATIVIDADE - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		$_LOGO_FILENAME_filename = "";
		if($_FILES["logo_filename_file"]["tmp_name"])
		{
			#Extensoes permitidas para a foto
			$allowedExts = array("gif","jpg","jpeg","png");
			$temp        = explode(".", $_FILES["logo_filename_file"]["name"]);
			$extension   = end($temp);
			
			if (
					//($_FILES["file"]["type"] == "image/gif") && 
					//($_FILES["file"]["type"] == "image/jpeg") && 
					//($_FILES["file"]["type"] == "image/png") && 
					($_FILES["logo_filename_file"]["size"] < 80000) &&
					in_array($extension, $allowedExts)
				) 
			{
				if ($_FILES["logo_filename_file"]["error"] > 0) 
				{
					return "0|IMAGEM LOGOMARCA - ".$_FILES["logo_filename_file"]["error"]."|error|";
					exit;
				}
				
				## Gera nome unico para o arquivo
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$_LOGO_FILENAME_filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(ANEXOS_PATH."/" . $_LOGO_FILENAME_filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["logo_filename_file"]["tmp_name"],ANEXOS_PATH."/" . $_LOGO_FILENAME_filename);
				
			}
			else
			{
				return "0|IMAGEM LOGOMARCA - ".TXT_UPLOAD_ARQUIVO_INVALIDO."|alert|";
				exit;
			}
		}

		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_IDCLIENTE = $mysqli->escape_String($_IDCLIENTE);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_PLANILHA_NUM = $mysqli->escape_String($_PLANILHA_NUM);
		$sql_UNIDADE_SITE = $mysqli->escape_String($_UNIDADE_SITE);
		$sql_DATA_ELABORACAO = $mysqli->escape_String($_DATA_ELABORACAO);
		$sql_AREA = $mysqli->escape_String($_AREA);
		$sql_SETOR = $mysqli->escape_String($_SETOR);
		$sql_GES = $mysqli->escape_String($_GES);
		$sql_CARGO_FUNCAO = $mysqli->escape_String($_CARGO_FUNCAO);
		$sql_CBO = $mysqli->escape_String($_CBO);
		$sql_ATIV_MACRO = $mysqli->escape_String($_ATIV_MACRO);
		$sql_TAREFA_EXEC = $mysqli->escape_String($_TAREFA_EXEC);
		$sql_JOR_TRAB = $mysqli->escape_String($_JOR_TRAB);
		$sql_TEMPO_EXPO = $mysqli->escape_String($_TEMPO_EXPO);
		$sql_TIPO_EXPO = $mysqli->escape_String($_TIPO_EXPO);
		$sql_MEIO_PROPAG = $mysqli->escape_String($_MEIO_PROPAG);
		$sql_FONTE_GERADORA = $mysqli->escape_String($_FONTE_GERADORA);
		$sql_ATIVIDADE = $mysqli->escape_String($_ATIVIDADE);
		$sql_COMBUSTIVEL = $mysqli->escape_String($_COMBUSTIVEL);
		$sql_QTDE = $mysqli->escape_String($_QTDE);
		$sql_FULGOR = $mysqli->escape_String($_FULGOR);
		$sql_INFLAMABILIDADE = $mysqli->escape_String($_INFLAMABILIDADE);
		$sql_EXPLOSIVIDADE = $mysqli->escape_String($_EXPLOSIVIDADE);
		$sql_ITEM_4Q1 = $mysqli->escape_String($_ITEM_4Q1);
		$sql_ITEM_1Q3 = $mysqli->escape_String($_ITEM_1Q3);
		$sql_ITEM_2Q3 = $mysqli->escape_String($_ITEM_2Q3);
		$sql_ITEM_3Q3 = $mysqli->escape_String($_ITEM_3Q3);
		$sql_EPI = $mysqli->escape_String($_EPI);
		$sql_EPC = $mysqli->escape_String($_EPC);
		$sql_ENQ_NR16 = $mysqli->escape_String($_ENQ_NR16);
		$sql_RESP_CAMPO_IDCOLABORADOR = $mysqli->escape_String($_RESP_CAMPO_IDCOLABORADOR);
		$sql_RESP_TECNICO_IDCOLABORADOR = $mysqli->escape_String($_RESP_TECNICO_IDCOLABORADOR);
		$sql_REGISTRO_RC = $mysqli->escape_String($_REGISTRO_RC);
		$sql_REGISTRO_RT = $mysqli->escape_String($_REGISTRO_RT);
		$sql_IMG_ATIV_FILENAME_filename = $mysqli->escape_String($_IMG_ATIV_FILENAME_filename);
		$sql_LOGO_FILENAME_filename = $mysqli->escape_String($_LOGO_FILENAME_filename);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id, AA.`img_ativ_filename`, AA.`logo_filename`
       FROM `INFLAMAVEL` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idINFLAMAVEL` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idINFLAMAVEL, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `INFLAMAVEL` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `idcliente` = ?
		        ,`ano` = ?
		        ,`mes` = ?
		        ,`planilha_num` = ?
		        ,`unidade_site` = ?
		        ,`data_elaboracao` = ?
		        ,`area` = ?
		        ,`setor` = ?
		        ,`ges` = ?
		        ,`cargo_funcao` = ?
		        ,`cbo` = ?
		        ,`ativ_macro` = ?
		        ,`tarefa_exec` = ?
		        ,`jor_trab` = ?
		        ,`tempo_expo` = ?
		        ,`tipo_expo` = ?
		        ,`meio_propag` = ?
		        ,`fonte_geradora` = ?
		        ,`atividade` = ?
		        ,`combustivel` = ?
		        ,`qtde` = ?
		        ,`fulgor` = ?
		        ,`inflamabilidade` = ?
		        ,`explosividade` = ?
		        ,`item_4q1` = ?
		        ,`item_1q3` = ?
		        ,`item_2q3` = ?
		        ,`item_3q3` = ?
		        ,`epi` = ?
		        ,`epc` = ?
		        ,`enq_nr16` = ?
		        ,`resp_campo_idcolaborador` = ?
		        ,`resp_tecnico_idcolaborador` = ?
		        ,`registro_rc` = ?
		        ,`registro_rt` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idINFLAMAVEL` = ?"
		)) 
		{
			$stmt->bind_param('sddddsssssssssssssssssssssssssssddssss', $sql_SID_USERNAME, $sql_IDCLIENTE, $sql_ANO, $sql_MES, $sql_PLANILHA_NUM, $sql_UNIDADE_SITE, $sql_DATA_ELABORACAO, $sql_AREA, $sql_SETOR, $sql_GES, $sql_CARGO_FUNCAO, $sql_CBO, $sql_ATIV_MACRO, $sql_TAREFA_EXEC, $sql_JOR_TRAB, $sql_TEMPO_EXPO, $sql_TIPO_EXPO, $sql_MEIO_PROPAG, $sql_FONTE_GERADORA, $sql_ATIVIDADE, $sql_COMBUSTIVEL, $sql_QTDE, $sql_FULGOR, $sql_INFLAMABILIDADE, $sql_EXPLOSIVIDADE, $sql_ITEM_4Q1, $sql_ITEM_1Q3, $sql_ITEM_2Q3, $sql_ITEM_3Q3, $sql_EPI, $sql_EPC, $sql_ENQ_NR16, $sql_RESP_CAMPO_IDCOLABORADOR, $sql_RESP_TECNICO_IDCOLABORADOR, $sql_REGISTRO_RC, $sql_REGISTRO_RT, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza IMAGEM ATIVIDADE
				if($_IMG_ATIV_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `INFLAMAVEL` SET 
					        `img_ativ_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idINFLAMAVEL` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_IMG_ATIV_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_IMG_ATIV_FILENAME); }
						}
						else
						{
							return "0|IMAGEM ATIVIDADE - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				# Atualiza IMAGEM LOGOMARCA
				if($_LOGO_FILENAME_filename)
				{
					if ($stmt2 = $mysqli->prepare(
					"UPDATE `INFLAMAVEL` SET 
					        `logo_filename` = ?
					  WHERE `idSYSTEM_CLIENTE` = ?
					    AND `idINFLAMAVEL` = ?"
					)) 
					{
						$stmt2->bind_param('sdd', $sql_LOGO_FILENAME_filename, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
						
						if($stmt2->execute())
						{
							# Apaga arquivo anterior
							if (file_exists(ANEXOS_PATH."/" . $o_LOGO_FILENAME)) { unlink (ANEXOS_PATH."/" . $o_LOGO_FILENAME); }
						}
						else
						{
							return "0|IMAGEM LOGOMARCA - ".TXT_MYSQLI_UPDATE_ERRO."|error|";
							exit;
						}
						
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
				}

				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `INFLAMAVEL` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idINFLAMAVEL` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_DATA_ELABORACAO = '%d/%m/%Y';
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id, AA.`idcliente` as idcliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`tarefa_exec` as tarefa_exec, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`atividade` as atividade, AA.`combustivel` as combustivel, AA.`qtde` as qtde, AA.`fulgor` as fulgor, AA.`inflamabilidade` as inflamabilidade, AA.`explosividade` as explosividade, AA.`item_4q1` as item_4q1, AA.`item_1q3` as item_1q3, AA.`item_2q3` as item_2q3, AA.`item_3q3` as item_3q3, AA.`epi` as epi, AA.`epc` as epc, AA.`enq_nr16` as enq_nr16, AA.`resp_campo_idcolaborador` as resp_campo_idcolaborador, AA.`resp_tecnico_idcolaborador` as resp_tecnico_idcolaborador, upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename`) as img_ativ_filename, lower(AA.`logo_filename`) as logo_filename
       FROM `INFLAMAVEL` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idINFLAMAVEL` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('sss', $sql_DATA_ELABORACAO, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_TAREFA_EXEC, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_ATIVIDADE, $o_COMBUSTIVEL, $o_QTDE, $o_FULGOR, $o_INFLAMABILIDADE, $o_EXPLOSIVIDADE, $o_ITEM_4Q1, $o_ITEM_1Q3, $o_ITEM_2Q3, $o_ITEM_3Q3, $o_EPI, $o_EPC, $o_ENQ_NR16, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
			$stmt->fetch();
			
			// Formata Unescape de Textareas
			$o_TAREFA_EXEC = unescape_string($o_TAREFA_EXEC);
			
			// Formata Datas Nulas
			if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_IDCLIENTE."|".$o_ANO."|".$o_MES."|".$o_PLANILHA_NUM."|".$o_UNIDADE_SITE."|".$o_DATA_ELABORACAO."|".$o_AREA."|".$o_SETOR."|".$o_GES."|".$o_CARGO_FUNCAO."|".$o_CBO."|".$o_ATIV_MACRO."|".$o_TAREFA_EXEC."|".$o_JOR_TRAB."|".$o_TEMPO_EXPO."|".$o_TIPO_EXPO."|".$o_MEIO_PROPAG."|".$o_FONTE_GERADORA."|".$o_ATIVIDADE."|".$o_COMBUSTIVEL."|".$o_QTDE."|".$o_FULGOR."|".$o_INFLAMABILIDADE."|".$o_EXPLOSIVIDADE."|".$o_ITEM_4Q1."|".$o_ITEM_1Q3."|".$o_ITEM_2Q3."|".$o_ITEM_3Q3."|".$o_EPI."|".$o_EPC."|".$o_ENQ_NR16."|".$o_RESP_CAMPO_IDCOLABORADOR."|".$o_RESP_TECNICO_IDCOLABORADOR."|".$o_REGISTRO_RC."|".$o_REGISTRO_RT."|".$o_IMG_ATIV_FILENAME."|".$o_LOGO_FILENAME."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idINFLAMAVEL` as id, CLNT.`nome_interno` as nome_interno, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, AA.`tarefa_exec` as tarefa_exec, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`atividade` as atividade, AA.`combustivel` as combustivel, AA.`qtde` as qtde, AA.`fulgor` as fulgor, AA.`inflamabilidade` as inflamabilidade, AA.`explosividade` as explosividade, AA.`item_4q1` as item_4q1, AA.`item_1q3` as item_1q3, AA.`item_2q3` as item_2q3, AA.`item_3q3` as item_3q3, AA.`epi` as epi, AA.`epc` as epc, AA.`enq_nr16` as enq_nr16, upper(CLBRDR.`username`) as username, upper(CLBRDR.`username`) as username, upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, lower(AA.`img_ativ_filename`) as img_ativ_filename, lower(AA.`logo_filename`) as logo_filename,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `INFLAMAVEL` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
    LEFT JOIN `COLABORADOR` as CLBRDR
           ON CLBRDR.`idcolaborador` = AA.`resp_campo_idcolaborador`
    LEFT JOIN `COLABORADOR` as CLBRDR
           ON CLBRDR.`idcolaborador` = AA.`resp_tecnico_idcolaborador`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idINFLAMAVEL` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('sssss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_IDCLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_TAREFA_EXEC, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_ATIVIDADE, $o_COMBUSTIVEL, $o_QTDE, $o_FULGOR, $o_INFLAMABILIDADE, $o_EXPLOSIVIDADE, $o_ITEM_4Q1, $o_ITEM_1Q3, $o_ITEM_2Q3, $o_ITEM_3Q3, $o_EPI, $o_EPC, $o_ENQ_NR16, $o_RESP_CAMPO_IDCOLABORADOR, $o_RESP_TECNICO_IDCOLABORADOR, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			$o_TAREFA_EXEC = unescape_string($o_TAREFA_EXEC);
			$o_TAREFA_EXEC = nl2br($o_TAREFA_EXEC);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_INFL_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_INFL_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_INFL_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_INFL_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_INFL_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_INFL_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_INFL_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_INFL_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_INFL_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_INFL_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_INFL_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_INFL_DEZ; }
				
				// Formata Datas Nulas
				if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_CLIENTE.':</b></th><td>'.$o_IDCLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_NUMERO_PLANILHA.':</b></th><td>'.$o_PLANILHA_NUM.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_UNIDADESITE.':</b></th><td>'.$o_UNIDADE_SITE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_DATA_ELABORACAO.':</b></th><td>'.$o_DATA_ELABORACAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_AREA.':</b></th><td>'.$o_AREA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_SETOR.':</b></th><td>'.$o_SETOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_GES.':</b></th><td>'.$o_GES.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_CARGOFUNCAO.':</b></th><td>'.$o_CARGO_FUNCAO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_CBO.':</b></th><td>'.$o_CBO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ATIVIDADE_MACRO.':</b></th><td>'.$o_ATIV_MACRO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_TAREFA_EXECUTADA.':</b></th><td>'.$o_TAREFA_EXEC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_JORNADA_DE_TRABALHO.':</b></th><td>'.$o_JOR_TRAB.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_TEMPO_EXPOSICAO.':</b></th><td>'.$o_TEMPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_TIPO_EXPOSICAO.':</b></th><td>'.$o_TIPO_EXPO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_MEIO_DE_PROPAGACAO.':</b></th><td>'.$o_MEIO_PROPAG.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_FONTE_GERADORA.':</b></th><td>'.$o_FONTE_GERADORA.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ATIVIDADE.':</b></th><td>'.$o_ATIVIDADE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_COMBUSTIVEL.':</b></th><td>'.$o_COMBUSTIVEL.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_QUANTIDADE.':</b></th><td>'.$o_QTDE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_FULGOR.':</b></th><td>'.$o_FULGOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_INFLAMABILIDADE.':</b></th><td>'.$o_INFLAMABILIDADE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_EXPLOSIVIDADE.':</b></th><td>'.$o_EXPLOSIVIDADE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ITEM_4Q1.':</b></th><td>'.$o_ITEM_4Q1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ITEM_1Q3.':</b></th><td>'.$o_ITEM_1Q3.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ITEM_2Q3.':</b></th><td>'.$o_ITEM_2Q3.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ITEM_3Q3.':</b></th><td>'.$o_ITEM_3Q3.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_EPI.':</b></th><td>'.$o_EPI.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_EPC.':</b></th><td>'.$o_EPC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_ENQUADRAMENTO_NR16.':</b></th><td>'.$o_ENQ_NR16.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_RESPONSAVEL_CAMPO.':</b></th><td>'.$o_RESP_CAMPO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_RESPONSAVEL_TECNICO.':</b></th><td>'.$o_RESP_TECNICO_IDCOLABORADOR.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_RESPONSAVEL_CAMPO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RC.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_RESPONSAVEL_TECNICO_REGISTRO.':</b></th><td>'.$o_REGISTRO_RT.'</td></tr>';
				if($o_IMG_ATIV_FILENAME){ $o_IMG_ATIV_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_IMG_ATIV_FILENAME.'" target="_blank">'.$o_IMG_ATIV_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_IMAGEM_ATIVIDADE.':</b></th><td>'.$o_IMG_ATIV_FILENAME_TXT.'</td></tr>';
				if($o_LOGO_FILENAME){ $o_LOGO_FILENAME_TXT = '<a href="//'.ANEXOS_URL.'/'.$o_LOGO_FILENAME.'" target="_blank">'.$o_LOGO_FILENAME.'</a>'; }
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_INFL_IMAGEM_LOGOMARCA.':</b></th><td>'.$o_LOGO_FILENAME_TXT.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
