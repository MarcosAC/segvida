<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: login.php
## Função..........: Efetua a autenticacao de login do usuario:
##                   - Valida dados de login e gera uma chave de sessao;
##                   - Acesso.php/js redireciona usuario para login.php passando a chave
##                   - login.php revalida chave e exibe tela do admin ou do cliente final
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_USERNAME = $IN_PASSWORD = $IN_SID = "";
	
	#rlgomide.com/medeibem/cgi-bin/login.php?l=br&u=rlgomide&p=d92ebc8c29ed7b5672fd49ed20674bee7bdba7ce3f0f564192137846b177be7701976cc11a3e04c310827ff7708d73016fc94ab97766e26d45594f45c9055431
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["l"]);
		$IN_USERNAME   = test_input($_GET["u"]);
		$IN_PASSWORD   = test_input($_GET["p"]);
*/
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_USERNAME   = test_input($_POST["u"]);
		$IN_PASSWORD   = test_input($_POST["p"]);
		
		## Define idioma
		if(isEmpty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !isEmpty($IN_USERNAME) &&
				!isEmpty($IN_PASSWORD) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
			exit;
		}
		
		## Dados necessarios recebidos - inicia processamento
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## uppercase
			mb_strtoupper($IN_USERNAME,"UTF-8");
			
			####
			# 1 - Verifica se o username existe e obtem dados basicos para validacao
			####
			
			/*****
			 * 1.0 - Valida login e define se é admin ou colab;
			 * 1.1 - Se for categoria CLI = Admin, USR = Colab;
			 * 1.2 - Obtem dados do plano do cliente;
			 *****/
			
			##Escapes
			$sql_DDMMYYYY = '%d/%m/%Y';
			$sql_USERNAME = $mysqli->escape_String($IN_USERNAME);
			
			
			## Valida Login Informado
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT U.`idSYSTEM_USER_ACCOUNT` as id, 
              upper(U.`username`) as username, 
              U.`senha` as password, 
              U.`salt` as salt, 
              upper(U.`nome`) as nome, upper(U.`sobrenome`) as sobrenome, lower(U.`email`) as email,
              upper(U.`categoria`) as categ,
              upper(C.`situacao`) as situacao,
              U.`profile_pic` as profile_pic, 
              U.`ind_confirmado` as ind_confirmado,
              lower(U.`idioma`) as idioma,
              U.`token_tmp` as token,
              C.`idCOLABORADOR` as idCOLABORADOR
         FROM `SYSTEM_USER_ACCOUNT` U
    LEFT JOIN `COLABORADOR` C
           ON C.`username` = U.`username`
        WHERE U.`ind_deletado` = 0
          AND upper(U.`username`) = upper(?) 
        LIMIT 1"
			)) 
			{
				$stmt->bind_param('s', $sql_USERNAME);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_ID, $o_USERNAME, $o_PASSWORD, $o_SALT, $o_NOME, $o_SOBRENOME, $o_EMAIL, $o_CATEGORIA, $o_USER_SITUACAO, $o_PROFILE_PIC, $o_IND_USUARIO_CONFIRMADO, $o_IDIOMA, $o_TOKEN_TMP, $o_idCOLABORADOR);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					##Registra tentativa falha de login
					registraTentativaDeLogin($mysqli, $IN_USERNAME);
					
					die("0|".TXT_USERNAME_SENHA_INVALIDOS."|alert|");
					exit;
				}
				if ($o_USER_SITUACAO == 'I') 
				{
					##Registra tentativa falha de login
					registraTentativaDeLogin($mysqli, $IN_USERNAME);
					
					die("0|".TXT_ACESSO_COLABORADOR_INATIVO."|alert|");
					exit;
				}
			}
			else
			{
				die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|");
				exit;
			}
			
			## Obtem idSYSTEM_CLIENTE
			$o_idSYSTEM_CLIENTE = getSystemClienteID($mysqli, $o_CATEGORIA, $o_USERNAME);
			
			## Obtem Dados do Plano do Cliente
			$_tmp = getDadosPlano($mysqli, $o_idSYSTEM_CLIENTE, $IN_LANG);
			
			$tmp = explode('|',$_tmp);
			$o_SYSTEM_CLIENTE_IND_DELETADO    = $tmp[0];
			$o_PLANO_CODIGO                   = $tmp[1];
			$o_PLANO_TITULO                   = $tmp[2];
			$o_PLANO_TIPO                     = $tmp[3];
			$o_PLANO_VALOR                    = $tmp[4];
			$o_PLANO_DATA_ATIVACAO            = $tmp[5];
			$o_PLANO_DATA_EXPIRACAO           = $tmp[6];
			$o_PLANO_DATA_RENOVACAO           = $tmp[7];
			$o_PLANO_DATA_RENOVACAO_COM_PRAZO = $tmp[8];
			$o_PLANO_SITUACAO                 = $tmp[9];
			
			//error_log("\n***\n* ERRO getDadosPlano\n***\n\n_tmp [".$_tmp."]\n\n", 0);
			
			$o_PLANO_TITULO = utf8_decode($o_PLANO_TITULO);
			
			## faz o hash da senha com um salt excusivo.
			$tmp_password = hash('sha512', $IN_PASSWORD . $o_SALT);
			
			####
			# checa brute - Verifica se o número de tentativas de login se esgotou
			####
			if( checa_brute($mysqli, $IN_USERNAME) == true )
			{
				// Avisa ao usuario que a conta está bloqueada
				// Usuario deve redefinir a senha de acesso para liberar a conta novamente
				die("0|".TXT_LOGIN_BLOQUEADO."|alert|");
				exit;
			}
			else
			{
				## Verifica se a senha informada e valida
				if($tmp_password == $o_PASSWORD)
				{
					// A senha está correta!
					
					## Ajusta language caso o idioma nao seja o lang especificado
					if($IN_LANG != $o_IDIOMA)
					{
						$IN_LANG = $o_IDIOMA;
						carrega_idioma($IN_LANG);
					}
					
					## Verifica se a conta do usuario esta confirmada
					## - Se nao estiver confirmada, envia link de confirmacao novamente e notifica o usuario
					if($o_IND_USUARIO_CONFIRMADO == 0)
					{
						## Conta ainda não foi ativada
						## Neste caso, reenviar email de confirmacao para o email
						
						#Formata URL de confirmacao
						switch($o_CATEGORIA)
						{
							case 'CLI':
								$CONF_URL = WEB_ADDRESS."/cgi-bin/conf_registro.php?i=".$IN_LANG."&t=".$o_TOKEN_TMP;
								break;
							case 'USR':
								$CONF_URL = WEB_ADDRESS."/cgi-bin/conf_registro_user.php?i=".$IN_LANG."&t=".$o_TOKEN_TMP;
								break;
							default:
								break;
						}
						
						#Formata tipo do plano
						if($o_PLANO_TIPO == "M"){ $PLANO_TIPO_TEXT = TXT_PLANO_TIPO_M; }
						                   else { $PLANO_TIPO_TEXT = TXT_PLANO_TIPO_A; }
						
						#Formata Valor do Plano
						$MOEDA_LOCALE = "pt_BR";
						if($o_IDIOMA == "en-us"){ $MOEDA_LOCALE = "en_US"; }
						if($o_IDIOMA == "es-es"){ $MOEDA_LOCALE = "es_ES"; }
						$PLANO_VALOR = formata_moeda($MOEDA_LOCALE,$o_PLANO_VALOR);
						
						## Create a new PHPMailer instance
						$mail = new PHPMailer(true);
						
						##Formata array com os dados
						$template_map = array(
							'{{IMG_URL}}'         => IMG_URL,
							'{{NOME}}'            => mb_strtoupper($o_NOME,"UTF-8"),
							'{{SOBRENOME}}'       => mb_strtoupper($o_SOBRENOME,"UTF-8"),
							'{{EMAIL}}'           => mb_strtolower($o_EMAIL,"UTF-8"),
							'{{PLANO_TITULO}}'    => mb_strtoupper($o_PLANO_TITULO,"UTF-8"),
							'{{PLANO_VALOR}}'     => 'R$ '.$PLANO_VALOR." /".$PLANO_TIPO_TEXT,
							'{{CONFIRMACAO_URL}}' => $CONF_URL
						);
						
						##Carrega texto do email via template ja processado
						$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_REGISTRO, $template_map, $IN_LANG);
						
						$isMailSent = 0;
						
						try 
						{
							ini_set("sendmail_from", "");
							$mail->CharSet = 'UTF-8';
							$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
							$mail->AddAddress($o_EMAIL);//Set who the message is to be sent to
							$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
							$mail->Subject = TXT_EMAIL_SUBJECT_REGISTRO;//Set the subject line
							$mail->MsgHTML($body);
							if ($mail->send()) { $isMailSent = 1; }
						} 
						catch (phpmailerException $e){ $MailError = $e->errorMessage(); } 
						catch (Exception $e){ $MailException = $e->getMessage(); }
						
						//send the message, check for errors
						if (!$isMailSent)
						{
							error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_REGISTRO."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
						}
						
						die("0|".TXT_LOGIN_REENVIO_LINK_CONFIRMACAO."|warning|");
						exit;
					}
					
					##inicia sessao
					$init_sid = sec_session_start();
					
					if($init_sid == false)
					{
						## Username e senha invalidos
						die("0|".TXT_INCAPAZ_DE_GERAR_SESSAO."|alert|");
						exit;
					}
					
					## Obtém o string usuário-agente do usuário. 
					$USER_BROWSER = $_SERVER['HTTP_USER_AGENT'];
					
					## Gera login_security da sessao
					$_SESSION['login_security'] = hash('sha512', $tmp_password ."~". $USER_BROWSER);
					
					## proteção XSS conforme imprimimos este valor
					$USER_ID = preg_replace("/[^0-9]+/", "", $o_ID);
					$_SESSION['user_id'] = $USER_ID;
					
					## proteção XSS conforme imprimimos este valor 
					$USER_USERNAME = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $IN_USERNAME);
					$_SESSION['user_username'] = $USER_USERNAME;
					
					$_SESSION['user_idioma']                      = $o_IDIOMA;
					$_SESSION['user_nome']                        = $o_NOME;
					$_SESSION['user_sobrenome']                   = $o_SOBRENOME;
					$_SESSION['user_email']                       = $o_EMAIL;
					$_SESSION['user_idsystem_cliente']            = $o_idSYSTEM_CLIENTE;
					$_SESSION['user_system_cliente_ind_deletado'] = $o_SYSTEM_CLIENTE_IND_DELETADO;
					$_SESSION['user_categoria']                   = $o_CATEGORIA;
					$_SESSION['user_profile_pic']                 = $o_PROFILE_PIC;
					$_SESSION['user_idcolaborador']               = $o_idCOLABORADOR;
					$_SESSION['plano_codigo']                     = $o_PLANO_CODIGO;
					$_SESSION['plano_titulo']                     = $o_PLANO_TITULO;
					$_SESSION['plano_tipo']                       = $o_PLANO_TIPO;
					$_SESSION['plano_data_ativacao']              = $o_PLANO_DATA_ATIVACAO;
					$_SESSION['plano_data_expiracao']             = $o_PLANO_DATA_EXPIRACAO;
					$_SESSION['plano_data_renovacao']             = $o_PLANO_DATA_RENOVACAO;
					$_SESSION['plano_data_renovacao_com_prazo']   = $o_PLANO_DATA_RENOVACAO_COM_PRAZO;
					$_SESSION['plano_situacao']                   = $o_PLANO_SITUACAO;
					
					##Registra atividade no log
					registraSYSTEM_USER_ACCOUNT_LOG($mysqli, $o_USERNAME,"Logou no sistema.");
					
					##Registra data do ultimo acesso
					registraSYSTEM_USER_ACCOUNT_ULT_ACESSO($mysqli, $o_USERNAME);
					
					// Login concluído com sucesso.
					die("1|".TXT_LOGIN_OK."|success|");
					exit;
				}
				else
				{
					##Registra tentativa falha de login
					registraTentativaDeLogin($mysqli, $IN_USERNAME);
					
					##Registra atividade no log
					registraSYSTEM_USER_ACCOUNT_LOG($mysqli,$o_USERNAME,"Tentativa de login - falha.");
					
					// Username e senha invalidos
					die("0|".TXT_USERNAME_SENHA_INVALIDOS."|alert|");
					exit;
				}
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	
	####
	# Verifica se houveram mais de 5 tentativas de login
	####
	function checa_brute($mysqli, $_username)
	{
		// Registra a hora atual 
		$now = time();
		
		// Todas as tentativas de login são contadas dentro do intervalo das últimas 2 horas. 
		$valid_attempts = $now - (2 * 60 * 60);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT `time` 
		   FROM `LOGIN_TENTATIVAS` 
		  WHERE upper(`username`) = upper(?) 
		    AND time > '$valid_attempts'"))
		{
			$stmt->bind_param('s', $_username);
			$stmt->execute();
			$stmt->store_result();
			//$stmt->bind_result($o_PASSWORD);
			$stmt->fetch();
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows > 5) 
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		
		return false;
		
	}
	
	####
	# Obtem id do cliente
	####
	function getSystemClienteID($mysqli, $_CATEGORIA, $_USERNAME)
	{
		switch($_CATEGORIA)
		{
			case 'USR':
				$_QUERY = "SELECT C.`idSYSTEM_CLIENTE` as idSYSTEM_CLIENTE FROM `COLABORADOR` C WHERE upper(C.`username`) = upper(?) LIMIT 1";
				break;
			case 'CLI':
			default:
				$_QUERY = "SELECT C.`idSYSTEM_CLIENTE` as idSYSTEM_CLIENTE FROM `SYSTEM_USER_ACCOUNT` U LEFT JOIN `SYSTEM_CLIENTE` C ON C.`idSYSTEM_USER_ACCOUNT` = U.`idSYSTEM_USER_ACCOUNT` WHERE upper(U.`username`) = upper(?) LIMIT 1";
				break;
		}
		
		$sql_USERNAME = $mysqli->escape_String($_USERNAME);
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			$stmt->bind_param('s', $sql_USERNAME);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idSYSTEM_CLIENTE);
			$stmt->fetch();
			
			return $o_idSYSTEM_CLIENTE;
		}
	}
	
	####
	# Obtem dados do plano contratado pelo cliente
	####
	function getDadosPlano($mysqli, $_idSYSTEM_CLIENTE, $_LANG)
	{
		##Escapes
		$sql_DDMMYYYY = '%d/%m/%Y';
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_idSYSTEM_CLIENTE);
		
		/*
		"   SELECT C.`ind_deletado` as ind_deletado,
               C.`plano_codigo_atual` as plano_codigo,
               CASE WHEN '".$_LANG."' = 'BR' THEN PP.`titulo_br`
                    WHEN '".$_LANG."' = 'US' THEN PP.`titulo_us`
                    WHEN '".$_LANG."' = 'SP' THEN PP.`titulo_sp`
               END AS plano_titulo,
               PP.`tipo` as plano_tipo,
               PP.`valor_cobranca` as plano_valor,
               date_format(C.`data_ativacao`, ?) as data_ativacao,
               date_format(C.`data_expiracao`, ?) as data_expiracao,
               date_format(C.`data_renovacao`, ?) as data_renovacao,
               date_format(DATE_ADD(C.`data_expiracao`, INTERVAL 7 DAY), ?) as data_renovacao_com_prazo,
               case when datediff(C.`data_expiracao`,now()) is null then 'N'
                    when datediff(C.`data_expiracao`,now()) > 0 then 'A'
                    when datediff(DATE_ADD(C.`data_expiracao`, INTERVAL 7 DAY),now()) > 0 then 'E'
                    when datediff(C.`data_expiracao`,now()) < 0 then 'R'
               end as situacao_plano
          FROM `SYSTEM_CLIENTE` C
     LEFT JOIN `SYSTEM_PLANO_PAGTO` PP
            ON PP.`codigo` = C.`plano_codigo_atual`
         WHERE C.`ind_deletado` = 0
           AND C.`idSYSTEM_CLIENTE` = ?
         LIMIT 1"
		*/
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"   SELECT C.`ind_deletado` as ind_deletado,
               C.`plano_codigo_atual` as plano_codigo,
               CASE WHEN '".$_LANG."' = 'BR' THEN PP.`titulo_br`
                    WHEN '".$_LANG."' = 'US' THEN PP.`titulo_us`
                    WHEN '".$_LANG."' = 'SP' THEN PP.`titulo_sp`
               END AS plano_titulo,
               PP.`tipo` as plano_tipo,
               PP.`valor_cobranca` as plano_valor,
               date_format(C.`data_ativacao`, ?) as data_ativacao,
               date_format(C.`data_expiracao`, ?) as data_expiracao,
               date_format(C.`data_renovacao`, ?) as data_renovacao,
               date_format(DATE_ADD(C.`data_expiracao`, INTERVAL 7 DAY), ?) as data_renovacao_com_prazo,
               'A' as situacao_plano
          FROM `SYSTEM_CLIENTE` C
     LEFT JOIN `SYSTEM_PLANO_PAGTO` PP
            ON PP.`codigo` = C.`plano_codigo_atual`
         WHERE C.`ind_deletado` = 0
           AND C.`idSYSTEM_CLIENTE` = ?
         LIMIT 1")) 
		{
			$stmt->bind_param('ssssd', $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_DDMMYYYY, $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_SYSTEM_CLIENTE_IND_DELETADO, $o_PLANO_CODIGO, $o_PLANO_TITULO, $o_PLANO_TIPO, $o_PLANO_VALOR, $o_PLANO_DATA_ATIVACAO, $o_PLANO_DATA_EXPIRACAO, $o_PLANO_DATA_RENOVACAO, $o_PLANO_DATA_RENOVACAO_COM_PRAZO, $o_PLANO_SITUACAO);
			$stmt->fetch();
		}
		
		return $o_SYSTEM_CLIENTE_IND_DELETADO."|".$o_PLANO_CODIGO."|".$o_PLANO_TITULO."|".$o_PLANO_TIPO."|".$o_PLANO_VALOR."|".$o_PLANO_DATA_ATIVACAO."|".$o_PLANO_DATA_EXPIRACAO."|".$o_PLANO_DATA_RENOVACAO."|".$o_PLANO_DATA_RENOVACAO_COM_PRAZO."|".$o_PLANO_SITUACAO;
	}
	
#################################################################################
###########
#####
##
?>
