<?php
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
?>	

<?php 
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Política de Privacidade" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="css/offcanvas.css">
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO ?></a></li>
						<li><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS ?></a></a></li>
						<li><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO ?></a></a></li>
						<li><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">

				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-info-circle" aria-hidden="true"></span> Política de Privacidade</h3>
					</div>
					
					<div class="row">
						<div class="media">
							<div class="media-left media-top">
								&nbsp;
							</div>
							<div class="media-body">
								<p class="text-justify">Esta política descreve as formas como coletamos, armazenamos, usamos e protegemos suas informações pessoais. Você aceita essa política e concorda com tal coleta, armazenamento e uso quando se inscrever ou usar nossos produtos, serviços ou qualquer outro recurso, tecnologia ou funcionalidade que nós oferecemos ao acessar nosso site ou por qualquer outro meio (coletivamente “os Serviços do prodfy.com.br”). Podemos alterar esta política a qualquer momento, divulgando uma versão revisada em nosso site. A versão revisada entrará em vigor assim que disponibilizada no site. Além disso, se a versão revisada incluir uma alteração substancial, avisaremos você com 30 dias de antecedência, divulgando o aviso sobre a alteração na página “Atualizações da política” do nosso site. Depois desse aviso de 30 dias, será considerado que você concorda com todas as emendas feitas a essa política.</p>
								
								<h4 class="media-heading">Importante: Política de Descadastramento (“Opt-out”)</h4>
								<p class="text-justify">O usuário dos nossos serviços pode a qualquer momento deixar de receber comunicações do nosso site. Para tanto basta enviar um e-mail para contato@prodfy.com.br indicando o seu desejo de não mais receber comunicações, ou simplesmente clicar no link ‘remover’ ou ‘unsubscribe’ contido no final de cada e-mail.</p>
								
								<h4 class="media-heading">Como coletamos informações a seu respeito</h4>
								<p class="text-justify">Quando você visita o site do prodfy.com.br ou usa nossos serviços, coletamos o seu endereço IP e as informações padrão de acesso à web como o tipo do seu navegador e as páginas que acessou em nosso site.</p>
								
								<h4 class="media-heading">Se você se inscrever, coletaremos os seguintes tipos de informações:</h4>
								<p class="text-justify">Informações de contato – o seu nome, endereço, telefone, e-mail, CPF e outras informações semelhantes. Informações financeiras – os números da conta bancária e do cartão de crédito que você forneceu quando adquiriu os Serviços do prodfy.com.br. Antes de permitir o uso dos Serviços do prodfy.com.br, poderemos exigir que você forneça informações adicionais que poderemos usar para verificar sua identidade ou endereço ou gerenciar risco, como sua data de nascimento, número de registro nacional, nacionalidade e outras informações de identificação. Também podemos obter informações suas através de terceiros, como centros de crédito e serviços de verificação de identidade.</p>
								<p class="text-justify">Quando estiver usando os Serviços do Prodfy, coletaremos informações sobre suas transações e outras atividades suas em nosso site ou, quando usar os Serviços do Prodfy, podemos coletar informações sobre seu computador ou outro dispositivo de acesso com finalidade de prevenção contra fraude.</p>
								<p class="text-justify">Por fim, podemos coletar informações adicionais de, e sobre, você por outros meios, como de contatos com nossa equipe de suporte ao cliente, de resultados de suas respostas a uma pesquisa, de interações com membros da família corporativa do prodfy.com.br e de outras empresas.</p>
								
								<h4 class="media-heading">Como usamos cookies</h4>
								<p class="text-justify">Quando você acessa nosso site, nós, incluindo as empresas que contratamos para acompanhar como nosso site é usado, podemos colocar pequenos arquivos de dados chamados “cookies” no seu computador.</p>
								<p class="text-justify">Enviamos um “cookie de sessão” para o seu computador quando você entra em sua conta ou usa os Serviços do prodfy.com.br. Esse tipo de cookie nos ajuda a reconhecê-lo se visitar várias páginas em nosso site durante a mesma sessão, para que não precisemos solicitar a sua senha em todas as páginas. Depois que você sair ou fechar o seu navegador, esse cookie irá expirar e deixará de ter efeito.</p>
								<p class="text-justify">Também usamos cookies mais permanentes para outras finalidades, como para exibir o seu usuário em nosso formulário de acesso, para que você não precise digitar novamente os seus dados de acesso sempre que entrar em sua conta.</p>
								<p class="text-justify">Codificamos nossos cookies para que apenas nós possamos interpretar as informações armazenadas neles. Você está livre para recusar nossos cookies caso o seu navegador permita, mas isso pode interferir no uso do nosso site.</p>
								<p class="text-justify">Nós e nossos prestadores de serviço também usamos cookies para personalizar nossos serviços, conteúdo e publicidade, avaliar a eficiência das promoções e promover confiança e segurança.</p>
								<p class="text-justify">Você pode encontrar cookies de terceiros ao usar os Serviços do prodfy.com.br em determinados sites que não estão sob nosso controle (por exemplo, se você visualizar uma página da Web criada por terceiros ou usar um aplicativo desenvolvido por terceiros, um cookie poderá ser colocado por essa página ou aplicativo).</p>
								
								<h4 class="media-heading">Marketing e Remarketing do Google</h4>
								<p class="text-justify">Nós utilizamos o recurso de Remarketing do Google Adwords para veicular anúncios em sites de parceiros do Google. Este recurso permite identificar que você visitou o nosso site e assim o Google pode exibir o nosso anúncio para você em diferentes websites. Diversos fornecedores de terceiros, inclusive o Google, compram espaços de publicidade em sites da Internet. Nós eventualmente contratamos o Google para exibir nossos anúncios nesses espaços. Para identificar a sua visita no nosso site, tanto outros fornecedores de terceiros, quanto o Google, utilizam-se de cookies, de forma similar ao exposto na seção “Como usamos cookies”. Você pode desativar o uso de cookies pelo Google acessando o Gerenciador de preferências de anúncio.</p>
								
								<h4 class="media-heading">Marketing e Remarketing no Facebook</h4>
								<p class="text-justify">Anúncios baseados no uso de sites ou aplicativos fora do Facebook: Uma das formas dos anúncios alcançarem você é quando as empresas ou organizações pedem ao Facebook para mostrar os anúncios para as pessoas que usaram os sites e aplicativos delas fora do Facebook.</p>
								<p class="text-justify">Por exemplo, quando você visita o site de uma empresa que usa cookies para registrar os visitantes, essa empresa pode pedir ao Facebook para mostrar seu anúncio para essa lista de visitantes, e você pode ver esses anúncios tanto dentro quanto fora do Facebook.</p>
								<p class="text-justify">Esse é um tipo de publicidade baseada em interesse e visitas a aplicativos e websites.</p>
								<p class="text-justify">Se você não quer que o Facebook ou outras empresas participantes coletem ou usem informações baseadas em sua atividade em sites, dispositivos ou aplicativos fora do Facebook com a finalidade de exibir os seus anúncios, você pode optar por não compartilhar essas informações por meio da Digital Advertising Alliance nos EUA, da Digital Advertising Alliance of Canada no Canadá ou a European Digital Advertising Alliance na Europa.</p>
								<p class="text-justify">Você também pode optar por não compartilhar essas informações por meio das configurações do seu dispositivo móvel.</p>
								<p class="text-justify">Você só precisa optar por não compartilhar essas informações uma vez. Se você optar por não participar da publicidade baseada em interesse no Facebook do seu celular ou computador, essa escolha será aplicada em todos os lugares em que você usa o Facebook.</p>
								
								<h4 class="media-heading">Como protegemos e armazenamos informações pessoais</h4>
								<p class="text-justify">Ao longo desta política, usamos o termo “informações pessoais” para descrever informações que possam ser associadas a uma determinada pessoa e possam ser usadas para identificar essa pessoa. Nós não consideraremos como informações pessoais as informações que devem permanecer anônimas, para que elas não identifiquem um determinado usuário.</p>
								<p class="text-justify">Armazenamos e processamos suas informações pessoais em nossos computadores e as protegemos sob medidas de proteção físicas, eletrônicas e processuais. Usamos proteções de computador, como firewalls e criptografia de dados, aplicamos controles de acesso físico a nossos prédios e arquivos e autorizamos o acesso a informações pessoais apenas para os funcionários que precisem delas para cumprir suas responsabilidades profissionais.</p>
								
								<h4 class="media-heading">Como usamos as informações pessoais que coletamos</h4>
								<p class="text-justify">Nossa finalidade principal ao coletar informações pessoais é fornecer a você uma experiência segura, tranquila, eficiente e personalizada. Para isso, usamos suas informações pessoais para:</p>
								<p class="text-justify">fornecer os serviços e o suporte ao cliente solicitados; processar transações e enviar avisos sobre as suas transações; solucionar disputas, cobrar taxas e solucionar problemas; impedir atividades potencialmente proibidas ou ilegais e garantir a aplicação do nosso Contrato do Usuário; personalizar, avaliar e melhorar nossos serviços, além do conteúdo e do layout do nosso site; enviar materiais de marketing direcionados, avisos de atualização no serviço e ofertas promocionais com base nas suas preferências de comunicação; comparar informações, para uma maior precisão, e verificá-las com terceiros.</p>
								
								<h4 class="media-heading">Marketing</h4>
								<p class="text-justify">Não vendemos nem alugamos suas informações pessoais para terceiros para fins de marketing sem seu consentimento explícito. Podemos combinar suas informações com as informações que coletamos de outras empresas e usá-las para melhorar e personalizar nossos serviços, conteúdo e publicidade. Se não desejar receber nossas mensagens de marketing nem participar de nossos programas de personalização de anúncios, basta indicar sua preferência mandando-nos um e-mail ou simplesmente clicar no link de descadastramento fornecido em todos os nossos e-mails.</p>
								
								<h4 class="media-heading">Como compartilhamos informações pessoais com outras partes</h4>
								<p class="text-justify">Podemos compartilhar suas informações pessoais com:</p>
								<p class="text-justify">Membros da família corporativa do prodfy.com.br para fornecer conteúdo, produtos e serviços conjuntos (como registro, transações e suporte ao cliente) para ajudar a detectar e impedir atos potencialmente ilegais e violações de nossas políticas, além de cooperar nas decisões quanto a seus produtos, serviços e comunicações. Os membros da nossa família corporativa usarão essas informações para lhe enviar comunicações de marketing apenas se você tiver solicitado seus serviços. Fornecedores dos serviços sob contrato que colaboram em partes de nossas operações comerciais; prevenção contra fraude, atividades de cobrança, marketing, serviços de tecnologia, etc. Nossos contratos determinam que esses fornecedores de serviço só usem suas informações em relação aos serviços que realizam para nós, e não em benefício próprio. Empresas com as quais pretendemos nos fundir ou adquirir (Se ocorrer uma fusão, exigiremos que a nova entidade constituída siga essa política de privacidade com relação às suas informações pessoais. Se suas informações pessoais puderem ser usadas contra essa política, você receberá um aviso prévio). Autoridades policiais, oficiais do governo ou outros terceiros quando: formos obrigados a isso por intimação, decisão judicial ou procedimento legal semelhante, precisamos fazer isso para estar em conformidade com a lei ou com as regras de associação de cartão de crédito ou estivermos cooperando com uma investigação policial em andamento. Acreditamos, de boa fé, que a divulgação das informações pessoais é necessária para impedir danos físicos ou perdas financeiras, para reportar atividade ilegal suspeita ou investigar violações do nosso Contrato do Usuário. Outros terceiros com seu consentimento ou orientação para tanto. Note que esses terceiros podem estar em outros países nos quais a legislação sobre o processamento de informações pessoais seja menos rígida do que a do seu país.</p>
								<p class="text-justify">Nos casos em que nossa empresa e um parceiro promovem juntos nossos Serviços, nós podemos divulgar ao parceiro algumas informações pessoais tais como nome, endereço e nome de usuário das pessoas que se inscreveram nos nossos Serviços como resultado da promoção conjunta com o único propósito de permitir a nós e ao parceiro avaliar os resultados da promoção. Nesse caso, informações pessoais não podem ser usadas pelo parceiro para nenhum outro propósito</p>
								<p class="text-justify">O prodfy.com.br não venderá ou alugará nenhuma das suas informações pessoais para terceiros no curso normal dos negócios e só compartilha suas informações pessoais com terceiros, conforme descrito nesta política.</p>
								<p class="text-justify">Se você se inscrever no prodfy.com.br diretamente em um site de terceiros ou por meio de um aplicativo de terceiros, qualquer informação inserida naquele site ou aplicativo (e não diretamente em um site do prodfy.com.br) será compartilhada com o proprietário desse site ou aplicativo – e suas informações podem estar sujeitas as políticas de privacidade deles.</p>
								
							</div>
						</div>
						<br/>
						
					</div><!--/row-->

				</div><!--/.col-xs-12.col-sm-9-->

			</div><!--/row-->
		</div><!--/container-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>