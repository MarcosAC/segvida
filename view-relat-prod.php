<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-relat-prod.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 450px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	
	/*##################
	  # confirm-dialog #
	  ################## */
	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
	
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	.input-group-addon {
	     min-width:150px;
	         width:150px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Ficha
	.ficha-table {
		with:100%;
	}
	
	.ficha-table th {
		width: 100px;
		text-align: right;
		vertical-align: top;
		padding: 2px;
		color:black;
		white-space: nowrap;
	}
	.ficha-table td {
		width: *;
		text-align: left;
		vertical-align: top;
		padding: 2px;
		color:#0000ff;
	}
	.datepicker {
			z-index:1051 !important;
		}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		//global $user_username, $user_prodfy_cliente_id, $user_plano_codigo
		//;
		
		
		####
		# Carrega dados do modulo
		####
		//$link_relat_prod_guia_inventario = '<li><a id="relat_prod_1" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
//		$link_relat_prod_guia_inventario = '<li><a href="javascript:$(this).trigger(\'f_show_relat\',[\'1\']);" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
		
		$link_relat_prod_guia_inventario = '<li><a id="lnk_relat1" role="button" style="color:blue;">'.TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO.'</a></li>';
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_RELAT_PROD_RELATORIOS; ?></a></li>
								<li class="active"><span><?php echo TXT_RELAT_PROD_PRODUCAO; ?></span></li>
							</ol></h2>
						<ul class="nav navbar-right panel_toolbox">
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<ul>
							<?php echo $link_relat_prod_guia_inventario; ?>
						</ul>
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->

<div id="tpl_relat">
</div>

<table id="tpl_relat2" class="table hidden">
		<thead>
			<tr>
				<th>Lote</th>
				<th>Objetivo</th>
				<th>Muda</th>
				<th>Data</th>
				<th>Hora Início</th>
				<th>Hora Fim</th>
				<th>Qtde</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>L001</td>
				<td><font size="-1">TESTE CLONAL</font></td>
				<td>CLR400</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L002</td>
				<td>TESTE CLONAL</td>
				<td>CLR401</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L003</td>
				<td>TESTE CLONAL</td>
				<td>CLR403</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L004</td>
				<td>TESTE CLONAL</td>
				<td>CLR404</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L005</td>
				<td>TESTE CLONAL</td>
				<td>CLR405</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L006</td>
				<td>TESTE CLONAL</td>
				<td>CLR406</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L007</td>
				<td>TESTE CLONAL</td>
				<td>CLR407</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L008</td>
				<td>TESTE CLONAL</td>
				<td>CLR408</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L009</td>
				<td>TESTE CLONAL</td>
				<td>CLR409</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L010</td>
				<td>TESTE CLONAL</td>
				<td>CLR410</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L011</td>
				<td>TESTE CLONAL</td>
				<td>CLR411</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
			<tr>
				<td>L012</td>
				<td>TESTE CLONAL</td>
				<td>CLR412</td>
				<td>___/___/______</td>
				<td>___:___:___</td>
				<td>___:___:___</td>
				<td>___________</td>
			</tr>
		</tbody>
	</table>

<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<!-- iCheck -->
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/jspdf.min.js"></script>
		<script src="js/jspdf.plugin.autotable.prodfy.js"></script>
		<script src="js/view-relat-prod.js" charset="UTF-8"></script>
		

<?php
	} 
?>
