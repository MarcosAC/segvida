<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-cfg-colabs.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	/*****
	 * - Mostra grid com os registros de colaboradores inseridos no sistema;
	 * - Botao de inclusao mostra modal com o formulário de cadastro;
	 * - Só permite incluir quantidade de colaboradores do plano contratado;
	 * - Ao mudar de plano, se o plano atual tiver quantidade menor de colaboradores, desativa todos para que o usuario libere aqueles da quantidade limite;
	 * - Para incluir registros além daquels já existentes, o usuário deverá deletar algum registro para incluir um novo;
	 *****/
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<!-- Datatables -->
	<link rel="stylesheet" href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css">
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 500px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	








	/*##################
	  # confirm-dialog #
	  ################## */


	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}


	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
				
				
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	
	.input-group-addon {
	     min-width:200px;
	         width:200px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Ficha
	.ficha-table {
		with:100%;
	}
	
	.ficha-table th {
		width: 100px;
		text-align: right;
		vertical-align: top;
		padding: 2px;
		color:black;
	}
	.ficha-table td {
		width: *;
		text-align: left;
		vertical-align: top;
		padding: 2px;
		color:#0000ff;
	}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		####
		# Carrega dados do modulo
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## Carrega limitacoes do plano contratado
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT P.`total_colaboradores` as total
         FROM `SYSTEM_PLANO_PAGTO` P
        WHERE P.`codigo` = ?"
			)) 
			{
				$sql_PLANO   = $mysqli->escape_String($SID_CTRL->getPLANO_CODIGO());
				//
				$stmt->bind_param('s', $sql_PLANO);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_LIMITE_REGS);
				
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$o_LIMITE_REGS = 0;
				}
				
			}
			
			## Carrega lista de registros
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT CB.`idCOLABORADOR` as id,
            CB.`username` as username, 
            concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome, 
            date_format(CB.`cad_date`,?) as cad_date,
            date_format(U.`dth_ult_acesso`,?) as ult_acesso,
            CB.`situacao` as sit
       FROM `COLABORADOR` CB
 INNER JOIN `SYSTEM_USER_ACCOUNT` U
         ON U.`username` = CB.`username`
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = CB.`idSYSTEM_CLIENTE`
      WHERE CB.`idSYSTEM_CLIENTE` = ?
   ORDER BY CB.`username`"
			)) 
			{
				$sql_DDMMYYYY           = '%d/%m/%Y';
				$sql_DDMMYYYYHHMMSS     = '%d/%m/%Y %T';
				//$sql_SID_USERNAME       = $mysqli->escape_String($user_username);
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('sss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_ID, $o_USERNAME, $o_NOME, $o_CAD_DATE, $o_ULT_ACESSO, $o_SIT);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$TBODY_LIST = "";
				}
				else
				{
					$TBODY_LIST = ""; $C=0;
					while($stmt->fetch())
					{
						# Formata situacao
						if($o_SIT == 'A'){ $SIT_TXT = '<div class="grid_color_green">Ativo</div>'; }
						            else { $SIT_TXT = '<div class="grid_color_red">Inativo</div>'; }
						//
						$TBODY_LIST .= '<tr>';
						$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
						
						# Verifica Nivel de Acesso
						if($ACESSO_MODULO->editar == 1)
						{
							$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
						}
						else
						{
							$TBODY_LIST .= '<a role="button" style="color: #c8c8c8; cursor: not-allowed;"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
						}
						
						$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
						$TBODY_LIST .= '<td>'.$o_USERNAME.'</td>';
						$TBODY_LIST .= '<td>'.$o_NOME.'</td>';
						$TBODY_LIST .= '<td>'.$o_CAD_DATE.'</td>';
						$TBODY_LIST .= '<td>'.$o_ULT_ACESSO.'</td>';
						$TBODY_LIST .= '<td>'.$SIT_TXT.'</td>';
						$TBODY_LIST .= '</tr>';
						//
						$C++;
					}
					
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			//$o_LIMITE_REGS = 5;
			
			## Define se pode inserir mais dados
			if($o_LIMITE_REGS == -1) { $IND_EXIBE_ADD_BTN = 1; } else
			{
				if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
				if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				//$add_btn_link = '<li><a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#cad-modal"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a></li>';
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO_COLABS.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			## Verifica Nivel de Acesso
			if($ACESSO_MODULO->editar == 0)
			{
				$add_btn_link = '<a href="#" role="button" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-lg fa-plus btn_color_disabled"></i></a>';
			}
			if($ACESSO_MODULO->apagar == 1)
			{
				$del_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_del\');" role="button" aria-expanded="false"><i class="fa fa-lg fa-trash btn_color_blue2"></i></a>';
			}
			else
			{
				$del_btn_link = '<a href="#" role="button" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-lg fa-trash btn_color_disabled"></i></a>';
			}
			
			
			## Carrega Relacao de Modulos para Nivel de Acesso
			if ($stmt = $mysqli->prepare(
"SELECT `modulo_codigo`, 
       `modulo_titulo_pt_br`,
       `modulo_titulo_en_us`,
       `modulo_titulo_es_es`,
       `exibir_ind_acessar`,
       `exibir_ind_editar`,
       `exibir_ind_apagar`
  FROM `SYSTEM_NIVEL_ACESSO` 
 WHERE `modulo_liberado`=1 
 ORDER BY `ordem`"
			)) 
			{
				//$stmt->bind_param('sss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_MOD_COD, $o_MOD_TIT_PTBR, $o_MOD_TIT_ENUS, $o_MOD_TIT_ESES, $o_EXIBIR_IND_ACESSAR, $o_EXIBIR_IND_EDITAR, $o_EXIBIR_IND_APAGAR);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$ACESSO_LIST = "";
				}
				else
				{
					$ACESSO_LIST = ""; $C=0;
					while($stmt->fetch())
					{
						# Define idioma do titulo
						switch($SID_CTRL->getIDIOMA())
						{
							case 'pt-br':
							case 'PT-BR':
								$o_MOD_TIT = $o_MOD_TIT_PTBR;
								break;
							case 'en-us':
							case 'EN-US':
								$o_MOD_TIT = $o_MOD_TIT_ENUS;
								break;
							case 'es-es':
							case 'ES-ES':
								$o_MOD_TIT = $o_MOD_TIT_ESES;
								break;
							default:
								break;
						}
						# Formata Titulo
						$o_TMP = explode("/",$o_MOD_TIT);
						$o_MOD_TIT_TXT = $o_TMP[0]."/<br/>".$o_TMP[1];
						//$o_MOD_TIT_TXT = $o_MOD_TIT;
						
						//
						$ACESSO_LIST .= '<div class="input-group" style="background-color:#fff;">';
						$ACESSO_LIST .= '	<span class="input-group-addon">';
						$ACESSO_LIST .= '		<small><b>'.$o_MOD_TIT_TXT.':</b></small>';
						$ACESSO_LIST .= '	</span>';
						$ACESSO_LIST .= '	<input id="idx_'.$o_MOD_COD.'" type="hidden" value="'.$C.'">';
						$ACESSO_LIST .= '	<input id="reg_acesso_codigo_'.$C.'" type="hidden" value="'.$o_MOD_COD.'">';
						$ACESSO_LIST .= '	&nbsp;&nbsp;<a role="button" onclick="$(\'#reg_acesso_ind_acessar_'.$C.'\').prop(\'checked\', true);$(\'#reg_acesso_ind_editar_'.$C.'\').prop(\'checked\', true);$(\'#reg_acesso_ind_apagar_'.$C.'\').prop(\'checked\', true);"><i class="fa fa-check-circle-o fa-lg"></i></a>&nbsp;&nbsp;';
						$ACESSO_LIST .= '	<a role="button" onclick="$(\'#reg_acesso_ind_acessar_'.$C.'\').prop(\'checked\', false);$(\'#reg_acesso_ind_editar_'.$C.'\').prop(\'checked\', false);$(\'#reg_acesso_ind_apagar_'.$C.'\').prop(\'checked\', false);"><i class="fa fa-circle-o fa-lg"></i></a>&nbsp;&nbsp;';
						
						if($o_EXIBIR_IND_ACESSAR == 1)
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_acessar_tipo_'.$C.'" type="hidden" value="1"><input id="reg_acesso_ind_acessar_'.$C.'" type="checkbox" class="acessar_chk"> '.TXT_NIVEL_ACESSO_ACESSAR.'&nbsp;&nbsp;'; } else
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_acessar_tipo_'.$C.'" type="hidden" value="0">'; }
						if($o_EXIBIR_IND_EDITAR == 1)
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_acessar_tipo_'.$C.'" type="hidden" value="1"><input id="reg_acesso_ind_editar_'.$C.'" type="checkbox"  class="editar_chk"> '.TXT_NIVEL_ACESSO_EDITAR.'&nbsp;&nbsp;'; } else
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_editar_tipo_'.$C.'" type="hidden" value="0">'; }
						if($o_EXIBIR_IND_APAGAR == 1)
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_acessar_tipo_'.$C.'" type="hidden" value="1"><input id="reg_acesso_ind_apagar_'.$C.'" type="checkbox"  class="apagar_chk"> '.TXT_NIVEL_ACESSO_APAGAR.'&nbsp;&nbsp;'; } else
						{ $ACESSO_LIST .= '	<input id="reg_acesso_ind_apagar_tipo_'.$C.'" type="hidden" value="0">'; }
						
						$ACESSO_LIST .= '</div>';
						//
						$C++;
					}
					
				}
				
			}
			
			
		}# /carrega dados #
		
		#<li id="search_button_link"><a href="#" onclick="javascript:$(this).trigger('f_show_advsearch');" role="button" aria-expanded="false"><i class="fa fa-lg fa-search btn_color_blue2"></i></a></li>
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_CFG_COLABS_CONFIGURACAO; ?></a></li>
								<li class="active"><span><?php echo TXT_CFG_COLABS_COLABORADORES; ?></span></li>
							</ol></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li id="add_button_link"><?php echo $add_btn_link; ?></li>
							<li id="del_button_link"><?php echo $del_btn_link; ?></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content"><input id='datatable_total_recs' value="<?php echo $o_TOTAL_REGS; ?>" type=hidden>
						<table id="main-datatable" class="table table-striped table-bordered bulk_action" style="max-width: 800px">
							<thead>
								<tr>
									<th class="text-center"><input type="checkbox" id="select-all" name="select_all" class="dt_checkbox"></th>
									<th class="text-center"><i class="fa fa-chevron-down"></i></th>
									<th><?php echo TXT_CFG_COLABS_FICHA_USUARIO; ?></th>
									<th><?php echo TXT_CFG_COLABS_FICHA_NOME; ?></th>
									<th><?php echo TXT_CFG_COLABS_FICHA_INCLUSAO; ?></th>
									<th><?php echo TXT_CFG_COLABS_FICHA_ULT_ACESSO; ?></th>
									<th><?php echo TXT_CFG_COLABS_FICHA_SITUACAO; ?></th>
								</tr>
							</thead>
							
							<tbody id="datatable_tbody">
								<?php echo $TBODY_LIST; ?>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->









<!-- BEGIN # MODAL CAD -->
<div class="modal fade" id="cad-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<span style="line-height: 10px; font-size: 10px;"><i class="fa fa-thumbs-o-up"></i>&nbsp;<?php echo TXT_TECLE_ESC_PARA_FECHAR; ?></span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
			</div>
			
			<!-- Begin # DIV Form -->
			<div id="div-forms">
				<!-- Begin | Form -->
				<form id="reg-form">
					<input id="reg_id" type="hidden">
					<input id="reg_acesso_count" type="hidden" value="<?php echo $C-1; ?>">
					<div class="modal-body">
						<div id="div-reg-msg">
							<div id="div-label-type"><?php echo TXT_CAD_MODAL_NOVO_REGISTRO; ?></div>
							<div id="icon-reg-msg" class="glyphicon glyphicon-chevron-right"></div>
							<span id="text-reg-msg"><?php echo TXT_CAD_MODAL_CAMPOS_COM_A_SAO_OBRIGATORIOS; ?></span>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_CFG_COLABS_FICHA_USUARIO; ?>:</b>
							</span>
							<input id="reg_username" type="text" maxlength=30 class="form-control logintext input-lower" style="margin-top: 0px; margin-bottom: 0px;" required >
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_CFG_COLABS_FICHA_CPF; ?>:</b>
							</span>
							<input id="reg_cpf" type="text" maxlength=14 class="form-control form-control2 numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_CFG_COLABS_FICHA_SITUACAO; ?>:</b>
							</span>
							<select id="reg_situacao" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
								<option value="" selected><?php echo TXT_CFG_COLABS_SELECIONE; ?></option>
								<option value="A"><?php echo TXT_CFG_COLABS_ATIVO; ?></option>
								<option value="I" ><?php echo TXT_CFG_COLABS_INATIVO; ?></option>
							</select>
						</div>
						<div style="height:30px; vertical-align: middle; text-align: center;">
							<span id="text-reg-msg" style="align:center;"><?php echo TXT_CFG_COLABS_FICHA_NIVEL_ACESSO; ?> <i class="fa fa-arrow-right fa-lg"></i> <input id="acessar_geral" type="checkbox" onclick="javascript:$('.acessar_chk').prop('checked',$('#acessar_geral').is(':checked'));"> <?php echo TXT_CFG_COLABS_ACESSAR; ?> &nbsp;&nbsp;&nbsp;<input id="editar_geral" type="checkbox" onclick="javascript:$('.editar_chk').prop('checked',$('#editar_geral').is(':checked'));"> <?php echo TXT_CFG_COLABS_EDITAR; ?> &nbsp;&nbsp;&nbsp;<input id="apagar_geral" type="checkbox" onclick="javascript:$('.apagar_chk').prop('checked',$('#apagar_geral').is(':checked'));"> <?php echo TXT_CFG_COLABS_APAGAR; ?></span>
						</div>
						<?php echo $ACESSO_LIST; ?>
					</div>
					<div class="modal-footer">
						<div>
							<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_REGISTER; ?></button>
							<button id="bt_save" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SAVE; ?></button>
						</div>
					</div>
				</form>
				<!-- End | Form -->
				
			</div>
			<!-- End # DIV Form -->
			
		</div>
	</div>
</div>
<!-- END # MODAL CAD -->



<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<!-- iCheck -->
		<script src="vendors/iCheck/icheck.min.js"></script>
		<!-- Datatables -->
		<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="vendors/datatables.net/js/dataTables.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/dataTables.buttons.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.colVis.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.flash.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.html5.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.print.min.js"></script>
		
		<script src="vendors/datatables.net/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
		<script src="vendors/datatables.net/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
		<script src="vendors/datatables.net/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/dataTables.responsive.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Scroller/js/dataTables.scroller.min.js"></script>
		<!-- <script src="vendors/datatables.net/extensions/Select/js/dataTables.select.min.js"></script> -->
		
		<script src="vendors/jszip/dist/jszip.min.js"></script>
		<script src="vendors/pdfmake/build/pdfmake.min.js"></script>
		<script src="vendors/pdfmake/build/vfs_fonts.js"></script>
		
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/view-cfg-colabs.js" charset="UTF-8"></script>
		

<?php
	} 
?>
