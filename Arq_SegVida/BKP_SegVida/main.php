<?php
##################################################################################
## Projeto: SEGVIDA
##  Modulo: main.php
##  Funcao: MAIN CONTROLLER - Controle central de solicitacoes e fluxo de execucao
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
##################################################################################
	ob_start();
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])){ $_LANG = valida_idioma($_COOKIE['lang']); } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
	##inicia sessao
	$init_sid = sec_session_start();

	##Conecta ao Banco de Dados
	$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
	if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
	
	
	## Valida se o usuario esta logado
	if(isLogedIn($mysqli) == false)
	{
		header('Location: index.php');
	}
	else
	{
		####
		# Dados Basicos
		####
		
		## Profile Pic
		$user_profile_pic = "no_profile_pic.jpg";
		if( isset($_SESSION['user_profile_pic']) &&
		    file_exists(PROFILE_PICS_PATH.'/'.$_SESSION['user_profile_pic'])
		)
		{
			$user_profile_pic = $_SESSION['user_profile_pic'];
		}
		
		## Categoria do usuário: admin, colaborador, usuário
		switch($_SESSION['user_categoria'])
		{
			case 'CLI':
				$user_categ = TXT_USER_CATEG_ADMIN;
				break;
			case 'USR':
				$user_categ = TXT_USER_CATEG_COLAB;
				break;
			case 'UFN':
				$user_categ = TXT_USER_CATEG_USU;
				break;
			default:
				break;
		}
		
		####
		# Gera Objeto de Sessao
		####
		//$SID_CTRL = new SessionCTRL($_LANG, $_SESSION['user_username'], $_SESSION['user_nome'], $_SESSION['user_sobrenome'], $_SESSION['user_email'], $_SESSION['user_idsystem_cliente'], $_SESSION['user_system_cliente_ind_deletado'], $_SESSION['user_plano_codigo'], $_SESSION['user_pagto_plano_codigo'], $_SESSION['user_pagto_token'], $_SESSION['user_pagto_numero'], $_SESSION['user_categoria'], $user_categ, $_SESSION['user_idcolaborador'], $user_profile_pic, $_SESSION['plano_codigo'], $_SESSION['plano_titulo'], $_SESSION['plano_tipo'], $_SESSION['plano_data_ativacao'], $_SESSION['plano_data_expiracao'], $_SESSION['plano_data_renovacao'], $_SESSION['plano_data_renovacao_com_prazo'], $_SESSION['plano_situacao']);
		$SID_CTRL = new SessionCTRL($_LANG, $_SESSION['user_username'], $_SESSION['user_nome'], $_SESSION['user_sobrenome'], $_SESSION['user_email'], $_SESSION['user_idsystem_cliente'], $_SESSION['user_system_cliente_ind_deletado'], $_SESSION['user_plano_codigo'], $_SESSION['user_pagto_plano_codigo'], $_SESSION['user_pagto_token'], $_SESSION['user_pagto_numero'], $_SESSION['user_categoria'], $user_categ, $_SESSION['user_idcolaborador'], $user_profile_pic, $_SESSION['plano_codigo'], $_SESSION['plano_titulo'], $_SESSION['plano_tipo'], $_SESSION['plano_data_ativacao'], $_SESSION['plano_data_expiracao'], $_SESSION['plano_data_renovacao'], $_SESSION['plano_data_renovacao_com_prazo'], 'A');
		
		
		#$plano_situacao =  'A';
		
		
		####
		# Verifica se há colaboradores configurados
		####
		{
			$user_total_colaboradores = 0;
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT upper(CB.`username`) as username,
              concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome
         FROM `COLABORADOR` CB
   INNER JOIN `SYSTEM_USER_ACCOUNT` U
           ON U.`username` = CB.`username`
        WHERE CB.`idSYSTEM_CLIENTE` = ?"
			)) 
			{
				$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_COLAB_USERNAME, $o_COLAB_NOME);
				
				$o_TOTAL_COLABORADORES=0;
				$COLABS_LIST = '';
				
				while($stmt->fetch())
				{
					$COLABS_LIST .= '<option value="'.$o_COLAB_USERNAME.'" data-subtext="('.$o_COLAB_NOME.')">'.$o_COLAB_USERNAME.'</option>';
					$o_TOTAL_COLABORADORES++;
				}
				
				$user_total_colaboradores = $o_TOTAL_COLABORADORES;
				
			}# /prepara query #
		}
		
		
		####
		# Carrega Matriz de Acesso do Usuário
		####
		{
			# Carrega relacao de modulos
			$sql_USER_CATEG = $mysqli->escape_String($SID_CTRL->getUSER_CATEGORIA());
			$sql_idCOLABORADOR = $mysqli->escape_String($SID_CTRL->getUSER_IDCOLABORADOR());
			
			
			//$sql_USER_CATEG = $mysqli->escape_String($user_categoria);
			//$sql_idCOLABORADOR = $mysqli->escape_String($user_colaborador_id);
			
			$QUERY = "SELECT PNA.`modulo_codigo`,
              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_acessar` is null THEN 0 ELSE CA.`ind_acessar` END
              END as ind_acessar,
              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_editar` is null THEN 0 ELSE CA.`ind_editar` END
              END as ind_editar,
              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_apagar` is null THEN 0 ELSE CA.`ind_apagar` END
              END as ind_apagar
         FROM `SYSTEM_NIVEL_ACESSO` PNA
    LEFT JOIN `COLABORADOR_ACESSO` CA
           ON CA.`modulo_codigo` = PNA.`modulo_codigo`
          AND CA.`idSYSTEM_CLIENTE` = '".$sql_idSYSTEM_CLIENTE."'
          AND CA.`idCOLABORADOR` = '".$sql_idCOLABORADOR."'
        WHERE `modulo_liberado` = 1 
     ORDER BY `ordem`";
			
			//error_log("main.php -> Carrega Matriz de Acesso do Usuário:\n\n".$QUERY."\n\n",0);
			
			if ($stmt = $mysqli->prepare($QUERY)) 
			{
				//$stmt->bind_param('ssssssss', $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_MOD_COD, $o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
				
				$ACESSO_CTRL = array();
				
				while($stmt->fetch())
				{
					# Define nivel de acesso
					if($o_MOD_COD)
					{
						$ACESSO_CTRL[$o_MOD_COD] = new AcessoCTRL($o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
					}
				}
				
			}
		}
		
		
		####
		# Side Menu
		####
		if($SID_CTRL->getPLANO_SITUACAO() == 'R')
		{
			####
			# LINK INATIVO //
			####
			
			##PainelDeControle
			$side_menu_link_painel_controle_dashboard = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_PAINEL_DE_CONTROLE_DASHBOARD);
			##Administrativo
			$side_menu_link_adm_cliente = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_ADM_CLIENTE);
			##Laudos
			/*
Avaliação de Risco
Biológico
Calor
Eletricidade
Explosivos
Inflamáveis
Particulado
Poeira
Radiação
Ruído
Vapores
Vibração VCI
Vibração VMB
			*/
			$side_menu_link_laudos_bio = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_BIO);
			$side_menu_link_laudos_calor = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_CALOR);
			$side_menu_link_laudos_eletr = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_ELETR);
			$side_menu_link_laudos_explo = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_EXPLO);
			$side_menu_link_laudos_infl = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_INFL);
			$side_menu_link_laudos_part = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_PART);
			$side_menu_link_laudos_poeira = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_POEIRA);
			$side_menu_link_laudos_rad = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_RAD);
			$side_menu_link_laudos_ruido = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_RUIDO);
			$side_menu_link_laudos_vapor = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_VAPOR);
			$side_menu_link_laudos_vibr_vci = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_LAUDOS_VIBR_VCI);
			/*
			##Formularios
			$side_menu_link_relat_prod = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_RELAT_PROD);
			$side_menu_link_relat_etiquetas = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_RELAT_ETIQUETAS);
			#$side_menu_link_relat_mudas = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_RELATORIOS_MUDAS);
			#$side_menu_link_relat_clientes = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_RELATORIOS_CLIENTES);
			#$side_menu_link_relat_colabs = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_RELATORIOS_COLABORADORES);
			*/
			##Config
			$side_menu_link_cfg_colabs = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_COLABORADORES);
			//$side_menu_link_cfg_dispositivos = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_DISPOSITIVOS);
			//$side_menu_link_cfg_sincr = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_SINCRONIA);
			$side_menu_link_cfg_dashboard = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_DASHBOARD);
			$side_menu_link_cfg_planilha_modelo = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_PLANILHA_MODELO);
			//$side_menu_link_cfg_plano = getMenuLink(0,"",TXT_SYSTEM_SIDE_MENU_CFG_PLANO_CONTRATADO);
			##Profile
			$profile_menu_link_cad = getMenuLink(0,"",TXT_SYSTEM_PROFILE_MENU_CAD);
			$profile_menu_link_ajuda = getMenuLink(0,"",TXT_SYSTEM_PROFILE_MENU_AJUDA);
			#$profile_menu_link_cfg = getMenuLink(0,"",TXT_SYSTEM_PROFILE_MENU_CFG);
			##Mensagens
			$msg_menu_enabled = 0;
			$msg_menu_botao_status_class = 'disabled';
		}
		else
		{
			####
			# LINK ATIVO //
			####
			
			##PainelDeControle
			$side_menu_link_painel_controle_dashboard = getMenuLink($ACESSO_CTRL["painel-dashboard"]->acessar,"painel-dashboard",TXT_SYSTEM_SIDE_MENU_PAINEL_DE_CONTROLE_DASHBOARD);
			##Administrativo
			$side_menu_link_adm_cliente = getMenuLink($ACESSO_CTRL["adm-cliente"]->acessar,"adm-cliente",TXT_SYSTEM_SIDE_MENU_ADM_CLIENTE);
			##Laudos
			/*
Avaliação de Risco
Biológico
Calor
Eletricidade
Explosivos
Inflamáveis
Particulado
Poeira
Radiação
Ruído
Vapores
Vibração VCI
Vibração VMB
			*/
			$side_menu_link_laudos_bio = getMenuLink($ACESSO_CTRL["laudos-bio"]->acessar,"laudos-bio",TXT_SYSTEM_SIDE_MENU_LAUDOS_BIO);
			$side_menu_link_laudos_calor = getMenuLink($ACESSO_CTRL["laudos-calor"]->acessar,"laudos-calor",TXT_SYSTEM_SIDE_MENU_LAUDOS_CALOR);
			$side_menu_link_laudos_eletr = getMenuLink($ACESSO_CTRL["laudos-eletr"]->acessar,"laudos-eletr",TXT_SYSTEM_SIDE_MENU_LAUDOS_ELETR);
			$side_menu_link_laudos_explo = getMenuLink($ACESSO_CTRL["laudos-explo"]->acessar,"laudos-explo",TXT_SYSTEM_SIDE_MENU_LAUDOS_EXPLO);
			$side_menu_link_laudos_infl = getMenuLink($ACESSO_CTRL["laudos-infl"]->acessar,"laudos-infl",TXT_SYSTEM_SIDE_MENU_LAUDOS_INFL);
			$side_menu_link_laudos_part = getMenuLink($ACESSO_CTRL["laudos-part"]->acessar,"laudos-part",TXT_SYSTEM_SIDE_MENU_LAUDOS_PART);
			$side_menu_link_laudos_poeira = getMenuLink($ACESSO_CTRL["laudos-poeira"]->acessar,"laudos-poeira",TXT_SYSTEM_SIDE_MENU_LAUDOS_POEIRA);
			$side_menu_link_laudos_rad = getMenuLink($ACESSO_CTRL["laudos-rad"]->acessar,"laudos-rad",TXT_SYSTEM_SIDE_MENU_LAUDOS_RAD);
			$side_menu_link_laudos_ruido = getMenuLink($ACESSO_CTRL["laudos-ruido"]->acessar,"laudos-ruido",TXT_SYSTEM_SIDE_MENU_LAUDOS_RUIDO);
			$side_menu_link_laudos_vapor = getMenuLink($ACESSO_CTRL["laudos-vapor"]->acessar,"laudos-vapor",TXT_SYSTEM_SIDE_MENU_LAUDOS_VAPOR);
			$side_menu_link_laudos_vibr_vci = getMenuLink($ACESSO_CTRL["laudos-vibr_vci"]->acessar,"laudos-vibr_vci",TXT_SYSTEM_SIDE_MENU_LAUDOS_VIBR_VCI);
			/*
			##Formularios
			$side_menu_link_relat_prod = getMenuLink($ACESSO_CTRL["relat-prod"]->acessar,"relat-prod",TXT_SYSTEM_SIDE_MENU_RELAT_PROD);
			$side_menu_link_relat_etiquetas = getMenuLink($ACESSO_CTRL["relat-etiquetas"]->acessar,"relat-etiquetas",TXT_SYSTEM_SIDE_MENU_RELAT_ETIQUETAS);
			#$side_menu_link_relat_mudas = getMenuLink($ACESSO_CTRL["relat-mudas"]->acessar,"relat-mudas",TXT_SYSTEM_SIDE_MENU_RELATORIOS_MUDAS);
			#$side_menu_link_relat_clientes = getMenuLink($ACESSO_CTRL["relat-clientes"]->acessar,"relat-clientes",TXT_SYSTEM_SIDE_MENU_RELATORIOS_CLIENTES);
			#$side_menu_link_relat_colabs = getMenuLink($ACESSO_CTRL["relat-colabs"]->acessar,"relat-colabs",TXT_SYSTEM_SIDE_MENU_RELATORIOS_COLABORADORES);
			*/
			##Config
			$side_menu_link_cfg_colabs = getMenuLink($ACESSO_CTRL["cfg-colabs"]->acessar,"cfg-colabs",TXT_SYSTEM_SIDE_MENU_CFG_COLABORADORES);
			//$side_menu_link_cfg_dispositivos = getMenuLink($ACESSO_CTRL["cfg-dispositivos"]->acessar,"cfg-dispositivos",TXT_SYSTEM_SIDE_MENU_CFG_DISPOSITIVOS);
			//$side_menu_link_cfg_sincr = getMenuLink($ACESSO_CTRL["cfg-sincr"]->acessar,"cfg-sincr",TXT_SYSTEM_SIDE_MENU_CFG_SINCRONIA);
			$side_menu_link_cfg_dashboard = getMenuLink($ACESSO_CTRL["cfg-dashboard"]->acessar,"cfg-dashboard",TXT_SYSTEM_SIDE_MENU_CFG_DASHBOARD);
			//$side_menu_link_cfg_boletos = getMenuLink($ACESSO_CTRL["cfg-boletos"]->acessar,"cfg-boletos",TXT_SYSTEM_SIDE_MENU_CFG_BOLETOS);
			$side_menu_link_cfg_planilha_modelo = getMenuLink($ACESSO_CTRL["cfg-planilha_modelo"]->acessar,"cfg-planilha_modelo",TXT_SYSTEM_SIDE_MENU_CFG_PLANILHA_MODELO);
			//$side_menu_link_cfg_plano = getMenuLink($ACESSO_CTRL["cfg-plano"]->acessar,"cfg-plano",TXT_SYSTEM_SIDE_MENU_CFG_PLANO_CONTRATADO);
			##Profile
			$profile_menu_link_cad = getMenuLink(1,"prfl-cad",TXT_SYSTEM_PROFILE_MENU_CAD);
			$profile_menu_link_ajuda = getMenuLink(1,"prfl-ajuda",TXT_SYSTEM_PROFILE_MENU_AJUDA);
			#$profile_menu_link_cfg = getMenuLink(1,"prfl-cfg",TXT_SYSTEM_PROFILE_MENU_CFG);
			#Mensagens
			$msg_menu_enabled = 1;
			$msg_menu_botao_status_class = '';
		}
		
		//$side_menu_link_cfg_plano           = '<li><a href="system.php?pg=cfg-plano">'.TXT_SYSTEM_SIDE_MENU_CFG_PLANO_CONTRATADO.'</a></li>';
		$side_menu_link_cfg_plano = getMenuLink($ACESSO_CTRL["cfg-plano"]->acessar,"cfg-plano",TXT_SYSTEM_SIDE_MENU_CFG_PLANO_CONTRATADO);
		
		
		####
		# Processa mensagens do sistema
		####
		if($msg_menu_enabled == 1)
		{
			## Carrega mensagens do usuario
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT M.`idMENSAGENS` as idMENSAGENS,
              M.`idSYSTEM_CLIENTE` as idSYSTEM_CLIENTE,
              M.`from_username` as from_username,
              U_FRM.`nome` as from_nome,
              U_FRM.`sobrenome` as from_sobrenome,
              U_FRM.`profile_pic` as from_profile_pic,
              M.`to_username` as to_username,
              M.`subject` as subject,
              M.`msg` as msg,
              DATE_FORMAT(M.`date_sent`,?) as date_sent,
              M.`anexo_filename` as anexo_filename,
              M.`ip` as ip,
              (SELECT count(1) 
                 FROM `MENSAGENS` M2
                WHERE upper(M2.`to_username`) = upper(?)
                  AND M2.`idSYSTEM_CLIENTE`  = C.`idSYSTEM_CLIENTE`
              ) as total_msgs
        FROM `MENSAGENS` as M
   LEFT JOIN `SYSTEM_CLIENTE` as C
          ON C.`idSYSTEM_CLIENTE` = M.`idSYSTEM_CLIENTE`
   LEFT JOIN `SYSTEM_USER_ACCOUNT` as U_FRM
          ON U_FRM.`username` = M.`from_username`
       WHERE upper(M.`to_username`) = upper(?)
       ORDER BY M.`date_sent` DESC 
          LIMIT 5"
			)) 
			{
				$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
				$sql_USERNAME       = $mysqli->escape_String($SID_CTRL->getUSER_USERNAME());
				//
				$stmt->bind_param('sss', $sql_DDMMYYYYHHMMSS, $sql_USERNAME, $sql_USERNAME);
				
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_MSG_idMENSAGENS, $o_MSG_idSYSTEM_CLIENTE, 
				$o_MSG_FROM_USERNAME, $o_MSG_FROM_NOME, $o_MSG_FROM_SOBRENOME, $o_MSG_FROM_PROFILE_PIC, 
				$o_MSG_TO_USERNAME, $o_MSG_SUBJECT, $o_MSG_MSG, $o_MSG_DATE_SENT, $o_MSG_ANEXO_FILENAME, 
				$o_MSG_IP, $o_MSGS_TOTAL);
				
				$msg_menu_msgs  = '<ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">';
				
				if( $user_total_colaboradores > 0)
				{
					$msg_menu_msgs .= '<li><div id="new_msg_link" class="text-center"><a role="button" data-toggle="modal" data-target="#enviarmsgs-modal"><span><i class="fa fa-pencil"></i>&nbsp;'.TXT_MOD_MSGS_ENVIAR_MSG.'</span></a></div></li>';
				}
				else
				{
					$msg_menu_msgs .= '<li><div id="new_msg_link" class="text-center"><a id="btn_no_new_msgs" data-text="'.TXT_MOD_MSGS_NENHUM_COLABORADOR_CONFIGURADO.'" role="button"><span><i class="fa fa-pencil"></i>&nbsp;'.TXT_MOD_MSGS_ENVIAR_MSG.'</span></a></div></li>';
				}
				
				
				while($stmt->fetch())
				{
					## Formata as mensagens iniciais
					
					## Formata nome do usuario
					$msg_from_name = $o_MSG_FROM_NOME." ".$o_MSG_FROM_SOBRENOME;
					
					## Formata imagem de perfil
					if($o_MSG_FROM_PROFILE_PIC && 
					   file_exists(PROFILE_PICS_PATH.'/'.$o_MSG_FROM_PROFILE_PIC)
						)  { $msg_profile_pic = $o_MSG_FROM_PROFILE_PIC; }
					else { $msg_profile_pic = 'no_profile_pic.jpg'; }
					
					## Formata data de envio
					$msg_dth_envio = $o_MSG_DATE_SENT;
					
					## Formata assunto
					$msg_assunto = $o_MSG_SUBJECT;
					
					## Formata mensagem
					$msg_menu_msgs .= '<li><a><span class="image"><img src="img/'.$msg_profile_pic.'" alt="" /></span>';
					$msg_menu_msgs .= '<span><span>'.$msg_from_name.'</span><span class="time">'.$msg_dth_envio.'</span></span>';
					$msg_menu_msgs .= '<span class="message">'.$msg_assunto.'</span></a></li>';
					
					## Formata total geral de mensagens
					$msg_total = $o_MSGS_TOTAL;
					
				}
				
				//$msg_menu_msgs .= '<li><div class="text-center"><a href="system.php?pg=msgs-msgs" role="button"><strong>'.TXT_MOD_MSGS_VER_TODAS_AS_MSGS.'</strong>&nbsp;<i class="fa fa-plus-circle"></i></a></div></li></ul></ul>';
				
				
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					$msg_total = '';
					$msg_menu_msgs .= '<li><div class="text-center"><a role="button"><strong><i class="fa fa-exclamation-triangle"></i>&nbsp;'.TXT_MOD_MSGS_NENHUMA_MSG_ENCONTRADA.'</strong></a></div></li></ul></ul>';
				}
				else
				{
					$msg_menu_msgs .= '<li><div class="text-center"><a href="system.php?pg=msgs-msgs" role="button"><strong>'.TXT_MOD_MSGS_VER_TODAS_AS_MSGS.'</strong>&nbsp;<i class="fa fa-plus-circle"></i></a></div></li></ul></ul>';
				}
				
			}# /prepara query #
			
			
			## Formata total de mensagens
			#$msg_total = 0;
			
			## Formata botao envelope
			$msg_menu_link    = '<a role="button" class="dropdown-toggle info-number '.$msg_menu_botao_status_class.'" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope-o"></i><span class="badge bg-green">'.$msg_total.'</span></a>';
			
		}
		else
		{
			## Formata botao envelope
			$msg_menu_link    = '<a style="color: #b7b7b7; cursor: not-allowed;"><i class="fa fa-envelope-o" style="color: #b7b7b7;"></i></a>';
		}
		
		## Formata painel de mensagens
		$msg_menu_panel = $msg_menu_link.$msg_menu_msgs;
		
		####
		# Processa pagina solicitada
		####
		switch($_SERVER["REQUEST_METHOD"])
		{
			case 'GET':
				$IN_PG = test_input($_GET["pg"]);
				break;
			case 'POST':
				$IN_PG = test_input($_POST["pg"]);
				break;
			default:
				break;
		}
		
		####
		# Processa requisicao
		####
		{
			## Processa logout
			if($IN_PG == "logout")
			{
				sec_session_start();
				
				// Desfaz todos os valores da sessão  
				$_SESSION = array();
				
				// obtém os parâmetros da sessão 
				$params = session_get_cookie_params();
				
				// Deleta o cookie em uso. 
				setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
				
				// Destrói a sessão 
				session_destroy();
				header('Location: ../index.php');
			}
			
			## Direciona para a tela de renovacao do plano se o prazo de utilizacao expirou
			if($plano_situacao == 'R')
			{
				if($IN_PG != 'cfg-plano')
				{
					header('Location: main.php?pg=cfg-plano');
				}
				else
				{
					include_once "view-cfg-plano_contratado.php";
					$side_menu_active_cfg = 'class="active"';
				}
			}
			else
			{
				switch ($IN_PG) 
				{
					####
					# PAGE CALL //
					####
					
					####
					# DASHBOARD
					####
					case 'painel-dashboard':
						$_TMP = 'painel-dashboard';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-painel-dashboard.php";
						$side_menu_active_painel_controle = 'class="active"';
						break;
					
					####
					# Laudos
					####
					/*
Avaliação de Risco
Biológico
Calor
Eletricidade
Explosivos
Inflamáveis
Particulado
Poeira
Radiação
Ruído
Vapores
Vibração VCI
Vibração VMB
			*/
					case 'laudos-bio':
						$_TMP = 'laudos-bio';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-bio.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-calor':
						$_TMP = 'laudos-calor';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-calor.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-eletr':
						$_TMP = 'laudos-eletr';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-eletr.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-explo':
						$_TMP = 'laudos-explo';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-explo.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-infl':
						$_TMP = 'laudos-infl';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-infl.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-part':
						$_TMP = 'laudos-part';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-part.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-poeira':
						$_TMP = 'laudos-poeira';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-poeira.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-rad':
						$_TMP = 'laudos-rad';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-rad.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-ruido':
						$_TMP = 'laudos-ruido';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-ruido.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-vapor':
						$_TMP = 'laudos-vapor';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-vapor.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					case 'laudos-vibr_vci':
						$_TMP = 'laudos-vibr_vci';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-laudos-vibr_vci.php";
						$side_menu_active_laudos = 'class="active"';
						break;
					
					####
					# Administrativo
					####
					/*
					case 'adm-fornecedor':
						$_TMP = 'adm-fornecedor';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-fornecedor.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					case 'adm-fornecedor_categoria':
						$_TMP = 'adm-fornecedor_categoria';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-fornecedor_categoria.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					case 'adm-contrato':
						$_TMP = 'adm-contrato';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-contrato.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					case 'adm-contrato_categoria':
						$_TMP = 'adm-contrato_categoria';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-contrato_categoria.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					*/
					case 'adm-cliente':
						$_TMP = 'adm-cliente';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-cliente.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					case 'adm-cliente_categoria':
						$_TMP = 'adm-cliente_categoria';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-adm-cliente_categoria.php";
						$side_menu_active_administrativo = 'class="active"';
						break;
					
					/*
					####
					# Formularios
					####
					case 'relat-prod':
						$_TMP = 'relat-prod';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-relat-prod.php";
						$side_menu_active_relat = 'class="active"';
						break;
					case 'relat-etiquetas':
						$_TMP = 'relat-etiquetas';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-relat-etiquetas.php";
						$side_menu_active_relat = 'class="active"';
						break;
					*/
					
					####
					# Configuracoes
					####
					case 'cfg-colabs':
						$_TMP = 'cfg-colabs';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-cfg-colabs.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					/*
					case 'cfg-dispositivos':
						$_TMP = 'cfg-dispositivos';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-cfg-dispositivos.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					case 'cfg-sincr':
						$_TMP = 'cfg-sincr';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-cfg-sincronia.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					*/
					case 'cfg-dashboard':
						$_TMP = 'cfg-dashboard';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-cfg-dashboard.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					case 'cfg-planilha_modelo':
						$_TMP = 'cfg-planilha_modelo';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-cfg-planilha_modelo.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					/*
					case 'cfg-boletos':
						$_TMP = 'cfg-boletos';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar, $IN_TARGET_ID);
						include_once "view-cfg-boletos.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					*/
					case 'cfg-plano':
						$_TMP = 'cfg-plano';
						$ACESSO_MODULO = new AcessoCTRL($ACESSO_CTRL[$_TMP]->acessar, $ACESSO_CTRL[$_TMP]->editar, $ACESSO_CTRL[$_TMP]->apagar);
						include_once "view-cfg-plano_contratado.php";
						$side_menu_active_cfg = 'class="active"';
						break;
					
					####
					# Perfil
					####
					case 'prfl-cad':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-prfl-cad.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'prfl-ajuda':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-prfl-ajuda.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					
					####
					# Ajuda
					####
					case 'ajuda-duvidas-1':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-duvidas-1.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-duvidas-2':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-duvidas-2.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-duvidas-3':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-duvidas-3.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-1':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-conceitos-1.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-2':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-conceitos-2.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-3':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-conceitos-3.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-4':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-conceitos-4.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-5':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-conceitos-5.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-6':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-6.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-conceitos-7':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-7.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-configuracoes-1':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-configuracoes-1.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-configuracoes-2':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-configuracoes-2.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-configuracoes-3':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-configuracoes-3.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-configuracoes-4':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-configuracoes-4.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-configuracoes-5':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-configuracoes-5.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-1':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-1.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-2':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-2.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-3':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-3.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-4':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-4.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-5':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-5.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-6':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-6.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-7':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-7.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-8':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-8.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-9':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-9.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-10':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-10.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-11':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-11.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-12':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-12.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					case 'ajuda-producao-13':
						$ACESSO_MODULO = new AcessoCTRL(1,1,1);
						include_once "view-ajuda-producao-13.php";
						//$side_menu_active_cfg = 'class="active"';
						break;
					####
					# Default
					####
					default:
						header('Location: main.php?pg=painel-dashboard');
						//include_once "system-dashboard.php";
						//$side_menu_active_painel_controle = 'class="active"';
						break;
				}
			}
		}
		
	}
	
	
	
	#<span style="font-size:120%; color:yellow; text-align:center;"><b>EM MANUTENÇÃO!</b></span><br/>
	
	####
	## Mostra HTML básico inicial
	####
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - ".TXT_PAGE_TITLE_SUFIX ?></title>
		
		<link rel="shortcut icon" href="img/favicon1.png"  type="image/x-icon">
		<link rel="stylesheet"    href="fonts/font-awesome/css/font-awesome.min.css">
		<!-- <link rel="stylesheet"    href="css/bootstrap.css" media="screen" /> -->
		<link rel="stylesheet"    href="vendors/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet"    href="css/bootstrap-dialog.min.css">
		<link rel="stylesheet"    href="css/bootstrap-select.min.css">
		<link rel="stylesheet"    href="css/bootstrap-datepicker3.min.css">
		<link rel="stylesheet"    href="css/ie10-viewport-bug-workaround.css">
		<!-- Animate -->
		<link rel="stylesheet"    href="vendors/animate.css/animate.min.css">
		<!-- NProgress -->
		<link rel="stylesheet"    href="vendors/nprogress/nprogress.css">
		<!-- iCheck -->
		<!-- <link rel="stylesheet" href="vendors/iCheck/skins/flat/green.css"> -->
		<link rel="stylesheet"    href="css/custom.css">
		
		<?php mostra_pg_html_head($SID_CTRL); ?>
		
		<script src="js/ie-emulation-modes-warning.js"></script>
		<style>
			<!--
			/*.btn-yellow*/
			.btn-yellow {
			  color: #000000;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:focus,
			.btn-yellow.focus {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:hover {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active,
			.btn-yellow.active,
			.open > .dropdown-toggle.btn-yellow {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active:hover,
			.btn-yellow.active:hover,
			.open > .dropdown-toggle.btn-yellow:hover,
			.btn-yellow:active:focus,
			.btn-yellow.active:focus,
			.open > .dropdown-toggle.btn-yellow:focus,
			.btn-yellow:active.focus,
			.btn-yellow.active.focus,
			.open > .dropdown-toggle.btn-yellow.focus {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active,
			.btn-yellow.active,
			.open > .dropdown-toggle.btn-yellow {
			  background-image: none;
			}
			.btn-yellow.disabled:hover,
			.btn-yellow[disabled]:hover,
			fieldset[disabled] .btn-yellow:hover,
			.btn-yellow.disabled:focus,
			.btn-yellow[disabled]:focus,
			fieldset[disabled] .btn-yellow:focus,
			.btn-yellow.disabled.focus,
			.btn-yellow[disabled].focus,
			fieldset[disabled] .btn-yellow.focus {
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow .badge {
			  color: #337ab7;
			  background-color: #ffffff;
			}
			
			/*.btn-lightblue - 3bafda*/
			.btn-lightblue {
			  color: #000000;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:focus,
			.btn-lightblue.focus {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:hover {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active,
			.btn-lightblue.active,
			.open > .dropdown-toggle.btn-lightblue {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active:hover,
			.btn-lightblue.active:hover,
			.open > .dropdown-toggle.btn-lightblue:hover,
			.btn-lightblue:active:focus,
			.btn-lightblue.active:focus,
			.open > .dropdown-toggle.btn-lightblue:focus,
			.btn-lightblue:active.focus,
			.btn-lightblue.active.focus,
			.open > .dropdown-toggle.btn-lightblue.focus {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active,
			.btn-lightblue.active,
			.open > .dropdown-toggle.btn-lightblue {
			  background-image: none;
			}
			.btn-lightblue.disabled:hover,
			.btn-lightblue[disabled]:hover,
			fieldset[disabled] .btn-lightblue:hover,
			.btn-lightblue.disabled:focus,
			.btn-lightblue[disabled]:focus,
			fieldset[disabled] .btn-lightblue:focus,
			.btn-lightblue.disabled.focus,
			.btn-lightblue[disabled].focus,
			fieldset[disabled] .btn-lightblue.focus {
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue .badge {
			  color: #337ab7;
			  background-color: #ffffff;
			}
			
			/*.btn-purple*/
			.btn-purple {
			  color: #ffffff;
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple:focus,
			.btn-purple.focus {
			  color: #3bafda;
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple:hover {
			  color: #3bafda;
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple:active,
			.btn-purple.active,
			.open > .dropdown-toggle.btn-purple {
			  color: #3bafda;
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple:active:hover,
			.btn-purple.active:hover,
			.open > .dropdown-toggle.btn-purple:hover,
			.btn-purple:active:focus,
			.btn-purple.active:focus,
			.open > .dropdown-toggle.btn-purple:focus,
			.btn-purple:active.focus,
			.btn-purple.active.focus,
			.open > .dropdown-toggle.btn-purple.focus {
			  color: #3bafda;
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple:active,
			.btn-purple.active,
			.open > .dropdown-toggle.btn-purple {
			  background-image: none;
			}
			.btn-purple.disabled:hover,
			.btn-purple[disabled]:hover,
			fieldset[disabled] .btn-purple:hover,
			.btn-purple.disabled:focus,
			.btn-purple[disabled]:focus,
			fieldset[disabled] .btn-purple:focus,
			.btn-purple.disabled.focus,
			.btn-purple[disabled].focus,
			fieldset[disabled] .btn-purple.focus {
			  background-color: #9d00d2;
			  border-color: #d349ff;
			}
			.btn-purple .badge {
			  color: #3bafda;
			  background-color: #ffffff;
			}
			
			
			.bootstrap-select.btn-group .dropdown-menu li a:hover {
				color: #0000ff !important;
				background: #fed670 !important;
			}
			
			.bootstrap-select.btn-group .dropdown-menu {
				background: #fefac2 !important;
			}
			-->
		</style>
	</head>
	<body class="nav-md">
		<div class="container body">
			<div class="main_container">
				<div class="col-md-3 left_col">
					<div class="left_col scroll-view">
						<div class="navbar nav_title" style="border: 0;">
							<span class="site_title">
								<a class="site_title"><i class="fa fa-thumbs-up fa-lg"></i> <span><img src='img/logo_menu_flat_97x24.png' width=97 height=24></span></a>
							</span>
						</div>

						<div class="clearfix"></div>

						<!-- menu profile quick info -->
						<div class="profile">
							<div class="profile_pic">
								<img src="<?php echo PROFILE_PICS_URL."/".$SID_CTRL->getUSER_PROFILE_PIC(); ?>" alt="..." class="img-circle profile_img">
							</div>
							<div class="profile_info">
								<span><i class="fa fa-user"></i>&nbsp;<?php echo $SID_CTRL->getUSER_USERNAME(); ?></span>
								<h2><?php echo utf8_ucfirst($SID_CTRL->getUSER_NOME())." ".utf8_ucfirst($SID_CTRL->getUSER_SOBRENOME()); ?></h2>
								<h3><?php echo $SID_CTRL->getUSER_CATEGORIA_TITULO(); ?></h3>
							</div>
						</div>
						<!-- /menu profile quick info -->

						<br />

<?php 
						####
						# HTML SIDE MENU //
						####
?>
						<!-- sidebar menu -->
						<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
							<div class="menu_section">
								<br clear='all'/>
								<ul class="nav side-menu">
									<li <?php echo $side_menu_active_painel_controle; ?>><a><i class="fa fa-dashboard"></i> <?php echo TXT_SYSTEM_SIDE_MENU_PAINEL_DE_CONTROLE; ?> <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php if($side_menu_active_painel_controle){ echo ' style="display: block;"'; } ?>>
											<?php echo $side_menu_link_painel_controle_dashboard ?>
										</ul>
									</li>
									<li <?php echo $side_menu_active_laudos; ?>><a><i class="fa fa-file-text"></i> <?php echo TXT_SYSTEM_SIDE_MENU_LAUDOS; ?> <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php if($side_menu_active_laudos){ echo ' style="display: block;"'; } ?>>
											
											<!--
Avaliação de Risco
Biológico
Calor
Eletricidade
Explosivos
Inflamáveis
Particulado
Poeira
Radiação
Ruído
Vapores
Vibração VCI
Vibração VMB
-->
											
											<?php echo $side_menu_link_laudos_bio; ?>
											<?php echo $side_menu_link_laudos_calor; ?>
											<?php echo $side_menu_link_laudos_eletr; ?>
											<?php echo $side_menu_link_laudos_explo; ?>
											<?php echo $side_menu_link_laudos_infl; ?>
											<?php echo $side_menu_link_laudos_part; ?>
											<?php echo $side_menu_link_laudos_poeira; ?>
											<?php echo $side_menu_link_laudos_rad; ?>
											<?php echo $side_menu_link_laudos_ruido; ?>
											<?php echo $side_menu_link_laudos_vapor; ?>
											<?php echo $side_menu_link_laudos_vibr_vci; ?>
										</ul>
									</li>
									<li <?php echo $side_menu_active_administrativo; ?>><a><i class="fa fa-handshake-o"></i> <?php echo TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO; ?> <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php if($side_menu_active_administrativo){ echo ' style="display: block;"'; } ?>>
											<?php echo $side_menu_link_adm_cliente; ?>
										</ul>
									</li>
									<!--
									<li <?php echo $side_menu_active_relat; ?>><a><i class="fa fa-bar-chart"></i> <?php echo TXT_SYSTEM_SIDE_MENU_RELATORIOS; ?> <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php if($side_menu_active_relat){ echo ' style="display: block;"'; } ?>>
											<?php echo $side_menu_link_relat_prod; ?>
											<?php echo $side_menu_link_relat_etiquetas; ?>
										</ul>
									</li>
									-->
									<li <?php echo $side_menu_active_cfg; ?>><a><i class="fa fa-cog"></i> <?php echo TXT_SYSTEM_SIDE_MENU_CFG; ?> <span class="fa fa-chevron-down"></span></a>
										<ul class="nav child_menu" <?php if($side_menu_active_cfg){ echo ' style="display: block;"'; } ?>>
											<?php echo $side_menu_link_cfg_colabs ?>
											<?php echo $side_menu_link_cfg_dashboard; ?>
											<?php echo $side_menu_link_cfg_planilha_modelo; ?>
											<!-- <?php echo $side_menu_link_cfg_boletos; ?> -->
											<!-- <?php echo $side_menu_link_cfg_plano ?> -->
										</ul>
									</li>
								</ul>
							</div>
						</div>
						<!-- /sidebar menu -->
					</div>
				</div>

				<!-- top navigation -->
				<div class="top_nav">
					<div class="nav_menu">
						<nav>
							<div class="nav toggle">
								<a id="menu_toggle"><i class="fa fa-bars"></i></a>
							</div>

							<ul class="nav navbar-nav navbar-right">
								<li class="">
									<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
										<img src="<?php echo PROFILE_PICS_URL."/".$SID_CTRL->getUSER_PROFILE_PIC(); ?>" alt=""><?php echo $SID_CTRL->getUSER_USERNAME(); ?>
										<span class=" fa fa-angle-down"></span>
									</a>
									<ul class="dropdown-menu dropdown-usermenu pull-right">
										<li><?php echo $profile_menu_link_cad; ?></li>
										<li><?php echo $profile_menu_link_cfg; ?></li>
										<li><?php echo $profile_menu_link_ajuda; ?></li>
										<li><a href="main.php?pg=logout"><i class="fa fa-sign-out pull-right"></i> <?php echo TXT_SYSTEM_PROFILE_MENU_LOGOUT; ?></a></li>
									</ul>
								</li>

								<!-- mensagens -->
								<!--
								<li id="msgs_menu_panel" role="presentation" class="dropdown">
									<?php echo $msg_menu_panel; ?>
								</li>
								-->
								<!-- /mensagens -->
								
								
							</ul>
						</nav>
					</div>
				</div>
				<!-- /top navigation -->

				<!-- page content -->
				<div class="right_col" role="main">
					<div class="">

						<div class="clearfix"></div>

						<div class="row">
							
							<!-- main_table -->
							<?php mostra_pg_html_body($SID_CTRL, $ACESSO_MODULO); ?>
							<!-- /main_tabe -->
							
						</div>
						
					</div>
				</div>
				<!-- /page content -->

				<!-- footer content -->
				<footer>
				<div class="pull-right">
					<a href="http://www.ther.com.br" target="_blank">SEGVIDA - Ther Sistemas - &copy; 2017</a>
				</div>
				<div class="clearfix"></div>
				</footer>
				<!-- /footer content -->
			</div>
		</div>


<?php mostra_pg_html_modal($SID_CTRL); ?>


<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconFicha" class='fa fa-address-card fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<div class="modal-body">
				<p class="text-center center-block"><i class="fa fa-spinner" aria-hidden="true"></i><br/><font color=#ffffff><b id='loadingBoxText'><?php echo TXT_MODAL_MSG_CARREGANDO ?></b></font></p>
			</div>
		</div>
	</div>
</div>
<!-- /loadingBox -->


<!-- BEGIN # MODAL ENVIAR MSG -->
<div class="modal fade" id="enviarmsgs-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" align="left">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-align:right; margin-top: 17px;">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				<h2><ol class="breadcrumb breadcrumb-arrow">
						<li><a>Mensagem</a></li>
						<li class="active"><span><?php echo TXT_BT_SEND; ?></span></li>
					</ol></h2>
					
			</div>
			
			<!-- Begin # DIV Form -->
			<div id="div-forms">
				<!-- Begin | Register Form -->
				<form id="enviarmsgs-form">
					<div class="modal-body">
						<div id="div-enviarmsgs-msg">
							<div id="icon-enviarmsgs-msg" class="glyphicon glyphicon-chevron-right">
								<span id="text-enviarmsgs-msg">Campos com * são obrigatórios</span>
							</div>
							<span id="to_username_panel">
								<div class="input-group">
									<span class="input-group-addon">
										<b><span style="color:#ff0000;">*</span>&nbsp;Para:</b>
									</span>
									<select multiple 
										id="reg_msgs_to_username" 
										data-selected-text-format="count" 
										data-actions-box="true"
										title="Selecione os usuários"
										class="form-control selectpicker" style="margin-top: 0px; margin-bottom: 10px;" required
										><?php echo $COLABS_LIST; ?></select>
										<!--
										<option value="" data-subtext="(Rodrigo Gomide)">rlgomide</option>
										<option value="" data-subtext="(Romulo Balga)">romuloba</option>
										<option value="" data-subtext="(Marcos Rodrigues)">mascosr</option>
										-->
								</div>
							</span>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;Assunto:</b>
								</span>
								<input id="reg_msgs_subject" type="text" maxlength=180 class="form-control textonly" placeholder="Até 180 caracteres" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;Mensagem:</b>
								</span>
								<textarea id="reg_msgs_text" rows=4 maxlength=1000  class="form-control" placeholder="Até 1000 caracteres" required></textarea>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b>Anexos:</b>
								</span>
								<input type="text" class="form-control" readonly>
								<label class="input-group-btn">
									<span class="btn btn-primary" style="margin-top: 0px; margin-bottom: 0px; border-radius: 3px;">
									<b class="fa fa-folder-open-o"></b> 
									<input type="hidden" name="MAX_FILE_SIZE" value="1048576">
									<input id="reg_msgs_anexos" type="file" class="form-control" style="display: none;" multiple>
									</span>
								</label>
							</div>
							<div class="modal-footer">
								<div>
									<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SEND; ?></button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- End | enviarmsgs Form -->
				
			</div>
			<!-- End # DIV Form -->
			
		</div>
	</div>
</div>
<!-- END # MODAL ENVIAR MSG -->

	<!-- jQuery -->
	<script src="vendors/jquery/dist/jquery.min.js"></script>
	<script>
		var oldJqTrigger = jQuery.fn.trigger;
		jQuery.fn.trigger = function()
		{
			if ( arguments && arguments.length > 0) 
			{
				if (typeof arguments[0] == "object") 
				{
					if (typeof arguments[0].type == "string") 
					{
						if (arguments[0].type == "show.bs.modal") 
						{
							var ret = oldJqTrigger.apply(this, arguments);
							if ($('.modal:visible').length) 
							{
								$('.modal-backdrop.in').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) + 10);
								$(this).css('z-index', parseInt($('.modal-backdrop.in').first().css('z-index')) + 10);
							}
							return ret;
						}
					}
				}
				else if (typeof arguments[0] == "string") 
				{
					if (arguments[0] == "hidden.bs.modal") 
					{
						if ($('.modal:visible').length) 
						{
							$('.modal-backdrop').first().css('z-index', parseInt($('.modal:visible').last().css('z-index')) - 10);
							$('body').addClass('modal-open');
						}
					}
				}
			}
			return oldJqTrigger.apply(this, arguments);
		};
	</script>
	<script src='js/jquery.storage.js'></script>
	
	<!-- Bootstrap -->
	<script src="js/bootstrap.min.js"></script>
	<script src="js/bootstrap-dialog.min.js"></script>
	<!-- Bootstrap select -->
	<script src="js/bootstrap-select-pt-br.js"></script>
	<!-- sha512 -->
	<script src="js/sha512.js"></script>
	<!-- numeral -->
	<!-- <script src="js/numeral-with-locales.min.js"></script> -->
	<!-- decimal -->
	<!-- <script src="js/decimal.min.js"></script> -->
	<!-- autoNumeric -->
	<script src="js/autoNumeric.min.js"></script>
	<!-- datepicker -->
	<script src="vendors/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
	<script src="vendors/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min.js" charset="UTF-8"></script>
	<!-- FastClick -->
	<script src="vendors/fastclick/lib/fastclick.js"></script>
	<!-- NProgress -->
	<script src="vendors/nprogress/nprogress.js"></script>
	<!-- Custom Theme Scripts -->
	<script src="js/custom.js" charset="UTF-8"></script>
	<!-- iCheck -->
	<script src="vendors/iCheck/icheck.min.js"></script>
	
	<?php if($SID_CTRL->getPLANO_CODIGO() == "PR1-M1" || $SID_CTRL->getPLANO_CODIGO() == "PR1-A1") { ?>
	<!--Start of Tawk.to Script-->
	<!--
	<script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	Tawk_API.visitor = {
		name  : '<?php echo $SID_CTRL->getUSER_NOME()." ".$SID_CTRL->getUSER_SOBRENOME(); ?>',
		email : '<?php echo $SID_CTRL->getUSER_EMAIL(); ?>',
		plano : '<?php echo $SID_CTRL->getPLANO_CODIGO(); ?>'
	};
	Tawk_API.onLoad = function()
	{
		Tawk_API.setAttributes({
			'name'  : '<?php echo $SID_CTRL->getUSER_NOME()." ".$SID_CTRL->getUSER_SOBRENOME(); ?>',
			'email' : '<?php echo $SID_CTRL->getUSER_EMAIL(); ?>',
			'plano' : '<?php echo $SID_CTRL->getPLANO_CODIGO(); ?>'
		}, function(error){});
	}
	;
	
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/58d01b0b78d62074c0a79e93/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
	
	</script>
	-->
	<!--End of Tawk.to Script-->
	<?php } ?>
	
	
<?php mostra_pg_html_javascript($SID_CTRL); ?>


	</body>
</html>
<?php ob_end_flush(); ?>
