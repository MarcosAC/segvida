﻿<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-laudos-emissao.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 4/10/2017 14:31:57
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_ANO = $IN_MES = $IN_CLIENTE = $IN_LD_BIO_PLAN = $IN_LD_BIO_MOD = $IN_LD_CALOR_PLAN = $IN_LD_CALOR_MOD = $IN_LD_ELETR_PLAN = $IN_LD_ELETR_MOD = $IN_LD_EXPL_PLAN = $IN_LD_EXPL_MOD = $IN_LD_INFL_PLAN = $IN_LD_INFL_MOD = $IN_LD_PART_PLAN = $IN_LD_PART_MOD = $IN_LD_POEI_PLAN = $IN_LD_POEI_MOD = $IN_LD_RAD_PLAN = $IN_LD_RAD_MOD = $IN_LD_RIS_PLAN = $IN_LD_RIS_MOD = $IN_LD_RUI_PLAN = $IN_LD_RUI_MOD = $IN_LD_VAP_PLAN = $IN_LD_VAP_MOD = $IN_LD_VBRVCI_PLAN = $IN_LD_VBRVCI_MOD = $IN_LD_VBRVMB_PLAN = $IN_LD_VBRVMB_MOD = "";
	
	$IN_LD_BIO_SET = $IN_LD_CALOR_SET = $IN_LD_ELETR_SET = $IN_LD_EXPL_SET = $IN_LD_INFL_SET = $IN_LD_PART_SET = $IN_LD_POEI_SET = $IN_LD_RAD_SET = $IN_LD_RIS_SET = $IN_LD_RUI_SET = $IN_LD_VAP_SET = $IN_LD_VBRVCI_SET = $IN_LD_VBRVMB_SET = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_ANO = test_input($_GET["ano"]);
		$IN_MES = test_input($_GET["mes"]);
		$IN_CLIENTE = test_input($_GET["cliente"]);
		$IN_LD_BIO_PLAN = test_input($_GET["ld_bio_plan"]);
		$IN_LD_BIO_MOD = test_input($_GET["ld_bio_mod"]);
		$IN_LD_CALOR_PLAN = test_input($_GET["ld_calor_plan"]);
		$IN_LD_CALOR_MOD = test_input($_GET["ld_calor_mod"]);
		$IN_LD_ELETR_PLAN = test_input($_GET["ld_eletr_plan"]);
		$IN_LD_ELETR_MOD = test_input($_GET["ld_eletr_mod"]);
		$IN_LD_EXPL_PLAN = test_input($_GET["ld_expl_plan"]);
		$IN_LD_EXPL_MOD = test_input($_GET["ld_expl_mod"]);
		$IN_LD_INFL_PLAN = test_input($_GET["ld_infl_plan"]);
		$IN_LD_INFL_MOD = test_input($_GET["ld_infl_mod"]);
		$IN_LD_PART_PLAN = test_input($_GET["ld_part_plan"]);
		$IN_LD_PART_MOD = test_input($_GET["ld_part_mod"]);
		$IN_LD_POEI_PLAN = test_input($_GET["ld_poei_plan"]);
		$IN_LD_POEI_MOD = test_input($_GET["ld_poei_mod"]);
		$IN_LD_RAD_PLAN = test_input($_GET["ld_rad_plan"]);
		$IN_LD_RAD_MOD = test_input($_GET["ld_rad_mod"]);
		$IN_LD_RUI_PLAN = test_input($_GET["ld_rui_plan"]);
		$IN_LD_RUI_MOD = test_input($_GET["ld_rui_mod"]);
		$IN_LD_VAP_PLAN = test_input($_GET["ld_vap_plan"]);
		$IN_LD_VAP_MOD = test_input($_GET["ld_vap_mod"]);
		$IN_LD_VBRVCI_PLAN = test_input($_GET["ld_vbrvci_plan"]);
		$IN_LD_VBRVCI_MOD = test_input($_GET["ld_vbrvci_mod"]);
		$IN_LD_VBRVMB_PLAN = test_input($_GET["ld_vbrvmb_plan"]);
		$IN_LD_VBRVMB_MOD = test_input($_GET["ld_vbrvmb_mod"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_ANO = test_input($_POST["ano"]);
		$IN_MES = test_input($_POST["mes"]);
		$IN_CLIENTE = test_input($_POST["cliente"]);
		$IN_LD_BIO_SET     = test_input($_POST["ld_bio_set"]);
		$IN_LD_BIO_PLAN    = test_input($_POST["ld_bio_plan"]);
		$IN_LD_BIO_MOD     = test_input($_POST["ld_bio_mod"]);
		$IN_LD_CALOR_SET   = test_input($_POST["ld_calor_set"]);
		$IN_LD_CALOR_PLAN  = test_input($_POST["ld_calor_plan"]);
		$IN_LD_CALOR_MOD   = test_input($_POST["ld_calor_mod"]);
		$IN_LD_ELETR_SET   = test_input($_POST["ld_eletr_set"]);
		$IN_LD_ELETR_PLAN  = test_input($_POST["ld_eletr_plan"]);
		$IN_LD_ELETR_MOD   = test_input($_POST["ld_eletr_mod"]);
		$IN_LD_EXPL_SET    = test_input($_POST["ld_expl_set"]);
		$IN_LD_EXPL_PLAN   = test_input($_POST["ld_expl_plan"]);
		$IN_LD_EXPL_MOD    = test_input($_POST["ld_expl_mod"]);
		$IN_LD_INFL_SET    = test_input($_POST["ld_infl_set"]);
		$IN_LD_INFL_PLAN   = test_input($_POST["ld_infl_plan"]);
		$IN_LD_INFL_MOD    = test_input($_POST["ld_infl_mod"]);
		$IN_LD_PART_SET    = test_input($_POST["ld_part_set"]);
		$IN_LD_PART_PLAN   = test_input($_POST["ld_part_plan"]);
		$IN_LD_PART_MOD    = test_input($_POST["ld_part_mod"]);
		$IN_LD_POEI_SET    = test_input($_POST["ld_poei_set"]);
		$IN_LD_POEI_PLAN   = test_input($_POST["ld_poei_plan"]);
		$IN_LD_POEI_MOD    = test_input($_POST["ld_poei_mod"]);
		$IN_LD_RAD_SET     = test_input($_POST["ld_rad_set"]);
		$IN_LD_RAD_PLAN    = test_input($_POST["ld_rad_plan"]);
		$IN_LD_RAD_MOD     = test_input($_POST["ld_rad_mod"]);
		$IN_LD_RIS_SET     = test_input($_POST["ld_ris_set"]);
		$IN_LD_RIS_PLAN    = test_input($_POST["ld_ris_plan"]);
		$IN_LD_RIS_MOD     = test_input($_POST["ld_ris_mod"]);
		$IN_LD_RUI_SET     = test_input($_POST["ld_rui_set"]);
		$IN_LD_RUI_PLAN    = test_input($_POST["ld_rui_plan"]);
		$IN_LD_RUI_MOD     = test_input($_POST["ld_rui_mod"]);
		$IN_LD_VAP_SET     = test_input($_POST["ld_vap_set"]);
		$IN_LD_VAP_PLAN    = test_input($_POST["ld_vap_plan"]);
		$IN_LD_VAP_MOD     = test_input($_POST["ld_vap_mod"]);
		$IN_LD_VBRVCI_SET  = test_input($_POST["ld_vbrvci_set"]);
		$IN_LD_VBRVCI_PLAN = test_input($_POST["ld_vbrvci_plan"]);
		$IN_LD_VBRVCI_MOD  = test_input($_POST["ld_vbrvci_mod"]);
		$IN_LD_VBRVMB_SET  = test_input($_POST["ld_vbrvmb_set"]);
		$IN_LD_VBRVMB_PLAN = test_input($_POST["ld_vbrvmb_plan"]);
		$IN_LD_VBRVMB_MOD  = test_input($_POST["ld_vbrvmb_mod"]);
		
		## case convertion
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## GERAR LAUDO FINAL
			case '8':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_GERAR($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_SET, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_SET, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_SET, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_SET, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_SET, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_SET, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_SET, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_SET, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RIS_SET, $IN_LD_RIS_PLAN, $IN_LD_RIS_MOD, $IN_LD_RUI_SET, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_SET, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_SET, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_SET, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_CLIENTE) || isEmpty($IN_LD_BIO_PLAN) || isEmpty($IN_LD_BIO_MOD) || isEmpty($IN_LD_CALOR_PLAN) || isEmpty($IN_LD_CALOR_MOD) || isEmpty($IN_LD_ELETR_PLAN) || isEmpty($IN_LD_ELETR_MOD) || isEmpty($IN_LD_EXPL_PLAN) || isEmpty($IN_LD_EXPL_MOD) || isEmpty($IN_LD_INFL_PLAN) || isEmpty($IN_LD_INFL_MOD) || isEmpty($IN_LD_PART_PLAN) || isEmpty($IN_LD_PART_MOD) || isEmpty($IN_LD_POEI_PLAN) || isEmpty($IN_LD_POEI_MOD) || isEmpty($IN_LD_RAD_PLAN) || isEmpty($IN_LD_RAD_MOD) || isEmpty($IN_LD_RUI_PLAN) || isEmpty($IN_LD_RUI_MOD) || isEmpty($IN_LD_VAP_PLAN) || isEmpty($IN_LD_VAP_MOD) || isEmpty($IN_LD_VBRVCI_PLAN) || isEmpty($IN_LD_VBRVCI_MOD) || isEmpty($IN_LD_VBRVMB_PLAN) || isEmpty($IN_LD_VBRVMB_MOD) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) || isEmpty($IN_REG_ID) || isEmpty($IN_ANO) || isEmpty($IN_MES) || isEmpty($IN_CLIENTE) || isEmpty($IN_LD_BIO_PLAN) || isEmpty($IN_LD_BIO_MOD) || isEmpty($IN_LD_CALOR_PLAN) || isEmpty($IN_LD_CALOR_MOD) || isEmpty($IN_LD_ELETR_PLAN) || isEmpty($IN_LD_ELETR_MOD) || isEmpty($IN_LD_EXPL_PLAN) || isEmpty($IN_LD_EXPL_MOD) || isEmpty($IN_LD_INFL_PLAN) || isEmpty($IN_LD_INFL_MOD) || isEmpty($IN_LD_PART_PLAN) || isEmpty($IN_LD_PART_MOD) || isEmpty($IN_LD_POEI_PLAN) || isEmpty($IN_LD_POEI_MOD) || isEmpty($IN_LD_RAD_PLAN) || isEmpty($IN_LD_RAD_MOD) || isEmpty($IN_LD_RUI_PLAN) || isEmpty($IN_LD_RUI_MOD) || isEmpty($IN_LD_VAP_PLAN) || isEmpty($IN_LD_VAP_MOD) || isEmpty($IN_LD_VBRVCI_PLAN) || isEmpty($IN_LD_VBRVCI_MOD) || isEmpty($IN_LD_VBRVMB_PLAN) || isEmpty($IN_LD_VBRVMB_MOD) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_ANO, $IN_MES, $IN_CLIENTE, $IN_LD_BIO_PLAN, $IN_LD_BIO_MOD, $IN_LD_CALOR_PLAN, $IN_LD_CALOR_MOD, $IN_LD_ELETR_PLAN, $IN_LD_ELETR_MOD, $IN_LD_EXPL_PLAN, $IN_LD_EXPL_MOD, $IN_LD_INFL_PLAN, $IN_LD_INFL_MOD, $IN_LD_PART_PLAN, $IN_LD_PART_MOD, $IN_LD_POEI_PLAN, $IN_LD_POEI_MOD, $IN_LD_RAD_PLAN, $IN_LD_RAD_MOD, $IN_LD_RUI_PLAN, $IN_LD_RUI_MOD, $IN_LD_VAP_PLAN, $IN_LD_VAP_MOD, $IN_LD_VBRVCI_PLAN, $IN_LD_VBRVCI_MOD, $IN_LD_VBRVMB_PLAN, $IN_LD_VBRVMB_MOD);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# GERA LAUDO FINAL
	####
	function executa_GERAR($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_SET, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_SET, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_SET, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_SET, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_SET, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_SET, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_SET, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_SET, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RIS_SET, $_LD_RIS_PLAN, $_LD_RIS_MOD, $_LD_RUI_SET, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_SET, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_SET, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_SET, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_DDMMAAAA = '%d/%m/%Y';
		
		####
		# PROCESSA LAUDOS
		####
		$o_RET = array(
			"bio_msg"     => "",
			"bio_file"    => "",
			"calor_msg"   => "",
			"calor_file"  => "",
			"eletr_msg"   => "",
			"eletr_file"  => "",
			"expl_msg"    => "",
			"expl_file"   => "",
			"infl_msg"    => "",
			"infl_file"   => "",
			"part_msg"    => "",
			"part_file"   => "",
			"poei_msg"    => "",
			"poei_file"   => "",
			"rad_msg"     => "",
			"rad_file"    => "",
			"ris_msg"     => "",
			"ris_file"    => "",
			"rui_msg"     => "",
			"rui_file"    => "",
			"vap_msg"     => "",
			"vap_file"    => "",
			"vbrvci_msg"  => "",
			"vbrvci_file" => "",
			"vbrvmb_msg"  => "",
			"vbrvmb_file" => ""
		);
		
		## Image Type
		/*
		$_img_type = array(
			"1" => "gif",
			"2" => "jpg",
			"3" => "png"
		);
		*/
		
		/** Include PHPExcel */
		//require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
		
		$LAUDOS_COUNT=0;
		
		####
		# Formata Cliente
		$o_CLIENTE_NOME_INTERNO = getClienteNomeInterno($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_CLIENTE);
		$o_CLIENTE_NOME_INTERNO = str_replace(" ","_",$o_CLIENTE_NOME_INTERNO);
		
		####
		# BIOLOGICO
		####
		if($_LD_BIO_SET 
		&& file_exists("includes/gera_laudo_final_bio.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_bio.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_BIO_MOD);
			$RET_TMP = GeraLaudoFinalBIO($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['bio_msg']  = $tmp[0];
			$o_RET['bio_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n bio_msg -> ".$o_RET['bio_msg']."\n\n"."bio_file -> ".$o_RET['bio_file']."\n\n",0);
			*/
		}
		
		####
		# CALOR
		####
		if($_LD_CALOR_SET 
		&& file_exists("includes/gera_laudo_final_calor.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_calor.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_CALOR_MOD);
			$RET_TMP = GeraLaudoFinalCALOR($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['calor_msg']  = $tmp[0];
			$o_RET['calor_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n calor_msg -> ".$o_RET['calor_msg']."\n\n"."calor_file -> ".$o_RET['calor_file']."\n\n",0);
			*/
			
		}
		####
		# ELETRICIDADE
		####
		if($_LD_ELETR_SET 
		&& file_exists("includes/gera_laudo_final_eletr.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_eletr.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_ELETR_MOD);
			$RET_TMP = GeraLaudoFinalELETR($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['eletr_msg']  = $tmp[0];
			$o_RET['eletr_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n eletr_msg -> ".$o_RET['eletr_msg']."\n\n"."eletr_file -> ".$o_RET['eletr_file']."\n\n",0);
			*/
		}
		####
		# EXPLOSIVO
		####
		if($_LD_EXPL_SET 
		&& file_exists("includes/gera_laudo_final_expl.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_expl.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_EXPL_MOD);
			$RET_TMP = GeraLaudoFinalEXPL($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['expl_msg']  = $tmp[0];
			$o_RET['expl_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n expl_msg -> ".$o_RET['expl_msg']."\n\n"."expl_file -> ".$o_RET['expl_file']."\n\n",0);
			*/
		}
		####
		# INFLAMAVEL
		####
		if($_LD_INFL_SET 
		&& file_exists("includes/gera_laudo_final_infl.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_infl.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_INFL_MOD);
			$RET_TMP = GeraLaudoFinalINFL($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['infl_msg']  = $tmp[0];
			$o_RET['infl_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n infl_msg -> ".$o_RET['infl_msg']."\n\n"."infl_file -> ".$o_RET['infl_file']."\n\n",0);
			*/
		}
		####
		# PARTICULADO
		####
		if($_LD_PART_SET 
		&& file_exists("includes/gera_laudo_final_part.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_part.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PART_MOD);
			$RET_TMP = GeraLaudoFinalPART($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PART_PLAN, $_LD_PART_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['part_msg']  = $tmp[0];
			$o_RET['part_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n part_msg -> ".$o_RET['part_msg']."\n\n"."part_file -> ".$o_RET['part_file']."\n\n",0);
			*/
		}
		####
		# POEIRA
		####
		if($_LD_POEI_SET 
		&& file_exists("includes/gera_laudo_final_poei.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_poei.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_POEI_MOD);
			$RET_TMP = GeraLaudoFinalPOEI($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['poei_msg']  = $tmp[0];
			$o_RET['poei_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n poei_msg -> ".$o_RET['poei_msg']."\n\n"."poei_file -> ".$o_RET['poei_file']."\n\n",0);
			*/
		}
		####
		# RADIACAO
		####
		if($_LD_RAD_SET 
		&& file_exists("includes/gera_laudo_final_rad.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_rad.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RAD_MOD);
			$RET_TMP = GeraLaudoFinalRAD($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['rad_msg']  = $tmp[0];
			$o_RET['rad_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n rad_msg -> ".$o_RET['rad_msg']."\n\n"."rad_file -> ".$o_RET['rad_file']."\n\n",0);
			*/
		}
		####
		# RISCO
		####
		if($_LD_RIS_SET 
		&& file_exists("includes/gera_laudo_final_ris.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_ris.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RIS_MOD);
			$RET_TMP = GeraLaudoFinalRIS($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RIS_PLAN, $_LD_RIS_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['ris_msg']  = $tmp[0];
			$o_RET['ris_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n ris_msg -> ".$o_RET['ris_msg']."\n\n"."ris_file -> ".$o_RET['ris_file']."\n\n",0);
			*/
		}
		####
		# RUIDO
		####
		if($_LD_RUI_SET 
		&& file_exists("includes/gera_laudo_final_rui.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_rui.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RUI_MOD);
			$RET_TMP = GeraLaudoFinalRUI($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['rui_msg']  = $tmp[0];
			$o_RET['rui_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n rui_msg -> ".$o_RET['rui_msg']."\n\n"."rui_file -> ".$o_RET['rui_file']."\n\n",0);
			*/
		}
		####
		# VAPOR
		####
		if($_LD_VAP_SET 
		&& file_exists("includes/gera_laudo_final_vap.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_vap.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VAP_MOD);
			$RET_TMP = GeraLaudoFinalVAP($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['vap_msg']  = $tmp[0];
			$o_RET['vap_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n vap_msg -> ".$o_RET['vap_msg']."\n\n"."vap_file -> ".$o_RET['vap_file']."\n\n",0);
			*/
		}
		####
		# VIBRCAO VCI
		####
		if($_LD_VBRVCI_SET 
		&& file_exists("includes/gera_laudo_final_vbrvci.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_vbrvci.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VBRVCI_MOD);
			$RET_TMP = GeraLaudoFinalVBRVCI($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['vbrvci_msg']  = $tmp[0];
			$o_RET['vbrvci_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\nvbrvci_msg -> ".$o_RET['vbrvci_msg']."\n\n"."vbrvci_file -> ".$o_RET['vbrvci_file']."\n\n",0);
			*/
		}
		####
		# VIBRACAO VMB
		####
		if($_LD_VBRVMB_SET 
		&& file_exists("includes/gera_laudo_final_vbrvmb.php")
		)
		{
			$LAUDOS_COUNT++;
			include_once "includes/gera_laudo_final_vbrvmb.php";
			$_MODELO = getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VBRVMB_MOD);
			$RET_TMP = GeraLaudoFinalVBRVMB($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD, $_MODELO);
			$tmp = explode("|",$RET_TMP);
			$o_RET['vbrvmb_msg']  = $tmp[0];
			$o_RET['vbrvmb_file'] = $tmp[1];
			
			/*
			error_log("mdl-laudos-emissao.php:\n\n vbrvmb_msg -> ".$o_RET['vbrvmb_msg']."\n\n"."vbrvmb_file -> ".$o_RET['vbrvmb_file']."\n\n",0);
			*/
		}
		
		
		
		####
		# GERA ZIP DOS LAUDOS
		####
		{
			$ZIP_filename = getValidRandomFilename(LAUDO_FINAL_PATH, "ZIP", 1);
			
			$ZIP = new ZipArchive();
			
			// Cria o Arquivo Zip, caso não consiga exibe mensagem de erro e finaliza script
			if($ZIP->open(LAUDO_FINAL_PATH.'/'.$ZIP_filename, ZIPARCHIVE::CREATE) == TRUE)
			{
				####
				# Insere os arquivos que devem conter no arquivo zip
				$hoje = date('d-m-Y__H-i-s');
				# BIO
				if($_LD_BIO_SET && $o_RET['bio_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['bio_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/biologico.xlsx'); }
				# CALOR
				if($_LD_CALOR_SET && $o_RET['calor_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['calor_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/calor.xlsx'); }
				# ELETRICIDADE
				if($_LD_ELETR_SET && $o_RET['eletr_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['eletr_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/eletricidade.xlsx'); }
				# EXPLOSIVO
				if($_LD_EXPL_SET && $o_RET['expl_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['expl_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/explosivo.xlsx'); }
				# INFLAMAVEL
				if($_LD_INFL_SET && $o_RET['infl_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['infl_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/inflamavel.xlsx'); }
				# PARTICULADO
				if($_LD_PART_SET && $o_RET['part_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['part_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/particulado.xlsx'); }
				# POEIRA
				if($_LD_POEI_SET && $o_RET['poei_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['poei_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/poeira.xlsx'); }
				# RADIACAO
				if($_LD_RAD_SET && $o_RET['rad_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['rad_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/radiacao_ionizante.xlsx'); }
				# RISCO
				if($_LD_RIS_SET && $o_RET['ris_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['ris_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/risco.xlsx'); }
				# RUIDO
				if($_LD_RUI_SET && $o_RET['rui_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['rui_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/ruido.xlsx'); }
				# VAPOR
				if($_LD_VAP_SET && $o_RET['vap_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['vap_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/vapor.xlsx'); }
				# VIBRACAO VCI
				if($_LD_VBRVCI_SET && $o_RET['vbrvci_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['vbrvci_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/vibracao_vci.xlsx'); }
				# VIBRACAO VMB
				if($_LD_VBRVMB_SET && $o_RET['vbrvmb_file']){ $ZIP->addFile(LAUDO_FINAL_PATH.'/'.$o_RET['vbrvmb_file'],'SEGVIDA_LAUDO_FINAL/'.$o_CLIENTE_NOME_INTERNO.'_'.$hoje.'/vibracao_vmb.xlsx'); }
			}
			else
			{
				error_log("mdl-laudos-emissao.php -> ZIP (".$ZIP_filename.") -> O Arquivo não pode ser criado!",0);
			}
			
			// Fecha arquivo Zip aberto
			$ZIP->close();
		}
		
		####
		# Remove arquivos ja processados
		####
		//if($RRR == 1)
		{
			# BIO
			if($_LD_BIO_SET && $o_RET['bio_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['bio_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['bio_file']); } }
			# CALOR
			if($_LD_CALOR_SET && $o_RET['calor_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['calor_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['calor_file']); } }
			# ELETRICIDADE
			if($_LD_ELETR_SET && $o_RET['eletr_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['eletr_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['eletr_file']); } }
			# EXPLOSIVO
			if($_LD_EXPL_SET && $o_RET['expl_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['expl_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['expl_file']); } }
			# INFLAMAVEL
			if($_LD_INFL_SET && $o_RET['infl_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['infl_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['infl_file']); } }
			# PARTICULADO
			if($_LD_PART_SET && $o_RET['part_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['part_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['part_file']); } }
			# POEIRA
			if($_LD_POEI_SET && $o_RET['poei_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['poei_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['poei_file']); } }
			# RADIACAO
			if($_LD_RAD_SET && $o_RET['rad_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['rad_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['rad_file']); } }
			# RISCO
			if($_LD_RIS_SET && $o_RET['ris_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['ris_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['ris_file']); } }
			# RUIDO
			if($_LD_RUI_SET && $o_RET['rui_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['rui_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['rui_file']); } }
			# VAPOR
			if($_LD_VAP_SET && $o_RET['vap_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['vap_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['vap_file']); } }
			# VIBRACAO VCI
			if($_LD_VBRVCI_SET && $o_RET['vbrvci_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['vbrvci_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['vbrvci_file']); } }
			# VIBRACAO VMB
			if($_LD_VBRVMB_SET && $o_RET['vbrvmb_file']){ if ( file_exists(LAUDO_FINAL_PATH.'/'.$o_RET['vbrvmb_file']) ) { unlink (LAUDO_FINAL_PATH.'/'.$o_RET['vbrvmb_file']); } }
		}
		
		####
		# Remove arquivos antigos da pasta LAUDO_FINAL_PATH - 30 dias
		####
		$_NOW   = time();
		$_PRAZO = 30*24*60*60;//30 dias
		if (file_exists(LAUDO_FINAL_PATH)) 
		{
			foreach (new DirectoryIterator(LAUDO_FINAL_PATH) as $fileInfo) 
			{
				if ($fileInfo->isDot()) 
				{
					continue;
				}
				if ($fileInfo->isFile() && $_NOW - $fileInfo->getCTime() >= $_PRAZO) 
				{
					unlink($fileInfo->getRealPath());
				}
			}
		}
		
		//$link = LAUDO_FINAL_URL."/".$RET_MSG['bio_file'];
		$link = LAUDO_FINAL_URL."/".$ZIP_filename;
		
		//$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo XLSX.</b></a>";
		
		     if($LAUDOS_COUNT == 1){ $_MSG = "Laudo Final gerado com sucesso:"; }
		else if($LAUDOS_COUNT > 1) { $_MSG = "Laudos Finais gerados com sucesso:"; }
		
		return "1|".$_MSG." <a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Baixar Arquivo</b></a>|success|";
		exit;
		
	}
	
	function getClienteNomeInterno($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT upper(AA.`nome_interno`)
       FROM `CLIENTE` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idCLIENTE` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_NOME_INTERNO);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			/*
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_LAUDO."|".$o_ARQUIVO_FILENAME."|";
				exit;
			}
			*/
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		return $o_NOME_INTERNO;
		//exit;
	}
	function getArquivoModeloPlanilha($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT lower(AA.`arquivo_filename`) as arquivo_filename
       FROM `PLANILHA_MODELO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idPLANILHA_MODELO` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ARQUIVO_FILENAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			/*
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_LAUDO."|".$o_ARQUIVO_FILENAME."|";
				exit;
			}
			*/
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		return $o_ARQUIVO_FILENAME;
		//exit;
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_LD_BIO_PLAN = $mysqli->escape_String($_LD_BIO_PLAN);
		$sql_LD_BIO_MOD = $mysqli->escape_String($_LD_BIO_MOD);
		$sql_LD_CALOR_PLAN = $mysqli->escape_String($_LD_CALOR_PLAN);
		$sql_LD_CALOR_MOD = $mysqli->escape_String($_LD_CALOR_MOD);
		$sql_LD_ELETR_PLAN = $mysqli->escape_String($_LD_ELETR_PLAN);
		$sql_LD_ELETR_MOD = $mysqli->escape_String($_LD_ELETR_MOD);
		$sql_LD_EXPL_PLAN = $mysqli->escape_String($_LD_EXPL_PLAN);
		$sql_LD_EXPL_MOD = $mysqli->escape_String($_LD_EXPL_MOD);
		$sql_LD_INFL_PLAN = $mysqli->escape_String($_LD_INFL_PLAN);
		$sql_LD_INFL_MOD = $mysqli->escape_String($_LD_INFL_MOD);
		$sql_LD_PART_PLAN = $mysqli->escape_String($_LD_PART_PLAN);
		$sql_LD_PART_MOD = $mysqli->escape_String($_LD_PART_MOD);
		$sql_LD_POEI_PLAN = $mysqli->escape_String($_LD_POEI_PLAN);
		$sql_LD_POEI_MOD = $mysqli->escape_String($_LD_POEI_MOD);
		$sql_LD_RAD_PLAN = $mysqli->escape_String($_LD_RAD_PLAN);
		$sql_LD_RAD_MOD = $mysqli->escape_String($_LD_RAD_MOD);
		$sql_LD_RUI_PLAN = $mysqli->escape_String($_LD_RUI_PLAN);
		$sql_LD_RUI_MOD = $mysqli->escape_String($_LD_RUI_MOD);
		$sql_LD_VAP_PLAN = $mysqli->escape_String($_LD_VAP_PLAN);
		$sql_LD_VAP_MOD = $mysqli->escape_String($_LD_VAP_MOD);
		$sql_LD_VBRVCI_PLAN = $mysqli->escape_String($_LD_VBRVCI_PLAN);
		$sql_LD_VBRVCI_MOD = $mysqli->escape_String($_LD_VBRVCI_MOD);
		$sql_LD_VBRVMB_PLAN = $mysqli->escape_String($_LD_VBRVMB_PLAN);
		$sql_LD_VBRVMB_MOD = $mysqli->escape_String($_LD_VBRVMB_MOD);
		
		
		## Verifica se o registro já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
"
		)) 
		{
			$stmt->bind_param('s', $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua o registro
		if($_DEBUG == 1)
		{
			//$SQL = "";
			//($statement = $mysqli->prepare($SQL)) or trigger_error($mysqli->error, E_USER_ERROR);
			//$statement->execute() or trigger_error($statement->error, E_USER_ERROR);
		}
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `EMISSAO` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_date` = now(),
		        `cad_username` = ?,
		        `ano` = ?
		        ,`mes` = ?
		        ,`cliente` = ?
		        ,`ld_bio_plan` = ?
		        ,`ld_bio_mod` = ?
		        ,`ld_calor_plan` = ?
		        ,`ld_calor_mod` = ?
		        ,`ld_eletr_plan` = ?
		        ,`ld_eletr_mod` = ?
		        ,`ld_expl_plan` = ?
		        ,`ld_expl_mod` = ?
		        ,`ld_infl_plan` = ?
		        ,`ld_infl_mod` = ?
		        ,`ld_part_plan` = ?
		        ,`ld_part_mod` = ?
		        ,`ld_poei_plan` = ?
		        ,`ld_poei_mod` = ?
		        ,`ld_rad_plan` = ?
		        ,`ld_rad_mod` = ?
		        ,`ld_rui_plan` = ?
		        ,`ld_rui_mod` = ?
		        ,`ld_vap_plan` = ?
		        ,`ld_vap_mod` = ?
		        ,`ld_vbrvci_plan` = ?
		        ,`ld_vbrvci_mod` = ?
		        ,`ld_vbrvmb_plan` = ?
		        ,`ld_vbrvmb_mod` = ?
"
		)) 
		{
			$stmt->bind_param('sssdddddddddddddddddddddddddd', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_CLIENTE, $sql_LD_BIO_PLAN, $sql_LD_BIO_MOD, $sql_LD_CALOR_PLAN, $sql_LD_CALOR_MOD, $sql_LD_ELETR_PLAN, $sql_LD_ELETR_MOD, $sql_LD_EXPL_PLAN, $sql_LD_EXPL_MOD, $sql_LD_INFL_PLAN, $sql_LD_INFL_MOD, $sql_LD_PART_PLAN, $sql_LD_PART_MOD, $sql_LD_POEI_PLAN, $sql_LD_POEI_MOD, $sql_LD_RAD_PLAN, $sql_LD_RAD_MOD, $sql_LD_RUI_PLAN, $sql_LD_RUI_MOD, $sql_LD_VAP_PLAN, $sql_LD_VAP_MOD, $sql_LD_VBRVCI_PLAN, $sql_LD_VBRVCI_MOD, $sql_LD_VBRVMB_PLAN, $sql_LD_VBRVMB_MOD);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_ANO, $_MES, $_CLIENTE, $_LD_BIO_PLAN, $_LD_BIO_MOD, $_LD_CALOR_PLAN, $_LD_CALOR_MOD, $_LD_ELETR_PLAN, $_LD_ELETR_MOD, $_LD_EXPL_PLAN, $_LD_EXPL_MOD, $_LD_INFL_PLAN, $_LD_INFL_MOD, $_LD_PART_PLAN, $_LD_PART_MOD, $_LD_POEI_PLAN, $_LD_POEI_MOD, $_LD_RAD_PLAN, $_LD_RAD_MOD, $_LD_RUI_PLAN, $_LD_RUI_MOD, $_LD_VAP_PLAN, $_LD_VAP_MOD, $_LD_VBRVCI_PLAN, $_LD_VBRVCI_MOD, $_LD_VBRVMB_PLAN, $_LD_VBRVMB_MOD)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_ANO = $mysqli->escape_String($_ANO);
		$sql_MES = $mysqli->escape_String($_MES);
		$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
		$sql_LD_BIO_PLAN = $mysqli->escape_String($_LD_BIO_PLAN);
		$sql_LD_BIO_MOD = $mysqli->escape_String($_LD_BIO_MOD);
		$sql_LD_CALOR_PLAN = $mysqli->escape_String($_LD_CALOR_PLAN);
		$sql_LD_CALOR_MOD = $mysqli->escape_String($_LD_CALOR_MOD);
		$sql_LD_ELETR_PLAN = $mysqli->escape_String($_LD_ELETR_PLAN);
		$sql_LD_ELETR_MOD = $mysqli->escape_String($_LD_ELETR_MOD);
		$sql_LD_EXPL_PLAN = $mysqli->escape_String($_LD_EXPL_PLAN);
		$sql_LD_EXPL_MOD = $mysqli->escape_String($_LD_EXPL_MOD);
		$sql_LD_INFL_PLAN = $mysqli->escape_String($_LD_INFL_PLAN);
		$sql_LD_INFL_MOD = $mysqli->escape_String($_LD_INFL_MOD);
		$sql_LD_PART_PLAN = $mysqli->escape_String($_LD_PART_PLAN);
		$sql_LD_PART_MOD = $mysqli->escape_String($_LD_PART_MOD);
		$sql_LD_POEI_PLAN = $mysqli->escape_String($_LD_POEI_PLAN);
		$sql_LD_POEI_MOD = $mysqli->escape_String($_LD_POEI_MOD);
		$sql_LD_RAD_PLAN = $mysqli->escape_String($_LD_RAD_PLAN);
		$sql_LD_RAD_MOD = $mysqli->escape_String($_LD_RAD_MOD);
		$sql_LD_RUI_PLAN = $mysqli->escape_String($_LD_RUI_PLAN);
		$sql_LD_RUI_MOD = $mysqli->escape_String($_LD_RUI_MOD);
		$sql_LD_VAP_PLAN = $mysqli->escape_String($_LD_VAP_PLAN);
		$sql_LD_VAP_MOD = $mysqli->escape_String($_LD_VAP_MOD);
		$sql_LD_VBRVCI_PLAN = $mysqli->escape_String($_LD_VBRVCI_PLAN);
		$sql_LD_VBRVCI_MOD = $mysqli->escape_String($_LD_VBRVCI_MOD);
		$sql_LD_VBRVMB_PLAN = $mysqli->escape_String($_LD_VBRVMB_PLAN);
		$sql_LD_VBRVMB_MOD = $mysqli->escape_String($_LD_VBRVMB_MOD);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idEMISSAO);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Verifica se o registro editado ja existe
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id
       FROM `EMISSAO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` != ?"
		)) 
		{
			$stmt->bind_param('sd', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_JA_EXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Efetua Atualizacao
		if ($stmt = $mysqli->prepare(
		"UPDATE `EMISSAO` SET 
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `ano` = ?
		        ,`mes` = ?
		        ,`cliente` = ?
		        ,`ld_bio_plan` = ?
		        ,`ld_bio_mod` = ?
		        ,`ld_calor_plan` = ?
		        ,`ld_calor_mod` = ?
		        ,`ld_eletr_plan` = ?
		        ,`ld_eletr_mod` = ?
		        ,`ld_expl_plan` = ?
		        ,`ld_expl_mod` = ?
		        ,`ld_infl_plan` = ?
		        ,`ld_infl_mod` = ?
		        ,`ld_part_plan` = ?
		        ,`ld_part_mod` = ?
		        ,`ld_poei_plan` = ?
		        ,`ld_poei_mod` = ?
		        ,`ld_rad_plan` = ?
		        ,`ld_rad_mod` = ?
		        ,`ld_rui_plan` = ?
		        ,`ld_rui_mod` = ?
		        ,`ld_vap_plan` = ?
		        ,`ld_vap_mod` = ?
		        ,`ld_vbrvci_plan` = ?
		        ,`ld_vbrvci_mod` = ?
		        ,`ld_vbrvmb_plan` = ?
		        ,`ld_vbrvmb_mod` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idEMISSAO` = ?"
		)) 
		{
			$stmt->bind_param('ssddddddddddddddddddddddddddss', $sql_SID_USERNAME, $sql_ANO, $sql_MES, $sql_CLIENTE, $sql_LD_BIO_PLAN, $sql_LD_BIO_MOD, $sql_LD_CALOR_PLAN, $sql_LD_CALOR_MOD, $sql_LD_ELETR_PLAN, $sql_LD_ELETR_MOD, $sql_LD_EXPL_PLAN, $sql_LD_EXPL_MOD, $sql_LD_INFL_PLAN, $sql_LD_INFL_MOD, $sql_LD_PART_PLAN, $sql_LD_PART_MOD, $sql_LD_POEI_PLAN, $sql_LD_POEI_MOD, $sql_LD_RAD_PLAN, $sql_LD_RAD_MOD, $sql_LD_RUI_PLAN, $sql_LD_RUI_MOD, $sql_LD_VAP_PLAN, $sql_LD_VAP_MOD, $sql_LD_VBRVCI_PLAN, $sql_LD_VBRVCI_MOD, $sql_LD_VBRVMB_PLAN, $sql_LD_VBRVMB_MOD, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente indicado
		
		$_QUERY = "DELETE FROM `EMISSAO` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idEMISSAO` IN (".$sql_REG_IDS.")";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id, AA.`ano` as ano, AA.`mes` as mes, AA.`cliente` as cliente, AA.`ld_bio_plan` as ld_bio_plan, AA.`ld_bio_mod` as ld_bio_mod, AA.`ld_calor_plan` as ld_calor_plan, AA.`ld_calor_mod` as ld_calor_mod, AA.`ld_eletr_plan` as ld_eletr_plan, AA.`ld_eletr_mod` as ld_eletr_mod, AA.`ld_expl_plan` as ld_expl_plan, AA.`ld_expl_mod` as ld_expl_mod, AA.`ld_infl_plan` as ld_infl_plan, AA.`ld_infl_mod` as ld_infl_mod, AA.`ld_part_plan` as ld_part_plan, AA.`ld_part_mod` as ld_part_mod, AA.`ld_poei_plan` as ld_poei_plan, AA.`ld_poei_mod` as ld_poei_mod, AA.`ld_rad_plan` as ld_rad_plan, AA.`ld_rad_mod` as ld_rad_mod, AA.`ld_rui_plan` as ld_rui_plan, AA.`ld_rui_mod` as ld_rui_mod, AA.`ld_vap_plan` as ld_vap_plan, AA.`ld_vap_mod` as ld_vap_mod, AA.`ld_vbrvci_plan` as ld_vbrvci_plan, AA.`ld_vbrvci_mod` as ld_vbrvci_mod, AA.`ld_vbrvmb_plan` as ld_vbrvmb_plan, AA.`ld_vbrvmb_mod` as ld_vbrvmb_mod
       FROM `EMISSAO` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_CLIENTE, $o_LD_BIO_PLAN, $o_LD_BIO_MOD, $o_LD_CALOR_PLAN, $o_LD_CALOR_MOD, $o_LD_ELETR_PLAN, $o_LD_ELETR_MOD, $o_LD_EXPL_PLAN, $o_LD_EXPL_MOD, $o_LD_INFL_PLAN, $o_LD_INFL_MOD, $o_LD_PART_PLAN, $o_LD_PART_MOD, $o_LD_POEI_PLAN, $o_LD_POEI_MOD, $o_LD_RAD_PLAN, $o_LD_RAD_MOD, $o_LD_RUI_PLAN, $o_LD_RUI_MOD, $o_LD_VAP_PLAN, $o_LD_VAP_MOD, $o_LD_VBRVCI_PLAN, $o_LD_VBRVCI_MOD, $o_LD_VBRVMB_PLAN, $o_LD_VBRVMB_MOD);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|".$o_ANO."|".$o_MES."|".$o_CLIENTE."|".$o_LD_BIO_PLAN."|".$o_LD_BIO_MOD."|".$o_LD_CALOR_PLAN."|".$o_LD_CALOR_MOD."|".$o_LD_ELETR_PLAN."|".$o_LD_ELETR_MOD."|".$o_LD_EXPL_PLAN."|".$o_LD_EXPL_MOD."|".$o_LD_INFL_PLAN."|".$o_LD_INFL_MOD."|".$o_LD_PART_PLAN."|".$o_LD_PART_MOD."|".$o_LD_POEI_PLAN."|".$o_LD_POEI_MOD."|".$o_LD_RAD_PLAN."|".$o_LD_RAD_MOD."|".$o_LD_RUI_PLAN."|".$o_LD_RUI_MOD."|".$o_LD_VAP_PLAN."|".$o_LD_VAP_MOD."|".$o_LD_VBRVCI_PLAN."|".$o_LD_VBRVCI_MOD."|".$o_LD_VBRVMB_PLAN."|".$o_LD_VBRVMB_MOD."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`idEMISSAO` as id, AA.`ano` as ano, AA.`mes` as mes, upper(CLNT.`nome_interno`) as nome_interno, BLGC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, CLR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, LTRCDD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, XPLSV.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, NFLMVL.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, PRTCLD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, PR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, RDC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, RD.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VPR.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VBRVC.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano, VBRVMB.`planilha_num` as planilha_num, PLNLHMDL.`ano` as ano,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `EMISSAO` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`cliente`
    LEFT JOIN `BIOLOGICO` as BLGC
           ON BLGC.`planilha_num` = AA.`ld_bio_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_bio_mod`
    LEFT JOIN `CALOR` as CLR
           ON CLR.`planilha_num` = AA.`ld_calor_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_calor_mod`
    LEFT JOIN `ELETRICIDADE` as LTRCDD
           ON LTRCDD.`planilha_num` = AA.`ld_eletr_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_eletr_mod`
    LEFT JOIN `EXPLOSIVO` as XPLSV
           ON XPLSV.`planilha_num` = AA.`ld_expl_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_expl_mod`
    LEFT JOIN `INFLAMAVEL` as NFLMVL
           ON NFLMVL.`planilha_num` = AA.`ld_infl_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_infl_mod`
    LEFT JOIN `PARTICULADO` as PRTCLD
           ON PRTCLD.`planilha_num` = AA.`ld_part_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_part_mod`
    LEFT JOIN `POEIRA` as PR
           ON PR.`planilha_num` = AA.`ld_poei_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_poei_mod`
    LEFT JOIN `RADIACAO` as RDC
           ON RDC.`planilha_num` = AA.`ld_rad_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_rad_mod`
    LEFT JOIN `RUIDO` as RD
           ON RD.`planilha_num` = AA.`ld_rui_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_rui_mod`
    LEFT JOIN `VAPOR` as VPR
           ON VPR.`planilha_num` = AA.`ld_vap_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vap_mod`
    LEFT JOIN `VIBR_VCI` as VBRVC
           ON VBRVC.`planilha_num` = AA.`ld_vbrvci_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vbrvci_mod`
    LEFT JOIN `VIBR_VMB` as VBRVMB
           ON VBRVMB.`planilha_num` = AA.`ld_vbrvmb_plan`
    LEFT JOIN `PLANILHA_MODELO` as PLNLHMDL
           ON PLNLHMDL.`idplanilha_modelo` = AA.`ld_vbrvmb_mod`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`idEMISSAO` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_ANO, $o_MES, $o_CLIENTE, $o_LD_BIO_PLAN, $o_LD_BIO_MOD, $o_LD_CALOR_PLAN, $o_LD_CALOR_MOD, $o_LD_ELETR_PLAN, $o_LD_ELETR_MOD, $o_LD_EXPL_PLAN, $o_LD_EXPL_MOD, $o_LD_INFL_PLAN, $o_LD_INFL_MOD, $o_LD_PART_PLAN, $o_LD_PART_MOD, $o_LD_POEI_PLAN, $o_LD_POEI_MOD, $o_LD_RAD_PLAN, $o_LD_RAD_MOD, $o_LD_RUI_PLAN, $o_LD_RUI_MOD, $o_LD_VAP_PLAN, $o_LD_VAP_MOD, $o_LD_VBRVCI_PLAN, $o_LD_VBRVCI_MOD, $o_LD_VBRVMB_PLAN, $o_LD_VBRVMB_MOD, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata select options formats
				if( $o_MES == "1"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JAN; }
				if( $o_MES == "2"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_FEV; }
				if( $o_MES == "3"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_MAR; }
				if( $o_MES == "4"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_ABR; }
				if( $o_MES == "5"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_MAI; }
				if( $o_MES == "6"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JUN; }
				if( $o_MES == "7"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_JUL; }
				if( $o_MES == "8"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_AGO; }
				if( $o_MES == "9"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_SET; }
				if( $o_MES == "10"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_OUT; }
				if( $o_MES == "11"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_NOV; }
				if( $o_MES == "12"){ $o_MES_TXT = TXT_LAUDOS_EMISSAO_DEZ; }
				
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_ANO.':</b></th><td>'.$o_ANO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_MES.':</b></th><td>'.$o_MES_TXT.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_CLIENTE.':</b></th><td>'.$o_CLIENTE.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_N_PLANILHA.':</b></th><td>'.$o_LD_BIO_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_BIOLOGICO_MODELO.':</b></th><td>'.$o_LD_BIO_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_CALOR_N_PLANILHA.':</b></th><td>'.$o_LD_CALOR_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_CALOR_MODELO.':</b></th><td>'.$o_LD_CALOR_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_N_PLANILHA.':</b></th><td>'.$o_LD_ELETR_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_ELETRICIDADE_MODELO.':</b></th><td>'.$o_LD_ELETR_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_N_PLANILHA.':</b></th><td>'.$o_LD_EXPL_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_EXPLOSIVO_MODELO.':</b></th><td>'.$o_LD_EXPL_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_N_PLANILHA.':</b></th><td>'.$o_LD_INFL_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_INFLAMAVEL_MODELO.':</b></th><td>'.$o_LD_INFL_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_N_PLANILHA.':</b></th><td>'.$o_LD_PART_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_PARTICULADO_MODELO.':</b></th><td>'.$o_LD_PART_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_N_PLANILHA.':</b></th><td>'.$o_LD_POEI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_POEIRA_MODELO.':</b></th><td>'.$o_LD_POEI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_N_PLANILHA.':</b></th><td>'.$o_LD_RAD_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RADIACAO_IONIZANTE_MODELO.':</b></th><td>'.$o_LD_RAD_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_N_PLANILHA.':</b></th><td>'.$o_LD_RUI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_RUIDO_MODELO.':</b></th><td>'.$o_LD_RUI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_N_PLANILHA.':</b></th><td>'.$o_LD_VAP_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VAPOR_MODELO.':</b></th><td>'.$o_LD_VAP_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_N_PLANILHA.':</b></th><td>'.$o_LD_VBRVCI_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VCI_MODELO.':</b></th><td>'.$o_LD_VBRVCI_MOD.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_N_PLANILHA.':</b></th><td>'.$o_LD_VBRVMB_PLAN.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_LAUDOS_EMISSAO_LAUDO_VIBRACAO_VMB_MODELO.':</b></th><td>'.$o_LD_VBRVMB_MOD.'</td></tr>';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
