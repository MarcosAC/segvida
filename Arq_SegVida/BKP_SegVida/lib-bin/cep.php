<?php 
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: cep.php
## Função..........: 1. Carrega os dados do cep informado direto dos correios
##                   2. Retorna os dados padronizados: uf, cidade, bairro, logradoudo
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once "geral/language.php";
	include('includes/phpQuery-onefile.php');
		
	function simple_curl($url,$post=array(),$get=array())
	{
		$url = explode('?',$url,2);
		if(count($url) === 2)
		{
			$temp_get = array();
			parse_str($url[1],$temp_get);
			$get = array_merge($get,$temp_get);
		}
	
		$ch = curl_init($url[0]."?".http_build_query($get));
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return curl_exec ($ch);
	}
	
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_CEP = test_input($_GET["cep"]);
*/
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_CEP = test_input($_POST["cep"]);
		
		
		$_DEBUG = 0;
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error||||"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		
		
		$sql_CEP = $mysqli->escape_string($IN_CEP);
		
		######
		# 1 - Tenta obter o CEP do repositorio do site
		######
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT C.`cep` as cep, 
          upper(C.`uf`) as uf,
          C.`cidade_codigo` as cidade_codigo,
          upper(M.`cidade`) as cidade,
          upper(C.`bairro`) as bairro,
          upper(C.`logradouro`) as logradouro
     FROM `CEP` as C
LEFT JOIN `MUNICIPIO_BR` M
       ON M.`codigo_ibge` = C.`cidade_codigo`
    WHERE C.`cep` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_CEP);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CEP, $o_UF, $o_CIDADE_CODIGO, $o_CIDADE, $o_BAIRRO, $o_LOGR);
			$stmt->fetch();
		}
		else
		{
			die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
			exit;
		}
		
		
		if($o_CEP)
		{
			//status|uf|cidade|bairro|logradouro|codigo_ibge|
			die("1|".$o_UF."|".$o_CIDADE."|".$o_BAIRRO."|".$o_LOGR."|".$o_CIDADE_CODIGO."|");
			exit;
		}
		
		
		
		
		
		######
		# 2 - Tenta obter o CEP do servico dos correios
		######
		
		/*
		$html = simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do',array(
			'cepEntrada'=>$IN_CEP,
			'tipoCep'=>'',
			'cepTemp'=>'',
			'metodo'=>'buscarCep'
		));
		
		//http://m.correios.com.br/movel/buscaCepConfirma.do?cepEntrada=36570000&tipoCep=&cepTemp=&metodo=buscarCep
		
		phpQuery::newDocumentHTML($html, $charset = 'utf-8');
		
		$dados = 
		array(
			'logradouro'=> trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
			'bairro'=> trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html()),
			'cidade/uf'=> trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
			'cep'=> trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
		);
		
		$dados['cidade/uf'] = explode('/',$dados['cidade/uf']);
		$dados['cidade'] = trim($dados['cidade/uf'][0]);
		$dados['uf'] = trim($dados['cidade/uf'][1]);
		unset($dados['cidade/uf']);
		
		//die(json_encode($dados));
		*/
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, 'https://viacep.com.br/ws/'.$IN_CEP.'/json/');
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$output = curl_exec($ch);
		
		$DATA = json_decode($output);
		$DATA = (array) $DATA;
		
		//error_log("chk_cep_rlg2.php: \n\noutput -> $output\n\ncidade -> ".$DATA['localidade']."\n\n",0);
		
		$dados = 
		array(
			'cep'         => $DATA["cep"],
			'logradouro'  => $DATA["logradouro"],
			'complemento' => $DATA["complemento"],
			'bairro'      => $DATA["bairro"],
			'cidade'      => $DATA["localidade"],
			'uf'          => $DATA["uf"],
			'unidade'     => $DATA["unidade"],
			'ibge'        => $DATA["ibge"],
			'gia'         => $DATA["gia"]
		);
		
		//Remove acentos
		$dados['cidade'] = strtoupper(remove_accent($dados['cidade']));
		
		$o_CORREIO_UF     = $dados['uf'];
		$o_CORREIO_CIDADE = $dados['cidade'];
		$o_CORREIO_BAIRRO = $dados['bairro'];
		$o_CORREIO_LOGR   = $dados['logradouro'];
		
		if( (isEmpty($o_CORREIO_UF) && isEmpty($o_CORREIO_CIDADE)) ||
		    (strlen($o_CORREIO_UF) < 2 && strlen($o_CORREIO_CIDADE) < 2)
		)
		{
			die("0|Incapaz de comunicar com o computador dos correios. Serviço temporariamente indisponível!|error||||");
			exit;
		}
		
		####
		# Descobre o codigo ibge da cidade
		####
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT M.`codigo_ibge` as codigo
       FROM `MUNICIPIO_BR` M
      WHERE M.`uf` = ?
        AND upper(M.`cidade`) = upper(?)"
		)) 
		{
			$sql_UF     = $mysqli->escape_string($dados['uf']);
			$sql_CIDADE = $mysqli->escape_string($dados['cidade']);
			//
			$stmt->bind_param('ss', $sql_UF,$sql_CIDADE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CODIGO);
			$stmt->fetch();
		}
		else
		{
			die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
			exit;
		}
		
		
		
		
		######
		# Se conseguiu dados do servidor dos correios, atualiza repositorio
		######
		
		$sql_UF = $mysqli->escape_string($o_CORREIO_UF);
		$sql_CIDADE_CODIGO = $mysqli->escape_string($o_CODIGO);
		$sql_BAIRRO = $mysqli->escape_string($o_CORREIO_BAIRRO);
		$sql_LOGR = $mysqli->escape_string($o_CORREIO_LOGR);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `CEP` 
		    SET `cad_date` = now(),
		        `cad_username` = 'SYSTEM',
		        `cep` = ?,
		        `uf` = ?,
		        `cidade_codigo` = ?,
		        `bairro` = ?,
		        `logradouro` = ?
"		)) 
		{
			$stmt->bind_param('sssss', $sql_CEP, $sql_UF, $sql_CIDADE_CODIGO, $sql_BAIRRO, $sql_LOGR);
			$stmt->execute();
		}
		else
		{
			die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
			exit;
		}
		
		
		
		
		//status|uf|cidade|bairro|logradouro|codigo_ibge|
		die("1|".$o_CORREIO_UF."|".$o_CORREIO_CIDADE."|".$o_CORREIO_BAIRRO."|".$o_CORREIO_LOGR."|".$o_CODIGO."|");
		
	}
	#Se nao for enviado via POST, nao mostra nada, apenas uma tela em branco.
	else
	{
		die("0||||||");
	}
	
#################################################################################
###########
#####
##
?>