<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: mdl-cfg-colabs.php
## Função..........: MODEL - Cadastro, select, delete, upload, datatable refresh;
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_USERNAME = $IN_CPF = $IN_SIT = "";
	
	## DEBUG
	#$_DEBUG=1;
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["l"]);
		$IN_DBO        = test_input($_GET["dbo"]);
		$IN_REG_ID     = test_input($_GET["rid"]);
		$IN_USERNAME   = test_input($_GET["username"]);
		$IN_CPF        = test_input($_GET["cpf"]);
		$IN_SIT        = test_input($_GET["sit"]);
		
		$IN_ACESSO_COUNT = test_input($_GET["act"]);
		
		$IN_ACESSO_OBJ  = array();
		
		for($i=0; $i<=$IN_ACESSO_COUNT; $i++)
		{
			$IN_ACESSO_CODIGO  = test_input($_GET["cdg_".$i]);
			$IN_ACESSO_ACESSAR = test_input($_GET["acs_".$i]);
			$IN_ACESSO_EDITAR  = test_input($_GET["edt_".$i]);
			$IN_ACESSO_APAGAR  = test_input($_GET["apg_".$i]);
			if($IN_ACESSO_CODIGO)
			{
				$IN_ACESSO_OBJ[] = new AcessoObj($IN_ACESSO_CODIGO, $IN_ACESSO_ACESSAR, $IN_ACESSO_EDITAR, $IN_ACESSO_APAGAR);
			}
		}
*/		
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_DBO        = test_input($_POST["dbo"]);
		$IN_REG_ID     = test_input($_POST["rid"]);
		$IN_REG_IDS    = test_input($_POST["rids"]);
		$IN_USERNAME   = test_input($_POST["username"]);
		$IN_CPF        = test_input($_POST["cpf"]);
		$IN_SIT        = test_input($_POST["sit"]);
		
		$IN_ACESSO_COUNT = test_input($_POST["act"]);
		
		$IN_ACESSO_OBJ  = array();
		
		for($i=0; $i<=$IN_ACESSO_COUNT; $i++)
		{
			$IN_ACESSO_CODIGO  = test_input($_POST["cdg_".$i]);
			$IN_ACESSO_ACESSAR = test_input($_POST["acs_".$i]);
			$IN_ACESSO_EDITAR  = test_input($_POST["edt_".$i]);
			$IN_ACESSO_APAGAR  = test_input($_POST["apg_".$i]);
			if($IN_ACESSO_CODIGO)
			{
				$IN_ACESSO_OBJ[] = new AcessoObj($IN_ACESSO_CODIGO, $IN_ACESSO_ACESSAR, $IN_ACESSO_EDITAR, $IN_ACESSO_APAGAR);
			}
		}
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false)
		{
			die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|");
			exit;
		}
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) ||
		   isEmpty($sid_USERNAME) ||
		   isEmpty($sid_PLANO_CODIGO))
		{
			die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|");
			exit;
		}
		
		//$IN_USERNAME = $_SESSION['user_username'];
		//$IN_REG_ID   = $_SESSION['user_id'];
		
		
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if(!isEmpty($IN_REG_ID) || $IN_REG_ID > 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## uppercase
				mb_strtoupper($IN_USERNAME,"UTF-8");
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_USERNAME, $IN_CPF, $IN_SIT);
				die($tmp_RESULT);
				exit;
				break;
			## INSERT
			case '2':
				if( isEmpty($IN_DBO) ||
						isEmpty($IN_USERNAME) ||
						isEmpty($IN_CPF) ||
						isEmpty($IN_SIT) 
					) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if(!isEmpty($IN_REG_ID) || $IN_REG_ID > 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## uppercase
				mb_strtoupper($IN_USERNAME,"UTF-8");
				## Executa
				$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $sid_PLANO_CODIGO, $IN_USERNAME, $IN_CPF, $IN_SIT, $IN_ACESSO_OBJ);
				die($tmp_RESULT);
				exit;
				break;
			## UPDATE
			case '3':
				if( isEmpty($IN_DBO) ||
						isEmpty($IN_USERNAME) ||
						isEmpty($IN_CPF) ||
						isEmpty($IN_SIT) ||
						isEmpty($IN_REG_ID)
					) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if(isEmpty($IN_REG_ID) || $IN_REG_ID <= 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## uppercase
				mb_strtoupper($IN_USERNAME,"UTF-8");
				## Executa
				$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID, $IN_USERNAME, $IN_CPF, $IN_SIT, $IN_ACESSO_OBJ);
				die($tmp_RESULT);
				exit;
				break;
			## DELETE
			case '4':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_IDS) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if( isEmpty($IN_REG_IDS) || $IN_REG_IDS <= 0 ) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_IDS);
				die($tmp_RESULT);
				exit;
				break;
			## FILE UPLOAD
			case '5':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if( isEmpty($IN_REG_ID) || $IN_REG_ID <= 0 ) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_UPLOAD($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## EDIT
			case '6':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if( isEmpty($IN_REG_ID) || $IN_REG_ID <= 0 ) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## uppercase
				mb_strtoupper($IN_USERNAME,"UTF-8");
				## Executa
				$tmp_RESULT = executa_EDIT($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_USERNAME, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## FICHA CONSOLIDADA
			case '7':
				if( isEmpty($IN_DBO) && isEmpty($IN_REG_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if( isEmpty($IN_REG_ID) || $IN_REG_ID <= 0 ) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_FICHA($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_REG_ID);
				die($tmp_RESULT);
				exit;
				break;
			## DATATABLE
			case '99':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				//if( isEmpty($IN_REG_ID) || $IN_REG_ID <= 0 ) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_DATATABLE($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $sid_PLANO_CODIGO);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# ACESSO_OBJECT
	####
	/*
	class AcessoObj
	{
		public $codigo  = "";
		public $acessar = "";
		public $editar  = "";
		public $apagar  = "";
		
		function __construct( $codigo, $acessar, $editar, $apagar) 
		{ 
			$this->codigo  = $codigo;
			$this->acessar = $acessar;
			$this->editar  = $editar;
			$this->apagar  = $apagar;
		}
		
		public function setCODIGO($_tmp){ $this->codigo = $_tmp; }
		public function getCODIGO(){ return $this->codigo; }
		
		public function setACESSAR($_tmp){ $this->acessar = $_tmp; }
		public function getACESSAR(){ return $this->acessar; }
		
		public function setEDITAR($_tmp){ $this->editar = $_tmp; }
		public function getEDITAR(){ return $this->editar; }
		
		public function setAPAGAR($_tmp){ $this->apagar = $_tmp; }
		public function getAPAGAR(){ return $this->apagar; }
	}
	*/
	
	
	####
	# DATATABLE INTEGRATION
	####
	function executa_DATATABLE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_PLANO_CODIGO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		//$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		//$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		//$sql_CPF                  = $mysqli->escape_String($_CPF);
		//$sql_SIT                  = $mysqli->escape_String($_SIT);
		//$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		//$WHERE = "";
		//$WHERE .= "";
		
		//
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT CB.`idCOLABORADOR` as id,
            CB.`username` as username, 
            concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome, 
            date_format(CB.`cad_date`,?) as cad_date,
            date_format(U.`dth_ult_acesso`,?) as ult_acesso,
            CB.`situacao` as sit
       FROM `COLABORADOR` CB
 INNER JOIN `SYSTEM_USER_ACCOUNT` U
         ON U.`username` = CB.`username`
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = CB.`idSYSTEM_CLIENTE`
      WHERE CB.`idSYSTEM_CLIENTE` = ?
   ORDER BY CB.`username`"
		)) 
		{
			$stmt->bind_param('sss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_USERNAME, $o_NOME, $o_CAD_DATE, $o_ULT_ACESSO, $o_SIT);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$TBODY_LIST = "";
			}
			else
			{
				$TBODY_LIST = ""; $C=0;
				while($stmt->fetch())
				{
					# Formata situacao
					if($o_SIT == 'A'){ $SIT_TXT = '<div class="grid_color_green">Ativo</div>'; }
					            else { $SIT_TXT = '<div class="grid_color_red">Inativo</div>'; }
					//
					$TBODY_LIST .= '<tr>';
					$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
					$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
					$TBODY_LIST .= '<td>'.$o_USERNAME.'</td>';
					$TBODY_LIST .= '<td>'.$o_NOME.'</td>';
					$TBODY_LIST .= '<td>'.$o_CAD_DATE.'</td>';
					$TBODY_LIST .= '<td>'.$o_ULT_ACESSO.'</td>';
					$TBODY_LIST .= '<td>'.$SIT_TXT.'</td>';
					$TBODY_LIST .= '</tr>';
					//
					$C++;
				}
				
			}
			
			$o_TOTAL_COLABS  = $C;
			
			## Carrega limitacoes do plano contratado
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT P.`total_colaboradores` as total
         FROM `SYSTEM_PLANO_PAGTO` P
        WHERE P.`codigo` = ?"
			)) 
			{
				$stmt->bind_param('s', $sql_SID_PLANO_CODIGO);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_LIMITE_COLABS);
				
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$o_LIMITE_COLABS = 0;
				}
				
			}
			
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_COLABS)){ $o_LIMITE_COLABS = 0; }
			if(isEmpty($o_TOTAL_COLABS)){ $o_TOTAL_COLABS = 0; }
			if($o_TOTAL_COLABS >= $o_LIMITE_COLABS){ $IND_EXIBE_ADD_BTN = 0; }
			else { $IND_EXIBE_ADD_BTN = 1; }
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO_COLABS.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			if(isEmpty($C)){ $C=0; }
			
			return "1|".$TBODY_LIST."|success|".$add_btn_link."|".$C."|";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		/* just explaining how to call mysqli_stmt_bind_param with a parameter array */ 
		/*
		$sql_link = mysqli_connect('localhost', 'my_user', 'my_password', 'world'); 
		$type = "isssi"; 
		$param = array("5", "File Description", "File Title", "Original Name", time()); 
		$sql = "INSERT INTO file_detail (file_id, file_description, file_title, file_original_name, file_upload_date) VALUES (?, ?, ?, ?, ?)"; 
		$sql_stmt = mysqli_prepare ($sql_link, $sql); 
		call_user_func_array('mysqli_stmt_bind_param', array_merge (array($sql_stmt, $type), $param); 
		mysqli_stmt_execute($sql_stmt); 
		*/
	}
	
	####
	# SELECT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_USERNAME, $_CPF, $_SIT)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		//$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		$sql_CPF                  = $mysqli->escape_String($_CPF);
		$sql_SIT                  = $mysqli->escape_String($_SIT);
		//$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		$WHERE = "";
		$WHERE .= "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT C.`username` as username
       FROM `COLABORADOR` C
      WHERE C.`idSYSTEM_CLIENTE` = ?
        AND upper(U.`username`) = upper(?)"
		)) 
		{
			$sql_idSYSTEM_CLIENTE_ID = $mysqli->escape_string($TMP_idSYSTEM_CLIENTE_ID);
			$sql_USERNAME            = $mysqli->escape_string($IN_USERNAME);
			//
			$stmt->bind_param('ss', $sql_idSYSTEM_CLIENTE_ID, $sql_USERNAME);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_USERNAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_CFG_COLABS_USUARIO_EXISTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_SID_PLANO_CODIGO, $_USERNAME, $_CPF, $_SIT, array $_ACESSO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME) ||
		   isEmpty($_SID_PLANO_CODIGO))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		$sql_CPF                  = $mysqli->escape_String($_CPF);
		$sql_SIT                  = $mysqli->escape_String($_SIT);
		
		## Verifica se atingiu a limitacao do plano
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT P.`total_colaboradores` as total,
            (SELECT count(1) 
               FROM `COLABORADOR` CB 
              WHERE CB.`idSYSTEM_CLIENTE` = ?) as total_colabs
       FROM `SYSTEM_PLANO_PAGTO` P
      WHERE P.`codigo` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_PLANO_CODIGO);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_LIMITE_COLABS,$o_TOTAL_COLABS);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_INCAPAZ_CARREGAR_DADOS_PLANO."|error|";
				exit;
			}
			
			if(isEmpty($o_TOTAL_COLABS)) { $o_TOTAL_COLABS=0; }
			if(isEmpty($o_LIMITE_COLABS)){ $o_LIMITE_COLABS=0; }
			
			if($o_TOTAL_COLABS >= $o_LIMITE_COLABS)
			{
				return "0|".TXT_LIMITE_PLANO_ATINGIDO_COLABS."|alert|";
				exit;
			}
			
		}
		
		## Verifica se o username existe e pode ser configurado como colaborador
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT U.`username` as username
       FROM `SYSTEM_USER_ACCOUNT` U
      WHERE U.`categoria` = 'USR'
        AND upper(U.`username`) = upper(?)
        AND U.`cpf` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_USERNAME, $sql_CPF);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_USERNAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_CFG_COLABS_USUARIO_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Verifica se o usuario já está cadastrado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT C.`username` as username
       FROM `COLABORADOR` C
      WHERE C.`idSYSTEM_CLIENTE` = ?
        AND upper(C.`username`) = upper(?)"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_USERNAME);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_USERNAME);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows != 0) 
			{
				return "0|".TXT_CFG_COLABS_USUARIO_EXISTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Usuario nao e um colaborador registrado, efetua o registro
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `COLABORADOR` 
		    SET `idSYSTEM_CLIENTE` = ?,
		        `cad_username` = ?,
		        `cad_date` = now(),
		        `updt_username` = ?,
		        `updt_date` = now(),
		        `username` = ?,
		        `situacao` = ?"
		)) 
		{
			$stmt->bind_param('sssss', $sql_SID_idSYSTEM_CLIENTE, $sql_SID_USERNAME, $sql_SID_USERNAME, $sql_USERNAME, $sql_SIT );
			
			if($stmt->execute())
			{
				# Obtem idCOLABORADOR do registro inserido
				if ($stmt = $mysqli->prepare(
				"SELECT C.`idCOLABORADOR` as id
           FROM `COLABORADOR` C
          WHERE C.`idSYSTEM_CLIENTE` = ?
            AND C.`username` = ?
          LIMIT 1"
				)) 
				{
					$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_USERNAME);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_idCOLABORADOR);
					$stmt->fetch();
					
					##Se nao encontrou dados, retorna
					//if ($stmt->num_rows == 0) 
					//{
					//	return "0|".TXT_CFG_COLABS_USUARIO_INEXISTENTE."|alert|";
					//	exit; 
					//}
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				# Insere nivel de acesso do colaborador
				if($o_idCOLABORADOR)
				{
					$sql_idCOLABORADOR = $mysqli->escape_String($o_idCOLABORADOR);
					
					$count = count($_ACESSO);
					for($i=0; $i<$count;$i++)
					{
						$sql_M_CODIGO  = $mysqli->escape_String($_ACESSO[$i]->getCODIGO());
						$sql_M_ACESSAR = $mysqli->escape_String($_ACESSO[$i]->getACESSAR());
						$sql_M_EDITAR  = $mysqli->escape_String($_ACESSO[$i]->getEDITAR());
						$sql_M_APAGAR  = $mysqli->escape_String($_ACESSO[$i]->getAPAGAR());
						
						# Efetua insert
						if ($stmt = $mysqli->prepare("INSERT INTO `COLABORADOR_ACESSO` SET
            `idSYSTEM_CLIENTE` = ?, 
            `idCOLABORADOR` = ?, 
            `cad_username` = ?, 
            `cad_date` = now(), 
            `modulo_codigo` = ?, 
            `ind_acessar` = ?, 
            `ind_editar` = ?, 
            `ind_apagar` = ?")) 
						{
							$stmt->bind_param('sssssss', $sql_SID_idSYSTEM_CLIENTE, $sql_idCOLABORADOR, $sql_SID_USERNAME, $sql_M_CODIGO, $sql_M_ACESSAR, $sql_M_EDITAR, $sql_M_APAGAR );
							$stmt->execute();
						}
						else
						{
							if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
							      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
							exit;
						}
						
					}//endfor
					
				}//endif
				
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID, $_USERNAME, $_CPF, $_SIT, array $_ACESSO)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		//$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		$sql_CPF                  = $mysqli->escape_String($_CPF);
		$sql_SIT                  = $mysqli->escape_String($_SIT);
		
		## Verifica se o registro do ID informado existe e se é do cliente system indicado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT C.`idCOLABORADOR` as id
       FROM `COLABORADOR` C
      WHERE C.`idSYSTEM_CLIENTE` = ?
        AND C.`idCOLABORADOR` = ?"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idCOLABORADOR);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_UPDATE_ID_INEXISTENTE."|alert|";
				exit; 
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
		## Registro existe, efetua atualização
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `COLABORADOR`
		    SET `updt_username` = ?,
		        `updt_date` = now(),
		        `username` = ?,
		        `situacao` = ?
		  WHERE `idSYSTEM_CLIENTE` = ?
		    AND `idCOLABORADOR` = ?"
		)) 
		{
			$stmt->bind_param('sssss', $sql_SID_USERNAME, $sql_USERNAME, $sql_SIT, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID );
			
			if($stmt->execute())
			{
				# Atualiza nivel de acesso do colaborador
				if($o_idCOLABORADOR)
				{
					$sql_idCOLABORADOR = $mysqli->escape_String($o_idCOLABORADOR);
					
					# Apaga registros de nivel de acesso para reinsercao da nova matriz
					
					# Executa SQL
					if ($stmt = $mysqli->prepare(
					"DELETE FROM `COLABORADOR_ACESSO` 
           WHERE `idSYSTEM_CLIENTE` = ? 
             AND `idCOLABORADOR` = ?")) 
					{
						$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
						$stmt->execute();
					}
					else
					{
						if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
						      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
						exit;
					}
					
					# Insere nova matriz
					
					$count = count($_ACESSO);
					for($i=0; $i<$count;$i++)
					{
						$sql_M_CODIGO  = $mysqli->escape_String($_ACESSO[$i]->getCODIGO());
						$sql_M_ACESSAR = $mysqli->escape_String($_ACESSO[$i]->getACESSAR());
						$sql_M_EDITAR  = $mysqli->escape_String($_ACESSO[$i]->getEDITAR());
						$sql_M_APAGAR  = $mysqli->escape_String($_ACESSO[$i]->getAPAGAR());
						
						# Efetua insert
						if ($stmt = $mysqli->prepare("INSERT INTO `COLABORADOR_ACESSO` SET
            `idSYSTEM_CLIENTE` = ?, 
            `idCOLABORADOR` = ?, 
            `cad_username` = ?, 
            `cad_date` = now(), 
            `modulo_codigo` = ?, 
            `ind_acessar` = ?, 
            `ind_editar` = ?, 
            `ind_apagar` = ?")) 
						{
							$stmt->bind_param('sssssss', $sql_SID_idSYSTEM_CLIENTE, $sql_idCOLABORADOR, $sql_SID_USERNAME, $sql_M_CODIGO, $sql_M_ACESSAR, $sql_M_EDITAR, $sql_M_APAGAR );
							$stmt->execute();
						}
						else
						{
							if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
							      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
							exit;
						}
						
					}//endfor
					
				}//endif
				
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_IDS)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		//$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		//$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		//$sql_CPF                  = $mysqli->escape_String($_CPF);
		//$sql_SIT                  = $mysqli->escape_String($_SIT);
		$sql_REG_IDS              = $mysqli->escape_String($_REG_IDS);
		
		## Verifica se o registro do ID informado existe e se é do cliente system indicado
		
		$_QUERY = "DELETE FROM `COLABORADOR` 
		  WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
		    AND `idCOLABORADOR` IN (".$sql_REG_IDS.")";
		
		//return "1|".$_QUERY."|success|";
		//exit;
		
		##Prepara query
		if ($stmt = $mysqli->prepare($_QUERY)) 
		{
			//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_IDS);
			
			if($stmt->execute())
			{
				# Apaga registros de nivel de acesso para reinsercao da nova matriz
				
				$QUERY = "DELETE FROM `COLABORADOR_ACESSO` 
         WHERE `idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE." 
           AND `idCOLABORADOR` IN (".$sql_REG_IDS.")";
				
				# Executa SQL
				if ($stmt = $mysqli->prepare($QUERY)) 
				{
					//$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
					$stmt->execute();
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPLOAD
	####
	function executa_UPLOAD($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		#Extensoes permitidas para a foto
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (
				(($_FILES["file"]["type"] == "image/gif")   || 
				 ($_FILES["file"]["type"] == "image/jpeg")  || 
				 ($_FILES["file"]["type"] == "image/jpg")   || 
				 ($_FILES["file"]["type"] == "image/pjpeg") || 
				 ($_FILES["file"]["type"] == "image/x-png") || 
				 ($_FILES["file"]["type"] == "image/png")) && 
				 ($_FILES["file"]["size"] < 80000) &&
				 in_array($extension, $allowedExts)
			) 
		{
			if ($_FILES["file"]["error"] > 0) 
			{
				//return "Return Code: " . $_FILES["file"]["error"] . "<br>";
				return "0|".$_FILES["file"]["error"]."|error|";
				exit;
			}
			else
			{
				//$nome_foto = md5(uniqid(time())).$extension; //nome que dará a imagem
				//$filename = $_FILES["file"]["name"];
				//echo "Upload: " . $_FILES["file"]["name"] . "<br>";
				//echo "Type: " . $_FILES["file"]["type"] . "<br>";
				//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
				//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
				
				
				## Carrega nome existente da foto de perfil do usuario
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT U.`profile_pic` as foto
           FROM `SYSTEM_USER_ACCOUNT` U
          WHERE U.`idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_REG_ID    = $mysqli->escape_String($_REG_ID);
					$stmt->bind_param('s', $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_NOME_FOTO_ATUAL);
					
					$stmt->fetch();
					
				}
				else
				{
					return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|";
					exit;
				}
				
				
				## Gera nome unico para o arquivo e
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(PROFILE_PICS_PATH."/" . $filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["file"]["tmp_name"],PROFILE_PICS_PATH."/" . $filename);
				
				## Atualiza base de dados com o nome novo da foto
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"UPDATE `SYSTEM_USER_ACCOUNT`
				    SET  `profile_pic` = ?
				  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_PROFILE_PIC = $mysqli->escape_String($filename);
					$sql_REG_ID      = $mysqli->escape_String($_REG_ID);
					//
					$stmt->bind_param('ss', $sql_PROFILE_PIC, $sql_REG_ID);
					
					if($stmt->execute())
					{
						## Apaga foto antiga do repositorio
						if( !isEmpty($o_NOME_FOTO_ATUAL) && 
						    ($o_NOME_FOTO_ATUAL != "no_profile_pic.jpg") && 
						    file_exists(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL)
						  )
						{ unlink(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL); }
						
						## Retorna OK com o nome da foto nova
						return "1|".TXT_MOD_PERFIL_FOTO_ATUALIZADA."|success|".$filename."|";
						exit;
					}
					else
					{
						return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
						exit;
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				//if (file_exists("uploads/" . $filename)) 
				//{
				//	//echo $filename . " already exists. ";
				//	return "0|".$filename . " already exists. (".$nome_atual.")(".$_REG_ID.")|error|";
				//	exit;
				//} 
				//else 
				//{
				//	move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $filename);
				//	//echo "Stored in: " . "uploads/" . $filename;
				//	return "1|"."Stored in: " . "uploads/" . $filename."(".$nome_atual.")|success|";
				//	exit;
				//}
			}
		}
		else
		{
			//echo "Invalid file";
			return "0|".TXT_UPLOAD_ARQUIVO_INVALIDO."|error|";
			exit;
		}
		
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		//$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		//$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		//$sql_CPF                  = $mysqli->escape_String($_CPF);
		//$sql_SIT                  = $mysqli->escape_String($_SIT);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT upper(CB.`username`) as username,
            U.`cpf` as cpf,
            upper(CB.`situacao`) as sit
       FROM `COLABORADOR` CB
 INNER JOIN `SYSTEM_USER_ACCOUNT` U
         ON U.`username` = CB.`username`
      WHERE CB.`idSYSTEM_CLIENTE` = ?
        AND CB.`idCOLABORADOR` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_USERNAME, $o_CPF, $o_SIT);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_CFG_COLABS_USUARIO_EXISTE."|alert||||";
				exit;
			}
			else
			{
				# Gerar matriz de acesso para javascript array
				
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT lower(CA.`modulo_codigo`), CA.`ind_acessar`, CA.`ind_editar`, CA.`ind_apagar`
           FROM `COLABORADOR_ACESSO` CA
          WHERE CA.`idSYSTEM_CLIENTE` = ?
            AND CA.`idCOLABORADOR` = ?"
				)) 
				{
					$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_A_CODIGO, $o_A_ACESSAR, $o_A_EDITAR, $o_A_APAGAR);
					
					##Se nao encontrou dados, retorna
					if ($stmt->num_rows == 0) 
					{
						$o_ACESSO_MATRIX = "";
					}
					else
					{
						while($stmt->fetch())
						{
							$CAP=""; if($o_ACESSO_MATRIX){ $CAP=","; }
							$o_ACESSO_MATRIX .= $CAP.'{"codigo":"'.$o_A_CODIGO.'", "acessar":"'.$o_A_ACESSAR.'", "editar":"'.$o_A_EDITAR.'", "apagar":"'.$o_A_APAGAR.'"}';
						}
						$o_ACESSO_MATRIX = "[".$o_ACESSO_MATRIX."]";
					}
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				return "1||success|".$o_USERNAME."|".$o_CPF."|".$o_SIT."|".$o_ACESSO_MATRIX."|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		global $IN_LANG;
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		//$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		//$sql_SID_PLANO_CODIGO     = $mysqli->escape_String($_SID_PLANO_CODIGO);
		//$sql_USERNAME             = $mysqli->escape_String($_USERNAME);
		//$sql_CPF                  = $mysqli->escape_String($_CPF);
		//$sql_SIT                  = $mysqli->escape_String($_SIT);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		//$WHERE = "";
		//$WHERE .= "";
		
		//
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT CB.`idCOLABORADOR` as id, 
            concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome,
            upper(U.`username`) as username,
            lower(U.`email`) as email,
            U.`fone_fixo1` as fonefixo1,
            U.`celular1` as celular1,
            upper(U.`uf`) as uf,
            upper(U.`cep`) as cep,
            upper(M.`cidade`) as cidade,
            upper(U.`bairro`) as bairro,
            upper(U.`end`) as end,
            upper(U.`numero`) as num,
            upper(U.`compl`) as compl,
            U.`cpf` as cpf,
            upper(CB.`situacao`) as sit,
            date_Format(CB.`cad_date`,?) as cad_date,
            lower(CB.`cad_username`) as cad_username,
            date_Format(U.`dth_ult_acesso`,?) as ult_acesso,
            U.`ip_ult_acesso` as ip_ult_acesso,
            U.`profile_pic` as pic
       FROM `COLABORADOR` CB
 INNER JOIN `SYSTEM_USER_ACCOUNT` U
         ON U.`username` = CB.`username`
 INNER JOIN `MUNICIPIO_BR` M
         ON M.`codigo_ibge` = U.`cidade_codigo`
      WHERE CB.`idSYSTEM_CLIENTE` = ?
        AND CB.`idCOLABORADOR` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_idCOLABORADOR, $o_NOME, $o_USERNAME, $o_EMAIL, $o_FONE1, $o_CEL1, $o_UF, $o_CEP, $o_CIDADE, $o_BAIRRO, $o_END, $o_NUM, $o_COMPL, $o_CPF, $o_SIT, $o_CAD_DATE, $o_CAD_USERAME, $o_ULT_ACESSO, $o_ULT_ACESSO_IP, $o_PROFILE_PIC);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_CFG_COLABS_USUARIO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				# Formata situacao
				if($o_SIT == 'A'){ $SIT_TXT = '<div class="grid_color_green">Ativo</div>'; }
				            else { $SIT_TXT = '<div class="grid_color_red">Inativo</div>'; }
				
				# Formata endereco
				if(!isEmpty($o_END))
				{
					$END = $o_END;
					if(!isEmpty($o_NUM)){ $END .= ", ".$o_NUM; }
					if(!isEmpty($o_COMPL)){ $END .= ", ".$o_COMPL; }
					if(!isEmpty($o_BAIRRO)){ $END .= "<br/>".$o_BAIRRO; }
					if(!isEmpty($o_CIDADE)){ $END .= "<br/>".$o_CIDADE; }
					if(!isEmpty($o_UF)){ $END .= " - ".$o_UF; }
					if(!isEmpty($o_CEP)){ $END .= "<br/>CEP ".$o_CEP; }
				}
				
				# Formata ultimo acesso
				if(!isEmpty($o_ULT_ACESSO))
				{
					$ULT_ACESSO = $o_ULT_ACESSO;
					if($o_ULT_ACESSO_IP){ $ULT_ACESSO .= " <small>(<i>IP ".$o_ULT_ACESSO_IP."</i>)</small>"; }
				}
				
				# Formata foto
				$profile_pic = "no_profile_pic.jpg";
				if( !isEmpty($o_PROFILE_PIC) && file_exists(PROFILE_PICS_PATH.'/'.$o_PROFILE_PIC) )
				{
					$profile_pic = $o_PROFILE_PIC;
				}
				
				if(!$profile_pic){ $profile_pic = "no_profile_pic.jpg"; }
				
				$USER_PROFILE_PIC = '<img src="'.PROFILE_PICS_URL.'/'.$profile_pic.'" alt="..." class="img-circle">';
				
				/* 
				<img id="profile_pic" width=128 height=128 src="<?php echo PROFILE_PICS_URL."/".$o_PROFILE_PIC; ?>" style="border: 1px solid #0000ff;" vspace="9">
				*/
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th class="ficha_pic" rowspan=10>'.$USER_PROFILE_PIC.'</th><th><b>'.TXT_CFG_COLABS_FICHA_NOME.'</b></th><td>'.$o_NOME.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_CPF.'</b></th><td>'.$o_CPF.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_EMAIL.'</b></th><td>'.$o_EMAIL.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_FONE1.'</b></th><td>'.$o_FONE1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_CEL1.'</b></th><td>'.$o_CEL1.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_ENDERECO.'</b></th><td><small>'.$END.'</small></td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_USUARIO.'</b></th><td>'.$o_USERNAME.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_INCLUSAO.'</b></th><td>'.$o_CAD_DATE.' (<i>'.$o_CAD_USERAME.'</i>)</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_ULT_ACESSO.'</b></th><td>'.$ULT_ACESSO.'</td></tr>';
				$FICHA .= '	<tr><th><b>'.TXT_CFG_COLABS_FICHA_SITUACAO.'</b></th><td>'.$SIT_TXT.'</td></tr></table>';
				
				
				# Gera lista de nivel de acesso
				
				$sql_idCOLABORADOR = $mysqli->escape_String($o_idCOLABORADOR);
				
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT PNA.`modulo_titulo_pt_br`,
                PNA.`modulo_titulo_en_us`,
                PNA.`modulo_titulo_es_es`,
                PNA.`exibir_ind_acessar`,
                PNA.`exibir_ind_editar`,
                PNA.`exibir_ind_apagar`,
                CA.`ind_acessar`,
                CA.`ind_editar`,
                CA.`ind_apagar`
           FROM `SYSTEM_NIVEL_ACESSO` PNA
           LEFT JOIN `COLABORADOR_ACESSO` CA
                  ON CA.`modulo_codigo` = PNA.`modulo_codigo`
                 AND CA.`idSYSTEM_CLIENTE` = ?
                 AND CA.`idCOLABORADOR` = ?
           WHERE PNA.`modulo_liberado` = 1 
          ORDER BY PNA.`ordem`"
				)) 
				{
					$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_A_TIT_PT, $o_A_TIT_EN, $o_A_TIT_ES, $o_A_EXIBIR_ACESSAR, $o_A_EXIBIR_EDITAR, $o_A_EXIBIR_APAGAR, $o_A_ACESSAR, $o_A_EDITAR, $o_A_APAGAR);
					
					while($stmt->fetch())
					{
						switch($IN_LANG)
						{
							case "pt-br":
								$o_A_TIT = $o_A_TIT_PT;
								break;
							case "en-us":
								$o_A_TIT = $o_A_TIT_EN;
								break;
							case "es-es":
								$o_A_TIT = $o_A_TIT_ES;
								break;
							default:
								break;
						}
						
						$ACESSO_ENTRY_INFO = "";
						
						if($o_A_EXIBIR_ACESSAR == 1)
						{
							if($o_A_ACESSAR == 1)
							{ 
								if($ACESSO_ENTRY_INFO){ $ACESSO_ENTRY_INFO .= ", ".TXT_NIVEL_ACESSO_ACESSAR; } else { $ACESSO_ENTRY_INFO .= TXT_NIVEL_ACESSO_ACESSAR; }
							}
						}
						if($o_A_EXIBIR_EDITAR == 1)
						{
							if($o_A_EDITAR == 1)
							{ 
								if($ACESSO_ENTRY_INFO){ $ACESSO_ENTRY_INFO .= ", ".TXT_NIVEL_ACESSO_EDITAR; } else { $ACESSO_ENTRY_INFO .= TXT_NIVEL_ACESSO_EDITAR; }
							}
						}
						if($o_A_EXIBIR_APAGAR == 1)
						{
							if($o_A_APAGAR == 1)
							{ 
								if($ACESSO_ENTRY_INFO){ $ACESSO_ENTRY_INFO .= ", ".TXT_NIVEL_ACESSO_APAGAR; } else { $ACESSO_ENTRY_INFO .= TXT_NIVEL_ACESSO_APAGAR; }
							}
						}
						
						if($ACESSO_ENTRY_INFO)
						{
							$ACESSO_LIST .= '	<tr><th style="white-space: nowrap;"><b>'.$o_A_TIT.':</b></th><td>'.$ACESSO_ENTRY_INFO.'</td></tr>';
						}
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				
				
				$FICHA .= '<br/><table class="ficha-table" style="width:100%;"><tr><th colspan=2 style="text-align: center; font-size:120%;"><b>'.TXT_CFG_COLABS_FICHA_NIVEL_ACESSO.'</b></th></tr>';
				$FICHA .= $ACESSO_LIST;
				
				$FICHA .= '</div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
