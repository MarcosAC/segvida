<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: mdl-prfl-cad.php
## Função..........: MODEL
##                   - Efetua a atualizacao do cadastro do usuario;
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_DBO = $IN_REG_ID = $IN_NOME = $IN_SOBRENOME = $IN_EMAIL = $IN_CPF = $IN_CNPJ = $IN_RAZAO = $IN_FONEFIXO1 = $IN_CELULAR1 = 
	$IN_PAIS = $IN_IDIOMA = $IN_CEP = $IN_UF = $IN_CIDADE_CODIGO = $IN_BAIRRO = $IN_END = $IN_NUM = $IN_COMPL = $IN_USERNAME = $IN_PASSWORD = "";
	
	//systemmudas.rlgomide.com/cgi-bin/system-prfl-cad-updt.php?l=br&nome=RODRIGO&sobrenome=GOMIDE&email=rlgomide%40gmail.com&cpf=03104925640&cnpj=88263823000150&razao=THER%20SISTEMAS%20INOVADORES%20LTDA&fonefixo1=3136115079&celular1=32988891566&pais=BR&cep=36570000&uf=MG&cidade=3171303&bairro=JOAO%20BRAZ&end=RUA%20PEDRA%20DO%20ANTA&num=65&compl=CASA%20B%20FUNDOS
	
	## DEBUG
	//$_DEBUG=1;
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["l"]);
		$IN_DBO        = test_input($_GET["dbo"]);
		$IN_REG_ID     = test_input($_GET["rid"]);
		$IN_NOME       = test_input($_GET["nome"]);
		$IN_SOBRENOME  = test_input($_GET["sobrenome"]);
		$IN_EMAIL      = test_input($_GET["email"]);
		$IN_CPF        = test_input($_GET["cpf"]);
		$IN_CNPJ       = test_input($_GET["cnpj"]);
		$IN_RAZAO      = test_input($_GET["razao"]);
		$IN_FONEFIXO1  = test_input($_GET["fonefixo1"]);
		$IN_CELULAR1   = test_input($_GET["celular1"]);
		$IN_PAIS       = test_input($_GET["pais"]);
		$IN_IDIOMA     = test_input($_GET["idioma"]);
		$IN_CEP        = test_input($_GET["cep"]);
		$IN_UF         = test_input($_GET["uf"]);
		$IN_CIDADE     = test_input($_GET["cidade"]);
		$IN_BAIRRO     = test_input($_GET["bairro"]);
		$IN_END        = test_input($_GET["end"]);
		$IN_NUM        = test_input($_GET["num"]);
		$IN_COMPL      = test_input($_GET["compl"]);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG       = test_input($_POST["l"]);
		$IN_DBO        = test_input($_POST["dbo"]);
		$IN_REG_ID     = test_input($_POST["rid"]);
		$IN_NOME       = test_input($_POST["nome"]);
		$IN_SOBRENOME  = test_input($_POST["sobrenome"]);
		$IN_EMAIL      = test_input($_POST["email"]);
		$IN_CPF        = test_input($_POST["cpf"]);
		$IN_CNPJ       = test_input($_POST["cnpj"]);
		$IN_RAZAO      = test_input($_POST["razao"]);
		$IN_FONEFIXO1  = test_input($_POST["fonefixo1"]);
		$IN_CELULAR1   = test_input($_POST["celular1"]);
		$IN_PAIS       = test_input($_POST["pais"]);
		$IN_IDIOMA     = test_input($_POST["idioma"]);
		$IN_CEP        = test_input($_POST["cep"]);
		$IN_UF         = test_input($_POST["uf"]);
		$IN_CIDADE     = test_input($_POST["cidade"]);
		$IN_BAIRRO     = test_input($_POST["bairro"]);
		$IN_END        = test_input($_POST["end"]);
		$IN_NUM        = test_input($_POST["num"]);
		$IN_COMPL      = test_input($_POST["compl"]);
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false)
		{
			die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|");
			exit;
		}
		
		$IN_USERNAME = $_SESSION['user_username'];
		$IN_REG_ID   = $_SESSION['user_id'];
		
		## Autentica dados obrigatorios
		if( !empty($IN_DBO) &&
				!empty($IN_NOME) &&
				!empty($IN_SOBRENOME) &&
				!empty($IN_EMAIL) &&
				!empty($IN_CPF) &&
				//!empty($IN_CNPJ) &&
				//!empty($IN_RAZAO) &&
				!empty($IN_FONEFIXO1) &&
				//!empty($IN_CELULAR1) &&
				!empty($IN_PAIS) &&
				!empty($IN_CEP) &&
				!empty($IN_UF) &&
				!empty($IN_CIDADE) &&
				!empty($IN_BAIRRO) &&
				!empty($IN_END) &&
				!empty($IN_NUM) &&
				//!empty($IN_COMPL) &&
				!empty($IN_USERNAME) 
			) 
		{ $GO=1; } else 
		{ 
			#Se for upload, valida e segue
			if(!empty($IN_DBO) && $IN_DBO == 4){ $GO=1; } else
			{
				$GO=0;
				die("0|".TXT_FALHA_TRANSF_DADOS."|alert|");
				exit;
			}
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			//$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			//if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## uppercase
			mb_strtoupper($IN_USERNAME,"UTF-8");
			mb_strtolower($IN_IDIOMA,"UTF-8");
			mb_strtolower($IN_EMAIL,"UTF-8");
			mb_strtoupper($IN_NOME,"UTF-8");
			mb_strtoupper($IN_SOBRENOME,"UTF-8");
			mb_strtoupper($IN_RAZAO,"UTF-8");
			mb_strtoupper($IN_END,"UTF-8");
			mb_strtoupper($IN_NUM,"UTF-8");
			mb_strtoupper($IN_COMPL,"UTF-8");
			mb_strtoupper($IN_BAIRRO,"UTF-8");
			
			####
			# INSERT, UPDATE ou DELETE?
			####
			switch($IN_DBO)
			{
				## SELECT
//				case '0':
//					if(!empty($IN_REG_ID) || $IN_REG_ID > 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
//					$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $IN_NOME, $IN_SOBRENOME, $IN_EMAIL, $IN_CPF, $IN_CNPJ, $IN_RAZAO, $IN_FONEFIXO1, $IN_CELULAR1, $IN_PAIS, $IN_IDIOMA, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUM, $IN_COMPL, $IN_USERNAME);
//					die($tmp_RESULT);
//					exit;
//					break;
				## INSERT
//				case '1':
//					if(!empty($IN_REG_ID) || $IN_REG_ID > 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
//					$tmp_RESULT = executa_INSERT($mysqli, $_DEBUG, $IN_NOME, $IN_SOBRENOME, $IN_EMAIL, $IN_CPF, $IN_CNPJ, $IN_RAZAO, $IN_FONEFIXO1, $IN_CELULAR1, $IN_PAIS, $IN_IDIOMA, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUM, $IN_COMPL, $IN_USERNAME);
//					die($tmp_RESULT);
//					exit;
//					break;
				## UPDATE
				case '2':
					if(empty($IN_REG_ID) || $IN_REG_ID <= 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
					$tmp_RESULT = executa_UPDATE($mysqli, $_DEBUG, $IN_REG_ID, $IN_NOME, $IN_SOBRENOME, $IN_EMAIL, $IN_CPF, $IN_CNPJ, $IN_RAZAO, $IN_FONEFIXO1, $IN_CELULAR1, $IN_PAIS, $IN_IDIOMA, $IN_CEP, $IN_UF, $IN_CIDADE, $IN_BAIRRO, $IN_END, $IN_NUM, $IN_COMPL, $IN_USERNAME);
					die($tmp_RESULT);
					exit;
					break;
				## DELETE
//				case '3':
//					if(empty($IN_REG_ID) || $IN_REG_ID <= 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
//					$tmp_RESULT = executa_DELETE($mysqli, $_DEBUG, $IN_REG_ID);
//					die($tmp_RESULT);
//					exit;
//					break;
				## FILE UPLOAD
				case '4':
					if(empty($IN_REG_ID) || $IN_REG_ID <= 0) { die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|"); exit; break; }
					$tmp_RESULT = executa_UPLOAD($mysqli, $_DEBUG, $IN_REG_ID);
					die($tmp_RESULT);
					exit;
					break;
				## DEFAULT
				default:
					die("0|".TXT_MYSQLI_OPERACAO_INVALIDA."|alert|");
					exit;
					break;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# INSERT
	####
	function executa_SELECT($mysqli, $_DEBUG, $_NOME, $_SOBRENOME, $_EMAIL, $_CPF, $_CNPJ, $_RAZAO, $_FONEFIXO1, $_CELULAR1, $_PAIS, $_IDIOMA, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUM, $_COMPL, $_USERNAME)
	{
		return;
		exit;
		
	}
	
	####
	# INSERT
	####
	function executa_INSERT($mysqli, $_DEBUG, $_NOME, $_SOBRENOME, $_EMAIL, $_CPF, $_CNPJ, $_RAZAO, $_FONEFIXO1, $_CELULAR1, $_PAIS, $_IDIOMA, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUM, $_COMPL, $_USERNAME)
	{
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `SYSTEM_USER_ACCOUNT` U
		    SET U.`nome` = ?,
		        U.`sobrenome` = ?,
		        U.`email` = ?,
		        U.`cpf` = ?,
		        U.`cnpj` = ?,
		        U.`razao_social` = ?,
		        U.`fonefixo1` = ?,
		        U.`celular1` = ?,
		        U.`pais` = ?,
		        U.`idioma` = ?,
		        U.`cep` = ?,
		        U.`uf` = ?,
		        U.`cidade_codigo` = ?,
		        U.`bairro` = ?,
		        U.`end` = ?,
		        U.`numero` = ?,
		        U.`compl` = ?,
		        U.`updt_username` = ?,
		        U.`updt_date` = now()"
		)) 
		{
			$sql_REG_ID    = $mysqli->escape_String($_REG_ID);
			$sql_USERNAME  = $mysqli->escape_String($_USERNAME);
			$sql_NOME      = $mysqli->escape_String($_NOME);
			$sql_SOBRENOME = $mysqli->escape_String($_SOBRENOME);
			$sql_EMAIL     = $mysqli->escape_String($_EMAIL);
			$sql_CPF       = $mysqli->escape_String($_CPF);
			$sql_CNPJ      = $mysqli->escape_String($_CNPJ);
			$sql_RAZAO     = $mysqli->escape_String($_RAZAO);
			$sql_FONEFIXO1 = $mysqli->escape_String($_FONEFIXO1);
			$sql_CELULAR1  = $mysqli->escape_String($_CELULAR1);
			$sql_PAIS      = $mysqli->escape_String($_PAIS);
			$sql_IDIOMA    = $mysqli->escape_String($_IDIOMA);
			$sql_CEP       = $mysqli->escape_String($_CEP);
			$sql_UF        = $mysqli->escape_String($_UF);
			$sql_CIDADE    = $mysqli->escape_String($_CIDADE);
			$sql_BAIRRO    = $mysqli->escape_String($_BAIRRO);
			$sql_END       = $mysqli->escape_String($_END);
			$sql_NUM       = $mysqli->escape_String($_NUM);
			$sql_COMPL     = $mysqli->escape_String($_COMPL);
			//
			$stmt->bind_param('ssssssssssssssssss', $sql_NOME, $sql_SOBRENOME, $sql_EMAIL, $sql_CPF, $sql_CNPJ, $sql_RAZAO, $sql_FONEFIXO1, $sql_CELULAR1, 
			$sql_PAIS, $sql_IDIOMA, $sql_CEP, $sql_UF, $sql_CIDADE, $sql_BAIRRO, $sql_END, $sql_NUM, $sql_COMPL, $sql_REG_ID );
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_INSERT_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_INSERT_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPDATE
	####
	function executa_UPDATE($mysqli, $_DEBUG, $_REG_ID, $_NOME, $_SOBRENOME, $_EMAIL, $_CPF, $_CNPJ, $_RAZAO, $_FONEFIXO1, $_CELULAR1, $_PAIS, $_IDIOMA, $_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUM, $_COMPL, $_USERNAME)
	{
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `SYSTEM_USER_ACCOUNT`
		    SET  `nome` = ?
		        ,`sobrenome` = ?
		        ,`email` = ?
		        ,`cpf` = ?
		        ,`cnpj` = ?
		        ,`razao_social` = ?
		        ,`fone_fixo1` = ?
		        ,`celular1` = ?
		        ,`pais` = ?
		        ,`idioma` = ?
		        ,`cep` = ?
		        ,`uf` = ?
		        ,`cidade_codigo` = ?
		        ,`bairro` = ?
		        ,`end` = ?
		        ,`numero` = ?
		        ,`compl` = ?
		        ,`updt_username` = ?
		        ,`updt_date` = now()
		  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
		)) 
		{
			$sql_NOME      = $mysqli->escape_String($_NOME);
			$sql_SOBRENOME = $mysqli->escape_String($_SOBRENOME);
			$sql_EMAIL     = $mysqli->escape_String($_EMAIL);
			$sql_CPF       = $mysqli->escape_String($_CPF);
			$sql_CNPJ      = $mysqli->escape_String($_CNPJ);
			$sql_RAZAO     = $mysqli->escape_String($_RAZAO);
			$sql_FONEFIXO1 = $mysqli->escape_String($_FONEFIXO1);
			$sql_CELULAR1  = $mysqli->escape_String($_CELULAR1);
			$sql_PAIS      = $mysqli->escape_String($_PAIS);
			$sql_IDIOMA    = $mysqli->escape_String($_IDIOMA);
			$sql_CEP       = $mysqli->escape_String($_CEP);
			$sql_UF        = $mysqli->escape_String($_UF);
			$sql_CIDADE    = $mysqli->escape_String($_CIDADE);
			$sql_BAIRRO    = $mysqli->escape_String($_BAIRRO);
			$sql_END       = $mysqli->escape_String($_END);
			$sql_NUM       = $mysqli->escape_String($_NUM);
			$sql_COMPL     = $mysqli->escape_String($_COMPL);
			$sql_USERNAME  = $mysqli->escape_String($_USERNAME);
			$sql_REG_ID    = $mysqli->escape_String($_REG_ID);
			//
			$stmt->bind_param('sssssssssssssssssss', $sql_NOME, $sql_SOBRENOME, $sql_EMAIL, $sql_CPF, $sql_CNPJ, $sql_RAZAO, $sql_FONEFIXO1, $sql_CELULAR1, $sql_PAIS, $sql_IDIOMA, $sql_CEP, $sql_UF, $sql_CIDADE, $sql_BAIRRO, $sql_END, $sql_NUM, $sql_COMPL, $sql_USERNAME, $sql_REG_ID);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_UPDATE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			    else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# DELETE
	####
	function executa_DELETE($mysqli, $_DEBUG, $_REG_ID)
	{
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"DELETE FROM `SYSTEM_USER_ACCOUNT` WHERE `idSYSTEM_USER_ACCOUNT` = ?"
		)) 
		{
			$sql_REG_ID    = $mysqli->escape_String($_REG_ID);
			//
			$stmt->bind_param('s', $sql_REG_ID);
			
			if($stmt->execute())
			{
				return "1|".TXT_MYSQLI_DELETE_OK."|success|";
				exit;
			}
			else
			{
				return "0|".TXT_MYSQLI_DELETE_ERRO."|error|";
				exit;
			}
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}	
		
	}
	
	####
	# UPLOAD
	####
	function executa_UPLOAD($mysqli, $_DEBUG, $_REG_ID)
	{
		#Extensoes permitidas para a foto
		$allowedExts = array("gif", "jpeg", "jpg", "png");
		$temp = explode(".", $_FILES["file"]["name"]);
		$extension = end($temp);
		
		if (
				(($_FILES["file"]["type"] == "image/gif")   || 
				 ($_FILES["file"]["type"] == "image/jpeg")  || 
				 ($_FILES["file"]["type"] == "image/jpg")   || 
				 ($_FILES["file"]["type"] == "image/pjpeg") || 
				 ($_FILES["file"]["type"] == "image/x-png") || 
				 ($_FILES["file"]["type"] == "image/png")) && 
				 ($_FILES["file"]["size"] < 80000) &&
				 in_array($extension, $allowedExts)
			) 
		{
			if ($_FILES["file"]["error"] > 0) 
			{
				//return "Return Code: " . $_FILES["file"]["error"] . "<br>";
				return "0|".$_FILES["file"]["error"]."|error|";
				exit;
			}
			else
			{
				//$nome_foto = md5(uniqid(time())).$extension; //nome que dará a imagem
				//$filename = $_FILES["file"]["name"];
				//echo "Upload: " . $_FILES["file"]["name"] . "<br>";
				//echo "Type: " . $_FILES["file"]["type"] . "<br>";
				//echo "Size: " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
				//echo "Temp file: " . $_FILES["file"]["tmp_name"] . "<br>";
				
				
				## Carrega nome existente da foto de perfil do usuario
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"SELECT U.`profile_pic` as foto
           FROM `SYSTEM_USER_ACCOUNT` U
          WHERE U.`idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_REG_ID    = $mysqli->escape_String($_REG_ID);
					$stmt->bind_param('s', $sql_REG_ID);
					$stmt->execute();
					$stmt->store_result();
					
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_NOME_FOTO_ATUAL);
					
					$stmt->fetch();
					
				}
				else
				{
					return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|";
					exit;
				}
				
				
				## Gera nome unico para o arquivo e
				## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
				## Continua gerando até encontrar um nome valido.
				do 
				{
					$filename = md5(uniqid(time())).".".$extension; //nome que dará a imagem
					if (file_exists(PROFILE_PICS_PATH."/" . $filename)) { $continua = true; } else { $continua = false; }
				} while ($continua == true);
				
				## Move o arquivo para a pasta de fotos de perfil
				move_uploaded_file($_FILES["file"]["tmp_name"],PROFILE_PICS_PATH."/" . $filename);
				
				## Atualiza base de dados com o nome novo da foto
				##Prepara query
				if ($stmt = $mysqli->prepare(
				"UPDATE `SYSTEM_USER_ACCOUNT`
				    SET  `profile_pic` = ?
				  WHERE `idSYSTEM_USER_ACCOUNT` = ?"
				)) 
				{
					$sql_PROFILE_PIC = $mysqli->escape_String($filename);
					$sql_REG_ID      = $mysqli->escape_String($_REG_ID);
					//
					$stmt->bind_param('ss', $sql_PROFILE_PIC, $sql_REG_ID);
					
					if($stmt->execute())
					{
						## Apaga foto antiga do repositorio
						if( !empty($o_NOME_FOTO_ATUAL) && 
						    ($o_NOME_FOTO_ATUAL != "no_profile_pic.jpg") && 
						    file_exists(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL)
						  )
						{ unlink(PROFILE_PICS_PATH."/" . $o_NOME_FOTO_ATUAL); }
						
						## Retorna OK com o nome da foto nova
						return "1|".TXT_MOD_PERFIL_FOTO_ATUALIZADA."|success|".$filename."|";
						exit;
					}
					else
					{
						return "0|".TXT_MYSQLI_UPDATE_ERRO."|error|";
						exit;
					}
					
				}
				else
				{
					if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
					      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
					exit;
				}
				
				//if (file_exists("uploads/" . $filename)) 
				//{
				//	//echo $filename . " already exists. ";
				//	return "0|".$filename . " already exists. (".$nome_atual.")(".$_REG_ID.")|error|";
				//	exit;
				//} 
				//else 
				//{
				//	move_uploaded_file($_FILES["file"]["tmp_name"],"uploads/" . $filename);
				//	//echo "Stored in: " . "uploads/" . $filename;
				//	return "1|"."Stored in: " . "uploads/" . $filename."(".$nome_atual.")|success|";
				//	exit;
				//}
			}
		}
		else
		{
			//echo "Invalid file";
			return "0|".TXT_UPLOAD_ARQUIVO_INVALIDO."|error|";
			exit;
		}
		
	}
	
#################################################################################
###########
#####
##
?>
