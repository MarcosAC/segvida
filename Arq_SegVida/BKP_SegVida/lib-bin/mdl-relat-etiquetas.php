<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: mdl-relat-etiqueta.php
##  Funcao: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 09/06/2017
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	include_once 'includes/tcpdf/PDF_Label_QR.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = $IN_DATA_ESTAQ = $IN_LOTE = $IN_LOTE_EXIBIR = $IN_MUDA = $IN_MUDA_EXIBIR = $IN_ETIQUETA = $IN_IMPRESSAO = $IN_IMPR_COL = $IN_IMPR_ROW = $IN_QTDE_IMPR = $IN_QTDE = $IN_BORDA = "";
	
	####
	# Set Debug Mode
	####
	#$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
		$IN_DATA = test_input($_GET["data"]);
		$IN_LOTE = test_input($_GET["lote"]);
		$IN_TITULO = test_input($_GET["titulo"]);
		$IN_TEXTO = test_input($_GET["texto"]);
/*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		$IN_DATA_ESTAQ = test_input($_POST["de"]);
		$IN_LOTE = test_input($_POST["lt"]);
		$IN_LOTE_EXIBIR = test_input($_POST["lte"]);
		$IN_MUDA = test_input($_POST["md"]);
		$IN_MUDA_EXIBIR = test_input($_POST["mde"]);
		$IN_ETIQUETA = test_input($_POST["etq"]);
		$IN_IMPRESSAO = test_input($_POST["imp"]);
		$IN_IMPR_COL = test_input($_POST["imp_c"]);
		$IN_IMPR_ROW = test_input($_POST["imp_r"]);
		$IN_QTDE_IMPR = test_input($_POST["qtdi"]);
		$IN_QTDE = test_input($_POST["qtd"]);
		$IN_BORDA = test_input($_POST["brd"]);
		
		if( isEmpty($IN_LOTE_EXIBIR) ){ $IN_LOTE_EXIBIR = 1; }
		if( isEmpty($IN_MUDA_EXIBIR) ){ $IN_MUDA_EXIBIR = 1; }
		
		## case convertion
		//mb_strtoupper($IN_TITULO,"UTF-8");
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idPRODFY_CLIENTE    = $_SESSION['user_idprodfy_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idPRODFY_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## SELECT
			case '1':
				if( isEmpty($IN_DBO) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				$tmp_RESULT = executa_SELECT($mysqli, $_DEBUG, $sid_idPRODFY_CLIENTE, $sid_USERNAME, $IN_DATA, $IN_LOTE, $IN_LOTE_EXIBIR, $IN_MUDA, $IN_MUDA_EXIBIR, $IN_TITULO, $IN_TEXTO);
				die($tmp_RESULT);
				exit;
				break;
			## IMPRESSAO
			case '2':
				if( isEmpty($IN_DBO) || isEmpty($IN_ETIQUETA) || isEmpty($IN_IMPRESSAO) || isEmpty($IN_QTDE_IMPR) || isEmpty($IN_QTDE) ) 
				{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				if( $IN_IMPRESSAO == "2" )
				{
					if( isEmpty($IN_IMPR_COL) || isEmpty($IN_IMPR_ROW) ) 
					{ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				}
				## Executa
				$tmp_RESULT = executa_IMPRESSAO_ETIQUETA1($mysqli, $_DEBUG, $sid_idPRODFY_CLIENTE, $IN_DATA_ESTAQ, $IN_LOTE, $IN_LOTE_EXIBIR, $IN_MUDA, $IN_MUDA_EXIBIR, $IN_ETIQUETA, $IN_IMPRESSAO, $IN_IMPR_COL, $IN_IMPR_ROW, $IN_QTDE_IMPR, $IN_QTDE, $IN_BORDA);
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# IMPRESSAO - ETIQUETA QR - 1
	####
	function executa_IMPRESSAO_ETIQUETA1($mysqli, $_DEBUG, $_SID_idPRODFY_CLIENTE, $_DATA_ESTAQ, $_LOTE, $_LOTE_EXIBIR, $_MUDA, $_MUDA_EXIBIR, $_ETIQUETA, $_IMPRESSAO, $_IMPR_COL, $_IMPR_ROW, $_QTDE_IMPR, $_QTDE, $_BORDA)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idPRODFY_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		$_tmp = explode('|',$_LOTE);
		$_LOTE_ID  = $_tmp[0];
		$_LOTE_COD = $_tmp[1];
		$_LOTE_OBJ = $_tmp[2];
		$_LOTE_CLI = $_tmp[3];
		
		$_tmp = explode('|',$_MUDA);
		$_MUDA_ID       = $_tmp[0];
		$_MUDA_NOME_INT = $_tmp[1];
		$_MUDA_NOME     = $_tmp[2];
		$_MUDA_NOME_CIE = $_tmp[3];
		
		## Processa Etiquetas
		
		switch($_IMPRESSAO)
		{
			case 3:// 3-Excel
				{
					//Label - Lote
					$o_LABEL_LOTE = '';
					switch($_LOTE_EXIBIR)
					{
						case '1': //Código
							$o_LABEL_LOTE .= $_LOTE_COD;
							break;
						case '2': //Código - Objetivo
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ;
							break;
						case '3': //Código - Cliente
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_CLI;
							break;
						case '4': //Código - Objetivo - Cliente
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ.' - '.$_LOTE_CLI;
							break;
						case '5': //Objetivo
							$o_LABEL_LOTE .= $_LOTE_OBJ;
							break;
						case '6': //Objetivo - Código
							$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_COD;
							break;
						case '7': //Objetivo - Cliente
							$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_CLI;
							break;
						case '8': //Cliente
							$o_LABEL_LOTE .= $_LOTE_CLI;
							break;
						case '9': //Cliente - Código
							$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_COD;
							break;
						case '10': //Cliente - Objetivo
							$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_OBJ;
							break;
						default:
							break;
					}
					
					//Label - Muda
					$o_LABEL_MUDA = '';
					switch($_MUDA_EXIBIR)
					{
						case '1': //Nome Interno
							$o_LABEL_MUDA .= $_MUDA_NOME_INT;
							break;
						case '2': //Nome Comum
							$o_LABEL_MUDA .= $_MUDA_NOME;
							break;
						case '3': //Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME_CIE;
							break;
						case '4': //Nome Interno - Nome Comum
							$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME;
							break;
						case '5': //Nome Interno - Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME_CIE;
							break;
						case '6': //Nome Comum - Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME.' - '.$_MUDA_NOME_CIE;
							break;
						default:
							break;
					}
					
					//Label - Qtde
					//if($_QTDE)
					//{
					//	if($labeltext){ $labeltext .= "\n".$_QTDE; } else { $labeltext .= $_QTDE; }
					//}
					
					/** Error reporting */
					/*
					error_reporting(E_ALL);
					ini_set('display_errors', TRUE);
					ini_set('display_startup_errors', TRUE);
					date_default_timezone_set('America/Sao_Paulo');
					*/
					
					//if (PHP_SAPI == 'cli') die('This example should only be run from a Web Browser');
					
					/** Include PHPExcel */
					require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
					include_once 'includes/qrcode/qrcode.class.php';
					
					
					// Create new PHPExcel object
					$objPHPExcel = new PHPExcel();
					//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
					
					$locale = 'pt_br';
					$validLocale = PHPExcel_Settings::setLocale($locale);
					
					// Set document properties
					$objPHPExcel->getProperties()
					            ->setCreator("Prodfy Plantas")
					            ->setLastModifiedBy("Prodfy Plantas")
					            ->setTitle("Etiquetas QR para Impressão")
					            ->setSubject("Etiquetas QR para Impressão")
					            ->setDescription("Lista com os dados para impressão de etiquetas com código QR")
					            ->setKeywords("prodfy plantas etiqueta qr impressão")
					            ->setCategory("Etiquetas QR");
					
					$_QR_IMG_LIST = [];
					
					$objPHPExcel->setActiveSheetIndex(0);
					
					$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
					//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
					//$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
					//$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
					//$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
					
					
					//Gera Lista
					for($i=1;$i<=$_QTDE_IMPR;$i++)
					{
						$objPHPExcel->getActiveSheet()->getRowDimension($i)->setRowHeight(60);
						
						//error level -> 'L', 'M', 'Q', 'H'
						
						//Add QR Image
						$qrcode = new QRcode(utf8_encode($_LOTE_COD."|".$_MUDA_ID."|".$_QTDE."|".$_DATA_ESTAQ."|"), 'L');
						$qrcode->disableBorder();
						
						$_FILENAME = getValidRandomFilename(PDF_PATH,'png',0);
						$_QR_FILENAME = 'qr_'.$i.'_'.$_FILENAME.'.png';
						$qrcode->displayPNG(60,array(255,255,255),array(0,0,0),PDF_PATH.'/'.$_QR_FILENAME,0);
						
						if (!in_array($_QR_FILENAME, $_QR_IMG_LIST)) 
						{ 
							array_push($_QR_IMG_LIST, $_QR_FILENAME);
						}
						
						$gdImage = imagecreatefrompng(PDF_PATH.'/'.$_QR_FILENAME);
						
						// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
						$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
						$objDrawing->setName('QRCode#'.$i);
						$objDrawing->setDescription('QRCode#'.$i);
						$objDrawing->setImageResource($gdImage);
						$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
						$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
						$objDrawing->setWidth(60);
						$objDrawing->setHeight(60);
						$objDrawing->setCoordinates('A'.$i);
						$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
						
						## Formata Label Text
						$_LABEL_TEXT = "";
						//Label - Data Estaq
						if($_DATA_ESTAQ)
						{
							if($_LABEL_TEXT){ $_LABEL_TEXT .= "\rE: ".$_DATA_ESTAQ; } else { $_LABEL_TEXT .= "E: ".$_DATA_ESTAQ; }
						}
						
						//Label - Lote
						$o_LABEL_LOTE = '';
						switch($_LOTE_EXIBIR)
						{
							case '1': //Código
								$o_LABEL_LOTE .= $_LOTE_COD;
								break;
							case '2': //Código - Objetivo
								$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ;
								break;
							case '3': //Código - Cliente
								$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_CLI;
								break;
							case '4': //Código - Objetivo - Cliente
								$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ.' - '.$_LOTE_CLI;
								break;
							case '5': //Objetivo
								$o_LABEL_LOTE .= $_LOTE_OBJ;
								break;
							case '6': //Objetivo - Código
								$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_COD;
								break;
							case '7': //Objetivo - Cliente
								$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_CLI;
								break;
							case '8': //Cliente
								$o_LABEL_LOTE .= $_LOTE_CLI;
								break;
							case '9': //Cliente - Código
								$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_COD;
								break;
							case '10': //Cliente - Objetivo
								$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_OBJ;
								break;
							default:
								break;
						}
						if($o_LABEL_LOTE)
						{
							if($_LABEL_TEXT){ $_LABEL_TEXT .= "\rL: ".$o_LABEL_LOTE; } else { $_LABEL_TEXT .= "L:".$o_LABEL_LOTE; }
						}
						
						//Label - Muda
						$o_LABEL_MUDA = '';
						switch($_MUDA_EXIBIR)
						{
							case '1': //Nome Interno
								$o_LABEL_MUDA .= $_MUDA_NOME_INT;
								break;
							case '2': //Nome Comum
								$o_LABEL_MUDA .= $_MUDA_NOME;
								break;
							case '3': //Nome Científico
								$o_LABEL_MUDA .= $_MUDA_NOME_CIE;
								break;
							case '4': //Nome Interno - Nome Comum
								$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME;
								break;
							case '5': //Nome Interno - Nome Científico
								$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME_CIE;
								break;
							case '6': //Nome Comum - Nome Científico
								$o_LABEL_MUDA .= $_MUDA_NOME.' - '.$_MUDA_NOME_CIE;
								break;
							default:
								break;
						}
						if($o_LABEL_MUDA)
						{
							if($_LABEL_TEXT){ $_LABEL_TEXT .= "\rP: ".$o_LABEL_MUDA; } else { $_LABEL_TEXT .= "P:".$o_LABEL_MUDA; }
						}
						
						//Label - Qtde
						if($_QTDE)
						{
							if($_LABEL_TEXT){ $_LABEL_TEXT .= "\rQ: ".$_QTDE; } else { $_LABEL_TEXT .= "Q:".$_QTDE; }
						}
						
						
						/*
						$objPHPExcel->getActiveSheet()
						            ->getStyle("A")
						            ->getAlignment()
						            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
						*/
						
						$objPHPExcel->getActiveSheet()
						            ->SetCellValue('A'.$i, $_LABEL_TEXT);
						
						$objPHPExcel->getActiveSheet()
						            ->getStyle('A'.$i)
						            ->getAlignment()
						            ->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT)
						            ->setWrapText(true);
						
						/*$objPHPExcel->getActiveSheet()
						            ->getStyle('A'.$i)
						            ->getAlignment()
						            ->setWrapText(true);*/
						/*
						$objPHPExcel->getActiveSheet()
						            ->SetCellValue('B'.$i, $_DATA_ESTAQ)
						            ->SetCellValue('C'.$i, $o_LABEL_LOTE)
						            ->SetCellValue('D'.$i, $o_LABEL_MUDA)
						            ->SetCellValue('E'.$i, $_QTDE);
						*/
						
						//Remove imagem QR do disco
						//if (file_exists(PDF_PATH.'/'.$_QR_FILENAME)) { unlink (PDF_PATH.'/'.$_QR_FILENAME); }
					}
					
					/*
					$lastrow = $objPHPExcel->getActiveSheet()->getHighestRow();
					$objPHPExcel->getActiveSheet()
					            ->getStyle('A1:A'.$lastrow)
					            ->getAlignment()
					            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
					*/
					
					// Rename worksheet
					$objPHPExcel->getActiveSheet()->setTitle('Etiquetas QR');
					
					// Set active sheet index to the first sheet, so Excel opens this as the first sheet
					$objPHPExcel->setActiveSheetIndex(0);
					
					/*
					// Redirect output to a client’s web browser (Excel2007)
					header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
					header('Content-Disposition: attachment;filename="01simple.xlsx"');
					header('Cache-Control: max-age=0');
					// If you're serving to IE 9, then the following may be needed
					header('Cache-Control: max-age=1');
					
					// If you're serving to IE over SSL, then the following may be needed
					header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
					header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
					header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
					header ('Pragma: public'); // HTTP/1.0
					
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$objWriter->save('php://output');
					
					*/
					
					//$_FILENAME_TMP = getValidRandomFilename(PDF_PATH,'xlsx',0);
					//$_FILENAME = $_FILENAME_TMP.'.xlsx';
					
					$_FILENAME = getValidRandomFilename(PDF_PATH,'xlsx',1);
					
					
					// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
					PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
					
					## Grava arquivo
					$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
					$objWriter->save(PDF_PATH."/".$_FILENAME);
					
					
					/*
					$zip = new ZipArchive();
					if( $zip->open( PDF_PATH.'/'.$_FILENAME_TMP.'.zip' , ZipArchive::CREATE )  === true)
					{
						$zip->addFile('arquivos/lorem.txt' , 'lorem.txt' );
						$zip->addFile(  'arquivos/not_today.jpg' , 'pasta/not_today.jpg') ;
						$zip->addFromString('string.txt' , "Uma string qualquer" );
						$zip->close();
					}
					*/
					
					
					$link = PDF_URL."/".$_FILENAME;
					
					$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo XLSX.</b></a>";
					
				}
				break;
			default://1-PDF-Padrao, 2-PDF-Posicao Especifica
				{
					//Gera PDF das etiquetas
					if($_IMPRESSAO == 1)
					{
						$pdf = new PDF_Label($_ETIQUETA);
					}
					else
					{
						$pdf = new PDF_Label($_ETIQUETA, "mm", $_IMPR_COL, $_IMPR_ROW);
					}
					$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) ); 
					$pdf->AddPage();
					$pdf->SetAuthor('Prodfy Plantas');
					$pdf->SetTitle('Etiquetas QR');
					$pdf->setFontSubsetting(true);
					$pdf->setPrintHeader(false);
					$pdf->setPrintFooter(false);
					
					switch($_ETIQUETA)
					{
						case 'PIMACO-A4251':
						case 'PIMACO-A4351':
							$labelrightspace = 18.5;
							$labeltopspace   = 1;
							break;
						case 'MAXPRINT-A4351':
							$labelrightspace = 18.5;
							$labeltopspace   = 1;
							break;
						default:
							break;
					}
					
					if($_BORDA == 1){ $border = 1; } else { $border = 0; }
					
					$headertext     = "";
					
					//Label - Data Estaq
					if($_DATA_ESTAQ)
					{
						if($labeltext){ $labeltext .= "\nE: ".$_DATA_ESTAQ; } else { $labeltext .= "E: ".$_DATA_ESTAQ; }
					}
					
					//Label - Lote
					$o_LABEL_LOTE = '';
					switch($_LOTE_EXIBIR)
					{
						case '1': //Código
							$o_LABEL_LOTE .= $_LOTE_COD;
							break;
						case '2': //Código - Objetivo
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ;
							break;
						case '3': //Código - Cliente
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_CLI;
							break;
						case '4': //Código - Objetivo - Cliente
							$o_LABEL_LOTE .= $_LOTE_COD.' - '.$_LOTE_OBJ.' - '.$_LOTE_CLI;
							break;
						case '5': //Objetivo
							$o_LABEL_LOTE .= $_LOTE_OBJ;
							break;
						case '6': //Objetivo - Código
							$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_COD;
							break;
						case '7': //Objetivo - Cliente
							$o_LABEL_LOTE .= $_LOTE_OBJ.' - '.$_LOTE_CLI;
							break;
						case '8': //Cliente
							$o_LABEL_LOTE .= $_LOTE_CLI;
							break;
						case '9': //Cliente - Código
							$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_COD;
							break;
						case '10': //Cliente - Objetivo
							$o_LABEL_LOTE .= $_LOTE_CLI.' - '.$_LOTE_OBJ;
							break;
						default:
							break;
					}
					if($o_LABEL_LOTE)
					{
						if($labeltext){ $labeltext .= "\nL: ".$o_LABEL_LOTE; } else { $labeltext .= "L:".$o_LABEL_LOTE; }
					}
					
					//Label - Muda
					$o_LABEL_MUDA = '';
					switch($_MUDA_EXIBIR)
					{
						case '1': //Nome Interno
							$o_LABEL_MUDA .= $_MUDA_NOME_INT;
							break;
						case '2': //Nome Comum
							$o_LABEL_MUDA .= $_MUDA_NOME;
							break;
						case '3': //Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME_CIE;
							break;
						case '4': //Nome Interno - Nome Comum
							$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME;
							break;
						case '5': //Nome Interno - Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME_INT.' - '.$_MUDA_NOME_CIE;
							break;
						case '6': //Nome Comum - Nome Científico
							$o_LABEL_MUDA .= $_MUDA_NOME.' - '.$_MUDA_NOME_CIE;
							break;
						default:
							break;
					}
					if($o_LABEL_MUDA)
					{
						if($labeltext){ $labeltext .= "\nP: ".$o_LABEL_MUDA; } else { $labeltext .= "P:".$o_LABEL_MUDA; }
					}
					
					//Label - Qtde
					if($_QTDE)
					{
						if($labeltext){ $labeltext .= "\nQ: ".$_QTDE; } else { $labeltext .= "Q:".$_QTDE; }
					}
					
					//$labeltext      = $o_CODIGO."\n".$o_CLONE."\n".$_QTDE;
					
					$padding        = "";
					$image          = "";
					$imagewidth     = "";
					$imageheight    = "";
					$headerfontsize = 0;
					$fontsize       = 10;
					$idfontsize     = "";
					$barcode        = $_LOTE_COD."|".$_MUDA_ID."|".$_QTDE."|".$_DATA_ESTAQ."|";
					//$nbw            = 0.30;
					$nbw            = 0.30;
					$barcodewidth   = ((strlen($barcode)+3)*(15+2)+20)*$nbw;
					$bh             = max(0.15*$barcodewidth+3,16); //at least 15% of length
					$barcodesize    = 20;
					$wantraligntext = "";
					
					$_IMPR_ETQ = 3;
					
					for($i=0;$i<$_QTDE_IMPR;$i++)
					{
						$pdf->Add_Label($headertext, $labeltext, $labelrightspace, $labeltopspace, $padding, $border, $image, $imagewidth, $imageheight,
														$headerfontsize, $fontsize, $idfontsize, $barcode, $nbw,$bh, $barcodesize, $wantraligntext );
					}
					
					## Gera nome unico para o arquivo e
					## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
					## Continua gerando até encontrar um nome valido.
					do 
					{
						$filename = md5(uniqid(time())).".pdf"; //nome que dará a imagem
						if (file_exists(PDF_PATH."/".$filename)) { $continua = true; } else { $continua = false; }
					} while ($continua == true);
					
					
					#etiquetas-qr--$oDATA.pdf
					
					//$oDATA = date('Ymdhis');
					$pdf->Output(PDF_PATH."/".$filename,'F');
					
					$link = PDF_URL."/".$filename;
					
					$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo PDF.</b></a>";
					
				}
				break;
		}
		
		
		## Deleta arquivos QR gerados
		$q = count($_QR_IMG_LIST);
		if($q > 0)
		{
			for($r=0;$r<=$q;$r++)
			{
				if($_QR_IMG_LIST[$r])
				{
					//Remove imagem QR do disco
					if (file_exists(PDF_PATH.'/'.$_QR_IMG_LIST[$r])) { unlink (PDF_PATH.'/'.$_QR_IMG_LIST[$r]); }
				}
			}
		}
		
		
		
		## Resposta da geracao
		return "1|".$ret_txt."|success||";
		exit;
		
		
		
		
		
		/*
		## Gera escapes das variaveis
		//$sql_DDMMYYYY             = '%d/%m/%Y';
		//$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		//$sql_SID_idPRODFY_CLIENTE = $mysqli->escape_String($_SID_idPRODFY_CLIENTE);
		//$sql_LOTE                 = $mysqli->escape_String($_LOTE_ID);
		//$sql_MUDA                 = $mysqli->escape_String($_MUDA_ID);
		//$sql_ETIQUETA             = $mysqli->escape_String($_ETIQUETA);
		//$sql_IMPRESSAO            = $mysqli->escape_String($_IMPRESSAO);
		//$sql_QTDE                 = $mysqli->escape_String($_QTDE);
		
		## Carrega os dados dentro do critério especificado
		
		## Define impressao do lote
		if($_LOTE != "")
		{
			$QUERY_WHERE_LOTE = "AND AA.`idLOTE` = ".$sql_LOTE;
		}
		
		## Define 
		$QUERY = "SELECT upper(AA.`codigo`) as codigo, upper(BJTV.`titulo`) as objetivo, 
            upper(MD.`nome_interno`) as nome_interno, upper(CLNT.`nome_interno`) as cliente
       FROM `LOTE` AA
    LEFT JOIN `OBJETIVO` as BJTV
           ON BJTV.`idobjetivo` = AA.`idobjetivo`
    LEFT JOIN `MUDA` as MD
           ON MD.`idmuda` = AA.`idmuda`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
      WHERE AA.`idPRODFY_CLIENTE` = ".$sql_SID_idPRODFY_CLIENTE."
        ".$QUERY_WHERE_LOTE."
      ORDER BY AA.`codigo`";
		
		##Prepara query
		if ($stmt = $mysqli->prepare($QUERY)) 
		{
			//$stmt->bind_param('sssss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idPRODFY_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CODIGO, $o_OBJETIVO, $o_CLONE, $o_CLIENTE);
			
			$stmt->fetch();
			
			//Gera PDF das etiquetas
			if($_IMPRESSAO == 1)
			{
				$pdf = new PDF_Label($_ETIQUETA);
			}
			else
			{
				$pdf = new PDF_Label($_ETIQUETA, "mm", $_IMPR_COL, $_IMPR_ROW);
			}
			$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) ); 
			$pdf->AddPage();
			$pdf->SetAuthor('Prodfy Plantas');
			$pdf->SetTitle('Etiquetas QR');
			$pdf->setFontSubsetting(true);
			$pdf->setPrintHeader(false);
			$pdf->setPrintFooter(false);
			
			switch($_ETIQUETA)
			{
				case 'PIMACO-A4251':
				case 'PIMACO-A4351':
					$labelrightspace = 18.5;
					$labeltopspace   = 4;
					break;
				case 'MAXPRINT-A4351':
					$labelrightspace = 18.5;
					$labeltopspace   = 4;
					break;
				default:
					break;
			}
			
			if($_BORDA == 1){ $border = 1; } else { $border = 0; }
			
			$headertext     = "";
			$labeltext      = $o_CODIGO."\n".$o_CLONE."\n".$_QTDE;
			$padding        = "";
			$image          = "";
			$imagewidth     = "";
			$imageheight    = "";
			$headerfontsize = 0;
			$fontsize       = 10;
			$idfontsize     = "";
			$barcode        = $o_CODIGO."|".$_QTDE."|";
			$nbw            = 0.30;
			$barcodewidth=((strlen($barcode)+3)*(15+2)+20)*$nbw;
			$bh=max(0.15*$barcodewidth+3,16); //at least 15% of length
			$barcodesize    = 20;
			$wantraligntext = "";
			
			$_IMPR_ETQ = 3;
			
			for($i=0;$i<$_QTDE_IMPR;$i++)
			{
				$pdf->Add_Label($headertext, $labeltext, $labelrightspace, $labeltopspace, $padding, $border, $image, $imagewidth, $imageheight,
												$headerfontsize, $fontsize, $idfontsize, $barcode, $nbw,$bh, $barcodesize, $wantraligntext );
			}
			
			## Gera nome unico para o arquivo e
			## Verifica se o nome gerado ja existe no repositorio de fotos de perfil.
			## Continua gerando até encontrar um nome valido.
			do 
			{
				$filename = md5(uniqid(time())).".pdf"; //nome que dará a imagem
				if (file_exists(PDF_PATH."/".$filename)) { $continua = true; } else { $continua = false; }
			} while ($continua == true);
			
			
			#etiquetas-qr--$oDATA.pdf
			
			//$oDATA = date('Ymdhis');
			$pdf->Output(PDF_PATH."/".$filename,'F');
			
			$link = PDF_URL."/".$filename;
			
			$ret_txt = "Etiquetas QR geradas com sucesso!<br/><br/><a href='http://".$link."' target='_blank' style='color:#0000FF'><b>Clique aqui para baixar o arquivo PDF.</b></a>";
			
			return "1|".$ret_txt."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		*/
		
	}
	
	function getValidRandomFilename($_PATH, $_EXTENSION, $_RETURN_TYPE)
	{
		$filename = '';
		
		if($_PATH)
		{
			do 
			{
				$filename_tmp = md5(uniqid(time())); //nome que dará a imagem
				$filename = $filename_tmp.".".$_EXTENSION; //nome que dará a imagem
				if (file_exists($_PATH."/".$filename)) { $continua = true; } else { $continua = false; }
			} while ($continua == true);
		}
		
		if($_RETURN_TYPE == 0)
		{
			return $filename_tmp;
		}
		else
		{
			return $filename;
		}
	}
	
	
#################################################################################
###########
#####
##
?>
