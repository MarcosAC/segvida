<?php
#################################################################################
## SEGVIDA - GERA LAUDO FINAL
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_laudo_final_vap.php
## Função..........: Gerar planilha excel com os dados consolidados do laudo final
##                 
#################################################################################
###########
#####
##

include_once "includes/config.php";
include_once "includes/aux_lib.php";
	
####
# GERA ARQUIVO REMESSA
####
function GeraLaudoFinalVAP($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PLAN, $_LD_MOD, $_MODELO)
{
	## Gera escapes das variaveis
	$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
	$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
	$sql_ANO = $mysqli->escape_String($_ANO);
	$sql_MES = $mysqli->escape_String($_MES);
	$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
	$sql_DDMMAAAA = '%d/%m/%Y';
	$sql_LD_PLAN    = $mysqli->escape_String($_LD_PLAN);
	$sql_LD_MOD     = $mysqli->escape_String($_LD_MOD);
	
	$_RET_MSG = $_RET_FILE = "";
	
	## Carrega dados do laudo para a planilha indicada
	if ($stmt = $mysqli->prepare(
	"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, 
          date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, 
          AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
          AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, 
          AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_proc_prod_1` as analise_proc_prod_1, 
          AA.`analise_obs_tarefa_1` as analise_obs_tarefa_1, AA.`analise_amostra_2` as analise_amostra_2, 
          date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, 
          AA.`analise_proc_prod_2` as analise_proc_prod_2, AA.`analise_obs_tarefa_2` as analise_obs_tarefa_2, 
          AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, 
          AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_proc_prod_3` as analise_proc_prod_3, 
          AA.`analise_obs_tarefa_3` as analise_obs_tarefa_3, AA.`analise_amostra_4` as analise_amostra_4, 
          date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, 
          AA.`analise_proc_prod_4` as analise_proc_prod_4, AA.`analise_obs_tarefa_4` as analise_obs_tarefa_4, 
          AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, 
          AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_proc_prod_5` as analise_proc_prod_5, 
          AA.`analise_obs_tarefa_5` as analise_obs_tarefa_5, AA.`analise_amostra_6` as analise_amostra_6, 
          date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, 
          AA.`analise_proc_prod_6` as analise_proc_prod_6, AA.`analise_obs_tarefa_6` as analise_obs_tarefa_6, AA.`jor_trab` as jor_trab, 
          AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, 
          AA.`fonte_geradora` as fonte_geradora, AA.`mitigacao` as mitigacao, 
          AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_num_serial_1` as coleta_num_serial_1, AA.`coleta_num_amostrador_1` as coleta_num_amostrador_1, AA.`coleta_num_relat_ensaio_1` as coleta_num_relat_ensaio_1, AA.`coleta_vazao_1` as coleta_vazao_1, AA.`coleta_tempo_amostragem_1` as coleta_tempo_amostragem_1, AA.`coleta_massa1_1` as coleta_massa1_1, AA.`coleta_massa2_1` as coleta_massa2_1, AA.`coleta_massa3_1` as coleta_massa3_1, AA.`coleta_massa4_1` as coleta_massa4_1, AA.`coleta_massa5_1` as coleta_massa5_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_num_serial_2` as coleta_num_serial_2, AA.`coleta_num_amostrador_2` as coleta_num_amostrador_2, AA.`coleta_num_relat_ensaio_2` as coleta_num_relat_ensaio_2, AA.`coleta_vazao_2` as coleta_vazao_2, AA.`coleta_tempo_amostragem_2` as coleta_tempo_amostragem_2, AA.`coleta_massa1_2` as coleta_massa1_2, AA.`coleta_massa2_2` as coleta_massa2_2, AA.`coleta_massa3_2` as coleta_massa3_2, AA.`coleta_massa4_2` as coleta_massa4_2, AA.`coleta_massa5_2` as coleta_massa5_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_num_serial_3` as coleta_num_serial_3, AA.`coleta_num_amostrador_3` as coleta_num_amostrador_3, AA.`coleta_num_relat_ensaio_3` as coleta_num_relat_ensaio_3, AA.`coleta_vazao_3` as coleta_vazao_3, AA.`coleta_tempo_amostragem_3` as coleta_tempo_amostragem_3, AA.`coleta_massa1_3` as coleta_massa1_3, AA.`coleta_massa2_3` as coleta_massa2_3, AA.`coleta_massa3_3` as coleta_massa3_3, AA.`coleta_massa4_3` as coleta_massa4_3, AA.`coleta_massa5_3` as coleta_massa5_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_num_serial_4` as coleta_num_serial_4, AA.`coleta_num_amostrador_4` as coleta_num_amostrador_4, AA.`coleta_num_relat_ensaio_4` as coleta_num_relat_ensaio_4, AA.`coleta_vazao_4` as coleta_vazao_4, AA.`coleta_tempo_amostragem_4` as coleta_tempo_amostragem_4, AA.`coleta_massa1_4` as coleta_massa1_4, AA.`coleta_massa2_4` as coleta_massa2_4, AA.`coleta_massa3_4` as coleta_massa3_4, AA.`coleta_massa4_4` as coleta_massa4_4, AA.`coleta_massa5_4` as coleta_massa5_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_num_serial_5` as coleta_num_serial_5, AA.`coleta_num_amostrador_5` as coleta_num_amostrador_5, AA.`coleta_num_relat_ensaio_5` as coleta_num_relat_ensaio_5, AA.`coleta_vazao_5` as coleta_vazao_5, AA.`coleta_tempo_amostragem_5` as coleta_tempo_amostragem_5, AA.`coleta_massa1_5` as coleta_massa1_5, AA.`coleta_massa2_5` as coleta_massa2_5, AA.`coleta_massa3_5` as coleta_massa3_5, AA.`coleta_massa4_5` as coleta_massa4_5, AA.`coleta_massa5_5` as coleta_massa5_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_num_serial_6` as coleta_num_serial_6, AA.`coleta_num_amostrador_6` as coleta_num_amostrador_6, AA.`coleta_num_relat_ensaio_6` as coleta_num_relat_ensaio_6, AA.`coleta_vazao_6` as coleta_vazao_6, AA.`coleta_tempo_amostragem_6` as coleta_tempo_amostragem_6, AA.`coleta_massa1_6` as coleta_massa1_6, AA.`coleta_massa2_6` as coleta_massa2_6, AA.`coleta_massa3_6` as coleta_massa3_6, AA.`coleta_massa4_6` as coleta_massa4_6, AA.`coleta_massa5_6` as coleta_massa5_6, 
          AA.`respirador` as respirador, AA.`cert_aprov` as cert_aprov, 
          concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
          concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
          upper(AA.`registro_rc`) as registro_rc, upper(AA.`registro_rt`) as registro_rt, 
          lower(AA.`img_ativ_filename`) as img_ativ_filename, 
          lower(AA.`logo_filename`) as logo_filename
       FROM `VAPOR` AA
INNER JOIN `CLIENTE` C
       ON C.`idCLIENTE` = AA.`idCLIENTE`
INNER JOIN `COLABORADOR` CLB1
       ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U1
       ON U1.`username` = CLB1.`username`
INNER JOIN `COLABORADOR` CLB2
       ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U2
       ON U2.`username` = CLB2.`username`
    WHERE AA.`idSYSTEM_CLIENTE` = ?
      AND AA.`idVAPOR` = ?
      LIMIT 1"
	)) 
	{
		$stmt->bind_param('sssssssssssssss', $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_PLAN);
		$stmt->execute();
		$stmt->store_result();
		
		// obtém variáveis a partir dos resultados. 
		$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_PROC_PROD_1, $o_ANALISE_OBS_TAREFA_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_PROC_PROD_2, $o_ANALISE_OBS_TAREFA_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_PROC_PROD_3, $o_ANALISE_OBS_TAREFA_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_PROC_PROD_4, $o_ANALISE_OBS_TAREFA_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_PROC_PROD_5, $o_ANALISE_OBS_TAREFA_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_TAREFA_EXEC_6, $o_ANALISE_PROC_PROD_6, $o_ANALISE_OBS_TAREFA_6, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_MITIGACAO, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_NUM_SERIAL_1, $o_COLETA_NUM_AMOSTRADOR_1, $o_COLETA_NUM_RELAT_ENSAIO_1, $o_COLETA_VAZAO_1, $o_COLETA_TEMPO_AMOSTRAGEM_1, $o_COLETA_MASSA1_1, $o_COLETA_MASSA2_1, $o_COLETA_MASSA3_1, $o_COLETA_MASSA4_1, $o_COLETA_MASSA5_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_NUM_SERIAL_2, $o_COLETA_NUM_AMOSTRADOR_2, $o_COLETA_NUM_RELAT_ENSAIO_2, $o_COLETA_VAZAO_2, $o_COLETA_TEMPO_AMOSTRAGEM_2, $o_COLETA_MASSA1_2, $o_COLETA_MASSA2_2, $o_COLETA_MASSA3_2, $o_COLETA_MASSA4_2, $o_COLETA_MASSA5_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_NUM_SERIAL_3, $o_COLETA_NUM_AMOSTRADOR_3, $o_COLETA_NUM_RELAT_ENSAIO_3, $o_COLETA_VAZAO_3, $o_COLETA_TEMPO_AMOSTRAGEM_3, $o_COLETA_MASSA1_3, $o_COLETA_MASSA2_3, $o_COLETA_MASSA3_3, $o_COLETA_MASSA4_3, $o_COLETA_MASSA5_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_NUM_SERIAL_4, $o_COLETA_NUM_AMOSTRADOR_4, $o_COLETA_NUM_RELAT_ENSAIO_4, $o_COLETA_VAZAO_4, $o_COLETA_TEMPO_AMOSTRAGEM_4, $o_COLETA_MASSA1_4, $o_COLETA_MASSA2_4, $o_COLETA_MASSA3_4, $o_COLETA_MASSA4_4, $o_COLETA_MASSA5_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_NUM_SERIAL_5, $o_COLETA_NUM_AMOSTRADOR_5, $o_COLETA_NUM_RELAT_ENSAIO_5, $o_COLETA_VAZAO_5, $o_COLETA_TEMPO_AMOSTRAGEM_5, $o_COLETA_MASSA1_5, $o_COLETA_MASSA2_5, $o_COLETA_MASSA3_5, $o_COLETA_MASSA4_5, $o_COLETA_MASSA5_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_NUM_SERIAL_6, $o_COLETA_NUM_AMOSTRADOR_6, $o_COLETA_NUM_RELAT_ENSAIO_6, $o_COLETA_VAZAO_6, $o_COLETA_TEMPO_AMOSTRAGEM_6, $o_COLETA_MASSA1_6, $o_COLETA_MASSA2_6, $o_COLETA_MASSA3_6, $o_COLETA_MASSA4_6, $o_COLETA_MASSA5_6, $o_RESPIRADOR, $o_CERT_APROV, $o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
		$stmt->fetch();
		
		// Formata Unescape de Textareas
		$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
		$o_ANALISE_PROC_PROD_1 = unescape_string($o_ANALISE_PROC_PROD_1);
		$o_ANALISE_OBS_TAREFA_1 = unescape_string($o_ANALISE_OBS_TAREFA_1);
		$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
		$o_ANALISE_PROC_PROD_2 = unescape_string($o_ANALISE_PROC_PROD_2);
		$o_ANALISE_OBS_TAREFA_2 = unescape_string($o_ANALISE_OBS_TAREFA_2);
		$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
		$o_ANALISE_PROC_PROD_3 = unescape_string($o_ANALISE_PROC_PROD_3);
		$o_ANALISE_OBS_TAREFA_3 = unescape_string($o_ANALISE_OBS_TAREFA_3);
		$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
		$o_ANALISE_PROC_PROD_4 = unescape_string($o_ANALISE_PROC_PROD_4);
		$o_ANALISE_OBS_TAREFA_4 = unescape_string($o_ANALISE_OBS_TAREFA_4);
		$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
		$o_ANALISE_PROC_PROD_5 = unescape_string($o_ANALISE_PROC_PROD_5);
		$o_ANALISE_OBS_TAREFA_5 = unescape_string($o_ANALISE_OBS_TAREFA_5);
		$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
		$o_ANALISE_PROC_PROD_6 = unescape_string($o_ANALISE_PROC_PROD_6);
		$o_ANALISE_OBS_TAREFA_6 = unescape_string($o_ANALISE_OBS_TAREFA_6);
		
		// Formata Datas Nulas
		if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
		if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
		if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
		if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
		if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
		if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
		if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
		
		## Formata Analises
		{
			$o_ANALISES_DATA = array(
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_1,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_1,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_1,
					'proc_prod'   => $o_ANALISE_PROC_PROD_1,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_1
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_2,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_2,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_2,
					'proc_prod'   => $o_ANALISE_PROC_PROD_2,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_2
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_3,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_3,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_3,
					'proc_prod'   => $o_ANALISE_PROC_PROD_3,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_3
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_4,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_4,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_4,
					'proc_prod'   => $o_ANALISE_PROC_PROD_4,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_4
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_5,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_5,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_5,
					'proc_prod'   => $o_ANALISE_PROC_PROD_5,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_5
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_6,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_6,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_6,
					'proc_prod'   => $o_ANALISE_PROC_PROD_6,
					'obs_tarefa'  => $o_ANALISE_OBS_TAREFA_6
				)
			);
		}
		
		## Formata Coletas
		{
			$o_COLETAS_DATA = array(
				array(
					'amostra'          => $o_COLETA_AMOSTRA_1,
					'data'             => $o_COLETA_DATA_1,
					'num_serial'       => $o_COLETA_NUM_SERIAL_1,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_1,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_1,
					'vazao'            => $o_COLETA_VAZAO_1,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_1,
					'massa1'           => $o_COLETA_MASSA1_1,
					'massa2'           => $o_COLETA_MASSA2_1,
					'massa3'           => $o_COLETA_MASSA3_1,
					'massa4'           => $o_COLETA_MASSA4_1,
					'massa5'           => $o_COLETA_MASSA5_1
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_2,
					'data'             => $o_COLETA_DATA_2,
					'num_serial'       => $o_COLETA_NUM_SERIAL_2,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_2,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_2,
					'vazao'            => $o_COLETA_VAZAO_2,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_2,
					'massa1'           => $o_COLETA_MASSA1_2,
					'massa2'           => $o_COLETA_MASSA2_2,
					'massa3'           => $o_COLETA_MASSA3_2,
					'massa4'           => $o_COLETA_MASSA4_2,
					'massa5'           => $o_COLETA_MASSA5_2
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_3,
					'data'             => $o_COLETA_DATA_3,
					'num_serial'       => $o_COLETA_NUM_SERIAL_3,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_3,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_3,
					'vazao'            => $o_COLETA_VAZAO_3,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_3,
					'massa1'           => $o_COLETA_MASSA1_3,
					'massa2'           => $o_COLETA_MASSA2_3,
					'massa3'           => $o_COLETA_MASSA3_3,
					'massa4'           => $o_COLETA_MASSA4_3,
					'massa5'           => $o_COLETA_MASSA5_3
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_4,
					'data'             => $o_COLETA_DATA_4,
					'num_serial'       => $o_COLETA_NUM_SERIAL_4,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_4,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_4,
					'vazao'            => $o_COLETA_VAZAO_4,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_4,
					'massa1'           => $o_COLETA_MASSA1_4,
					'massa2'           => $o_COLETA_MASSA2_4,
					'massa3'           => $o_COLETA_MASSA3_4,
					'massa4'           => $o_COLETA_MASSA4_4,
					'massa5'           => $o_COLETA_MASSA5_4
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_5,
					'data'             => $o_COLETA_DATA_5,
					'num_serial'       => $o_COLETA_NUM_SERIAL_5,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_5,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_5,
					'vazao'            => $o_COLETA_VAZAO_5,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_5,
					'massa1'           => $o_COLETA_MASSA1_5,
					'massa2'           => $o_COLETA_MASSA2_5,
					'massa3'           => $o_COLETA_MASSA3_5,
					'massa4'           => $o_COLETA_MASSA4_5,
					'massa5'           => $o_COLETA_MASSA5_5
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_6,
					'data'             => $o_COLETA_DATA_6,
					'num_serial'       => $o_COLETA_NUM_SERIAL_6,
					'num_amostrador'   => $o_COLETA_NUM_AMOSTRADOR_6,
					'num_relat_ensaio' => $o_COLETA_NUM_RELAT_ENSAIO_6,
					'vazao'            => $o_COLETA_VAZAO_6,
					'tempo_amostragem' => $o_COLETA_TEMPO_AMOSTRAGEM_6,
					'massa1'           => $o_COLETA_MASSA1_6,
					'massa2'           => $o_COLETA_MASSA2_6,
					'massa3'           => $o_COLETA_MASSA3_6,
					'massa4'           => $o_COLETA_MASSA4_6,
					'massa5'           => $o_COLETA_MASSA5_6
				)
			);
		}
		
		##Se nao encontrou dados, retorna
		if ($stmt->num_rows == 0) 
		{
			$_RET_MSG  = "Nenhum registro encontrado!";
			$_RET_FILE = "";
		}
		else
		{
			# Gera Planilha
			/** Include PHPExcel */
			require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
			
			# Create new PHPExcel object
			//$objPHPExcel = new PHPExcel();
			//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
			$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
			
			$locale = 'pt_br';
			$validLocale = PHPExcel_Settings::setLocale($locale);
			
			# Set document properties
			$objPHPExcel->getProperties()
			            ->setCreator("SEGVIDA")
			            ->setLastModifiedBy("SEGVIDA")
			            ->setTitle("Laudo Vapor")
			            ->setSubject("")
			            ->setDescription("Laudo Vapor")
			            ->setKeywords("segvida laudo vapor")
			            ->setCategory("Laudo");
			
			## IMAGEM LOGOMARCA
			if($o_LOGO_FILENAME)
			{
				list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
				
				$img_logo_type2 = $_img_type[$img_logo_type];
				$img_logo_width_points  = $img_logo_width * 0.75;
				$img_logo_height_points = $img_logo_height * 0.75;
				
				/*
				if($_DEBUG == 1)
				{
					$img_logo_type2 = $_img_type[$img_logo_type];
					error_log("geralaudo_final_vap.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
				}
				*/
				
				switch($img_logo_type)
				{
					case '1'://gif
						$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '2'://jpg
						$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_logo');
				$objDrawing->setDescription('img_logo');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_logo_width_points);
				$objDrawing->setHeight($img_logo_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A1');
				$objDrawing->setImageResource($gd_img_logo);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			
			## EMPRESA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J1', $o_CLIENTE);
			
			## PLANILHA_NUM
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V1', $o_PLANILHA_NUM);
			
			## ANO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('W1', $o_ANO);
			
			## UNIDADE
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J2', $o_UNIDADE_SITE);
			
			## DATA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V2', $o_DATA_ELABORACAO);
			
			## AREA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J3', $o_AREA);
			
			## SETOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('O3', $o_SETOR);
			
			## GES
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V3', $o_GES);
			
			## CARGO_FUNCAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J4', $o_CARGO_FUNCAO);
			
			## CBO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V4', $o_CBO);
			
			## ATIV_MACRO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A6', $o_ATIV_MACRO);
			
			
			####
			# ANALISES
			$analises_total = count($o_ANALISES_DATA);
			for($a=0;$a<$analises_total;$a++)
			{
				$ITEM = (array) $o_ANALISES_DATA[$a];
				
				if($ITEM['amostra'])
				{
					//$o_ANALISES_DATA[$a][]
					
					$aa = 11 + $a;
					
					## ANALISE_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['amostra']);
					
					## ANALISE_DATA_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('B'.$aa, $ITEM['data']);
					
					## ANALISE_TAREFA_EXEC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['tarefa_exec']);
					
					## ANALISE_PROC_PROD
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('K'.$aa, $ITEM['proc_prod']);
					
					## ANALISE_OBS_TAREFA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('R'.$aa, $ITEM['obs_tarefa']);
				}
			}
			
			## JOR_TRAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A19', $o_JOR_TRAB);
			
			## TEMPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D19', $o_TEMPO_EXPO);
			
			## TIPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G19', $o_TIPO_EXPO);
			
			## MEIO_PROPAG
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J19', $o_MEIO_PROPAG);
			
			## FONTE_GERADORA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M19', $o_FONTE_GERADORA);
			
			## EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R19', $o_MITIGACAO);
			
			####
			# COLETAS
			$aa=0;
			$coletas_total = count($o_COLETAS_DATA);
			for($a=0;$a<$coletas_total;$a++)
			{
				$ITEM = (array) $o_COLETAS_DATA[$a];
				
				if($ITEM['amostra'])
				{
					$aa = 23 + $a;
					
					## COLETA_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['amostra']);
					
					## COLETA_DATA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('B'.$aa, $ITEM['data']);
					
					## COLETA_NUM_SERIAL
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('D'.$aa, $ITEM['num_serial']);
					
					## COLETA_NUM_AMOSTRADOR
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('F'.$aa, $ITEM['num_amostrador']);
					
					## COLETA_NUM_RELAT_ENSAIO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('H'.$aa, $ITEM['num_relat_ensaio']);
					
					## COLETA_VAZAO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J'.$aa, $ITEM['vazao']);
					
					## COLETA_TEMPO_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('L'.$aa, $ITEM['tempo_amostragem']);
					
					## COLETA_MASSA1
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('O'.$aa, $ITEM['massa1']);
					
					## COLETA_MASSA2
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('Q'.$aa, $ITEM['massa2']);
					
					## COLETA_MASSA3
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('S'.$aa, $ITEM['massa3']);
					
					## COLETA_MASSA4
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('U'.$aa, $ITEM['massa4']);
					
					## COLETA_MASSA5
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('W'.$aa, $ITEM['massa5']);
				}
			}
			
			## RESPIRADOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('C29', $o_RESPIRADOR);
			
			## CERT_APROV
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('I29', $o_CERT_APROV);
			
			## RESP_CAMPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D63', $o_RESP_CAMPO);
			
			## RESP_TECNICO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P63', $o_RESP_TECNICO);
			
			## REGISTRO_RC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D64', $o_REGISTRO_RC);
			
			## REGISTRO_RT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P64', $o_REGISTRO_RT);
			
			## IMAGEM ATIVIDADE
			if($o_IMG_ATIV_FILENAME)
			{
				list($img_ativ_width, $img_ativ_height, $img_ativ_type, $img_ativ_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
				
				$img_ativ_width_points  = $img_ativ_width * 0.75;
				$img_ativ_height_points = $img_ativ_height * 0.75;
				
				switch($img_ativ_type)
				{
					case '1'://gif
						$gd_img_ativ = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '2'://jpg
						$gd_img_ativ = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_ativ');
				$objDrawing->setDescription('img_ativ');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_ativ_width_points);
				$objDrawing->setHeight($img_ativ_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A32');
				$objDrawing->setImageResource($gd_img_ativ);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Laudo Particulado');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
			
			
			// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			
			## Grava arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			try 
			{
				$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
				$_RET_MSG  = "";
				$_RET_FILE = $_FILENAME;
			} 
			catch (Exception $e) 
			{
				error_log("gera_laudo_final_vap.php:\n\nobjWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME); -> ".$e->getMessage()."\n\n",0);
			}
			
		}
	}
	else
	{
		if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
		      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
		exit;
	}
	
	//error_log("gera_laudo_final_vap.php:\n\n returning -> ".$_RET_MSG."|".$_RET_FILE."|\n\n",0);
	
	return $_RET_MSG."|".$_RET_FILE."|";
	
}


#################################################################################
###########
#####
##
?>
