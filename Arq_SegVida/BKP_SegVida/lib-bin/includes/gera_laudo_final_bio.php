<?php
#################################################################################
## SEGVIDA - GERA LAUDO FINAL
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_laudo_final_bio.php
## Função..........: Gerar planilha excel com os dados consolidados do laudo final
##                 
#################################################################################
###########
#####
##

include_once "includes/config.php";
include_once "includes/aux_lib.php";
	
####
# GERA ARQUIVO REMESSA
####
function GeraLaudoFinalBIO($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PLAN, $_LD_MOD, $_MODELO)
{
	## Gera escapes das variaveis
	$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
	$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
	$sql_ANO = $mysqli->escape_String($_ANO);
	$sql_MES = $mysqli->escape_String($_MES);
	$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
	$sql_DDMMAAAA = '%d/%m/%Y';
	$sql_LD_PLAN    = $mysqli->escape_String($_LD_PLAN);
	$sql_LD_MOD     = $mysqli->escape_String($_LD_MOD);
	
	$_RET_MSG = $_RET_FILE = "";
	
	## Carrega dados do laudo para a planilha indicada
	if ($stmt = $mysqli->prepare(
	"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, 
          AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, 
          AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
          AA.`tarefa_exec` as tarefa_exec, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, 
          AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epi` as epi, AA.`epc` as epc, 
          AA.`enq_nr14` as enq_nr14, 
          concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
          concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
          upper(AA.`registro_rc`) as registro_rc, 
          upper(AA.`registro_rt`) as registro_rt, 
          lower(AA.`img_ativ_filename`) as img_ativ_filename, 
          lower(AA.`logo_filename`) as logo_filename
     FROM `BIOLOGICO` AA
INNER JOIN `CLIENTE` C
       ON C.`idCLIENTE` = AA.`idCLIENTE`
INNER JOIN `COLABORADOR` CLB1
       ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U1
       ON U1.`username` = CLB1.`username`
INNER JOIN `COLABORADOR` CLB2
       ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U2
       ON U2.`username` = CLB2.`username`
    WHERE AA.`idSYSTEM_CLIENTE` = ?
      AND AA.`idBIOLOGICO` = ?
      LIMIT 1"
	)) 
	{
		$stmt->bind_param('sss', $sql_DDMMAAAA, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_PLAN);
		$stmt->execute();
		$stmt->store_result();
		
		// obtém variáveis a partir dos resultados. 
		$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_TAREFA_EXEC, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPI, $o_EPC, $o_ENQ_NR14, $o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME, $o_LOGO_FILENAME);
		$stmt->fetch();
		
		// Formata Unescape de Textareas
		$o_TAREFA_EXEC = unescape_string($o_TAREFA_EXEC);
		
		// Formata Datas Nulas
		if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
		
		##Se nao encontrou dados, retorna
		if ($stmt->num_rows == 0) 
		{
			$_RET_MSG  = "Nenhum registro encontrado!";
			$_RET_FILE = "";
		}
		else
		{
			# Gera Planilha
			/** Include PHPExcel */
			require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
			
			# Create new PHPExcel object
			//$objPHPExcel = new PHPExcel();
			//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
			$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
			
			$locale = 'pt_br';
			$validLocale = PHPExcel_Settings::setLocale($locale);
			
			# Set document properties
			$objPHPExcel->getProperties()
			            ->setCreator("SEGVIDA")
			            ->setLastModifiedBy("SEGVIDA")
			            ->setTitle("Laudo Biológico")
			            ->setSubject("")
			            ->setDescription("Laudo Biológico")
			            ->setKeywords("segvida laudo biológico")
			            ->setCategory("Laudo");
			
			## IMAGEM LOGOMARCA
			if($o_LOGO_FILENAME)
			{
				list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
				
				$img_logo_type2 = $_img_type[$img_logo_type];
				$img_logo_width_points  = $img_logo_width * 0.75;
				$img_logo_height_points = $img_logo_height * 0.75;
				
				/*
				if($_DEBUG == 1)
				{
					$img_logo_type2 = $_img_type[$img_logo_type];
					error_log("geralaudo_final_bio.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
				}
				*/
				
				switch($img_logo_type)
				{
					case '1'://gif
						$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '2'://jpg
						$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_logo');
				$objDrawing->setDescription('img_logo');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_logo_width_points);
				$objDrawing->setHeight($img_logo_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A1');
				$objDrawing->setImageResource($gd_img_logo);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			## EMPRESA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J1', $o_CLIENTE);
			
			## PLANILHA_NUM
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V1', $o_PLANILHA_NUM);
			
			## ANO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('W1', $o_ANO);
			
			## UNIDADE
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J2', $o_UNIDADE_SITE);
			
			## DATA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V2', $o_DATA_ELABORACAO);
			
			## AREA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J3', $o_AREA);
			
			## SETOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('O3', $o_SETOR);
			
			## GES
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V3', $o_GES);
			
			## CARGO_FUNCAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J4', $o_CARGO_FUNCAO);
			
			## CBO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('V4', $o_CBO);
			
			## ATIV_MACRO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A6', $o_ATIV_MACRO);
			
			## TAREFA_EXEC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A11', $o_TAREFA_EXEC);
			
			## JOR_TRAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A19', $o_JOR_TRAB);
			
			## TEMPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D19', $o_TEMPO_EXPO);
			
			## TIPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G19', $o_TIPO_EXPO);
			
			## MEIO_PROPAG
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J19', $o_MEIO_PROPAG);
			
			## FONTE_GERADORA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M19', $o_FONTE_GERADORA);
			
			## EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('F21', $o_EPI);
			
			## EPC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('F22', $o_EPC);
			
			## ENQ_NR14
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A24', $o_ENQ_NR14);
			
			## RESP_CAMPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D35', $o_RESP_CAMPO);
			
			## RESP_TECNICO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P35', $o_RESP_TECNICO);
			
			## REGISTRO_RC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D36', $o_REGISTRO_RC);
			
			## REGISTRO_RT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P36', $o_REGISTRO_RT);
			
			## IMAGEM ATIVIDADE
			if($o_IMG_ATIV_FILENAME)
			{
				list($img_ativ_width, $img_ativ_height, $img_ativ_type, $img_ativ_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
				
				$img_ativ_width_points  = $img_ativ_width * 0.75;
				$img_ativ_height_points = $img_ativ_height * 0.75;
				
				switch($img_ativ_type)
				{
					case '1'://gif
						$gd_img_ativ = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '2'://jpg
						$gd_img_ativ = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_ativ = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_ativ');
				$objDrawing->setDescription('img_ativ');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_ativ_width_points);
				$objDrawing->setHeight($img_ativ_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('R11');
				$objDrawing->setImageResource($gd_img_ativ);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Laudo Biológico');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
			
			
			// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			
			## Grava arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			try 
			{
				$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
				$_RET_MSG  = "";
				$_RET_FILE = $_FILENAME;
			} 
			catch (Exception $e) 
			{
				error_log("gera_laudo_final_bio.php:\n\nobjWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME); -> ".$e->getMessage()."\n\n",0);
			}
			
		}
	}
	else
	{
		if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
		      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
		exit;
	}
	
	//error_log("gera_laudo_final_bio.php:\n\n returning -> ".$_RET_MSG."|".$_RET_FILE."|\n\n",0);
	
	return $_RET_MSG."|".$_RET_FILE."|";
	
}


#################################################################################
###########
#####
##
?>
