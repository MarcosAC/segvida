<?php
#################################################################################
## SEGVIDA - GERA LAUDO FINAL
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_laudo_final_vbrvci.php
## Função..........: Gerar planilha excel com os dados consolidados do laudo final
##                 
#################################################################################
###########
#####
##

include_once "includes/config.php";
include_once "includes/aux_lib.php";
	
####
# GERA ARQUIVO REMESSA
####
function GeraLaudoFinalVBRVCI($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_LD_PLAN, $_LD_MOD, $_MODELO)
{
	## Gera escapes das variaveis
	$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
	$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
	$sql_ANO = $mysqli->escape_String($_ANO);
	$sql_MES = $mysqli->escape_String($_MES);
	$sql_CLIENTE = $mysqli->escape_String($_CLIENTE);
	$sql_DDMMAAAA = '%d/%m/%Y';
	$sql_LD_PLAN    = $mysqli->escape_String($_LD_PLAN);
	$sql_LD_MOD     = $mysqli->escape_String($_LD_MOD);
	
	$_RET_MSG = $_RET_FILE = "";
	
	## Carrega dados do laudo para a planilha indicada
	if ($stmt = $mysqli->prepare(
	"SELECT C.`empresa` as cliente, AA.`ano` as ano, AA.`mes` as mes, AA.`planilha_num` as planilha_num, AA.`unidade_site` as unidade_site, date_format(AA.`data_elaboracao`,?) as data_elaboracao, AA.`area` as area, AA.`setor` as setor, AA.`ges` as ges, AA.`cargo_funcao` as cargo_funcao, AA.`cbo` as cbo, AA.`ativ_macro` as ativ_macro, 
          AA.`analises` as analises, AA.`analise_amostra_1` as analise_amostra_1, date_format(AA.`analise_data_amostragem_1`,?) as analise_data_amostragem_1, AA.`analise_paradigma_1` as analise_paradigma_1, AA.`analise_tarefa_exec_1` as analise_tarefa_exec_1, AA.`analise_amostra_2` as analise_amostra_2, date_format(AA.`analise_data_amostragem_2`,?) as analise_data_amostragem_2, AA.`analise_paradigma_2` as analise_paradigma_2, AA.`analise_tarefa_exec_2` as analise_tarefa_exec_2, AA.`analise_amostra_3` as analise_amostra_3, date_format(AA.`analise_data_amostragem_3`,?) as analise_data_amostragem_3, AA.`analise_paradigma_3` as analise_paradigma_3, AA.`analise_tarefa_exec_3` as analise_tarefa_exec_3, AA.`analise_amostra_4` as analise_amostra_4, date_format(AA.`analise_data_amostragem_4`,?) as analise_data_amostragem_4, AA.`analise_paradigma_4` as analise_paradigma_4, AA.`analise_tarefa_exec_4` as analise_tarefa_exec_4, AA.`analise_amostra_5` as analise_amostra_5, date_format(AA.`analise_data_amostragem_5`,?) as analise_data_amostragem_5, AA.`analise_paradigma_5` as analise_paradigma_5, AA.`analise_tarefa_exec_5` as analise_tarefa_exec_5, AA.`analise_amostra_6` as analise_amostra_6, date_format(AA.`analise_data_amostragem_6`,?) as analise_data_amostragem_6, AA.`analise_paradigma_6` as analise_paradigma_6, AA.`analise_tarefa_exec_6` as analise_tarefa_exec_6, 
          AA.`agente_risco` as agente_risco, AA.`jor_trab` as jor_trab, AA.`tempo_expo` as tempo_expo, AA.`tipo_expo` as tipo_expo, AA.`meio_propag` as meio_propag, AA.`fonte_geradora` as fonte_geradora, AA.`epc` as epc, AA.`fabr_epi` as fabr_epi, AA.`mod_epi` as mod_epi, AA.`cert_aprov` as cert_aprov, AA.`grau_atenuacao` as grau_atenuacao, 
          AA.`acel_1` as acel_1, AA.`acel_marca_1` as acel_marca_1, AA.`acel_modelo_1` as acel_modelo_1, AA.`acel_serial_1` as acel_serial_1, AA.`acel_cert_calib_1` as acel_cert_calib_1, 
          AA.`acel_2` as acel_2, AA.`acel_marca_2` as acel_marca_2, AA.`acel_modelo_2` as acel_modelo_2, AA.`acel_serial_2` as acel_serial_2, AA.`acel_cert_calib_2` as acel_cert_calib_2, 
          AA.`coletas` as coletas, AA.`coleta_amostra_1` as coleta_amostra_1, date_format(AA.`coleta_data_1`,?) as coleta_data_1, AA.`coleta_duracao_1` as coleta_duracao_1, AA.`coleta_aceleracao_x_1` as coleta_aceleracao_x_1, AA.`coleta_aceleracao_y_1` as coleta_aceleracao_y_1, AA.`coleta_aceleracao_z_1` as coleta_aceleracao_z_1, AA.`coleta_vdv_x_1` as coleta_vdv_x_1, AA.`coleta_vdv_y_1` as coleta_vdv_y_1, AA.`coleta_vdv_z_1` as coleta_vdv_z_1, AA.`coleta_fator_crista_1` as coleta_fator_crista_1, AA.`coleta_amostra_2` as coleta_amostra_2, date_format(AA.`coleta_data_2`,?) as coleta_data_2, AA.`coleta_duracao_2` as coleta_duracao_2, AA.`coleta_aceleracao_x_2` as coleta_aceleracao_x_2, AA.`coleta_aceleracao_y_2` as coleta_aceleracao_y_2, AA.`coleta_aceleracao_z_2` as coleta_aceleracao_z_2, AA.`coleta_vdv_x_2` as coleta_vdv_x_2, AA.`coleta_vdv_y_2` as coleta_vdv_y_2, AA.`coleta_vdv_z_2` as coleta_vdv_z_2, AA.`coleta_fator_crista_2` as coleta_fator_crista_2, AA.`coleta_amostra_3` as coleta_amostra_3, date_format(AA.`coleta_data_3`,?) as coleta_data_3, AA.`coleta_duracao_3` as coleta_duracao_3, AA.`coleta_aceleracao_x_3` as coleta_aceleracao_x_3, AA.`coleta_aceleracao_y_3` as coleta_aceleracao_y_3, AA.`coleta_aceleracao_z_3` as coleta_aceleracao_z_3, AA.`coleta_vdv_x_3` as coleta_vdv_x_3, AA.`coleta_vdv_y_3` as coleta_vdv_y_3, AA.`coleta_vdv_z_3` as coleta_vdv_z_3, AA.`coleta_fator_crista_3` as coleta_fator_crista_3, AA.`coleta_amostra_4` as coleta_amostra_4, date_format(AA.`coleta_data_4`,?) as coleta_data_4, AA.`coleta_duracao_4` as coleta_duracao_4, AA.`coleta_aceleracao_x_4` as coleta_aceleracao_x_4, AA.`coleta_aceleracao_y_4` as coleta_aceleracao_y_4, AA.`coleta_aceleracao_z_4` as coleta_aceleracao_z_4, AA.`coleta_vdv_x_4` as coleta_vdv_x_4, AA.`coleta_vdv_y_4` as coleta_vdv_y_4, AA.`coleta_vdv_z_4` as coleta_vdv_z_4, AA.`coleta_fator_crista_4` as coleta_fator_crista_4, AA.`coleta_amostra_5` as coleta_amostra_5, date_format(AA.`coleta_data_5`,?) as coleta_data_5, AA.`coleta_duracao_5` as coleta_duracao_5, AA.`coleta_aceleracao_x_5` as coleta_aceleracao_x_5, AA.`coleta_aceleracao_y_5` as coleta_aceleracao_y_5, AA.`coleta_aceleracao_z_5` as coleta_aceleracao_z_5, AA.`coleta_vdv_x_5` as coleta_vdv_x_5, AA.`coleta_vdv_y_5` as coleta_vdv_y_5, AA.`coleta_vdv_z_5` as coleta_vdv_z_5, AA.`coleta_fator_crista_5` as coleta_fator_crista_5, AA.`coleta_amostra_6` as coleta_amostra_6, date_format(AA.`coleta_data_6`,?) as coleta_data_6, AA.`coleta_duracao_6` as coleta_duracao_6, AA.`coleta_aceleracao_x_6` as coleta_aceleracao_x_6, AA.`coleta_aceleracao_y_6` as coleta_aceleracao_y_6, AA.`coleta_aceleracao_z_6` as coleta_aceleracao_z_6, AA.`coleta_vdv_x_6` as coleta_vdv_x_6, AA.`coleta_vdv_y_6` as coleta_vdv_y_6, AA.`coleta_vdv_z_6` as coleta_vdv_z_6, AA.`coleta_fator_crista_6` as coleta_fator_crista_6, 
          AA.`tempo_medicao` as tempo_medicao, AA.`nr09_aren` as nr09_aren, AA.`nr09_vdvr` as nr09_vdvr, AA.`nr15_aren` as nr15_aren, AA.`nr15_vdvr` as nr15_vdvr, 
          concat(U1.`nome`,' ',U1.`sobrenome`) as resp_campo, 
          concat(U2.`nome`,' ',U2.`sobrenome`) as resp_tecnico, 
          upper(AA.`registro_rc`) as registro_rc, 
          upper(AA.`registro_rt`) as registro_rt, 
          lower(AA.`img_ativ_filename_1`) as img_ativ_filename_1, 
          lower(AA.`img_ativ_filename_2`) as img_ativ_filename_2, 
          lower(AA.`logo_filename`) as logo_filename
       FROM `VIBR_VCI` AA
INNER JOIN `CLIENTE` C
       ON C.`idCLIENTE` = AA.`idCLIENTE`
INNER JOIN `COLABORADOR` CLB1
       ON CLB1.`idCOLABORADOR` = AA.`resp_campo_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U1
       ON U1.`username` = CLB1.`username`
INNER JOIN `COLABORADOR` CLB2
       ON CLB2.`idCOLABORADOR` = AA.`resp_tecnico_idcolaborador`
INNER JOIN `SYSTEM_USER_ACCOUNT` U2
       ON U2.`username` = CLB2.`username`
    WHERE AA.`idSYSTEM_CLIENTE` = ?
      AND AA.`idVIBR_VCI` = ?
      LIMIT 1"
	)) 
	{
		$stmt->bind_param('sssssssssssssss', $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_DDMMAAAA, $sql_COLETA_DATA_6, $sql_SID_idSYSTEM_CLIENTE, $sql_LD_PLAN);
		$stmt->execute();
		$stmt->store_result();
		
		// obtém variáveis a partir dos resultados. 
		$stmt->bind_result($o_CLIENTE, $o_ANO, $o_MES, $o_PLANILHA_NUM, $o_UNIDADE_SITE, $o_DATA_ELABORACAO, $o_AREA, $o_SETOR, $o_GES, $o_CARGO_FUNCAO, $o_CBO, $o_ATIV_MACRO, $o_ANALISES, $o_ANALISE_AMOSTRA_1, $o_ANALISE_DATA_AMOSTRAGEM_1, $o_ANALISE_PARADIGMA_1, $o_ANALISE_TAREFA_EXEC_1, $o_ANALISE_AMOSTRA_2, $o_ANALISE_DATA_AMOSTRAGEM_2, $o_ANALISE_PARADIGMA_2, $o_ANALISE_TAREFA_EXEC_2, $o_ANALISE_AMOSTRA_3, $o_ANALISE_DATA_AMOSTRAGEM_3, $o_ANALISE_PARADIGMA_3, $o_ANALISE_TAREFA_EXEC_3, $o_ANALISE_AMOSTRA_4, $o_ANALISE_DATA_AMOSTRAGEM_4, $o_ANALISE_PARADIGMA_4, $o_ANALISE_TAREFA_EXEC_4, $o_ANALISE_AMOSTRA_5, $o_ANALISE_DATA_AMOSTRAGEM_5, $o_ANALISE_PARADIGMA_5, $o_ANALISE_TAREFA_EXEC_5, $o_ANALISE_AMOSTRA_6, $o_ANALISE_DATA_AMOSTRAGEM_6, $o_ANALISE_PARADIGMA_6, $o_ANALISE_TAREFA_EXEC_6, $o_AGENTE_RISCO, $o_JOR_TRAB, $o_TEMPO_EXPO, $o_TIPO_EXPO, $o_MEIO_PROPAG, $o_FONTE_GERADORA, $o_EPC, $o_FABR_EPI, $o_MOD_EPI, $o_CERT_APROV, $o_GRAU_ATENUACAO, $o_ACEL_1, $o_ACEL_MARCA_1, $o_ACEL_MODELO_1, $o_ACEL_SERIAL_1, $o_ACEL_CERT_CALIB_1, $o_ACEL_2, $o_ACEL_MARCA_2, $o_ACEL_MODELO_2, $o_ACEL_SERIAL_2, $o_ACEL_CERT_CALIB_2, $o_COLETAS, $o_COLETA_AMOSTRA_1, $o_COLETA_DATA_1, $o_COLETA_DURACAO_1, $o_COLETA_ACELERACAO_X_1, $o_COLETA_ACELERACAO_Y_1, $o_COLETA_ACELERACAO_Z_1, $o_COLETA_VDV_X_1, $o_COLETA_VDV_Y_1, $o_COLETA_VDV_Z_1, $o_COLETA_FATOR_CRISTA_1, $o_COLETA_AMOSTRA_2, $o_COLETA_DATA_2, $o_COLETA_DURACAO_2, $o_COLETA_ACELERACAO_X_2, $o_COLETA_ACELERACAO_Y_2, $o_COLETA_ACELERACAO_Z_2, $o_COLETA_VDV_X_2, $o_COLETA_VDV_Y_2, $o_COLETA_VDV_Z_2, $o_COLETA_FATOR_CRISTA_2, $o_COLETA_AMOSTRA_3, $o_COLETA_DATA_3, $o_COLETA_DURACAO_3, $o_COLETA_ACELERACAO_X_3, $o_COLETA_ACELERACAO_Y_3, $o_COLETA_ACELERACAO_Z_3, $o_COLETA_VDV_X_3, $o_COLETA_VDV_Y_3, $o_COLETA_VDV_Z_3, $o_COLETA_FATOR_CRISTA_3, $o_COLETA_AMOSTRA_4, $o_COLETA_DATA_4, $o_COLETA_DURACAO_4, $o_COLETA_ACELERACAO_X_4, $o_COLETA_ACELERACAO_Y_4, $o_COLETA_ACELERACAO_Z_4, $o_COLETA_VDV_X_4, $o_COLETA_VDV_Y_4, $o_COLETA_VDV_Z_4, $o_COLETA_FATOR_CRISTA_4, $o_COLETA_AMOSTRA_5, $o_COLETA_DATA_5, $o_COLETA_DURACAO_5, $o_COLETA_ACELERACAO_X_5, $o_COLETA_ACELERACAO_Y_5, $o_COLETA_ACELERACAO_Z_5, $o_COLETA_VDV_X_5, $o_COLETA_VDV_Y_5, $o_COLETA_VDV_Z_5, $o_COLETA_FATOR_CRISTA_5, $o_COLETA_AMOSTRA_6, $o_COLETA_DATA_6, $o_COLETA_DURACAO_6, $o_COLETA_ACELERACAO_X_6, $o_COLETA_ACELERACAO_Y_6, $o_COLETA_ACELERACAO_Z_6, $o_COLETA_VDV_X_6, $o_COLETA_VDV_Y_6, $o_COLETA_VDV_Z_6, $o_COLETA_FATOR_CRISTA_6, $o_TEMPO_MEDICAO, $o_NR09_AREN, $o_NR09_VDVR, $o_NR15_AREN, $o_NR15_VDVR, $o_RESP_CAMPO, $o_RESP_TECNICO, $o_REGISTRO_RC, $o_REGISTRO_RT, $o_IMG_ATIV_FILENAME_1, $o_IMG_ATIV_FILENAME_2, $o_LOGO_FILENAME);
		$stmt->fetch();
		
		// Formata Unescape de Textareas
		$o_ANALISE_PARADIGMA_1 = unescape_string($o_ANALISE_PARADIGMA_1);
		$o_ANALISE_TAREFA_EXEC_1 = unescape_string($o_ANALISE_TAREFA_EXEC_1);
		$o_ANALISE_PARADIGMA_2 = unescape_string($o_ANALISE_PARADIGMA_2);
		$o_ANALISE_TAREFA_EXEC_2 = unescape_string($o_ANALISE_TAREFA_EXEC_2);
		$o_ANALISE_PARADIGMA_3 = unescape_string($o_ANALISE_PARADIGMA_3);
		$o_ANALISE_TAREFA_EXEC_3 = unescape_string($o_ANALISE_TAREFA_EXEC_3);
		$o_ANALISE_PARADIGMA_4 = unescape_string($o_ANALISE_PARADIGMA_4);
		$o_ANALISE_TAREFA_EXEC_4 = unescape_string($o_ANALISE_TAREFA_EXEC_4);
		$o_ANALISE_PARADIGMA_5 = unescape_string($o_ANALISE_PARADIGMA_5);
		$o_ANALISE_TAREFA_EXEC_5 = unescape_string($o_ANALISE_TAREFA_EXEC_5);
		$o_ANALISE_PARADIGMA_6 = unescape_string($o_ANALISE_PARADIGMA_6);
		$o_ANALISE_TAREFA_EXEC_6 = unescape_string($o_ANALISE_TAREFA_EXEC_6);
		
		// Formata Datas Nulas
		if($o_DATA_ELABORACAO == '00/00/0000'){ $o_DATA_ELABORACAO = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_1 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_1 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_2 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_2 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_3 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_3 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_4 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_4 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_5 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_5 = ''; }
		if($o_ANALISE_DATA_AMOSTRAGEM_6 == '00/00/0000'){ $o_ANALISE_DATA_AMOSTRAGEM_6 = ''; }
		if($o_COLETA_DATA_1 == '00/00/0000'){ $o_COLETA_DATA_1 = ''; }
		if($o_COLETA_DATA_2 == '00/00/0000'){ $o_COLETA_DATA_2 = ''; }
		if($o_COLETA_DATA_3 == '00/00/0000'){ $o_COLETA_DATA_3 = ''; }
		if($o_COLETA_DATA_4 == '00/00/0000'){ $o_COLETA_DATA_4 = ''; }
		if($o_COLETA_DATA_5 == '00/00/0000'){ $o_COLETA_DATA_5 = ''; }
		if($o_COLETA_DATA_6 == '00/00/0000'){ $o_COLETA_DATA_6 = ''; }
		
		## Formata Analises
		{
			$o_ANALISES_DATA = array(
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_1,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_1,
					'paradigma'   => $o_ANALISE_PARADIGMA_1,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_1
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_2,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_2,
					'paradigma'   => $o_ANALISE_PARADIGMA_2,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_2
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_3,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_3,
					'paradigma'   => $o_ANALISE_PARADIGMA_3,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_3
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_4,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_4,
					'paradigma'   => $o_ANALISE_PARADIGMA_4,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_4
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_5,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_5,
					'paradigma'   => $o_ANALISE_PARADIGMA_5,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_5
				),
				array(
					'amostra'     => $o_ANALISE_AMOSTRA_6,
					'data'        => $o_ANALISE_DATA_AMOSTRAGEM_6,
					'paradigma'   => $o_ANALISE_PARADIGMA_6,
					'tarefa_exec' => $o_ANALISE_TAREFA_EXEC_6
				)
			);
		}
		
		## Formata Acelerometros
		{
			$o_ACELS_DATA = array(
				array(
					'eqp'        => $o_ACEL_1,
					'marca'      => $o_ACEL_MARCA_1,
					'modelo'     => $o_ACEL_MODELO_1,
					'serial'     => $o_ACEL_SERIAL_1,
					'cert_calib' => $o_ACEL_CERT_CALIB_1,
				),
				array(
					'eqp'        => $o_ACEL_2,
					'marca'      => $o_ACEL_MARCA_2,
					'modelo'     => $o_ACEL_MODELO_2,
					'serial'     => $o_ACEL_SERIAL_2,
					'cert_calib' => $o_ACEL_CERT_CALIB_2,
				)
			);
		}
		
		## Formata Coletas
		{
			$o_COLETAS_DATA = array(
				array(
					'amostra'          => $o_COLETA_AMOSTRA_1,
					'data'             => $o_COLETA_DATA_1,
					'duracao'          => $o_COLETA_DURACAO_1,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_1,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_1,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_1,
					'vdv_x'            => $o_COLETA_VDV_X_1,
					'vdv_y'            => $o_COLETA_VDV_Y_1,
					'vdv_z'            => $o_COLETA_VDV_Z_1,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_1
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_2,
					'data'             => $o_COLETA_DATA_2,
					'duracao'          => $o_COLETA_DURACAO_2,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_2,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_2,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_2,
					'vdv_x'            => $o_COLETA_VDV_X_2,
					'vdv_y'            => $o_COLETA_VDV_Y_2,
					'vdv_z'            => $o_COLETA_VDV_Z_2,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_2
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_3,
					'data'             => $o_COLETA_DATA_3,
					'duracao'          => $o_COLETA_DURACAO_3,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_3,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_3,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_3,
					'vdv_x'            => $o_COLETA_VDV_X_3,
					'vdv_y'            => $o_COLETA_VDV_Y_3,
					'vdv_z'            => $o_COLETA_VDV_Z_3,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_3
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_4,
					'data'             => $o_COLETA_DATA_4,
					'duracao'          => $o_COLETA_DURACAO_4,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_4,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_4,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_4,
					'vdv_x'            => $o_COLETA_VDV_X_4,
					'vdv_y'            => $o_COLETA_VDV_Y_4,
					'vdv_z'            => $o_COLETA_VDV_Z_4,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_4
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_5,
					'data'             => $o_COLETA_DATA_5,
					'duracao'          => $o_COLETA_DURACAO_5,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_5,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_5,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_5,
					'vdv_x'            => $o_COLETA_VDV_X_5,
					'vdv_y'            => $o_COLETA_VDV_Y_5,
					'vdv_z'            => $o_COLETA_VDV_Z_5,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_5
				),
				array(
					'amostra'          => $o_COLETA_AMOSTRA_6,
					'data'             => $o_COLETA_DATA_6,
					'duracao'          => $o_COLETA_DURACAO_6,
					'aceleracao_x'     => $o_COLETA_ACELERACAO_X_6,
					'aceleracao_y'     => $o_COLETA_ACELERACAO_Y_6,
					'aceleracao_z'     => $o_COLETA_ACELERACAO_Z_6,
					'vdv_x'            => $o_COLETA_VDV_X_6,
					'vdv_y'            => $o_COLETA_VDV_Y_6,
					'vdv_z'            => $o_COLETA_VDV_Z_6,
					'fator_crista'     => $o_COLETA_FATOR_CRISTA_6
				)
			);
		}
		
		##Se nao encontrou dados, retorna
		if ($stmt->num_rows == 0) 
		{
			$_RET_MSG  = "Nenhum registro encontrado!";
			$_RET_FILE = "";
		}
		else
		{
			# Gera Planilha
			/** Include PHPExcel */
			require_once PHPEXCEL_PATH.'/Classes/PHPExcel.php';
			
			# Create new PHPExcel object
			//$objPHPExcel = new PHPExcel();
			//$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
			$objPHPExcel = PHPExcel_IOFactory::load(ANEXOS_PATH.'/'.$_MODELO);
			
			$locale = 'pt_br';
			$validLocale = PHPExcel_Settings::setLocale($locale);
			
			# Set document properties
			$objPHPExcel->getProperties()
			            ->setCreator("SEGVIDA")
			            ->setLastModifiedBy("SEGVIDA")
			            ->setTitle("Laudo Vibração VCI")
			            ->setSubject("")
			            ->setDescription("Laudo Vibração VCI")
			            ->setKeywords("segvida laudo vibração vci")
			            ->setCategory("Laudo");
			
			## IMAGEM LOGOMARCA
			if($o_LOGO_FILENAME)
			{
				list($img_logo_width, $img_logo_height, $img_logo_type, $img_logo_attr) = getimagesize(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
				
				$img_logo_type2 = $_img_type[$img_logo_type];
				$img_logo_width_points  = $img_logo_width * 0.75;
				$img_logo_height_points = $img_logo_height * 0.75;
				
				/*
				if($_DEBUG == 1)
				{
					$img_logo_type2 = $_img_type[$img_logo_type];
					error_log("geralaudo_final_vap.php:\n\n logo -> ".ANEXOS_PATH.'/'.$o_LOGO_FILENAME."\nlogo_width -> ".$img_logo_width."\n\nlogo_height ->".$img_logo_height."\n\nlogo_type -> ".$img_logo_type."(".$img_logo_type2.")\n\n",0);
				}
				*/
				
				switch($img_logo_type)
				{
					case '1'://gif
						$gd_img_logo = imagecreatefromgif(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '2'://jpg
						$gd_img_logo = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
					case '3'://png
					default:
						$gd_img_logo = imagecreatefrompng(ANEXOS_PATH.'/'.$o_LOGO_FILENAME);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_logo');
				$objDrawing->setDescription('img_logo');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_logo_width_points);
				$objDrawing->setHeight($img_logo_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A1');
				$objDrawing->setImageResource($gd_img_logo);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			## EMPRESA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G1', $o_CLIENTE);
			
			## PLANILHA_NUM
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R1', $o_PLANILHA_NUM);
			
			## ANO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('S1', $o_ANO);
			
			## UNIDADE
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G2', $o_UNIDADE_SITE);
			
			## DATA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R2', $o_DATA_ELABORACAO);
			
			## AREA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G3', $o_AREA);
			
			## SETOR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('L3', $o_SETOR);
			
			## GES
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R3', $o_GES);
			
			## CARGO_FUNCAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('I4', $o_CARGO_FUNCAO);
			
			## CBO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R4', $o_CBO);
			
			## ATIV_MACRO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A6', $o_ATIV_MACRO);
			
			####
			# ANALISES
			$analises_total = count($o_ANALISES_DATA);
			for($a=0;$a<$analises_total;$a++)
			{
				$ITEM = (array) $o_ANALISES_DATA[$a];
				
				if($ITEM['amostra'])
				{
					//$o_ANALISES_DATA[$a][]
					
					$aa = 11 + $a;
					
					## ANALISE_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('C'.$aa, $ITEM['amostra']);
					
					## ANALISE_DATA_AMOSTRAGEM
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['data']);
					
					## ANALISE_PARADIGMA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('E'.$aa, $ITEM['paradigma']);
					
					## ANALISE_TAREFA_EXEC
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('J'.$aa, $ITEM['tarefa_exec']);
				}
			}
			
			## AGENTE_RISCO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A19', $o_AGENTE_RISCO);
			
			## JOR_TRAB
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D19', $o_JOR_TRAB);
			
			## TEMPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('G19', $o_TEMPO_EXPO);
			
			## TIPO_EXPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J19', $o_TIPO_EXPO);
			
			## MEIO_PROPAG
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('M19', $o_MEIO_PROPAG);
			
			## FONTE_GERADORA
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P19', $o_FONTE_GERADORA);
			
			## EPC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D20', $o_EPC);
			
			## FABR_EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('D22', $o_FABR_EPI);
			
			## MOD_EPI
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('J22', $o_MOD_EPI);
			
			## CERT_APROV
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('P22', $o_CERT_APROV);
			
			## GRAU_ATENUACAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R22', $o_GRAU_ATENUACAO);
			
			####
			# ACELEROMETROS
			$acel_total = 2;
			for($a=0;$a<$acel_total;$a++)
			{
				$ITEM = (array) $o_ACELS_DATA[$a];
				
				if($ITEM['eqp'])
				{
					//$o_ACELS_DATA[$a][]
					
					$aa = 25 + $a;
					
					## EPQ
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['eqp']);
					
					## MARCA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('E'.$aa, $ITEM['marca']);
					
					## MODELO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('H'.$aa, $ITEM['modelo']);
					
					## SERIAL
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('L'.$aa, $ITEM['serial']);
					
					## CERT_CALIB
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('P'.$aa, $ITEM['cert_calib']);
				}
			}
			
			####
			# COLETAS
			$aa=0;
			$coletas_total = count($o_COLETAS_DATA);
			for($a=0;$a<$coletas_total;$a++)
			{
				$ITEM = (array) $o_COLETAS_DATA[$a];
				
				if($ITEM['amostra'])
				{
					$aa = 29 + $a;
					
					## COLETA_DATA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('A'.$aa, $ITEM['data']);
					
					## COLETA_AMOSTRA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('C'.$aa, $ITEM['amostra']);
					
					## COLETA_DURACAO
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('E'.$aa, $ITEM['duracao']);
					
					## COLETA_ACELERACAO_X
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('G'.$aa, $ITEM['aceleracao_x']);
					
					## COLETA_ACELERACAO_Y
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('I'.$aa, $ITEM['aceleracao_y']);
					
					## COLETA_ACELERACAO_Z
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('K'.$aa, $ITEM['aceleracao_z']);
					
					## COLETA_VDV_X
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('M'.$aa, $ITEM['vdv_x']);
					
					## COLETA_VDV_Y
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('O'.$aa, $ITEM['vdv_y']);
					
					## COLETA_VDV_Z
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('Q'.$aa, $ITEM['vdv_z']);
					
					## COLETA_FATOR_CRISTA
					$objPHPExcel->getActiveSheet()
					            ->SetCellValue('S'.$aa, $ITEM['fator_crista']);
				}
			}
			
			## TEMPO_MEDICAO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('E39', $o_TEMPO_MEDICAO);
			
			## NR09_AREN
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('H39', $o_NR09_AREN);
			
			## NR09_VDVR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('K39', $o_NR09_VDVR);
			
			## NR15_AREN
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('R39', $o_NR15_AREN);
			
			## NR15_VDVR
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('S39', $o_NR15_VDVR);
			
			## RESP_CAMPO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A83', $o_RESP_CAMPO);
			
			## RESP_TECNICO
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('A85', $o_RESP_TECNICO);
			
			## REGISTRO_RC
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('H83', $o_REGISTRO_RC);
			
			## REGISTRO_RT
			$objPHPExcel->getActiveSheet()
			            ->SetCellValue('H85', $o_REGISTRO_RT);
			
			## IMAGEM ATIVIDADE 1
			if($o_IMG_ATIV_FILENAME_1)
			{
				list($img_ativ_1_width, $img_ativ_1_height, $img_ativ_1_type, $img_ativ_1_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_1);
				
				$img_ativ_1_width_points  = $img_ativ_1_width * 0.75;
				$img_ativ_1_height_points = $img_ativ_1_height * 0.75;
				
				switch($img_ativ_1_type)
				{
					case '1'://gif
						$gd_img_ativ_1 = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_1);
						break;
					case '2'://jpg
						$gd_img_ativ_1 = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_1);
						break;
					case '3'://png
					default:
						$gd_img_ativ_1 = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_1);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_ativ_1');
				$objDrawing->setDescription('img_ativ_1');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_ativ_1_width_points);
				$objDrawing->setHeight($img_ativ_1_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('A44');
				$objDrawing->setImageResource($gd_img_ativ_1);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			
			## IMAGEM ATIVIDADE 2
			if($o_IMG_ATIV_FILENAME_2)
			{
				list($img_ativ_2_width, $img_ativ_2_height, $img_ativ_2_type, $img_ativ_2_attr) = getimagesize(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_2);
				
				$img_ativ_2_width_points  = $img_ativ_2_width * 0.75;
				$img_ativ_2_height_points = $img_ativ_2_height * 0.75;
				
				switch($img_ativ_2_type)
				{
					case '1'://gif
						$gd_img_ativ_2 = imagecreatefromgif(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_2);
						break;
					case '2'://jpg
						$gd_img_ativ_2 = imagecreatefromjpeg(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_2);
						break;
					case '3'://png
					default:
						$gd_img_ativ_2 = imagecreatefrompng(ANEXOS_PATH.'/'.$o_IMG_ATIV_FILENAME_2);
						break;
				}
				
				// Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
				$objDrawing  = new PHPExcel_Worksheet_MemoryDrawing();
				$objDrawing->setName('img_ativ_2');
				$objDrawing->setDescription('img_ativ_2');
				$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_PNG);
				$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
				$objDrawing->setResizeProportional(false);
				$objDrawing->setWidth($img_ativ_2_width_points);
				$objDrawing->setHeight($img_ativ_2_height_points);
				//$objDrawing->setOffsetX(110);
				$objDrawing->setCoordinates('K44');
				$objDrawing->setImageResource($gd_img_ativ_2);
				$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
			}
			
			
			
			####
			# FIALIZACAO
			// Rename worksheet
			$objPHPExcel->getActiveSheet()->setTitle('Laudo Vibração VCI');
			
			// Set active sheet index to the first sheet, so Excel opens this as the first sheet
			$objPHPExcel->setActiveSheetIndex(0);
			
			$_FILENAME = getValidRandomFilename(LAUDO_FINAL_PATH,'xlsx',1);
			
			
			// Use PCLZip rather than ZipArchive to create the Excel2007 OfficeOpenXML file
			PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
			
			## Grava arquivo
			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			try 
			{
				$objWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME);
				$_RET_MSG  = "";
				$_RET_FILE = $_FILENAME;
			} 
			catch (Exception $e) 
			{
				error_log("gera_laudo_final_vbrvci.php:\n\nobjWriter->save(LAUDO_FINAL_PATH."/".$_FILENAME); -> ".$e->getMessage()."\n\n",0);
			}
			
		}
	}
	else
	{
		if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
		      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
		exit;
	}
	
	//error_log("gera_laudo_final_vbrvci.php:\n\n returning -> ".$_RET_MSG."|".$_RET_FILE."|\n\n",0);
	
	return $_RET_MSG."|".$_RET_FILE."|";
	
}


#################################################################################
###########
#####
##
?>
