<?php
#################################################################################
## PRODFY.COM.BR
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: language.php
## Função..........: Definicao de constantes dos textos do sistema
## Funcionalidade..: Idioma EN-US
##                   Textos em ingles dos Estados Unidos
## Utilizacao......: include_once "MAIN_CGI_PATH/includes/language/en-us/language.php";
## Pré-requisitos..: Nenhum
##
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Definicoes
#################################################################################

####
# index.php
####
define("TXT_PG_MN_INICIO"              ,"Start");
define("TXT_PG_MN_ACESSO"              ,"Access");
define("TXT_PG_MN_PLANOS"              ,"Plans");
define("TXT_PG_MN_COLABS"              ,"Colaborators");
define("TXT_PG_MN_USERS"               ,"Users");
define("TXT_PG_MN_SOBRE"               ,"About");
define("TXT_PG_MN_CONTATO"             ,"Contact");

####
# usuarios.php
####
define("TXT_PG_USUARIOS_BT_CRIAR_CONTA"   ,"Create My User Account");


####
# Geral - Paginas
####
define("TXT_PAGE_TITLE_PREFIX"       ,"Prodfy Seedlings");
define("TXT_SISTEMA_CONTROLE_MUDAS"  ,"Production Control System For Seedlings");
define("TXT_PAGE_TITLE"              ,"Prodfy Seedlings - Production Control System For Seedlings");
define("TXT_META_DESCRIPTION"        ,"Prodify Seedlings - Production Control System For Seedlings");
define("TXT_META_AUTHOR"             ,"Rodrigo Leite Gomide");
define("TXT_META_KEYWORDS"           ,"Prodify, Seedlings, Management, Administration, Production, System, Web");
define("TXT_META_SUBJECT"            ,"Prodify Seedlings - Production Control System For Seedlings");
define("TXT_META_COPYRIGHT"          ,"Rodrigo Leite Gomide");
define("TXT_META_ABSTRACT"           ,"Prodify Seedlings - Production Control System For Seedlings");
define("TXT_META_LANGUAGE"           ,"en-us");
define("TXT_TERMOS_DE_USO"           ,"Terms of Use");
define("TXT_POLITICA_DE_PRIVACIDADE" ,"Privacy Policy");
define("TXT_MODAL_BTN_FECHAR"        ,"Close");
define("TXT_MODAL_MSG_CARREGANDO"    ,"Loading...");


####
# Geral - Sistema
####
define("TXT_FALHA_TRANSF_DADOS"           ,"Failed to transfer data. Please try again!");
define("TXT_NAO_POSSIVEL_CONNECTAR"       ,"Unable to connect:");
define("TXT_SEM_ACESSO_AO_DB"             ,"No DB access:");
define("TXT_QUERY_INVALIDA"               ,"Invalid Query:");
define("TXT_INCAPAZ_DE_PREPARAR_SQL_STMT" ,"Unable to prepare SQL statement!");
define("TXT_ERRO_SOLICITACAO"             ,"There was an error processing the request. Please try again!");
define("TXT_ERRO_DADOS_OBR"               ,"Mandatory data was not transferred. Please try again!");
define("TXT_USERNAME_LIBERADO"            ,"Username Released!");
define("TXT_USERNAME_JA_EM_USO"           ,"Username already in use!");
define("TXT_USUARIO_NAO_LOGADO"           ,"User is not logged in!");
define("TXT_FAVOR_EFETUAR_NOVO_LOGIN"     ,"Please re-login and try again!");
define("TXT_SYS_NO_SESSION"               ,"Login expired. Please login again!");

####
# Plano
####
define("TXT_PLANO_TIPO_M"                   ,"Month");
define("TXT_PLANO_TIPO_A"                   ,"Year");
define("TXT_MENSAL"                         ,"Monthly");
define("TXT_ANUAL"                          ,"Yearly");
define("TXT_PLANO_INEXISTENTE"              ,"Plan non-existent!");
define("TXT_MSG_CONTRATA_PLANO_SELECIONADO" ,"Plan selected for acquisition");
define("TXT_PAGTO_TOKEN_INEXISTENTE"        ,"Inexistent Token!");
define("TXT_INCAPAZ_CARREGAR_DADOS_PLANO"   ,"Unable to load data from the contracted plan!");
define("TXT_LIMITE_PLANO_ATINGIDO_MUDAS"    ,"Reached the contracted plan varieties of seedlings limit! Switch to a new plan that best meets your current need or contact financial support (financeiro@prodfy.com.br) to prepare a customized Plan.");
define("TXT_LIMITE_PLANO_ATINGIDO_COLABS"   ,"Reached the contracted plan collaborators limit! Switch to a new plan that best meets your current need or contact financial support (financeiro@prodfy.com.br) to prepare a customized Plan.");
define("TXT_LIMITE_PLANO_ATINGIDO_DISPS"    ,"Reached the contracted plan devices limit! Switch to a new plan that best meets your current need or contact financial support (financeiro@prodfy.com.br) to prepare a customized Plan.");
define("TXT_LIMITE_PLANO_ATINGIDO"          ,"Reached the contracted plan records limit! Switch to a new plan that best meets your current need or contact financial support (financeiro@prodfy.com.br) to prepare a customized Plan.");


####
# Templates de emails para envio
####
define("EMAIL_TEMPLATE_CONTATO_FORM"          ,"contato_via_website");
define("EMAIL_TEMPLATE_REGISTRO"              ,"registro_cliente_prodfy_mudas");
define("EMAIL_TEMPLATE_CONF_REGISTRO"         ,"confirmacao_registro_cliente_prodfy_mudas");
define("EMAIL_TEMPLATE_REGISTRO_USER"         ,"registro_usuario_prodfy_mudas");
define("EMAIL_TEMPLATE_CONF_REGISTRO_USER"    ,"confirmacao_registro_usuario_prodfy_mudas");
define("EMAIL_TEMPLATE_NOVA_SENHA"            ,"esqueci_senha_passo1");
define("EMAIL_TEMPLATE_NOVA_SENHA_CONCLUIDO"  ,"esqueci_senha_concluido");
define("EMAIL_TEMPLATE_CONF_PAGTO"            ,"confirmacao_pagto_cliente_prodfy_mudas");

####
# Contato via Form
####
define("TXT_EMAIL_SUBJECT_CONTATO_FORM" ,"Prodfy Seedlings - Contact by Website");
define("TXT_MSG_CONTATO_NAO_ENVIADO"    ,"E-Mail can not be sent!");
define("TXT_MSG_CONTATO_ENVIADO"        ,"Your message has been sent successfully. Thank you!");

####
# Registro
####
define("TXT_EMAIL_SUBJECT_REGISTRO"     ,"Prodfy Seedlings - Registration Confirmation");
define("TXT_MSG_REGISTRO_EFETIVADO"     ,"Registration successfully completed! A message has just been sent to the informed email. Activate your account by clicking the verification link.");

####
# Confirma Registro
####
define("TXT_TOKEN_INEXISTENTE"                ,"Confirmation Token Inexistent!");
define("TXT_EMAIL_SUBJECT_CONTA_ATIVADA"      ,"Prodfy Seedlings - Account Enabled and Ready to Use");
define("TXT_EMAIL_SUBJECT_CONTA_REGISTRADA"   ,"Prodfy Seedlings - Registered and Ready to Use Account");
define("TXT_MSG_CONTA_ATIVADA"                ,"Account Enabled Successfully!");
define("TXT_MSG_CONTA_REGISTRADA"             ,"Account registered successfully!");
define("TXT_CONF_REGISTRO_ACESSAR"            ,"Start");

####
# Redefinicao de Senha
####
define("TXT_EMAIL_SUBJECT_REDEFINICAO_SENHA"      ,"Prodfy Seedlings - Password Reset");
define("TXT_MSG_REDEFINICAO_SENHA_INSTRUCOES"     ,"A message has just been sent to your e-mail with instructions for resetting your access password.");
define("LB_RESETPWD_SENHA_REDEFINIDA_COM_SUCESSO" ,"Password Reset Successful!");
define("LB_RESETPWD_INSTRUCAO_INICIAL"            ,"To define a new access password, fill out the form below and click on NEXT:");
define("LB_RESETPWD_USERNAME"                     ,"USERNAME:");
define("LB_RESETPWD_EMAIL"                        ,"E-MAIL:");
define("LB_RESETPWD_SENHA"                        ,"*Password");
define("LB_RESETPWD_SENHA_CONFIRMACAO"            ,"*Password for confirmation");
define("LB_RESETPWD_CAPTCHA"                      ,"*Type the code");
define("LB_RESETPWD_BTN_AVANCAR"                  ,"Next");
define("TXT_EMAIL_SUBJECT_SENHA_ALTERADA"         ,"Prodfy Seedlings - Password Changed Successfully");
define("TXT_RESETPWD_TOKEN_EXPIRADO"              ,"The validity of your password change request has expired. Please generate new request and try again!");
//The validity of your password change request has expired. Please generate a new request and try again!
//La validez de una solicitud de cambio de contraseña ha caducado. Por favor, generar nuevos solitação y vuelve a intentarlo!
define("TXT_RESETPWD_SENHA_ALTERADA_COM_SUCESSO"  ,"Password changed successfully!");
//Password changed successfully!
//Contraseña cambiada correctamente!


####
# Confirma Pagamento
####
define("TXT_CONF_PAGTO_TOKEN_INEXISTENTE"     ,"Inexistent Token!");
define("TXT_CONF_PAGTO_MSG_PLANO_ATIVADO"     ,"Contracted Plan activated successfully!");


####
# Login
####
define("TXT_USERNAME_SENHA_INVALIDOS"       ,"Invalid username and/or password. Please try again!");
define("TXT_USERNAME_INVALIDO"              ,"Invalid username. Please try again!");
define("TXT_PASSWORD_INVALIDO"              ,"Invalid password. Please try again!");
define("TXT_LOGIN_BLOQUEADO"                ,"User blocked due to multiple failed login attempts! Reset your access password to unlock your username.");
define("TXT_LOGIN_OK"                       ,"Login successfully authenticated!");
define("TXT_INCAPAZ_DE_GERAR_SESSAO"        ,"Error trying to generate secure session. Please try again.");
define("TXT_LOGIN_REENVIO_LINK_CONFIRMACAO" ,"Your account has not yet been activated! A message has just been sent to the registered e-mail. Activate your account by clicking the confirmation link.");
define("TXT_USUARIO_NAO_AUTENTICADO"        ,"User not authenticated. Please login again!");
define("TXT_ACESSO_COLABORADOR_INATIVO"     ,"Collaborator access is inactive. Please contact the system administrator!");


####
# Portal - Logado
####
define("TXT_USER_CATEG_ADMIN"       ,"ADMINISTRATOR");
define("TXT_USER_CATEG_COLAB"       ,"COLLABORATOR");
define("TXT_USER_CATEG_USU"         ,"USER");

####
# Menu Prodfy Mudas
####
define("TXT_PRODFY_SIDE_MENU_PAINEL_DE_CONTROLE"           ,"Control Panel");
define("TXT_PRODFY_SIDE_MENU_PAINEL_DE_CONTROLE_DASHBOARD" ,"Dashboard");
define("TXT_PRODFY_SIDE_MENU_PROD"                         ,"Production");
define("TXT_PRODFY_SIDE_MENU_PROD_VISAO_GERAL"             ,"Overview");
define("TXT_PRODFY_SIDE_MENU_PROD_HISTORICO"               ,"Historic");
define("TXT_PRODFY_SIDE_MENU_PROD_INVENTARIO"              ,"Inventory");
define("TXT_PRODFY_SIDE_MENU_PROD_LOTE"                    ,"Lot");
define("TXT_PRODFY_SIDE_MENU_PROD_OBJETIVO"                ,"Goal");
define("TXT_PRODFY_SIDE_MENU_PROD_PONTOS_DE_CONTROLE"      ,"Control Points");
define("TXT_PRODFY_SIDE_MENU_PROD_ESTAGIO"                 ,"Stage");
define("TXT_PRODFY_SIDE_MENU_MUDAS"                        ,"Seedlings");
define("TXT_PRODFY_SIDE_MENU_MUDAS_MUDA"                   ,"Seedling");
define("TXT_PRODFY_SIDE_MENU_MUDAS_DOENCA"                 ,"Disease");
define("TXT_PRODFY_SIDE_MENU_MUDAS_ESPECIE"                ,"Species");
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO"               ,"Administrative");
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO_CLIENTE"       ,"Customer");
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO_CONTRATO"      ,"Contract");
define("TXT_PRODFY_SIDE_MENU_RELATORIOS"                   ,"Reports");
define("TXT_PRODFY_SIDE_MENU_RELATORIOS_PRODUCAO"          ,"Production");
define("TXT_PRODFY_SIDE_MENU_RELATORIOS_MUDAS"             ,"Seedlings");
define("TXT_PRODFY_SIDE_MENU_RELATORIOS_CLIENTES"          ,"Customers");
define("TXT_PRODFY_SIDE_MENU_RELATORIOS_COLABORADORES"     ,"Collaborators");
define("TXT_PRODFY_SIDE_MENU_CFG"                          ,"Settings");
define("TXT_PRODFY_SIDE_MENU_CFG_COLABORADORES"            ,"Collaborators");
define("TXT_PRODFY_SIDE_MENU_CFG_DISPOSITIVOS"             ,"Devices");
define("TXT_PRODFY_SIDE_MENU_CFG_SINCRONIA"                ,"Sync");
define("TXT_PRODFY_SIDE_MENU_CFG_PLANO_CONTRATADO"         ,"Contracted Plan");
define("TXT_PRODFY_PROFILE_MENU_CAD"                       ,"Registration");
define("TXT_PRODFY_PROFILE_MENU_CFG"                       ,"Settings");
define("TXT_PRODFY_PROFILE_MENU_AJUDA"                     ,"Help");
define("TXT_PRODFY_PROFILE_MENU_LOGOUT"                    ,"Exit");

####
# Modulo Mensagens
####
define("TXT_MOD_MSGS_VER_TODAS_AS_MSGS"                ,"View All Messages");
define("TXT_MOD_MSGS_NENHUMA_MSG_ENCONTRADA"           ,"No Message Found");
define("TXT_MOD_MSGS_ENVIAR_MSG"                       ,"Send Message");
define("TXT_MOD_MSGS_APAGAR_MSG"                       ,"Delete Message");
define("TXT_MOD_MSGS_REPLY"                            ,"Reply");
define("TXT_MOD_MSGS_REPLY_TO_ALL"                     ,"Reply to All");
define("TXT_MOD_MSGS_ADD_ANEXO"                        ,"Include Attachment");
define("TXT_MOD_MSGS_NENHUM_COLABORADOR_CONFIGURADO"   ,"No collaborator set up!");

####
# Modulo Perfil
####
define("TXT_MOD_PERFIL_PERFIL"                 ,"Perfil");
define("TXT_MOD_PERFIL_CADASTRO"               ,"Cadastro");
define("TXT_MOD_PERFIL_NOME"                   ,"Nome");
define("TXT_MOD_PERFIL_SOBRENOME"              ,"Sobrenome");
define("TXT_MOD_PERFIL_EMAIL"                  ,"E-Mail");
define("TXT_MOD_PERFIL_CPF"                    ,"CPF");
define("TXT_MOD_PERFIL_CNPJ"                   ,"CNPJ");
define("TXT_MOD_PERFIL_RAZAO_SOCIAL"           ,"Razão Social");
define("TXT_MOD_PERFIL_FONE_FIXO"              ,"Telefone Fixo");
define("TXT_MOD_PERFIL_CELULAR"                ,"Celular");
define("TXT_MOD_PERFIL_PAIS"                   ,"País");
define("TXT_MOD_PERFIL_IDIOMA"                 ,"Idioma");
define("TXT_MOD_PERFIL_BRASIL"                 ,"Brasil");
define("TXT_MOD_PERFIL_PORTUGUES_BRASIL"       ,"Português Brasil");
define("TXT_MOD_PERFIL_US_ENGLISH"             ,"US English");
define("TXT_MOD_PERFIL_ESPANHOL"               ,"Español");
define("TXT_MOD_PERFIL_CEP"                    ,"CEP");
define("TXT_MOD_PERFIL_CEP_MSG"                ,"Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.");
define("TXT_MOD_PERFIL_UF"                     ,"Estado");
define("TXT_MOD_PERFIL_CIDADE"                 ,"Cidade");
define("TXT_MOD_PERFIL_BAIRRO"                 ,"Bairro");
define("TXT_MOD_PERFIL_NUMERO"                 ,"Número");
define("TXT_MOD_PERFIL_COMPL"                  ,"Complemento");
define("TXT_MOD_PERFIL_ALTERAR_FOTO"           ,"Alterar Foto");
define("TXT_MOD_PERFIL_IMG_MSG"                ,"Imagem de 128x128 pixels, JPG, JPEG, PNG ou GIF, até 80kb");
define("TXT_MOD_PERFIL_ARQUIVO"                ,"Arquivo");
define("TXT_MOD_PERFIL_ENVIAR"                 ,"Enviar");
define("TXT_MOD_PERFIL_FOTO_ATUALIZADA"        ,"Profile photo updated successfully. Please log in again to update the system!");

####
# Operacoes SQL
####
define("TXT_MYSQLI_OPERACAO_INVALIDA"     ,"Invalid SQL Operation!");
define("TXT_MYSQLI_INSERT_OK"             ,"Registration successful!");
define("TXT_MYSQLI_INSERT_ERRO"           ,"Error while trying to register!");
define("TXT_MYSQLI_UPDATE_OK"             ,"Record updated successfully!");
define("TXT_MYSQLI_UPDATE_ERRO"           ,"Error when trying to update the record!");
define("TXT_MYSQLI_UPDATE_ID_INEXISTENTE" ,"Unable to perform update. Registration non existent!");
define("TXT_MYSQLI_DELETE_OK"             ,"Registration(s) deleted successfully!");
define("TXT_MYSQLI_DELETE_ERRO"           ,"Error trying to delete the record(s)!");
define("TXT_MYSQLI_DELETE_ID_INEXISTENTE" ,"Unable to delete. Record non existent!");
define("TXT_MYSQLI_SELECT_NO_RECORD"      ,"No records found!");
define("TXT_MYSQLI_REGISTRO_JA_EXISTENTE" ,"Record already exists!");
define("TXT_MYSQLI_REGISTRO_INEXISTENTE"  ,"Record non existent!");

####
# File Upload
####
define("TXT_UPLOAD_ARQUIVO_INVALIDO"            ,"Invalid file!");
define("TXT_UPLOAD_ARQUIVO_JA_EXISTE"           ,"File already exists!");
define("TXT_UPLOAD_APENAS_JPG_JPEG_PNG_OU_GIF"  ,"Only JPG, JPEG, PNG or GIF files!");
define("TXT_UPLOAD_APENAS_PDF"                  ,"Only PDF files!");

####
# Datatable
####
define("TXT_DATATABLE_NENHUM_REGISTRO_ENCONTRADO"    ,"No records found");
define("TXT_DATATABLE_MOSTRANDO_0_DE_0_REGISTROS"    ,"Showing 0 to 0 of 0 records");

####
# Modulos - Geral
####
define("TXT_ATIVO"                                   ,"Active");
define("TXT_INATIVO"                                 ,"Inactive");
define("TXT_FICHA_ULT_UPDATE"                        ,"Last update:");
define("TXT_CAD_MODAL_NOVO_REGISTRO"                 ,"NEW RECORD");
define("TXT_CAD_MODAL_CAMPOS_COM_A_SAO_OBRIGATORIOS" ,"Fields with * are required");
define("TXT_BT_REGISTER"                             ,"Register");
define("TXT_BT_SAVE"                                 ,"Save");
define("TXT_BT_SEND"                                 ,"Send");
define("TXT_NIVEL_ACESSO_ACESSAR"                    ,"Access");
define("TXT_NIVEL_ACESSO_EDITAR"                     ,"Edit");
define("TXT_NIVEL_ACESSO_APAGAR"                     ,"Delete");

























####
# view-cfg-colabs.php
####
define("TXT_CFG_COLABS_USUARIO_INEXISTENTE"    ,"Usuário inexistente!");
define("TXT_CFG_COLABS_USUARIO_EXISTE"         ,"Usuário existe!");
define("TXT_CFG_COLABS_USUARIO_NAO_CONFIRMADO" ,"Usuário informado ainda não confirmou os dados cadastrais!");
define("TXT_CFG_COLABS_USUARIO_INATIVO"        ,"Conta do usuário inativa!");
define("TXT_CFG_COLABS_INCAPAZ_CARREGAR_PLANO" ,"Incapaz de carregar dados do plano contratado!");
define("TXT_CFG_COLABS_FICHA_NOME"             ,"Nome:");
define("TXT_CFG_COLABS_FICHA_CPF"              ,"CPF:");
define("TXT_CFG_COLABS_FICHA_EMAIL"            ,"E-Mail:");
define("TXT_CFG_COLABS_FICHA_FONE1"            ,"Fone Fixo:");
define("TXT_CFG_COLABS_FICHA_CEL1"             ,"Celular:");
define("TXT_CFG_COLABS_FICHA_ENDERECO"         ,"Endereço:");
define("TXT_CFG_COLABS_FICHA_USUARIO"          ,"Usuário:");
define("TXT_CFG_COLABS_FICHA_INCLUSAO"         ,"Inclusão:");
define("TXT_CFG_COLABS_FICHA_ULT_ACESSO"       ,"Últ. Acesso:");
define("TXT_CFG_COLABS_FICHA_SITUACAO"         ,"Situação:");

####
# view-cfg-dashboard.php
####
define("TXT_PRODFY_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_PRODFY_SIDE_MENU_CFG_DASHBOARD","Dashboard");
define("TXT_CFG_DASHBOARD_CFG","cfg");
define("TXT_CFG_DASHBOARD_DASHBOARD","dashboard");
define("TXT_CFG_DASHBOARD_CONFIGURACAO","Configuração");
define("TXT_CFG_DASHBOARD_DASHBOARD","Dashboard");
define("TXT_CFG_DASHBOARD_PAINEL","Painel");
define("TXT_CFG_DASHBOARD_ORDEM","Ordem");
define("TXT_CFG_DASHBOARD_EXIBIR","Exibir");
define("TXT_CFG_DASHBOARD_O_CAMPO_EXIBIR_E_OBRIGATORIO","O campo EXIBIR é obrigatório!");
define("TXT_CFG_DASHBOARD_1","1");
define("TXT_CFG_DASHBOARD_SIM","Sim");
define("TXT_CFG_DASHBOARD_0","0");
define("TXT_CFG_DASHBOARD_NAO","Não");

####
# view-adm-cliente.php
####
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO","Administrativo");
define("TXT_PRODFY_SIDE_MENU_ADM_CLIENTE","Cliente");
define("TXT_ADM_CLIENTE_ADM","adm");
define("TXT_ADM_CLIENTE_CLIENTE","cliente");
define("TXT_ADM_CLIENTE_ADMINISTRATIVO","Administrativo");
define("TXT_ADM_CLIENTE_CLIENTE","Cliente");
define("TXT_ADM_CLIENTE_NOME_INTERNO","Nome Interno");
define("TXT_ADM_CLIENTE_O_CAMPO_NOME_INTERNO_E_OBRIGATORIO","O campo NOME INTERNO é obrigatório!");
define("TXT_ADM_CLIENTE_EMPRESA","Empresa");
define("TXT_ADM_CLIENTE_O_CAMPO_EMPRESA_E_OBRIGATORIO","O campo EMPRESA é obrigatório!");
define("TXT_ADM_CLIENTE_CNPJ","CNPJ");
define("TXT_ADM_CLIENTE_O_CAMPO_CNPJ_E_OBRIGATORIO","O campo CNPJ é obrigatório!");
define("TXT_ADM_CLIENTE_FONE_1","Fone 1");
define("TXT_ADM_CLIENTE_O_CAMPO_FONE_1_E_OBRIGATORIO","O campo FONE 1 é obrigatório!");
define("TXT_ADM_CLIENTE_FONE_2","Fone 2");
define("TXT_ADM_CLIENTE_E_MAIL","E-Mail");
define("TXT_ADM_CLIENTE_O_CAMPO_E_MAIL_E_OBRIGATORIO","O campo E-MAIL é obrigatório!");
define("TXT_ADM_CLIENTE_CEP","CEP");
define("TXT_ADM_CLIENTE_O_CAMPO_CEP_E_OBRIGATORIO","O campo CEP é obrigatório!");
define("TXT_ADM_CLIENTE_ESTADO","Estado");
define("TXT_ADM_CLIENTE_O_CAMPO_ESTADO_E_OBRIGATORIO","O campo ESTADO é obrigatório!");
define("TXT_ADM_CLIENTE_AC","AC");
define("TXT_ADM_CLIENTE_ACRE","ACRE");
define("TXT_ADM_CLIENTE_AL","AL");
define("TXT_ADM_CLIENTE_ALAGOAS","ALAGOAS");
define("TXT_ADM_CLIENTE_AM","AM");
define("TXT_ADM_CLIENTE_AMAZONAS","AMAZONAS");
define("TXT_ADM_CLIENTE_AP","AP");
define("TXT_ADM_CLIENTE_AMAPA","AMAPÁ");
define("TXT_ADM_CLIENTE_BA","BA");
define("TXT_ADM_CLIENTE_BAHIA","BAHIA");
define("TXT_ADM_CLIENTE_CE","CE");
define("TXT_ADM_CLIENTE_CEARA","CEARÁ");
define("TXT_ADM_CLIENTE_DF","DF");
define("TXT_ADM_CLIENTE_DISTRITO_FEDERAL","DISTRITO FEDERAL");
define("TXT_ADM_CLIENTE_ES","ES");
define("TXT_ADM_CLIENTE_ESPIRITO_SANTO","ESPÍRITO SANTO");
define("TXT_ADM_CLIENTE_GO","GO");
define("TXT_ADM_CLIENTE_GOIAS","GOIÁS");
define("TXT_ADM_CLIENTE_MA","MA");
define("TXT_ADM_CLIENTE_MARANHAO","MARANHÃO");
define("TXT_ADM_CLIENTE_MG","MG");
define("TXT_ADM_CLIENTE_MINAS_GERAIS","MINAS GERAIS");
define("TXT_ADM_CLIENTE_MS","MS");
define("TXT_ADM_CLIENTE_MATO_GROSSO_DO_SUL","MATO GROSSO DO SUL");
define("TXT_ADM_CLIENTE_MT","MT");
define("TXT_ADM_CLIENTE_MATO_GROSSO","MATO GROSSO");
define("TXT_ADM_CLIENTE_PA","PA");
define("TXT_ADM_CLIENTE_PARA","PARÁ");
define("TXT_ADM_CLIENTE_PB","PB");
define("TXT_ADM_CLIENTE_PARAIBA","PARAÍBA");
define("TXT_ADM_CLIENTE_PE","PE");
define("TXT_ADM_CLIENTE_PERNAMBUCO","PERNAMBUCO");
define("TXT_ADM_CLIENTE_PI","PI");
define("TXT_ADM_CLIENTE_PIAUI","PIAUÍ");
define("TXT_ADM_CLIENTE_PR","PR");
define("TXT_ADM_CLIENTE_PARANA","PARANÁ");
define("TXT_ADM_CLIENTE_RJ","RJ");
define("TXT_ADM_CLIENTE_RIO_DE_JANEIRO","RIO DE JANEIRO");
define("TXT_ADM_CLIENTE_RN","RN");
define("TXT_ADM_CLIENTE_RIO_GRANDE_DO_NORTE","RIO GRANDE DO NORTE");
define("TXT_ADM_CLIENTE_RO","RO");
define("TXT_ADM_CLIENTE_RONDONIA","RONDÔNIA");
define("TXT_ADM_CLIENTE_RR","RR");
define("TXT_ADM_CLIENTE_RORAIMA","RORAIMA");
define("TXT_ADM_CLIENTE_RS","RS");
define("TXT_ADM_CLIENTE_RIO_GRANDE_DO_SUL","RIO GRANDE DO SUL");
define("TXT_ADM_CLIENTE_SC","SC");
define("TXT_ADM_CLIENTE_SANTA_CATARINA","SANTA CATARINA");
define("TXT_ADM_CLIENTE_SE","SE");
define("TXT_ADM_CLIENTE_SERGIPE","SERGIPE");
define("TXT_ADM_CLIENTE_SP","SP");
define("TXT_ADM_CLIENTE_SAO_PAULO","SÃO PAULO");
define("TXT_ADM_CLIENTE_TO","TO");
define("TXT_ADM_CLIENTE_TOCANTINS","TOCANTINS");
define("TXT_ADM_CLIENTE_CIDADE","Cidade");
define("TXT_ADM_CLIENTE_O_CAMPO_CIDADE_E_OBRIGATORIO","O campo CIDADE é obrigatório!");
define("TXT_ADM_CLIENTE_BAIRRO","Bairro");
define("TXT_ADM_CLIENTE_O_CAMPO_BAIRRO_E_OBRIGATORIO","O campo BAIRRO é obrigatório!");
define("TXT_ADM_CLIENTE_ENDERECO","Endereço");
define("TXT_ADM_CLIENTE_O_CAMPO_ENDERECO_E_OBRIGATORIO","O campo ENDEREÇO é obrigatório!");
define("TXT_ADM_CLIENTE_NUMERO","Número");
define("TXT_ADM_CLIENTE_O_CAMPO_NUMERO_E_OBRIGATORIO","O campo NÚMERO é obrigatório!");
define("TXT_ADM_CLIENTE_COMPLEMENTO","Complemento");
define("TXT_ADM_CLIENTE_OBSERVACAO","Observação");
define("TXT_ADM_CLIENTE_CONTATO_NOME","Contato - Nome");
define("TXT_ADM_CLIENTE_CONTATO_FONE","Contato - Fone");
define("TXT_ADM_CLIENTE_CONTATO_CELULAR","Contato - Celular");
define("TXT_ADM_CLIENTE_CONTATO_WHATSAPP","Contato - WhatsApp");
define("TXT_ADM_CLIENTE_SITUACAO","Situação");
define("TXT_ADM_CLIENTE_O_CAMPO_SITUACAO_E_OBRIGATORIO","O campo SITUAÇÃO é obrigatório!");
define("TXT_ADM_CLIENTE_A","A");
define("TXT_ADM_CLIENTE_ATIVO","Ativo");
define("TXT_ADM_CLIENTE_I","I");
define("TXT_ADM_CLIENTE_INATIVO","Inativo");

####
# view-mudas-especie.php
####
define("TXT_PRODFY_SIDE_MENU_MUDAS","Mudas");
define("TXT_PRODFY_SIDE_MENU_MUDAS_ESPECIE","Espécie");
define("TXT_MUDAS_ESPECIE_MUDAS","mudas");
define("TXT_MUDAS_ESPECIE_ESPECIE","especie");
define("TXT_MUDAS_ESPECIE_MUDAS","Mudas");
define("TXT_MUDAS_ESPECIE_ESPECIE","Espécie");
define("TXT_MUDAS_ESPECIE_NOME_COMUM","Nome Comum");
define("TXT_MUDAS_ESPECIE_O_CAMPO_NOME_COMUM_E_OBRIGATORIO","O campo NOME COMUM é obrigatório!");
define("TXT_MUDAS_ESPECIE_NOME_ESPECIE","Nome Espécie");
define("TXT_MUDAS_ESPECIE_O_CAMPO_NOME_ESPECIE_E_OBRIGATORIO","O campo NOME ESPÉCIE é obrigatório!");
define("TXT_MUDAS_ESPECIE_NOME_CIENTIFICO","Nome Científico");
define("TXT_MUDAS_ESPECIE_USO_LIBERADO","Uso Liberado");
define("TXT_MUDAS_ESPECIE_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_MUDAS_ESPECIE_SIM","Sim");
define("TXT_MUDAS_ESPECIE_NAO","Não");

####
# view-mudas-doenca.php
####
define("TXT_PRODFY_SIDE_MENU_MUDAS","Mudas");
define("TXT_PRODFY_SIDE_MENU_MUDAS_DOENCA","Doença");
define("TXT_MUDAS_DOENCA_MUDAS","mudas");
define("TXT_MUDAS_DOENCA_DOENCA","doenca");
define("TXT_MUDAS_DOENCA_MUDAS","Mudas");
define("TXT_MUDAS_DOENCA_DOENCA","Doença");
define("TXT_MUDAS_DOENCA_NOME","Nome");
define("TXT_MUDAS_DOENCA_O_CAMPO_NOME_E_OBRIGATORIO","O campo NOME é obrigatório!");
define("TXT_MUDAS_DOENCA_CARACTERISTICAS","Características");
define("TXT_MUDAS_DOENCA_INCUBACAO","Incubação");
define("TXT_MUDAS_DOENCA_O_CAMPO_INCUBACAO_E_OBRIGATORIO","O campo INCUBAÇÃO é obrigatório!");
define("TXT_MUDAS_DOENCA_UNIDADE","Unidade");
define("TXT_MUDAS_DOENCA_O_CAMPO_UNIDADE_E_OBRIGATORIO","O campo UNIDADE é obrigatório!");
define("TXT_MUDAS_DOENCA_SG","SG");
define("TXT_MUDAS_DOENCA_SEGUNDOS","Segundos");
define("TXT_MUDAS_DOENCA_MN","MN");
define("TXT_MUDAS_DOENCA_MINUTOS","Minutos");
define("TXT_MUDAS_DOENCA_HR","HR");
define("TXT_MUDAS_DOENCA_HORAS","Horas");
define("TXT_MUDAS_DOENCA_DD","DD");
define("TXT_MUDAS_DOENCA_DIAS","Dias");
define("TXT_MUDAS_DOENCA_MM","MM");
define("TXT_MUDAS_DOENCA_MESES","Meses");
define("TXT_MUDAS_DOENCA_AA","AA");
define("TXT_MUDAS_DOENCA_ANOS","Anos");
define("TXT_MUDAS_DOENCA_TRATAMENTO","Tratamento");
define("TXT_MUDAS_DOENCA_USO_LIBERADO","Uso Liberado");
define("TXT_MUDAS_DOENCA_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_MUDAS_DOENCA_1","1");
define("TXT_MUDAS_DOENCA_SIM","Sim");
define("TXT_MUDAS_DOENCA_0","0");
define("TXT_MUDAS_DOENCA_NAO","Não");

####
# view-mudas-muda.php
####
define("TXT_PRODFY_SIDE_MENU_MUDAS","Mudas");
define("TXT_PRODFY_SIDE_MENU_MUDAS_MUDA","Muda");
define("TXT_MUDAS_MUDA_MUDAS","mudas");
define("TXT_MUDAS_MUDA_MUDA","muda");
define("TXT_MUDAS_MUDA_MUDAS","Mudas");
define("TXT_MUDAS_MUDA_MUDA","Muda");
define("TXT_MUDAS_MUDA_NOME_INTERNO","Nome Interno");
define("TXT_MUDAS_MUDA_O_CAMPO_NOME_INTERNO_E_OBRIGATORIO","O campo NOME INTERNO é obrigatório!");
define("TXT_MUDAS_MUDA_NOME","Nome");
define("TXT_MUDAS_MUDA_O_CAMPO_NOME_E_OBRIGATORIO","O campo NOME é obrigatório!");
define("TXT_MUDAS_MUDA_ESPECIE","Espécie");
define("TXT_MUDAS_MUDA_O_CAMPO_ESPECIE_E_OBRIGATORIO","O campo ESPÉCIE é obrigatório!");
define("TXT_MUDAS_MUDA_DENSIDADE","Densidade");
define("TXT_MUDAS_MUDA_IMA","IMA");
define("TXT_MUDAS_MUDA_RESISTENTE","Resistente");
define("TXT_MUDAS_MUDA_SELECIONE_A_DOENCA","Selecione a Doença");
define("TXT_MUDAS_MUDA_USO_LIBERADO","Uso Liberado");
define("TXT_MUDAS_MUDA_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_MUDAS_MUDA_1","1");
define("TXT_MUDAS_MUDA_SIM","Sim");
define("TXT_MUDAS_MUDA_0","0");
define("TXT_MUDAS_MUDA_NAO","Não");

####
# view-prod-objetivo.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_OBJETIVO","Objetivo");
define("TXT_PROD_OBJETIVO_PROD","prod");
define("TXT_PROD_OBJETIVO_OBJETIVO","objetivo");
define("TXT_PROD_OBJETIVO_PRODUCAO","Produção");
define("TXT_PROD_OBJETIVO_OBJETIVO","Objetivo");
define("TXT_PROD_OBJETIVO_TITULO","Título");
define("TXT_PROD_OBJETIVO_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");
define("TXT_PROD_OBJETIVO_DESCRICAO","Descrição");

####
# view-prod-ponto_controle.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_PONTO_CONTROLE","Ponto de Controle");
define("TXT_PROD_PONTO_CONTROLE_PROD","prod");
define("TXT_PROD_PONTO_CONTROLE_PONTO_CONTROLE","ponto_controle");
define("TXT_PROD_PONTO_CONTROLE_PRODUCAO","Produção");
define("TXT_PROD_PONTO_CONTROLE_PONTO_DE_CONTROLE","Ponto de Controle");
define("TXT_PROD_PONTO_CONTROLE_CODIGO","Código");
define("TXT_PROD_PONTO_CONTROLE_O_CAMPO_CODIGO_E_OBRIGATORIO","O campo CÓDIGO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_TITULO","Título");
define("TXT_PROD_PONTO_CONTROLE_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_DESCRICAO","Descrição");
define("TXT_PROD_PONTO_CONTROLE_O_CAMPO_DESCRICAO_E_OBRIGATORIO","O campo DESCRIÇÃO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_MATURACAO","Maturação");
define("TXT_PROD_PONTO_CONTROLE_O_CAMPO_MATURACAO_E_OBRIGATORIO","O campo MATURAÇÃO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_UNIDADE","Unidade");
define("TXT_PROD_PONTO_CONTROLE_O_CAMPO_UNIDADE_E_OBRIGATORIO","O campo UNIDADE é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_SG","SG");
define("TXT_PROD_PONTO_CONTROLE_SEGUNDO","Segundo");
define("TXT_PROD_PONTO_CONTROLE_SEGUNDOS","Segundos");
define("TXT_PROD_PONTO_CONTROLE_MN","MN");
define("TXT_PROD_PONTO_CONTROLE_MINUTO","Minuto");
define("TXT_PROD_PONTO_CONTROLE_MINUTOS","Minutos");
define("TXT_PROD_PONTO_CONTROLE_HR","HR");
define("TXT_PROD_PONTO_CONTROLE_HORA","Hora");
define("TXT_PROD_PONTO_CONTROLE_HORAS","Horas");
define("TXT_PROD_PONTO_CONTROLE_DD","DD");
define("TXT_PROD_PONTO_CONTROLE_DIA","Dia");
define("TXT_PROD_PONTO_CONTROLE_DIAS","Dias");
define("TXT_PROD_PONTO_CONTROLE_SM","SM");
define("TXT_PROD_PONTO_CONTROLE_SEMANA","Semana");
define("TXT_PROD_PONTO_CONTROLE_SEMANAS","Semanas");
define("TXT_PROD_PONTO_CONTROLE_MM","MM");
define("TXT_PROD_PONTO_CONTROLE_MES","Mês");
define("TXT_PROD_PONTO_CONTROLE_MESES","Meses");
define("TXT_PROD_PONTO_CONTROLE_AA","AA");
define("TXT_PROD_PONTO_CONTROLE_ANO","Ano");
define("TXT_PROD_PONTO_CONTROLE_ANOS","Anos");

####
# view-prod-ponto_controle_estagio.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_PONTO_CONTROLE_ESTAGIO","Estágio de Ponto de Controle");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_PROD","prod");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_PONTO_CONTROLE_ESTAGIO","ponto_controle_estagio");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_PRODUCAO","Produção");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_ESTAGIO_DE_PONTO_DE_CONTROLE","Estágio de Ponto de Controle");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_PONTO_DE_CONTROLE","Ponto de Controle");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_PONTO_DE_CONTROLE_E_OBRIGATORIO","O campo PONTO DE CONTROLE é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_CODIGO","Código");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_CODIGO_E_OBRIGATORIO","O campo CÓDIGO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_TITULO","Título");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_DESCRICAO","Descrição");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_DESCRICAO_E_OBRIGATORIO","O campo DESCRIÇÃO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_MATURACAO","Maturação");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_MATURACAO_E_OBRIGATORIO","O campo MATURAÇÃO é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_UNIDADE","Unidade");
define("TXT_PROD_PONTO_CONTROLE_ESTAGIO_O_CAMPO_UNIDADE_E_OBRIGATORIO","O campo UNIDADE é obrigatório!");
define("TXT_PROD_PONTO_CONTROLE_SG","SG");
define("TXT_PROD_PONTO_CONTROLE_SEGUNDO","Segundo");
define("TXT_PROD_PONTO_CONTROLE_SEGUNDOS","Segundos");
define("TXT_PROD_PONTO_CONTROLE_MN","MN");
define("TXT_PROD_PONTO_CONTROLE_MINUTO","Minuto");
define("TXT_PROD_PONTO_CONTROLE_MINUTOS","Minutos");
define("TXT_PROD_PONTO_CONTROLE_HR","HR");
define("TXT_PROD_PONTO_CONTROLE_HORA","Hora");
define("TXT_PROD_PONTO_CONTROLE_HORAS","Horas");
define("TXT_PROD_PONTO_CONTROLE_DD","DD");
define("TXT_PROD_PONTO_CONTROLE_DIA","Dia");
define("TXT_PROD_PONTO_CONTROLE_DIAS","Dias");
define("TXT_PROD_PONTO_CONTROLE_SM","SM");
define("TXT_PROD_PONTO_CONTROLE_SEMANA","Semana");
define("TXT_PROD_PONTO_CONTROLE_SEMANAS","Semanas");
define("TXT_PROD_PONTO_CONTROLE_MM","MM");
define("TXT_PROD_PONTO_CONTROLE_MES","Mês");
define("TXT_PROD_PONTO_CONTROLE_MESES","Meses");
define("TXT_PROD_PONTO_CONTROLE_AA","AA");
define("TXT_PROD_PONTO_CONTROLE_ANO","Ano");
define("TXT_PROD_PONTO_CONTROLE_ANOS","Anos");

####
# view-adm-contrato_categoria.php
####
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO","Administrativo");
define("TXT_PRODFY_SIDE_MENU_ADM_CONTRATO_CATEGORIA","Categoria de Contrato");
define("TXT_ADM_CONTRATO_CATEGORIA_ADM","adm");
define("TXT_ADM_CONTRATO_CATEGORIA_CONTRATO_CATEGORIA","contrato_categoria");
define("TXT_ADM_CONTRATO_CATEGORIA_ADMINISTRATIVO","Administrativo");
define("TXT_ADM_CONTRATO_CATEGORIA_CATEGORIA_DE_CONTRATO","Categoria de Contrato");
define("TXT_ADM_CONTRATO_CATEGORIA_TITULO","Título");
define("TXT_ADM_CONTRATO_CATEGORIA_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");


####
# view-adm-contrato.php
####
define("TXT_PRODFY_SIDE_MENU_ADMINISTRATIVO","Administrativo");
define("TXT_PRODFY_SIDE_MENU_ADM_CONTRATO","Contrato");
define("TXT_ADM_CONTRATO_ADM","adm");
define("TXT_ADM_CONTRATO_CONTRATO","contrato");
define("TXT_ADM_CONTRATO_ADMINISTRATIVO","Administrativo");
define("TXT_ADM_CONTRATO_CONTRATO","Contrato");
define("TXT_ADM_CONTRATO_CATEGORIA","Categoria");
define("TXT_ADM_CONTRATO_O_CAMPO_CATEGORIA_E_OBRIGATORIO","O campo CATEGORIA é obrigatório!");
define("TXT_ADM_CONTRATO_CODIGO","Código");
define("TXT_ADM_CONTRATO_O_CAMPO_CODIGO_E_OBRIGATORIO","O campo CÓDIGO é obrigatório!");
define("TXT_ADM_CONTRATO_TITULO","Título");
define("TXT_ADM_CONTRATO_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");
define("TXT_ADM_CONTRATO_DESCRICAO","Descrição");
define("TXT_ADM_CONTRATO_CLIENTE","Cliente");
define("TXT_ADM_CONTRATO_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_ADM_CONTRATO_DATA_ASSINATURA","Data Assinatura");
define("TXT_ADM_CONTRATO_DATA_INICIO","Data Início");
define("TXT_ADM_CONTRATO_DATA_FIM","Data Fim");
define("TXT_ADM_CONTRATO_VALOR","Valor");
define("TXT_ADM_CONTRATO_O_CAMPO_VALOR_E_OBRIGATORIO","O campo VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELAS","Parcelas");
define("TXT_ADM_CONTRATO_O_CAMPO_PARCELAS_E_OBRIGATORIO","O campo PARCELAS é obrigatório!");
define("TXT_ADM_CONTRATO_0","0");
define("TXT_ADM_CONTRATO_0","0");
define("TXT_ADM_CONTRATO_1","1");
define("TXT_ADM_CONTRATO_1","1");
define("TXT_ADM_CONTRATO_2","2");
define("TXT_ADM_CONTRATO_2","2");
define("TXT_ADM_CONTRATO_3","3");
define("TXT_ADM_CONTRATO_3","3");
define("TXT_ADM_CONTRATO_4","4");
define("TXT_ADM_CONTRATO_4","4");
define("TXT_ADM_CONTRATO_5","5");
define("TXT_ADM_CONTRATO_5","5");
define("TXT_ADM_CONTRATO_6","6");
define("TXT_ADM_CONTRATO_6","6");
define("TXT_ADM_CONTRATO_7","7");
define("TXT_ADM_CONTRATO_7","7");
define("TXT_ADM_CONTRATO_8","8");
define("TXT_ADM_CONTRATO_8","8");
define("TXT_ADM_CONTRATO_9","9");
define("TXT_ADM_CONTRATO_9","9");
define("TXT_ADM_CONTRATO_10","10");
define("TXT_ADM_CONTRATO_10","10");
define("TXT_ADM_CONTRATO_11","11");
define("TXT_ADM_CONTRATO_11","11");
define("TXT_ADM_CONTRATO_12","12");
define("TXT_ADM_CONTRATO_12","12");
define("TXT_ADM_CONTRATO_PARCELA_1","Parcela #1");
define("TXT_ADM_CONTRATO_PARCELA_1_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_1_O_CAMPO_PARCELA_1_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #1 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_1_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_1_O_CAMPO_PARCELA_1_VALOR_E_OBRIGATORIO","O campo PARCELA #1 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_2","Parcela #2");
define("TXT_ADM_CONTRATO_PARCELA_2_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_2_O_CAMPO_PARCELA_2_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #2 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_2_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_2_O_CAMPO_PARCELA_2_VALOR_E_OBRIGATORIO","O campo PARCELA #2 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_3","Parcela #3");
define("TXT_ADM_CONTRATO_PARCELA_3_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_3_O_CAMPO_PARCELA_3_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #3 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_3_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_3_O_CAMPO_PARCELA_3_VALOR_E_OBRIGATORIO","O campo PARCELA #3 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_4","Parcela #4");
define("TXT_ADM_CONTRATO_PARCELA_4_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_4_O_CAMPO_PARCELA_4_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #4 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_4_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_4_O_CAMPO_PARCELA_4_VALOR_E_OBRIGATORIO","O campo PARCELA #4 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_5","Parcela #5");
define("TXT_ADM_CONTRATO_PARCELA_5_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_5_O_CAMPO_PARCELA_5_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #5 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_5_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_5_O_CAMPO_PARCELA_5_VALOR_E_OBRIGATORIO","O campo PARCELA #5 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_6","Parcela #6");
define("TXT_ADM_CONTRATO_PARCELA_6_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_6_O_CAMPO_PARCELA_6_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #6 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_6_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_6_O_CAMPO_PARCELA_6_VALOR_E_OBRIGATORIO","O campo PARCELA #6 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_7","Parcela #7");
define("TXT_ADM_CONTRATO_PARCELA_7_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_7_O_CAMPO_PARCELA_7_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #7 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_7_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_7_O_CAMPO_PARCELA_7_VALOR_E_OBRIGATORIO","O campo PARCELA #7 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_8","Parcela #8");
define("TXT_ADM_CONTRATO_PARCELA_8_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_8_O_CAMPO_PARCELA_8_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #8 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_8_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_8_O_CAMPO_PARCELA_8_VALOR_E_OBRIGATORIO","O campo PARCELA #8 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_9","Parcela #9");
define("TXT_ADM_CONTRATO_PARCELA_9_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_9_O_CAMPO_PARCELA_9_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #9 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_9_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_9_O_CAMPO_PARCELA_9_VALOR_E_OBRIGATORIO","O campo PARCELA #9 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_10","Parcela #10");
define("TXT_ADM_CONTRATO_PARCELA_10_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_10_O_CAMPO_PARCELA_10_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #10 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_10_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_10_O_CAMPO_PARCELA_10_VALOR_E_OBRIGATORIO","O campo PARCELA #10 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_11","Parcela #11");
define("TXT_ADM_CONTRATO_PARCELA_11_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_11_O_CAMPO_PARCELA_11_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #11 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_11_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_11_O_CAMPO_PARCELA_11_VALOR_E_OBRIGATORIO","O campo PARCELA #11 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_12","Parcela #12");
define("TXT_ADM_CONTRATO_PARCELA_12_DATA_DE_VENCIMENTO","Data de Vencimento");
define("TXT_ADM_CONTRATO_PARCELA_12_O_CAMPO_PARCELA_12_DATA_DE_VENCIMENTO_E_OBRIGATORIO","O campo PARCELA #12 - DATA DE VENCIMENTO é obrigatório!");
define("TXT_ADM_CONTRATO_PARCELA_12_VALOR_R","Valor (R$)");
define("TXT_ADM_CONTRATO_PARCELA_12_O_CAMPO_PARCELA_12_VALOR_E_OBRIGATORIO","O campo PARCELA #12 - VALOR é obrigatório!");
define("TXT_ADM_CONTRATO_OBSERVACAO","Observação");
define("TXT_ADM_CONTRATO_SITUACAO","Situação");
define("TXT_ADM_CONTRATO_O_CAMPO_SITUACAO_E_OBRIGATORIO","O campo SITUAÇÃO é obrigatório!");
define("TXT_ADM_CONTRATO_AT","AT");
define("TXT_ADM_CONTRATO_ATIVO","Ativo");
define("TXT_ADM_CONTRATO_CC","CC");
define("TXT_ADM_CONTRATO_CONCLUIDO","Concluído");
define("TXT_ADM_CONTRATO_CA","CA");
define("TXT_ADM_CONTRATO_CANCELADO","Cancelado");
define("TXT_ADM_CONTRATO_CANCELADO_DATA","Cancelado - Data");
define("TXT_ADM_CONTRATO_CANCELADO_MOTIVO","Cancelado - Motivo");

####
# view-prod-lote.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_LOTE","Lote");
define("TXT_PROD_LOTE_PROD","prod");
define("TXT_PROD_LOTE_LOTE","lote");
define("TXT_PROD_LOTE_PRODUCAO","Produção");
define("TXT_PROD_LOTE_LOTE","Lote");
define("TXT_PROD_LOTE_CODIGO","Código");
define("TXT_PROD_LOTE_O_CAMPO_CODIGO_E_OBRIGATORIO","O campo CÓDIGO é obrigatório!");
define("TXT_PROD_LOTE_OBJETIVO","Objetivo");
define("TXT_PROD_LOTE_O_CAMPO_OBJETIVO_E_OBRIGATORIO","O campo OBJETIVO é obrigatório!");
define("TXT_PROD_LOTE_MUDA","Muda");
define("TXT_PROD_LOTE_O_CAMPO_MUDA_E_OBRIGATORIO","O campo MUDA é obrigatório!");
define("TXT_PROD_LOTE_CLIENTE","Cliente");
define("TXT_PROD_LOTE_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_PROD_LOTE_CONTRATO","Contrato");
define("TXT_PROD_LOTE_QTDE_ALVO","Qtde Alvo");
define("TXT_PROD_LOTE_O_CAMPO_QTDE_INICIAL_E_OBRIGATORIO","O campo QTDE INICIAL é obrigatório!");
define("TXT_PROD_LOTE_DATA_ESTAQUEAMENTO","Data Estaqueamento");
define("TXT_PROD_LOTE_HORA_ESTAQUEAMENTO","Hora Estaqueamento");
define("TXT_PROD_LOTE_DIAS","Dias");
define("TXT_PROD_LOTE_IDADE","Idade");
define("TXT_PROD_LOTE_USO_LIBERADO","Uso Liberado");
define("TXT_PROD_LOTE_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_PROD_LOTE_1","1");
define("TXT_PROD_LOTE_SIM","Sim");
define("TXT_PROD_LOTE_0","0");
define("TXT_PROD_LOTE_NAO","Não");
define("TXT_PROD_LOTE_QTDE_ATUAL","Qtde Atual");
define("TXT_PROD_LOTE_ULT_CONT","Últ. Contagem");

####
# view-prod-lote_inventario.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_LOTE_INVENTARIO","Inventário de Lote");
define("TXT_PROD_LOTE_INVENTARIO_PROD","prod");
define("TXT_PROD_LOTE_INVENTARIO_LOTE_INVENTARIO","lote_inventario");
define("TXT_PROD_LOTE_INVENTARIO_PRODUCAO","Produção");
define("TXT_PROD_LOTE_INVENTARIO_INVENTARIO_DE_LOTE","Inventário de Lote");
define("TXT_PROD_LOTE_INVENTARIO_LOTE","Lote");
define("TXT_PROD_LOTE_INVENTARIO_O_CAMPO_LOTE_E_OBRIGATORIO","O campo LOTE é obrigatório!");
define("TXT_PROD_LOTE_INVENTARIO_DATA_INICIO","Data Início");
define("TXT_PROD_LOTE_INVENTARIO_O_CAMPO_DATA_INICIO_E_OBRIGATORIO","O campo DATA INÍCIO é obrigatório!");
define("TXT_PROD_LOTE_INVENTARIO_DATA_FIM","Data Fim");
define("TXT_PROD_LOTE_INVENTARIO_O_CAMPO_DATA_FIM_E_OBRIGATORIO","O campo DATA FIM é obrigatório!");
define("TXT_PROD_LOTE_INVENTARIO_QUANTIDADE","Quantidade");
define("TXT_PROD_LOTE_INVENTARIO_DISPOSITIVO_NO","Dispositivo No");
define("TXT_PROD_LOTE_INVENTARIO_HORA_INICIO","Hora Início");
define("TXT_PROD_LOTE_INVENTARIO_HORA_FIM","Hora Fim");

####
# view-prod-lote_hist.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_LOTE_HIST","Histórico de Lote");
define("TXT_PROD_LOTE_HIST_PROD","prod");
define("TXT_PROD_LOTE_HIST_LOTE_HIST","lote_hist");
define("TXT_PROD_LOTE_HIST_PRODUCAO","Produção");
define("TXT_PROD_LOTE_HIST_HISTORICO_DE_LOTE","Histórico de Lote");
define("TXT_PROD_LOTE_HIST_DATA","Data");
define("TXT_PROD_LOTE_HIST_O_CAMPO_DATA_E_OBRIGATORIO","O campo DATA é obrigatório!");
define("TXT_PROD_LOTE_HIST_LOTE","Lote");
define("TXT_PROD_LOTE_HIST_O_CAMPO_LOTE_E_OBRIGATORIO","O campo LOTE é obrigatório!");
define("TXT_PROD_LOTE_HIST_TITULO","Título");
define("TXT_PROD_LOTE_HIST_O_CAMPO_TITULO_E_OBRIGATORIO","O campo TÍTULO é obrigatório!");
define("TXT_PROD_LOTE_HIST_TEXTO","Texto");
define("TXT_PROD_LOTE_HIST_O_CAMPO_TEXTO_E_OBRIGATORIO","O campo TEXTO é obrigatório!");

####
# view-prod-lote_evolucao.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_LOTE_EVOLUCAO","Evolução de Lote");
define("TXT_PROD_LOTE_EVOLUCAO_PROD","prod");
define("TXT_PROD_LOTE_EVOLUCAO_LOTE_EVOLUCAO","lote_evolucao");
define("TXT_PROD_LOTE_EVOLUCAO_PRODUCAO","Produção");
define("TXT_PROD_LOTE_EVOLUCAO_EVOLUCAO_DE_LOTE","Evolução de Lote");
define("TXT_PROD_LOTE_EVOLUCAO_LOTE","Lote");
define("TXT_PROD_LOTE_EVOLUCAO_O_CAMPO_LOTE_E_OBRIGATORIO","O campo LOTE é obrigatório!");
define("TXT_PROD_LOTE_EVOLUCAO_PONTO_DE_CONTROLE","Ponto de Controle");
define("TXT_PROD_LOTE_EVOLUCAO_O_CAMPO_PONTO_DE_CONTROLE_E_OBRIGATORIO","O campo PONTO DE CONTROLE é obrigatório!");
define("TXT_PROD_LOTE_EVOLUCAO_ESTAGIO","Estágio");
define("TXT_PROD_LOTE_EVOLUCAO_O_CAMPO_ESTAGIO_E_OBRIGATORIO","O campo ESTÁGIO é obrigatório!");
define("TXT_PROD_LOTE_EVOLUCAO_DATA_INICIO","Data Início");
define("TXT_PROD_LOTE_EVOLUCAO_HORA_INICIO","Hora Início");
define("TXT_PROD_LOTE_EVOLUCAO_O_CAMPO_DATA_INICIO_E_OBRIGATORIO","O campo DATA INÍCIO é obrigatório!");
define("TXT_PROD_LOTE_EVOLUCAO_O_CAMPO_HORA_INICIO_E_OBRIGATORIO","O campo HORA INÍCIO é obrigatório!");
define("TXT_PROD_LOTE_EVOLUCAO_DATA_CONCLUSAO","Data Conclusão");
define("TXT_PROD_LOTE_EVOLUCAO_HORA_CONCLUSAO","Hora Conclusão");
define("TXT_PROD_LOTE_EVOLUCAO_ESTAGIO_SELECIONE_PONTO_CONTROLE","Selecione um Ponto de Controle");
define("TXT_PROD_LOTE_EVOLUCAO_ESTAGIO_NENHUM_ESTAGIO_ENCONTRATO_PARA_O_PONTO_DE_CONTROLE_SELECIONADO","Nenhum estágio encontrado para o Ponto de Controle selecionado!");

####
# view-prod-visao_geral.php
####
define("TXT_PRODFY_SIDE_MENU_PRODUCAO","Produção");
define("TXT_PRODFY_SIDE_MENU_PROD_VISAO_GERAL","Visão Geral");
define("TXT_PROD_VISAO_GERAL_PROD","prod");
define("TXT_PROD_VISAO_GERAL_VISAO_GERAL","visao_geral");
define("TXT_PROD_VISAO_GERAL_PRODUCAO","Produção");
define("TXT_PROD_VISAO_GERAL_VISAO_GERAL","Visão Geral");
define("TXT_PROD_VISAO_GERAL_TOTAL_DE_MUDAS","Total de Mudas");
define("TXT_PROD_VISAO_GERAL_PERDAS","Perdas");
define("TXT_PROD_VISAO_GERAL_RESPONSAVEIS","Responsáveis");
define("TXT_PROD_VISAO_GERAL_ALERTAS","Alertas");

####
# view-painel-dashboard.php
####
define("TXT_PAINEL_DASHBOARD_PAINEL_DETALHES","Detalhes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_COLABORADORES","Colaboradores");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_DISPOSITIVOS","Dispositivos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_CLIENTES","Clientes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_CONTRATOS","Contratos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_MUDAS","Variedades de Muda");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_ESPECIES","Espécies");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_DOENCAS","Doenças");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_OBJETIVOS","Objetivos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_PONTOS_DE_CONTROLE","Pontos de Controle");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_ESTAGIOS","Estágios");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES","Lotes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES_CONCLUIDOS","Lotes Concluídos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES_EM_PRODUCAO","Lotes em Produção");

####
# view-cfg-dispositivos.php
####
define("TXT_PRODFY_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_PRODFY_SIDE_MENU_CFG_DISPOSITIVOS","Dispositivos");
define("TXT_CFG_DISPOSITIVOS_CFG","cfg");
define("TXT_CFG_DISPOSITIVOS_CONFIGURACAO","Configuração");
define("TXT_CFG_DISPOSITIVOS_DISPOSITIVOS","Dispositivos");
define("TXT_CFG_DISPOSITIVOS_NUMERO","Número");
define("TXT_CFG_DISPOSITIVOS_O_CAMPO_NUMERO_E_OBRIGATORIO","O campo NÚMERO é obrigatório!");
define("TXT_CFG_DISPOSITIVOS_SENHA","Senha");
define("TXT_CFG_DISPOSITIVOS_COLABORADOR","Colaborador");
define("TXT_CFG_DISPOSITIVOS_ULT_SINCR","Últ. Sincronismo");
define("TXT_CFG_DISPOSITIVOS_USO_LIBERADO","Uso Liberado");
define("TXT_CFG_DISPOSITIVOS_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_CFG_DISPOSITIVOS_1","1");
define("TXT_CFG_DISPOSITIVOS_SIM","Sim");
define("TXT_CFG_DISPOSITIVOS_0","0");
define("TXT_CFG_DISPOSITIVOS_NAO","Não");

#################################################################################
###########
#####
##
