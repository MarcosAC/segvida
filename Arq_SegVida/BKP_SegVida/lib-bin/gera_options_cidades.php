<?php
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_options_cidades.php
## Função..........: Gera <option>...</option> com a lista das cidades para o estado informado, com selected na cidade indicada
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once "geral/language.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_FIELD_ID = $IN_UF = $IN_CSS = $IN_REQUIRED = $IN_FIRSTBLANK = $IN_FIRSTBLANKTEXT = $IN_UF = $IN_CIDADE = $IN_IBGE_CODE = "";
	
	//prodfymudas.rlgomide.com/cgi-bin/gera_options_cidades.php?l=br&uf=MG&r=1&fb=1&fbt=*Cidades
	
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG           = test_input($_GET["l"]);
		$IN_UF             = test_input($_GET["uf"]);
		$IN_FIRSTBLANK     = test_input($_GET["fb"]);
		$IN_FIRSTBLANKTEXT = test_input($_GET["fbt"]);
		$IN_CODIGO         = test_input($_GET["c"]);
		$IN_CIDADE         = test_input($_GET["cdd"]);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["l"]);
		$IN_UF             = test_input($_POST["uf"]);
		$IN_FIRSTBLANK     = test_input($_POST["fb"]);
		$IN_FIRSTBLANKTEXT = test_input($_POST["fbt"]);
		$IN_CODIGO         = test_input($_POST["c"]);
		$IN_CIDADE         = test_input($_POST["cdd"]);
		
		## Autentica dados obrigatorios
		if( !empty($IN_UF)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|Falha na transferência dos dados. Favor tentar novamente!|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara where
			$WHERE_TMP = $CAP = "";
			//
			if(!empty($IN_UF))
			{
				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
				$WHERE_TMP .= sprintf($CAP." upper(`uf`) = upper('%s')",
				$mysqli->escape_string($IN_UF));
			}
			//if(!empty($IN_CIDADE))
			//{
			//	if(!empty($WHERE_TMP)){ $CAP = " AND "; }
			//	$WHERE_TMP .= sprintf($CAP." upper(`cidade`) = upper('%s')",
			//	mysql_real_escape_string($IN_CIDADE));
			//}
//			if(!empty($IN_CODIGO))
//			{
//				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
//				$WHERE_TMP .= sprintf($CAP." upper(`codigo_ibge`) = upper('%s')",
//				$mysqli->escape_string($IN_CODIGO));
//			}
			//
			$WHERE = " WHERE ".$WHERE_TMP;//." collate utf8_general_ci";
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT `codigo_ibge` as codigo,`cidade` as cidade 
			   FROM `MUNICIPIO_BR` 
			  $WHERE"
			)) 
			{
				//$stmt->bind_param('s', $email);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CODIGO, $o_CIDADE);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|Nenhuma cidade encontrada para o Estado selecionado!|alert|");
					exit; 
				}
				else
				{ 
					## Localiza pela cidade
					if(!empty($IN_CIDADE))
					{
						$cidade_tmp = remove_accent($IN_CIDADE);
					}
					
					##Corre pelas linhas encontradas
					while ($stmt->fetch()) 
					{
						$SELECTED = "";
						
						## Localiza pelo codigo
						if(!empty($IN_CODIGO))
						{
							if($IN_CODIGO == $o_CODIGO ){ $SELECTED = "selected"; }
						}
						## Localiza pela cidade
						else if(!empty($IN_CIDADE))
						{
							if(strtoupper($cidade_tmp) == strtoupper($o_CIDADE) ){ $SELECTED = "selected"; }
						}
						
						$ITEM .= "<option value='".utf8_decode($o_CODIGO)."' ".$SELECTED.">".$o_CIDADE."</option>";
					}
					
					if($IN_FIRSTBLANK == 1){ $FIRSTBLANK = '<option value="">'.$IN_FIRSTBLANKTEXT.'</option>'; } else { $FIRSTBLANK=''; }
					
					## Formata lista final
					$LISTA = $FIRSTBLANK.$ITEM;
					
					die('1|'.$LISTA.'|success|');
					exit;
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
