<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: conf_registro.php
## Função..........: Ativa a conta do usuado e a conta de cliente gratis
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	//$WEB_ADDRESS = WEB_ADDRESS;
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_TOKEN = "";
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG        = test_input($_GET["l"]);
		$IN_TOKEN       = test_input($_GET["t"]);
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "pt-br"; }
		
		## Carrega Idioma
		carrega_idioma($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_TOKEN) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.TXT_TOKEN_INEXISTENTE.'</div>';
			show_msg($MY_MSG);
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## GERA TOKEN
			#$TOKEN_TMP = strtoupper(gera_token());
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT F_ConfirmaRegistroCliente_UPDATE(?,?) as ret"
			)) 
			{
				$sql_LANG     = $mysqli->escape_string($IN_LANG);
				$sql_TOKEN    = $mysqli->escape_string($IN_TOKEN);
				//
				$stmt->bind_param('ss', $sql_LANG,$sql_TOKEN);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_RESULT);
				$stmt->fetch();
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.TXT_ERRO_SOLICITACAO.'</div>';
					show_msg($MY_MSG);
					exit;
				}
				else
				{
					//status|msg|user_account_id|cliente_gescond_id|plano_dias_gratis|plano_tipo|plano_tit|plano_valor|
					list($STATUS, $MSG, $EMAIL) = explode("|", $o_RESULT);
					
					if($STATUS == 0)
					{
						$MY_MSG = '<div class="text-center center-block"><font color="#FF0000"><span class="fa fa-exclamation-triangle fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-danger text-center center-block" role="alert">'.$MSG.'</div>';
						
						//error_log("\nshow_msg(".$MY_MSG.", ".TXT_META_LANGUAGE.", ".TXT_PAGE_TITLE.", ".TXT_META_DESCRIPTION.", ".TXT_META_AUTHOR.", ".TXT_META_KEYWORDS.", ".TXT_META_SUBJECT.", ".TXT_META_COPYRIGHT.", ".TXT_META_ABSTRACT.", ".TXT_TERMOS_DE_USO.", ".TXT_POLITICA_DE_PRIVACIDADE.", ".WEB_ADDRESS.");\n\n", 0);
						
						show_msg($MY_MSG);
						exit;
					}
					else
					{
						#Formata URL de login
						$LOGIN_URL = WEB_ADDRESS."/index.php";
						
						## Create a new PHPMailer instance
						$mail = new PHPMailer(true);
						
						##Formata array com os dados
						$template_map = array(
							'{{IMG_URL}}'         => IMG_URL,
							'{{EMAIL}}'           => mb_strtolower($EMAIL,"UTF-8"),
							'{{LOGIN_URL}}' => $LOGIN_URL
						);
						
						##Carrega texto do email via template ja processado
						$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_CONF_REGISTRO,$template_map,$IN_LANG);
						
						$isMailSent = 0;
						
						try 
						{
							ini_set("sendmail_from", "");
							$mail->CharSet = 'UTF-8';
							$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
							$mail->AddAddress($EMAIL);//Set who the message is to be sent to
							
							//if(!empty(EMAIL_CONTATO_BCC))
							//{
							//	$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
							//}
							
							$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
							
							$mail->Subject = TXT_EMAIL_SUBJECT_CONTA_ATIVADA;//Set the subject line
							
							$mail->MsgHTML($body);
							
							if ($mail->send()) { $isMailSent = 1; }
						} 
						catch (phpmailerException $e) 
						{
							$MailError = $e->errorMessage();
						} 
						catch (Exception $e) 
						{
							$MailException = $e->getMessage();
						}
						
						//send the message, check for errors
						if (!$isMailSent) 
						{
							error_log("\n***\n* ERRO PHPMailer\n***\n\nEmail [".EMAIL_TEMPLATE_CONF_REGISTRO."] nao foi enviado \nMailError: ".$MailError."\nMailException: ".$MailException."\n\n", 0);
						}
						
						$MY_MSG = '<div class="text-center center-block"><font color="#008000"><span class="fa fa-thumbs-o-up fa-4x" aria-hidden="true"></span></font></div><br/><div class="alert alert-success text-center center-block" role="alert">'.TXT_MSG_CONTA_ATIVADA.'</div><br/><div id=\'loged_panel\'><a href="//'.$LOGIN_URL.'" class="btn btn-yellow center-block" role="button" style="width:100px;"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;'.TXT_CONF_REGISTRO_ACESSAR.'</a></div>';
						show_msg($MY_MSG);
						exit;
					}
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("");
		exit;
	}
	
	
	
	function show_msg($_MSG)
	{
		$TXT_META_LANGUAGE           = TXT_META_LANGUAGE;
		$TXT_PAGE_TITLE              = TXT_PAGE_TITLE;
		$TXT_META_DESCRIPTION        = TXT_META_DESCRIPTION;
		$TXT_META_AUTHOR             = TXT_META_AUTHOR;
		$TXT_META_KEYWORDS           = TXT_META_KEYWORDS;
		$TXT_META_SUBJECT            = TXT_META_SUBJECT;
		$TXT_META_COPYRIGHT          = TXT_META_COPYRIGHT;
		$TXT_META_ABSTRACT           = TXT_META_ABSTRACT;
		$TXT_TERMOS_DE_USO           = TXT_TERMOS_DE_USO;
		$TXT_POLITICA_DE_PRIVACIDADE = TXT_POLITICA_DE_PRIVACIDADE;
		$WEB_ADDRESS                 = "http://".WEB_ADDRESS;
		
		$RET_MSG = <<<EOT
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="$TXT_META_LANGUAGE" />
		<meta name="description" content="$TXT_META_DESCRIPTION" />
		<meta name="author"      content="$TXT_META_AUTHOR">
		<meta name="keywords"    content="$TXT_META_KEYWORDS" />
		<meta name="subject"     content="$TXT_META_SUBJECT" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="$TXT_META_COPYRIGHT" />
		<meta name="abstract"    content="$TXT_META_ABSTRACT" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="$WEB_ADDRESS/img/favicon1.png" type="image/x-icon">
		
		<title>$TXT_PAGE_TITLE</title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="$WEB_ADDRESS/css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="$WEB_ADDRESS/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<!-- <link rel="stylesheet" href="$WEB_ADDRESS/css/offcanvas.css"> -->
		<link rel="stylesheet" href="$WEB_ADDRESS/fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="$WEB_ADDRESS/css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="$WEB_ADDRESS/css/login_form.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="$WEB_ADDRESS/js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			.alert {
				width:40%;
			}
			
		</style>
		
	</head>
	<body>
		<div class="container-fluid center-block">
			<img src="$WEB_ADDRESS/img/logo_326x240.png" border=0 class="img-responsive center-block"><br/>
			
			$_MSG
			
			<br/>
		</div><!--/.container-->
		
		<div id='footer_panel' class="text-center">
			<a href="$WEB_ADDRESS/termos-de-uso.php" target="_blank" role="button">$TXT_TERMOS_DE_USO</a> | <a href="$WEB_ADDRESS/politica-de-privacidade.php" target="_blank" role="button">$TXT_POLITICA_DE_PRIVACIDADE</a>
		</div>
		
		
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="$WEB_ADDRESS/js/jquery.min.js"></script>
		<script src='$WEB_ADDRESS/js/jquery.storage.js'></script>
		<script src="$WEB_ADDRESS/js/bootstrap.min.js"></script>
		<!-- <script src="$WEB_ADDRESS/js/bootstrap-select.min.js"></script> -->
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="$WEB_ADDRESS/js/ie10-viewport-bug-workaround.js"></script>
		
	</body>
</html>
EOT;
		
		die($RET_MSG);
		exit;
	}
	
#################################################################################
###########
#####
##
?>
