<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: cadastro_usr.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 02/09/2017 12:48:15
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	$IN_LANG = $IN_PLANO = $IN_INDICACAO = "";
	
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG      = test_input($_POST["l"]);
	}
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG      = test_input($_GET["l"]);
	}
	
	## Define idioma
	//if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	$IN_LANG = valida_idioma($IN_LANG);
	carrega_idioma($IN_LANG);
	
	####
	# Mostra pagina em PT-BR
	####
	if( $IN_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Cadastro de Colaborador" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="css/offcanvas.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="css/planos.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			body { marting-bottom:0; }
			
			/*.btn-yellow*/
			.btn-yellow {
			  color: #000000;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:focus,
			.btn-yellow.focus {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:hover {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active,
			.btn-yellow.active,
			.open > .dropdown-toggle.btn-yellow {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active:hover,
			.btn-yellow.active:hover,
			.open > .dropdown-toggle.btn-yellow:hover,
			.btn-yellow:active:focus,
			.btn-yellow.active:focus,
			.open > .dropdown-toggle.btn-yellow:focus,
			.btn-yellow:active.focus,
			.btn-yellow.active.focus,
			.open > .dropdown-toggle.btn-yellow.focus {
			  color: #0000ff;
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow:active,
			.btn-yellow.active,
			.open > .dropdown-toggle.btn-yellow {
			  background-image: none;
			}
			.btn-yellow.disabled:hover,
			.btn-yellow[disabled]:hover,
			fieldset[disabled] .btn-yellow:hover,
			.btn-yellow.disabled:focus,
			.btn-yellow[disabled]:focus,
			fieldset[disabled] .btn-yellow:focus,
			.btn-yellow.disabled.focus,
			.btn-yellow[disabled].focus,
			fieldset[disabled] .btn-yellow.focus {
			  background-color: #f8d039;
			  border-color: #ef9900;
			}
			.btn-yellow .badge {
			  color: #337ab7;
			  background-color: #ffffff;
			}
			
			/*.btn-lightblue - 3bafda*/
			.btn-lightblue {
			  color: #000000;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:focus,
			.btn-lightblue.focus {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:hover {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active,
			.btn-lightblue.active,
			.open > .dropdown-toggle.btn-lightblue {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active:hover,
			.btn-lightblue.active:hover,
			.open > .dropdown-toggle.btn-lightblue:hover,
			.btn-lightblue:active:focus,
			.btn-lightblue.active:focus,
			.open > .dropdown-toggle.btn-lightblue:focus,
			.btn-lightblue:active.focus,
			.btn-lightblue.active.focus,
			.open > .dropdown-toggle.btn-lightblue.focus {
			  color: #0000ff;
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue:active,
			.btn-lightblue.active,
			.open > .dropdown-toggle.btn-lightblue {
			  background-image: none;
			}
			.btn-lightblue.disabled:hover,
			.btn-lightblue[disabled]:hover,
			fieldset[disabled] .btn-lightblue:hover,
			.btn-lightblue.disabled:focus,
			.btn-lightblue[disabled]:focus,
			fieldset[disabled] .btn-lightblue:focus,
			.btn-lightblue.disabled.focus,
			.btn-lightblue[disabled].focus,
			fieldset[disabled] .btn-lightblue.focus {
			  background-color: #3bafda;
			  border-color: #3ebef0;
			}
			.btn-lightblue .badge {
			  color: #337ab7;
			  background-color: #ffffff;
			}
			.bootstrap-select.btn-group .dropdown-menu li a:hover {
				color: #0000ff !important;
				background: #fed670 !important;
			}
			
			.bootstrap-select.btn-group .dropdown-menu {
				background: #fefac2 !important;
			}
			
			
			
			
			@import url(http://fonts.googleapis.com/css?family=Roboto);
			
			* {
			    font-family: 'Roboto', sans-serif;
			}
			
			
			.form-control-select {
				margin-top: 0px;
			}
			
			select {
				border-radius: 0px;
				margin-top: 0px;
			}
			
			
			/* #########################################
			   #    override the bootstrap configs     #
			   ######################################### */
			
			.modal-backdrop.in {
			    filter: alpha(opacity=50);
			    opacity: .8;
			}
			
			.modal-content {
			    background-color: #ececec;
			    border: 1px solid #bdc3c7;
			    border-radius: 0px;
			    outline: 0;
			}
			
			.modal-header {
			    min-height: 16.43px;
			    padding: 15px 15px 15px 15px;
			    border-bottom: 0px;
			}
			
			.modal-body {
			    position: relative;
			    padding: 5px 15px 5px 15px;
			}
			
			.modal-footer {
			    padding: 15px 15px 15px 15px;
			    text-align: left;
			    border-top: 0px;
			}
			
			.checkbox {
			    margin-bottom: 0px;
			}
			
			.btn {
			    border-radius: 0px;
			}
			
			.btn:focus,
			.btn:active:focus,
			.btn.active:focus,
			.btn.focus,
			.btn:active.focus,
			.btn.active.focus {
			    outline: none;
			}
			
			.btn-lg, .btn-group-lg>.btn {
			    border-radius: 0px;
			}
			
			.btn-link {
			    padding: 5px 10px 0px 0px;
			    color: #95a5a6;
			}
			
			.btn-link:hover, .btn-link:focus {
			    color: #2c3e50;
			    text-decoration: none;
			}
			
			.glyphicon {
			    top: 0px;
			}
			
			.form-control {
			  border-radius: 0px;
			}
			.input-upper {
			  text-transform: uppercase;
			}
			.input-lower {
			  text-transform: lowercase;
			}
			
			#div-label-type 
			{
				border: 1px solid #003a1f;
				height: 30px;
				line-height: 28px;
				transition: all ease-in-out 500ms;
				text-align: center;
				font-size: 90%;
				font-weight: bold;
				color: #003a1f;
				margin-bottom: 10px;
			}
						
						
			* {
			    box-sizing: border-box;
			}
			
			.columns {
			    float: left;
			    width: 33.3%;
			    padding: 8px;
			}
			
			.button {
			    background-color: #4CAF50;
			    border: none;
			    color: white;
			    padding: 10px 35px;
			    text-align: center;
			    text-decoration: none;
			    font-size: 18px;
			    border-radius: 4px;
			}
			
			@media only screen and (max-width: 600px) {
			    .columns {
			        width: 100%;
			    }
			}
			
			
			.input-group-addon {
			     min-width:150px;
			         width:150px;
			    text-align:right;
			}
			
			.input-group { width: 100%; margin-bottom: 10px; }
			
			
			select:not([multiple]) {
			    -webkit-appearance: none;
			    -moz-appearance: none;
			    background-position: right 50%;
			    background-repeat: no-repeat;
			    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
			    padding: .5em;
			    padding-right: 1.5em;
			}
			
			.no-border {
			    border-radius: 0;
			}
			
		</style>
		
	</head>
	<body>
		<div class="container-fluid">
			<div class="row text-center center-block">
				<div class="col-xs-12 col-sm-9 col-centered">
					<center>
					<h3><center><img src="img/logo_326x240.png" width="326" height="240" border=0></center></h3><br/>
					<div class="col-xs-9 col-sm-6 col-centered">
						<p align=center style="color:#434a54; font-size: 120%;">Preencha o formulário abaixo para registrar uma conta de usuário. Campos com um <span style="color:#ff0000;">*</span> são obrigatórios.</p><br/>
						<div class="clearfix"></div>
						<!-- <p align="justify" style="color:#434a54; font-size: 120%;">Obs.: Campos com um <span style="color:#ff0000;">*</span> são obrigatórios</p> -->
						
						<!-- Begin | Register Form -->
						<form id="register-form" name="register-form">
							<!-- <input id="cad_username" value="<?php echo $o_USERNAME; ?>" type=hidden> -->
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_NOME; ?>:</b>
								</span>
								<input id="register_nome" type="text" maxlength=45  class="form-control textonly_az input-upper" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_SOBRENOME; ?>:</b>
								</span>
								<input id="register_sobrenome" type="text" maxlength=45  class="form-control textonly_az input-upper" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_EMAIL; ?>:</b>
								</span>
								<input id="register_email" type="text" maxlength=180 class="form-control input-lower" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_CPF; ?>:</b>
								</span>
								<input id="register_cpf" type="text" maxlength=14  class="form-control numeric" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_CNPJ; ?>:</b>
								</span>
								<input id="register_cnpj" type="text" maxlength=18  class="form-control numeric">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_RAZAO_SOCIAL; ?>:</b>
								</span>
								<input id="register_razaosocial" type="text" maxlength=180 class="form-control textonly input-upper">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_FONE_FIXO; ?>:</b>
								</span>
								<input id="register_fonefixo1" type="text" maxlength=15  class="form-control numeric" required
									data-toggle="tooltip" data-placement="auto right" title="Digite o número do seu telefone fixo com o código de área Ex.: 3138852424">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_CELULAR; ?>:</b>
								</span>
								<input id="register_celular1" type="text" maxlength=15  class="form-control numeric"
									data-toggle="tooltip" data-placement="auto left" title="Digite o número do seu celular com o código de área Ex.: 31999474426">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_PAIS; ?>:</b>
								</span>
								<select id="register_pais" class="form-control" style="margin-top: 0px;" required>
										<option value="BR" selected>Brasil</option>
									</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_IDIOMA; ?>:</b>
								</span>
								<select id="register_idioma" class="form-control" style="margin-top: 0px;" required>
									<option value="pt-br" selected>Português Brasil</option>
									<!--<option value="en-us" >US English</option>
									<option value="es-es" >Español</option>-->
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_CEP; ?>:</b>
								</span>
								<input id="register_cep" type="text" maxlength=10 class="form-control numeric" style="margin-top: 0px;" required
									data-toggle="tooltip" data-placement="auto left" title="Entre com o CEP do seu endereço. Ao clicar em outro campo o sistema irá tentar carregar os dados do endereço. Obs.: Deve ser um CEP válido.">
									<input id="register_cep_uf" type="hidden">
									<input id="register_cep_cidade_codigo" type="hidden">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_UF; ?>:</b>
								</span>
								<select id="register_uf" class="form-control" style="margin-top:0; border-radius:0;" required>
										<option value="" selected>--Selecione--</option><option value="AC">ACRE</option><option value="AL">ALAGOAS</option><option value="AM">AMAZONAS</option><option value="AP">AMAPÁ</option><option value="BA">BAHIA</option><option value="CE">CEARÁ</option><option value="DF">DISTRITO FEDERAL</option><option value="ES">ESPÍRITO SANTO</option><option value="GO">GOIÁS</option><option value="MA">MARANHÃO</option><option value="MG">MINAS GERAIS</option><option value="MS">MATO GROSSO DO SUL</option><option value="MT">MATO GROSSO</option><option value="PA">PARÁ</option><option value="PB">PARAÍBA</option><option value="PE">PERNAMBUCO</option><option value="PI">PIAUÍ</option><option value="PR">PARANÁ</option><option value="RJ">RIO DE JANEIRO</option><option value="RN">RIO GRANDE DO NORTE</option><option value="RO">RONDÔNIA</option><option value="RR">RORAIMA</option><option value="RS">RIO GRANDE DO SUL</option><option value="SC">SANTA CATARINA</option><option value="SE">SERGIPE</option><option value="SP">SÃO PAULO</option><option value="TO">TOCANTINS</option>
									</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_CIDADE; ?>:</b>
								</span>
								<select id="register_cidade" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="" selected hidden>*Cidade (Escolha Um Estado Primeiro)</option>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_BAIRRO; ?>:</b>
								</span>
								<input id="register_bairro" type="text" maxlength=180 class="form-control textonly input-upper" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;Endereço:</b>
								</span>
								<input id="register_end" type="text" maxlength=180 class="form-control textonly input-upper" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_SYSTEM_CAD_NUMERO; ?>:</b>
								</span>
								<input id="register_num" type="text" maxlength=30 class="form-control textonly input-upper" required>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_COMPL; ?>:</b>
								</span>
								<input id="register_compl" type="text" maxlength=180 class="form-control textonly input-upper">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_USERNAME; ?>:</b>
								</span>
								<input id="register_username" type="text" maxlength=30  class="form-control logintext input-lower" required 
									data-toggle="tooltip" data-placement="auto right" title="O campo USUÁRIO deve ter de 6 a 30 dígitos. Deve conter apenas letras, números e underlines.">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_PASSWORD; ?>:</b>
								</span>
								<input id="register_password" type="password" maxlength=30 class="form-control" style="margin-top: 0px;" required
									data-toggle="tooltip" data-placement="auto left" title="A senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula.">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_SYSTEM_CAD_PASSWORD2; ?>:</b>
								</span>
								<input id="register_password2" type="password" maxlength=30  class="form-control" style="margin-top: 0px;" required
									data-toggle="tooltip" data-placement="auto right" title="Entre exatamente com a mesma senha digitada para confirmação.">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<!-- <b><?php echo TXT_SYSTEM_CAD_CAPTCHA; ?>:</b> -->
									<img id='register_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>&nbsp;&nbsp;
									<a id="register_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh fa-lg" aria-hidden="true"></span></a>
								</span>
								<div>
									<input id="register_captcha_code" type="text" class="form-control" style="margin-top: 1px;" placeholder="Digite o Código" required>
								</div>
							</div>
							<div class="checkbox" style="margin-top: 1px; margin-bottom: 10px;"><center>
								<label>
								<input id="register_termos" type="checkbox" required> Li e aceito o <a href="docs/termo-de-servico.pdf" target="_blank">termo de serviço</a>.
								</label></center>
							</div>
							<div>
								<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_SYSTEM_CAD_REGISTRAR; ?></button>
							</div>
						</form>
						<!-- End | cad Form -->
						
					</div><!--/.col-xs-12.col-sm-9-->
					</center>
					
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
		</div><!--/container-->
		
		
		
		
		
		
		
		
		
		
<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" data-backdrop="static" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<!--<div class="modal-header">
			</div>-->
			<div class="modal-body">
				<p class="text-center center-block"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color=#ffffff><b id='loadingBoxText'><?php echo TXT_MODAL_MSG_CARREGANDO ?></b></font></p>
			</div>
			<!--<div class="modal-footer" id="msgBoxFooter">
			</div>-->
		</div>
	</div>
</div>
<!-- /msgBox -->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='js/jquery.storage.js'></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script src="js/sha512.js"></script>
		<script src="js/cadastro_usr.js"></script>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<center>
			<hr size=1 noshade width=500>
			Desenvolvido pela Ther Sistemas Inovadores - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a>
			</center>
			<!--
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2017 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<div class="footer-icons">
						<a href="https://www.facebook.com/prodfy"><i class="fa fa-facebook"></i></a>
						<a href="https://www.linkedin.com/company-beta/16207365"><i class="fa fa-linkedin"></i></a>
						<a href="https://www.youtube.com/channel/UCdy42hfN-TRlvLFGF2KNVEA"><i class="fa fa-youtube"></i></a>
					</div>
				</div>
			</footer>
			-->
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>