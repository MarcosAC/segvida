<?php
#################################################################################
## ME DEI BEM - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
## Módulo: mdl-relat-prod.php
## Função: MODEL - Select, Insert, Update, Delete, Upload, Datatable Refresh;
#################################################################################

	####
	# Set Header Definition
	####
	
	header('Content-Type: text/html; charset=utf-8');
	
	####
	# Aditional Includes
	####
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	####
	# Variable Definition
	####
	$GO = $IN_LANG = $IN_DBO = $IN_REG_ID = $IN_REG_IDS = "";
	
	####
	# Set Debug Mode
	####
	$_DEBUG=1;
	
	####
	# Request Method Validation
	####
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG = test_input($_GET["l"]);
		$IN_DBO = test_input($_GET["dbo"]);
		$IN_REG_ID = test_input($_GET["rid"]);
		$IN_RELAT_ID = test_input($_GET["rltid"]);
		$IN_REG_IDS = test_input($_GET["rids"]);
/ */
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG = test_input($_POST["l"]);
		$IN_DBO = test_input($_POST["dbo"]);
		$IN_RELAT_ID = test_input($_POST["rltid"]);
		$IN_REG_ID = test_input($_POST["rid"]);
		$IN_REG_IDS = test_input($_POST["rids"]);
		
		## case convertion
		
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Carrega Idioma
		$IN_LANG = valida_idioma($IN_LANG);
		carrega_idioma($IN_LANG);
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		## Valida se o usuario esta logado
		if(isLogedIn($mysqli) == false){ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Carrega ID do cliente Prodfy
		$sid_idSYSTEM_CLIENTE    = $_SESSION['user_idsystem_cliente'];
		$sid_USERNAME            = $_SESSION['user_username'];
		$sid_PLANO_CODIGO        = $_SESSION['plano_codigo'];
		if(isEmpty($sid_idSYSTEM_CLIENTE) || isEmpty($sid_USERNAME) || isEmpty($sid_PLANO_CODIGO))
		{ die("0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|"); exit; }
		
		## Autentica dados obrigatorios
		if( isEmpty($IN_DBO) ){ $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; }
		switch($IN_DBO)
		{
			## RELATORIO
			case '8':
				if( isEmpty($IN_DBO) && isEmpty($IN_RELAT_ID) ) { $GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit; break; }
				## Executa
				switch($IN_RELAT_ID)
				{
					case 1://Guia de Inventario de lotes
						$tmp_RESULT = executa_RELATORIO1($mysqli, $_DEBUG, $sid_idSYSTEM_CLIENTE, $IN_RELAT_ID);
						break;
					default:
						break;
				}
				die($tmp_RESULT);
				exit;
				break;
			## DEFAULT
			default:
				$GO=0; die("0|".TXT_FALHA_TRANSF_DADOS."|alert|"); exit;
				break;
		}
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
	####
	# EDIT
	####
	function executa_EDIT($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_SID_USERNAME, $_REG_ID)
	{
		## Valida dados de sessao
		if(isEmpty($_SID_idSYSTEM_CLIENTE) ||
		   isEmpty($_SID_USERNAME))
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_SID_USERNAME         = $mysqli->escape_String($_SID_USERNAME);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados do registro para edicao
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`id` as id
       FROM `` AA
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`id` = ?
        LIMIT 1"
		)) 
		{
			$stmt->bind_param('ss', $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID);
			$stmt->fetch();
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				return "1||success|";
				exit;
			}
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# FICHA
	####
	function executa_FICHA($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_REG_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_REG_ID               = $mysqli->escape_String($_REG_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT AA.`id` as id,
            date_Format(AA.`cad_date`,?) as cad_date,
            AA.`cad_username` as cad_username,
            concat(upper(U1.`nome`),' ',upper(U1.`sobrenome`)) as cad_nome,
            date_Format(AA.`updt_date`,?) as updt_date,
            AA.`updt_username` as updt_username,
            concat(upper(U2.`nome`),' ',upper(U2.`sobrenome`)) as updt_nome
       FROM `` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
      WHERE AA.`idSYSTEM_CLIENTE` = ?
        AND AA.`id` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_ID, $o_CAD_DATE, $o_CAD_USERNAME, $o_CAD_NOME, $o_UPDT_DATE, $o_UPDT_USERNAME, $o_UPDT_NOME);
			$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				
				$ULT_UPDATE_TXT = '';
				if( $o_UPDT_DATE )
				{
					$ULT_UPDATE_TXT = $o_UPDT_DATE." (<i>".$o_UPDT_USERNAME."</i>)";
				}
				else
				{
					$ULT_UPDATE_TXT = $o_CAD_DATE." (<i>".$o_CAD_USERNAME."</i>)";
				}
				
				$FICHA  = '<div style="overflow-x:auto;"><table class="ficha-table">';
				$FICHA .= '	<tr><th nowrap><b>'.TXT_FICHA_ULT_UPDATE.'</b></th><td>'.$ULT_UPDATE_TXT.'</td></tr>';
				$FICHA .= '</table></div>';
				
			}
			
			return "1|".$FICHA."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# RELATORIO1 - Guia de Inventario de Lotes para Coleta de Quantidade em Papel
	####
	function executa_RELATORIO1($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_RELAT_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_RELAT_ID             = $mysqli->escape_String($_RELAT_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		$QUERY = "SELECT upper(AA.`codigo`) as codigo, upper(BJTV.`titulo`) as titulo, 
            upper(MD.`nome_interno`) as muda, 
            upper(CLNT.`nome_interno`) as cliente
       FROM `LOTE` AA
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U1
         ON U1.`username` = AA.`cad_username`
 LEFT JOIN `SYSTEM_USER_ACCOUNT` U2
         ON U2.`username` = AA.`updt_username`
    LEFT JOIN `OBJETIVO` as BJTV
           ON BJTV.`idobjetivo` = AA.`idobjetivo`
    LEFT JOIN `MUDA` as MD
           ON MD.`idmuda` = AA.`idmuda`
    LEFT JOIN `CLIENTE` as CLNT
           ON CLNT.`idcliente` = AA.`idcliente`
        WHERE AA.`idSYSTEM_CLIENTE` = ".$sql_SID_idSYSTEM_CLIENTE."
          AND AA.`uso_liberado` = 1
     ORDER BY 1";
		if ($stmt = $mysqli->prepare($QUERY)) 
		{
			//$stmt->bind_param('ssss', $sql_DDMMYYYYHHMMSS, $sql_DDMMYYYYHHMMSS, $sql_SID_idSYSTEM_CLIENTE, $sql_REG_ID);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CODIGO, $o_OBJETIVO, $o_MUDA, $o_CLIENTE);
			//$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				$RELAT  = '<table id="tpl_relat1" class="table hidden">';
				$RELAT .= '<thead>';
				$RELAT .= '<tr>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_LOTE.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_OBJETIVO.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_MUDA.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_DATA.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_HORA_INICIO.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_HORA_FIM.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_QTDE.'</th>';
				$RELAT .= '</tr>';
				$RELAT .= '</thead>';
				$RELAT .= '<tbody>';
				
				$C=0;
				while($stmt->fetch())
				{
					$RELAT .= '<tr>';
					$RELAT .= '<td>'.$o_CODIGO.'</td>';
					$RELAT .= '<td>'.$o_OBJETIVO.'</td>';
					$RELAT .= '<td>'.$o_MUDA.'</td>';
					$RELAT .= '<td>___/___/______</td>';
					$RELAT .= '<td>___:___:___</td>';
					$RELAT .= '<td>___:___:___</td>';
					$RELAT .= '<td>___________</td>';
					$RELAT .= '</tr>';
					$C++;
				}
				
				$RELAT .= '</tbody>';
				$RELAT .= '</table>';
			}
			
			return "1|".$RELAT."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	####
	# RELATORIO2 - Fichas dos Colaboradores
	####
	function executa_RELATORIO2($mysqli, $_DEBUG, $_SID_idSYSTEM_CLIENTE, $_RELAT_ID)
	{
		## Valida dados de sessao
		if( isEmpty($_SID_idSYSTEM_CLIENTE) )
		{
			return "0|".TXT_USUARIO_NAO_AUTENTICADO."|alert|";
			exit;
		}
		
		## Gera escapes das variaveis
		$sql_DDMMYYYY             = '%d/%m/%Y';
		$sql_DDMMYYYYHHMMSS       = '%d/%m/%Y %T';
		$sql_SID_idSYSTEM_CLIENTE = $mysqli->escape_String($_SID_idSYSTEM_CLIENTE);
		$sql_RELAT_ID             = $mysqli->escape_String($_RELAT_ID);
		
		## Carrega os dados dentro do critério especificado
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT concat(upper(U.`nome`),' ',upper(U.`sobrenome`)) as nome,
            upper(U.`username`) as username,
            lower(U.`email`) as email,
            U.`fone_fixo1` as fonefixo1,
            U.`celular1` as celular1,
            upper(U.`uf`) as uf,
            upper(U.`cep`) as cep,
            upper(M.`cidade`) as cidade,
            upper(U.`bairro`) as bairro,
            upper(U.`end`) as end,
            upper(U.`numero`) as num,
            upper(U.`compl`) as compl,
            U.`cpf` as cpf,
            upper(CB.`situacao`) as sit,
            U.`profile_pic` as pic
       FROM `COLABORADOR` CB
 INNER JOIN `SYSTEM_USER_ACCOUNT` U
         ON U.`username` = CB.`username`
 INNER JOIN `MUNICIPIO_BR` M
         ON M.`codigo_ibge` = U.`cidade_codigo`
      WHERE CB.`idSYSTEM_CLIENTE` = ?
      LIMIT 1"
		)) 
		{
			$stmt->bind_param('s', $sql_SID_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_NOME, $o_USERNAME, $o_EMAIL, $o_FONE1, $o_CEL1, $o_UF, $o_CEP, $o_CIDADE, $o_BAIRRO, $o_END, $o_NUM, $o_COMPL, $o_CPF, $o_SIT, $o_PROFILE_PIC);
			$stmt->fetch();
			
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				return "0|".TXT_MYSQLI_REGISTRO_INEXISTENTE."|alert||||";
				exit;
			}
			else
			{
				$RELAT  = '<table id="tpl_relat2" class="table hidden">';
				$RELAT .= '<thead>';
				$RELAT .= '<tr>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_LOTE.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_OBJETIVO.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_MUDA.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_DATA.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_HORA_INICIO.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_HORA_FIM.'</th>';
				$RELAT .= '<th>'.TXT_RELAT_PROD_R1_QTDE.'</th>';
				$RELAT .= '</tr>';
				$RELAT .= '</thead>';
				$RELAT .= '<tbody>';
				
				$C=0;
				while($stmt->fetch())
				{
					$RELAT .= '<tr>';
					$RELAT .= '<td>'.$o_CODIGO.'</td>';
					$RELAT .= '<td>'.$o_OBJETIVO.'</td>';
					$RELAT .= '<td>'.$o_MUDA.'</td>';
					$RELAT .= '<td>___/___/______</td>';
					$RELAT .= '<td>___:___:___</td>';
					$RELAT .= '<td>___:___:___</td>';
					$RELAT .= '<td>___________</td>';
					$RELAT .= '</tr>';
					$C++;
				}
				
				$RELAT .= '</tbody>';
				$RELAT .= '</table>';
			}
			
			return "1|".$RELAT."|success||";
			exit;
			
		}
		else
		{
			if($_DEBUG){ return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."(". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")|error|"; }
			      else { return "0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|"; }
			exit;
		}
		
	}
	
	
#################################################################################
###########
#####
##
?>
