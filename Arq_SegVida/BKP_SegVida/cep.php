<?php 
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: cep.php
## Função..........: 1. Carrega os dados do cep informado direto dos correios
##                   2. Retorna os dados padronizados: uf, cidade, bairro, logradoudo
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once "geral/language.php";
	include('includes/phpQuery-onefile.php');
		
	function simple_curl($url,$post=array(),$get=array())
	{
		$url = explode('?',$url,2);
		if(count($url) === 2)
		{
			$temp_get = array();
			parse_str($url[1],$temp_get);
			$get = array_merge($get,$temp_get);
		}
	
		$ch = curl_init($url[0]."?".http_build_query($get));
		curl_setopt ($ch, CURLOPT_POST, 1);
		curl_setopt ($ch, CURLOPT_POSTFIELDS, http_build_query($post));
		curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		return curl_exec ($ch);
	}
	
	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_CEP = test_input($_GET["cep"]);

/*	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_CEP = test_input($_POST["cep"]);
*/		
		$html = simple_curl('http://m.correios.com.br/movel/buscaCepConfirma.do',array(
			'cepEntrada'=>$IN_CEP,
			'tipoCep'=>'',
			'cepTemp'=>'',
			'metodo'=>'buscarCep'
		));
		
		//http://m.correios.com.br/movel/buscaCepConfirma.do?cepEntrada=36570000&tipoCep=&cepTemp=&metodo=buscarCep
		
		phpQuery::newDocumentHTML($html, $charset = 'utf-8');
		
		$dados = 
		array(
			'logradouro'=> trim(pq('.caixacampobranco .resposta:contains("Logradouro: ") + .respostadestaque:eq(0)')->html()),
			'bairro'=> trim(pq('.caixacampobranco .resposta:contains("Bairro: ") + .respostadestaque:eq(0)')->html()),
			'cidade/uf'=> trim(pq('.caixacampobranco .resposta:contains("Localidade / UF: ") + .respostadestaque:eq(0)')->html()),
			'cep'=> trim(pq('.caixacampobranco .resposta:contains("CEP: ") + .respostadestaque:eq(0)')->html())
		);
		
		$dados['cidade/uf'] = explode('/',$dados['cidade/uf']);
		$dados['cidade'] = trim($dados['cidade/uf'][0]);
		$dados['uf'] = trim($dados['cidade/uf'][1]);
		unset($dados['cidade/uf']);
		
		//die(json_encode($dados));
		
		//Remove acentos
		$dados['cidade'] = strtoupper(remove_accent($dados['cidade']));
		
		$o_CORREIO_UF = $dados['uf'];
		$o_CORREIO_CIDADE = $dados['cidade'];
		
		if( (isEmpty($o_CORREIO_UF) && isEmpty($o_CORREIO_CIDADE)) ||
		    (strlen($o_CORREIO_UF) < 2 && strlen($o_CORREIO_CIDADE) < 2)
		)
		{
			die("0|Incapaz de comunicar com o computador dos correios. Serviço temporariamente indisponível!|error||||");
			exit;
		}
		
		####
		# Descobre o codigo ibge da cidade
		####
		
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error||||"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT M.`codigo_ibge` as codigo
       FROM `MUNICIPIO_BR` M
      WHERE M.`uf` = ?
        AND upper(M.`cidade`) = upper(?)"
		)) 
		{
			$sql_UF     = $mysqli->escape_string($dados['uf']);
			$sql_CIDADE = $mysqli->escape_string($dados['cidade']);
			//
			$stmt->bind_param('ss', $sql_UF,$sql_CIDADE);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CODIGO);
			$stmt->fetch();
		}
		else
		{
			die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
			exit;
		}
		
		//status|uf|cidade|bairro|logradouro|codigo_ibge|
		die("1|".$dados['uf']."|".$dados['cidade']."|".$dados['bairro']."|".$dados['logradouro']."|".$o_CODIGO."|");
		
	}
	#Se nao for enviado via POST, nao mostra nada, apenas uma tela em branco.
	else
	{
		die("0||||||");
	}
	
#################################################################################
###########
#####
##
?>