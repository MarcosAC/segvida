<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-perfl-cad.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
<!-- CSS - Plano Contratado -->
		<style>
@import url(http://fonts.googleapis.com/css?family=Roboto);

* {
    font-family: 'Roboto', sans-serif;
}


.form-control-select {
	margin-top: 10px;
}

select {
	border-radius: 0px;
	margin-top: 10px;
}


/* #########################################
   #    override the bootstrap configs     #
   ######################################### */

.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: .8;
}

.modal-content {
    background-color: #ececec;
    border: 1px solid #bdc3c7;
    border-radius: 0px;
    outline: 0;
}

.modal-header {
    min-height: 16.43px;
    padding: 15px 15px 15px 15px;
    border-bottom: 0px;
}

.modal-body {
    position: relative;
    padding: 5px 15px 5px 15px;
}

.modal-footer {
    padding: 15px 15px 15px 15px;
    text-align: left;
    border-top: 0px;
}

.checkbox {
    margin-bottom: 0px;
}

.btn {
    border-radius: 0px;
}

.btn:focus,
.btn:active:focus,
.btn.active:focus,
.btn.focus,
.btn:active.focus,
.btn.active.focus {
    outline: none;
}

.btn-lg, .btn-group-lg>.btn {
    border-radius: 0px;
}

.btn-link {
    padding: 5px 10px 0px 0px;
    color: #95a5a6;
}

.btn-link:hover, .btn-link:focus {
    color: #2c3e50;
    text-decoration: none;
}

.glyphicon {
    top: 0px;
}

.form-control {
  border-radius: 0px;
}
.input-upper {
  text-transform: uppercase;
}
.input-lower {
  text-transform: lowercase;
}

#div-label-type 
{
	border: 1px solid #003a1f;
	height: 30px;
	line-height: 28px;
	transition: all ease-in-out 500ms;
	text-align: center;
	font-size: 90%;
	font-weight: bold;
	color: #003a1f;
	margin-bottom: 10px;
}
			
			
* {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 35px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
    border-radius: 4px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}


.input-group-addon {
     min-width:150px;
         width:150px;
    text-align:right;
}

.input-group { width: 100%; }


select:not([multiple]) {
    -webkit-appearance: none;
    -moz-appearance: none;
    background-position: right 50%;
    background-repeat: no-repeat;
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
    padding: .5em;
    padding-right: 1.5em;
}

.no-border {
    border-radius: 0;
}

		</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		//global $user_username, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE()
		//;
		
		
		####
		# Carrega dados do cadastro
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT upper(U.`username`) as username,
              lower(U.`email`) as email,
              upper(U.`nome`) as nome,
              upper(U.`sobrenome`) as sobrenome,
              U.`cpf` as cpf,
              U.`cnpj` as cnpj,
              upper(U.`razao_social`) as razao_social,
              U.`fone_fixo1` as fone_fixo1,
              U.`celular1` as celular1,
              lower(U.`idioma`) as idioma,
              upper(U.`pais`) as pais,
              U.`cep` as cep,
              upper(U.`uf`) as uf,
              U.`cidade_codigo` as cidade_codigo,
              M.`cidade` as cidade_nome,
              upper(U.`bairro`) as bairro,
              upper(U.`end`) as end,
              upper(U.`numero`) as numero,
              upper(U.`compl`) as compl,
              upper(U.`categoria`) as categoria,
              lower(U.`profile_pic`) as profile_pic,
              date_format(U.`cad_date`,?) as cad_date,
              date_format(U.`dth_ult_acesso`,?) as dth_ult_acesso
         FROM `SYSTEM_USER_ACCOUNT` U
    LEFT JOIN `MUNICIPIO_BR` M
           ON M.`codigo_ibge` = U.`cidade_codigo`
        WHERE U.`username` = ?
        LIMIT 1"
			)) 
			{
				$sql_DDMMYYYY = '%d/%m/%Y';
				$sql_DDMMYYYYHHMMSS = '%d/%m/%Y %T';
				$sql_USERNAME       = $mysqli->escape_String($SID_CTRL->getUSER_USERNAME());
				$stmt->bind_param('sss', $sql_DDMMYYYY, $sql_DDMMYYYYHHMMSS, $sql_USERNAME);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_USERNAME, $o_EMAIL, $o_NOME, $o_SOBRENOME, $o_CPF, $o_CNPJ, $o_RAZAO_SOCIAL, $o_FONE_FIXO1, $o_CELULAR1, 
				$o_IDIOMA, $o_PAIS, $o_CEP, $o_UF, $o_CIDADE_CODIGO, $o_CIDADE_NOME, $o_BAIRRO, $o_END, $o_NUMERO, $o_COMPL, $o_CATEGORIA, 
				$o_PROFILE_PIC, $o_CAD_DATE, $o_DTH_ULT_ACESSO);
				$stmt->fetch();
			}
			
			####
			# Formata Profile_PIC
			####
			if( !empty($o_PROFILE_PIC) &&
			    file_exists(PROFILE_PICS_PATH.'/'.$o_PROFILE_PIC)
			) {} else 
			{
				$o_PROFILE_PIC = "no_profile_pic.jpg";
			}
			
			
			####
			# Formata select Pais
			####
			switch($o_PAIS)
			{
				case 'BR':
					$o_PAIS_SET_BR = "selected";
					break;
				case 'US':
					$o_PAIS_SET_US = "selected";
					break;
				case 'ES':
					$o_PAIS_SET_ES = "selected";
					break;
				default:
					$o_PAIS_SET_BR = "selected";
					break;
			}
			
			####
			# Formata select Idioma
			####
			switch($o_IDIOMA)
			{
				case 'pt-br':
					$o_IDIOMA_SET_PT_BR = "selected";
					break;
				case 'en-us':
					$o_IDIOMA_SET_EN_US = "selected";
					break;
				case 'es-es':
					$o_IDIOMA_SET_ES_ES = "selected";
					break;
				default:
					$o_IDIOMA_SET_PT_BR = "selected";
					break;
			}
			
			####
			# Formata select UF
			####
			switch($o_UF)
			{
				case 'AC':
					$o_UF_SET_AC = "selected";
					break;
				case 'AL':
					$o_UF_SET_AL = "selected";
					break;
				case 'AM':
					$o_UF_SET_AM = "selected";
					break;
				case 'AP':
					$o_UF_SET_AP = "selected";
					break;
				case 'BA':
					$o_UF_SET_BA = "selected";
					break;
				case 'CE':
					$o_UF_SET_CE = "selected";
					break;
				case 'DF':
					$o_UF_SET_DF = "selected";
					break;
				case 'ES':
					$o_UF_SET_ES = "selected";
					break;
				case 'GO':
					$o_UF_SET_GO = "selected";
					break;
				case 'MA':
					$o_UF_SET_MA = "selected";
					break;
				case 'MG':
					$o_UF_SET_MG = "selected";
					break;
				case 'MS':
					$o_UF_SET_MS = "selected";
					break;
				case 'MT':
					$o_UF_SET_MT = "selected";
					break;
				case 'PA':
					$o_UF_SET_PA = "selected";
					break;
				case 'PB':
					$o_UF_SET_PB = "selected";
					break;
				case 'PE':
					$o_UF_SET_PE = "selected";
					break;
				case 'PI':
					$o_UF_SET_PI = "selected";
					break;
				case 'PR':
					$o_UF_SET_PR = "selected";
					break;
				case 'RJ':
					$o_UF_SET_RJ = "selected";
					break;
				case 'RN':
					$o_UF_SET_RN = "selected";
					break;
				case 'RO':
					$o_UF_SET_RO = "selected";
					break;
				case 'RR':
					$o_UF_SET_RR = "selected";
					break;
				case 'RS':
					$o_UF_SET_RS = "selected";
					break;
				case 'SC':
					$o_UF_SET_SC = "selected";
					break;
				case 'SE':
					$o_UF_SET_SE = "selected";
					break;
				case 'SP':
					$o_UF_SET_SP = "selected";
					break;
				case 'TO':
					$o_UF_SET_TO = "selected";
					break;
				default:
					break;
			}
			
			####
			# Gera lista de cidades para o UF do cadastro
			####
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT M.`codigo_ibge` as codigo_ibge,
              upper(M.`uf`) as uf,
              upper(M.`cidade`) as cidade
         FROM `MUNICIPIO_BR` M
        WHERE M.`uf` = ?
        ORDER BY M.`cidade`"
			)) 
			{
				$sql_UF = $mysqli->escape_String($o_UF);
				$stmt->bind_param('s', $sql_UF);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_LISTA_CODIGO, $o_LISTA_UF, $o_LISTA_CIDADE);
				
				while($stmt->fetch())
				{
					$set = '';
					if($o_CIDADE_CODIGO == $o_LISTA_CODIGO){ $set = 'selected'; }
					$LISTA_CIDADES .= '<option value="'.$o_LISTA_CODIGO.'"'.$set.'>'.$o_LISTA_CIDADE.'</option>';
				}
			}
		
		
		
		
		
		}# /carrega dados #
		
?>
		<!-- main_table -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_MOD_PERFIL_PERFIL; ?></a></li>
							<li class="active"><span><?php echo TXT_MOD_PERFIL_CADASTRO; ?></span></li>
						</ol></h2>
						<div class="clearfix"></div>
				</div>
				
				<!-- Dados Basicos da conta -->
				<div class="row">
					<div class="col-xs-6 col-sm-2 text-center center-block">
						<img id="profile_pic" width=128 height=128 src="<?php echo PROFILE_PICS_URL."/".$o_PROFILE_PIC; ?>" style="border: 1px solid #0000ff;" vspace="9"><br/>
						<a role="button" data-toggle="modal" data-target="#alterafoto-modal"><b>Alterar Foto</b></a>
					</div>
					<div class="col-xs-10 col-sm-7 col-centered">
						<div id="panel_dados_basicos">
							<h2><ol class="breadcrumb breadcrumb-arrow-small1">
									<li><a>Usuário</a></li>
									<li class="active"><span style="color:#0000ff;"> <?php echo $o_USERNAME; ?></span></li>
								</ol></h2>
							<h2><ol class="breadcrumb breadcrumb-arrow-small1">
									<li><a>Identificação</a></li>
									<li class="active"><span id="panel_dados_basicos_identificacao" style="color:#0000ff;"> <?php echo $o_NOME." ".$o_SOBRENOME; ?> &nbsp;&nbsp;<i class="fa fa-chevron-right"></i>&nbsp; CPF: <?php echo $o_CPF; ?></span></li>
								</ol></h2>
							<h2><ol class="breadcrumb breadcrumb-arrow-small1">
									<li><a>Cadastro</a></li>
									<li class="active"><span style="color:#0000ff;"> <?php echo $o_CAD_DATE; ?></span></li>
								</ol></h2>
							<h2><ol class="breadcrumb breadcrumb-arrow-small1">
									<li><a>Último Acesso</a></li>
									<li class="active"><span style="color:#0000ff;"> <?php echo $o_DTH_ULT_ACESSO; ?></span></li>
								</ol></h2>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<!-- /Dados Basicos da conta -->
				<br/>
					
				<div class="x_content">
					<!-- /main_tabe -->
					<div class="col-xs-12 col-sm-9 col-centered">
						<h3 style="color:#434a54;"><span class="fa fa-user-circle-o" aria-hidden="true"></span> Dados Cadastrais</h3>
						<div class="clearfix"></div>
						<p align="justify" style="color:#434a54; font-size: 120%;">Para atualizar seus dados cadastrais, efetue as alterações desejadas e clique em SALVAR.</p>
						<p align="justify" style="color:#434a54; font-size: 120%;">Obs.: Campos com um <span style="color:#ff0000;">*</span> são obrigatórios</p>
						
						<!-- Begin | Register Form -->
						<form id="cad-form">
							<!-- <input id="cad_username" value="<?php echo $o_USERNAME; ?>" type=hidden> -->
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_NOME; ?>:</b>
								</span>
								<input id="cad_nome" type="text" maxlength=45 class="form-control textonly_az input-upper" required
								value="<?php echo $o_NOME; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_SOBRENOME; ?>:</b>
								</span>
								<input id="cad_sobrenome" type="text" maxlength=45 class="form-control textonly_az input-upper" required
								value="<?php echo $o_SOBRENOME; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_EMAIL; ?>:</b>
								</span>
								<input id="cad_email" type="text" maxlength=180 class="form-control input-lower" required
								value="<?php echo $o_EMAIL; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_CPF; ?>:</b>
								</span>
								<input id="cad_cpf" type="text" maxlength=14 class="form-control numeric" required
								value="<?php echo $o_CPF; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_MOD_PERFIL_CNPJ; ?>:</b>
								</span>
								<input id="cad_cnpj" type="text" maxlength=18 class="form-control numeric" 
								value="<?php echo $o_CNPJ; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_MOD_PERFIL_RAZAO_SOCIAL; ?>:</b>
								</span>
								<input id="cad_razaosocial" type="text" maxlength=180 class="form-control textonly input-upper" 
								value="<?php echo $o_RAZAO_SOCIAL; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_FONE_FIXO; ?>:</b>
								</span>
								<input id="cad_fonefixo1" type="text" maxlength=15 class="form-control numeric" required
								value="<?php echo $o_FONE_FIXO1; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_MOD_PERFIL_CELULAR; ?>:</b>
								</span>
								<input id="cad_celular1" type="text" maxlength=15 class="form-control numeric" 
								value="<?php echo $o_CELULAR1; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_PAIS; ?>:</b>
								</span>
								<select id="cad_pais" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="BR" <?php echo $o_PAIS_SET_BR; ?>><?php echo TXT_MOD_PERFIL_BRASIL; ?></option>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_IDIOMA; ?>:</b>
								</span>
								<select id="cad_idioma" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="pt-br" <?php echo $o_IDIOMA_SET_PT_BR; ?>><?php echo TXT_MOD_PERFIL_PORTUGUES_BRASIL; ?></option>
									<!-- <option value="en-us" <?php echo $o_IDIOMA_SET_EN_US; ?>><?php echo TXT_MOD_PERFIL_US_ENGLISH; ?></option> -->
									<!-- <option value="es-es" <?php echo $o_IDIOMA_SET_ES_ES; ?>><?php echo TXT_MOD_PERFIL_ESPANHOL; ?></option> -->
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_CEP; ?>:</b>
								</span>
								<input id="cad_cep" type="text" maxlength=10 class="form-control numeric" required
								data-toggle="tooltip" data-placement="auto bottom" title="<?php echo TXT_MOD_PERFIL_CEP_MSG; ?>"
								value="<?php echo $o_CEP; ?>">
								<input id="cad_cep_uf" type="hidden">
								<input id="cad_cep_cidade_codigo" type="hidden">
								<!--
								<span class="input-group-addon">
									<a id="cad_search_cep_btn" class="btn-lightgray" role="button"><span class="fa fa-search" aria-hidden="true"></span></a>
								</span>
								-->
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_UF; ?>:</b>
								</span>
								<select id="cad_uf" class="form-control" style="margin-top:0; border-radius:0;" required>
									<option value="AC" <?php echo $o_UF_SET_AC; ?>>ACRE</option><option value="AL" <?php echo $o_UF_SET_AL; ?>>ALAGOAS</option><option value="AM" <?php echo $o_UF_SET_AM; ?>>AMAZONAS</option><option value="AP" <?php echo $o_UF_SET_AP; ?>>AMAPÁ</option><option value="BA" <?php echo $o_UF_SET_BA; ?>>BAHIA</option><option value="CE" <?php echo $o_UF_SET_CE; ?>>CEARÁ</option><option value="DF" <?php echo $o_UF_SET_DF; ?>>DISTRITO FEDERAL</option><option value="ES" <?php echo $o_UF_SET_ES; ?>>ESPÍRITO SANTO</option><option value="GO" <?php echo $o_UF_SET_GO; ?>>GOIÁS</option><option value="MA" <?php echo $o_UF_SET_MA; ?>>MARANHÃO</option><option value="MG" <?php echo $o_UF_SET_MG; ?>>MINAS GERAIS</option><option value="MS" <?php echo $o_UF_SET_MS; ?>>MATO GROSSO DO SUL</option><option value="MT" <?php echo $o_UF_SET_MT; ?>>MATO GROSSO</option><option value="PA" <?php echo $o_UF_SET_PA; ?>>PARÁ</option><option value="PB" <?php echo $o_UF_SET_PB; ?>>PARAÍBA</option><option value="PE" <?php echo $o_UF_SET_PE; ?>>PERNAMBUCO</option><option value="PI" <?php echo $o_UF_SET_PI; ?>>PIAUÍ</option><option value="PR" <?php echo $o_UF_SET_PR; ?>>PARANÁ</option><option value="RJ" <?php echo $o_UF_SET_RJ; ?>>RIO DE JANEIRO</option><option value="RN" <?php echo $o_UF_SET_RN; ?>>RIO GRANDE DO NORTE</option><option value="RO" <?php echo $o_UF_SET_RO; ?>>RONDÔNIA</option><option value="RR" <?php echo $o_UF_SET_RR; ?>>RORAIMA</option><option value="RS" <?php echo $o_UF_SET_RS; ?>>RIO GRANDE DO SUL</option><option value="SC" <?php echo $o_UF_SET_SC; ?>>SANTA CATARINA</option><option value="SE" <?php echo $o_UF_SET_SE; ?>>SERGIPE</option><option value="SP" <?php echo $o_UF_SET_SP; ?>>SÃO PAULO</option><option value="TO" <?php echo $o_UF_SET_TO; ?>>TOCANTINS</option>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_CIDADE; ?>:</b>
								</span>
								<select id="cad_cidade" class="form-control" style="margin-top:0; border-radius:0;" required>
									<?php echo $LISTA_CIDADES; ?>
								</select>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_BAIRRO; ?>:</b>
								</span>
								<input id="cad_bairro" type="text" maxlength=180 class="form-control textonly input-upper" required
								value="<?php echo $o_BAIRRO; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;Endereço:</b>
								</span>
								<input id="cad_end" type="text" maxlength=180 class="form-control textonly input-upper" required
								value="<?php echo $o_END; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_MOD_PERFIL_NUMERO; ?>:</b>
								</span>
								<input id="cad_numero" type="text" maxlength=180 class="form-control textonly input-upper" required
								value="<?php echo $o_NUMERO; ?>">
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_MOD_PERFIL_COMPL; ?>:</b>
								</span>
								<input id="cad_compl" type="text" maxlength=180 class="form-control textonly input-upper" 
								value="<?php echo $o_COMPL; ?>">
							</div>
							<!--
							<div class="input-group">
								<span class="input-group-addon">
									<b><span style="color:#ff0000;">*</span>&nbsp;Senha Atual:</b>
								</span>
								<input id="cad_password" type="password" maxlength=30 class="form-control" style="margin-top:0;" required
								value="">
							</div>
							-->
							<div>
								<button id="bt_cad_salvar" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SAVE; ?></button>
							</div>
						</form>
						<!-- End | cad Form -->
						
					</div><!--/.col-xs-12.col-sm-9-->
	
	
	
					</div>
				</div>
			</div>
		</div>












<!-- BEGIN # MODAL UPLOAD FOTO -->
<div class="modal fade" id="alterafoto-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" align="left">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="text-align:right; margin-top: 17px;">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
				<h2><ol class="breadcrumb breadcrumb-arrow">
						<li><a><?php echo TXT_MOD_PERFIL_PERFIL; ?></a></li>
						<li class="active"><span><?php echo TXT_MOD_PERFIL_ALTERAR_FOTO; ?></span></li>
					</ol></h2>
			</div>
			
			<!-- Begin # DIV Form -->
			<div id="div-forms">
				<!-- Begin | Register Form -->
				<form id="alterafoto-form">
					<div class="modal-body">
						<div id="div-alterafoto-msg">
							<div id="icon-alterafoto-msg" class="glyphicon glyphicon-chevron-right">
								<span id="text-alterafoto-msg"><?php echo TXT_MOD_PERFIL_IMG_MSG; ?></span>
							</div>
							<div class="input-group">
								<span class="input-group-addon">
									<b><?php echo TXT_MOD_PERFIL_ARQUIVO; ?>:</b>
								</span>
								<input id="file_info" type="text" class="form-control" readonly>
								<label class="input-group-btn">
									<span class="btn btn-primary" style="margin-top: 0px; margin-bottom: 0px; border-radius: 3px;">
									<b class="fa fa-folder-open-o"></b> 
									<input type="hidden" name="MAX_FILE_SIZE" value="80000">
									<input id="file_upload" type="file" name="file" class="form-control" style="display: none;" required>
									</span>
								</label>
							</div>
							<div class="modal-footer">
								<div>
									<button id="bt_enviar_foto" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SEND; ?></button>
								</div>
							</div>
						</div>
					</div>
				</form>
				<!-- End | alterafoto Form -->
				
			</div>
			<!-- End # DIV Form -->
			
		</div>
	</div>
</div>
<!-- END # MODAL UPLOAD FOTO -->








<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/view-prfl-cad.js"></script>
		
		
<?php
	} 
?>
