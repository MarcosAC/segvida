<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: index.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 02/09/2017 13:22:11
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<style>
		.panel-default > .panel-heading-custom 
		{
			background: #003a1f; 
			color: #fff;
			font-size: 100%;
			line-height:20px;
		}
		.panel_bgcolor_gray 
		{
			background: #e6e6e6;
		}
		.panel_info
		{
			color:#000;
			font-size: 200%;
			font-weight: bold;
		}
		.panel_color_green
		{
			color:#003a1f;
		}
		.panel_color_black
		{
			color:#000;
		}
		.columns {
				float: left;
				width: 20%;//33.3%;
				padding: 8px;
		}
	</style>
<?php
	}
	
	####
	# FUNCOES DE APOIO
	####
	function geraPainel($_IND_ACESSAR, $_TOTAL, $_ICON, $_PG_LINK, $_TITLE)
	{
		$RET = '';
		$RET .= '<div class="columns">';
		$RET .= '	<div class="panel panel-default">';
		$RET .= '		<div class="panel-heading panel-heading-custom">';
		$RET .= '			<h3 class="panel-title">'.$_TITLE.'</h3>';
		$RET .= '		</div>';
		$RET .= '		<div class="panel-body panel_bgcolor_gray">';
		$RET .= '			<center><table><tr>';
		$RET .= '				<td><i class="fa '.$_ICON.' fa-2x panel_color_green"></i>&nbsp;&nbsp;</td>';
		$RET .= '				<td><div class="panel_info">'.$_TOTAL.'</div></td>';
		$RET .= '			</tr></table></center>';
		$RET .= '		</div>';
		if($_IND_ACESSAR == 1)
		{
			$RET .= '		<div class="panel-footer panel-heading-custom"><a href="prodfy.php?pg='.$_PG_LINK.'" style="color:white;">+ '.TXT_PAINEL_DASHBOARD_PAINEL_DETALHES.'</a></div>';
		}
		else
		{
			$RET .= '		<div class="panel-footer panel-heading-custom"><a style="color:#737475; cursor: not-allowed;"><i>+ '.TXT_PAINEL_DASHBOARD_PAINEL_DETALHES.'</i></a></div>';
		}
		$RET .= '	</div>';
		$RET .= '</div>';
		return $RET;
	}
	function getTotalMudas($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `MUDA` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalDoencas($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `MUDA_DOENCA` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalEspecies($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `MUDA_ESPECIE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalColaboradores($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `COLABORADOR` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalDispositivos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `DISPOSITIVO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		return $RET;
	}
	function getTotalClientes($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `CLIENTE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalContratos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `CONTRATO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalObjetivos($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `OBJETIVO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalPontosDeControle($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `PONTO_CONTROLE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalEstagios($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `PONTO_CONTROLE_ESTAGIO` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	function getTotalLotes($mysqli, $_USER_SYSTEM_CLIENTE_ID)
	{
		##Escapes
		$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($_USER_SYSTEM_CLIENTE_ID);
		//$sql_idLOTE           = $mysqli->escape_String($_idLOTE);
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT count(1) as total
       FROM `LOTE` AA
 INNER JOIN `SYSTEM_CLIENTE` C
         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
      WHERE AA.`idSYSTEM_CLIENTE` = ?"
		)) 
		{
			$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_QTDE);
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 0) 
			{
				$RET = 0;
			}
			else
			{
				$stmt->fetch();
				$RET = 0;
				if($o_QTDE){ $RET = $o_QTDE; }
			}
			
		}
		
		return $RET;
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		## Carrega dados
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			
			####
			# Carrega Matriz de Acesso do Usuário
			####
			{
				# Carrega relacao de modulos
				$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				$sql_USER_CATEG = $mysqli->escape_String($SID_CTRL->getUSER_CATEGORIA());
				$sql_idCOLABORADOR = $mysqli->escape_String($SID_CTRL->getUSER_IDCOLABORADOR());
				
				
				//$sql_USER_CATEG = $mysqli->escape_String($user_categoria);
				//$sql_idCOLABORADOR = $mysqli->escape_String($user_colaborador_id);
				
				$QUERY = "SELECT PNA.`modulo_codigo`,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_acessar` is null THEN 0 ELSE CA.`ind_acessar` END
	              END as ind_acessar,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_editar` is null THEN 0 ELSE CA.`ind_editar` END
	              END as ind_editar,
	              CASE WHEN '".$sql_USER_CATEG."' = 'CLI' THEN 1
	                   WHEN '".$sql_USER_CATEG."' = 'USR' THEN CASE WHEN CA.`ind_apagar` is null THEN 0 ELSE CA.`ind_apagar` END
	              END as ind_apagar
	         FROM `SYSTEM_NIVEL_ACESSO` PNA
	    LEFT JOIN `COLABORADOR_ACESSO` CA
	           ON CA.`modulo_codigo` = PNA.`modulo_codigo`
	          AND CA.`idSYSTEM_CLIENTE` = '".$sql_idSYSTEM_CLIENTE."'
	          AND CA.`idCOLABORADOR` = '".$sql_idCOLABORADOR."'
	        WHERE `modulo_liberado` = 1 
	     ORDER BY `ordem`";
				if ($stmt = $mysqli->prepare($QUERY)) 
				{
					//$stmt->bind_param('ssssssss', $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_USER_CATEG, $sql_idSYSTEM_CLIENTE, $sql_idCOLABORADOR);
					$stmt->execute();
					$stmt->store_result();
					//
					// obtém variáveis a partir dos resultados. 
					$stmt->bind_result($o_MOD_COD, $o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
					
					$ACESSO_CTRL = array();
					
					while($stmt->fetch())
					{
						# Define nivel de acesso
						if($o_MOD_COD)
						{
							$ACESSO_CTRL[$o_MOD_COD] = new AcessoCTRL($o_MOD_ACESSAR, $o_MOD_EDITAR, $o_MOD_APAGAR);
						}
					}
					
				}
			}
			
			##Carrega Indicadores do Dashboard
			
			##Escapes
			$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT lower(AA.`codigo`) as codigo,
	            AA.`ordem` as ordem
	       FROM `SYSTEM_CFG_DASHBOARD` AA
	 INNER JOIN `SYSTEM_CLIENTE` C
	         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
	      WHERE AA.`idSYSTEM_CLIENTE` = ?
	        AND AA.`ind_exibir` = 1
	   ORDER BY AA.`ordem`"
			)) 
			{
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CODIGO,$o_ORDEM);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$RET = 0;
				}
				else
				{
					while($stmt->fetch())
					{
						switch($o_CODIGO)
						{
							/*
							case 'lote':
								$o_TOTAL = getTotalLotes($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['prod-lote']->acessar, $o_TOTAL, 'fa-archive', 'prod-lote', TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES);
								break;
							*/
							case 'contrato':
								$o_TOTAL = getTotalContratos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['adm-contrato']->acessar, $o_TOTAL, 'fa-file-text', 'adm-contrato', TXT_PAINEL_DASHBOARD_TOTAL_DE_CONTRATOS);
								break;
							case 'cliente':
								$o_TOTAL = getTotalClientes($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['adm-cliente']->acessar, $o_TOTAL, 'fa-address-card', 'adm-cliente', TXT_PAINEL_DASHBOARD_TOTAL_DE_CLIENTES);
								break;
							/*
							case 'pontocontrole':
								$o_TOTAL = getTotalPontosDeControle($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['prod-ponto_controle']->acessar, $o_TOTAL, 'fa-check-circle-o', 'prod-ponto_controle', TXT_PAINEL_DASHBOARD_TOTAL_DE_PONTOS_DE_CONTROLE);
								break;
							case 'estagio':
								$o_TOTAL = getTotalEstagios($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['prod-ponto_controle_estagio']->acessar, $o_TOTAL, 'fa-check-square-o', 'prod-ponto_controle_estagio', TXT_PAINEL_DASHBOARD_TOTAL_DE_ESTAGIOS);
								break;
							case 'objetivo':
								$o_TOTAL = getTotalObjetivos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['prod-objetivo']->acessar, $o_TOTAL, 'fa-bullseye', 'prod-objetivos', TXT_PAINEL_DASHBOARD_TOTAL_DE_OBJETIVOS);
								break;
							case 'muda':
								$o_TOTAL = getTotalMudas($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['mudas-muda']->acessar, $o_TOTAL, 'fa-envira', 'mudas-muda', TXT_PAINEL_DASHBOARD_TOTAL_DE_MUDAS);
								break;
							case 'doenca':
								$o_TOTAL = getTotalDoencas($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['mudas-doenca']->acessar, $o_TOTAL, 'fa-user-md', 'mudas-doenca', TXT_PAINEL_DASHBOARD_TOTAL_DE_DOENCAS);
								break;
							case 'especie':
								$o_TOTAL = getTotalEspecies($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['mudas-especie']->acessar, $o_TOTAL, 'fa-pagelines', 'mudas-especie', TXT_PAINEL_DASHBOARD_TOTAL_DE_ESPECIES);
								break;
							*/
							case 'colaborador':
								$o_TOTAL = getTotalColaboradores($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['cfg-colabs']->acessar, $o_TOTAL, 'fa-id-badge', 'cfg-colabs', TXT_PAINEL_DASHBOARD_TOTAL_DE_COLABORADORES);
								break;
							/*
							case 'dispositivo':
								$o_TOTAL = getTotalDispositivos($mysqli, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
								$PANELS .= geraPainel($ACESSO_CTRL['cfg-dispositivos']->acessar, $o_TOTAL, 'fa-mobile', 'cfg-dispositivos', TXT_PAINEL_DASHBOARD_TOTAL_DE_DISPOSITIVOS);
								break;
							*/
							default:
								break;
						}
					}
				}
				
			}
			
		}
		##/carega planos
	

		
?>
		<!-- main_table -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_PAINEL_DASHBOARD_PAINEL_DE_CONTROLE; ?></a></li>
							<li class="active"><span><?php echo TXT_PAINEL_DASHBOARD_DASHBOARD; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div class="row">
						<?php echo $PANELS; ?>
					</div>
					
				</div>
			</div>
		</div>
		<!-- /main_tabe -->
		
<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
<?php
	} 
?>
