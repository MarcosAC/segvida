<?php
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
?>	

<?php 
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Sobre o Sistema" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="css/offcanvas.css">
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO; ?></a></li>
						<li><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO; ?></a></a></li>
						<li><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS; ?></a></a></li>
						<li><a href="usuarios.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_USERS; ?></a></a></li>
						<li class="active"><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE; ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO; ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">

				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-info-circle" aria-hidden="true"></span> Sobre o Prodfy Mudas</h3>
					</div>
					
					<div class="row text-center center-block">
						<div class="media">
							<div class="media-left media-top">
								<!--
								<img class="media-object" src="img/rlg_circle_2015.png" alt="Rodrigo Leite Gomide"><br/>
								<a class="btn btn-default center-block" href="docs/rlgomide_curriculo_2016_br.pdf" target="_blank" role="button"><span class="fa fa-download" aria-hidden="true"></span>&nbsp;Currículo</a>
								-->&nbsp;
							</div>
							<div class="media-body">
								<h3 class="media-heading text-left">Prodfy Mudas - Sistema de Controle de Produção de Mudas</h3>
								<p class="text-left">
									<span class="label label-green">Controle de Produção</span> 
									<span class="label label-green">Clientes</span> 
									<span class="label label-green">Mudas</span> 
									<span class="label label-green">Espécies</span> 
									<span class="label label-green">Doenças</span> 
									<span class="label label-green">Dispositivos</span> 
									<span class="label label-green">Aplicativo de Campo</span> 
								</p>
								<p class="text-justify text-margin-20">
									In sem justo, commodo ut, suscipit at, pharetra vitae, orci. Duis sapien nunc, commodo et, interdum suscipit, sollicitudin et, dolor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aliquam id dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Mauris dictum facilisis augue. Fusce tellus. Pellentesque arcu. Maecenas fermentum, sem in pharetra pellentesque, velit turpis volutpat ante, in pharetra metus odio a lectus. Sed elit dui, pellentesque a, faucibus vel, interdum nec, diam. Mauris dolor felis, sagittis at, luctus sed, aliquam non, tellus. Etiam ligula pede, sagittis quis, interdum ultricies, scelerisque eu, urna. Nullam at arcu a est sollicitudin euismod. Praesent dapibus. Duis bibendum, lectus ut viverra rhoncus, dolor nunc faucibus libero, eget facilisis enim ipsum id lacus. Nam sed tellus id magna elementum tincidunt. Morbi a metus. Phasellus enim erat, vestibulum vel, aliquam a, posuere eu, velit. Nullam sapien sem, ornare ac, nonummy non, lobortis a, enim. Nunc tincidunt ante vitae massa. Duis ante orci, molestie vitae, vehicula venenatis, tincidunt ac, pede. Nulla accumsan, elit sit amet varius semper, nulla mauris mollis quam, tempor suscipit diam nulla vel leo. Etiam commodo dui eget wisi. Donec iaculis gravida nulla. Donec quis nibh at felis congue commodo. Etiam bibendum elit eget erat.
								</p>
								<br/>
								
							</div>
						</div>
						<br/>

						
					</div><!--/row-->
				</div><!--/.col-xs-12.col-sm-9-->

<!--
				<div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar">
					<div class="list-group">
						<div class="list-group-item list-group-item-gray active"><span class="fa fa-chevron-right" aria-hidden="true"></span>&nbsp;Recursos</div>
						<a href="#" class="list-group-item">xxx</a>
						<a href="#" class="list-group-item">yyy</a>
						<a href="#" class="list-group-item">zzz</a>
					</div>
					
					<div class="list-group">
						<div class="list-group-item list-group-item-gray active"><span class="fa fa-chevron-right" aria-hidden="true"></span>&nbsp;Idiomas</div>
						<a href="#" class="list-group-item">Português</a>
						<a href="#" class="list-group-item">Inglês</a>
						<a href="#" class="list-group-item">Espanhol</a>
					</div>
					
				</div>
				-->
				<!--/.sidebar-offcanvas-->
			</div><!--/row-->
		</div><!--/container-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>