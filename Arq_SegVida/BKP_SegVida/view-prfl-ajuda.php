<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-prfl-ajuda.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
<!-- CSS - Plano Contratado -->
		<style>
@import url(http://fonts.googleapis.com/css?family=Roboto);

* {
    font-family: 'Roboto', sans-serif;
}


.form-control-select {
	margin-top: 10px;
}

select {
	border-radius: 0px;
	margin-top: 10px;
}


/* #########################################
   #    override the bootstrap configs     #
   ######################################### */

.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: .8;
}

.modal-content {
    background-color: #ececec;
    border: 1px solid #bdc3c7;
    border-radius: 0px;
    outline: 0;
}

.modal-header {
    min-height: 16.43px;
    padding: 15px 15px 15px 15px;
    border-bottom: 0px;
}

.modal-body {
    position: relative;
    padding: 5px 15px 5px 15px;
}

.modal-footer {
    padding: 15px 15px 15px 15px;
    text-align: left;
    border-top: 0px;
}

.checkbox {
    margin-bottom: 0px;
}

.btn {
    border-radius: 0px;
}

.btn:focus,
.btn:active:focus,
.btn.active:focus,
.btn.focus,
.btn:active.focus,
.btn.active.focus {
    outline: none;
}

.btn-lg, .btn-group-lg>.btn {
    border-radius: 0px;
}

.btn-link {
    padding: 5px 10px 0px 0px;
    color: #95a5a6;
}

.btn-link:hover, .btn-link:focus {
    color: #2c3e50;
    text-decoration: none;
}

.glyphicon {
    top: 0px;
}

.form-control {
  border-radius: 0px;
}
.input-upper {
  text-transform: uppercase;
}
.input-lower {
  text-transform: lowercase;
}

#div-label-type 
{
	border: 1px solid #003a1f;
	height: 30px;
	line-height: 28px;
	transition: all ease-in-out 500ms;
	text-align: center;
	font-size: 90%;
	font-weight: bold;
	color: #003a1f;
	margin-bottom: 10px;
}
			
			
* {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 35px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
    border-radius: 4px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}


.input-group-addon {
     min-width:150px;
         width:150px;
    text-align:right;
}

.input-group { width: 100%; }


select:not([multiple]) {
    -webkit-appearance: none;
    -moz-appearance: none;
    background-position: right 50%;
    background-repeat: no-repeat;
    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
    padding: .5em;
    padding-right: 1.5em;
}

.no-border {
    border-radius: 0;
}

		</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		//global $user_username, $SID_CTRL->getUSER_IDSYSTEM_CLIENTE(), $user_categoria
		//;
		
?>
		<!-- main_table -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_MOD_PERFIL_PERFIL; ?></a></li>
							<li class="active"><span><?php echo TXT_MOD_PERFIL_AJUDA; ?></span></li>
						</ol></h2>
						<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
					<div class="col-xs-12 col-sm-9 col-centered">
						<!--
						<h3 style="color:#434a54;"><span class="fa fa-question-circle" aria-hidden="true"></span> Dúvidas Gerais</h3>
						<div class="clearfix"></div>
						<ul style="color:#434a54; font-size: 120%;">
						<?php if($SID_CTRL->getUSER_CATEGORIA() == "CLI"){ ?>
							<li><a href="main.php?pg=ajuda-duvidas-1" role="button">Como atualizo meus dados cadastrais?</a></li>
							<li><a href="main.php?pg=ajuda-duvidas-2" role="button">Como altero a minha senha de acesso?</a></li>
							<li><a href="main.php?pg=ajuda-duvidas-3" role="button">Como renovo meu plano de acesso?</a></li>
							
						<?php } else if($SID_CTRL->getUSER_CATEGORIA() == "USR"){ ?>
							<li><a href="main.php?pg=ajuda-duvidas-1" role="button">Como atualizo meus dados cadastrais?</a></li>
							<li><a href="main.php?pg=ajuda-duvidas-2" role="button">Como altero a minha senha de acesso?</a></li>
							<li><a href="main.php?pg=ajuda-duvidas-3" role="button">Como renovo meu plano de acesso?</a></li>
						
						<?php } ?>
						</ul>
						
						<h3 style="color:#434a54;"><span class="fa fa-question-circle" aria-hidden="true"></span> Conceitos Básicos</h3>
						<div class="clearfix"></div>
						<ul style="color:#434a54; font-size: 120%;">
						<?php if($SID_CTRL->getUSER_CATEGORIA() == "CLI"){ ?>
							<li><a href="main.php?pg=ajuda-conceitos-1" role="button">Especificação mínima para utilização do Portal Prodfy Mudas</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-2" role="button">Contrato de licenciamento de uso da plataforma</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-3" role="button">Planos de acesso: Mensal, Anual, Personalizado</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-4" role="button">Cliente Prodfy, Colaborador e Cliente Final</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-5" role="button">Tecnologia RFID de comunicação</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-6" role="button">Equipamentos auxiliares: Etiquetas RFID, Scanners RFID, Aplicativos de Integração para Smartphones</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-7" role="button">Como eu começo a configurar o meu sistema?</a></li>
						
						
						<?php } else if($SID_CTRL->getUSER_CATEGORIA() == "USR"){ ?>
							<li><a href="main.php?pg=ajuda-conceitos-1" role="button">Especificação mínima para utilização do Portal Prodfy Mudas</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-2" role="button">Contrato de licenciamento de uso da plataforma</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-3" role="button">Planos de acesso: Mensal, Anual, Personalizado</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-4" role="button">Cliente Prodfy, Colaborador e Cliente Final</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-5" role="button">Tecnologia RFID de comunicação</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-6" role="button">Equipamentos auxiliares: Etiquetas RFID, Scanners RFID, Aplicativos de Integração para Smartphones</a></li>
							<li><a href="main.php?pg=ajuda-conceitos-7" role="button">Como eu começo a configurar o meu sistema?</a></li>
							
						<?php } ?>
						</ul>
						-->
						<h3 style="color:#434a54;"><span class="fa fa-question-circle" aria-hidden="true"></span> Configurações</h3>
						<div class="clearfix"></div>
						<ul style="color:#434a54; font-size: 120%;">
						<?php if($SID_CTRL->getUSER_CATEGORIA() == "CLI"){ ?>
							<li><a href="main.php?pg=ajuda-configuracoes-1" role="button">Como incluo um colaborador para operar o sistema?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-2" role="button">Como incluo um dispositivo no sistema?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-3" role="button">Como defino os parâmetros de sincronia dos dispositivos?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-4" role="button">Como defino as informações que devem ser exibidas no Painel de Controle?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-5" role="button">Como incluo um Cliente Final para ter acesso ao sistema?</a></li>
							
						<?php } else if($SID_CTRL->getUSER_CATEGORIA() == "USR"){ ?>
							<li><a href="main.php?pg=ajuda-configuracoes-1" role="button">Como incluo um colaborador para operar o sistema?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-2" role="button">Como incluo um dispositivo no sistema?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-3" role="button">Como defino os parâmetros de sincronia dos dispositivos?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-4" role="button">Como defino as informações que devem ser exibidas no Painel de Controle?</a></li>
							<li><a href="main.php?pg=ajuda-configuracoes-5" role="button">Como incluo um Cliente Final para ter acesso ao sistema?</a></li>
							
						<?php } ?>
						</ul>
						
						<!--
						<h3 style="color:#434a54;"><span class="fa fa-question-circle" aria-hidden="true"></span> Produção</h3>
						<div class="clearfix"></div>
						<ul style="color:#434a54; font-size: 120%;">
						<?php if($SID_CTRL->getUSER_CATEGORIA() == "CLI"){ ?>
							<li><a href="main.php?pg=ajuda-producao-1" role="button">Conceito básico da Metodologia Prodfy de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-2" role="button">Incluíndo Espécies de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-3" role="button">Incluíndo Doenças de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-4" role="button">Incluíndo Variedades de Mudas de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-5" role="button">Incluíndo Clientes para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-6" role="button">Definindo Objetivos de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-7" role="button">Definindo Pontos de Controle de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-8" role="button">Definindo Estágios de um Ponto de Controle de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-9" role="button">Criando um Lote de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-10" role="button">Alterando a situação de fluxo um Lote de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-11" role="button">Efetuando inventário da quantidade de mudas de um Lote.</a></li>
							<li><a href="main.php?pg=ajuda-producao-12" role="button">Incluíndo registro de histórico de um Lote.</a></li>
							<li><a href="main.php?pg=ajuda-producao-13" role="button">Enxergando o cenário consolidado da situação atual da produção.</a></li>
							
						<?php } else if($SID_CTRL->getUSER_CATEGORIA() == "USR"){ ?>
							<li><a href="main.php?pg=ajuda-producao-1" role="button">Conceito básico da Metodologia Prodfy de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-2" role="button">Incluíndo Espécies de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-3" role="button">Incluíndo Doenças de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-4" role="button">Incluíndo Variedades de Mudas de plantas para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-5" role="button">Incluíndo Clientes para utilização e controle.</a></li>
							<li><a href="main.php?pg=ajuda-producao-6" role="button">Definindo Objetivos de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-7" role="button">Definindo Pontos de Controle de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-8" role="button">Definindo Estágios de um Ponto de Controle de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-9" role="button">Criando um Lote de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-10" role="button">Alterando a situação de fluxo um Lote de Produção.</a></li>
							<li><a href="main.php?pg=ajuda-producao-11" role="button">Efetuando inventário da quantidade de mudas de um Lote.</a></li>
							<li><a href="main.php?pg=ajuda-producao-12" role="button">Incluíndo registro de histórico de um Lote.</a></li>
							<li><a href="main.php?pg=ajuda-producao-13" role="button">Enxergando o cenário consolidado da situação atual da produção.</a></li>
							
						<?php } ?>
						</ul>
						-->
						
					</div>
				</div>
				
				
				
				
						<!--
						<p align="justify" style="color:#434a54; font-size: 120%;"><b>Dúvidas gerais</b></p>
						-->
						
						
			</div>
		</div>

<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<!-- <script src="js/view-prfl-cad.js"></script> -->
<?php
	} 
?>
