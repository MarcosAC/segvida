<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-clientes-cad.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<!-- Datatables -->
	<link rel="stylesheet" href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css">
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 450px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	
	/*##################
	  # confirm-dialog #
	  ################## */
	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
	
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	.input-group-addon {
	     min-width:160px;
	         width:160px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Ficha
	.ficha-table {
		with:100%;
	}
	
	.ficha-table th {
		width: 100px;
		text-align: right;
		vertical-align: top;
		padding: 2px;
		color:black;
		white-space: nowrap;
	}
	.ficha-table td {
		width: *;
		text-align: left;
		vertical-align: top;
		padding: 2px;
		color:#0000ff;
	}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		####
		# Carrega dados do modulo
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			## Inicia modulo sem limitacoes de plano
			$o_LIMITE_REGS = -1;//Sem limites de registros
			
//			## Carrega limitacoes do plano contratado
//			
//			##Prepara query
//			if ($stmt = $mysqli->prepare(
//			"SELECT P.`total_colaboradores` as total
//         FROM `SYSTEM_PLANO_PAGTO` P
//        WHERE P.`codigo` = ?"
//			)) 
//			{
//				$sql_PLANO   = $mysqli->escape_String($user_plano_codigo);
//				//
//				$stmt->bind_param('s', $sql_PLANO);
//				$stmt->execute();
//				$stmt->store_result();
//				//
//				// obtém variáveis a partir dos resultados. 
//				$stmt->bind_result($o_LIMITE_REGS);
//				
//				$stmt->fetch();
//				
//				##Se nao encontrou dados, retorna
//				if ($stmt->num_rows == 0) 
//				{
//					$o_LIMITE_REGS = 0;
//				}
//				
//			}
			
			## Carrega lista de registros
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT AA.`idCLIENTE` as id, upper(AA.`nome_interno`) as nome_interno, upper(AA.`empresa`) as empresa, AA.`uf` as uf, upper(MB.`cidade`) as cidade_codigo, AA.`situacao` as situacao
         FROM `CLIENTE` AA
   INNER JOIN `SYSTEM_CLIENTE` C
           ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
    LEFT JOIN `MUNICIPIO_BR` as MB
           ON MB.`codigo_ibge` = AA.`cidade_codigo`
        WHERE AA.`idSYSTEM_CLIENTE` = ?
     ORDER BY 1,2"
			)) 
			{
				$sql_idSYSTEM_CLIENTE   = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_ID, $o_NOME_INTERNO, $o_EMPRESA, $o_UF, $o_CIDADE, $o_SITUACAO);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$TBODY_LIST = "";
				}
				else
				{
					$TBODY_LIST = ""; $C=0;
					while($stmt->fetch())
					{
						# Formata select options information
						if( $o_UF == "AC"){ $o_UF_TXT = TXT_CLIENTES_CAD_ACRE; }
						if( $o_UF == "AL"){ $o_UF_TXT = TXT_CLIENTES_CAD_ALAGOAS; }
						if( $o_UF == "AM"){ $o_UF_TXT = TXT_CLIENTES_CAD_AMAZONAS; }
						if( $o_UF == "AP"){ $o_UF_TXT = TXT_CLIENTES_CAD_AMAPA; }
						if( $o_UF == "BA"){ $o_UF_TXT = TXT_CLIENTES_CAD_BAHIA; }
						if( $o_UF == "CE"){ $o_UF_TXT = TXT_CLIENTES_CAD_CEARA; }
						if( $o_UF == "DF"){ $o_UF_TXT = TXT_CLIENTES_CAD_DISTRITO_FEDERAL; }
						if( $o_UF == "ES"){ $o_UF_TXT = TXT_CLIENTES_CAD_ESPIRITO_SANTO; }
						if( $o_UF == "GO"){ $o_UF_TXT = TXT_CLIENTES_CAD_GOIAS; }
						if( $o_UF == "MA"){ $o_UF_TXT = TXT_CLIENTES_CAD_MARANHAO; }
						if( $o_UF == "MG"){ $o_UF_TXT = TXT_CLIENTES_CAD_MINAS_GERAIS; }
						if( $o_UF == "MS"){ $o_UF_TXT = TXT_CLIENTES_CAD_MATO_GROSSO_DO_SUL; }
						if( $o_UF == "MT"){ $o_UF_TXT = TXT_CLIENTES_CAD_MATO_GROSSO; }
						if( $o_UF == "PA"){ $o_UF_TXT = TXT_CLIENTES_CAD_PARA; }
						if( $o_UF == "PB"){ $o_UF_TXT = TXT_CLIENTES_CAD_PARAIBA; }
						if( $o_UF == "PE"){ $o_UF_TXT = TXT_CLIENTES_CAD_PERNAMBUCO; }
						if( $o_UF == "PI"){ $o_UF_TXT = TXT_CLIENTES_CAD_PIAUI; }
						if( $o_UF == "PR"){ $o_UF_TXT = TXT_CLIENTES_CAD_PARANA; }
						if( $o_UF == "RJ"){ $o_UF_TXT = TXT_CLIENTES_CAD_RIO_DE_JANEIRO; }
						if( $o_UF == "RN"){ $o_UF_TXT = TXT_CLIENTES_CAD_RIO_GRANDE_DO_NORTE; }
						if( $o_UF == "RO"){ $o_UF_TXT = TXT_CLIENTES_CAD_RONDONIA; }
						if( $o_UF == "RR"){ $o_UF_TXT = TXT_CLIENTES_CAD_RORAIMA; }
						if( $o_UF == "RS"){ $o_UF_TXT = TXT_CLIENTES_CAD_RIO_GRANDE_DO_SUL; }
						if( $o_UF == "SC"){ $o_UF_TXT = TXT_CLIENTES_CAD_SANTA_CATARINA; }
						if( $o_UF == "SE"){ $o_UF_TXT = TXT_CLIENTES_CAD_SERGIPE; }
						if( $o_UF == "SP"){ $o_UF_TXT = TXT_CLIENTES_CAD_SAO_PAULO; }
						if( $o_UF == "TO"){ $o_UF_TXT = TXT_CLIENTES_CAD_TOCANTINS; }
						$o_CIDADE_TXT = $o_CIDADE;
						if( $o_SITUACAO == "A"){ $o_SITUACAO_TXT = "<div class=\"grid_color_green\">".TXT_CLIENTES_CAD_ATIVO."</div>"; }
						if( $o_SITUACAO == "I"){ $o_SITUACAO_TXT = "<div class=\"grid_color_red\">".TXT_CLIENTES_CAD_INATIVO."</div>"; }
						
						$TBODY_LIST .= '<tr>';
						$TBODY_LIST .= '<td class="text-center"><input id="chk_'.$C.'" data-id="'.$o_ID.'" type="checkbox" class="dt_checkbox" name="table_records"></td><td class="text-center">';
						
						# Verifica Nivel de Acesso
						if($ACESSO_MODULO->editar == 1)
						{
							$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_edit\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
						}
						else
						{
							$TBODY_LIST .= '<a role="button" style="color: #c8c8c8; cursor: not-allowed;"><i class="fa fa-pencil-square fa-lg"></i></a>&nbsp;&nbsp;';
						}
						
						$TBODY_LIST .= '<a onclick="javascript:$(this).trigger(\'f_show_ficha\',[\''.$o_ID.'\']);" role="button"><i class="fa fa-file-text fa-lg"></i></a></td>';
						$TBODY_LIST .= '<td>'.$o_NOME_INTERNO.'</td>';
						$TBODY_LIST .= '<td>'.$o_EMPRESA.'</td>';
						$TBODY_LIST .= '<td>'.$o_UF_TXT.'</td>';
						$TBODY_LIST .= '<td>'.$o_CIDADE_TXT.'</td>';
						$TBODY_LIST .= '<td>'.$o_SITUACAO_TXT.'</td>';
						$TBODY_LIST .= '</tr>';
						//
						$C++;
					}
					
				}
				
			}
			
			$o_TOTAL_REGS  = $C;
			
			## Define se pode inserir mais dados
			if(isEmpty($o_LIMITE_REGS)){ $o_LIMITE_REGS = 0; }
			if(isEmpty($o_TOTAL_REGS)){ $o_TOTAL_REGS = 0; }
			if($o_LIMITE_REGS == -1) { $IND_EXIBE_ADD_BTN = 1; } else
			{
				if($o_TOTAL_REGS >= $o_LIMITE_REGS){ $IND_EXIBE_ADD_BTN = 0; }
				else { $IND_EXIBE_ADD_BTN = 1; }
			}
			
			## Configura link dos botoes
			if($IND_EXIBE_ADD_BTN == 1)
			{
				//$add_btn_link = '<li><a href="#" role="button" aria-expanded="false" data-toggle="modal" data-target="#cad-modal"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a></li>';
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_register\',[\'\']);" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			else
			{
				$add_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_msgbox\',[\''.TXT_LIMITE_PLANO_ATINGIDO.'\',\' \',\'alert\']); return false;" role="button" aria-expanded="false"><i class="fa fa-lg fa-plus btn_color_blue2"></i></a>';
			}
			
			## Verifica Nivel de Acesso
			if($ACESSO_MODULO->editar == 0)
			{
				$add_btn_link = '<a href="#" role="button" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-lg fa-plus btn_color_disabled"></i></a>';
			}
			if($ACESSO_MODULO->apagar == 1)
			{
				$del_btn_link = '<a href="#" onclick="javascript:$(this).trigger(\'f_show_del\');" role="button" aria-expanded="false"><i class="fa fa-lg fa-trash btn_color_blue2"></i></a>';
			}
			else
			{
				$del_btn_link = '<a href="#" role="button" aria-expanded="false" style="cursor: not-allowed;"><i class="fa fa-lg fa-trash btn_color_disabled"></i></a>';
			}
			
		}# /carrega dados #
		
		#<li id="search_button_link"><a href="#" onclick="javascript:$(this).trigger('f_show_advsearch');" role="button" aria-expanded="false"><i class="fa fa-lg fa-search btn_color_blue2"></i></a></li>
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO; ?></a></li>
								<li class="active"><span><?php echo TXT_SYSTEM_SIDE_MENU_ADM_CLIENTE; ?></span></li>
							</ol></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li id="add_button_link"><?php echo $add_btn_link; ?></li>
							<li id="del_button_link"><?php echo $del_btn_link; ?></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content"><input id='datatable_total_recs' value="<?php echo $o_TOTAL_REGS; ?>" type=hidden>
						<table id="main-datatable" class="table table-striped table-bordered bulk_action" style="max-width: 800px">
							<thead>
								<tr>
									<th class="text-center"><input type="checkbox" id="select-all" name="select_all" class="dt_checkbox"></th>
									<th class="text-center"><i class="fa fa-chevron-down"></i></th>
									<th><?php echo TXT_ADM_CLIENTE_NOME_INTERNO; ?></th>
									<th><?php echo TXT_ADM_CLIENTE_EMPRESA; ?></th>
									<th><?php echo TXT_ADM_CLIENTE_ESTADO; ?></th>
									<th><?php echo TXT_ADM_CLIENTE_CIDADE; ?></th>
									<th><?php echo TXT_ADM_CLIENTE_SITUACAO; ?></th>
								</tr>
							</thead>
							
							<tbody id="datatable_tbody">
								<?php echo $TBODY_LIST; ?>
							</tbody>
							
						</table>
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->


<!-- BEGIN # MODAL CAD -->
<div class="modal fade" id="cad-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" data-backdrop="static">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header" align="center">
				<span style="line-height: 10px; font-size: 10px;"><i class="fa fa-thumbs-o-up"></i>&nbsp;<?php echo TXT_TECLE_ESC_PARA_FECHAR; ?></span>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
				</button>
			</div>
			
			<!-- Begin # DIV Form -->
			<div id="div-forms">
				<!-- Begin | Form -->
				<form id="reg-form">
					<input id="reg_id" type="hidden">
					<div class="modal-body">
						<div id="div-reg-msg">
							<div id="div-label-type"><?php echo TXT_CAD_MODAL_NOVO_REGISTRO; ?></div>
							<div id="icon-reg-msg" class="glyphicon glyphicon-chevron-right"></div>
							<span id="text-reg-msg"><?php echo TXT_CAD_MODAL_CAMPOS_COM_A_SAO_OBRIGATORIOS; ?></span>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_NOME_INTERNO; ?>:</b>
							</span>
							<input id="reg_nome_interno" type="text" maxlength="30" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_EMPRESA; ?>:</b>
							</span>
							<input id="reg_empresa" type="text" maxlength="180" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_CNPJ; ?>:</b>
							</span>
							<input id="reg_cnpj" type="text" maxlength="14" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_FONE_1; ?>:</b>
							</span>
							<input id="reg_fone1" type="text" maxlength="15" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_FONE_2; ?>:</b>
							</span>
							<input id="reg_fone2" type="text" maxlength="15" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_E_MAIL; ?>:</b>
							</span>
							<input id="reg_email" type="text" maxlength="255" class="form-control email input-lower" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_CEP; ?>:</b>
							</span>
							<input id="reg_cep" type="text" maxlength="8" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;" required>
							<input id="reg_cep_uf" type="hidden">
							<input id="reg_cep_cidade_codigo" type="hidden">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_ESTADO; ?>:</b>
							</span>
							<select id="reg_uf" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
								<option value=''></option><option value='AC'><?php echo TXT_ADM_CLIENTE_ACRE; ?></option><option value='AL'><?php echo TXT_ADM_CLIENTE_ALAGOAS; ?></option><option value='AM'><?php echo TXT_ADM_CLIENTE_AMAZONAS; ?></option><option value='AP'><?php echo TXT_ADM_CLIENTE_AMAPA; ?></option><option value='BA'><?php echo TXT_ADM_CLIENTE_BAHIA; ?></option><option value='CE'><?php echo TXT_ADM_CLIENTE_CEARA; ?></option><option value='DF'><?php echo TXT_ADM_CLIENTE_DISTRITO_FEDERAL; ?></option><option value='ES'><?php echo TXT_ADM_CLIENTE_ESPIRITO_SANTO; ?></option><option value='GO'><?php echo TXT_ADM_CLIENTE_GOIAS; ?></option><option value='MA'><?php echo TXT_ADM_CLIENTE_MARANHAO; ?></option><option value='MG'><?php echo TXT_ADM_CLIENTE_MINAS_GERAIS; ?></option><option value='MS'><?php echo TXT_ADM_CLIENTE_MATO_GROSSO_DO_SUL; ?></option><option value='MT'><?php echo TXT_ADM_CLIENTE_MATO_GROSSO; ?></option><option value='PA'><?php echo TXT_ADM_CLIENTE_PARA; ?></option><option value='PB'><?php echo TXT_ADM_CLIENTE_PARAIBA; ?></option><option value='PE'><?php echo TXT_ADM_CLIENTE_PERNAMBUCO; ?></option><option value='PI'><?php echo TXT_ADM_CLIENTE_PIAUI; ?></option><option value='PR'><?php echo TXT_ADM_CLIENTE_PARANA; ?></option><option value='RJ'><?php echo TXT_ADM_CLIENTE_RIO_DE_JANEIRO; ?></option><option value='RN'><?php echo TXT_ADM_CLIENTE_RIO_GRANDE_DO_NORTE; ?></option><option value='RO'><?php echo TXT_ADM_CLIENTE_RONDONIA; ?></option><option value='RR'><?php echo TXT_ADM_CLIENTE_RORAIMA; ?></option><option value='RS'><?php echo TXT_ADM_CLIENTE_RIO_GRANDE_DO_SUL; ?></option><option value='SC'><?php echo TXT_ADM_CLIENTE_SANTA_CATARINA; ?></option><option value='SE'><?php echo TXT_ADM_CLIENTE_SERGIPE; ?></option><option value='SP'><?php echo TXT_ADM_CLIENTE_SAO_PAULO; ?></option><option value='TO'><?php echo TXT_ADM_CLIENTE_TOCANTINS; ?></option>
							</select>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_CIDADE; ?>:</b>
							</span>
							<select id="reg_cidade" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
								<option value=''>(<?php echo TXT_ADM_CLIENTE_ESCOLHA_UMA_CIDADE_PRIMEIRO; ?>)</option>
							</select>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_BAIRRO; ?>:</b>
							</span>
							<input id="reg_bairro" type="text" maxlength="45" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_ENDERECO; ?>:</b>
							</span>
							<input id="reg_end" type="text" maxlength="45" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_NUMERO; ?>:</b>
							</span>
							<input id="reg_numero" type="text" maxlength="15" class="form-control textonly" style="margin-top: 0px; margin-bottom: 0px;" required>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_COMPLEMENTO; ?>:</b>
							</span>
							<input id="reg_compl" type="text" maxlength="45" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_OBSERVACAO; ?>:</b>
							</span>
							<textarea id="reg_obs" rows="3" cols="35" maxlength="255" class="form-control obstext" style="margin-top: 0px; margin-bottom: 0px;"></textarea>
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_CONTATO_NOME; ?>:</b>
							</span>
							<input id="reg_contato_nome" type="text" maxlength="60" class="form-control textonly input-upper" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_CONTATO_FONE; ?>:</b>
							</span>
							<input id="reg_contato_fone1" type="text" maxlength="15" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_CONTATO_CELULAR; ?>:</b>
							</span>
							<input id="reg_contato_cel1" type="text" maxlength="15" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><?php echo TXT_ADM_CLIENTE_CONTATO_WHATSAPP; ?>:</b>
							</span>
							<input id="reg_contato_whatsapp" type="text" maxlength="15" class="form-control numeric" style="margin-top: 0px; margin-bottom: 0px;">
						</div>
						<div class="input-group">
							<span class="input-group-addon">
								<b><span style="color:#ff0000;">*</span>&nbsp;<?php echo TXT_ADM_CLIENTE_SITUACAO; ?>:</b>
							</span>
							<select id="reg_situacao" class="form-control" style="margin-top: 0px; margin-bottom: 0px;" required>
								<option value='A'><?php echo TXT_ADM_CLIENTE_ATIVO; ?></option><option value='I'><?php echo TXT_ADM_CLIENTE_INATIVO; ?></option>
							</select>
						</div>
					</div>
					<div class="modal-footer">
						<div>
							<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_REGISTER; ?></button>
							<button id="bt_save" type="button" class="btn btn-yellow btn-lg btn-block"><?php echo TXT_BT_SAVE; ?></button>
						</div>
					</div>
				</form>
				<!-- End | Form -->
				
			</div>
			<!-- End # DIV Form -->
			
		</div>
	</div>
</div>
<!-- END # MODAL CAD -->


<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<!-- iCheck -->
		<script src="vendors/iCheck/icheck.min.js"></script>
		<!-- Datatables -->
		<script src="vendors/datatables.net/js/jquery.dataTables.min.js"></script>
		<script src="vendors/datatables.net/js/dataTables.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/dataTables.buttons.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.colVis.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.flash.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.html5.min.js"></script>
		<script src="vendors/datatables.net/extensions/Buttons/js/buttons.print.min.js"></script>
		
		<script src="vendors/datatables.net/extensions/FixedHeader/js/dataTables.fixedHeader.min.js"></script>
		<script src="vendors/datatables.net/extensions/FixedColumns/js/dataTables.fixedColumns.min.js"></script>
		<script src="vendors/datatables.net/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/dataTables.responsive.min.js"></script>
		<script src="vendors/datatables.net/extensions/Responsive/js/responsive.bootstrap.min.js"></script>
		<script src="vendors/datatables.net/extensions/Scroller/js/dataTables.scroller.min.js"></script>
		<!-- <script src="vendors/datatables.net/extensions/Select/js/dataTables.select.min.js"></script> -->
		
		<script src="vendors/jszip/dist/jszip.min.js"></script>
		<script src="vendors/pdfmake/build/pdfmake.min.js"></script>
		<script src="vendors/pdfmake/build/vfs_fonts.js"></script>
		
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/view-clientes-cad.js" charset="UTF-8"></script>
		

<?php
	} 
?>
