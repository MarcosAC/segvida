<?php
#################################################################################
## SEGVIDA - Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: do_send_contact.php
## Função..........: 1. Envia email de contato do site www.rlgomide.com para o email de contato
##                   2. Mostra mensagem de ok ou erro
##                   3. Envia um email de confirmacao ao email do contato
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	## System configuration
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	include_once 'PHPMailer/PHPMailerAutoload.php';
	
	## Carrega inputs iniciais
	## Carrega inputs
	$MY_MSG = $MY_REG_OK = $GO = "";
	
	#define variables and set to empty values
	$IN_NAME = $IN_EMAIL = $IN_SUBJECT = $IN_MSG = "";
	
	#Valida metodo de solicitacao
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_NAME    = test_input($_POST["name"]);
		$IN_EMAIL   = test_input($_POST["email"]);
		$IN_SUBJECT = test_input($_POST["subject"]);
		$IN_MSG     = test_input($_POST["msg"]);
		
		#$IN_NAME    = "Miguel Leite Gomide";
		#$IN_EMAIL   = "miguel@gmail.com";
		#$IN_SUBJECT = "Teste";
		#$IN_MSG     = "Teste de envio de email dos caçadores!";
		
		## Define idioma
		if(empty($IN_LANG)) { $IN_LANG = "BR"; }
		
		## Carrega Idioma
		carrega_language($IN_LANG);
		
		## Autentica dados obrigatorios
		if( !empty($IN_NAME) &&
				!empty($IN_EMAIL) && 
				!empty($IN_SUBJECT) && 
				!empty($IN_MSG) 
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			#($IN_NAME)($IN_EMAIL)($IN_SUBJECT)($IN_MSG)
			$MY_MSG = "0|".TXT_ERRO_DADOS_OBR."|alert|";
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Formata Mensagem ao contato
			
			##Formata Mensagem
			$MSG_TXT = $IN_MSG;
			str_replace("\n", "<br/>", $MSG_TXT);
			
			## Create a new PHPMailer instance
			$mail = new PHPMailer(true);
			
			##Formata array com os dados
			$template_map = array(
				'{{IMG_URL}}' => IMG_URL,
				'{{DOMAIN}}'  => DOMAIN,
				'{{NAME}}'    => $IN_NAME,
				'{{EMAIL}}'   => $IN_EMAIL,
				'{{SUBJECT}}' => $IN_SUBJECT,
				'{{MSG}}'     => $IN_MSG
			);
			
			##Carrega texto do email via template ja processado
			$body = get_template_text(EMAIL_TEMPLATE_PATH, EMAIL_TEMPLATE_CONTATO_FORM,$template_map,"pt-br");
			
			//echo "body:\n\n//--START".$body."\n//--END";
			
			$isMailSent = 0;
			
			try 
			{
				//if($cfg->SMTP_AUTH)
				//{
				//	$mail->IsSMTP();
				//	$mail->SMTPSecure = "ssl";
				//	$mail->SMTPDebug = 0;
				//	$mail->Port      = $cfg->SMTP_PORT; //Indica a porta de conexão para a saída de e-mails
				//	$mail->Host      = $cfg->SMTP_HOST;//Endereço do Host do SMTP Locaweb
				//	$mail->SMTPAuth  = $cfg->SMTP_AUTH; //define se haverá ou não autenticação no SMTP
				//	$mail->Username  = $cfg->SMTP_USER; //Login de autenticação do SMTP
				//	$mail->Password  = $cfg->SMTP_PASS; //Senha de autenticação do SMTP
				//}
				$mail->AddReplyTo(EMAIL_SISTEMA_NOREPLY, EMAIL_SISTEMA_NOREPLY_NOME);//Set an alternative reply-to address
				$mail->AddAddress(EMAIL_CONTATO);//Set who the message is to be sent to
				
				if(!empty(EMAIL_CONTATO_BCC))
				{
					$mail->AddBCC(EMAIL_CONTATO_BCC);//Set who the message is to be sent to
				}
				
				$mail->SetFrom(EMAIL_SISTEMA, EMAIL_SISTEMA_NOME);//Set who the message is to be sent from
				
				$mail->Subject = TXT_EMAIL_SUBJECT_CONTATO_FORM;//Set the subject line
				
				//$mail->AltBody = 'To view the message, please use an HTML compatible email viewer!'; // optional - MsgHTML will create an alternate automatically
				//$mail->MsgHTML(file_get_contents('contents.html'));
				$mail->MsgHTML($body);
				//$mail->AddAttachment('images/phpmailer.gif');      // attachment
				//$mail->AddAttachment('images/phpmailer_mini.gif'); // attachment
				//if($mail->Send();
				if ($mail->send()) { $isMailSent = 1; }
			} 
			catch (phpmailerException $e) 
			{
				$MailError = $e->errorMessage();//Pretty error messages from PHPMailer //echo "\nerrorMessage => ".$e->errorMessage();
			} 
			catch (Exception $e) 
			{
				$MailException = $e->getMessage();//Boring error messages from anything else! //echo "\Exception => ".$e->getMessage();
			}
			
			//send the message, check for errors
			if (!$isMailSent) { $MY_MSG = "0|".TXT_MSG_CONTATO_NAO_ENVIADO."|alert|"; }
			             else { $MY_MSG = "1|".TXT_MSG_CONTATO_ENVIADO."|success|"; }
			
		}#endifgo
		
		//echo "Content-type:text/html; charset=utf-8\n\n";
		echo $MY_MSG;
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		echo "0|O.O?|error|";
	}
	
#################################################################################
###########
#####
##
?>
