<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: language.php
##  Funcao: IDIOMA - PT-BR - Textos em portugues do Brasil
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
#################################################################################

#################################################################################
## Definicoes
#################################################################################

####
# index.php
####
define("TXT_PG_MN_INICIO"              ,"Início");
define("TXT_PG_MN_ACESSO"              ,"Acesso");
define("TXT_PG_MN_PLANOS"              ,"Planos");
define("TXT_PG_MN_COLABS"              ,"Colaboradores");
define("TXT_PG_MN_USERS"               ,"Usuários");
define("TXT_PG_MN_SOBRE"               ,"Sobre");
define("TXT_PG_MN_CONTATO"             ,"Contato");

####
# usuarios.php
####
define("TXT_PG_USUARIOS_BT_CRIAR_CONTA"   ,"Criar Minha Conta de Usuário");


####
# Geral - Paginas
####
define("TXT_PAGE_TITLE_PREFIX"       ,"SEGVIDA");
define("TXT_PAGE_TITLE_SUFIX"        ,"Consultoria Especializada em Segurança do Trabalho");
define("TXT_PAGE_TITLE"              ,"SEGVIDA - Consultoria Especializada em Segurança do Trabalho");
define("TXT_META_DESCRIPTION"        ,"SEGVIDA - Consultoria Especializada em Segurança do Trabalho");
define("TXT_META_AUTHOR"             ,"Rodrigo Leite Gomide");
define("TXT_META_KEYWORDS"           ,"Consultoria, Especializada, Segurança, Trabalho, Eficiência");
define("TXT_META_SUBJECT"            ,"SEGVIDA - Consultoria Especializada em Segurança do Trabalho");
define("TXT_META_COPYRIGHT"          ,"2017 - Ther Sistemas Inovadores Ltda");
define("TXT_META_ABSTRACT"           ,"SEGVIDA - Consultoria Especializada em Segurança do Trabalho");
define("TXT_META_LANGUAGE"           ,"pt-br");
define("TXT_TERMOS_DE_USO"           ,"Termos de Uso");
define("TXT_POLITICA_DE_PRIVACIDADE" ,"Política de Privacidade");
define("TXT_MODAL_BTN_FECHAR"        ,"Fechar");
define("TXT_MODAL_MSG_CARREGANDO"    ,"Carregando...");


####
# Geral - Sistema
####
define("TXT_FALHA_TRANSF_DADOS"           ,"Falha na transferência dos dados. Favor tentar novamente!");
define("TXT_NAO_POSSIVEL_CONNECTAR"       ,"Não foi possível conectar:");
define("TXT_SEM_ACESSO_AO_DB"             ,"Sem acesso ao DB:");
define("TXT_QUERY_INVALIDA"               ,"Query inválida:");
define("TXT_INCAPAZ_DE_PREPARAR_SQL_STMT" ,"Incapaz de preparar instrução SQL!");
define("TXT_ERRO_SOLICITACAO"             ,"Ocorreu um erro ao processar a solicitação. Favor tentar novamente!");
define("TXT_ERRO_DADOS_OBR"               ,"Dados obrigatórios não foram transferidos. Favor tentar novamente!");
define("TXT_USERNAME_LIBERADO"            ,"Usuário liberado!");
define("TXT_USERNAME_JA_EM_USO"           ,"Usuário já em uso!");
define("TXT_USUARIO_NAO_LOGADO"           ,"Usuário não logado!");
define("TXT_FAVOR_EFETUAR_NOVO_LOGIN"     ,"Favor efetuar novo login e tentar novamente!");
define("TXT_SYS_NO_SESSION"               ,"Login expirado. Favor fazer novo login!");
define("TXT_CORREIO_SERVICO_OFFLINE"      ,"Incapaz de comunicar com o computador dos correios. Serviço temporariamente indisponível!");
define("TXT_TECLE_ESC_PARA_FECHAR"        ,"TECLE ESC PARA FECHAR");



####
# Plano
####
define("TXT_PLANO_TIPO_M"                   ,"Mês");
define("TXT_PLANO_TIPO_A"                   ,"Ano");
define("TXT_MENSAL"                         ,"Mensal");
define("TXT_ANUAL"                          ,"Anual");
define("TXT_PLANO_INEXISTENTE"              ,"Plano inexistente!");
define("TXT_MSG_CONTRATA_PLANO_SELECIONADO" ,"Plano selecionado para contratação");
define("TXT_PAGTO_TOKEN_INEXISTENTE"        ,"Token Inexistente!");
define("TXT_INCAPAZ_CARREGAR_DADOS_PLANO"   ,"Incapaz de carregar dados do plano contratado!");
//define("TXT_LIMITE_PLANO_ATINGIDO_MUDAS"    ,"Atingindo limite de variedades de mudas do plano contratado! Mude para um novo plano que melhor atenda à sua necessidade atual ou entre em contato com o suporte financeiro (financeiro@prodfy.com.br) para elaboração de um Plano Personalizado sob medida.");
//define("TXT_LIMITE_PLANO_ATINGIDO_COLABS"   ,"Atingindo limite de colaboradores do plano contratado! Mude para um novo plano que melhor atenda à sua necessidade atual ou entre em contato com o suporte financeiro (financeiro@prodfy.com.br) para elaboração de um Plano Personalizado sob medida.");
//define("TXT_LIMITE_PLANO_ATINGIDO_DISPS"    ,"Atingindo limite de dispositivos do plano contratado! Mude para um novo plano que melhor atenda à sua necessidade atual ou entre em contato com o suporte financeiro (financeiro@prodfy.com.br) para elaboração de um Plano Personalizado sob medida.");
define("TXT_LIMITE_PLANO_ATINGIDO"          ,"Atingindo limite de registros do plano contratado! Mude para um novo plano que melhor atenda à sua necessidade atual ou entre em contato com o suporte financeiro (contato@ther.com.br) para elaboração de um Plano Personalizado sob medida.");

####
# Boleto
####
define("TXT_BOLETO_SOLICITACAO_INVALIDA"     ,"Solicitação Inválida!");
define("TXT_BOLETO_TOKEN_INEXISTENTE"        ,"Token Inexistente!");
define("TXT_BOLETO_TOKEN_INVALIDO"           ,"Token Inválido!");
define("TXT_BOLETO_COBR_INVALIDA"            ,"Tipo de Cobrança Inválida!");


####
# Templates de emails para envio
####
define("EMAIL_TEMPLATE_CONTATO_FORM"          ,"contato_via_website");
define("EMAIL_TEMPLATE_REGISTRO"              ,"registro_cliente");
define("EMAIL_TEMPLATE_CONF_REGISTRO"         ,"confirmacao_registro_cliente");
define("EMAIL_TEMPLATE_REGISTRO_USER"         ,"registro_usuario");
define("EMAIL_TEMPLATE_CONF_REGISTRO_USER"    ,"confirmacao_registro_usuario");
define("EMAIL_TEMPLATE_AVISO_ADMIN_REGISTRO"  ,"aviso_admin_registro_cliente");
define("EMAIL_TEMPLATE_NOVA_SENHA"            ,"esqueci_senha_passo1");
define("EMAIL_TEMPLATE_NOVA_SENHA_CONCLUIDO"  ,"esqueci_senha_concluido");
define("EMAIL_TEMPLATE_CONF_PAGTO"            ,"confirmacao_pagto_cliente");

####
# Contato via Form
####
define("TXT_EMAIL_SUBJECT_CONTATO_FORM" ,"SEGVIDA - Contato Via Website");
define("TXT_MSG_CONTATO_NAO_ENVIADO"    ,"E-Mail não pode ser enviado!");
define("TXT_MSG_CONTATO_ENVIADO"        ,"Sua mensagem foi enviada com sucesso. Obrigado!");

####
# Processamento Auxiliar - Gera Options Automation, etc
####
//define("TXT_GERA_OPTIONS_NENHUM_CENTRO_DE_CUSTO_SECUNDARIO_ENCONTRADO_PARA_O_CENTRO_DE_CUSTO_SELECIONADO" ,"Nenhum Centro de Custo Secundário encontrado para o Centro de Custo Primário selecionado!");
//define("TXT_GERA_CALC_FECH_MENSAL_NENHUM_VALOR_ENCONTRADO_PARA_O_PERIODO_INDICADO" ,"Nenhum Valor encontrado para o período indicado!");
//define("TXT_GERA_OPTIONS_NENHUM_CONDOMINIO_ENCONTRADO_PARA_O_BANCO_SELECIONADO" ,"Nenhum Condomínio encontrado para o Banco selecionado!");


####
# Registro
####
define("TXT_EMAIL_SUBJECT_REGISTRO"          ,"SEGVIDA - Confirmação de Registro");
define("TXT_MSG_REGISTRO_EFETIVADO"          ,"Registro efetivado com sucesso! Uma mensagem acaba de ser enviada ao e-mail informado. Ative sua conta clicando no link de confirmação.");
define("TXT_EMAIL_SUBJECT_CONFIRMACAO_PAGTO" ,"SEGVIDA - Confirmação de Pagamento");

####
# Registro - Cadastro
####
define("TXT_SYSTEM_CAD_PERFIL"                 ,"Perfil");
define("TXT_SYSTEM_CAD_AJUDA"                  ,"Ajuda");
define("TXT_SYSTEM_CAD_CADASTRO"               ,"Cadastro");
define("TXT_SYSTEM_CAD_NOME"                   ,"Nome");
define("TXT_SYSTEM_CAD_SOBRENOME"              ,"Sobrenome");
define("TXT_SYSTEM_CAD_EMAIL"                  ,"E-Mail");
define("TXT_SYSTEM_CAD_CPF"                    ,"CPF");
define("TXT_SYSTEM_CAD_CNPJ"                   ,"CNPJ");
define("TXT_SYSTEM_CAD_RAZAO_SOCIAL"           ,"Razão Social");
define("TXT_SYSTEM_CAD_FONE_FIXO"              ,"Telefone Fixo");
define("TXT_SYSTEM_CAD_CELULAR"                ,"Celular");
define("TXT_SYSTEM_CAD_PAIS"                   ,"País");
define("TXT_SYSTEM_CAD_IDIOMA"                 ,"Idioma");
define("TXT_SYSTEM_CAD_BRASIL"                 ,"Brasil");
define("TXT_SYSTEM_CAD_PORTUGUES_BRASIL"       ,"Português Brasil");
define("TXT_SYSTEM_CAD_US_ENGLISH"             ,"US English");
define("TXT_SYSTEM_CAD_ESPANHOL"               ,"Español");
define("TXT_SYSTEM_CAD_CEP"                    ,"CEP");
define("TXT_SYSTEM_CAD_CEP_MSG"                ,"Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.");
define("TXT_SYSTEM_CAD_UF"                     ,"Estado");
define("TXT_SYSTEM_CAD_CIDADE"                 ,"Cidade");
define("TXT_SYSTEM_CAD_BAIRRO"                 ,"Bairro");
define("TXT_SYSTEM_CAD_NUMERO"                 ,"Número");
define("TXT_SYSTEM_CAD_COMPL"                  ,"Complemento");
define("TXT_SYSTEM_CAD_USERNAME"               ,"Usuário");
define("TXT_SYSTEM_CAD_PASSWORD"               ,"Senha");
define("TXT_SYSTEM_CAD_PASSWORD2"              ,"Conf. de Senha");
define("TXT_SYSTEM_CAD_PLANO"                  ,"Plano de Pagto");
define("TXT_SYSTEM_CAD_CAPTCHA"                ,"Autenticação");
define("TXT_SYSTEM_CAD_TERMOS"                 ,"Termos");
define("TXT_SYSTEM_CAD_REGISTRAR"              ,"Registrar");

####
# Login
####
define("TXT_SYSTEM_LOGIN_USUARIO"              ,"Usuário");
define("TXT_SYSTEM_LOGIN_SENHA"                ,"Senha");
define("TXT_SYSTEM_LOGIN_EMAIL"                ,"E-Mail");
define("TXT_SYSTEM_LOGIN_LEMBRAR"              ,"Lembrar");
define("TXT_SYSTEM_LOGIN_DIGITE_O_CODIGO"      ,"Digite o Código");
define("TXT_SYSTEM_LOGIN_ESQUECEU"             ,"Esqueceu a Senha?");
define("TXT_SYSTEM_LOGIN_ENTRAR"               ,"Entrar");
define("TXT_SYSTEM_LOGIN_ENVIAR"               ,"Enviar");
define("TXT_SYSTEM_LOGIN_INSTRUC1"             ,"Entre com seus dados de acesso e clique em ENTRAR.");
define("TXT_SYSTEM_LOGIN_INSTRUC2"             ,"Digite seu username e e-mail.");
define("TXT_SYSTEM_LOGIN_RECUPERA_SENHA"       ,"RECUPERA SENHA");



####
# Confirma Registro
####
define("TXT_TOKEN_INEXISTENTE"                ,"Token de Confirmação Inexistente!");
define("TXT_EMAIL_SUBJECT_CONTA_ATIVADA"      ,"SEGVIDA - Conta Ativada e Pronta Para Uso");
define("TXT_EMAIL_SUBJECT_CONTA_REGISTRADA"   ,"SEGVIDA - Conta Registrada e Pronta Para Uso");
define("TXT_MSG_CONTA_ATIVADA"                ,"Conta Ativada Com Sucesso!");
define("TXT_MSG_CONTA_REGISTRADA"             ,"Conta Registrada Com Sucesso!");
define("TXT_CONF_REGISTRO_ACESSAR"            ,"Acessar");

####
# Redefinicao de Senha
####
define("TXT_EMAIL_SUBJECT_REDEFINICAO_SENHA"      ,"SEGVIDA - Redefinição de Senha de Acesso");
define("TXT_MSG_REDEFINICAO_SENHA_INSTRUCOES"     ,"Uma mensagem acaba de ser enviada para o seu e-mail com as instruções para redefinição de sua senha de acesso.");
define("LB_RESETPWD_SENHA_REDEFINIDA_COM_SUCESSO" ,"Senha Redefinida Com Sucesso!");
define("LB_RESETPWD_INSTRUCAO_INICIAL"            ,"Para definir uma nova senha de acesso, preencha o formulário abaixo e clique em AVANÇAR:");
define("LB_RESETPWD_USERNAME"                     ,"USERNAME:");
define("LB_RESETPWD_EMAIL"                        ,"E-MAIL:");
define("LB_RESETPWD_SENHA"                        ,"*Senha");
define("LB_RESETPWD_SENHA_CONFIRMACAO"            ,"*Senha para confirmação");
define("LB_RESETPWD_CAPTCHA"                      ,"*Digite o Código");
define("LB_RESETPWD_BTN_AVANCAR"                  ,"Avançar");
define("TXT_EMAIL_SUBJECT_SENHA_ALTERADA"         ,"SEGVIDA - Senha Alterada Com Sucesso");
define("TXT_RESETPWD_TOKEN_EXPIRADO"              ,"A validade de sua solicitação de alteração de senha expirou. Favor gerar nova solitação e tentar novamente!");
//The validity of your password change request has expired. Please generate a new request and try again!
//La validez de una solicitud de cambio de contraseña ha caducado. Por favor, generar nuevos solitação y vuelve a intentarlo!
define("TXT_RESETPWD_SENHA_ALTERADA_COM_SUCESSO"  ,"Senha alterada com sucesso!");
//Password changed successfully!
//Contraseña cambiada correctamente!


####
# Confirma Pagamento
####
define("TXT_CONF_PAGTO_TOKEN_INEXISTENTE"     ,"Token Inexistente!");
define("TXT_CONF_PAGTO_MSG_PLANO_ATIVADO"     ,"Plano Contratado ativado com sucesso!");


####
# Login
####
define("TXT_USERNAME_SENHA_INVALIDOS"       ,"Username e/ou senha inválidos. Favor tentar novamente!");
define("TXT_USERNAME_INVALIDO"              ,"Username inválido. Favor tentar novamente!");
define("TXT_PASSWORD_INVALIDO"              ,"Senha inválida. Favor tentar novamente!");
define("TXT_LOGIN_BLOQUEADO"                ,"Usuário bloqueado devido a várias tentativas falhas de login! Redefina a sua senha de acesso para desbloquear seu usuário.");
define("TXT_LOGIN_OK"                       ,"Login autenticado com sucesso!");
define("TXT_INCAPAZ_DE_GERAR_SESSAO"        ,"Erro ao tentar gerar sessão segura. Favor tentar novamente.");
define("TXT_LOGIN_REENVIO_LINK_CONFIRMACAO" ,"Sua conta ainda não foi ativada! Uma mensagem acaba de ser enviada ao e-mail cadastrado. Ative sua conta clicando no link de confirmação.");
define("TXT_USUARIO_NAO_AUTENTICADO"        ,"Usuário não autenticado. Favor efetuar novo login!");
define("TXT_ACESSO_COLABORADOR_INATIVO"     ,"Acesso do colaborador está inativo. Favor contactar o administrador do sistema!");


####
# Portal - Logado
####
define("TXT_USER_CATEG_ADMIN"       ,"ADMINISTRADOR");
define("TXT_USER_CATEG_COLAB"       ,"COLABORADOR");
define("TXT_USER_CATEG_USU"         ,"USUÁRIO");

####
# Menu SYSTEM
####
define("TXT_SYSTEM_SIDE_MENU_PAINEL_DE_CONTROLE"           ,"Painel de Controle");
define("TXT_SYSTEM_SIDE_MENU_PAINEL_DE_CONTROLE_DASHBOARD" ,"Dashboard");
//define("TXT_SYSTEM_SIDE_MENU_PROD"                         ,"Produção");
//define("TXT_SYSTEM_SIDE_MENU_PROD_VISAO_GERAL"             ,"Visão Geral");
//define("TXT_SYSTEM_SIDE_MENU_PROD_HISTORICO"               ,"Histórico");
//define("TXT_SYSTEM_SIDE_MENU_PROD_INVENTARIO"              ,"Inventário");
//define("TXT_SYSTEM_SIDE_MENU_PROD_LOTE"                    ,"Lote");
//define("TXT_SYSTEM_SIDE_MENU_PROD_OBJETIVO"                ,"Objetivo");
//define("TXT_SYSTEM_SIDE_MENU_PROD_PONTOS_DE_CONTROLE"      ,"Pontos de Controle");
//define("TXT_SYSTEM_SIDE_MENU_PROD_ESTAGIO"                 ,"Estágio");
//define("TXT_SYSTEM_SIDE_MENU_MUDAS"                        ,"Mudas");
//define("TXT_SYSTEM_SIDE_MENU_MUDAS_MUDA"                   ,"Muda");
//define("TXT_SYSTEM_SIDE_MENU_MUDAS_DOENCA"                 ,"Doença");
//define("TXT_SYSTEM_SIDE_MENU_MUDAS_ESPECIE"                ,"Espécie");
define("TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO"               ,"Administrativo");
//define("TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO_CLIENTE"       ,"Cliente");
//define("TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO_CONTRATO"      ,"Contrato");
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS"                   ,"Relatórios");
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS_PRODUCAO"          ,"Produção");
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS_MUDAS"             ,"Mudas");
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS_CLIENTES"          ,"Clientes");
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS_COLABORADORES"     ,"Colaboradores");
define("TXT_SYSTEM_SIDE_MENU_CFG"                          ,"Configurações");
define("TXT_SYSTEM_SIDE_MENU_CFG_COLABORADORES"            ,"Colaboradores");
define("TXT_SYSTEM_SIDE_MENU_CFG_DISPOSITIVOS"             ,"Dispositivos");
define("TXT_SYSTEM_SIDE_MENU_CFG_SINCRONIA"                ,"Sincronia");
define("TXT_SYSTEM_SIDE_MENU_CFG_PLANO_CONTRATADO"         ,"Plano Contratado");
define("TXT_SYSTEM_PROFILE_MENU_CAD"                       ,"Cadastro");
define("TXT_SYSTEM_PROFILE_MENU_CFG"                       ,"Configurações");
define("TXT_SYSTEM_PROFILE_MENU_AJUDA"                     ,"Ajuda");
define("TXT_SYSTEM_PROFILE_MENU_LOGOUT"                    ,"Sair");


####
# Modulo Mensagens
####
define("TXT_MOD_MSGS_VER_TODAS_AS_MSGS"                ,"Ver Todas As Mensagens");
define("TXT_MOD_MSGS_NENHUMA_MSG_ENCONTRADA"           ,"Nenhuma Mensagem Encontrada");
define("TXT_MOD_MSGS_ENVIAR_MSG"                       ,"Enviar Mensagem");
define("TXT_MOD_MSGS_APAGAR_MSG"                       ,"Apagar Mensagem");
define("TXT_MOD_MSGS_REPLY"                            ,"Responder");
define("TXT_MOD_MSGS_REPLY_TO_ALL"                     ,"Responder a Todos");
define("TXT_MOD_MSGS_ADD_ANEXO"                        ,"Incuir Anexo");
define("TXT_MOD_MSGS_NENHUM_COLABORADOR_CONFIGURADO"   ,"Nenhum colaborador configurado!");

####
# Modulo Perfil
####
define("TXT_MOD_PERFIL_PERFIL"                 ,"Perfil");
define("TXT_MOD_PERFIL_AJUDA"                  ,"Ajuda");
define("TXT_MOD_PERFIL_CADASTRO"               ,"Cadastro");
define("TXT_MOD_PERFIL_NOME"                   ,"Nome");
define("TXT_MOD_PERFIL_SOBRENOME"              ,"Sobrenome");
define("TXT_MOD_PERFIL_EMAIL"                  ,"E-Mail");
define("TXT_MOD_PERFIL_CPF"                    ,"CPF");
define("TXT_MOD_PERFIL_CNPJ"                   ,"CNPJ");
define("TXT_MOD_PERFIL_RAZAO_SOCIAL"           ,"Razão Social");
define("TXT_MOD_PERFIL_FONE_FIXO"              ,"Telefone Fixo");
define("TXT_MOD_PERFIL_CELULAR"                ,"Celular");
define("TXT_MOD_PERFIL_PAIS"                   ,"País");
define("TXT_MOD_PERFIL_IDIOMA"                 ,"Idioma");
define("TXT_MOD_PERFIL_BRASIL"                 ,"Brasil");
define("TXT_MOD_PERFIL_PORTUGUES_BRASIL"       ,"Português Brasil");
define("TXT_MOD_PERFIL_US_ENGLISH"             ,"US English");
define("TXT_MOD_PERFIL_ESPANHOL"               ,"Español");
define("TXT_MOD_PERFIL_CEP"                    ,"CEP");
define("TXT_MOD_PERFIL_CEP_MSG"                ,"Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.");
define("TXT_MOD_PERFIL_UF"                     ,"Estado");
define("TXT_MOD_PERFIL_CIDADE"                 ,"Cidade");
define("TXT_MOD_PERFIL_BAIRRO"                 ,"Bairro");
define("TXT_MOD_PERFIL_NUMERO"                 ,"Número");
define("TXT_MOD_PERFIL_COMPL"                  ,"Complemento");
define("TXT_MOD_PERFIL_ALTERAR_FOTO"           ,"Alterar Foto");
define("TXT_MOD_PERFIL_IMG_MSG"                ,"Imagem de 128x128 pixels, JPG, JPEG, PNG ou GIF, até 80kb");
define("TXT_MOD_PERFIL_ARQUIVO"                ,"Arquivo");
define("TXT_MOD_PERFIL_ENVIAR"                 ,"Enviar");
define("TXT_MOD_PERFIL_FOTO_ATUALIZADA"        ,"Foto do perfil atualizada com sucesso. Efetue novo login para atualização do sistema!");

####
# Modulo Ajuda
####
define("TXT_MOD_AJUDA_AJUDA"                    ,"Ajuda");
define("TXT_MOD_AJUDA_DUVIDAS_GERAIS"           ,"Dúvidas Gerais");
define("TXT_MOD_AJUDA_DUVIDAS_CONCEITOS BASICOS","Conceitos basicos?");
define("TXT_MOD_AJUDA_CONCEITOS_CONFIGURACAO"   ,"Configuração?");
define("TXT_MOD_AJUDA_PRODUÇÃO"                 ,"Produção");

####
# Operacoes SQL
####
define("TXT_MYSQLI_OPERACAO_INVALIDA"     ,"Operação SQL Inválida!");
define("TXT_MYSQLI_INSERT_OK"             ,"Registro efetuado com sucesso!");
define("TXT_MYSQLI_INSERT_ERRO"           ,"Erro ao tentar efetuar o registro!");
define("TXT_MYSQLI_UPDATE_OK"             ,"Registro atualizado com sucesso!");
define("TXT_MYSQLI_UPDATE_ERRO"           ,"Erro ao tentar atualizar o registro!");
define("TXT_MYSQLI_UPDATE_ID_INEXISTENTE" ,"Incapaz de realizar atualização. Registro inexistente!");
define("TXT_MYSQLI_DELETE_OK"             ,"Registro(s) apagado(s) com sucesso!");
define("TXT_MYSQLI_DELETE_ERRO"           ,"Erro ao tentar apagar o(s) registro(s)!");
define("TXT_MYSQLI_DELETE_ID_INEXISTENTE" ,"Incapaz de apagar. Registro inexistente!");
define("TXT_MYSQLI_SELECT_NO_RECORD"      ,"Nenhum registro encontrado!");
define("TXT_MYSQLI_REGISTRO_JA_EXISTENTE" ,"Registro já existente!");
define("TXT_MYSQLI_REGISTRO_INEXISTENTE"  ,"Registro inexistente!");

####
# File Upload
####
define("TXT_UPLOAD_ARQUIVO_INVALIDO"            ,"Arquivo inválido!");
define("TXT_UPLOAD_ARQUIVO_JA_EXISTE"           ,"Arquivo já existe!");
define("TXT_UPLOAD_APENAS_JPG_JPEG_PNG_OU_GIF"  ,"Apenas arquivos JPG, JPEG, PNG ou GIF!");
define("TXT_UPLOAD_APENAS_PDF"                  ,"Apenas arquivos PDF!");

####
# Datatable
####
define("TXT_DATATABLE_NENHUM_REGISTRO_ENCONTRADO"    ,"Nenhum registro encontrado");
define("TXT_DATATABLE_MOSTRANDO_0_DE_0_REGISTROS"    ,"Mostrando 0 até 0 de 0 registros");

####
# Modulos - Geral
####
define("TXT_ATIVO"                                   ,"Ativo");
define("TXT_INATIVO"                                 ,"Inativo");
define("TXT_FICHA_ULT_UPDATE"                        ,"Última Atualização:");
define("TXT_CAD_MODAL_NOVO_REGISTRO"                 ,"NOVO REGISTRO");
define("TXT_CAD_MODAL_CAMPOS_COM_A_SAO_OBRIGATORIOS" ,"Campos com * são obrigatórios");
define("TXT_BT_REGISTER"                             ,"Registrar");
define("TXT_BT_SAVE"                                 ,"Salvar");
define("TXT_BT_CANCEL"                               ,"Cancelar");
define("TXT_BT_SEND"                                 ,"Enviar");
define("TXT_BT_GENERATE"                             ,"Gerar");
define("TXT_BT_CALC"                                 ,"Calcular");
define("TXT_NIVEL_ACESSO_ACESSAR"                    ,"Acessar");
define("TXT_NIVEL_ACESSO_EDITAR"                     ,"Editar");
define("TXT_NIVEL_ACESSO_APAGAR"                     ,"Apagar");


####
# view-cfg-plano.php
####
define("TXT_CFG_PLANO_CONFIGURACOES"               ,"Configurações");
define("TXT_CFG_PLANO_PLANO_CONTRATADO"            ,"Plano Contratado");
define("TXT_CFG_PLANO_PLANO_ATUAL"                 ,"Plano Atual");
define("TXT_CFG_PLANO_SITUACAO"                    ,"Situação");
define("TXT_CFG_PLANO_EXPIRADO_OPERANDO_ATE"       ,"Expirado, operando até");
define("TXT_CFG_PLANO_INATIVO"                     ,"Inativo");
define("TXT_CFG_PLANO_PRAZO_TEXT1"                 ,"O prazo de utilização do plano contratado expirou. O sistema ainda continuará operando durante o período de renovação. Após este prazo o sistema se torna inativo e não poderá mais ser utilizado. Por favor, ative um novo plano para continuar utilizando o ME DEI BEM.");
define("TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC1"        ,"Selecione o plano desejado clicando no botão de contratação.");
define("TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC2"        ,"Selecione a forma de pagamento.");
define("TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC3"        ,"Efetue o pagamento.");
define("TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC4"        ,"Assim que o pagamento for confirmado o plano será ativado automaticamente.");
define("TXT_CFG_PLANO_PRAZO_TEXT2"                 ,"O prazo operacional do plano contratado se esgotou. Por favor, ative um novo plano para continuar utilizando o ME DEI BEM.");
define("TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC1"        ,"Selecione o plano desejado clicando no botão de contratação.");
define("TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC2"        ,"Selecione a forma de pagamento.");
define("TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC3"        ,"Efetue o pagamento.");
define("TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC4"        ,"Assim que o pagamento for confirmado o plano será ativado automaticamente.");
define("TXT_CFG_PLANO_PLANOS_PARA_CONTRATACAO"     ,"Planos para Contratação");
define("TXT_CFG_PLANO_PLANO_A_CONTRATAR"           ,"Plano a Contratar");
define("TXT_CFG_PLANO_PAGAMENTO"                   ,"Pagamento");
define("TXT_CFG_PLANO_BOLETO_BANCARIO"             ,"Boleto Bancário");
define("TXT_CFG_PLANO_CARTAO_DE_CREDITO"           ,"Cartão de Crédito");
define("TXT_CFG_PLANO_AGUARDANDO_PAGAMENTO"        ,"Aguardando pagamento");
define("TXT_CFG_PLANO_MENSAL"                      ,"Mensal");
define("TXT_CFG_PLANO_ANUAL"                       ,"Anual");
define("TXT_CFG_PLANO_MES"                         ,"mês");
define("TXT_CFG_PLANO_ANO"                         ,"ano");
define("TXT_CFG_PLANO_CONTRATAR"                   ,"Contratar");
define("TXT_CFG_PLANO_ECONOMIZE"                   ,"Economize");
define("TXT_CFG_PLANO_SIMBOLO_MOEDA"               ,"R$");


define("TXT_CFG_PLANO_GESTAO_DE"                   ,"Gestão de");
define("TXT_CFG_PLANO_CONDOMINIO"                  ,"Condomínio");
define("TXT_CFG_PLANO_CONDOMINIOS"                 ,"Condomínios");
define("TXT_CFG_PLANO_COM"                         ,"Com");
define("TXT_CFG_PLANO_COM_ATE"                     ,"Com até");
define("TXT_CFG_PLANO_COLABORADOR"                 ,"Colaborador");
define("TXT_CFG_PLANO_COLABORADORES"               ,"Colaboradores");
define("TXT_CFG_PLANO_GESTAO_1_CONDOMINIO"         ,"Gestão de 1 Condomínio");
define("TXT_CFG_PLANO_GESTAO_10_CONDOMINIOS"       ,"Gestão de até 10 Condomínios");
define("TXT_CFG_PLANO_GESTAO_50_CONDOMINIOS"       ,"Gestão de até 50 Condomínios");
define("TXT_CFG_PLANO_GESTAO_150_CONDOMINIOS"      ,"Gestão de até 150 Condomínios");
define("TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO"      ,"Cobrança do Condomínio");
define("TXT_CFG_PLANO_COBRANCA_DOS_CONDOMINIOS"    ,"Cobrança dos Condomínios");
define("TXT_CFG_PLANO_EMISSAO_DE_BOLETOS"          ,"Emissão de Boletos");
define("TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE"    ,"Emissão de 2ª Via On-Line");
define("TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS"       ,"Verificação Automática de Pagamentos");
define("TXT_CFG_PLANO_CONTR_CLIENTES"              ,"Controle de Clientes");
define("TXT_CFG_PLANO_CONTR_CONTRATOS"             ,"Controle de Contratos");
define("TXT_CFG_PLANO_CONTR_ATAS"                  ,"Controle de Atas com Arquivos");
define("TXT_CFG_PLANO_ATEND_PREMIUM"               ,"ATENDIMENTO PREMIUM");



/*
define("TXT_CFG_PLANO_CONTRATAR_PLANO_INSTRUC2"    ,"Produção: Visão Geral, Evolução de Lote, Histórico de Lote, Inventário de Lote, Perdas, Lotes, Estágios, Pontos de Controle e Objetivos");
define("TXT_CFG_PLANO_CONTRATAR_PLANO_INSTRUC3"    ,"Controle: Painel de controle com totalizações de registros de todo o sistema, Clientes, Objetivos, Pontos de Controle, Estágios, Lotes, Inventários, Históricos e Alertas");
define("TXT_CFG_PLANO_CONTRATAR_PLANO_INSTRUC4"    ,"Aplicativo de Gravação/Leitura de Etiquetas RFID com contagem de clones em campo");
define("TXT_CFG_PLANO_CONTRATAR_PLANO_INSTRUC5"    ,"Qtde de Lotes Ilimitados");
define("TXT_CFG_PLANO_ATE"                         ,"Até");
define("TXT_CFG_PLANO_VARIEDADES_DE_MUDA"          ,"variedades de muda");
define("TXT_CFG_PLANO_COLABORADORES_SIMULTANEOS"   ,"colaboradores simultâneos");
define("TXT_CFG_PLANO_DISPOSITIVO_DE_APOIO"        ,"dispositivo de Apoio");
define("TXT_CFG_PLANO_DISPOSITIVOS_SIMULTANEOS"    ,"dispositivos simultâneos");

define("TXT_CFG_PLANO_VEJA_E_COMPARE_TODAS_AS_FUNCIONALIDADES"        ,"Veja e compare todas as funcionalidades");
define("TXT_CFG_PLANO_TODAS_AS_FUNCIONALIDADES_DO_PLANO_ECONOMICO"    ,"Todas as funcionalidades do plano econômino +");
define("TXT_CFG_PLANO_INFORMATIVOS_DIARIOS_POR_EMAIL"                 ,"Informativos diários por e-mail");
define("TXT_CFG_PLANO_TODAS_AS_FUNCIONALIDADES_DO_PLANO_ESSENCIAL"    ,"Todas as funcionalidades do plano essencial +");
define("TXT_CFG_PLANO_GESTAO_DE_CONTRATOS"                            ,"Gestão de Contratos");
define("TXT_CFG_PLANO_REPOSITORIO_DE_ARQUIVOS"                        ,"Repositório de Arquivos");
*/

####
# view-cfg-colabs.php
####
define("TXT_CFG_COLABS_CONFIGURACAO"           ,"Configuração");
define("TXT_CFG_COLABS_COLABORADORES"          ,"Colaboradores");
define("TXT_CFG_COLABS_USUARIO_INEXISTENTE"    ,"Usuário inexistente!");
define("TXT_CFG_COLABS_USUARIO_EXISTE"         ,"Usuário existe!");
define("TXT_CFG_COLABS_USUARIO_NAO_CONFIRMADO" ,"Usuário informado ainda não confirmou os dados cadastrais!");
define("TXT_CFG_COLABS_USUARIO_INATIVO"        ,"Conta do usuário inativa!");
define("TXT_CFG_COLABS_INCAPAZ_CARREGAR_PLANO" ,"Incapaz de carregar dados do plano contratado!");
define("TXT_CFG_COLABS_FICHA_NOME"             ,"Nome");
define("TXT_CFG_COLABS_FICHA_CPF"              ,"CPF");
define("TXT_CFG_COLABS_FICHA_EMAIL"            ,"E-Mail");
define("TXT_CFG_COLABS_FICHA_FONE1"            ,"Fone Fixo");
define("TXT_CFG_COLABS_FICHA_CEL1"             ,"Celular");
define("TXT_CFG_COLABS_FICHA_ENDERECO"         ,"Endereço");
define("TXT_CFG_COLABS_FICHA_USUARIO"          ,"Usuário");
define("TXT_CFG_COLABS_FICHA_INCLUSAO"         ,"Inclusão");
define("TXT_CFG_COLABS_FICHA_ULT_ACESSO"       ,"Últ. Acesso");
define("TXT_CFG_COLABS_FICHA_SITUACAO"         ,"Situação");
define("TXT_CFG_COLABS_FICHA_NIVEL_ACESSO"     ,"Nível de Acesso");
define("TXT_CFG_COLABS_SELECIONE"              ,"--Selecione--");
define("TXT_CFG_COLABS_ATIVO"                  ,"Ativo");
define("TXT_CFG_COLABS_INATIVO"                ,"Inativo");
define("TXT_CFG_COLABS_ACESSAR"                ,"Acessar");
define("TXT_CFG_COLABS_EDITAR"                 ,"Editar");
define("TXT_CFG_COLABS_APAGAR"                 ,"Apagar");


####
# view-cfg-dashboard.php
####
define("TXT_SYSTEM_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_SYSTEM_SIDE_MENU_CFG_DASHBOARD","Dashboard");
define("TXT_CFG_DASHBOARD_CFG","cfg");
define("TXT_CFG_DASHBOARD_CONFIGURACAO","Configuração");
define("TXT_CFG_DASHBOARD_DASHBOARD","Dashboard");
define("TXT_CFG_DASHBOARD_PAINEL","Painel");
define("TXT_CFG_DASHBOARD_ORDEM","Ordem");
define("TXT_CFG_DASHBOARD_EXIBIR","Exibir");
define("TXT_CFG_DASHBOARD_O_CAMPO_EXIBIR_E_OBRIGATORIO","O campo EXIBIR é obrigatório!");
define("TXT_CFG_DASHBOARD_1","1");
define("TXT_CFG_DASHBOARD_SIM","Sim");
define("TXT_CFG_DASHBOARD_0","0");
define("TXT_CFG_DASHBOARD_NAO","Não");

####
# view-cfg-dispositivos.php
####
define("TXT_SYSTEM_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_SYSTEM_SIDE_MENU_CFG_DISPOSITIVOS","Dispositivos");
define("TXT_CFG_DISPOSITIVOS_CFG","cfg");
define("TXT_CFG_DISPOSITIVOS_CONFIGURACAO","Configuração");
define("TXT_CFG_DISPOSITIVOS_DISPOSITIVOS","Dispositivos");
define("TXT_CFG_DISPOSITIVOS_NUMERO","Número");
define("TXT_CFG_DISPOSITIVOS_O_CAMPO_NUMERO_E_OBRIGATORIO","O campo NÚMERO é obrigatório!");
define("TXT_CFG_DISPOSITIVOS_SENHA","Senha");
define("TXT_CFG_DISPOSITIVOS_COLABORADOR","Colaborador");
define("TXT_CFG_DISPOSITIVOS_ULT_SINCR","Últ. Sincronismo");
define("TXT_CFG_DISPOSITIVOS_USO_LIBERADO","Uso Liberado");
define("TXT_CFG_DISPOSITIVOS_O_CAMPO_USO_LIBERADO_E_OBRIGATORIO","O campo USO LIBERADO é obrigatório!");
define("TXT_CFG_DISPOSITIVOS_1","1");
define("TXT_CFG_DISPOSITIVOS_SIM","Sim");
define("TXT_CFG_DISPOSITIVOS_0","0");
define("TXT_CFG_DISPOSITIVOS_NAO","Não");

####
# view-cfg-sincr.php
####
define("TXT_SYSTEM_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_SYSTEM_SIDE_MENU_CFG_SINCR","Sincronia");
define("TXT_CFG_SINCR_CFG","cfg");
define("TXT_CFG_SINCR_CONFIGURACAO","Configuração");
define("TXT_CFG_SINCR_SINCRONIA","Sincronia");
define("TXT_CFG_SINCR_EMPRESA","Empresa");
define("TXT_CFG_SINCR_QR_INTRUC1","Utilize o código QR abaixo para configuração automática do dispositivo.");
define("TXT_CFG_SINCR_QR_INTRUC2","Utilize os códigos QR abaixo para configuração automática dos dispositivos.");
define("TXT_CFG_SINCR_RAZAO_EM_BRANCO","Campo Razão Social do cadastro em branco!");
define("TXT_CFG_SINCR_RAZAO_EM_BRANCO_INSTRUC","Preencha o campo Razão Social do cadastro do cliente para que o nome da empresa apareça no APP");

####
# view-adm-cliente.php
####
define("TXT_SYSTEM_SIDE_MENU_ADMINISTRATIVO","Administrativo");
define("TXT_SYSTEM_SIDE_MENU_ADM_CLIENTE","Cliente");
define("TXT_ADM_CLIENTE_ADM","adm");
define("TXT_ADM_CLIENTE_ADMINISTRATIVO","Administrativo");
define("TXT_ADM_CLIENTE_CLIENTE","Cliente");
define("TXT_ADM_CLIENTE_NOME_INTERNO","Nome Interno");
define("TXT_ADM_CLIENTE_O_CAMPO_NOME_INTERNO_E_OBRIGATORIO","O campo NOME INTERNO é obrigatório!");
define("TXT_ADM_CLIENTE_EMPRESA","Empresa");
define("TXT_ADM_CLIENTE_O_CAMPO_EMPRESA_E_OBRIGATORIO","O campo EMPRESA é obrigatório!");
define("TXT_ADM_CLIENTE_CNPJ","CNPJ");
define("TXT_ADM_CLIENTE_O_CAMPO_CNPJ_E_OBRIGATORIO","O campo CNPJ é obrigatório!");
define("TXT_ADM_CLIENTE_FONE_1","Fone 1");
define("TXT_ADM_CLIENTE_O_CAMPO_FONE_1_E_OBRIGATORIO","O campo FONE 1 é obrigatório!");
define("TXT_ADM_CLIENTE_FONE_2","Fone 2");
define("TXT_ADM_CLIENTE_E_MAIL","E-Mail");
define("TXT_ADM_CLIENTE_O_CAMPO_E_MAIL_E_OBRIGATORIO","O campo E-MAIL é obrigatório!");
define("TXT_ADM_CLIENTE_CEP","CEP");
define("TXT_ADM_CLIENTE_O_CAMPO_CEP_E_OBRIGATORIO","O campo CEP é obrigatório!");
define("TXT_ADM_CLIENTE_ESTADO","Estado");
define("TXT_ADM_CLIENTE_O_CAMPO_ESTADO_E_OBRIGATORIO","O campo ESTADO é obrigatório!");
define("TXT_ADM_CLIENTE_AC","AC");
define("TXT_ADM_CLIENTE_ACRE","ACRE");
define("TXT_ADM_CLIENTE_AL","AL");
define("TXT_ADM_CLIENTE_ALAGOAS","ALAGOAS");
define("TXT_ADM_CLIENTE_AM","AM");
define("TXT_ADM_CLIENTE_AMAZONAS","AMAZONAS");
define("TXT_ADM_CLIENTE_AP","AP");
define("TXT_ADM_CLIENTE_AMAPA","AMAPÁ");
define("TXT_ADM_CLIENTE_BA","BA");
define("TXT_ADM_CLIENTE_BAHIA","BAHIA");
define("TXT_ADM_CLIENTE_CE","CE");
define("TXT_ADM_CLIENTE_CEARA","CEARÁ");
define("TXT_ADM_CLIENTE_DF","DF");
define("TXT_ADM_CLIENTE_DISTRITO_FEDERAL","DISTRITO FEDERAL");
define("TXT_ADM_CLIENTE_ES","ES");
define("TXT_ADM_CLIENTE_ESPIRITO_SANTO","ESPÍRITO SANTO");
define("TXT_ADM_CLIENTE_GO","GO");
define("TXT_ADM_CLIENTE_GOIAS","GOIÁS");
define("TXT_ADM_CLIENTE_MA","MA");
define("TXT_ADM_CLIENTE_MARANHAO","MARANHÃO");
define("TXT_ADM_CLIENTE_MG","MG");
define("TXT_ADM_CLIENTE_MINAS_GERAIS","MINAS GERAIS");
define("TXT_ADM_CLIENTE_MS","MS");
define("TXT_ADM_CLIENTE_MATO_GROSSO_DO_SUL","MATO GROSSO DO SUL");
define("TXT_ADM_CLIENTE_MT","MT");
define("TXT_ADM_CLIENTE_MATO_GROSSO","MATO GROSSO");
define("TXT_ADM_CLIENTE_PA","PA");
define("TXT_ADM_CLIENTE_PARA","PARÁ");
define("TXT_ADM_CLIENTE_PB","PB");
define("TXT_ADM_CLIENTE_PARAIBA","PARAÍBA");
define("TXT_ADM_CLIENTE_PE","PE");
define("TXT_ADM_CLIENTE_PERNAMBUCO","PERNAMBUCO");
define("TXT_ADM_CLIENTE_PI","PI");
define("TXT_ADM_CLIENTE_PIAUI","PIAUÍ");
define("TXT_ADM_CLIENTE_PR","PR");
define("TXT_ADM_CLIENTE_PARANA","PARANÁ");
define("TXT_ADM_CLIENTE_RJ","RJ");
define("TXT_ADM_CLIENTE_RIO_DE_JANEIRO","RIO DE JANEIRO");
define("TXT_ADM_CLIENTE_RN","RN");
define("TXT_ADM_CLIENTE_RIO_GRANDE_DO_NORTE","RIO GRANDE DO NORTE");
define("TXT_ADM_CLIENTE_RO","RO");
define("TXT_ADM_CLIENTE_RONDONIA","RONDÔNIA");
define("TXT_ADM_CLIENTE_RR","RR");
define("TXT_ADM_CLIENTE_RORAIMA","RORAIMA");
define("TXT_ADM_CLIENTE_RS","RS");
define("TXT_ADM_CLIENTE_RIO_GRANDE_DO_SUL","RIO GRANDE DO SUL");
define("TXT_ADM_CLIENTE_SC","SC");
define("TXT_ADM_CLIENTE_SANTA_CATARINA","SANTA CATARINA");
define("TXT_ADM_CLIENTE_SE","SE");
define("TXT_ADM_CLIENTE_SERGIPE","SERGIPE");
define("TXT_ADM_CLIENTE_SP","SP");
define("TXT_ADM_CLIENTE_SAO_PAULO","SÃO PAULO");
define("TXT_ADM_CLIENTE_TO","TO");
define("TXT_ADM_CLIENTE_TOCANTINS","TOCANTINS");
define("TXT_ADM_CLIENTE_CIDADE","Cidade");
define("TXT_ADM_CLIENTE_O_CAMPO_CIDADE_E_OBRIGATORIO","O campo CIDADE é obrigatório!");
define("TXT_ADM_CLIENTE_ESCOLHA_UMA_CIDADE_PRIMEIRO","Escolha Um Estado Primeiro");
define("TXT_ADM_CLIENTE_BAIRRO","Bairro");
define("TXT_ADM_CLIENTE_O_CAMPO_BAIRRO_E_OBRIGATORIO","O campo BAIRRO é obrigatório!");
define("TXT_ADM_CLIENTE_ENDERECO","Endereço");
define("TXT_ADM_CLIENTE_O_CAMPO_ENDERECO_E_OBRIGATORIO","O campo ENDEREÇO é obrigatório!");
define("TXT_ADM_CLIENTE_NUMERO","Número");
define("TXT_ADM_CLIENTE_O_CAMPO_NUMERO_E_OBRIGATORIO","O campo NÚMERO é obrigatório!");
define("TXT_ADM_CLIENTE_COMPLEMENTO","Complemento");
define("TXT_ADM_CLIENTE_OBSERVACAO","Observação");
define("TXT_ADM_CLIENTE_CONTATO","Contato");
define("TXT_ADM_CLIENTE_CONTATO_NOME","Contato - Nome");
define("TXT_ADM_CLIENTE_CONTATO_FONE","Contato - Fone");
define("TXT_ADM_CLIENTE_CONTATO_CELULAR","Contato - Celular");
define("TXT_ADM_CLIENTE_CONTATO_WHATSAPP","Contato - WhatsApp");
define("TXT_ADM_CLIENTE_CONTATO_EMAIL","Contato - E-Mail");
define("TXT_ADM_CLIENTE_O_CAMPO_CONTATO_EMAIL_E_OBRIGATORIO","O campo CONTATO - E-MAIL é obrigatório!");
define("TXT_ADM_CLIENTE_SITUACAO","Situação");
define("TXT_ADM_CLIENTE_O_CAMPO_SITUACAO_E_OBRIGATORIO","O campo SITUAÇÃO é obrigatório!");
define("TXT_ADM_CLIENTE_A","A");
define("TXT_ADM_CLIENTE_ATIVO","Ativo");
define("TXT_ADM_CLIENTE_I","I");
define("TXT_ADM_CLIENTE_INATIVO","Inativo");
define("TXT_ADM_CLIENTE_LD_RUIDO","Laudo Ruído");
define("TXT_ADM_CLIENTE_LD_PART","Laudo Particulado");
define("TXT_ADM_CLIENTE_LD_POEI","Laudo Poeira");
define("TXT_ADM_CLIENTE_LD_VAP","Laudo Vapores");
define("TXT_ADM_CLIENTE_LD_VIBR_VCI","Laudo Vibração VCI");
define("TXT_ADM_CLIENTE_LD_VIBR_VMB","Laudo Vibração VMB");
define("TXT_ADM_CLIENTE_LD_CALOR","Laudo Calor");
define("TXT_ADM_CLIENTE_LD_BIO","Laudo Biológico");
define("TXT_ADM_CLIENTE_LD_ELETR","Laudo Eletricidade");
define("TXT_ADM_CLIENTE_LD_EXP","Laudo Explosivos");
define("TXT_ADM_CLIENTE_LD_INFL","Laudo Inflamáveis");
define("TXT_ADM_CLIENTE_LD_RAD","Laudo Radiação");
define("TXT_ADM_CLIENTE_LD_RIS","Laudo Avaliação de Risco");
define("TXT_ADM_CLIENTE_S","Sim");
define("TXT_ADM_CLIENTE_N","Não");
define("TXT_ADM_CLIENTE_LAUDOS","Laudos");
define("TXT_ADM_CLIENTE_LD_TIT_RUIDO","Ruído");
define("TXT_ADM_CLIENTE_LD_TIT_PART","Particulado");
define("TXT_ADM_CLIENTE_LD_TIT_POEI","Poeira");
define("TXT_ADM_CLIENTE_LD_TIT_VAP","Vapores");
define("TXT_ADM_CLIENTE_LD_TIT_VIBR_VCI","Vibração VCI");
define("TXT_ADM_CLIENTE_LD_TIT_VIBR_VMB","Vibração VMB");
define("TXT_ADM_CLIENTE_LD_TIT_CALOR","Calor");
define("TXT_ADM_CLIENTE_LD_TIT_BIO","Biológico");
define("TXT_ADM_CLIENTE_LD_TIT_ELETR","Eletricidade");
define("TXT_ADM_CLIENTE_LD_TIT_EXP","Explosivos");
define("TXT_ADM_CLIENTE_LD_TIT_INFL","Inflamáveis");
define("TXT_ADM_CLIENTE_LD_TIT_RAD","Radiação");
define("TXT_ADM_CLIENTE_LD_TIT_RIS","Avaliação de Risco");

####
# view-laudos-ruido.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_RUIDO","Ruído");
define("TXT_LAUDOS_RUIDO_LAUDOS","Laudos");
define("TXT_LAUDOS_RUIDO_RUIDO","Ruído");
define("TXT_LAUDOS_RUIDO_CLIENTE","Cliente");
define("TXT_LAUDOS_RUIDO_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_RUIDO_ANO","Ano");
define("TXT_LAUDOS_RUIDO_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_RUIDO_MES","Mês");
define("TXT_LAUDOS_RUIDO_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_RUIDO_1","1");
define("TXT_LAUDOS_RUIDO_JAN","JAN");
define("TXT_LAUDOS_RUIDO_2","2");
define("TXT_LAUDOS_RUIDO_FEV","FEV");
define("TXT_LAUDOS_RUIDO_3","3");
define("TXT_LAUDOS_RUIDO_MAR","MAR");
define("TXT_LAUDOS_RUIDO_4","4");
define("TXT_LAUDOS_RUIDO_ABR","ABR");
define("TXT_LAUDOS_RUIDO_5","5");
define("TXT_LAUDOS_RUIDO_MAI","MAI");
define("TXT_LAUDOS_RUIDO_6","6");
define("TXT_LAUDOS_RUIDO_JUN","JUN");
define("TXT_LAUDOS_RUIDO_7","7");
define("TXT_LAUDOS_RUIDO_JUL","JUL");
define("TXT_LAUDOS_RUIDO_8","8");
define("TXT_LAUDOS_RUIDO_AGO","AGO");
define("TXT_LAUDOS_RUIDO_9","9");
define("TXT_LAUDOS_RUIDO_SET","SET");
define("TXT_LAUDOS_RUIDO_10","10");
define("TXT_LAUDOS_RUIDO_OUT","OUT");
define("TXT_LAUDOS_RUIDO_11","11");
define("TXT_LAUDOS_RUIDO_NOV","NOV");
define("TXT_LAUDOS_RUIDO_12","12");
define("TXT_LAUDOS_RUIDO_DEZ","DEZ");
define("TXT_LAUDOS_RUIDO_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_RUIDO_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_RUIDO_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_RUIDO_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_RUIDO_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_RUIDO_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_RUIDO_ELABORACAO","Elaboração");
define("TXT_LAUDOS_RUIDO_AREA","Área");
define("TXT_LAUDOS_RUIDO_SETOR","Setor");
define("TXT_LAUDOS_RUIDO_GES","GES");
define("TXT_LAUDOS_RUIDO_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_RUIDO_CBO","CBO");
define("TXT_LAUDOS_RUIDO_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRAS","Análise - Amostras");
define("TXT_LAUDOS_RUIDO_O_CAMPO_ANALISE_AMOSTRAS_E_OBRIGATORIO","O campo ANÁLISE - AMOSTRAS é obrigatório!");
define("TXT_LAUDOS_RUIDO_1","1");
define("TXT_LAUDOS_RUIDO_2","2");
define("TXT_LAUDOS_RUIDO_3","3");
define("TXT_LAUDOS_RUIDO_4","4");
define("TXT_LAUDOS_RUIDO_5","5");
define("TXT_LAUDOS_RUIDO_6","6");
define("TXT_LAUDOS_RUIDO_ANALISES","Análises");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_1","Análise #1 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_1_PROCESSO_PRODUTIVO","Análise #1 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_1_OBS_TAREFA","Análise #1 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_2","Análise #2 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_2_PROCESSO_PRODUTIVO","Análise #2 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_2_OBS_TAREFA","Análise #2 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_3","Análise #3 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_3_PROCESSO_PRODUTIVO","Análise #3 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_3_OBS_TAREFA","Análise #3 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_4","Análise #4 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_4_PROCESSO_PRODUTIVO","Análise #4 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_4_OBS_TAREFA","Análise #4 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_5","Análise #5 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_5_PROCESSO_PRODUTIVO","Análise #5 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_5_OBS_TAREFA","Análise #5 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_6","Análise #6 - Amostra");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_6_PROCESSO_PRODUTIVO","Análise #6 - Processo Produtivo");
define("TXT_LAUDOS_RUIDO_ANALISE_AMOSTRA_6_OBS_TAREFA","Análise #6 - Obs Tarefa");
define("TXT_LAUDOS_RUIDO_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_RUIDO_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_RUIDO_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_RUIDO_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_RUIDO_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_RUIDO_MITIGACAO","Mitigação");
define("TXT_LAUDOS_RUIDO_COLETAS","Coletas");
define("TXT_LAUDOS_RUIDO_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1","Coleta #1 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_DATA","Coleta #1 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_N_SERIAL","Coleta #1 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_N_CERT_DE_CALIBRACAO","Coleta #1 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_TEMPO_DE_AMOSTRAGEM","Coleta #1 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_DOSE","Coleta #1 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_LAVG","Coleta #1 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_TWA","Coleta #1 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_NR09","Coleta #1 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_NR15","Coleta #1 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_1_ACGIH","Coleta #1 - ACGIH");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2","Coleta #2 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_DATA","Coleta #2 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_N_SERIAL","Coleta #2 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_N_CERT_DE_CALIBRACAO","Coleta #2 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_TEMPO_DE_AMOSTRAGEM","Coleta #2 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_DOSE","Coleta #2 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_LAVG","Coleta #2 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_TWA","Coleta #2 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_NR09","Coleta #2 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_NR15","Coleta #2 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_2_ACGIH","Coleta #2 - ACGIH");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3","Coleta #3 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_DATA","Coleta #3 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_N_SERIAL","Coleta #3 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_N_CERT_DE_CALIBRACAO","Coleta #3 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_TEMPO_DE_AMOSTRAGEM","Coleta #3 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_DOSE","Coleta #3 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_LAVG","Coleta #3 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_TWA","Coleta #3 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_NR09","Coleta #3 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_NR15","Coleta #3 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_3_ACGIH","Coleta #3 - ACGIH");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4","Coleta #4 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_DATA","Coleta #4 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_N_SERIAL","Coleta #4 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_N_CERT_DE_CALIBRACAO","Coleta #4 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_TEMPO_DE_AMOSTRAGEM","Coleta #4 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_DOSE","Coleta #4 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_LAVG","Coleta #4 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_TWA","Coleta #4 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_NR09","Coleta #4 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_NR15","Coleta #4 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_4_ACGIH","Coleta #4 - ACGIH");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5","Coleta #5 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_DATA","Coleta #5 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_N_SERIAL","Coleta #5 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_N_CERT_DE_CALIBRACAO","Coleta #5 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_TEMPO_DE_AMOSTRAGEM","Coleta #5 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_DOSE","Coleta #5 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_LAVG","Coleta #5 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_TWA","Coleta #5 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_NR09","Coleta #5 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_NR15","Coleta #5 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_5_ACGIH","Coleta #5 - ACGIH");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6","Coleta #6 - Amostra");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_DATA","Coleta #6 - Data");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_N_SERIAL","Coleta #6 - Nº Serial");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_N_CERT_DE_CALIBRACAO","Coleta #6 - Nº Cert. de Calibração");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_TEMPO_DE_AMOSTRAGEM","Coleta #6 - Tempo de Amostragem");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_DOSE","Coleta #6 - Dose");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_LAVG","Coleta #6 - LAVG");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_TWA","Coleta #6 - TWA");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_NR09","Coleta #6 - NR09");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_NR15","Coleta #6 - NR15");
define("TXT_LAUDOS_RUIDO_COLETA_AMOSTRA_6_ACGIH","Coleta #6 - ACGIH");
define("TXT_LAUDOS_RUIDO_CERTIFICADO_DE_APROVACAO","Certificado de Aprovação");
define("TXT_LAUDOS_RUIDO_NRRSF","NRRSF");
define("TXT_LAUDOS_RUIDO_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_RUIDO_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_RUIDO_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_RUIDO_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_RUIDO_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_RUIDO_IMAGEM_LOGOMARCA","Imagem Logomarca");


####
# view-laudos-eletr.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_ELETR","Eletricidade");
define("TXT_LAUDOS_ELETR_LAUDOS","Laudos");
define("TXT_LAUDOS_ELETR_ELETRICIDADE","Eletricidade");
define("TXT_LAUDOS_ELETR_CLIENTE","Cliente");
define("TXT_LAUDOS_ELETR_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_ELETR_ANO","Ano");
define("TXT_LAUDOS_ELETR_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_ELETR_MES","Mês");
define("TXT_LAUDOS_ELETR_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_ELETR_1","1");
define("TXT_LAUDOS_ELETR_JAN","JAN");
define("TXT_LAUDOS_ELETR_2","2");
define("TXT_LAUDOS_ELETR_FEV","FEV");
define("TXT_LAUDOS_ELETR_3","3");
define("TXT_LAUDOS_ELETR_MAR","MAR");
define("TXT_LAUDOS_ELETR_4","4");
define("TXT_LAUDOS_ELETR_ABR","ABR");
define("TXT_LAUDOS_ELETR_5","5");
define("TXT_LAUDOS_ELETR_MAI","MAI");
define("TXT_LAUDOS_ELETR_6","6");
define("TXT_LAUDOS_ELETR_JUN","JUN");
define("TXT_LAUDOS_ELETR_7","7");
define("TXT_LAUDOS_ELETR_JUL","JUL");
define("TXT_LAUDOS_ELETR_8","8");
define("TXT_LAUDOS_ELETR_AGO","AGO");
define("TXT_LAUDOS_ELETR_9","9");
define("TXT_LAUDOS_ELETR_SET","SET");
define("TXT_LAUDOS_ELETR_10","10");
define("TXT_LAUDOS_ELETR_OUT","OUT");
define("TXT_LAUDOS_ELETR_11","11");
define("TXT_LAUDOS_ELETR_NOV","NOV");
define("TXT_LAUDOS_ELETR_12","12");
define("TXT_LAUDOS_ELETR_DEZ","DEZ");
define("TXT_LAUDOS_ELETR_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_ELETR_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_ELETR_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_ELETR_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_ELETR_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_ELETR_ELABORACAO","Elaboração");
define("TXT_LAUDOS_ELETR_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_ELETR_AREA","Área");
define("TXT_LAUDOS_ELETR_SETOR","Setor");
define("TXT_LAUDOS_ELETR_GES","GES");
define("TXT_LAUDOS_ELETR_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_ELETR_CBO","CBO");
define("TXT_LAUDOS_ELETR_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_ELETR_TAREFA_EXECUTADA","Tarefa Executada");
define("TXT_LAUDOS_ELETR_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_ELETR_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_ELETR_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_ELETR_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_ELETR_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_ELETR_EPI","EPI");
define("TXT_LAUDOS_ELETR_EPC","EPC");
define("TXT_LAUDOS_ELETR_ENQUADRAMENTO_NR16","Enquadramento NR16");
define("TXT_LAUDOS_ELETR_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_ELETR_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_ELETR_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_ELETR_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_ELETR_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_ELETR_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_ELETR_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_ELETR_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-bio.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_BIO","Biológico");
define("TXT_LAUDOS_BIO_LAUDOS","Laudos");
define("TXT_LAUDOS_BIO_BIOLOGICO","Biológico");
define("TXT_LAUDOS_BIO_CLIENTE","Cliente");
define("TXT_LAUDOS_BIO_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_BIO_ANO","Ano");
define("TXT_LAUDOS_BIO_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_BIO_MES","Mês");
define("TXT_LAUDOS_BIO_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_BIO_1","1");
define("TXT_LAUDOS_BIO_JAN","JAN");
define("TXT_LAUDOS_BIO_2","2");
define("TXT_LAUDOS_BIO_FEV","FEV");
define("TXT_LAUDOS_BIO_3","3");
define("TXT_LAUDOS_BIO_MAR","MAR");
define("TXT_LAUDOS_BIO_4","4");
define("TXT_LAUDOS_BIO_ABR","ABR");
define("TXT_LAUDOS_BIO_5","5");
define("TXT_LAUDOS_BIO_MAI","MAI");
define("TXT_LAUDOS_BIO_6","6");
define("TXT_LAUDOS_BIO_JUN","JUN");
define("TXT_LAUDOS_BIO_7","7");
define("TXT_LAUDOS_BIO_JUL","JUL");
define("TXT_LAUDOS_BIO_8","8");
define("TXT_LAUDOS_BIO_AGO","AGO");
define("TXT_LAUDOS_BIO_9","9");
define("TXT_LAUDOS_BIO_SET","SET");
define("TXT_LAUDOS_BIO_10","10");
define("TXT_LAUDOS_BIO_OUT","OUT");
define("TXT_LAUDOS_BIO_11","11");
define("TXT_LAUDOS_BIO_NOV","NOV");
define("TXT_LAUDOS_BIO_12","12");
define("TXT_LAUDOS_BIO_DEZ","DEZ");
define("TXT_LAUDOS_BIO_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_BIO_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_BIO_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_BIO_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_BIO_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_BIO_ELABORACAO","Elaboração");
define("TXT_LAUDOS_BIO_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_BIO_AREA","Área");
define("TXT_LAUDOS_BIO_SETOR","Setor");
define("TXT_LAUDOS_BIO_GES","GES");
define("TXT_LAUDOS_BIO_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_BIO_CBO","CBO");
define("TXT_LAUDOS_BIO_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_BIO_TAREFA_EXECUTADA","Tarefa Executada");
define("TXT_LAUDOS_BIO_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_BIO_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_BIO_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_BIO_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_BIO_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_BIO_EPI","EPI");
define("TXT_LAUDOS_BIO_EPC","EPC");
define("TXT_LAUDOS_BIO_ENQUADRAMENTO_NR16","Enquadramento NR16");
define("TXT_LAUDOS_BIO_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_BIO_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_BIO_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_BIO_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_BIO_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_BIO_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_BIO_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_BIO_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-explo.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_EXPLO","Explosivo");
define("TXT_LAUDOS_EXPLO_LAUDOS","Laudos");
define("TXT_LAUDOS_EXPLO_EXPLOSIVO","Explosivo");
define("TXT_LAUDOS_EXPLO_CLIENTE","Cliente");
define("TXT_LAUDOS_EXPLO_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_EXPLO_ANO","Ano");
define("TXT_LAUDOS_EXPLO_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_EXPLO_MES","Mês");
define("TXT_LAUDOS_EXPLO_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_EXPLO_1","1");
define("TXT_LAUDOS_EXPLO_JAN","JAN");
define("TXT_LAUDOS_EXPLO_2","2");
define("TXT_LAUDOS_EXPLO_FEV","FEV");
define("TXT_LAUDOS_EXPLO_3","3");
define("TXT_LAUDOS_EXPLO_MAR","MAR");
define("TXT_LAUDOS_EXPLO_4","4");
define("TXT_LAUDOS_EXPLO_ABR","ABR");
define("TXT_LAUDOS_EXPLO_5","5");
define("TXT_LAUDOS_EXPLO_MAI","MAI");
define("TXT_LAUDOS_EXPLO_6","6");
define("TXT_LAUDOS_EXPLO_JUN","JUN");
define("TXT_LAUDOS_EXPLO_7","7");
define("TXT_LAUDOS_EXPLO_JUL","JUL");
define("TXT_LAUDOS_EXPLO_8","8");
define("TXT_LAUDOS_EXPLO_AGO","AGO");
define("TXT_LAUDOS_EXPLO_9","9");
define("TXT_LAUDOS_EXPLO_SET","SET");
define("TXT_LAUDOS_EXPLO_10","10");
define("TXT_LAUDOS_EXPLO_OUT","OUT");
define("TXT_LAUDOS_EXPLO_11","11");
define("TXT_LAUDOS_EXPLO_NOV","NOV");
define("TXT_LAUDOS_EXPLO_12","12");
define("TXT_LAUDOS_EXPLO_DEZ","DEZ");
define("TXT_LAUDOS_EXPLO_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_EXPLO_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_EXPLO_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_EXPLO_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_EXPLO_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_EXPLO_ELABORACAO","Elaboração");
define("TXT_LAUDOS_EXPLO_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_EXPLO_AREA","Área");
define("TXT_LAUDOS_EXPLO_SETOR","Setor");
define("TXT_LAUDOS_EXPLO_GES","GES");
define("TXT_LAUDOS_EXPLO_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_EXPLO_CBO","CBO");
define("TXT_LAUDOS_EXPLO_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_EXPLO_TAREFA_EXECUTADA","Tarefa Executada");
define("TXT_LAUDOS_EXPLO_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_EXPLO_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_EXPLO_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_EXPLO_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_EXPLO_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_EXPLO_EPI","EPI");
define("TXT_LAUDOS_EXPLO_EPC","EPC");
define("TXT_LAUDOS_EXPLO_ENQUADRAMENTO_NR16","Enquadramento NR16");
define("TXT_LAUDOS_EXPLO_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_EXPLO_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_EXPLO_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_EXPLO_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_EXPLO_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_EXPLO_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_EXPLO_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_EXPLO_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-infl.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_INFL","Inflamável");
define("TXT_LAUDOS_INFL_LAUDOS","Laudos");
define("TXT_LAUDOS_INFL_INFLAMAVEL","Inflamável");
define("TXT_LAUDOS_INFL_CLIENTE","Cliente");
define("TXT_LAUDOS_INFL_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_INFL_ANO","Ano");
define("TXT_LAUDOS_INFL_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_INFL_MES","Mês");
define("TXT_LAUDOS_INFL_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_INFL_1","1");
define("TXT_LAUDOS_INFL_JAN","JAN");
define("TXT_LAUDOS_INFL_2","2");
define("TXT_LAUDOS_INFL_FEV","FEV");
define("TXT_LAUDOS_INFL_3","3");
define("TXT_LAUDOS_INFL_MAR","MAR");
define("TXT_LAUDOS_INFL_4","4");
define("TXT_LAUDOS_INFL_ABR","ABR");
define("TXT_LAUDOS_INFL_5","5");
define("TXT_LAUDOS_INFL_MAI","MAI");
define("TXT_LAUDOS_INFL_6","6");
define("TXT_LAUDOS_INFL_JUN","JUN");
define("TXT_LAUDOS_INFL_7","7");
define("TXT_LAUDOS_INFL_JUL","JUL");
define("TXT_LAUDOS_INFL_8","8");
define("TXT_LAUDOS_INFL_AGO","AGO");
define("TXT_LAUDOS_INFL_9","9");
define("TXT_LAUDOS_INFL_SET","SET");
define("TXT_LAUDOS_INFL_10","10");
define("TXT_LAUDOS_INFL_OUT","OUT");
define("TXT_LAUDOS_INFL_11","11");
define("TXT_LAUDOS_INFL_NOV","NOV");
define("TXT_LAUDOS_INFL_12","12");
define("TXT_LAUDOS_INFL_DEZ","DEZ");
define("TXT_LAUDOS_INFL_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_INFL_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_INFL_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_INFL_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_INFL_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_INFL_ELABORACAO","Elaboração");
define("TXT_LAUDOS_INFL_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_INFL_AREA","Área");
define("TXT_LAUDOS_INFL_SETOR","Setor");
define("TXT_LAUDOS_INFL_GES","GES");
define("TXT_LAUDOS_INFL_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_INFL_CBO","CBO");
define("TXT_LAUDOS_INFL_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_INFL_TAREFA_EXECUTADA","Tarefa Executada");
define("TXT_LAUDOS_INFL_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_INFL_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_INFL_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_INFL_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_INFL_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_INFL_ATIVIDADE","Atividade");
define("TXT_LAUDOS_INFL_COMBUSTIVEL","Combustível");
define("TXT_LAUDOS_INFL_QUANTIDADE","Quantidade");
define("TXT_LAUDOS_INFL_FULGOR","Fulgor");
define("TXT_LAUDOS_INFL_INFLAMABILIDADE","Inflamabilidade");
define("TXT_LAUDOS_INFL_EXPLOSIVIDADE","Explosividade");
define("TXT_LAUDOS_INFL_ITEM_4Q1","Ítem 4Q1");
define("TXT_LAUDOS_INFL_ITEM_1Q3","Ítem 1Q3");
define("TXT_LAUDOS_INFL_ITEM_2Q3","Ítem 2Q3");
define("TXT_LAUDOS_INFL_ITEM_3Q3","Ítem 3Q3");
define("TXT_LAUDOS_INFL_EPI","EPI");
define("TXT_LAUDOS_INFL_EPC","EPC");
define("TXT_LAUDOS_INFL_ENQUADRAMENTO_NR16","Enquadramento NR16");
define("TXT_LAUDOS_INFL_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_INFL_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_INFL_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_INFL_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_INFL_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_INFL_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_INFL_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_INFL_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");


####
# view-laudos-calor.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_CALOR","Calor");
define("TXT_LAUDOS_CALOR_LAUDOS","Laudos");
define("TXT_LAUDOS_CALOR_CALOR","Calor");
define("TXT_LAUDOS_CALOR_CLIENTE","Cliente");
define("TXT_LAUDOS_CALOR_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_CALOR_ANO","Ano");
define("TXT_LAUDOS_CALOR_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_CALOR_MES","Mês");
define("TXT_LAUDOS_CALOR_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_CALOR_1","1");
define("TXT_LAUDOS_CALOR_JAN","JAN");
define("TXT_LAUDOS_CALOR_2","2");
define("TXT_LAUDOS_CALOR_FEV","FEV");
define("TXT_LAUDOS_CALOR_3","3");
define("TXT_LAUDOS_CALOR_MAR","MAR");
define("TXT_LAUDOS_CALOR_4","4");
define("TXT_LAUDOS_CALOR_ABR","ABR");
define("TXT_LAUDOS_CALOR_5","5");
define("TXT_LAUDOS_CALOR_MAI","MAI");
define("TXT_LAUDOS_CALOR_6","6");
define("TXT_LAUDOS_CALOR_JUN","JUN");
define("TXT_LAUDOS_CALOR_7","7");
define("TXT_LAUDOS_CALOR_JUL","JUL");
define("TXT_LAUDOS_CALOR_8","8");
define("TXT_LAUDOS_CALOR_AGO","AGO");
define("TXT_LAUDOS_CALOR_9","9");
define("TXT_LAUDOS_CALOR_SET","SET");
define("TXT_LAUDOS_CALOR_10","10");
define("TXT_LAUDOS_CALOR_OUT","OUT");
define("TXT_LAUDOS_CALOR_11","11");
define("TXT_LAUDOS_CALOR_NOV","NOV");
define("TXT_LAUDOS_CALOR_12","12");
define("TXT_LAUDOS_CALOR_DEZ","DEZ");
define("TXT_LAUDOS_CALOR_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_CALOR_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_CALOR_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_CALOR_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_CALOR_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_CALOR_ELABORACAO","Elaboração");
define("TXT_LAUDOS_CALOR_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_CALOR_AREA","Área");
define("TXT_LAUDOS_CALOR_SETOR","Setor");
define("TXT_LAUDOS_CALOR_GES","GES");
define("TXT_LAUDOS_CALOR_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_CALOR_CBO","CBO");
define("TXT_LAUDOS_CALOR_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_CALOR_ANALISES","Análises");
define("TXT_LAUDOS_CALOR_O_CAMPO_ANALISES_E_OBRIGATORIO","O campo ANÁLISES é obrigatório!");
define("TXT_LAUDOS_CALOR_1","1");
define("TXT_LAUDOS_CALOR_2","2");
define("TXT_LAUDOS_CALOR_3","3");
define("TXT_LAUDOS_CALOR_4","4");
define("TXT_LAUDOS_CALOR_5","5");
define("TXT_LAUDOS_CALOR_6","6");
define("TXT_LAUDOS_CALOR_ANALISE_1_AMOSTRA","Análise #1 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_1_PROCESSO_PRODUTIVO","Análise #1 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_1_OBS_PERTINENTE","Análise #1 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_ANALISE_2_AMOSTRA","Análise #2 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_2_PROCESSO_PRODUTIVO","Análise #2 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_2_OBS_PERTINENTE","Análise #2 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_ANALISE_3_AMOSTRA","Análise #3 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_3_PROCESSO_PRODUTIVO","Análise #3 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_3_OBS_PERTINENTE","Análise #3 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_ANALISE_4_AMOSTRA","Análise #4 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_4_PROCESSO_PRODUTIVO","Análise #4 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_4_OBS_PERTINENTE","Análise #4 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_ANALISE_5_AMOSTRA","Análise #5 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_5_PROCESSO_PRODUTIVO","Análise #5 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_5_OBS_PERTINENTE","Análise #5 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_ANALISE_6_AMOSTRA","Análise #6 - Amostra");
define("TXT_LAUDOS_CALOR_ANALISE_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_CALOR_ANALISE_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_CALOR_ANALISE_6_PROCESSO_PRODUTIVO","Análise #6 - Processo Produtivo");
define("TXT_LAUDOS_CALOR_ANALISE_6_OBS_PERTINENTE","Análise #6 - Obs Pertinente");
define("TXT_LAUDOS_CALOR_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_CALOR_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_CALOR_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_CALOR_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_CALOR_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_CALOR_EPC","EPC");
define("TXT_LAUDOS_CALOR_EPI","EPI");
define("TXT_LAUDOS_CALOR_DISPOSITIVO_1","Dispositivo #1");
define("TXT_LAUDOS_CALOR_DISPOSITIVO_2","Dispositivo #2");
define("TXT_LAUDOS_CALOR_LIMITE_DE_TOLERANCIA","Limite de Tolerância");
define("TXT_LAUDOS_CALOR_QUADRO_COMPARATIVO","Quadro Comparativo");
define("TXT_LAUDOS_CALOR_METABOLISMO","Metabolismo");
define("TXT_LAUDOS_CALOR_COLETAS","Coletas");
define("TXT_LAUDOS_CALOR_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_CALOR_COLETA_1_PASSO","Coleta #1 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_1_CARGA_SOLAR","Coleta #1 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_1_TIPO_DE_ATIVIDADE","Coleta #1 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_1_TEMPERATURA_DO_PASSO","Coleta #1 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_1_DESCRICAO_DA_TAREFA","Coleta #1 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_1_TBNM","Coleta #1 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_1_TBSM","Coleta #1 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_1_TGM","Coleta #1 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_1_UR","Coleta #1 - UR");
define("TXT_LAUDOS_CALOR_COLETA_1_VAR","Coleta #1 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_1_M","Coleta #1 - M");
define("TXT_LAUDOS_CALOR_COLETA_2_PASSO","Coleta #2 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_2_CARGA_SOLAR","Coleta #2 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_2_TIPO_DE_ATIVIDADE","Coleta #2 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_2_TEMPERATURA_DO_PASSO","Coleta #2 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_2_DESCRICAO_DA_TAREFA","Coleta #2 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_2_TBNM","Coleta #2 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_2_TBSM","Coleta #2 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_2_TGM","Coleta #2 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_2_UR","Coleta #2 - UR");
define("TXT_LAUDOS_CALOR_COLETA_2_VAR","Coleta #2 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_2_M","Coleta #2 - M");
define("TXT_LAUDOS_CALOR_COLETA_3_PASSO","Coleta #3 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_3_CARGA_SOLAR","Coleta #3 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_3_TIPO_DE_ATIVIDADE","Coleta #3 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_3_TEMPERATURA_DO_PASSO","Coleta #3 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_3_DESCRICAO_DA_TAREFA","Coleta #3 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_3_TBNM","Coleta #3 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_3_TBSM","Coleta #3 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_3_TGM","Coleta #3 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_3_UR","Coleta #3 - UR");
define("TXT_LAUDOS_CALOR_COLETA_3_VAR","Coleta #3 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_3_M","Coleta #3 - M");
define("TXT_LAUDOS_CALOR_COLETA_4_PASSO","Coleta #4 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_4_CARGA_SOLAR","Coleta #4 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_4_TIPO_DE_ATIVIDADE","Coleta #4 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_4_TEMPERATURA_DO_PASSO","Coleta #4 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_4_DESCRICAO_DA_TAREFA","Coleta #4 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_4_TBNM","Coleta #4 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_4_TBSM","Coleta #4 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_4_TGM","Coleta #4 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_4_UR","Coleta #4 - UR");
define("TXT_LAUDOS_CALOR_COLETA_4_VAR","Coleta #4 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_4_M","Coleta #4 - M");
define("TXT_LAUDOS_CALOR_COLETA_5_PASSO","Coleta #5 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_5_CARGA_SOLAR","Coleta #5 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_5_TIPO_DE_ATIVIDADE","Coleta #5 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_5_TEMPERATURA_DO_PASSO","Coleta #5 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_5_DESCRICAO_DA_TAREFA","Coleta #5 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_5_TBNM","Coleta #5 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_5_TBSM","Coleta #5 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_5_TGM","Coleta #5 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_5_UR","Coleta #5 - UR");
define("TXT_LAUDOS_CALOR_COLETA_5_VAR","Coleta #5 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_5_M","Coleta #5 - M");
define("TXT_LAUDOS_CALOR_COLETA_6_PASSO","Coleta #6 - Passo");
define("TXT_LAUDOS_CALOR_COLETA_6_CARGA_SOLAR","Coleta #6 - Carga Solar");
define("TXT_LAUDOS_CALOR_COLETA_6_TIPO_DE_ATIVIDADE","Coleta #6 - Tipo de Atividade");
define("TXT_LAUDOS_CALOR_COLETA_6_TEMPERATURA_DO_PASSO","Coleta #6 - Temperatura do Passo");
define("TXT_LAUDOS_CALOR_COLETA_6_DESCRICAO_DA_TAREFA","Coleta #6 - Descrição da Tarefa");
define("TXT_LAUDOS_CALOR_COLETA_6_TBNM","Coleta #6 - TBNM");
define("TXT_LAUDOS_CALOR_COLETA_6_TBSM","Coleta #6 - TBSM");
define("TXT_LAUDOS_CALOR_COLETA_6_TGM","Coleta #6 - TGM");
define("TXT_LAUDOS_CALOR_COLETA_6_UR","Coleta #6 - UR");
define("TXT_LAUDOS_CALOR_COLETA_6_VAR","Coleta #6 - VAR");
define("TXT_LAUDOS_CALOR_COLETA_6_M","Coleta #6 - M");
define("TXT_LAUDOS_CALOR_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_CALOR_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_CALOR_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_CALOR_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_CALOR_IMAGEM_ATIVIDADE_1","Imagem Atividade #1");
define("TXT_LAUDOS_CALOR_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_1_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade #1, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_CALOR_IMAGEM_ATIVIDADE_2","Imagem Atividade #2");
define("TXT_LAUDOS_CALOR_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_2_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade #2, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");

####
# view-laudos-part.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_PART","Particulado");
define("TXT_LAUDOS_PART_LAUDOS","Laudos");
define("TXT_LAUDOS_PART_PARTICULADO","Particulado");
define("TXT_LAUDOS_PART_CLIENTE","Cliente");
define("TXT_LAUDOS_PART_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_PART_ANO","Ano");
define("TXT_LAUDOS_PART_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_PART_MES","Mês");
define("TXT_LAUDOS_PART_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_PART_1","1");
define("TXT_LAUDOS_PART_JAN","JAN");
define("TXT_LAUDOS_PART_2","2");
define("TXT_LAUDOS_PART_FEV","FEV");
define("TXT_LAUDOS_PART_3","3");
define("TXT_LAUDOS_PART_MAR","MAR");
define("TXT_LAUDOS_PART_4","4");
define("TXT_LAUDOS_PART_ABR","ABR");
define("TXT_LAUDOS_PART_5","5");
define("TXT_LAUDOS_PART_MAI","MAI");
define("TXT_LAUDOS_PART_6","6");
define("TXT_LAUDOS_PART_JUN","JUN");
define("TXT_LAUDOS_PART_7","7");
define("TXT_LAUDOS_PART_JUL","JUL");
define("TXT_LAUDOS_PART_8","8");
define("TXT_LAUDOS_PART_AGO","AGO");
define("TXT_LAUDOS_PART_9","9");
define("TXT_LAUDOS_PART_SET","SET");
define("TXT_LAUDOS_PART_10","10");
define("TXT_LAUDOS_PART_OUT","OUT");
define("TXT_LAUDOS_PART_11","11");
define("TXT_LAUDOS_PART_NOV","NOV");
define("TXT_LAUDOS_PART_12","12");
define("TXT_LAUDOS_PART_DEZ","DEZ");
define("TXT_LAUDOS_PART_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_PART_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_PART_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_PART_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_PART_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_PART_ELABORACAO","Elaboração");
define("TXT_LAUDOS_PART_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_PART_AREA","Área");
define("TXT_LAUDOS_PART_SETOR","Setor");
define("TXT_LAUDOS_PART_GES","GES");
define("TXT_LAUDOS_PART_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_PART_CBO","CBO");
define("TXT_LAUDOS_PART_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_PART_ANALISES","Análises");
define("TXT_LAUDOS_PART_O_CAMPO_ANALISES_E_OBRIGATORIO","O campo ANÁLISES é obrigatório!");
define("TXT_LAUDOS_PART_ANALISE_1_AMOSTRA","Análise #1 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_1_PROCESSO_PRODUTIVO","Análise #1 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_1_OBS_TAREFA","Análise #1 - Obs Tarefa");
define("TXT_LAUDOS_PART_ANALISE_2_AMOSTRA","Análise #2 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_2_PROCESSO_PRODUTIVO","Análise #2 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_2_OBS_TAREFA","Análise #2 - Obs Tarefa");
define("TXT_LAUDOS_PART_ANALISE_3_AMOSTRA","Análise #3 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_3_PROCESSO_PRODUTIVO","Análise #3 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_3_OBS_TAREFA","Análise #3 - Obs Tarefa");
define("TXT_LAUDOS_PART_ANALISE_4_AMOSTRA","Análise #4 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_4_PROCESSO_PRODUTIVO","Análise #4 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_4_OBS_TAREFA","Análise #4 - Obs Tarefa");
define("TXT_LAUDOS_PART_ANALISE_5_AMOSTRA","Análise #5 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_5_PROCESSO_PRODUTIVO","Análise #5 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_5_OBS_TAREFA","Análise #5 - Obs Tarefa");
define("TXT_LAUDOS_PART_ANALISE_6_AMOSTRA","Análise #6 - Amostra");
define("TXT_LAUDOS_PART_ANALISE_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_PART_ANALISE_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_PART_ANALISE_6_PROCESSO_PRODUTIVO","Análise #6 - Processo Produtivo");
define("TXT_LAUDOS_PART_ANALISE_6_OBS_TAREFA","Análise #6 - Obs Tarefa");
define("TXT_LAUDOS_PART_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_PART_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_PART_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_PART_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_PART_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_PART_MITIGACAO","Mitigação");
define("TXT_LAUDOS_PART_COLETAS","Coletas");
define("TXT_LAUDOS_PART_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_PART_COLETA_1_AMOSTRA","Coleta #1 - Amostra");
define("TXT_LAUDOS_PART_COLETA_1_DATA","Coleta #1 - Data");
define("TXT_LAUDOS_PART_COLETA_1_N_SERIAL","Coleta #1 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_1_N_AMOSTRADOR","Coleta #1 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_1_N_RELATORIO_DE_ENSAIO","Coleta #1 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_1_VAZAO","Coleta #1 - Vazão");
define("TXT_LAUDOS_PART_COLETA_1_TEMPO_AMOSTRAGEM","Coleta #1 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_1_MASSA_1","Coleta #1 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_1_MASSA_2","Coleta #1 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_1_MASSA_3","Coleta #1 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_1_MASSA_4","Coleta #1 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_1_MASSA_5","Coleta #1 - Massa #5");
define("TXT_LAUDOS_PART_COLETA_2_AMOSTRA","Coleta #2 - Amostra");
define("TXT_LAUDOS_PART_COLETA_2_DATA","Coleta #2 - Data");
define("TXT_LAUDOS_PART_COLETA_2_N_SERIAL","Coleta #2 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_2_N_AMOSTRADOR","Coleta #2 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_2_N_RELATORIO_DE_ENSAIO","Coleta #2 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_2_VAZAO","Coleta #2 - Vazão");
define("TXT_LAUDOS_PART_COLETA_2_TEMPO_AMOSTRAGEM","Coleta #2 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_2_MASSA_1","Coleta #2 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_2_MASSA_2","Coleta #2 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_2_MASSA_3","Coleta #2 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_2_MASSA_4","Coleta #2 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_2_MASSA_5","Coleta #2 - Massa #5");
define("TXT_LAUDOS_PART_COLETA_3_AMOSTRA","Coleta #3 - Amostra");
define("TXT_LAUDOS_PART_COLETA_3_DATA","Coleta #3 - Data");
define("TXT_LAUDOS_PART_COLETA_3_N_SERIAL","Coleta #3 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_3_N_AMOSTRADOR","Coleta #3 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_3_N_RELATORIO_DE_ENSAIO","Coleta #3 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_3_VAZAO","Coleta #3 - Vazão");
define("TXT_LAUDOS_PART_COLETA_3_TEMPO_AMOSTRAGEM","Coleta #3 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_3_MASSA_1","Coleta #3 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_3_MASSA_2","Coleta #3 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_3_MASSA_3","Coleta #3 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_3_MASSA_4","Coleta #3 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_3_MASSA_5","Coleta #3 - Massa #5");
define("TXT_LAUDOS_PART_COLETA_4_AMOSTRA","Coleta #4 - Amostra");
define("TXT_LAUDOS_PART_COLETA_4_DATA","Coleta #4 - Data");
define("TXT_LAUDOS_PART_COLETA_4_N_SERIAL","Coleta #4 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_4_N_AMOSTRADOR","Coleta #4 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_4_N_RELATORIO_DE_ENSAIO","Coleta #4 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_4_VAZAO","Coleta #4 - Vazão");
define("TXT_LAUDOS_PART_COLETA_4_TEMPO_AMOSTRAGEM","Coleta #4 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_4_MASSA_1","Coleta #4 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_4_MASSA_2","Coleta #4 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_4_MASSA_3","Coleta #4 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_4_MASSA_4","Coleta #4 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_4_MASSA_5","Coleta #4 - Massa #5");
define("TXT_LAUDOS_PART_COLETA_5_AMOSTRA","Coleta #5 - Amostra");
define("TXT_LAUDOS_PART_COLETA_5_DATA","Coleta #5 - Data");
define("TXT_LAUDOS_PART_COLETA_5_N_SERIAL","Coleta #5 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_5_N_AMOSTRADOR","Coleta #5 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_5_N_RELATORIO_DE_ENSAIO","Coleta #5 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_5_VAZAO","Coleta #5 - Vazão");
define("TXT_LAUDOS_PART_COLETA_5_TEMPO_AMOSTRAGEM","Coleta #5 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_5_MASSA_1","Coleta #5 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_5_MASSA_2","Coleta #5 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_5_MASSA_3","Coleta #5 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_5_MASSA_4","Coleta #5 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_5_MASSA_5","Coleta #5 - Massa #5");
define("TXT_LAUDOS_PART_COLETA_6_AMOSTRA","Coleta #6 - Amostra");
define("TXT_LAUDOS_PART_COLETA_6_DATA","Coleta #6 - Data");
define("TXT_LAUDOS_PART_COLETA_6_N_SERIAL","Coleta #6 - Nº Serial");
define("TXT_LAUDOS_PART_COLETA_6_N_AMOSTRADOR","Coleta #6 - Nº Amostrador");
define("TXT_LAUDOS_PART_COLETA_6_N_RELATORIO_DE_ENSAIO","Coleta #6 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_PART_COLETA_6_VAZAO","Coleta #6 - Vazão");
define("TXT_LAUDOS_PART_COLETA_6_TEMPO_AMOSTRAGEM","Coleta #6 - Tempo Amostragem");
define("TXT_LAUDOS_PART_COLETA_6_MASSA_1","Coleta #6 - Massa #1");
define("TXT_LAUDOS_PART_COLETA_6_MASSA_2","Coleta #6 - Massa #2");
define("TXT_LAUDOS_PART_COLETA_6_MASSA_3","Coleta #6 - Massa #3");
define("TXT_LAUDOS_PART_COLETA_6_MASSA_4","Coleta #6 - Massa #4");
define("TXT_LAUDOS_PART_COLETA_6_MASSA_5","Coleta #6 - Massa #5");
define("TXT_LAUDOS_PART_RESPIRADOR","Respirador");
define("TXT_LAUDOS_PART_CERTIFICADO_DE_APROVACAO","Certificado de Aprovação");
define("TXT_LAUDOS_PART_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_PART_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_PART_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_PART_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_PART_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_PART_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_PART_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_PART_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-poeira.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_POEIRA","Poeira");
define("TXT_LAUDOS_POEIRA_LAUDOS","Laudos");
define("TXT_LAUDOS_POEIRA_POEIRA","Poeira");
define("TXT_LAUDOS_POEIRA_CLIENTE","Cliente");
define("TXT_LAUDOS_POEIRA_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_POEIRA_ANO","Ano");
define("TXT_LAUDOS_POEIRA_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_POEIRA_MES","Mês");
define("TXT_LAUDOS_POEIRA_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_POEIRA_1","1");
define("TXT_LAUDOS_POEIRA_JAN","JAN");
define("TXT_LAUDOS_POEIRA_2","2");
define("TXT_LAUDOS_POEIRA_FEV","FEV");
define("TXT_LAUDOS_POEIRA_3","3");
define("TXT_LAUDOS_POEIRA_MAR","MAR");
define("TXT_LAUDOS_POEIRA_4","4");
define("TXT_LAUDOS_POEIRA_ABR","ABR");
define("TXT_LAUDOS_POEIRA_5","5");
define("TXT_LAUDOS_POEIRA_MAI","MAI");
define("TXT_LAUDOS_POEIRA_6","6");
define("TXT_LAUDOS_POEIRA_JUN","JUN");
define("TXT_LAUDOS_POEIRA_7","7");
define("TXT_LAUDOS_POEIRA_JUL","JUL");
define("TXT_LAUDOS_POEIRA_8","8");
define("TXT_LAUDOS_POEIRA_AGO","AGO");
define("TXT_LAUDOS_POEIRA_9","9");
define("TXT_LAUDOS_POEIRA_SET","SET");
define("TXT_LAUDOS_POEIRA_10","10");
define("TXT_LAUDOS_POEIRA_OUT","OUT");
define("TXT_LAUDOS_POEIRA_11","11");
define("TXT_LAUDOS_POEIRA_NOV","NOV");
define("TXT_LAUDOS_POEIRA_12","12");
define("TXT_LAUDOS_POEIRA_DEZ","DEZ");
define("TXT_LAUDOS_POEIRA_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_POEIRA_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_POEIRA_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_POEIRA_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_POEIRA_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_POEIRA_ELABORACAO","Elaboração");
define("TXT_LAUDOS_POEIRA_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_POEIRA_AREA","Área");
define("TXT_LAUDOS_POEIRA_SETOR","Setor");
define("TXT_LAUDOS_POEIRA_GES","GES");
define("TXT_LAUDOS_POEIRA_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_POEIRA_CBO","CBO");
define("TXT_LAUDOS_POEIRA_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_POEIRA_ANALISES","Análises");
define("TXT_LAUDOS_POEIRA_O_CAMPO_ANALISES_E_OBRIGATORIO","O campo ANÁLISES é obrigatório!");
define("TXT_LAUDOS_POEIRA_ANALISE_1_AMOSTRA","Análise #1 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_1_PROCESSO_PRODUTIVO","Análise #1 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_1_OBS_TAREFA","Análise #1 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_ANALISE_2_AMOSTRA","Análise #2 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_2_PROCESSO_PRODUTIVO","Análise #2 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_2_OBS_TAREFA","Análise #2 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_ANALISE_3_AMOSTRA","Análise #3 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_3_PROCESSO_PRODUTIVO","Análise #3 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_3_OBS_TAREFA","Análise #3 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_ANALISE_4_AMOSTRA","Análise #4 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_4_PROCESSO_PRODUTIVO","Análise #4 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_4_OBS_TAREFA","Análise #4 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_ANALISE_5_AMOSTRA","Análise #5 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_5_PROCESSO_PRODUTIVO","Análise #5 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_5_OBS_TAREFA","Análise #5 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_ANALISE_6_AMOSTRA","Análise #6 - Amostra");
define("TXT_LAUDOS_POEIRA_ANALISE_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_POEIRA_ANALISE_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_POEIRA_ANALISE_6_PROCESSO_PRODUTIVO","Análise #6 - Processo Produtivo");
define("TXT_LAUDOS_POEIRA_ANALISE_6_OBS_TAREFA","Análise #6 - Obs Tarefa");
define("TXT_LAUDOS_POEIRA_AGENTE_DE_RISCO","Agente de Risco");
define("TXT_LAUDOS_POEIRA_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_POEIRA_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_POEIRA_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_POEIRA_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_POEIRA_MITIGACAO","Mitigação");
define("TXT_LAUDOS_POEIRA_COLETAS","Coletas");
define("TXT_LAUDOS_POEIRA_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_POEIRA_COLETA_1_AMOSTRA","Coleta #1 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_1_DATA","Coleta #1 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_1_N_SERIAL","Coleta #1 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_1_N_AMOSTRADOR","Coleta #1 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_1_RELATORIO_DE_ENSAIO","Coleta #1 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_1_VAZAO","Coleta #1 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_1_TEMPO_AMOSTRAGEM","Coleta #1 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_1_MASSA_1","Coleta #1 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_1_PESO_SIO2","Coleta #1 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_1_SIO2","Coleta #1 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_1_ACGIH","Coleta #1 - ACGIH");
define("TXT_LAUDOS_POEIRA_COLETA_2_AMOSTRA","Coleta #2 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_2_DATA","Coleta #2 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_2_N_SERIAL","Coleta #2 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_2_N_AMOSTRADOR","Coleta #2 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_2_RELATORIO_DE_ENSAIO","Coleta #2 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_2_VAZAO","Coleta #2 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_2_TEMPO_AMOSTRAGEM","Coleta #2 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_2_MASSA_1","Coleta #2 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_2_PESO_SIO2","Coleta #2 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_2_SIO2","Coleta #2 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_2_ACGIH","Coleta #2 - ACGIH");
define("TXT_LAUDOS_POEIRA_COLETA_3_AMOSTRA","Coleta #3 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_3_DATA","Coleta #3 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_3_N_SERIAL","Coleta #3 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_3_N_AMOSTRADOR","Coleta #3 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_3_RELATORIO_DE_ENSAIO","Coleta #3 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_3_VAZAO","Coleta #3 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_3_TEMPO_AMOSTRAGEM","Coleta #3 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_3_MASSA_1","Coleta #3 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_3_PESO_SIO2","Coleta #3 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_3_SIO2","Coleta #3 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_3_ACGIH","Coleta #3 - ACGIH");
define("TXT_LAUDOS_POEIRA_COLETA_4_AMOSTRA","Coleta #4 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_4_DATA","Coleta #4 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_4_N_SERIAL","Coleta #4 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_4_N_AMOSTRADOR","Coleta #4 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_4_RELATORIO_DE_ENSAIO","Coleta #4 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_4_VAZAO","Coleta #4 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_4_TEMPO_AMOSTRAGEM","Coleta #4 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_4_MASSA_1","Coleta #4 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_4_PESO_SIO2","Coleta #4 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_4_SIO2","Coleta #4 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_4_ACGIH","Coleta #4 - ACGIH");
define("TXT_LAUDOS_POEIRA_COLETA_5_AMOSTRA","Coleta #5 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_5_DATA","Coleta #5 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_5_N_SERIAL","Coleta #5 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_5_N_AMOSTRADOR","Coleta #5 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_5_RELATORIO_DE_ENSAIO","Coleta #5 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_5_VAZAO","Coleta #5 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_5_TEMPO_AMOSTRAGEM","Coleta #5 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_5_MASSA_1","Coleta #5 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_5_PESO_SIO2","Coleta #5 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_5_SIO2","Coleta #5 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_5_ACGIH","Coleta #5 - ACGIH");
define("TXT_LAUDOS_POEIRA_COLETA_6_AMOSTRA","Coleta #6 - Amostra");
define("TXT_LAUDOS_POEIRA_COLETA_6_DATA","Coleta #6 - Data");
define("TXT_LAUDOS_POEIRA_COLETA_6_N_SERIAL","Coleta #6 - Nº Serial");
define("TXT_LAUDOS_POEIRA_COLETA_6_N_AMOSTRADOR","Coleta #6 - Nº Amostrador");
define("TXT_LAUDOS_POEIRA_COLETA_6_RELATORIO_DE_ENSAIO","Coleta #6 - Relatório de Ensaio");
define("TXT_LAUDOS_POEIRA_COLETA_6_VAZAO","Coleta #6 - Vazão");
define("TXT_LAUDOS_POEIRA_COLETA_6_TEMPO_AMOSTRAGEM","Coleta #6 - Tempo Amostragem");
define("TXT_LAUDOS_POEIRA_COLETA_6_MASSA_1","Coleta #6 - Massa #1");
define("TXT_LAUDOS_POEIRA_COLETA_6_PESO_SIO2","Coleta #6 - Peso SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_6_SIO2","Coleta #6 - % SIO2");
define("TXT_LAUDOS_POEIRA_COLETA_6_ACGIH","Coleta #6 - ACGIH");
define("TXT_LAUDOS_POEIRA_RESPIRADOR","Respirador");
define("TXT_LAUDOS_POEIRA_CERTIFICADO_DE_APROVACAO","Certificado de Aprovação");
define("TXT_LAUDOS_POEIRA_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_POEIRA_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_POEIRA_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_POEIRA_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_POEIRA_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_POEIRA_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_POEIRA_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_POEIRA_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-rad.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_RAD","Radiação Ionizante");
define("TXT_LAUDOS_RAD_LAUDOS","Laudos");
define("TXT_LAUDOS_RAD_RADIACAO_IONIZANTE","Radiação Ionizante");
define("TXT_LAUDOS_RAD_CLIENTE","Cliente");
define("TXT_LAUDOS_RAD_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_RAD_ANO","Ano");
define("TXT_LAUDOS_RAD_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_RAD_MES","Mês");
define("TXT_LAUDOS_RAD_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_RAD_1","1");
define("TXT_LAUDOS_RAD_JAN","JAN");
define("TXT_LAUDOS_RAD_2","2");
define("TXT_LAUDOS_RAD_FEV","FEV");
define("TXT_LAUDOS_RAD_3","3");
define("TXT_LAUDOS_RAD_MAR","MAR");
define("TXT_LAUDOS_RAD_4","4");
define("TXT_LAUDOS_RAD_ABR","ABR");
define("TXT_LAUDOS_RAD_5","5");
define("TXT_LAUDOS_RAD_MAI","MAI");
define("TXT_LAUDOS_RAD_6","6");
define("TXT_LAUDOS_RAD_JUN","JUN");
define("TXT_LAUDOS_RAD_7","7");
define("TXT_LAUDOS_RAD_JUL","JUL");
define("TXT_LAUDOS_RAD_8","8");
define("TXT_LAUDOS_RAD_AGO","AGO");
define("TXT_LAUDOS_RAD_9","9");
define("TXT_LAUDOS_RAD_SET","SET");
define("TXT_LAUDOS_RAD_10","10");
define("TXT_LAUDOS_RAD_OUT","OUT");
define("TXT_LAUDOS_RAD_11","11");
define("TXT_LAUDOS_RAD_NOV","NOV");
define("TXT_LAUDOS_RAD_12","12");
define("TXT_LAUDOS_RAD_DEZ","DEZ");
define("TXT_LAUDOS_RAD_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_RAD_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_RAD_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_RAD_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_RAD_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_RAD_ELABORACAO","Elaboração");
define("TXT_LAUDOS_RAD_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_RAD_AREA","Área");
define("TXT_LAUDOS_RAD_SETOR","Setor");
define("TXT_LAUDOS_RAD_GES","GES");
define("TXT_LAUDOS_RAD_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_RAD_CBO","CBO");
define("TXT_LAUDOS_RAD_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_RAD_TAREFA_EXECUTADA","Tarefa Executada");
define("TXT_LAUDOS_RAD_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_RAD_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_RAD_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_RAD_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_RAD_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_RAD_EPI","EPI");
define("TXT_LAUDOS_RAD_EPC","EPC");
define("TXT_LAUDOS_RAD_ENQUADRAMENTO_NR16","Enquadramento NR16");
define("TXT_LAUDOS_RAD_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_RAD_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_RAD_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_RAD_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_RAD_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_RAD_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_RAD_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_RAD_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-vapor.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_VAPOR","Vapor");
define("TXT_LAUDOS_VAPOR_LAUDOS","Laudos");
define("TXT_LAUDOS_VAPOR_VAPOR","Vapor");
define("TXT_LAUDOS_VAPOR_CLIENTE","Cliente");
define("TXT_LAUDOS_VAPOR_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_VAPOR_ANO","Ano");
define("TXT_LAUDOS_VAPOR_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_VAPOR_MES","Mês");
define("TXT_LAUDOS_VAPOR_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_VAPOR_1","1");
define("TXT_LAUDOS_VAPOR_JAN","JAN");
define("TXT_LAUDOS_VAPOR_2","2");
define("TXT_LAUDOS_VAPOR_FEV","FEV");
define("TXT_LAUDOS_VAPOR_3","3");
define("TXT_LAUDOS_VAPOR_MAR","MAR");
define("TXT_LAUDOS_VAPOR_4","4");
define("TXT_LAUDOS_VAPOR_ABR","ABR");
define("TXT_LAUDOS_VAPOR_5","5");
define("TXT_LAUDOS_VAPOR_MAI","MAI");
define("TXT_LAUDOS_VAPOR_6","6");
define("TXT_LAUDOS_VAPOR_JUN","JUN");
define("TXT_LAUDOS_VAPOR_7","7");
define("TXT_LAUDOS_VAPOR_JUL","JUL");
define("TXT_LAUDOS_VAPOR_8","8");
define("TXT_LAUDOS_VAPOR_AGO","AGO");
define("TXT_LAUDOS_VAPOR_9","9");
define("TXT_LAUDOS_VAPOR_SET","SET");
define("TXT_LAUDOS_VAPOR_10","10");
define("TXT_LAUDOS_VAPOR_OUT","OUT");
define("TXT_LAUDOS_VAPOR_11","11");
define("TXT_LAUDOS_VAPOR_NOV","NOV");
define("TXT_LAUDOS_VAPOR_12","12");
define("TXT_LAUDOS_VAPOR_DEZ","DEZ");
define("TXT_LAUDOS_VAPOR_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_VAPOR_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_VAPOR_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_VAPOR_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_VAPOR_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_VAPOR_ELABORACAO","Elaboração");
define("TXT_LAUDOS_VAPOR_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_VAPOR_AREA","Área");
define("TXT_LAUDOS_VAPOR_SETOR","Setor");
define("TXT_LAUDOS_VAPOR_GES","GES");
define("TXT_LAUDOS_VAPOR_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_VAPOR_CBO","CBO");
define("TXT_LAUDOS_VAPOR_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_VAPOR_ANALISES","Análises");
define("TXT_LAUDOS_VAPOR_O_CAMPO_ANALISES_E_OBRIGATORIO","O campo ANÁLISES é obrigatório!");
define("TXT_LAUDOS_VAPOR_ANALISE_1_AMOSTRA","Análise #1 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_1_PROCESSO_PRODUTIVO","Análise #1 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_1_OBS_TAREFA","Análise #1 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_ANALISE_2_AMOSTRA","Análise #2 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_2_PROCESSO_PRODUTIVO","Análise #2 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_2_OBS_TAREFA","Análise #2 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_ANALISE_3_AMOSTRA","Análise #3 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_3_PROCESSO_PRODUTIVO","Análise #3 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_3_OBS_TAREFA","Análise #3 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_ANALISE_4_AMOSTRA","Análise #4 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_4_PROCESSO_PRODUTIVO","Análise #4 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_4_OBS_TAREFA","Análise #4 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_ANALISE_5_AMOSTRA","Análise #5 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_5_PROCESSO_PRODUTIVO","Análise #5 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_5_OBS_TAREFA","Análise #5 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_ANALISE_6_AMOSTRA","Análise #6 - Amostra");
define("TXT_LAUDOS_VAPOR_ANALISE_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_VAPOR_ANALISE_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_VAPOR_ANALISE_6_PROCESSO_PRODUTIVO","Análise #6 - Processo Produtivo");
define("TXT_LAUDOS_VAPOR_ANALISE_6_OBS_TAREFA","Análise #6 - Obs Tarefa");
define("TXT_LAUDOS_VAPOR_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_VAPOR_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_VAPOR_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_VAPOR_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_VAPOR_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_VAPOR_MITIGACAO","Mitigação");
define("TXT_LAUDOS_VAPOR_COLETAS","Coletas");
define("TXT_LAUDOS_VAPOR_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_VAPOR_COLETA_1_AMOSTRA","Coleta #1 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_1_DATA","Coleta #1 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_1_N_SERIAL","Coleta #1 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_1_N_AMOSTRADOR","Coleta #1 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_1_N_RELATORIO_DE_ENSAIO","Coleta #1 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_1_VAZAO","Coleta #1 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_1_TEMPO_AMOSTRAGEM","Coleta #1 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_1_MASSA_1","Coleta #1 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_1_MASSA_2","Coleta #1 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_1_MASSA_3","Coleta #1 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_1_MASSA_4","Coleta #1 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_1_MASSA_5","Coleta #1 - Massa #5");
define("TXT_LAUDOS_VAPOR_COLETA_2_AMOSTRA","Coleta #2 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_2_DATA","Coleta #2 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_2_N_SERIAL","Coleta #2 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_2_N_AMOSTRADOR","Coleta #2 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_2_N_RELATORIO_DE_ENSAIO","Coleta #2 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_2_VAZAO","Coleta #2 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_2_TEMPO_AMOSTRAGEM","Coleta #2 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_2_MASSA_1","Coleta #2 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_2_MASSA_2","Coleta #2 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_2_MASSA_3","Coleta #2 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_2_MASSA_4","Coleta #2 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_2_MASSA_5","Coleta #2 - Massa #5");
define("TXT_LAUDOS_VAPOR_COLETA_3_AMOSTRA","Coleta #3 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_3_DATA","Coleta #3 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_3_N_SERIAL","Coleta #3 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_3_N_AMOSTRADOR","Coleta #3 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_3_N_RELATORIO_DE_ENSAIO","Coleta #3 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_3_VAZAO","Coleta #3 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_3_TEMPO_AMOSTRAGEM","Coleta #3 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_3_MASSA_1","Coleta #3 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_3_MASSA_2","Coleta #3 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_3_MASSA_3","Coleta #3 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_3_MASSA_4","Coleta #3 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_3_MASSA_5","Coleta #3 - Massa #5");
define("TXT_LAUDOS_VAPOR_COLETA_4_AMOSTRA","Coleta #4 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_4_DATA","Coleta #4 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_4_N_SERIAL","Coleta #4 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_4_N_AMOSTRADOR","Coleta #4 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_4_N_RELATORIO_DE_ENSAIO","Coleta #4 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_4_VAZAO","Coleta #4 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_4_TEMPO_AMOSTRAGEM","Coleta #4 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_4_MASSA_1","Coleta #4 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_4_MASSA_2","Coleta #4 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_4_MASSA_3","Coleta #4 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_4_MASSA_4","Coleta #4 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_4_MASSA_5","Coleta #4 - Massa #5");
define("TXT_LAUDOS_VAPOR_COLETA_5_AMOSTRA","Coleta #5 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_5_DATA","Coleta #5 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_5_N_SERIAL","Coleta #5 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_5_N_AMOSTRADOR","Coleta #5 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_5_N_RELATORIO_DE_ENSAIO","Coleta #5 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_5_VAZAO","Coleta #5 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_5_TEMPO_AMOSTRAGEM","Coleta #5 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_5_MASSA_1","Coleta #5 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_5_MASSA_2","Coleta #5 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_5_MASSA_3","Coleta #5 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_5_MASSA_4","Coleta #5 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_5_MASSA_5","Coleta #5 - Massa #5");
define("TXT_LAUDOS_VAPOR_COLETA_6_AMOSTRA","Coleta #6 - Amostra");
define("TXT_LAUDOS_VAPOR_COLETA_6_DATA","Coleta #6 - Data");
define("TXT_LAUDOS_VAPOR_COLETA_6_N_SERIAL","Coleta #6 - Nº Serial");
define("TXT_LAUDOS_VAPOR_COLETA_6_N_AMOSTRADOR","Coleta #6 - Nº Amostrador");
define("TXT_LAUDOS_VAPOR_COLETA_6_N_RELATORIO_DE_ENSAIO","Coleta #6 - Nº Relatório de Ensaio");
define("TXT_LAUDOS_VAPOR_COLETA_6_VAZAO","Coleta #6 - Vazão");
define("TXT_LAUDOS_VAPOR_COLETA_6_TEMPO_AMOSTRAGEM","Coleta #6 - Tempo Amostragem");
define("TXT_LAUDOS_VAPOR_COLETA_6_MASSA_1","Coleta #6 - Massa #1");
define("TXT_LAUDOS_VAPOR_COLETA_6_MASSA_2","Coleta #6 - Massa #2");
define("TXT_LAUDOS_VAPOR_COLETA_6_MASSA_3","Coleta #6 - Massa #3");
define("TXT_LAUDOS_VAPOR_COLETA_6_MASSA_4","Coleta #6 - Massa #4");
define("TXT_LAUDOS_VAPOR_COLETA_6_MASSA_5","Coleta #6 - Massa #5");
define("TXT_LAUDOS_VAPOR_RESPIRADOR","Respirador");
define("TXT_LAUDOS_VAPOR_CERTIFICADO_DE_APROVACAO","Certificado de Aprovação");
define("TXT_LAUDOS_VAPOR_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_VAPOR_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_VAPOR_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_VAPOR_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_VAPOR_IMAGEM_ATIVIDADE","Imagem Atividade");
define("TXT_LAUDOS_VAPOR_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_VAPOR_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_VAPOR_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");

####
# view-laudos-vibr_vci.php
####
define("TXT_SYSTEM_SIDE_MENU_LAUDOS","Laudos");
define("TXT_SYSTEM_SIDE_MENU_LAUDOS_VIBR_VCI","Vibração VCI");
define("TXT_LAUDOS_VIBR_VCI_LAUDOS","Laudos");
define("TXT_LAUDOS_VIBR_VCI_VIBRACAO_VCI","Vibração VCI");
define("TXT_LAUDOS_VIBR_VCI_CLIENTE","Cliente");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_CLIENTE_E_OBRIGATORIO","O campo CLIENTE é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_ANO","Ano");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_MES","Mês");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_1","1");
define("TXT_LAUDOS_VIBR_VCI_JAN","JAN");
define("TXT_LAUDOS_VIBR_VCI_2","2");
define("TXT_LAUDOS_VIBR_VCI_FEV","FEV");
define("TXT_LAUDOS_VIBR_VCI_3","3");
define("TXT_LAUDOS_VIBR_VCI_MAR","MAR");
define("TXT_LAUDOS_VIBR_VCI_4","4");
define("TXT_LAUDOS_VIBR_VCI_ABR","ABR");
define("TXT_LAUDOS_VIBR_VCI_5","5");
define("TXT_LAUDOS_VIBR_VCI_MAI","MAI");
define("TXT_LAUDOS_VIBR_VCI_6","6");
define("TXT_LAUDOS_VIBR_VCI_JUN","JUN");
define("TXT_LAUDOS_VIBR_VCI_7","7");
define("TXT_LAUDOS_VIBR_VCI_JUL","JUL");
define("TXT_LAUDOS_VIBR_VCI_8","8");
define("TXT_LAUDOS_VIBR_VCI_AGO","AGO");
define("TXT_LAUDOS_VIBR_VCI_9","9");
define("TXT_LAUDOS_VIBR_VCI_SET","SET");
define("TXT_LAUDOS_VIBR_VCI_10","10");
define("TXT_LAUDOS_VIBR_VCI_OUT","OUT");
define("TXT_LAUDOS_VIBR_VCI_11","11");
define("TXT_LAUDOS_VIBR_VCI_NOV","NOV");
define("TXT_LAUDOS_VIBR_VCI_12","12");
define("TXT_LAUDOS_VIBR_VCI_DEZ","DEZ");
define("TXT_LAUDOS_VIBR_VCI_N_PLANILHA","Nº Planilha");
define("TXT_LAUDOS_VIBR_VCI_NUMERO_PLANILHA","Número Planilha");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_NUMERO_PLANILHA_E_OBRIGATORIO","O campo NÚMERO PLANILHA é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_UNIDADESITE","Unidade/Site");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_UNIDADESITE_E_OBRIGATORIO","O campo UNIDADE/SITE é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_ELABORACAO","Elaboração");
define("TXT_LAUDOS_VIBR_VCI_DATA_ELABORACAO","Data Elaboração");
define("TXT_LAUDOS_VIBR_VCI_AREA","Área");
define("TXT_LAUDOS_VIBR_VCI_SETOR","Setor");
define("TXT_LAUDOS_VIBR_VCI_GES","GES");
define("TXT_LAUDOS_VIBR_VCI_CARGOFUNCAO","Cargo/Função");
define("TXT_LAUDOS_VIBR_VCI_CBO","CBO");
define("TXT_LAUDOS_VIBR_VCI_ATIVIDADE_MACRO","Atividade Macro");
define("TXT_LAUDOS_VIBR_VCI_ANALISES","Análises");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_ANALISES_E_OBRIGATORIO","O campo ANÁLISES é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_1_AMOSTRA","Análise #1 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_1_DATA_AMOSTRAGEM","Análise #1 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_1_PARADIGMA","Análise #1 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_1_TAREFA_EXECUTADA","Análise #1 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_2_AMOSTRA","Análise #2 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_2_DATA_AMOSTRAGEM","Análise #2 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_2_PARADIGMA","Análise #2 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_2_TAREFA_EXECUTADA","Análise #2 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_3_AMOSTRA","Análise #3 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_3_DATA_AMOSTRAGEM","Análise #3 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_3_PARADIGMA","Análise #3 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_3_TAREFA_EXECUTADA","Análise #3 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_4_AMOSTRA","Análise #4 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_4_DATA_AMOSTRAGEM","Análise #4 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_4_PARADIGMA","Análise #4 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_4_TAREFA_EXECUTADA","Análise #4 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_5_AMOSTRA","Análise #5 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_5_DATA_AMOSTRAGEM","Análise #5 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_5_PARADIGMA","Análise #5 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_5_TAREFA_EXECUTADA","Análise #5 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_6_AMOSTRA","Análise #6 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_6_DATA_AMOSTRAGEM","Análise #6 - Data Amostragem");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_6_PARADIGMA","Análise #6 - Paradigma");
define("TXT_LAUDOS_VIBR_VCI_ANALISE_6_TAREFA_EXECUTADA","Análise #6 - Tarefa Executada");
define("TXT_LAUDOS_VIBR_VCI_AGENTE_DE_RISCO","Agente de Risco");
define("TXT_LAUDOS_VIBR_VCI_JORNADA_DE_TRABALHO","Jornada de Trabalho");
define("TXT_LAUDOS_VIBR_VCI_TEMPO_EXPOSICAO","Tempo Exposição");
define("TXT_LAUDOS_VIBR_VCI_TIPO_EXPOSICAO","Tipo Exposição");
define("TXT_LAUDOS_VIBR_VCI_MEIO_DE_PROPAGACAO","Meio de Propagação");
define("TXT_LAUDOS_VIBR_VCI_FONTE_GERADORA","Fonte Geradora");
define("TXT_LAUDOS_VIBR_VCI_EPC","EPC");
define("TXT_LAUDOS_VIBR_VCI_FABRICANTE_EPI","Fabricante EPI");
define("TXT_LAUDOS_VIBR_VCI_MODELO_EPI","Modelo EPI");
define("TXT_LAUDOS_VIBR_VCI_CERTIFICADO_DE_APROVACAO","Certificado de Aprovação");
define("TXT_LAUDOS_VIBR_VCI_GRAU_DE_ATENUACAO","Grau de Atenuação");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1","Acelerômetro #1");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_MARCA","Acelerômetro #1 - Marca");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_MODELO","Acelerômetro #1 - Modelo");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_N_SERIAL","Acelerômetro #1 - Nº Serial");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_1_CERT_DE_CALIBRACAO","Acelerômetro #1 - Cert. de Calibração");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2","Acelerômetro #2");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_MARCA","Acelerômetro #2 - Marca");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_MODELO","Acelerômetro #2 - Modelo");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_N_SERIAL","Acelerômetro #2 - Nº Serial");
define("TXT_LAUDOS_VIBR_VCI_ACELEROMETRO_2_CERT_DE_CALIBRACAO","Acelerômetro #2 - Cert. de Calibração");
define("TXT_LAUDOS_VIBR_VCI_COLETAS","Coletas");
define("TXT_LAUDOS_VIBR_VCI_O_CAMPO_COLETAS_E_OBRIGATORIO","O campo COLETAS é obrigatório!");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_AMOSTRA","Coleta #1 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_DATA","Coleta #1 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_DURACAO","Coleta #1 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_X","Coleta #1 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_Y","Coleta #1 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_ACELERACAO_Z","Coleta #1 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_X","Coleta #1 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_Y","Coleta #1 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_VDV_Z","Coleta #1 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_1_FATOR_CRISTA","Coleta #1 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_AMOSTRA","Coleta #2 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_DATA","Coleta #2 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_DURACAO","Coleta #2 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_X","Coleta #2 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_Y","Coleta #2 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_ACELERACAO_Z","Coleta #2 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_X","Coleta #2 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_Y","Coleta #2 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_VDV_Z","Coleta #2 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_2_FATOR_CRISTA","Coleta #2 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_AMOSTRA","Coleta #3 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_DATA","Coleta #3 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_DURACAO","Coleta #3 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_X","Coleta #3 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_Y","Coleta #3 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_ACELERACAO_Z","Coleta #3 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_X","Coleta #3 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_Y","Coleta #3 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_VDV_Z","Coleta #3 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_3_FATOR_CRISTA","Coleta #3 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_AMOSTRA","Coleta #4 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_DATA","Coleta #4 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_DURACAO","Coleta #4 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_X","Coleta #4 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_Y","Coleta #4 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_ACELERACAO_Z","Coleta #4 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_X","Coleta #4 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_Y","Coleta #4 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_VDV_Z","Coleta #4 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_4_FATOR_CRISTA","Coleta #4 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_AMOSTRA","Coleta #5 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_DATA","Coleta #5 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_DURACAO","Coleta #5 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_X","Coleta #5 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_Y","Coleta #5 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_ACELERACAO_Z","Coleta #5 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_X","Coleta #5 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_Y","Coleta #5 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_VDV_Z","Coleta #5 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_5_FATOR_CRISTA","Coleta #5 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_AMOSTRA","Coleta #6 - Amostra");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_DATA","Coleta #6 - Data");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_DURACAO","Coleta #6 - Duração");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_X","Coleta #6 - Aceleração X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_Y","Coleta #6 - Aceleração Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_ACELERACAO_Z","Coleta #6 - Aceleração Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_X","Coleta #6 - VDV X");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_Y","Coleta #6 - VDV Y");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_VDV_Z","Coleta #6 - VDV Z");
define("TXT_LAUDOS_VIBR_VCI_COLETA_6_FATOR_CRISTA","Coleta #6 - Fator Crista");
define("TXT_LAUDOS_VIBR_VCI_TEMPO_DE_MEDICAO","Tempo de Medição");
define("TXT_LAUDOS_VIBR_VCI_NR09_AREN","NR09 AREN");
define("TXT_LAUDOS_VIBR_VCI_NR09_VDVR","NR09 VDVR");
define("TXT_LAUDOS_VIBR_VCI_NR15_AREN","NR15 AREN");
define("TXT_LAUDOS_VIBR_VCI_NR15_VDVR","NR15 VDVR");
define("TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_CAMPO","Responsável Campo");
define("TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_TECNICO","Responsável Técnico");
define("TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_CAMPO_REGISTRO","Responsável Campo - Registro");
define("TXT_LAUDOS_VIBR_VCI_RESPONSAVEL_TECNICO_REGISTRO","Responsável Técnico - Registro");
define("TXT_LAUDOS_VIBR_VCI_IMAGEM_ATIVIDADE_1","Imagem Atividade #1");
define("TXT_LAUDOS_VIBR_VCI_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_1_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade #1, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_VIBR_VCI_IMAGEM_ATIVIDADE_2","Imagem Atividade #2");
define("TXT_LAUDOS_VIBR_VCI_PARA_INCLUIR_UMA_IMAGEM_DA_ATIVIDADE_2_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_2MB","Para incluir uma Imagem da Atividade #2, anexe um arquivo jpg, jpeg, png ou gif de até 2mb.");
define("TXT_LAUDOS_VIBR_VCI_IMAGEM_LOGOMARCA","Imagem Logomarca");
define("TXT_LAUDOS_VIBR_VCI_PARA_INCLUIR_UMA_LOGOMARCA_ANEXE_UM_ARQUIVO_JPG_JPEG_PNG_OU_GIF_DE_ATE_80K","Para incluir uma Logomarca, anexe um arquivo jpg, jpeg, png ou gif de até 80k.");


####
# view-painel-dashboard.php
####
define("TXT_PAINEL_DASHBOARD_PAINEL_DE_CONTROLE","Painel de Controle");
define("TXT_PAINEL_DASHBOARD_DASHBOARD","Dashboard");
define("TXT_PAINEL_DASHBOARD_PAINEL_DETALHES","Detalhes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_COLABORADORES","Colaboradores");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_DISPOSITIVOS","Dispositivos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_CLIENTES","Clientes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_CONTRATOS","Contratos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_MUDAS","Variedades de Muda");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_ESPECIES","Espécies");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_DOENCAS","Doenças");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_OBJETIVOS","Objetivos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_PONTOS_DE_CONTROLE","Pontos de Controle");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_ESTAGIOS","Estágios");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES","Lotes");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES_CONCLUIDOS","Lotes Concluídos");
define("TXT_PAINEL_DASHBOARD_TOTAL_DE_LOTES_EM_PRODUCAO","Lotes em Produção");


####
# view-relat-prod.php
####
define("TXT_SYSTEM_SIDE_MENU_RELATORIOS","Relatórios");
define("TXT_SYSTEM_SIDE_MENU_RELAT_PROD","Produção");
define("TXT_RELAT_PROD_RELATORIOS","Relatórios");
define("TXT_RELAT_PROD_PRODUCAO","Produção");
define("TXT_RELAT_PROD_PRODUCAO_GUIA_INVENTARIO","Guia de Inventário de Lotes para informe de quantidade");
//Guia de Inventario
define("TXT_RELAT_PROD_R1_LOTE","Lote");
define("TXT_RELAT_PROD_R1_OBJETIVO","Objetivo");
define("TXT_RELAT_PROD_R1_MUDA","Muda");
define("TXT_RELAT_PROD_R1_DATA","Data");
define("TXT_RELAT_PROD_R1_HORA_INICIO","Hora Início");
define("TXT_RELAT_PROD_R1_HORA_FIM","Hora Fim");
define("TXT_RELAT_PROD_R1_QTDE","Qtde");

####
# view-relat-etiquetas.php
####
//define("TXT_SYSTEM_SIDE_MENU_RELATORIOS","Relatórios");
define("TXT_SYSTEM_SIDE_MENU_RELAT_ETIQUETAS","Etiquetas QR");
define("TXT_RELAT_ETIQUETAS_RELATORIOS","Relatórios");
define("TXT_RELAT_ETIQUETAS_ETIQUETAS","Etiquetas QR");
define("TXT_RELAT_ETIQUETAS_LOTE","Lote");
define("TXT_RELAT_ETIQUETAS_ETIQUETA","Etiqueta");
define("TXT_RELAT_ETIQUETAS_IMPRESSAO","Impressão");
define("TXT_RELAT_ETIQUETAS_QTDE_IMPRESSAO","Qtde para Impressão");
define("TXT_RELAT_ETIQUETAS_QTDE_IMPRESSAO_TOOLTIP","Quantidade de etiquetas que serão impressas!");
define("TXT_RELAT_ETIQUETAS_QTDE_CODIFICADA","Qtde Codificada");
define("TXT_RELAT_ETIQUETAS_QTDE_CODIFICADA_TOOLTIP","Quantidade de mudas que a etiqueta representa. Ex.: 1 = Um tubete, 36 = Uma bandeija, etc");
define("TXT_RELAT_ETIQUETAS_IMPRESSAO_COLUNA","Começar na Coluna");
define("TXT_RELAT_ETIQUETAS_IMPRESSAO_COLUNA_TOOLTIP","Indique o número da coluna da etiqueta de onde a impressão será iniciada.");
define("TXT_RELAT_ETIQUETAS_IMPRESSAO_LINHA","Começar na Linha");
define("TXT_RELAT_ETIQUETAS_IMPRESSAO_LINHA_TOOLTIP","Indique o número da linha da etiqueta de onde a impressão será iniciada.");
define("TXT_RELAT_ETIQUETAS_BORDA","Bordas");

####
# view-cfg-planilha_modelo.php
####
define("TXT_SYSTEM_SIDE_MENU_CONFIGURACAO","Configuração");
define("TXT_SYSTEM_SIDE_MENU_CFG_PLANILHA_MODELO","Planilha Modelo");
define("TXT_CFG_PLANILHA_MODELO_CONFIGURACAO","Configuração");
define("TXT_CFG_PLANILHA_MODELO_PLANILHA_MODELO","Planilha Modelo");
define("TXT_CFG_PLANILHA_MODELO_ANO","Ano");
define("TXT_CFG_PLANILHA_MODELO_O_CAMPO_ANO_E_OBRIGATORIO","O campo ANO é obrigatório!");
define("TXT_CFG_PLANILHA_MODELO_MES","Mês");
define("TXT_CFG_PLANILHA_MODELO_O_CAMPO_MES_E_OBRIGATORIO","O campo MÊS é obrigatório!");
define("TXT_CFG_PLANILHA_MODELO_1","1");
define("TXT_CFG_PLANILHA_MODELO_JAN","JAN");
define("TXT_CFG_PLANILHA_MODELO_2","2");
define("TXT_CFG_PLANILHA_MODELO_FEV","FEV");
define("TXT_CFG_PLANILHA_MODELO_3","3");
define("TXT_CFG_PLANILHA_MODELO_MAR","MAR");
define("TXT_CFG_PLANILHA_MODELO_4","4");
define("TXT_CFG_PLANILHA_MODELO_ABR","ABR");
define("TXT_CFG_PLANILHA_MODELO_5","5");
define("TXT_CFG_PLANILHA_MODELO_MAI","MAI");
define("TXT_CFG_PLANILHA_MODELO_6","6");
define("TXT_CFG_PLANILHA_MODELO_JUN","JUN");
define("TXT_CFG_PLANILHA_MODELO_7","7");
define("TXT_CFG_PLANILHA_MODELO_JUL","JUL");
define("TXT_CFG_PLANILHA_MODELO_8","8");
define("TXT_CFG_PLANILHA_MODELO_AGO","AGO");
define("TXT_CFG_PLANILHA_MODELO_9","9");
define("TXT_CFG_PLANILHA_MODELO_SET","SET");
define("TXT_CFG_PLANILHA_MODELO_10","10");
define("TXT_CFG_PLANILHA_MODELO_OUT","OUT");
define("TXT_CFG_PLANILHA_MODELO_11","11");
define("TXT_CFG_PLANILHA_MODELO_NOV","NOV");
define("TXT_CFG_PLANILHA_MODELO_12","12");
define("TXT_CFG_PLANILHA_MODELO_DEZ","DEZ");
define("TXT_CFG_PLANILHA_MODELO_LAUDO","Laudo");
define("TXT_CFG_PLANILHA_MODELO_O_CAMPO_LAUDO_E_OBRIGATORIO","O campo LAUDO é obrigatório!");
define("TXT_CFG_PLANILHA_MODELO_RISCO","risco");
define("TXT_CFG_PLANILHA_MODELO_ANALISE_DE_RISCO","Análise de Risco");
define("TXT_CFG_PLANILHA_MODELO_BIO","bio");
define("TXT_CFG_PLANILHA_MODELO_BIOLOGICO","Biológico");
define("TXT_CFG_PLANILHA_MODELO_CALOR","Calor");
define("TXT_CFG_PLANILHA_MODELO_ELETR","eletr");
define("TXT_CFG_PLANILHA_MODELO_ELETRICIDADE","Eletricidade");
define("TXT_CFG_PLANILHA_MODELO_EXPL","expl");
define("TXT_CFG_PLANILHA_MODELO_EXPLOSIVO","Explosivo");
define("TXT_CFG_PLANILHA_MODELO_INFL","infl");
define("TXT_CFG_PLANILHA_MODELO_INFLAMAVEL","Inflamável");
define("TXT_CFG_PLANILHA_MODELO_PART","part");
define("TXT_CFG_PLANILHA_MODELO_PARTICULADO","Particulado");
define("TXT_CFG_PLANILHA_MODELO_POEI","poei");
define("TXT_CFG_PLANILHA_MODELO_POEIRA","Poeira");
define("TXT_CFG_PLANILHA_MODELO_RAD","rad");
define("TXT_CFG_PLANILHA_MODELO_RADIACAO","Radiação Ionizante");
define("TXT_CFG_PLANILHA_MODELO_RUI","rui");
define("TXT_CFG_PLANILHA_MODELO_RUIDO","Ruído");
define("TXT_CFG_PLANILHA_MODELO_VAP","vap");
define("TXT_CFG_PLANILHA_MODELO_VAPORES","Vapores");
define("TXT_CFG_PLANILHA_MODELO_VBRVCI","vbrvci");
define("TXT_CFG_PLANILHA_MODELO_VIBRACAO_VCI","Vibração VCI");
define("TXT_CFG_PLANILHA_MODELO_VBRVMB","vbrvmb");
define("TXT_CFG_PLANILHA_MODELO_VIBRACAO_VMB","Vibração VMB");
define("TXT_CFG_PLANILHA_MODELO_ARQUIVO","Arquivo");
define("TXT_CFG_PLANILHA_MODELO_ARQUIVOENVIAR_UMA_PLANILHA_EXCEL_DE_NO_MAXIMO_3MB_ELA_SERA_O_MODELO_QUE_SERA_USADO_QUANDO_A_EMISSAO_DO_LAUDO_FOR_GERADO","Arquivo:Enviar uma planilha excel de no máximo 3mb. Ela será o modelo que será usado quando a emissão do laudo for gerado.");


####
# app.php
####
define("TXT_APP_KEY_NOT_FOUND","Chave de Dispositivo inválida! Favor reconfigurar o dispositivo com os dados QR de configuração do Portal Prodfy Plantas.");
define("TXT_APP_SETUP_DATA_NOT_FOUND","Dados de configuração não encontrados. Favor tentar novamente.");
define("TXT_APP_SINCRONIA_CONCLUIDA_COM_SUCESSO","Sincronia concluída com sucesso!");


#################################################################################
###########
#####
##
