<?php
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: db_connect.php
## Função..........: Estabelece conexao com o banco de dados
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## START
#################################################################################

	include_once 'config.php';
	
	####
	# Define protocolo de requisicao web
	####
	function check_https_lock()
	{
		if(LOCK_HTTPS_ACCESS)
		{
			if(!$_SERVER['HTTPS'])
			{
				$tmp = WEB_ADDRESS;
				str_replace("http:", "https:", $tmp);
			}
		}
	}
	
	####
	# Testa a string contra hacking
	####
	function test_input($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		
		#return utf8_decode($data);
		return $data;
	}
	
	####
	# Obtem locale do idioma
	####
	function get_locale($lang)
	{
		switch(strtoupper($lang))
		{
			case "BR":
				return "pt-br";
				break;
			case "US":
				return "en-us";
				break;
			case "SP":
				return "es-sp";
			default:
				return "pt-br";
		}
	}
	
	####
	# Carrega idioma da linguagem indicada
	####
	function carrega_language($lang)
	{
		if(empty($lang)){ $lang = "BR"; }
		$locale = get_locale($lang);
		if(empty($locale)){ $locale="pt-br"; }
		include_once LANGUAGE_FOLDER_PATH."/".$locale."/language.php";
	}
	
	####
	# Transforma em array dados de um arquivo ini
	####
	class ArrayINI implements ArrayAccess, IteratorAggregate 
	{
		private $lang;
		
		public function __construct($ini) 
		{
			$this->lang = parse_ini_file($ini);
		}
		
		function __invoke($offset) 
		{
			return $this->offsetGet($offset);
		}
		
		public function getIterator() 
		{
			return new ArrayIterator($this->lang);
		}
		
		public function offsetSet($offset, $value) 
		{
			if (is_null($offset)) 
			{
				$this->lang[] = $value;
			} else {
				$this->lang[$offset] = $value;
			}
		}
		
		public function offsetExists($offset) 
		{
			return isset($this->lang[$offset]);
		}
		
		public function offsetUnset($offset) 
		{
			unset($this->lang[$offset]);
		}
		
		public function offsetGet($offset) 
		{
			return isset($this->lang[$offset]) ? $this->lang[$offset] : null;
		}
	}
	
	####
	# Formata moeda
	####
	function formata_moeda($locale,$valor)
	{
		setlocale(LC_MONETARY, $locale);
		$tmp1 = money_format('%n', $valor);
		$tmp2 = explode(" ",$tmp1);
		return $tmp2[1];
	}
	
	####
	# Remove acentos da string
	####
	function remove_accent($str) 
	{ 
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		return str_replace($a, $b, $str); 
	}
	
	####
	# Gera um token aleatorio de identificacao
	####
	function gera_token()
	{
		$c = array("b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z");
		$v = array("a","e","i","o","u");
		$n = array("0","1","2","3","4","5","6","7","8","9");
		for($i=1; $i<=8; $i+=1)
		{
			$tmp .= $c[mt_rand(0,sizeof($c)-1)].$n[mt_rand(0,sizeof($n)-1)].$v[mt_rand(0,sizeof($v)-1)].$n[mt_rand(0,sizeof($n)-1)];
		}#endfor
		$tmp .= time();
		return $tmp;
	}
	
	####
	# Processa template, substituindo as macros pelo texto fornecido no array
	####
	function get_template_text($_path, $_file, $_array, $_language)
	{
		if(empty($_language)){ $_language = 'pt-br'; }
		
		if(empty($_path) || empty($_file) || empty($_array))
		{
			return;
		}
		else
		{
			##Formata arquivo final
			$_tmpl_file = $_path.'/'.$_file.'_'.$_language.'.tt';
			
			##Checa se o arquivo de template existe e carrega matriz
			if(!file_exists($_tmpl_file))
			{
				return;
			}
			else
			{
				##Carrega conteudo do template para processamento
				$tmp = file_get_contents($_tmpl_file);
				
				##Processa dados
				$tmp = str_replace(array_keys($_array), array_values($_array), $tmp);
				
				return $tmp;
			}
		}
		
	}
	
	####
	# Gera uma string randomica
	####
	function generateRandomString($length = 256) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	####
	# Inicia uma sessao segura
	####
	function sec_session_start() 
	{
		$session_name = SID_NAME;// Estabeleça um nome personalizado para a sessão
		$secure       = SECURE;
		// Isso impede que o JavaScript possa acessar a identificação da sessão.
		$httponly     = true;
		// Assim você força a sessão a usar apenas cookies. 
		if (ini_set('session.use_only_cookies', 1) === FALSE) 
		{
			header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
			exit();
		}
		// Obtém params de cookies atualizados.
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams["lifetime"],
				$cookieParams["path"], 
				$cookieParams["domain"], 
				$secure,
				$httponly);
		// Estabelece o nome fornecido acima como o nome da sessão.
		session_name($session_name);
		session_start();						// Inicia a sessão PHP 
		session_regenerate_id();		// Recupera a sessão e deleta a anterior. 
	}
	
	####
	# Login - Verifica se username e senha conferem com o banco de dados
	####
#	function login($email, $password, $mysqli) 
#	{
#		// Usando definições pré-estabelecidas significa que a injeção de SQL (um tipo de ataque) não é possível.
#		if ($stmt = $mysqli->prepare("SELECT id, username, password, salt 
#				FROM members
#			 WHERE email = ?
#				LIMIT 1")) {
#				$stmt->bind_param('s', $email);	// Relaciona	"$email" ao parâmetro.
#				$stmt->execute();		// Executa a tarefa estabelecida.
#				$stmt->store_result();
# 
#				// obtém variáveis a partir dos resultados. 
#				$stmt->bind_result($user_id, $username, $db_password, $salt);
#				$stmt->fetch();
# 
#				// faz o hash da senha com um salt excusivo.
#				$password = hash('sha512', $password . $salt);
#				if ($stmt->num_rows == 1) {
#						// Caso o usuário exista, conferimos se a conta está bloqueada
#						// devido ao limite de tentativas de login ter sido ultrapassado 
# 
#						if (checkbrute($user_id, $mysqli) == true) {
#								// A conta está bloqueada 
#								// Envia um email ao usuário informando que a conta está bloqueada 
#								retorna false (falso);
#						} else {
#								// Verifica se a senha confere com o que consta no banco de dados
#								// a senha do usuário é enviada.
#								if ($db_password == $password) {
#										// A senha está correta!
#										// Obtém o string usuário-agente do usuário. 
#										$user_browser = $_SERVER['HTTP_USER_AGENT'];
#										// proteção XSS conforme imprimimos este valor
#										$user_id = preg_replace("/[^0-9]+/", "", $user_id);
#										$_SESSION['user_id'] = $user_id;
#										// proteção XSS conforme imprimimos este valor 
#										$username = preg_replace("/[^a-zA-Z0-9_\-]+/", 
#																																"", 
#																																$username);
#										$_SESSION['username'] = $username;
#										$_SESSION['login_string'] = hash('sha512', 
#															$password . $user_browser);
#										// Login concluído com sucesso.
#										return true;
#								} else {
#										// A senha não está correta
#										// Registramos essa tentativa no banco de dados
#										$now = time();
#										$mysqli->query("INSERT INTO login_attempts(user_id, time)
#																		VALUES ('$user_id', '$now')");
#										return false;
#								}
#						}
#				} else {
#						// Tal usuário não existe.
#						return false;
#				}
#		}
#	}

#################################################################################
###########
#####
##
