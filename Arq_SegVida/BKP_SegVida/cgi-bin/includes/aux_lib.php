<?php
#################################################################################
## KONDOO - AUXLIB
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: aux_lib.php
## Função..........: Biblioteca auxiliar de funcoes do sistema
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## START
#################################################################################

	include_once 'config.php';
	
	####
	# Define protocolo de requisicao web
	####
	function check_https_lock()
	{
		if(LOCK_HTTPS_ACCESS)
		{
			if(!$_SERVER['HTTPS'])
			{
				$tmp = WEB_ADDRESS;
				str_replace("http:", "https:", $tmp);
			}
		}
	}
	
	####
	# Testa a string contra hacking
	####
	function test_input($data)
	{
		$data = trim($data);
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		
		#return utf8_decode($data);
		return $data;
	}
	
	####
	# Obtem locale do idioma
	####
	function get_locale($lang)
	{
		switch(strtoupper($lang))
		{
			case "BR":
				return "pt-br";
				break;
			case "US":
				return "en-us";
				break;
			case "SP":
				return "es-sp";
			default:
				return "pt-br";
		}
	}
	
	####
	# Verifica se a variavel está vazia, considera o valor 0 dentro dela como sendo conteudo válido
	####
	function isEmpty($_str)
	{
		if( empty($_str) )
		{
			if($_str == 0){ return false; } else { return true; }
		}
		return false;
	}
	
	####
	# Carrega idioma da linguagem indicada
	####
	function carrega_language($lang)
	{
		if(empty($lang)){ $lang = "BR"; }
		$locale = get_locale($lang);
		if(empty($locale)){ $locale="pt-br"; }
		include_once LANGUAGE_FOLDER_PATH."/".$locale."/language.php";
	}
	
	####
	# Valida idioma
	####
	function valida_idioma($idioma)
	{
		switch($idioma)
		{
			case 'pt-br':
			case 'PT-BR':
				return "pt-br";
				break;
			case 'en-us':
			case 'EN-US':
				return "en-us";
				break;
			case 'es-es':
			case 'ES-ES':
				return "es-es";
				break;
			default:
				return "pt-br";
				break;
		}
	}
	
	####
	# Carrega idioma
	####
	function carrega_idioma($idioma)
	{
		switch($idioma)
		{
			case 'pt-br':
				include_once LANGUAGE_FOLDER_PATH."/pt-br/language.php";
				break;
			case 'en-us':
				include_once LANGUAGE_FOLDER_PATH."/en-us/language.php";
				break;
			case 'es-es':
				include_once LANGUAGE_FOLDER_PATH."/es-es/language.php";
				break;
			default:
				include_once LANGUAGE_FOLDER_PATH."/pt-br/language.php";
				break;
		}
		
	}
	
	####
	# ACESSO_OBJECT
	####
	class AcessoObj
	{
		public $codigo  = "";
		public $acessar = "";
		public $editar  = "";
		public $apagar  = "";
		
		function __construct( $codigo, $acessar, $editar, $apagar) 
		{ 
			$this->codigo  = $codigo;
			$this->acessar = $acessar;
			$this->editar  = $editar;
			$this->apagar  = $apagar;
		}
		
		public function setCODIGO($_tmp){ $this->codigo = $_tmp; }
		public function getCODIGO(){ return $this->codigo; }
		
		public function setACESSAR($_tmp){ $this->acessar = $_tmp; }
		public function getACESSAR(){ return $this->acessar; }
		
		public function setEDITAR($_tmp){ $this->editar = $_tmp; }
		public function getEDITAR(){ return $this->editar; }
		
		public function setAPAGAR($_tmp){ $this->apagar = $_tmp; }
		public function getAPAGAR(){ return $this->apagar; }
	}
	
	####
	# ACESSO_CTRL
	####
	class AcessoCTRL
	{
		public $acessar = "";
		public $editar  = "";
		public $apagar  = "";
		
		function __construct($acessar, $editar, $apagar) 
		{ 
			$this->acessar = $acessar;
			$this->editar  = $editar;
			$this->apagar  = $apagar;
		}
		
		public function setACESSAR($_tmp){ $this->acessar = $_tmp; }
		public function getACESSAR(){ return $this->acessar; }
		
		public function setEDITAR($_tmp){ $this->editar = $_tmp; }
		public function getEDITAR(){ return $this->editar; }
		
		public function setAPAGAR($_tmp){ $this->apagar = $_tmp; }
		public function getAPAGAR(){ return $this->apagar; }
	}
	
	####
	# SESSAO
	####
	class SessionCTRL
	{
		public $idioma  = "";
		public $user_username = "";
		public $user_nome  = "";
		public $user_sobrenome  = "";
		public $user_email  = "";
		public $user_idsystem_cliente  = "";
		public $user_system_cliente_ind_deletado  = "";
		public $user_plano_codigo  = "";
		public $user_pagto_plano_codigo  = "";
		public $user_pagto_token  = "";
		public $user_pagto_numero  = "";
		public $user_categoria  = "";
		public $user_categoria_titulo = "";
		public $user_idcolaborador  = "";
		public $user_profile_pic  = "";
		public $plano_codigo  = "";
		public $plano_titulo  = "";
		public $plano_tipo  = "";
		public $plano_data_ativacao  = "";
		public $plano_data_expiracao  = "";
		public $plano_data_renovacao  = "";
		public $plano_data_renovacao_com_prazo  = "";
		public $plano_situacao  = "";
		
		function __construct($idioma, $user_username, $user_nome, $user_sobrenome, $user_email, $user_idsystem_cliente, $user_system_cliente_ind_deletado, $user_plano_codigo, $user_pagto_plano_codigo, $user_pagto_token, $user_pagto_numero, $user_categoria, $user_categoria_titulo, $user_idcolaborador, $user_profile_pic, $plano_codigo, $plano_titulo, $plano_tipo, $plano_data_ativacao, $plano_data_expiracao, $plano_data_renovacao, $plano_data_renovacao_com_prazo, $plano_situacao) 
		{ 
			$this->idioma = $idioma;
			$this->user_username = $user_username;
			$this->user_nome = $user_nome;
			$this->user_sobrenome = $user_sobrenome;
			$this->user_email = $user_email;
			$this->user_idsystem_cliente = $user_idsystem_cliente;
			$this->user_system_cliente_ind_deletado = $user_system_cliente_ind_deletado;
			$this->user_plano_codigo = $user_plano_codigo;
			$this->user_pagto_plano_codigo = $user_pagto_plano_codigo;
			$this->user_pagto_token = $user_pagto_token;
			$this->user_pagto_numero = $user_pagto_numero;
			$this->user_categoria = $user_categoria;
			$this->user_categoria_titulo = $user_categoria_titulo;
			$this->user_idcolaborador = $user_idcolaborador;
			$this->user_profile_pic = $user_profile_pic;
			$this->plano_codigo = $plano_codigo;
			$this->plano_titulo = $plano_titulo;
			$this->plano_tipo = $plano_tipo;
			$this->plano_data_ativacao = $plano_data_ativacao;
			$this->plano_data_expiracao = $plano_data_expiracao;
			$this->plano_data_renovacao = $plano_data_renovacao;
			$this->plano_data_renovacao_com_prazo = $plano_data_renovacao_com_prazo;
			$this->plano_situacao = $plano_situacao;
		}
		
		public function setIDIOMA($_tmp)
		{ 
			if(isEmpty($_tmp)){ $_tmp = "pt-br"; } 
			$this->idioma = $_tmp; 
		}
		
		public function getIDIOMA()
		{ 
			if(isEmpty($this->idioma)) 
			{ 
				$this->idioma = "pt-br"; 
			} 
			return $this->idioma;
		}
		
		public function setUSER_USERNAME($_tmp){ $this->user_username = $_tmp; }
		public function getUSER_USERNAME(){ return $this->user_username; }
		public function setUSER_NOME($_tmp){ $this->user_nome = $_tmp; }
		public function getUSER_NOME(){ return $this->user_nome; }
		public function setUSER_SOBRENOME($_tmp){ $this->user_sobrenome = $_tmp; }
		public function getUSER_SOBRENOME(){ return $this->user_sobrenome; }
		public function setUSER_EMAIL($_tmp){ $this->user_email = $_tmp; }
		public function getUSER_EMAIL(){ return $this->user_email; }
		public function setUSER_IDSYSTEM_CLIENTE($_tmp){ $this->user_idsystem_cliente = $_tmp; }
		public function getUSER_IDSYSTEM_CLIENTE(){ return $this->user_idsystem_cliente; }
		public function setUSER_SYSTEM_CLIENTE_IND_DELETADO($_tmp){ $this->user_system_cliente_ind_deletado = $_tmp; }
		public function getUSER_SYSTEM_CLIENTE_IND_DELETADO(){ return $this->user_system_cliente_ind_deletado; }
		public function setUSER_PLANO_CODIGO($_tmp){ $this->user_plano_codigo = $_tmp; }
		public function getUSER_PLANO_CODIGO(){ return $this->user_plano_codigo; }
		public function setUSER_PAGTO_PLANO_CODIGO($_tmp){ $this->user_pagto_plano_codigo = $_tmp; }
		public function getUSER_PAGTO_PLANO_CODIGO(){ return $this->user_pagto_plano_codigo; }
		public function setUSER_PAGTO_TOKEN($_tmp){ $this->user_pagto_token = $_tmp; }
		public function getUSER_PAGTO_TOKEN(){ return $this->user_pagto_token; }
		public function setUSER_PAGTO_NUMERO($_tmp){ $this->user_pagto_numero = $_tmp; }
		public function getUSER_PAGTO_NUMERO(){ return $this->user_pagto_numero; }
		public function setUSER_CATEGORIA($_tmp){ $this->user_categoria = $_tmp; }
		public function getUSER_CATEGORIA(){ return $this->user_categoria; }
		public function setUSER_CATEGORIA_TITULO($_tmp){ $this->user_categoria_titulo = $_tmp; }
		public function getUSER_CATEGORIA_TITULO(){ return $this->user_categoria_titulo; }
		public function setUSER_IDCOLABORADOR($_tmp){ $this->user_idcolaborador = $_tmp; }
		public function getUSER_IDCOLABORADOR(){ return $this->user_idcolaborador; }
		public function setUSER_PROFILE_PIC($_tmp){ $this->user_profile_pic = $_tmp; }
		public function getUSER_PROFILE_PIC(){ return $this->user_profile_pic; }
		public function setPLANO_CODIGO($_tmp){ $this->plano_codigo = $_tmp; }
		public function getPLANO_CODIGO(){ return $this->plano_codigo; }
		public function setPLANO_TITULO($_tmp){ $this->plano_titulo = $_tmp; }
		public function getPLANO_TITULO(){ return $this->plano_titulo; }
		public function setPLANO_TIPO($_tmp){ $this->plano_tipo = $_tmp; }
		public function getPLANO_TIPO(){ return $this->plano_tipo; }
		public function setPLANO_DATA_ATIVACAO($_tmp){ $this->plano_data_ativacao = $_tmp; }
		public function getPLANO_DATA_ATIVACAO(){ return $this->plano_data_ativacao; }
		public function setPLANO_DATA_EXPIRACAO($_tmp){ $this->plano_data_expiracao = $_tmp; }
		public function getPLANO_DATA_EXPIRACAO(){ return $this->plano_data_expiracao; }
		public function setPLANO_DATA_RENOVACAO($_tmp){ $this->plano_data_renovacao = $_tmp; }
		public function getPLANO_DATA_RENOVACAO(){ return $this->plano_data_renovacao; }
		public function setPLANO_DATA_RENOVACAO_COM_PRAZO($_tmp){ $this->plano_data_renovacao_com_prazo = $_tmp; }
		public function getPLANO_DATA_RENOVACAO_COM_PRAZO(){ return $this->plano_data_renovacao_com_prazo; }
		public function setPLANO_SITUACAO($_tmp){ $this->plano_situacao = $_tmp; }
		public function getPLANO_SITUACAO(){ return $this->plano_situacao; }
	}
	
	####
	# GET MENU LINK
	####
	function getMenuLink($_ACESSO, $_MOD_COD, $_TITLE)
	{
		switch($_ACESSO)
		{
			case 1:
				return '<li><a href="main.php?pg='.$_MOD_COD.'">'.$_TITLE.'</a></li>';
				break;
			default:
				return '<li><a style="color: #3d5767; cursor: not-allowed;">'.$_TITLE.'</a></li>';
				break;
		}
	}
	
	####
	# Transforma em array dados de um arquivo ini
	####
	class ArrayINI implements ArrayAccess, IteratorAggregate 
	{
		private $lang;
		
		public function __construct($ini) 
		{
			$this->lang = parse_ini_file($ini);
		}
		
		function __invoke($offset) 
		{
			return $this->offsetGet($offset);
		}
		
		public function getIterator() 
		{
			return new ArrayIterator($this->lang);
		}
		
		public function offsetSet($offset, $value) 
		{
			if (is_null($offset)) 
			{
				$this->lang[] = $value;
			} else {
				$this->lang[$offset] = $value;
			}
		}
		
		public function offsetExists($offset) 
		{
			return isset($this->lang[$offset]);
		}
		
		public function offsetUnset($offset) 
		{
			unset($this->lang[$offset]);
		}
		
		public function offsetGet($offset) 
		{
			return isset($this->lang[$offset]) ? $this->lang[$offset] : null;
		}
	}
	
	####
	# Formata moeda
	####
	function formata_moeda($locale,$valor)
	{
		setlocale(LC_MONETARY, $locale);
		$tmp1 = money_format('%n', $valor);
		$tmp2 = explode(" ",$tmp1);
		return $tmp2[1];
	}
	
	####
	# Remove acentos da string
	####
	function remove_accent($str) 
	{ 
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'); 
		return str_replace($a, $b, $str); 
	}
	function remove_acentos($str) 
	{ 
		$a = array('À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'Ð', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', '?', '?', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', '?', '?', 'L', 'l', 'N', 'n', 'N', 'n', 'N', 'n', '?', 'O', 'o', 'O', 'o', 'O', 'o', 'Œ', 'œ', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'Š', 'š', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Ÿ', 'Z', 'z', 'Z', 'z', 'Ž', 'ž', '?', 'ƒ', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', '?', '?', '?', '?', '?', '?', 'ç', 'Ç', "'" ); 
		$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o','c','C', ' '); 
		return str_replace($a, $b, $str); 
	}
	
	####
	# unescape_string
	####
	function unescape_string($str) 
	{ 
		$a = array('\n', '\r', '\\', '\'', '"');
		$b = array("\n", "\r", "\\", "'", '"'); 
		return str_replace($a, $b, $str); 
	}
	
	####
	# Gera um token aleatorio de identificacao
	####
	function gera_token()
	{
		$c = array("b","c","d","f","g","h","j","k","l","m","n","p","r","s","t","v","w","x","y","z");
		$v = array("a","e","i","o","u");
		$n = array("0","1","2","3","4","5","6","7","8","9");
		for($i=1; $i<=8; $i+=1)
		{
			$tmp .= $c[mt_rand(0,sizeof($c)-1)].$n[mt_rand(0,sizeof($n)-1)].$v[mt_rand(0,sizeof($v)-1)].$n[mt_rand(0,sizeof($n)-1)];
		}#endfor
		$tmp .= time();
		return $tmp;
	}
	
	####
	# Processa template, substituindo as macros pelo texto fornecido no array
	####
	function get_template_text($_path, $_file, $_array, $_language)
	{
		if(empty($_language)){ $_language = 'pt-br'; }
		
		if(empty($_path) || empty($_file) || empty($_array))
		{
			return;
		}
		else
		{
			##Formata arquivo final
			$_tmpl_file = $_path.'/'.$_file.'_'.$_language.'.tt';
			
			##Checa se o arquivo de template existe e carrega matriz
			if(!file_exists($_tmpl_file))
			{
				return;
			}
			else
			{
				##Carrega conteudo do template para processamento
				$tmp = file_get_contents($_tmpl_file);
				
				##Processa dados
				$tmp = str_replace(array_keys($_array), array_values($_array), $tmp);
				
				return $tmp;
			}
		}
		
	}
	
	####
	# Gera uma string randomica
	####
	function generateRandomString($length = 256) 
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) 
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	####
	# Limpa URL
	####
	function esc_url($url) 
	{
		if ('' == $url) 
		{
			return $url;
		}
		
		$url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
		
		$strip = array('%0d', '%0a', '%0D', '%0A');
		$url = (string) $url;
		
		$count = 1;
		while ($count) 
		{
			$url = str_replace($strip, '', $url, $count);
		}
		
		$url = str_replace(';//', '://', $url);
		$url = htmlentities($url);
		$url = str_replace('&amp;', '&#038;', $url);
		$url = str_replace("'", '&#039;', $url);
		
		if ($url[0] !== '/') 
		{
			// Estamos interessados somente em links relacionados provenientes de $_SERVER['PHP_SELF']
			return '';
		} else {
				return $url;
		}
	}
	
	####
	# Inicia uma sessao segura
	####
	function sec_session_start() 
	{
		$session_name = SID_NAME;// Estabeleça um nome personalizado para a sessão
		$secure       = SECURE;
		
		// Isso impede que o JavaScript possa acessar a identificação da sessão.
		$httponly     = true;
		
		// Assim você força a sessão a usar apenas cookies. 
		if (ini_set('session.use_only_cookies', 1) === FALSE) 
		{
			return false;
		}
		// Obtém params de cookies atualizados.
		$cookieParams = session_get_cookie_params();
		session_set_cookie_params($cookieParams["lifetime"],
				$cookieParams["path"], 
				$cookieParams["domain"], 
				$secure,
				$httponly);
		// Estabelece o nome fornecido acima como o nome da sessão.
		session_name($session_name);
		session_start();						// Inicia a sessão PHP 
		session_regenerate_id();		// Recupera a sessão e deleta a anterior.
		
		return true;
	}
	
	####
	# Obtem o IP do usuario
	####
	function getUserIP() 
	{
		$client = @$_SERVER['HTTP_CLIENT_IP']; 
		$forward = @$_SERVER['HTTP_X_FORWARDED_FOR']; 
		return filter_var($client, FILTER_VALIDATE_IP) ? $client : filter_var($forward, FILTER_VALIDATE_IP) ? $forward : $_SERVER['REMOTE_ADDR']; 
	}
	
	####
	# Registra entrada no log do usuario
	####
	function registraSYSTEM_USER_ACCOUNT_LOG($mysqli, $_username, $_descr) 
	{
		$_ip = getUserIP();
		if(empty($_ip)){ $_ip = "ip-unknown"; }
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `SYSTEM_USER_ACCOUNT_LOG` 
		 (`data`,`username`,`ip`,`descr`) 
		 VALUES (now(),?,?,?)"
		))
		{
			$stmt->bind_param('sss', $_username, $_ip, $_descr);
			// Executa a tarefa pré-estabelecida. 
			$stmt->execute();
			$stmt->store_result();
		}
		
	}
	
	####
	# Registra data do ultimo acesso do usuario
	####
	function registraSYSTEM_USER_ACCOUNT_ULT_ACESSO($mysqli, $_username) 
	{
		$_ip = getUserIP();
		if(empty($_ip)){ $_ip = "ip-unknown"; }
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"UPDATE `SYSTEM_USER_ACCOUNT` U
		    SET U.`dth_ult_acesso` = now(),
		        U.`ip_ult_acesso` = ?
		  WHERE U.`username` = ?"
		))
		{
			$stmt->bind_param('ss', $_ip, $_username);
			// Executa a tarefa pré-estabelecida. 
			$stmt->execute();
			$stmt->store_result();
		}
		
	}
	
	####
	# Registra Tentativa Falha de Login
	####
	function registraTentativaDeLogin($mysqli, $_username)
	{
		$now = time();
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"INSERT INTO `LOGIN_TENTATIVAS` (`data`,`username`, `time`) values (now(),?,?)"
		)) 
		{
			$stmt->bind_param('ss', $_username, $now);
			// Executa a tarefa pré-estabelecida. 
			$stmt->execute();
			$stmt->store_result();
		}
		
	}
	
	####
	# Checa Tentativas de Ataque BruteForce
	####
	function checkbrute($mysqli, $user_id) 
	{
		// Registra a hora atual 
		$now = time();
		
		// Todas as tentativas de login são contadas dentro do intervalo das últimas 2 horas. 
		$valid_attempts = $now - (2 * 60 * 60);
		
		if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) 
		{
			$stmt->bind_param('i', $user_id);
			// Executa a tarefa pré-estabelecida. 
			$stmt->execute();
			$stmt->store_result();
			// Se houve mais do que 5 tentativas fracassadas de login 
			if ($stmt->num_rows > 5) 
			{
					return true;
			} else {
					return false;
			}
		}
		else
		{
			return false;	
		}
	}
	
	####
	# Checa se o usuario esta logado
	####
	function isLogedIn($mysqli)
	{
		##inicia sessao
		$init_sid = sec_session_start();
		
		## Verifica se todas as variaveis de sessao foram definidas 
		if (isset($_SESSION['user_id'], 
							$_SESSION['user_username'], 
							$_SESSION['user_nome'],
							$_SESSION['user_sobrenome'],
							$_SESSION['user_email'],
							$_SESSION['login_security'])) 
		{
			$user_id                = $_SESSION['user_id'];
			$user_login_security    = $_SESSION['login_security'];
			//$user_username          = $_SESSION['user_username']; 
			//$user_nome              = $_SESSION['user_nome'];
			//$user_sobrenome         = $_SESSION['user_sobrenome'];
			//$user_email             = $_SESSION['user_email'];
			//$user_system_cliente_id = $_SESSION['user_idsystem_cliente'];
			//$user_colaborador_id    = $_SESSION['user_colaborador_id'];
			//$user_profile_pic       = $_SESSION['user_profile_pic']
			
			// Pega a string do usuário.
			$user_browser = $_SERVER['HTTP_USER_AGENT'];
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT U.`senha` as password
				 FROM `SYSTEM_USER_ACCOUNT` U
				WHERE U.`idSYSTEM_USER_ACCOUNT` = ? 
				LIMIT 1"))
			{
				$sql_USER_ID = $mysqli->escape_string($user_id);
				$stmt->bind_param('s', $sql_USER_ID);
				$stmt->execute();
				$stmt->store_result();
				$stmt->bind_result($o_PASSWORD);
				$stmt->fetch();
			} 
			//else 
			//{
			//	die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
			//}
			
			##Se nao encontrou dados, retorna
			if ($stmt->num_rows == 1) 
			{
				$o_LOGIN_SECURITY = hash('sha512', $o_PASSWORD .'~'. $user_browser);
				if ($o_LOGIN_SECURITY == $user_login_security) 
				{
						##Registra data do ultimo acesso
						registraSYSTEM_USER_ACCOUNT_ULT_ACESSO($mysqli, $_SESSION['user_username']);
							
						// Logado!!!
						return true;
				} else {
						// Não foi logado 
						return false;
				}
			}
			else
			{
				return false;
			}
			
		} else {
				// Não foi logado 
				return false;
		}
			
	}
	
	
	####
	# Converte a primeira letra da palavra para maiusculo, deixa o resto minusculo
	####
	function utf8_ucfirst($string, $e ='utf-8') 
	{ 
		if (function_exists('mb_strtoupper') && 
		    function_exists('mb_substr') && 
		    !empty($string)) 
		{
			$string = mb_strtolower($string, $e); 
			$upper = mb_strtoupper($string, $e); 
			preg_match('#(.)#us', $upper, $matches); 
			$string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e); 
		}
		else 
		{
				$string = ucfirst($string); 
		}
		return $string; 
	}
	
	####
	# getRequestInput
	####
	function getRequestInput($_input)
	{
		switch($_SERVER["REQUEST_METHOD"])
		{
			case 'GET':
				$RET = test_input($_GET[$_input]);
				break;
			case 'POST':
				$RET = test_input($_POST[$_input]);
				break;
			default:
				break;
		}
		return $RET;
	}
	
	####
	# getRequestInputRaw
	####
	function getRequestInputRaw($_input)
	{
		switch($_SERVER["REQUEST_METHOD"])
		{
			case 'GET':
				$RET = $_GET[$_input];
				break;
			case 'POST':
				$RET = $_POST[$_input];
				break;
			default:
				break;
		}
		return $RET;
	}
	
	
	####
	# APP - geraErrorMsgJSON
	####
	function geraAppErrorMsgJSON($_msg)
	{
		$TMP = array(
			"stat"          => 0, 
			"stat_msg"      => "".$_msg.""
		);
		$JSON = json_encode($TMP);
		return $JSON;
	}
	
	####
	# Obtem nome da cidade e o estado do codigo ibge informado
	####
	function getCidadeInfo($mysqli, $_codigo) 
	{
		$sql_CODIGO = $mysqli->escape_String($_codigo);
		
		$oRET = "";
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT upper(M.cidade) as cidade, upper(M.uf) as UF 
       FROM `MUNICIPIO_BR` M 
      WHERE M.codigo_ibge = ? 
      LIMIT 1"
		))
		{
			$stmt->bind_param('s', $sql_CODIGO);
			$stmt->execute();
			$stmt->store_result();
			
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($oCIDADE, $oUF);
			$stmt->fetch();
			$oRET = $oCIDADE."-".$oUF;
			
		}
		else
		{
			error_log("aux_lib.php -> getCidadeInfo() -> (". htmlspecialchars($stmt->error) .")(".htmlspecialchars($mysqli->error).")");
		}
		
		return $oRET;
	}
	
	####
	# Converte ponto para virgula
	####
	function ponto2Virgula($txt)
	{
		$tmp = explode(".",$txt);
		$ret = $tmp[0];
		if($tmp[1])
		{
			$ret = $ret.",".$tmp[1];
		}
		return $ret;
	}
	
	####
	# Converte virgula para ponto
	####
	function virgula2Ponto($txt)
	{
		$tmp = explode(",",$txt);
		$ret = $tmp[0];
		if($tmp[1])
		{
			$ret = $ret.".".$tmp[1];
		}
		return $ret;
	}
	
	####
	# Formata endereco
	####
	function formataEndereco($_CEP, $_UF, $_CIDADE, $_BAIRRO, $_END, $_NUMERO, $_COMPL)
	{
		$END_TXT = '';
		
		if($_END)
		{
			$END_TXT = $_END;
		}
		if($_NUMERO)
		{
			if($END_TXT){ $END_TXT .= ", ".$_NUMERO; } 
			       else { $END_TXT .= $_NUMERO; }
		}
		if($_COMPL)
		{
			if($END_TXT){ $END_TXT .= ", ".$_COMPL; } 
			       else { $END_TXT .= $_COMPL; }
		}
		if($_BAIRRO)
		{
			if($END_TXT){ $END_TXT .= "<br/>".$_BAIRRO; } 
			       else { $END_TXT .= $_BAIRRO; }
		}
		if($_CIDADE)
		{
			if($END_TXT){ $END_TXT .= "<br/>".$_CIDADE; } 
			       else { $END_TXT .= $_CIDADE; }
		}
		if($_UF)
		{
			if($END_TXT){ $END_TXT .= " - ".$_UF; } 
			       else { $END_TXT .= $_UF; }
		}
		if($_CEP)
		{
			if($END_TXT){ $END_TXT .= " - CEP ".$_CEP; } 
			       else { $END_TXT .= "CEP ".$_CEP; }
		}
		$END_TXT = "<small>".$END_TXT."</small>";
		
		return $END_TXT;
	}
	
	####
	# proximoDiaUtil
	# 
	# Função para calcular o próximo dia útil de uma data
	# Formato de entrada da $data: AAAA-MM-DD
	####
	function proximoDiaUtil($_DATA, $_SAIDA = 'd/m/Y') 
	{
		// Converte $data em um UNIX TIMESTAMP
		$_timestamp = strtotime($_DATA);
		// Calcula qual o dia da semana de $data
		// O resultado será um valor numérico:
		// 1 -> Segunda ... 7 -> Domingo
		$_dia = date('N', $_timestamp);
		// Se for sábado (6) ou domingo (7), calcula a próxima segunda-feira
		if ($_dia >= 6) 
		{
			$_timestamp_final = $_timestamp + ((8 - $_dia) * 3600 * 24);
		}
		else
		{
			// Não é sábado nem domingo, mantém a data de entrada
			$_timestamp_final = $_timestamp;
		}
		
		return date($_SAIDA, $_timestamp_final);
	}
	
	####
	# Formata Semafaro de Pagamento
	####
	function getCorSemaforoPagto_old($_DATA_VENC, $_DATA_PAGTO, $_VALOR_PAGO)
	{
		$_DATA_ATUAL = date_create('now')->format('d/m/Y');
		$tmp = explode('/',$_DATA_ATUAL);
		$_DATA_ATUAL_DIA = $tmp[0]; $_DATA_ATUAL_MES = $tmp[1]; $_DATA_ATUAL_ANO = $tmp[2];
		$_DATA_ATUAL_NUM = $_DATA_ATUAL_ANO.$_DATA_ATUAL_MES.$_DATA_ATUAL_DIA;
		
		$tmp = explode('/',$_DATA_VENC);
		$_DATA_VENC_DIA = $tmp[0]; $_DATA_VENC_MES = $tmp[1]; $_DATA_VENC_ANO = $tmp[2];
		$_DATA_VENC_NUM = $_DATA_VENC_ANO.$_DATA_VENC_MES.$_DATA_VENC_DIA;
		
		$tmp = explode('/',$_DATA_PAGTO);
		$_DATA_PAGTO_DIA = $tmp[0]; $_DATA_PAGTO_MES = $tmp[1]; $_DATA_PAGTO_ANO = $tmp[2];
		$_DATA_PAGTO_NUM = $_DATA_PAGTO_ANO.$_DATA_PAGTO_MES.$_DATA_PAGTO_DIA;
		
		$COR_TXT = "";
		
		//Formata indicador de cor
		// Verde - Pago
		// Vermelho - Pendente de pagamento
		// Cinza - Pendente, mas de outro mes ou ano
		if($_DATA_PAGTO_NUM > 0 && $_VALOR_PAGO > 0)
		{
			$COR_TXT = "#008000";
		}
		else
		{
			if( ($_DATA_VENC_MES > $_DATA_ATUAL_MES) || 
			    ($_DATA_VENC_ANO > $_DATA_ATUAL_ANO)
				)
				{
					$COR_TXT = "#c8c8c8";
				}
				else
				{
					$COR_TXT = "#FF0000";
				}
		}
		
		//error_log("getCorSemaforoPagto:\n\nDATA_ATUAL -> ($_DATA_ATUAL)($_DATA_ATUAL_NUM)\nDATA_VENC -> ($_DATA_VENC)($_DATA_VENC_NUM)\nDATA_PAGTO -> ($_DATA_PAGTO)($_DATA_PAGTO_NUM)\nCOR_TXT -> ($COR_TXT)\n\n",0);
		
		return $COR_TXT;
		
	}
	
	####
	# Formata Semafaro de Pagamento
	####
	function getCorSemaforoPagto($_DATA_VENC, $_DATA_PAGTO, $_VALOR_PAGO)
	{
		$_DATA_ATUAL = date_create('now')->format('d/m/Y');
		$tmp = explode('/',$_DATA_ATUAL);
		$_DATA_ATUAL_DIA = $tmp[0]; $_DATA_ATUAL_MES = $tmp[1]; $_DATA_ATUAL_ANO = $tmp[2];
		$_DATA_ATUAL_NUM = $_DATA_ATUAL_ANO.$_DATA_ATUAL_MES.$_DATA_ATUAL_DIA;
		
		$tmp = explode('/',$_DATA_VENC);
		$_DATA_VENC_DIA = $tmp[0]; $_DATA_VENC_MES = $tmp[1]; $_DATA_VENC_ANO = $tmp[2];
		$_DATA_VENC_NUM = $_DATA_VENC_ANO.$_DATA_VENC_MES.$_DATA_VENC_DIA;
		
		$tmp = explode('/',$_DATA_PAGTO);
		$_DATA_PAGTO_DIA = $tmp[0]; $_DATA_PAGTO_MES = $tmp[1]; $_DATA_PAGTO_ANO = $tmp[2];
		$_DATA_PAGTO_NUM = $_DATA_PAGTO_ANO.$_DATA_PAGTO_MES.$_DATA_PAGTO_DIA;
		
		$_DATA_ATUAL_SQL = $_DATA_ATUAL_ANO.'-'.$_DATA_ATUAL_MES.'-'.$_DATA_ATUAL_DIA;
		$_DATA_VENC_SQL  = $_DATA_VENC_ANO.'-'.$_DATA_VENC_MES.'-'.$_DATA_VENC_DIA;
		$_DATA_PAGTO_SQL = $_DATA_PAGTO_ANO.'-'.$_DATA_PAGTO_MES.'-'.$_DATA_PAGTO_DIA;
		
		if($_DATA_ATUAL)
		{
			$dDataHoje = new DateTime($_DATA_ATUAL_SQL);
		}
		if($_DATA_VENC)
		{
			$dDataVenc  = new DateTime($_DATA_VENC_SQL);
		}
		if($_DATA_PAGTO)
		{
			$dDataPagto = new DateTime($_DATA_PAGTO_SQL);
		}
		/*
		if($_DATA_ATUAL && $_DATA_VENC)
		{
			$dDiff = $dDataAtual->diff($dDataVenc);
		}
		*/
		
		//error_log("aux_lib.php -> getCorSemaforoPagto2():\n\n data atual -> $_DATA_ATUAL_SQL\n\n data_venc -> $_DATA_VENC_SQL\n\n data_pagto -> $_DATA_PAGTO_SQL\n\n dDiff -> ".$dDiff->format('%R')."\n\n dDiff->days -> ".$dDiff->days."\n\n dDiff_b -> ".$dDiff_b->format('%R')."(".$dDiff_b->days.")\n\n",0);
		
		//echo $dDiff->format('%R'); // use for point out relation: smaller/greater
		//echo $dDiff->days;
		
		
		$COR_TXT = "";
		
		//Formata indicador de cor
		// Verde - Pago
		// Amarelo - Vence hoje
		// Vermelho - vencido
		// Cinza - Lancamento futuro
		if($_DATA_PAGTO_NUM > 0 && $_VALOR_PAGO > 0)
		{
			$COR_TXT = "#008000";
		}
		else
		{
			//Vencimento < Hoje
			if($dDataVenc < $dDataHoje)
			{
				$COR_TXT = "#FF0000";
			}
			//Vencimento = Hoje
			else if($dDataVenc == $dDataHoje)
			{
				$COR_TXT = "#feb741";
			}
			//Vencimento > Hoje
			else if($dDataVenc > $dDataHoje)
			{
				$COR_TXT = "#c8c8c8";
			}
			
		}
		
		//error_log("getCorSemaforoPagto2:\n\nDATA_ATUAL -> ($_DATA_ATUAL)($_DATA_ATUAL_NUM)\nDATA_VENC -> ($_DATA_VENC)($_DATA_VENC_NUM)\nDATA_PAGTO -> ($_DATA_PAGTO)($_DATA_PAGTO_NUM)\nCOR_TXT -> ($COR_TXT)\n\n",0);
		
		return $COR_TXT;
		
	}
	
	####
	# Obtem data no formato DD/MM/YYYY
	# obs: data no formato YYYY-MM-DD, ex.: 2017-01-01
	###
	function formataDataBR($_DATA)
	{
		$_tmp = explode("-",$_DATA);
		$_DD   = $_tmp[2];
		$_MM   = $_tmp[1];
		$_YYYY = $_tmp[0];
		return $_DD.'/'.$_MM.'/'.$_YYYY;
	}
	
	
	####
	# Obtem data no formato DDMMYYYY
	# obs: data no formato YYYY-MM-DD, ex.: 2017-01-01
	###
	function getDDMMYYYY($_DATA)
	{
		$_tmp = explode("-",$_DATA);
		$_DD   = $_tmp[2];
		$_MM   = $_tmp[1];
		$_YYYY = $_tmp[0];
		return $_DD.$_MM.$_YYYY;
	}
	
	####
	# Obtem data no formato DD
	# obs: data no formato YYYY-MM-DD, ex.: 2017-01-01
	###
	function getDD($_DATA)
	{
		$_tmp = explode("-",$_DATA);
		$_DD   = $_tmp[2];
		$_MM   = $_tmp[1];
		$_YYYY = $_tmp[0];
		return $_DD;
	}
	
	####
	# Obtem data no formato MM
	# obs: data no formato YYYY-MM-DD, ex.: 2017-01-01
	###
	function getMM($_DATA)
	{
		$_tmp = explode("-",$_DATA);
		$_DD   = $_tmp[2];
		$_MM   = $_tmp[1];
		$_YYYY = $_tmp[0];
		return $_MM;
	}
	
	####
	# Obtem data no formato YYYY
	# obs: data no formato YYYY-MM-DD, ex.: 2017-01-01
	###
	function getYYYY($_DATA)
	{
		$_tmp = explode("-",$_DATA);
		$_DD   = $_tmp[2];
		$_MM   = $_tmp[1];
		$_YYYY = $_tmp[0];
		return $_YYYY;
	}
	
	####
	# Gera um nome aleatorio valido (nao existe no HD)
	# _PATH        -> Caminho onde sera gravado o arquivo
	# _EXTENSION   -> Extensao do arquivo sem ponto, só o texto
	# _RETURN_TYPE -> 0 - Nome apenas, 1 - nome completo com a extensao inclusa
	####
	function getValidRandomFilename($_PATH, $_EXTENSION, $_RETURN_TYPE)
	{
		$filename = '';
		
		if($_PATH)
		{
			do 
			{
				$filename_tmp = md5(uniqid(time())); //nome que dará a imagem
				$filename = $filename_tmp.".".$_EXTENSION; //nome que dará a imagem
				if (file_exists($_PATH."/".$filename)) { $continua = true; } else { $continua = false; }
			} while ($continua == true);
		}
		
		if($_RETURN_TYPE == 0)
		{
			return $filename_tmp;
		}
		else
		{
			return $filename;
		}
	}
	
	####
	# Filtra valor informado para padrao CNAB de remessa
	# obs: entrar valores em padrao us. ex.: 145.30
	####
	function GetValorRemessa($_VLR)
	{
		$tmp = str_replace(",","",$_VLR);
		$tmp = str_replace(".","",$tmp);
		return $tmp;
	}
	
	####
	# Apaga arquivos antigos em uma pasta
	####
	function delete_old_files($_PATH, $_EXTENSION, $_DAYS)
	{
		//$date = strtotime('-'.$_DAYS.' day', mktime());
		$date = strtotime('-'.$_DAYS.' day', time());
		
		foreach(glob($_PATH.'/*.'.$_EXTENSION) as $file)
		{
			$filetime = filemtime($file);
		
			if( $date > $filetime )
			{
				//echo 'Antigo: ' . $file . '<br />';
				unlink ($file);
			}
			/*else
			{
				//echo 'Novo: ' . $file . '<br />';
			}*/
		}
	}
	
#################################################################################
###########
#####
##
