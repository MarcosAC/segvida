<?php
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: chk_code.php
## Função..........: Checa o captcha informado na e valido
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	require_once "geral/config.php";
	$cfg = new APPConfig;
	
	require_once "geral/cgi_security.php";
	//require_once "geral/language.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_FIELD_ID = $IN_ESTADO = $IN_CSS = $IN_REQUIRED = $IN_FIRSTBLANK = $IN_FIRSTBLANKTEXT = $IN_UF = $IN_CIDADE = $IN_IBGE_CODE = "";
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_FIELD_ID   = test_input($_GET["field_id"]);
		$IN_ESTADO     = test_input($_GET["estado"]);
		$IN_CSS        = test_input($_GET["css"]);
		$IN_REQUIRED   = test_input($_GET["required"]);
		$IN_FIRSTBLANK = test_input($_GET["firstblank"]);
		$IN_CIDADE     = test_input($_GET["cidade"]);
		$IN_CODIGO     = test_input($_GET["codigo"]);
		
		//$IN_CIDADE2     = remove_accent($IN_CIDADE);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["lang"]);
		$IN_FIELD_ID       = test_input($_POST["field_id"]);
		$IN_ESTADO         = test_input($_POST["estado"]);
		$IN_CSS            = test_input($_POST["css"]);
		$IN_REQUIRED       = test_input($_POST["required"]);
		$IN_FIRSTBLANK     = test_input($_POST["firstblank"]);
		$IN_FIRSTBLANKTEXT = test_input($_POST["firstblanktext"]);
		$IN_CIDADE         = test_input($_POST["cidade"]);
		$IN_CODIGO         = test_input($_POST["codigo"]);
		
		## Autentica dados obrigatorios
		if( !empty($IN_ESTADO) &&
				!empty($IN_FIELD_ID)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|Falha na transferência dos dados. Favor tentar novamente!|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$CON = mysql_connect($cfg->DB_HOST, $cfg->DB_USERNAME, $cfg->DB_PASSWORD);// or die ("Sem conexão com o servidor");
			if (!$CON) { die("0|Não foi possível conectar: ".mysql_error()."|error|"); exit; }
			
			##Checa conexao com o Banco de Dados
			$SELECT_DB = mysql_select_db($cfg->DB_DATABASE);// or die("Sem acesso ao DB, entre em contato com o Administrador");
			if (!$SELECT_DB) { die("0|Sem acesso ao DB: ".mysql_error()."|error|"); exit; }
			
			##UTF-8
			$QUERY = "SET NAMES utf8";
			$RESULT = mysql_query($QUERY);
			
			##Prepara query
			$QUERY = "SELECT `codigo_ibge` as codigo,`cidade` as cidade FROM `MUNICIPIOS_BR` ";
			//
			$WHERE_TMP = $CAP = "";
			//
			if(!empty($IN_ESTADO))
			{
				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
				//$WHERE_TMP .= sprintf($CAP." upper(`uf`) = _utf8 upper('%s')",
				$WHERE_TMP .= sprintf($CAP." upper(`uf`) = upper('%s')",
				mysql_real_escape_string($IN_ESTADO));
			}
			//if(!empty($IN_CIDADE))
			//{
			//	if(!empty($WHERE_TMP)){ $CAP = " AND "; }
			//	$WHERE_TMP .= sprintf($CAP." upper(`cidade`) = upper('%s')",
			//	mysql_real_escape_string($IN_CIDADE));
			//}
			if(!empty($IN_CODIGO))
			{
				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
				$WHERE_TMP .= sprintf($CAP." upper(`codigo_ibge`) = upper('%s')",
				mysql_real_escape_string($IN_CODIGO));
			}
			//
			$QUERY .= " WHERE ".$WHERE_TMP;//." collate utf8_general_ci";
			
			
			##Executa query
			$RESULT = mysql_query($QUERY);
			if (!$RESULT) { die("0|Query inválida: ".mysql_error().")|error|"); exit; }
			
			##Debug
			#if (!$result) 
			#{
			#	$message  = 'Invalid query: ' . mysql_error() . "\n";
			#	$message .= 'Whole query: ' . $query;
			#	die($message);
			#}
			
			##Se nao encontrou dados, retorna
			if (mysql_num_rows($RESULT) == 0) 
			{ 
				die("0|Nenhuma cidade encontrada para o Estado selecionado!|alert|");
				exit; 
			}
			else
			{
				$cidade_tmp = remove_accent($IN_CIDADE);
				
				##Corre pelas linhas encontradas
				while ($row = mysql_fetch_assoc($RESULT)) 
				{
					$SELECTED = "";
					if(!empty($cidade_tmp) && strtoupper($cidade_tmp) == strtoupper($row['cidade']) ){ $SELECTED = "selected"; }
					$ITEM .= "<option value='".utf8_decode($row['codigo'])."' ".$SELECTED.">".$row['cidade']."</option>";
					
					//echo "(".strtoupper($cidade_tmp).") = (".strtoupper($row['cidade']).")\n<br/>";
				}
				if($IN_FIRSTBLANK == 1){ $FIRSTBLANK = '<option value="">'.$IN_FIRSTBLANKTEXT.'</option>'; } else { $FIRSTBLANK=''; }
				//$LISTA = '<select id="'.strtoupper($IN_FIELD_ID).'" name="'.strtoupper($IN_FIELD_ID).'" class=\''.$IN_CSS.'\'>'.$FIRSTBLANK.$ITEM.'</select>';
				
				$LISTA = '<select id="'.strtolower($IN_FIELD_ID).'"';
				if( !empty($IN_CSS) ){ $LISTA .= ' class="'.$IN_CSS.'"'; }
				if( $IN_REQUIRED == 1 ){ $LISTA .= ' required'; }
				$LISTA .= '>'.$FIRSTBLANK.$ITEM.'</select>';
				
				//die('1|('.$IN_CIDADE2.')('.$IN_CIDADE.')('.$row['cidade'].')('.$JOSTA.')'.$LISTA.'|success|');
				die('1|'.$LISTA.'|success|');
				exit;
				
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
