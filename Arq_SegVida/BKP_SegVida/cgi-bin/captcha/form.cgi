#!/usr/bin/perl -w
# -w -> Show Warnings
# -W -> Show All Warnings
##############################################################################
# By BumbleBeeWare.com 2006
# Simple CAPTCHA using static premade images
# form.cgi
##############################################################################

BEGIN {
    my $b__dir = (-d '/home/rlgom197/perl'?'/home/rlgom197/perl':( getpwuid($>) )[7].'/perl');
    unshift @INC,$b__dir.'5/lib/perl5',$b__dir.'5/lib/perl5/i686-linux',map { $b__dir . $_ } @INC;
	}
	
print "Content-type: text/html\n\n";

# get a random number for the querry on the captcha.cgi
# a random number will force old browsers to update the image
# here we just used the time function, since it will be unique on every access of the page
$randomnumber = time;

print "<html>
<head>
<title>TEST CAPTCHA IMAGE Verification</title>
</head>

<body>

<form method=\"POST\" action=\"http://www.rlgomide.com/cgi-bin/captcha/check-captcha.cgi\">
    <p align=\"center\"><img src=\"http://www.rlgomide.com/cgi-bin/captcha/captcha.cgi?$randomnumber\"></p>
    <center><input type=\"text\" size=\"20\" name=\"verifytext\"><input
    type=\"submit\" name=\"checktext\" value=\"Verify\"></center>
</form>
</body>
</html>";


