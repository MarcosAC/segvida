<?php
#################################################################################
## RLGOMIDE.COM
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
##
## Módulo..........: gera_lista_cidades.php
## Função..........: Gera <SELECT> com a lista das cidades para o estado informado
##                 
#################################################################################
###########
#####
##

##
#####
###########
#################################################################################
## Carregamentos iniciais
#################################################################################
	
	header('Content-Type: text/html; charset=utf-8');
	
	## System configuration.
	include_once "includes/config.php";
	include_once "includes/aux_lib.php";
	//include_once "geral/language.php";
	
	## Carrega inputs iniciais
	## Carrega inputs
	$GO = "";
	
	#define variables and set to empty values
	$IN_LANG = $IN_FIELD_ID = $IN_ESTADO = $IN_CSS = $IN_REQUIRED = $IN_FIRSTBLANK = $IN_FIRSTBLANKTEXT = $IN_UF = $IN_CIDADE = $IN_IBGE_CODE = "";
	
	#Valida metodo de solicitacao
/*	if($_SERVER["REQUEST_METHOD"] == "GET") 
	{
		$IN_LANG       = test_input($_GET["lang"]);
		$IN_FIELD_ID   = test_input($_GET["field_id"]);
		$IN_ESTADO     = test_input($_GET["estado"]);
		$IN_CSS        = test_input($_GET["css"]);
		$IN_REQUIRED   = test_input($_GET["required"]);
		$IN_FIRSTBLANK = test_input($_GET["firstblank"]);
		$IN_CIDADE     = test_input($_GET["cidade"]);
		$IN_CODIGO     = test_input($_GET["codigo"]);
*/
		
	if($_SERVER["REQUEST_METHOD"] == "POST") 
	{
		$IN_LANG           = test_input($_POST["lang"]);
		$IN_FIELD_ID       = test_input($_POST["field_id"]);
		$IN_ESTADO         = test_input($_POST["estado"]);
		$IN_CSS            = test_input($_POST["css"]);
		$IN_REQUIRED       = test_input($_POST["required"]);
		$IN_FIRSTBLANK     = test_input($_POST["firstblank"]);
		$IN_FIRSTBLANKTEXT = test_input($_POST["firstblanktext"]);
		$IN_CIDADE         = test_input($_POST["cidade"]);
		$IN_CODIGO         = test_input($_POST["codigo"]);
		$IN_OUTPUT         = test_input($_POST["output"]);
		
		
		## Autentica dados obrigatorios
		if( !empty($IN_ESTADO) &&
				!empty($IN_FIELD_ID)
			) 
		{ $GO=1; } else 
		{ 
			$GO=0;
			die("0|Falha na transferência dos dados. Favor tentar novamente!|alert|");
			exit;
		}
		
		## Autenticação bem sucedida
		if($GO == 1)
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara where
			$WHERE_TMP = $CAP = "";
			//
			if(!empty($IN_ESTADO))
			{
				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
				$WHERE_TMP .= sprintf($CAP." upper(`uf`) = upper('%s')",
				$mysqli->escape_string($IN_ESTADO));
			}
			//if(!empty($IN_CIDADE))
			//{
			//	if(!empty($WHERE_TMP)){ $CAP = " AND "; }
			//	$WHERE_TMP .= sprintf($CAP." upper(`cidade`) = upper('%s')",
			//	mysql_real_escape_string($IN_CIDADE));
			//}
			if(!empty($IN_CODIGO))
			{
				if(!empty($WHERE_TMP)){ $CAP = " AND "; }
				$WHERE_TMP .= sprintf($CAP." upper(`codigo_ibge`) = upper('%s')",
				$mysqli->escape_string($IN_CODIGO));
			}
			//
			$WHERE = " WHERE ".$WHERE_TMP;//." collate utf8_general_ci";
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT `codigo_ibge` as codigo,`cidade` as cidade 
			   FROM `MUNICIPIO_BR` 
			  $WHERE"
			)) 
			{
				//$stmt->bind_param('s', $email);
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_codigo, $o_cidade);
				
				$stmt->execute();
				$stmt->store_result();
				//
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{ 
					die("0|Nenhuma cidade encontrada para o Estado selecionado!|alert|");
					exit; 
				}
				else
				{ 
					$cidade_tmp = remove_accent($IN_CIDADE);
					
					##Corre pelas linhas encontradas
					while ($stmt->fetch()) 
					{
						$SELECTED = "";
						if(!empty($cidade_tmp) && strtoupper($cidade_tmp) == strtoupper($o_cidade) ){ $SELECTED = "selected"; }
						$ITEM .= "<option value='".utf8_decode($o_codigo)."' ".$SELECTED.">".$o_cidade."</option>";
					}
					
					if($IN_FIRSTBLANK == 1){ $FIRSTBLANK = '<option value="">'.$IN_FIRSTBLANKTEXT.'</option>'; } else { $FIRSTBLANK=''; }
					
					## Apenas a lista de options
					if($IN_OUTPUT == 2)
					{
						$LISTA = $ITEM;
					}
					
					## A lista de options e o select
					else
					{
						$LISTA = '<select id="'.strtolower($IN_FIELD_ID).'"';
						if( !empty($IN_CSS) ){ $LISTA .= ' class="'.$IN_CSS.'"'; }
						if( $IN_REQUIRED == 1 ){ $LISTA .= ' required'; }
						$LISTA .= '>'.$FIRSTBLANK.$ITEM.'</select>';
					}
					
					die('1|'.$LISTA.'|success|');
					exit;
				}
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error|");
				exit;
			}
			
		}#endifgo
		
	}
	#Se nao for enviado via POST, nao mostra dada, apenas uma tela em branco.
	else
	{
		die("0|O.O?|error|");
	}
	
#################################################################################
###########
#####
##
?>
