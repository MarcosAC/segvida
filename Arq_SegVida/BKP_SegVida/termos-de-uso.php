<?php
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
?>	

<?php 
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Termos de Uso" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link rel="stylesheet" href="css/offcanvas.css">
		<link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>

		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO ?></a></li>
						<li><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS ?></a></a></li>
						<li><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO ?></a></a></li>
						<li><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">

				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-info-circle" aria-hidden="true"></span> Termos de Uso</h3>
					</div>
					
					<div class="row">
						<div class="media">
							<div class="media-left media-top">
								&nbsp;
							</div>
							<div class="media-body">
								<h4 class="media-heading">Visão Geral</h4>
								<p class="text-justify">
									Estes Termos de Uso explicam os princípios de operação dos Travadores de Conteúdo neste website.
									Neste website, você pode encontrar Travadores de Conteúdo que podem pedí-lo para logar-se, inscrever-se, entrar com o seu nome ou executar outras ações para ter acesso a conteúdo trancado.
								</p>
								<h4>Usando o seu Endereço de Email</h4>
								<p class="text-justify">
									Quando você entra com o seu email ou loga-se através de redes sociais, você concorda que o seu email seja adicionado à lista de inscrição para envio de notícias alvo e ofertas especiais. Você pode desinscrever-se a qualquer momento clicando no link ao final de qualquer email recebido de nós.
								</p>
								<h4>Apps Sociais & Permissões</h4>
								<p class="text-justify">
									Quando você se loga através de redes sociais, o Travador de Conteúdo pode pedí-lo para conceder permissões de leitura ou execução de algumas ações sociais.
									O Travador de Conteúdo recupera apenas a seguinte informação (de acordo com a Política de Privacidade deste website): Nome da Pessoa, Endereço de Email
								</p>
								<p class="text-justify">
									O Travador de Conteúdo nunca coleta outra informação e nunca publica nada em redes sociais em seu benefício sem as suas permissões. Depois de destravar o conteúdo o Travador de Conteúdo remove todos os tokens de acesso recebidos de você e nunca os usa novamente. Se houverem quaisquer perguntas referentes a estes termos de uso você pode nos contatar.
								</p>
								<br/>
								<p class="text-center">
									<u>Se houverem quaisquer perguntas referentes a estes termos de uso você pode nos contatar.</u>
								</p>
							</div>
						</div>
						<br/>
						
					</div><!--/row-->

				</div><!--/.col-xs-12.col-sm-9-->

			</div><!--/row-->
		</div><!--/container-->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>