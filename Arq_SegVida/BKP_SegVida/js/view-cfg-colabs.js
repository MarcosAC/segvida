/*#################################################################################
  # Projeto: SEGVIDA
  #  Modulo: view-cfg-colabs.js
  #  Funcao: Interface - Automacao de Camada;
  #   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
  ################################################################################# */
$(document).ready(function() 
{
	
	//Setup
	var base_url = '//segvida.prodfy.com.br';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// Verificacao de URL Correto ~ START
	// Verifica se o URL do site esta correto e redireciona se necessario
	function getAbsolutePath() 
	{
		var loc = window.location;
		//var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/') + 1);
		var pathName = loc.pathname.substring(0, loc.pathname.lastIndexOf('/'));
		return loc.href.substring(0, loc.href.length - ((loc.pathname + loc.search + loc.hash).length - pathName.length));
	}
	
	tmp_site_url = getAbsolutePath();
	var tmp0 = base_url.split('//');
	var tmp1 = tmp_site_url.split('//');
	//alert(tmp0[1]+' = '+tmp1[1]);
	
	//Se a pagina nao for acessada no endereco correto, carrega novamente no endereco certo
	if(tmp0[1] != tmp1[1])
	{
		window.location.replace(base_url+'/main.php?pg=cfg-colabs');
	}
	////////////////////// Verificacao de URL Correto ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	////////////////////// Formatacao de Conteudo de Formularios ~ START
	function setContent(myObj,myCase) 
	{
		var _tmp = myObj.val();
		
		if(isEmpty(myCase) || isEmpty(_tmp))
		{
			return;
		}
		
		switch(myCase)
		{
			case 'CPF':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'CNPJ':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			case 'FONE':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				_t = Number(_t); _t = _t+0;
				myObj.val(_t);
				break;
			case 'CEP':
				//Deixa apenas numeros
				var _t = _tmp.replace(/[^0-9]/g, '');
				myObj.val(_t);
				break;
			default:
				break;
		}
		
	}
	////////////////////// Formatacao de Conteudo de Formularios ~ STOP
	
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() { showLoadingBox('Processando...'); }).ajaxStop(function() { hideLoadingBox(); });
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	/*$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});*/
	$(document).on("f_show_msgbox", function(e,_txt,_title,_type)
	{
		show_msgbox(_txt,_title,_type);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconFicha").hide();
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'ficha':
				$("#msgBoxIconFicha").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// LOADINGBOX ~ START
	var LoadingBoxPanel = '';
	function showLoadingBox(_title)
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
		LoadingBoxPanel = $('<div class="modal-backdrop" style="background: rgba(0,0,0, 0.8);"><div style="position: fixed;top: 50%;left: 50%;transform: translate(-50%, -50%);width:160px;height:80px;-webkit-border-radius: 8px;-moz-border-radius: 8px;border-radius: 8px;background-color:#5C9CCC;display: flex;justify-content: center;"><p class="text-center center-block" style="margin-top: 12px;"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color="#ffffff"><b id="loadingBoxText">'+_title+'</b></font></p></div>');
		if( $('#cad-modal').is(':visible') ){ LoadingBoxPanel.appendTo('#cad-modal'); } else { LoadingBoxPanel.appendTo(document.body); }
	}
	function hideLoadingBox()
	{
		if(LoadingBoxPanel){ LoadingBoxPanel.remove(); }
	}
	////////////////////// LOADINGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#reg_captcha_refresh_btn", function()
	{
		$("#reg_captcha_code").val('');
		$('#reg_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#reg_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// Datatable ~ START
	var myTable = $('#main-datatable').DataTable(
	{
		dom: "lBfrtip",
		buttons: [
			{
				extend: "copy",
				className: "btn-sm",
				//text: "<i class='fa fa-clipboard fa-lg' data-toggle='tooltip' data-placement='bottom' title='Copiar'></i>"
				text: "<i class='fa fa-clipboard fa-lg'></i>",
				exportOptions: { columns: [ 2, 3, 4, 5, 6 ] }
			},
			{
				extend: "csv",
				className: "btn-sm",
				//text: "<i class='fa fa-file-text-o fa-lg' data-toggle='tooltip' data-placement='bottom' title='CSV'></i>"
				text: "<i class='fa fa-file-text-o fa-lg'></i>",
				exportOptions: { columns: [ 2, 3, 4, 5, 6 ] }
			},
			{
				extend: "excel",
				className: "btn-sm",
				//text: "<i class='fa fa-file-excel-o fa-lg' data-toggle='tooltip' data-placement='bottom' title='Excel'></i>"
				text: "<i class='fa fa-file-excel-o fa-lg'></i>",
				title: "Prodfy Mudas - Colaboradores",
				exportOptions: { columns: [ 2, 3, 4, 5, 6 ] }
			},
//			{
//				extend: "pdf",
//				className: "btn-sm",
//				//text: "<i class='fa fa-file-pdf-o fa-lg' data-toggle='tooltip' data-placement='bottom' title='PDF'></i>"
//				text: "<i class='fa fa-file-pdf-o fa-lg'></i>",
//				title: "Prodfy Mudas - Colaboradores",
//				exportOptions: { columns: [ 2, 3, 4, 5, 6 ] }
//			},
			{
				extend: "print",
				className: "btn-sm",
				//text: "<i class='fa fa-print fa-lg' data-toggle='tooltip' data-placement='bottom' title='Imprimir'></i>"
				text: "<i class='fa fa-print fa-lg'></i>",
				title: "Prodfy Mudas - Colaboradores",
				exportOptions: { columns: [ 2, 3, 4, 5, 6 ] }
			},
		],
		responsive: true,
		fixedHeader: true,
		select: true,
		//info:true,
		language: 
		{
			    sEmptyTable: "Nenhum registro encontrado",
			          sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
			     sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
			  sInfoFiltered: "(Filtrados de _MAX_ registros)",
			   sInfoPostFix: "",
			 sInfoThousands: ".",
			    sLengthMenu: "_MENU_ Resultados por página",
			sLoadingRecords: "Carregando...",
			    sProcessing: "Processando...",
			   sZeroRecords: "Nenhum registro encontrado",
			        sSearch: "Pesquisar",
			      oPaginate: {
													    sNext: "<i class='fa fa-arrow-circle-right fa-lg'></i>",
													sPrevious: "<i class='fa fa-arrow-circle-left fa-lg'></i>",
													   sFirst: "Primeiro",
													    sLast: "Último"
												},
			          oAria: {
			          					 sSortAscending: ": Ordenar colunas de forma ascendente",
			          					sSortDescending: ": Ordenar colunas de forma descendente"
			          				}
			          				,
			        buttons: {
												  copyTitle: 'Copiado para o clipboard',
												   copyKeys: 'Pressione <i>ctrl</i> ou <i>\u2318</i> + <i>C</i> para copiar os dados da tabela para o clipboard. <br><br>Para cancelar, clique sobre esta mensagem ou pressione ESC.',
												copySuccess: {
																				_: 'Copiados %d registros',
																				1: 'Copiado 1 registro'
																		}
												}
		
		},
		
		'order': [[ 2, 'asc' ]],
		'columnDefs': [
			{ 
				'targets': 0,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '10px',
				'render': function (data,type, full, meta){ return '<div class="checkbox checkbox-primary"><input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '"></div>'; }
			},
			{
				'targets': 1,
				'searchable': false,
				'orderable': false, 
				'className': 'dt-body-center',
				'width': '50px'
			}
		]
		
	});
	
	// Handle click on "Select all" control
	$('#select-all').on('click', function()
	{
		// Get all rows with search applied
		//var rows = myTable.rows({ 'search': 'applied' }).nodes();
		var rows = myTable.rows({ 'page':'current', 'filter': 'applied' }).nodes();
		// Check/uncheck checkboxes for all rows in the table
		$('input[type="checkbox"]', rows).prop('checked', this.checked);
	});

	// Handle click on checkbox to set state of "Select all" control
	$('#main-datatable tbody').on('change', 'input[type="checkbox"]', function()
	{
		// If checkbox is not checked
		if(!this.checked)
		{
			var el = $('#select-all').get(0);
			// If "Select all" control is checked and has 'indeterminate' property
			if(el && el.checked && ('indeterminate' in el))
			{
				// Set visual state of "Select all" control 
				// as 'indeterminate'
				el.indeterminate = true;
			}
		}
	});
	
	$('#main-datatable').on( 'page.dt', function () 
	{
		$('#select-all').prop('checked', false);
	});
	////////////////////// Datatable ~ STOP
	
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var onlyNumbersReg = /^\\d*$/;
	var emailRegex = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	//var re = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var apenasLetrasNumerosUnderlinesRegex = /^\w+$/;//apenas letras, numeros e underlines
	var apenasNumerosRegex = /^\\d*$/;
	// regex - pelo menos 6 dígitos, conter pelo menos 1 numero, 1 letra minuscula, 1 letra maiuscula
	var passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}/; 
	var usernameRegex = /^\w+$/;//apenas letras, numeros e underlines
	
	
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	////////////////////// Processa Form ~ START
	
	//USERNAME
	$(document).on("blur", "#reg_username", function()
	{
		if( !frm_valida_username(this.value,true) ){ return false; }
	});
	function frm_valida_username(value,ret_msg)
	{
		var _tmp = value;
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( (_tmp.length < 6) || (_tmp.length > 30) )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um USUÁRIO válido!',' ','alert');
			}
			return false;
		}
		else if(!usernameRegex.test(_tmp))
		{ 
			show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert');
			return false;
		}
		return true;
	}
	function checa_existe_colab_username(_username,_cpf)
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("u", _username);
		_postdata.append("c", _cpf);
		
		//alert(base_url + "/lib-bin/existe_colab_username.php?"+_postdata);
		
		$.ajax({
				 url: base_url + "/lib-bin/existe_colab_username.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						return "existe";
					}
					else
					{
						return "nao_existe";
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	function checa_existe_colab(_username,_cpf)
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("u", _username);
		_postdata.append("c", _cpf);
		
		//alert(base_url + "/lib-bin/existe_colab.php?"+_postdata);
		
		$.ajax({
				 url: base_url + "/lib-bin/existe_colab.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						return "existe";
					}
					else
					{
						return "nao_existe";
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//CPF
	$(document).on("blur", "#reg_cpf", function()
	{
		setContent($('#reg_cpf'), 'CPF');
		if( !frm_valida_cpf(this.value,true) ){ return false; }
	});
	function frm_valida_cpf(value,ret_msg)
	{
		var _tmp = value;
		
		if( isEmpty(_tmp) )
		{
			if( ret_msg )
			{
				show_msgbox('O campo CPF é obrigatório!',' ','alert');
			}
			return false;
		}
		else if( chk_cpf(_tmp) == false )
		{
			if( ret_msg )
			{
				show_msgbox('Entre com um CPF válido!',' ','alert');
			}
			return false;
		}
		return true;
	}
	function chk_cpf(s)
	{
		var i;
		var c = s.substr(0,9);
		var dv = s.substr(9,2);
		var d1 = 0;
		//
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(10-i); }
		if (d1 == 0) return false;
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(0) != d1){ return false; }
		d1 *= 2;
		for (i = 0; i < 9; i++){ d1 += c.charAt(i)*(11-i); }
		d1 = 11 - (d1 % 11);
		if (d1 > 9) d1 = 0;
		if (dv.charAt(1) != d1){ return false; }
		return true;
	}
	
	////////////////////// Form ~ START
	var $formRegister = $('#cad-form');
	var $divForms = $('#div-forms');
	var $modalAnimateTime = 300;
	var $msgAnimateTime = 150;
	var $msgShowTime = 2000;
	
	//Clear Inputs
	function clear_inputs()
	{
		$('#reg_username').val('');
		$('#reg_cpf').val('');
		$('#reg_situacao').val('');
		var acesso_count = $('#reg_acesso_count').val();
		for(i=1;i<=acesso_count;i++)
		{
			$('#reg_acesso_ind_acessar_'+i).prop('checked', false);
			$('#reg_acesso_ind_editar_'+i).prop('checked', false);
			$('#reg_acesso_ind_apagar_'+i).prop('checked', false);
		}
	}
	// Registrar
	$(document).on("f_show_register", function(e)
	{
		$('#div-label-type').text('NOVO REGISTRO');
		$('#bt_register').show();
		$('#bt_save').hide();
		$('#reg_id').val('');
		$('#reg_username').val('');
		$('#reg_cpf').val('');
		$('#reg_situacao').val('');
		var acesso_count = $('#reg_acesso_count').val();
		for(i=1;i<=acesso_count;i++)
		{
			$('#reg_acesso_ind_acessar_'+i).prop('checked', false);
			$('#reg_acesso_ind_editar_'+i).prop('checked', false);
			$('#reg_acesso_ind_apagar_'+i).prop('checked', false);
		}
		$('#cad-modal').modal('show');
		return false;
	});
	$('#bt_register').click( function () 
	{
		//Ajuste de conteudo
		setContent($('#reg_cpf'), 'CPF');
		
		var rg_username          = $('#reg_username').val();
		var rg_cpf               = $('#reg_cpf').val();
		var rg_situacao          = $('#reg_situacao').val();
		var rg_acesso_count      = $('#reg_acesso_count').val();
		
		//Username
		if (isEmpty(rg_username)) { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert');           $('#reg_username').focus();     return false; }
		if(!usernameRegex.test(rg_username)) { show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert'); $('#reg_username').focus();     return false; }
		
		if (isEmpty(rg_cpf))          { show_msgbox('O campo CPF é obrigatório!',' ','alert'); $('#reg_cpf').focus(); return false; }
		if( !frm_valida_cpf(rg_cpf,true) ){ return false; }
		
		if (isEmpty(rg_situacao)) { show_msgbox('O campo SITUAÇÃO é obrigatório!',' ','alert'); $('#reg_situacao').focus(); return false; }
		
		if( checa_existe_colab_username(rg_username,rg_cpf) == "nao_existe" ) { show_msgbox('Usuário <b>'+rg_username+'</b> inexistente!',' ','alert'); $('#reg_username').focus();     return false; }
		
		if( checa_existe_colab(rg_username,rg_cpf) == "existe" ) { show_msgbox('Usuário <b>'+rg_username+'</b> já registrado como colaborador!',' ','alert'); $('#reg_username').focus();     return false; }
		
		//Formatacao
		rg_username  = rg_username.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "2");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("username", rg_username);
		_postdata.append("cpf", rg_cpf);
		_postdata.append("sit", rg_situacao);
		
		//Formata postdata dos niveis de acesso
		var tmp_codigo = '',
		    tmp_ind_acessar_tipo = '',
		    tmp_ind_acessar = '',
		    tmp_ind_editar_tipo = '',
		    tmp_ind_editar = '',
		    tmp_ind_apagar_tipo = '',
		    tmp_ind_apagar = ''
		;
		
		_postdata.append("act", rg_acesso_count);
		
		for(i=0;i<=rg_acesso_count;i++)
		{
			//codigo
			tmp_codigo = $('#reg_acesso_codigo_'+i).val();
			//ind_acessar
			tmp_ind_acessar_tipo = $('#reg_acesso_ind_acessar_tipo_'+i).val();
			if(tmp_ind_acessar_tipo == 0){ tmp_ind_acessar = 0; } else { if( $('#reg_acesso_ind_acessar_'+i).is(':checked') == true ){ tmp_ind_acessar = 1; } else { tmp_ind_acessar = 0; } }
			//ind_editar
			tmp_ind_editar_tipo = $('#reg_acesso_ind_editar_tipo_'+i).val();
			if(tmp_ind_editar_tipo == 0){ tmp_ind_editar = 0; } else { if( $('#reg_acesso_ind_editar_'+i).is(':checked')  == true ){ tmp_ind_editar = 1; } else { tmp_ind_editar = 0; } }
			//ind_apagar
			tmp_ind_apagar_tipo = $('#reg_acesso_ind_apagar_tipo_'+i).val();
			if(tmp_ind_apagar_tipo == 0){ tmp_ind_apagar = 0; } else { if( $('#reg_acesso_ind_apagar_'+i).is(':checked')  == true ){ tmp_ind_apagar = 1; } else { tmp_ind_apagar = 0; } }
			
			_postdata.append("cdg_"+i, tmp_codigo);
			_postdata.append("acs_"+i, tmp_ind_acessar);
			_postdata.append("edt_"+i, tmp_ind_editar);
			_postdata.append("apg_"+i, tmp_ind_apagar);
		}
		
		//alert("http://mudas.prodfy.com.br/lib-bin/mdl-cfg-colabs.php?"+_postdata);
		//exit;
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		//alert(base_url + "/lib-bin/mdl-cfg-colabs.php?"+_postdata);
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Clear
						clear_inputs();
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						$('#cad-modal').scrollTop(0);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						refresh_datatable();
						//refresh_msgs_module();
						//$(this).trigger('f_refresh_msgs_module');
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	// Editar
	$(document).on("f_show_edit", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de executar edição do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "6");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//alert(base_url + "/lib-bin/existe_colab.php?"+_postdata);
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], username:tmp[3], cpf:tmp[4], sit:tmp[5], acesso:tmp[6]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						//Alimenta dados
						$('#div-label-type').text('EDITAR REGISTRO');
						$('#bt_register').hide();
						$('#bt_save').show();
						$('#reg_id').val(_id);
						$('#reg_username').val(myRet.username);
						$('#reg_cpf').val(myRet.cpf);
						$('#reg_situacao').val(myRet.sit);
						//
						
						//Matriz de acesso
						if(myRet.acesso)
						{
							var acessoMatrix = jQuery.parseJSON(myRet.acesso);
							if(acessoMatrix)
							{
								for (var i=0; i<acessoMatrix.length;i++)
								{
									var tmp_idx = $('#idx_'+acessoMatrix[i].codigo).val();
									//temp = temp + "\nidx_"+acessoMatrix[i].codigo+": "+tmp_idx;
									
									if(acessoMatrix[i].acessar == 1){ var tmp_acessar = true; } else { var tmp_acessar = false; }
									if(acessoMatrix[i].editar == 1){ var tmp_editar = true; } else { var tmp_editar = false; }
									if(acessoMatrix[i].apagar == 1){ var tmp_apagar = true; } else { var tmp_apagar = false; }
									$('#reg_acesso_ind_acessar_'+tmp_idx).prop('checked', tmp_acessar);
									$('#reg_acesso_ind_editar_'+tmp_idx).prop('checked', tmp_editar);
									$('#reg_acesso_ind_apagar_'+tmp_idx).prop('checked', tmp_apagar);
								}
							}
						}
						//alert("temp: "+temp);
						//
						hideLoadingBox();
						$('#cad-modal').modal('show');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	$('#bt_save').click( function () 
	{
		//Ajuste de conteudo
		setContent($('#reg_cpf'), 'CPF');
		
		var rg_id                = $('#reg_id').val();
		var rg_username          = $('#reg_username').val();
		var rg_cpf               = $('#reg_cpf').val();
		var rg_situacao          = $('#reg_situacao').val();
		var rg_acesso_count      = $('#reg_acesso_count').val();
		
		//ID
		if (isEmpty(rg_id)) { show_msgbox('Incapaz de determinar qual registro deve ser editado!',' ','error'); $('#reg_username').focus(); return false; }
		
		//Username
		if (isEmpty(rg_username)) { show_msgbox('O campo USUÁRIO é obrigatório!',' ','alert'); $('#reg_username').focus(); return false; }
		if(!usernameRegex.test(rg_username)) { show_msgbox('O campo USUÁRIO deve conter apenas letras, números e underlines. Favor tentar novamente!',' ','alert'); $('#reg_username').focus(); return false; }
		
		if (isEmpty(rg_cpf)) { show_msgbox('O campo CPF é obrigatório!',' ','alert'); $('#reg_cpf').focus(); return false; }
		if( !frm_valida_cpf(rg_cpf,true) ){ return false; }
		
		if (isEmpty(rg_situacao)) { show_msgbox('O campo SITUAÇÃO é obrigatório!',' ','alert'); $('#reg_situacao').focus(); return false; }
		
		if( checa_existe_colab_username(rg_username,rg_cpf) == "nao_existe" ) { show_msgbox('Usuário <b>'+rg_username+'</b> inexistente!',' ','alert'); $('#reg_username').focus();     return false; }
		
		if( checa_existe_colab(rg_username,rg_cpf) == "existe" ) { show_msgbox('Usuário <b>'+rg_username+'</b> já registrado como colaborador!',' ','alert'); $('#reg_username').focus();     return false; }
		
		//Formatacao
		rg_username  = rg_username.toUpperCase();
		
		//Show trobbler
		showLoadingBox('Processando...');
		
		//disabled all the text fields
		$('.form-control').attr('disabled','true');
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "3");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", rg_id);
		_postdata.append("username", rg_username);
		_postdata.append("cpf", rg_cpf);
		_postdata.append("sit", rg_situacao);
		
		//Formata postdata dos niveis de acesso
		var tmp_codigo = '',
		    tmp_ind_acessar_tipo = '',
		    tmp_ind_acessar = '',
		    tmp_ind_editar_tipo = '',
		    tmp_ind_editar = '',
		    tmp_ind_apagar_tipo = '',
		    tmp_ind_apagar = ''
		;
		
		_postdata.append("act", rg_acesso_count);
		
		for(i=0;i<=rg_acesso_count;i++)
		{
			//codigo
			tmp_codigo = $('#reg_acesso_codigo_'+i).val();
			//ind_acessar
			tmp_ind_acessar_tipo = $('#reg_acesso_ind_acessar_tipo_'+i).val();
			if(tmp_ind_acessar_tipo == 0){ tmp_ind_acessar = 0; } else { if( $('#reg_acesso_ind_acessar_'+i).is(':checked') == true ){ tmp_ind_acessar = 1; } else { tmp_ind_acessar = 0; } }
			//ind_editar
			tmp_ind_editar_tipo = $('#reg_acesso_ind_editar_tipo_'+i).val();
			if(tmp_ind_editar_tipo == 0){ tmp_ind_editar = 0; } else { if( $('#reg_acesso_ind_editar_'+i).is(':checked')  == true ){ tmp_ind_editar = 1; } else { tmp_ind_editar = 0; } }
			//ind_apagar
			tmp_ind_apagar_tipo = $('#reg_acesso_ind_apagar_tipo_'+i).val();
			if(tmp_ind_apagar_tipo == 0){ tmp_ind_apagar = 0; } else { if( $('#reg_acesso_ind_apagar_'+i).is(':checked')  == true ){ tmp_ind_apagar = 1; } else { tmp_ind_apagar = 0; } }
			
			_postdata.append("cdg_"+i, tmp_codigo);
			_postdata.append("acs_"+i, tmp_ind_acessar);
			_postdata.append("edt_"+i, tmp_ind_editar);
			_postdata.append("apg_"+i, tmp_ind_apagar);
		}
		
		$(".form-control").removeAttr("disabled");
		
		//Debug
		//show_msgbox(base_url + "/lib-bin/gera_lista_cidades.php?"+_postdata,' ','alert');
		//return;
		
		//alert(base_url + "/lib-bin/mdl-cfg-colabs.php?"+_postdata);
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						$('#cad-modal').modal('hide');
						
						//Clear
						clear_inputs();
						
						//Refresh datatable
						refresh_datatable();
						
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
					else
					{
						$(".form-control").removeAttr("disabled");
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	// Refresh datatable
	function refresh_datatable()
	{
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "99");
		_postdata.append("l", $.Storage.loadItem('language'));
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2], btn_link:tmp[3], total_recs:tmp[4]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						myTable.clear();
						$("#select-all").prop('checked', false);
						myTable.rows.add( $(myRet.text) ).draw();
						
						//Atualiza add button
						$('#add_button_link').empty().append(myRet.btn_link);
						
						//Atualiza total de registros
						if(isEmpty(myRet.total_recs)){ myRet.total_recs = 0; }
						$('#datatable_total_recs').val(myRet.total_recs);
					}
					else
					{
						//alert('JOSTA => '+myRet.text);
						return false;
					}
					
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	}
	
	//Ficha Consolidada
	$(document).on("f_show_ficha", function(e,_id)
	{
		if(isEmpty(_id))
		{
			show_msgbox('Incapaz de carregar ficha do registro!',' ','error');
			return false;
		}
		
		//Formata postdata
		var _postdata = new FormData();
		_postdata.append("dbo", "7");
		_postdata.append("l", $.Storage.loadItem('language'));
		_postdata.append("rid", _id);
		
		//alert(base_url + "/lib-bin/mdl-cfg-colabs.php?"+_postdata);
		
		//Show trobbler
		showLoadingBox('Carregando...');
		
		$.ajax({
				 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
				type: "post",
				//dataType: "json",
				data: _postdata,
				processData: false,
				contentType: false,
				cache: false,
				//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR)
				{
					var tmp   = response.split('|');
					var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
					if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
					//
					if(myRet.status == 1)
					{
						hideLoadingBox();
						show_msgbox(myRet.text,'Ficha do Colaborador','ficha');
					}
					else
					{
						hideLoadingBox();
						show_msgbox(myRet.text,' ',myRet.msgbox_type);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown)
				{
						console.log("The following error occured: "+textStatus, errorThrown);
						hideLoadingBox();
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){},
				statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
		});
		
		return false;
	});
	
	//Delete Item
	$(document).on("f_show_del", function(e)
	{
		//Verifica se existe algum checkbox marcado para delecao
		var total_recs = $('#datatable_total_recs').val();
		if(isEmpty(total_recs)){ total_recs=0; }
		
		
		var allChks = [];
		$(".dt_checkbox:checked").each(function() 
		{ 
			if(!isEmpty($(this).attr('data-id'))){ allChks.push($(this).attr('data-id')); } 
		});
		
		if(allChks.length <= 0)
		{
			show_msgbox('Nenhum registro selecionado!',' ','alert');
			return false;
		}
		
		var confirmDiag = BootstrapDialog.confirm(
		{
               title: 'ATENÇÃO!',
			       message: 'Os registros selecionados serão apagados. Confirma?',
			          type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
			      closable: true, // <-- Default value is false
     //closeByBackdrop: false,
     //closeByKeyboard: false,
           draggable: false, // <-- Default value is false
			btnCancelLabel: 'Não', // <-- Default value is 'Cancel',
			    btnOKLabel: 'Sim', // <-- Default value is 'OK',
			    btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
			      callback: function(result) 
			{
				// result will be true if button was click, while it will be false if users close the dialog directly.
				if(result) 
				{ 
					var delIDs = allChks.join(',');
					confirmDiag.close();
					//show_msgbox('Pronto para deletar!{'+tmp+'}',' ','alert');
					
					//Processa delete dos registros selecionados
					//Formata postdata
					var _postdata = new FormData();
					_postdata.append("dbo", "4");
					_postdata.append("l", $.Storage.loadItem('language'));
					_postdata.append("rids", delIDs);
					
					//alert(base_url + "/lib-bin/mdl-cfg-colabs.php?"+_postdata);
					
					//Show trobbler
					showLoadingBox('Processando...');
					
					$.ajax({
							 url: base_url + "/lib-bin/mdl-cfg-colabs.php",
							type: "post",
							//dataType: "json",
							data: _postdata,
							processData: false,
							contentType: false,
							cache: false,
							//scriptCharset: "utf-8",
							success: function(response, textStatus, jqXHR)
							{
								var tmp   = response.split('|');
								var myRet = {status:tmp[0], text:tmp[1], msgbox_type:tmp[2]};
								if(isEmpty(myRet.msgbox_type)){ myRet.msgbox_type = 'alert'; }
								//
								if(myRet.status == 1)
								{
									refresh_datatable();
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
								else
								{
									hideLoadingBox();
									show_msgbox(myRet.text,' ',myRet.msgbox_type);
									return false;
								}
							},
							error: function(jqXHR, textStatus, errorThrown)
							{
									console.log("The following error occured: "+textStatus, errorThrown);
									hideLoadingBox();
									show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
									return false;
							},
							complete: function(){},
							statusCode: { 404: function() { hideLoadingBox(); show_msgbox_pagenotfound(); return false; } }
					});
					
					return false;
				}
				else
				{
					confirmDiag.close();
					return false;
				}
			}
		});
		
		return false;
	});
	
	function modalAnimate ($oldForm, $newForm) 
	{
		var $oldH = $oldForm.height();
		var $newH = $newForm.height();
		$divForms.css("height",$oldH);
		$oldForm.fadeToggle($modalAnimateTime, function()
		{
			$divForms.animate({height: $newH}, $modalAnimateTime, function()
			{
				$newForm.fadeToggle($modalAnimateTime);
			});
		});
	}

	function msgFade ($msgId, $msgText) 
	{
		$msgId.fadeOut($msgAnimateTime, function() 
		{
			$(this).text($msgText).fadeIn($msgAnimateTime);
		});
	}

	function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) 
	{
		var $msgOld = $divTag.text();
		msgFade($textTag, $msgText);
		$divTag.addClass($divClass);
		$iconTag.removeClass("glyphicon-chevron-right");
		$iconTag.addClass($iconClass + " " + $divClass);
		setTimeout(function() 
		{
			msgFade($textTag, $msgOld);
			$divTag.removeClass($divClass);
			$iconTag.addClass("glyphicon-chevron-right");
			$iconTag.removeClass($iconClass + " " + $divClass);
		}, $msgShowTime);
	}
	////////////////////// Form ~ STOP
	
	
});