/* #####################################################################
   # KONDOO - contact
   ##################################################################### */
   
$(document).ready(function()
{
	//Setup
	var base_url = '//meu.kondoo.me';
	var img_url  = base_url + '/img';
	
	//Cache Captcha Loading Image
	var captcha_loading_src = new Image();
	captcha_loading_src.src = base_url + '/img/captcha_loading.png';
	
	
	////////////////////// CARREGAMENTO ~ START
	$('#loadingBox').ajaxStart(function() 
	{
		$('#loadingBox').modal('show');
	}).ajaxStop(function() {
		$('#loadingBox').modal('hide');
	});
	//////////////////////// CARREGAMENTO ~ STOP
	
	
	////////////////////// isEmpty ~ START
	function isEmpty(str) 
	{
		//alert(str + ' => '+ (typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null));
		return typeof str == 'string' && !str.trim() || typeof str == 'undefined' || str === null;
	}
	////////////////////// isEmpty ~ STOP
	
	
	////////////////////// IDIOMA INICIAL ~ START
	//Idioma inicial
	if($.Storage.loadItem('language') == '' || 
	   $.Storage.loadItem('language') == undefined)
	{
		switch( $.Storage.loadItem('language') )
		{
			case 'us':
			case 'en-us':
				$.Storage.saveItem('language', 'en-us');
				break;
			case 'sp':
			case 'es':
			case 'es-es':
				$.Storage.saveItem('language', 'es-es');
				break;
			case 'br':
			case 'pt-br':
			default:
				$.Storage.saveItem('language', 'pt-br');
				break;
		}
	}
	//alert("language => "+$.Storage.loadItem('language'));
	////////////////////// IDIOMA INICIAL ~ STOP
	
	
	////////////////////// KEYPRESS CONTROL ~ START
	$(document).on("keypress", ".numeric", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			return;
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".currency", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
				(48 <= event.which && event.which <= 57) || // Always 1 through 9
				//(48 == event.which && $(this).attr("value")) || // No 0 first digit
				isControlKey) { // Opera assigns values for control keys.
			//return;
			return(currencyFormat(this,'.',',',event));
		} else {
			event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (40 <= event.which && event.which <= 41) || // ),(
		    (45 <= event.which && event.which <= 45) || // -
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (32 <= event.which && event.which <= 32) || // space
		    (65 <= event.which && event.which <= 90) || // a-z
		    (95 <= event.which && event.which <= 95) || // _
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (192 <= event.which && event.which <= 196) || // A-Z
		    (199 <= event.which && event.which <= 207) || // A-Z
		    (209 <= event.which && event.which <= 214) || // A-Z
		    (217 <= event.which && event.which <= 220) || // A-Z
		    (224 <= event.which && event.which <= 228) || // A-Z
		    (231 <= event.which && event.which <= 239) || // A-Z
		    (241 <= event.which && event.which <= 246) || // A-Z
		    (249 <= event.which && event.which <= 252) || // A-Z
		    //(48 == event.which && $(this).attr("value")) || // No 0 first digit
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".textonly_az", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".logintext", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".codigobanco", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    (65 <= event.which && event.which <= 90) || // a-z
		    (97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	$(document).on("keypress", ".numeric_point", function(event)
	{
		// Backspace, tab, enter, end, home, left, right
		// We don't support the del key in Opera because del == . == 46.
		var controlKeys = [8, 9, 13, 35, 36, 37, 39];
		// IE doesn't support indexOf
		var isControlKey = controlKeys.join(",").match(new RegExp(event.which));
		// Some browsers just don't raise events for control keys. Easy.
		// e.g. Safari backspace.
		if (!event.which || // Control keys in most browsers. e.g. Firefox tab is 0
		    //(65 <= event.which && event.which <= 90) || // a-z
		    //(97 <= event.which && event.which <= 122) || // A-Z
		    (48 <= event.which && event.which <= 57) || // 1-9
		    (46 <= event.which && event.which <= 46) || // .(ponto)
		    //(45 <= event.which && event.which <= 45) || // -
		    //(95 <= event.which && event.which <= 95) || // _
		    isControlKey) { // Opera assigns values for control keys.
		  return;
		} else {
		  event.preventDefault();
		}
	});
	////////////////////// KEYPRESS CONTROL ~ STOP
	
	
	////////////////////// MSGBOX ~ START
	$(document).on('show.bs.modal', '.modal', function (event) 
	{
		var zIndex = 1040 + (10 * $('.modal:visible').length);
		$(this).css('z-index', zIndex);
		setTimeout(function() 
		{
			$('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		}, 0);
	});
	function show_msgbox(_txt,_title,_type)
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		switch (_type) 
		{
			case 'alert':
				$("#msgBoxIconAlert").show();
				if(isEmpty(_title)){ _title = 'Alerta'; }
				break;
			case 'info':
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'confirm':
				$("#msgBoxIconConfirm").show();
				if(isEmpty(_title)){ _title = 'Confirma?'; }
				break;
			case 'error':
				$("#msgBoxIconError").show();
				if(isEmpty(_title)){ _title = 'Erro'; }
				break;
			case 'prompt':
				$("#msgBoxIconPrompt").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'success':
				$("#msgBoxIconSuccess").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
			case 'warning':
				$("#msgBoxIconWarning").show();
				if(isEmpty(_title)){ _title = 'Aviso'; }
				break;
			default:
				$("#msgBoxIconInfo").show();
				if(isEmpty(_title)){ _title = ''; }
				break;
		}
		
		//_title = _type;
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		//$('#msgBox').modal({backdrop: 'static', show:true});
		$('#msgBox').modal({backdrop: 'stacked', show:true});
		
	}
	function show_msgbox_pagenotfound()
	{
		//Hide Icons
		$("#msgBoxIconAlert").hide();
		$("#msgBoxIconInfo").hide();
		$("#msgBoxIconConfirm").hide();
		$("#msgBoxIconError").hide();
		$("#msgBoxIconPrompt").hide();
		$("#msgBoxIconSuccess").hide();
		$("#msgBoxIconWarning").hide();
		
		//Set type
		$("#msgBoxIconError").show();
		
		switch ($.Storage.loadItem('language')) 
		{
			case 'br':
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
			case 'us':
				_title = 'Error!';
				_txt = 'Page not found!';
				break;
			case 'sp':
				_title = 'Erro!';
				_txt = 'Página no encontrada!';
				break;
			default:
				_title = 'Erro!';
				_txt = 'Página não encontrada!';
				break;
		}
		
		$('#msgBoxLabel').text(_title);
		$('#msgBoxBody').empty().append(_txt);
		$('#msgBox').modal({backdrop: 'stacked', show:true});
	}
	////////////////////// MSGBOX ~ STOP
	
	
	////////////////////// CAPTCHA ~ START
	$(document).on("click", "#contact_captcha_refresh_btn", function()
	{
		$("#contact_captcha_code").val('');
		$('#contact_captcha_img').attr('src', captcha_loading_src.src);
		var numRand = Math.floor(Math.random()*101);
		var captcha_src = base_url + '/lib-bin/captcha/captcha.cgi?'+numRand;
		$('#contact_captcha_img').attr('src', captcha_src);
		return false;
	});
	////////////////////// CAPTCHA ~ STOP
	
	
	////////////////////// VALIDACOES DE FORM ~ START
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	var onlyNumbersReg = /^\\d*$/;
	
	// Checa email
	function chk_mail(mail) 
	{
		if ( !/^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)\$/.test(mail)) 
		{
			return false;
		}
		return true;
	}//endfunc
	
	////////////////////// VALIDACOES DE FORM ~ STOP
	
	
	
	
	//$(document).on("keyup", "#nome", function(){ this.value = this.value.toUpperCase(); });
	//$(document).on("keyup", "#email", function(){ this.value = this.value.toLowerCase(); });
	
	////////////////////// SEND_CONTACT ~ START
	$(document).on("f_send_contact", function()
	{
		var _name = $('#name').val();
		if(isEmpty(_name))
		{
			show_msgbox('O campo NOME é obrigatório!',' ','alert');
			return false;
		}
		var _email = $('#email').val();
		if(isEmpty(_email))
		{
			show_msgbox('O campo E-MAIL é obrigatório!',' ','alert');
			return false;
		}
		if( !emailReg.test(_email) )
		{
			show_msgbox('O E-MAIL informado é inválido!',' ','alert');
			return false;
		}
		var _subject = $('#subject').val();
		if(isEmpty(_subject))
		{
			show_msgbox('O campo ASSUNTO é obrigatório!',' ','alert');
			return false;
		}
		var _msg = $('#msg').val();
		if(isEmpty(_msg))
		{
			show_msgbox('O campo MENSAGEM é obrigatório! ('+_msg + ')',' ','alert');
			return false;
		}
		
		var width  = $('#loading-panel').width();
		var height = $('#loading-panel').height();
		$("#loading-panel").css({ top: '50%', left: '50%',position: 'fixed' });
		$("#report-loading").css({ top: ((height / 2) - 25), left: ((width / 2) - 50) }).fadeIn(200);// fast fade in of 200 mili-seconds
		
		var _postdata = "name="             + encodeURIComponent(_name)
		              + "&email="           + encodeURIComponent(_email)
		              + "&subject="         + encodeURIComponent(_subject)
		              + "&msg="             + encodeURIComponent(_msg)
		              ;
		do_bt_send_contact_save(_postdata);
		
		return false;
	});
	function do_bt_send_contact_save(_data)
	{
		//show_msgbox(base_url + "/lib-bin/do_send_contact.php?"+_data,' ','alert');
		//return;
		
		$.ajax({
				 url: base_url + "/lib-bin/do_send_contact.php",
				type: "post",
				data: _data,
			 cache: false,
			//scriptCharset: "utf-8",
				success: function(response, textStatus, jqXHR){
					var myResponse = response.split('|');
					if(isEmpty(myResponse[2])){ myResponse[2] = 'alert'; }
					if(myResponse[0] == 1)
					{
						clear_bt_send_contact_save();
						$("#report-loading").fadeOut(500);
						show_msgbox(myResponse[1],' ',myResponse[2]);
						return false;
					}
					else
					{
						$("#report-loading").fadeOut(500);
						show_msgbox(myResponse[1],' ',myResponse[2]);
						return false;
					}
				},
				error: function(jqXHR, textStatus, errorThrown){
						console.log("The following error occured: "+textStatus, errorThrown);
						$("#report-loading").fadeOut(500);
						show_msgbox(textStatus + ' - ' + errorThrown,' ','error');
						return false;
				},
				complete: function(){
				},
				statusCode: {
					404: function() {
						$("#report-loading").fadeOut(500);
						show_msgbox_pagenotfound();
						return false;
					}
				}
		});
	}
	function clear_bt_send_contact_save()
	{
		$('#name').val('');
		$('#email').val('');
		$('#subject').val('');
		$('#msg').val('');
	}
	////////////////////// SEND_CONTACT ~ STOP

	
	
})

