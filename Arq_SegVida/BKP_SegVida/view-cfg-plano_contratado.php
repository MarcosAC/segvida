<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-cfg-plano_contratado.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
<!-- CSS - Plano Contratado -->
		<style>
@import url(http://fonts.googleapis.com/css?family=Roboto);

* {
    font-family: 'Roboto', sans-serif;
}

#comprar-modal .modal-dialog {
    width: 350px;
}

#comprar-modal input[type=text], input[type=password] {
	margin-top: 10px;
}

.form-control-select {
	margin-top: 10px;
}

select {
	border-radius: 0px;
	margin-top: 10px;
}

#div-login-msg,
#div-lost-msg,
#div-register-msg {
    border: 1px solid #dadfe1;
    height: 30px;
    line-height: 28px;
    transition: all ease-in-out 500ms;
}

#div-login-msg.success,
#div-lost-msg.success,
#div-register-msg.success {
    border: 1px solid #68c3a3;
    background-color: #c8f7c5;
}

#div-login-msg.error,
#div-lost-msg.error,
#div-register-msg.error {
    border: 1px solid #eb575b;
    background-color: #ffcad1;
}

#icon-login-msg,
#icon-lost-msg,
#icon-register-msg {
    width: 30px;
    float: left;
    line-height: 28px;
    text-align: center;
    background-color: #dadfe1;
    margin-right: 5px;
    transition: all ease-in-out 500ms;
}

#icon-login-msg.success,
#icon-lost-msg.success,
#icon-register-msg.success {
    background-color: #68c3a3 !important;
}

#icon-login-msg.error,
#icon-lost-msg.error,
#icon-register-msg.error {
    background-color: #eb575b !important;
}

#img_logo {
    max-height: 100px;
    max-width: 100px;
}

/* #########################################
   #    override the bootstrap configs     #
   ######################################### */

.modal-backdrop.in {
    filter: alpha(opacity=50);
    opacity: .8;
}

.modal-content {
    background-color: #ececec;
    border: 1px solid #bdc3c7;
    border-radius: 0px;
    outline: 0;
}

.modal-header {
    min-height: 16.43px;
    padding: 15px 15px 15px 15px;
    border-bottom: 0px;
}

.modal-body {
    position: relative;
    padding: 5px 15px 5px 15px;
}

.modal-footer {
    padding: 15px 15px 15px 15px;
    text-align: left;
    border-top: 0px;
}

.checkbox {
    margin-bottom: 0px;
}

.btn {
    border-radius: 0px;
}

.btn:focus,
.btn:active:focus,
.btn.active:focus,
.btn.focus,
.btn:active.focus,
.btn.active.focus {
    outline: none;
}

.btn-lg, .btn-group-lg>.btn {
    border-radius: 0px;
}

.btn-link {
    padding: 5px 10px 0px 0px;
    color: #95a5a6;
}

.btn-link:hover, .btn-link:focus {
    color: #2c3e50;
    text-decoration: none;
}

.glyphicon {
    top: 0px;
}

.form-control {
  border-radius: 0px;
}
.input-upper {
  text-transform: uppercase;
}
.input-lower {
  text-transform: lowercase;
}

#div-label-type 
{
	border: 1px solid #003a1f;
	height: 30px;
	line-height: 28px;
	transition: all ease-in-out 500ms;
	text-align: center;
	font-size: 90%;
	font-weight: bold;
	color: #003a1f;
	margin-bottom: 10px;
}
			
			
* {
    box-sizing: border-box;
}

.columns {
    float: left;
    width: 33.3%;
    padding: 8px;
}

.price_table {
    list-style-type: none;
    border: 1px solid #eee;
    margin: 0;
    padding: 0;
    -webkit-transition: 0.3s;
    transition: 0.3s;
    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2);
    color: #434a54;
}

//.price_table:hover {
//    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
//}

.price_table .header {
    background-color: #111;
    color: white;
    font-size: 20px;
}

.price_table li {
    border-bottom: 1px solid #eee;
    padding: 10px;
    text-align: center;
}

.price_table .grey {
    background-color: #e5e5e5;
    font-size: 35px;
    color:#434a54;
}

.button {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 10px 35px;
    text-align: center;
    text-decoration: none;
    font-size: 18px;
    border-radius: 4px;
}

@media only screen and (max-width: 600px) {
    .columns {
        width: 100%;
    }
}



		</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		####
		# Carrega dados dos planos
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Prepara query
			$QUERY = "SELECT `codigo` AS codigo, `tipo` AS tipo, 
	              CASE WHEN '".$SID_CTRL->getIDIOMA()."' = 'pt-br' THEN `titulo_br`
	                   WHEN '".$SID_CTRL->getIDIOMA()."' = 'en-us' THEN `titulo_us`
	                   WHEN '".$SID_CTRL->getIDIOMA()."' = 'es-es' THEN `titulo_sp`
	               END AS titulo,
	              FORMAT(`valor_cobranca`,2,'de_DE') AS valor_cobranca,
	              FORMAT(`valor_economia`,2,'de_DE') AS valor_economia,
	              `total_condominios` AS total_condominios, 
	              `total_colaboradores` AS total_colaboradores, 
	              `dias_gratis` AS dias_gratis 
	         FROM `SYSTEM_PLANO_PAGTO` order by `ordem`";
			if ($stmt = $mysqli->prepare($QUERY)) 
			{
				//$stmt->bind_param('s', $email);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CODIGO, $o_TIPO, $o_TITULO, $o_VALOR_COBRANCA, $o_VALOR_ECONOMIA, $o_TOTAL_CONDOMINIOS, $o_TOTAL_COLABORADORES, $o_DIAS_GRATIS);
	
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows > 0) 
				{ 
					##Corre pelas linhas encontradas
					while ($stmt->fetch()) 
					{
						$plano[$o_CODIGO]['tipo']                = $o_TIPO;
						if($o_TIPO == "M"){ $plano_tipo_descr[$o_CODIGO] = TXT_CFG_PLANO_MENSAL; }
						if($o_TIPO == "A"){ $plano_tipo_descr[$o_CODIGO] = TXT_CFG_PLANO_ANUAL; }
						$plano[$o_CODIGO]['titulo']              = $o_TITULO;
						$plano[$o_CODIGO]['valor_cobranca']      = $o_VALOR_COBRANCA;
						$plano[$o_CODIGO]['valor_economia']      = $o_VALOR_ECONOMIA;
						$plano[$o_CODIGO]['total_condominios']   = $o_TOTAL_CONDOMINIOS;
						$plano[$o_CODIGO]['total_colaboradores'] = $o_TOTAL_COLABORADORES;
						$plano[$o_CODIGO]['dias_gratis']         = $o_DIAS_GRATIS;
					}
				}
			}
			
		}
		##/carega planos
		
		####
		# Formata plano contratado info
		####
		//$plano_atual_titulo              = $plano[$plano_codigo]['titulo']." - ".$plano_tipo_descr[$plano_codigo];
		//$plano_atual_ativacao            = $plano_data_ativacao;
		//$plano_atual_expiracao           = $plano_data_expiracao;
		//$plano_atual_renovacao_com_prazo = $plano_data_renovacao_com_prazo;
		
		$plano_atual_titulo              = $plano[$SID_CTRL->getPLANO_CODIGO()]['titulo']." - ".$plano_tipo_descr[$SID_CTRL->getPLANO_CODIGO()];
		$plano_atual_ativacao            = $SID_CTRL->getPLANO_DATA_ATIVACAO();
		$plano_atual_expiracao           = $SID_CTRL->getPLANO_DATA_EXPIRACAO();
		$plano_atual_renovacao_com_prazo = $SID_CTRL->getPLANO_DATA_RENOVACAO_COM_PRAZO();
		
		
		
		####
		# Carrega dados do plano a contratar caso situacao do plano não seja ativo (A)
		####
		if($SID_CTRL->getPLANO_SITUACAO() != 'A')
		{
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"      SELECT PP.`codigo` AS codigo, C.`pagto_token` as pagto_token
		           FROM `SYSTEM_PLANO_PAGTO` PP
		     INNER JOIN `SYSTEM_CLIENTE` C
		             ON C.`pagto_plano_codigo` = PP.`codigo`
		          WHERE C.`idSYSTEM_CLIENTE` = ?
		          LIMIT 1"
			))
			{
				$sql_idSYSTEM_CLIENTE = $mysqli->escape_string($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
				//
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_PAGTO_PLANO_CODIGO, $o_PAGTO_TOKEN);
				$stmt->fetch();
			}
			else
			{
				die("0|".TXT_INCAPAZ_DE_PREPARAR_SQL_STMT."|error||||");
				exit;
			}
			
			####
			# Se ainda nao houver plano para contratacao selecionado, mostra o plano atual
			####
			if(isEmpty($o_PAGTO_PLANO_CODIGO)){ $o_PAGTO_PLANO_CODIGO = $SID_CTRL->getPLANO_CODIGO(); }
			
			####
			# Formata plano a contratar info
			####
			$pagto_plano_titulo = $plano[$o_PAGTO_PLANO_CODIGO]['titulo']." - ".$plano_tipo_descr[$o_PAGTO_PLANO_CODIGO] ." - R$ ".$plano[$o_PAGTO_PLANO_CODIGO]['valor_cobranca'];
		
		}
		
		
?>
		<!-- main_table -->
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="x_title">
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_CONFIGURACOES; ?></a></li>
							<li class="active"><span><?php echo TXT_CFG_PLANO_PLANO_CONTRATADO; ?> </span></li>
						</ol></h2>
						<div class="clearfix"></div>
<?php if($SID_CTRL->getPLANO_SITUACAO() == 'A') { ?>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_PLANO_ATUAL; ?></a></li>
							<li class="active"><span style="color:#008000;"> <?php echo $plano_atual_titulo; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_SITUACAO; ?></a></li>
							<li class="active"><span style="color:#008000;"> Ativo até <?php echo $plano_atual_expiracao; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
<?php } else if($SID_CTRL->getPLANO_SITUACAO() == 'E') { ?>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_PLANO_ATUAL; ?></a></li>
							<li class="active"><span style="color:#e86e11;"> <?php echo $plano_atual_titulo; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_SITUACAO; ?></a></li>
							<li class="active"><span style="color:#e86e11;"> <?php echo TXT_CFG_PLANO_EXPIRADO_OPERANDO_ATE; ?> <?php echo $plano_atual_renovacao_com_prazo; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
<?php } else if($SID_CTRL->getPLANO_SITUACAO() == 'R') { ?>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_PLANO_ATUAL; ?></a></li>
							<li class="active"><span style="color:#ff0000;"> <?php echo $plano_atual_titulo; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
					<h2><ol class="breadcrumb breadcrumb-arrow">
							<li><a><?php echo TXT_CFG_PLANO_SITUACAO; ?></a></li>
							<li class="active"><span style="color:#ff0000;"> <?php echo TXT_CFG_PLANO_INATIVO; ?></span></li>
						</ol></h2>
					<div class="clearfix"></div>
<?php } ?>
					
					
				</div>
				<div class="x_content">
<?php if($SID_CTRL->getPLANO_SITUACAO() == 'E') { ?>
					<p align="justify" style="color:#434a54; font-size: 120%;"><?php echo TXT_CFG_PLANO_PRAZO_TEXT1; ?></p>
					<ol>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC1; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC2; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC3; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT1_INSTRUC4; ?></li>
					</ol>
					<form id="pagto-form" name="pagto-form">
					<input type=hidden id="pagto_plano_codigo" value="<?php echo $o_PAGTO_PLANO_CODIGO; ?>">
					<input type=hidden id="pagto_token" value="<?php echo $o_PAGTO_TOKEN; ?>">
					</form>
<?php } else if($SID_CTRL->getPLANO_SITUACAO() == 'R') { ?>
					<p align="justify" style="color:#434a54; font-size: 120%;"><?php echo TXT_CFG_PLANO_PRAZO_TEXT2; ?></p>
					<ol>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC1; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC2; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC3; ?></li>
						<li><?php echo TXT_CFG_PLANO_PRAZO_TEXT2_INSTRUC4; ?></li>
					</ol>
					<form id="pagto-form" name="pagto-form">
					<input type=hidden id="pagto_plano_codigo" value="<?php echo $o_PAGTO_PLANO_CODIGO; ?>">
					<input type=hidden id="pagto_token" value="<?php echo $o_PAGTO_TOKEN; ?>">
					</form>

<?php } ?>
				</div>
				<div class="x_content">
					<!-- /main_tabe -->
				<div class="col-xs-12 col-sm-9 col-centered">
					<h3 style="color:#434a54;"><span class="fa fa-shopping-cart" aria-hidden="true"></span> <?php echo TXT_CFG_PLANO_PLANOS_PARA_CONTRATACAO; ?></h3>
					<div class="clearfix"></div>
					
					
					<!-- Plano a Contratar -->
					<?php if($SID_CTRL->getPLANO_SITUACAO() != 'A') { ?>
					<div id="panel_plano_a_contratar">
						<h2><ol class="breadcrumb breadcrumb-arrow2">
								<li><a><?php echo TXT_CFG_PLANO_PLANO_A_CONTRATAR; ?></a></li>
								<li class="active"><span id="pagto_plano_titulo" style="color:#0000ff;"> <?php echo $pagto_plano_titulo; ?></span></li>
							</ol></h2>
						<h2><ol class="breadcrumb breadcrumb-arrow2">
								<li><a><?php echo TXT_CFG_PLANO_PAGAMENTO; ?></a></li>
								<li class="active"><span id="btn_pagto_boleto" role="button" style="color:#0000ff;"><i class="fa fa-barcode"></i> <?php echo TXT_CFG_PLANO_BOLETO_BANCARIO; ?></span> <!--<span id="btn_pagto_cartao" role="button" style="color:#0000ff;"><i class="fa fa-credit-card"></i> <?php echo TXT_CFG_PLANO_CARTAO_DE_CREDITO; ?></span>--></li>
							</ol></h2>
						<h2><ol class="breadcrumb breadcrumb-arrow2">
								<li><a><?php echo TXT_CFG_PLANO_SITUACAO; ?></a></li>
								<li class="active"><span style="color:#ff0000;"> <?php echo TXT_CFG_PLANO_AGUARDANDO_PAGAMENTO; ?></span></li>
							</ol></h2>
						<div class="clearfix"></div>
					</div>
					<?php } ?>
					<!-- /Plano a Contratar -->
					<br/>
					
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#mensal"><b><?php echo TXT_CFG_PLANO_MENSAL; ?></b></a></li>
						<li><a data-toggle="tab" href="#anual"><b><?php echo TXT_CFG_PLANO_ANUAL; ?></b></a></li>
					</ul>
					
					<div class="tab-content">
						<div id="mensal" class="tab-pane fade in active">
							<div class="row">
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#4CAF50"><?php echo mb_strtoupper($plano['INI-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['INI-M1']['valor_cobranca']; ?></b><sub><font size=-1>/<?php echo TXT_CFG_PLANO_MES; ?></font></sub></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['INI-M1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['INI-M1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['INI-M1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADOR; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
										<!-- <li><a href="funcionalidades.htm" target="_blank" role="button" style="color:#0000ff;"><?php echo TXT_CFG_PLANO_VEJA_E_COMPARE_TODAS_AS_FUNCIONALIDADES; ?></a></li> -->
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#64a5de"><?php echo mb_strtoupper($plano['BAS-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['BAS-M1']['valor_cobranca']; ?></b><sub><font size=-1>/<?php echo TXT_CFG_PLANO_MES; ?></font></sub></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['BAS-M1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['BAS-M1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['BAS-M1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#de9846"><?php echo mb_strtoupper($plano['ESS-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['ESS-M1']['valor_cobranca']; ?></b><sub><font size=-1>/<?php echo TXT_CFG_PLANO_MES; ?></font></sub></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ESS-M1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['ESS-M1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['ESS-M1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_ATAS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#8e1a55"><?php echo mb_strtoupper($plano['PR1-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['PR1-M1']['valor_cobranca']; ?></b><sub><font size=-1>/<?php echo TXT_CFG_PLANO_MES; ?></font></sub></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['PR1-M1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['PR1-M1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['PR1-M1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_ATAS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_ATEND_PREMIUM; ?></li>
									</ul>
								</div>
								
							</div><!--/row-->
						</div>
						
						
						
						
						
						
						
						
						<div id="anual" class="tab-pane fade">
							<div class="row">
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#4CAF50"><?php echo mb_strtoupper($plano['INI-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4><?php echo TXT_CFG_PLANO_ECONOMIZE; ?></font><br/><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['INI-A1']['valor_economia']; ?></b><br/><font size=3><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?> <?php echo $plano['INI-A1']['valor_cobranca']; ?>/<?php echo TXT_CFG_PLANO_ANO; ?></font></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['INI-A1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['INI-A1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['INI-A1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADOR; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#64a5de"><?php echo mb_strtoupper($plano['BAS-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4><?php echo TXT_CFG_PLANO_ECONOMIZE; ?></font><br/><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['BAS-A1']['valor_economia']; ?></b><br/><font size=3><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?> <?php echo $plano['BAS-A1']['valor_cobranca']; ?>/<?php echo TXT_CFG_PLANO_ANO; ?></font></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['BAS-A1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['BAS-A1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['BAS-A1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#de9846"><?php echo mb_strtoupper($plano['ESS-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4><?php echo TXT_CFG_PLANO_ECONOMIZE; ?></font><br/><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['ESS-A1']['valor_economia']; ?></b><br/><font size=3><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?> <?php echo $plano['ESS-A1']['valor_cobranca']; ?>/<?php echo TXT_CFG_PLANO_ANO; ?></font></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ESS-A1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['ESS-A1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['ESS-A1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_ATAS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price_table">
										<li class="header" style="background-color:#8e1a55"><?php echo mb_strtoupper($plano['PR1-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4><?php echo TXT_CFG_PLANO_ECONOMIZE; ?></font><br/><sup><font size=-1><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?></font></sup> <b><?php echo $plano['PR1-A1']['valor_economia']; ?></b><br/><font size=3><?php echo TXT_CFG_PLANO_SIMBOLO_MOEDA; ?> <?php echo $plano['PR1-A1']['valor_cobranca']; ?>/<?php echo TXT_CFG_PLANO_ANO; ?></font></li>
										<?php if( $SID_CTRL->getPLANO_SITUACAO() != 'A'){ ?>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['PR1-A1']);"
											><?php echo TXT_CFG_PLANO_CONTRATAR; ?></button>
										</li>
										<?php } ?>
										<li><?php echo TXT_CFG_PLANO_GESTAO_DE; ?> <?php echo $plano['PR1-A1']['total_condominios']; ?> <?php echo TXT_CFG_PLANO_CONDOMINIOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COM; ?> <?php echo $plano['PR1-A1']['total_colaboradores']; ?> <?php echo TXT_CFG_PLANO_COLABORADORES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CLIENTES; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_CONTRATOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_CONTR_ATAS; ?></li>
										<li><?php echo TXT_CFG_PLANO_COBRANCA_DO_CONDOMINIO; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_BOLETOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_EMISSAO_DE_2A_VIA_ONLINE; ?></li>
										<li><?php echo TXT_CFG_PLANO_VERIF_AUTOM_DE_PAGTOS; ?></li>
										<li><?php echo TXT_CFG_PLANO_ATEND_PREMIUM; ?></li>
									</ul>
								</div>
								
							</div><!--/row-->
						</div>
					</div>
					
					
				</div><!--/.col-xs-12.col-sm-9-->



				</div>
			</div>
		</div>


<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<script src="js/view-cfg-plano_contratado.js"></script>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
<?php
	} 
?>
