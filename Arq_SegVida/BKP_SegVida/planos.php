<?php
#################################################################################
## SEGVIDA
#################################################################################
## Criado por: Rodrigo Leite Gomide - rlgomide@gmail.com
#################################################################################
	
	## System configuration
	include_once "cgi-bin/includes/config.php";
	include_once "cgi-bin/includes/aux_lib.php";
	
	## Define idioma
	if(isset($_COOKIE['lang'])) { $_LANG = $_COOKIE['lang']; } else { $_LANG = "pt-br"; setcookie('lang', $_LANG); }
	
	## Carrega Idioma
	carrega_idioma($_LANG);
	
	## Carrega dados dos planos
	//$plano['ECO-G1']['tipo'] = 'Tipo';
	//$plano['ECO-G1']['titulo'] = 'economico';
	
	//carregaDadosPlanos($_LANG,$plano[][]);
	{
		##Conecta ao Banco de Dados
		$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
		if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
		
		##UTF-8
		if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
		
		##Prepara query
		if ($stmt = $mysqli->prepare(
		"SELECT `codigo` AS codigo, `tipo` AS tipo, 
			        CASE WHEN '$_LANG' = 'pt-br' THEN `titulo_br`
                   WHEN '$_LANG' = 'en-us' THEN `titulo_us`
                   WHEN '$_LANG' = 'es-es' THEN `titulo_sp`
               END AS titulo,
              FORMAT(`valor_cobranca`,2,'de_DE') AS valor_cobranca,
              FORMAT(`valor_economia`,2,'de_DE') AS valor_economia,
              `total_mudas` AS total_mudas, 
              `total_colaboradores` AS total_colaboradores, 
              `total_dispositivos` AS total_dispositivos,
              `dias_gratis` AS dias_gratis 
         FROM `PRODFY_PLANO_PAGTO` order by `ordem`"
		)) 
		{
			//$stmt->bind_param('s', $email);
			$stmt->execute();
			$stmt->store_result();
			//
			// obtém variáveis a partir dos resultados. 
			$stmt->bind_result($o_CODIGO, $o_TIPO, $o_TITULO, $o_VALOR_COBRANCA, $o_VALOR_ECONOMIA, $o_TOTAL_MUDAS, $o_TOTAL_COLABORADORES, $o_TOTAL_DISPOSITIVOS, $o_DIAS_GRATIS);

			##Corre pelas linhas encontradas
			while ($row = $stmt->fetch()) 
			{
				$plano[$o_CODIGO]['tipo']                = $o_TIPO;
				$plano[$o_CODIGO]['titulo']              = $o_TITULO;
				$plano[$o_CODIGO]['valor_cobranca']      = $o_VALOR_COBRANCA;
				$plano[$o_CODIGO]['valor_economia']      = $o_VALOR_ECONOMIA;
				$plano[$o_CODIGO]['total_mudas']         = $o_TOTAL_MUDAS;
				$plano[$o_CODIGO]['total_colaboradores'] = $o_TOTAL_COLABORADORES;
				$plano[$o_CODIGO]['total_dispositivos']  = $o_TOTAL_DISPOSITIVOS;
				$plano[$o_CODIGO]['dias_gratis']         = $o_DIAS_GRATIS;
			}
		}
	}
	
	####
	# Mostra pagina em PT-BR
	####
	if( $_LANG == 'pt-br') { 
?>
<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		
		<meta name="language"    content="<?php echo TXT_META_LANGUAGE ?>" />
		<meta name="description" content="<?php echo TXT_META_DESCRIPTION ?>" />
		<meta name="author"      content="<?php echo TXT_META_AUTHOR ?>">
		<meta name="keywords"    content="<?php echo TXT_META_KEYWORDS ?>" />
		<meta name="subject"     content="<?php echo TXT_META_SUBJECT ?>" />
		<meta name="robots"      content="All" />
		<meta name="copyright"   content="<?php echo TXT_META_COPYRIGHT ?>" />
		<meta name="abstract"    content="<?php echo TXT_META_ABSTRACT ?>" />
		<meta name="MSSmartTagsPreventParsing" content="true" />
		
		<link rel="shortcut icon" href="img/favicon1.png" type="image/x-icon">
		
		<title><?php echo TXT_PAGE_TITLE_PREFIX." - Planos de Aquisição" ?></title>
		
		<!-- Carregando o CSS do Bootstrap -->
		<link href="css/bootstrap.css" rel="stylesheet" media="screen" />
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
		
		<!-- Custom styles for this template -->
		<link href="css/offcanvas.css" rel="stylesheet">
		<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link rel="stylesheet" href="css/footer-distributed-with-address-and-phones.css">
		<link rel="stylesheet" href="css/planos.css">
		
		<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
		<!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		<script src="js/ie-emulation-modes-warning.js"></script>
		
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
		
		<style>
			* {
			    box-sizing: border-box;
			}
			
			.columns {
			    float: left;
			    width: 33.3%;
			    padding: 8px;
			}
			
			.price {
			    list-style-type: none;
			    border: 1px solid #eee;
			    margin: 0;
			    padding: 0;
			    -webkit-transition: 0.3s;
			    transition: 0.3s;
			}
			
			.price:hover {
			    box-shadow: 0 8px 12px 0 rgba(0,0,0,0.2)
			}
			
			.price .header {
			    background-color: #111;
			    color: white;
			    font-size: 20px;
			}
			
			.price li {
			    border-bottom: 1px solid #eee;
			    padding: 10px;
			    text-align: center;
			}
			
			.price .grey {
			    background-color: #eee;
			    font-size: 35px;
			}
			
			.button {
			    background-color: #4CAF50;
			    border: none;
			    color: white;
			    padding: 10px 25px;
			    text-align: center;
			    text-decoration: none;
			    font-size: 18px;
			}
			
			@media only screen and (max-width: 600px) {
			    .columns {
			        width: 100%;
			    }
			}
			
			select:not([multiple]) {
			    -webkit-appearance: none;
			    -moz-appearance: none;
			    background-position: right 50%;
			    background-repeat: no-repeat;
			    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
			    padding: .5em;
			    padding-right: 1.5em;
			}

			
		</style>
		
	</head>
	<body>
		<nav class="navbar navbar-fixed-top navbar-inverse">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="#" class="logo"><img src="img/top_navbar_logo.png"/></a>
				</div>
				<div id="navbar" class="collapse navbar-collapse">
					<ul class="nav navbar-nav">
						<li><a href="index.php"><span class="fa fa-home" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_INICIO; ?></a></li>
						<li><a href="acesso.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_ACESSO; ?></a></a></li>
						<li class="active"><a href="planos.php"><span class="fa fa-shopping-cart" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_PLANOS; ?></a></a></li>
						<li><a href="usuarios.php"><span class="fa fa-users" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_USERS; ?></a></a></li>
						<li><a href="sobre.php"><span class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_SOBRE; ?></a></a></li>
						<li><a href="contato.php"><span class="fa fa-envelope" aria-hidden="true"></span>&nbsp;<?php echo TXT_PG_MN_CONTATO; ?></a></a></li>
					</ul>
					<!--
					<div id='loged_panel'>
						<button id='btn_show_login_panel' type="button" class="btn btn-yellow btn-sm navbar-btn navbar-right" 
							role="button" data-toggle="modal" data-target="#login-modal"><span class="fa fa-lock" aria-hidden="true"></span>&nbsp;Log in</button>
					</div>
					-->
				</div><!-- /.nav-collapse -->
			</div><!-- /.container -->
		</nav><!-- /.navbar -->
		
		<div class="container-fluid">
			<div class="row text-center center-block">
				<div class="col-xs-12 col-sm-9 col-centered">
					<div class="page-header">
						<h3 class="page-title"><span class="fa fa-shopping-cart" aria-hidden="true"></span> Planos de Aquisição</h3>
					</div>
					
					<p class="text-justify">O Prodfy Mudas é comercializado na forma de assinaturas com custo fixo mensal. Quanto maior o tempo de fidelização, maior o desconto no valor mensal. Todos os planos possuem o pacote básico que pode ser expandido com a aquisição de módulos adicionais com recursos específicos conforme a necessidade do cliente.</p>
					
					<ul class="nav nav-tabs">
						<li class="active"><a data-toggle="tab" href="#mensal">Mensal</a></li>
						<li><a data-toggle="tab" href="#anual">Anual</a></li>
					</ul>
					
					<div class="tab-content">
						<div id="mensal" class="tab-pane fade in active">
							<div class="row">
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#4CAF50"><?php echo mb_strtoupper($plano['ECO-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1>R$</font></sup> <b><?php echo $plano['ECO-M1']['valor_cobranca']; ?></b><sub><font size=-1>/mês</font></sub></li>
										<li>Controle de Mudas, Espécies e Doenças</li>
										<li>Produção: Visão Geral, Evolução de Lote, Histórico de Lote, Inventário de Lote, Perdas, Lotes, Estágios, Pontos de Controle e Objetivos</li>
										<li>Controle: Painel de controle com totalizações de registros de todo o sistema, Clientes, Objetivos, Pontos de Controle, Estágios, Lotes, Inventários, Históricos e Alertas</li>
										<li>Aplicativo de Gravação/Leitura de Etiquetas RFID com contagem de mudas em campo</li>
										<li>Qtde de Lotes Ilimitados</li>
										<li>Até <?php echo $plano['ECO-M1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['ECO-M1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['ECO-M1']['total_dispositivos']; ?> dispositivo de apoio</li>
										<li><a href="funcionalidades.htm">Veja e compare todas as funcionalidades</a></li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ECO-M1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['ECO-M1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#64a5de"><?php echo mb_strtoupper($plano['ESS-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1>R$</font></sup> <b><?php echo $plano['ESS-M1']['valor_cobranca']; ?></b><sub><font size=-1>/mês</font></sub></li>
										<li><b>Todas as funcionalidades do plano econômino +</b></li>
										<li>Informativos diários por e-mail</li>
										<li>Até <?php echo $plano['ESS-M1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['ESS-M1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['ESS-M1']['total_dispositivos']; ?> dispositivos simultâneos</li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ESS-M1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['ESS-M1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#de9846"><?php echo mb_strtoupper($plano['PR1-M1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><sup><font size=-1>R$</font></sup> <b><?php echo $plano['PR1-M1']['valor_cobranca']; ?></b><sub><font size=-1>/mês</font></sub></li>
										<li><b>Todas as funcionalidades do plano essencial +</b></li>
										<li>Gestão de Contratos</li>
										<li>Repositório de Arquivos</li>
										<li>Até <?php echo $plano['PR1-M1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['PR1-M1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['PR1-M1']['total_dispositivos']; ?> dispositivos simultâneos</li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['PR1-M1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['PR1-M1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
							</div><!--/row-->
						</div>
						<div id="anual" class="tab-pane fade">
							<div class="row">
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#4CAF50"><?php echo mb_strtoupper($plano['ECO-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4>Economize</font><br/><sup><font size=-1>R$</font></sup> <b><?php echo $plano['ECO-A1']['valor_economia']; ?></b><br/><font size=3>R$ <?php echo $plano['ECO-A1']['valor_cobranca']; ?>/ano</font></li>
										<li>Controle de Mudas, Espécies e Doenças</li>
										<li>Produção: Visão Geral, Evolução de Lote, Histórico de Lote, Inventário de Lote, Perdas, Lotes, Estágios, Pontos de Controle e Objetivos</li>
										<li>Controle: Painel de controle com totalizações de registros de todo o sistema, Clientes, Objetivos, Pontos de Controle, Estágios, Lotes, Inventários, Históricos e Alertas</li>
										<li>Aplicativo de Gravação/Leitura de Etiquetas RFID com contagem de mudas em campo</li>
										<li>Qtde de Lotes Ilimitados</li>
										<li>Até <?php echo $plano['ECO-A1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['ECO-A1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['ECO-A1']['total_dispositivos']; ?> dispositivo de apoio</li>
										<li><a href="funcionalidades.htm">Veja e compare todas as funcionalidades</a></li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ECO-A1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['ECO-A1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#64a5de"><?php echo mb_strtoupper($plano['ESS-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4>Economize</font><br/><sup><font size=-1>R$</font></sup> <b><?php echo $plano['ESS-A1']['valor_economia']; ?></b><br/><font size=3>R$ <?php echo $plano['ESS-A1']['valor_cobranca']; ?>/ano</font></li>
										<li><b>Todas as funcionalidades do plano econômino +</b></li>
										<li>Informativos diários por e-mail</li>
										<li>Até <?php echo $plano['ESS-A1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['ESS-A1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['ESS-A1']['total_dispositivos']; ?> dispositivos simultâneos</li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['ESS-A1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['ESS-A1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
								<div class="columns">
									<ul class="price">
										<li class="header" style="background-color:#de9846"><?php echo mb_strtoupper($plano['PR1-A1']['titulo'],"UTF-8"); ?></li>
										<li class="grey"><font size=4>Economize</font><br/><sup><font size=-1>R$</font></sup> <b><?php echo $plano['PR1-A1']['valor_economia']; ?></b><br/><font size=3>R$ <?php echo $plano['PR1-A1']['valor_cobranca']; ?>/ano</font></li>
										<li><b>Todas as funcionalidades do plano essencial +</b></li>
										<li>Gestão de Contratos</li>
										<li>Repositório de Arquivos</li>
										<li>Até <?php echo $plano['PR1-A1']['total_mudas']; ?> variedades de muda</li>
										<li>Até <?php echo $plano['PR1-A1']['total_colaboradores']; ?> colaboradores simultâneos</li>
										<li>Até <?php echo $plano['PR1-A1']['total_dispositivos']; ?> dispositivos simultâneos</li>
										<li class="grey">
											<button type="button" class="button" 
											role="button" data-toggle="modal" data-target="#comprar-modal"
											onclick="javascript:$(this).trigger('f_comprar_plano',['PR1-A1']);"
											>Experimente Grátis<br/><small>por <?php echo $plano['PR1-A1']['dias_gratis']; ?> dias</small></button>
										</li>
									</ul>
								</div>
								
							</div><!--/row-->
						</div>
					</div>
					
					
				</div><!--/.col-xs-12.col-sm-9-->
			</div><!--/row-->
		</div><!--/container-->
		
		<!-- BEGIN # MODAL COMPRAR -->
		<div class="modal fade" id="comprar-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header" align="center">
						<img src="img/logo_login_200x59.png">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
						</button>
					</div>
					
					<!-- Begin # DIV Form -->
					<div id="div-forms">
						<!-- Begin | Register Form -->
						<form id="register-form" name="register-form">
							<div class="modal-body">
								<div id="div-register-msg">
									<div id="div-label-type">AQUISIÇÃO DE PLANO</div>
									<div id="icon-register-msg" class="glyphicon glyphicon-chevron-right"></div>
										<span id="text-register-msg">Campos com * são obrigatórios</span>
									</div>
									<input id="register_nome"        type="text" maxlength=45  class="form-control textonly_az input-upper" placeholder="*Nome" required>
									<input id="register_sobrenome"   type="text" maxlength=45  class="form-control textonly_az input-upper" placeholder="*Sobrenome" required>
									<input id="register_email"       type="text" maxlength=180 class="form-control input-lower"             placeholder="*E-Mail" required>
									<input id="register_cpf"         type="text" maxlength=14  class="form-control numeric"     placeholder="*CPF" required>
									<input id="register_cnpj"        type="text" maxlength=18  class="form-control numeric"     placeholder="CNPJ">
									<input id="register_razaosocial" type="text" maxlength=180 class="form-control textonly input-upper"    placeholder="Razão Social">
									<input id="register_fonefixo1"   type="text" maxlength=15  class="form-control numeric"     placeholder="*Telefone Fixo" required
									data-toggle="tooltip" data-placement="auto right" title="digite o número do seu telefone fixo com o código de área Ex.: 3138852424">
									<input id="register_celular1"    type="text" maxlength=15  class="form-control numeric" style="margin-bottom: 10px;" placeholder="Celular"
									data-toggle="tooltip" data-placement="auto left" title="Digite o número do seu celular com o código de área Ex.: 31999474426">
									<select id="register_pais" class="form-control" style="margin-top: 0px; margin-bottom: 10px;" required>
										<!-- <option value="" selected hidden>*País</option> -->
										<option value="BR" selected>Brasil</option>
									</select>
									<select id="register_idioma" class="form-control" style="margin-top: 0px; margin-bottom: 10px;" required>
										<!-- <option value="" selected hidden>*Idioma</option> -->
										<option value="pt-br" selected>Português Brasil</option>
										<!--<option value="en-us" >US English</option>
										<option value="es-es" >Español</option>-->
									</select>
									<input id="register_cep" type="text" maxlength=10 class="form-control numeric" style="margin-top: 0px;" placeholder="*CEP" required
									data-toggle="tooltip" data-placement="auto bottom" title="Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.">
									<input id="register_cep_uf" type="hidden">
									<input id="register_cep_cidade_codigo" type="hidden">
									<!--
									<div class="input-group">
										<input id="register_cep" type="text" maxlength=10 class="form-control numeric" style="margin-top: 0px;" placeholder="*CEP" required
										data-toggle="tooltip" data-placement="auto bottom" title="Entre com o CEP do seu endereço e clique no botão de busca ao lado para carregar os dados automaticamente. Obs.: Deve ser um CEP válido.">
										<input id="register_cep_ok" type="hidden">
										<input id="register_cep_uf" type="hidden">
										<input id="register_cep_cidade_codigo" type="hidden">
										<span class="input-group-addon">
											<a id="register_search_cep_btn" class="btn-lightgray" role="button"><span class="fa fa-search" aria-hidden="true"></span></a>
										</span>
									</div>
									-->
									<select id="register_uf" class="form-control" required>
										<option value="" selected hidden>*Estado</option><option value="AC">ACRE</option><option value="AL">ALAGOAS</option><option value="AM">AMAZONAS</option><option value="AP">AMAPÁ</option><option value="BA">BAHIA</option><option value="CE">CEARÁ</option><option value="DF">DISTRITO FEDERAL</option><option value="ES">ESPÍRITO SANTO</option><option value="GO">GOIÁS</option><option value="MA">MARANHÃO</option><option value="MG">MINAS GERAIS</option><option value="MS">MATO GROSSO DO SUL</option><option value="MT">MATO GROSSO</option><option value="PA">PARÁ</option><option value="PB">PARAÍBA</option><option value="PE">PERNAMBUCO</option><option value="PI">PIAUÍ</option><option value="PR">PARANÁ</option><option value="RJ">RIO DE JANEIRO</option><option value="RN">RIO GRANDE DO NORTE</option><option value="RO">RONDÔNIA</option><option value="RR">RORAIMA</option><option value="RS">RIO GRANDE DO SUL</option><option value="SC">SANTA CATARINA</option><option value="SE">SERGIPE</option><option value="SP">SÃO PAULO</option><option value="TO">TOCANTINS</option>
									</select>
									<span id='cidade_panel'>
										<select id="register_cidade" class="form-control" required>
											<option value="" selected hidden>*Cidade (Escolha Um Estado Primeiro)</option>
										</select>
									</span>
									<input id="register_bairro"     type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="*Bairro" required>
									<input id="register_end"        type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="*Endereço" required>
									<input id="register_num"        type="text"     maxlength=30  class="form-control textonly input-upper"    placeholder="*Número" required>
									<input id="register_compl"      type="text"     maxlength=180 class="form-control textonly input-upper"    placeholder="Complemento">
									<input id="register_username"   type="text"     maxlength=30  class="form-control logintext input-lower"   placeholder="*Usuário (de 6 a 30 dígitos)" required 
									data-toggle="tooltip" data-placement="auto top" title="O campo USUÁRIO deve ter de 6 a 30 dígitos. Deve conter apenas letras, números e underlines.">
									<input id="register_password"   type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="*Senha" required
									data-toggle="tooltip" data-placement="auto left" title="A senhas devem ter pelo menos 6 dígitos, conter pelo menos um número, uma letra minúscula e uma letra maiúscula.">
									<!-- <a class="btn-lightgray" role="button" href="javascript:show_msgbox('Digite novamente a senha para confirmar que a senha digitada está correta.',' ','info');"><span class="badge"><span class="fa fa-question-circle" aria-hidden="true"></span></span></a> -->
									<input id="register_password2"  type="password" maxlength=30  class="form-control" style="margin-bottom: 10px;" placeholder="*Senha para confirmação" required
									data-toggle="tooltip" data-placement="auto right" title="Entre exatamente com a mesma senha digitada para confirmação.">
									<span id="planos_panel">
										<select id="register_plano" class="form-control" style="margin-top: 0px; margin-bottom: 10px;" required>
											<option value="" selected hidden>*Plano de Pagto</option>
											<!-- <option value="G">Grátis por 30 Dias</option> -->
											<option value="">Mensal:</option>
											<option value="ECO-M1"><?php echo $plano['ECO-M1']['titulo']; ?> (R$<?php echo $plano['ECO-M1']['valor_cobranca']; ?>/mês)</option>
											<option value="ESS-M1"><?php echo $plano['ESS-M1']['titulo']; ?> (R$<?php echo $plano['ESS-M1']['valor_cobranca']; ?>/mês)</option>
											<option value="PR1-M1"><?php echo $plano['PR1-M1']['titulo']; ?> (R$<?php echo $plano['PR1-M1']['valor_cobranca']; ?>/mês)</option>
											<option value="">&nbsp;</option>
											<option value="">Anual:</option>
											<option value="ECO-A1"><?php echo $plano['ECO-A1']['titulo']; ?> (R$<?php echo $plano['ECO-A1']['valor_cobranca']; ?>/ano)</option>
											<option value="ESS-A1"><?php echo $plano['ESS-A1']['titulo']; ?> (R$<?php echo $plano['ESS-A1']['valor_cobranca']; ?>/ano)</option>
											<option value="PR1-A1"><?php echo $plano['PR1-A1']['titulo']; ?> (R$<?php echo $plano['PR1-A1']['valor_cobranca']; ?>/ano)</option>
										</select>
									</span>
									<div class="input-group" style="margin-bottom: 10px;">
										<span class="input-group-addon">
											<img id='register_captcha_img' src="captcha/captcha_loading.png" width=70 height=22>
											<a id="register_captcha_refresh_btn" class="btn-lightgray" role="button"><span class="fa fa-refresh" aria-hidden="true"></span></a>
										</span>
										<input id="register_captcha_code" type="text"  class="form-control" style="margin-top: 1px;" placeholder="Digite o Código" required>
									</div>
									<div class="checkbox" style="margin-top: 1px;">
										<label>
										<input id="register_termos" type="checkbox" required> Li e aceito o <a href="docs/termo-de-servico.pdf" target="_blank">termo de serviço</a>.
										</label>
									</div>
								</div>
							<div class="modal-footer">
								<div>
									<button id="bt_register" type="button" class="btn btn-yellow btn-lg btn-block">Registrar</button>
								</div>
							</div>
						</form>
						<!-- End | Register Form -->
						
					</div>
					<!-- End # DIV Form -->
					
				</div>
			</div>
		</div>
		<!-- END # MODAL LOGIN -->
		
		
		
		
		
		
		
		
<!-- msgBox -->
<div class="modal fade" id="msgBox" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="msgBoxLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Fechar"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="msgBoxLabel">Titulo<!--msgBox title--></h4>
			</div>
			<div class="modal-body">
				<div class="media">
					<div class="media-left media-top" id="msgBoxIcon">
						<span id="msgBoxIconInfo" class='fa fa-info-circle fa-3x' aria-hidden='true' style='color:blue;'></span>
						<span id="msgBoxIconConfirm" class='fa fa-queston-circle-o fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconError" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:red;'></span>
						<span id="msgBoxIconPrompt" class='fa fa-pencil-square-o fa-3x' aria-hidden='true' style='color:#c8c8c8;'></span>
						<span id="msgBoxIconSuccess" class='fa fa-check-circle-o fa-3x' aria-hidden='true' style='color:#008000;'></span>
						<span id="msgBoxIconWarning" class='fa fa-exclamation-triangle fa-3x' aria-hidden='true' style='color:orange;'></span>
						<span id="msgBoxIconAlert" class='fa fa-exclamation-circle fa-3x' aria-hidden='true' style='color:orange;'></span>
					</div>
					<div class="media-body">
						<p class="text-left" id="msgBoxBody"><!--msgBox body--></p>
					</div>
				</div>
			</div>
			<div class="modal-footer" id="msgBoxFooter">
				<button type="button" class="btn btn-default center-block" data-dismiss="modal">Fechar</button>
			</div>
		</div>
	</div>
</div>
<!-- /msgBox -->

<!-- loadingBox -->
<div class="modal fade" id="loadingBox" data-backdrop="static" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content" style="background-color: #5c9ccc;">
			<!--<div class="modal-header">
			</div>-->
			<div class="modal-body">
				<p class="text-center center-block"><img src="img/ajax-loader01_blue.gif" width="32" height="32" /><br/><font color=#ffffff><b id='loadingBoxText'><?php echo TXT_MODAL_MSG_CARREGANDO ?></b></font></p>
			</div>
			<!--<div class="modal-footer" id="msgBoxFooter">
			</div>-->
		</div>
	</div>
</div>
<!-- /msgBox -->


		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src='js/jquery.storage.js'></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="js/ie10-viewport-bug-workaround.js"></script>
		<script src="js/sha512.js"></script>
		<script src="js/planos.js"></script>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		
		<!-- footer_panel -->
		<div id='footer_panel'>
			<footer class="footer-distributed">
				<div class="footer-left">
					<img src="img/footer_logo_293x79.png" border=0 class="img-responsive">
					<p class="footer-links">
						<a href="termos-de-uso.php"><?php echo TXT_TERMOS_DE_USO ?></a>
						|
						<a href="politica-de-privacidade.php"><?php echo TXT_POLITICA_DE_PRIVACIDADE ?></a>
					</p>
					<p class="footer-company-name">Ther Sistemas &copy; 2016 - <a href="http://www.ther.com.br" role="button" target="_blank" style="color:#0098ff;">www.ther.com.br</a></p>
				</div>
				
				<div class="footer-center">
					<div>
						<i class="fa fa-map-marker"></i>
						<p style="color:#626468;"><span>Av Oraida Mendes de Castro, 6000 Sala 23<br/>
							Novo Silvestre - CEP 36.570-000<br/>
							Viçosa - MG - Brasil</span></p>
					</div>
					<div>
						<i class="fa fa-phone"></i>
						<p style="color:#626468;">+55-31-3885-2424 / +55-31-99947-4426</p>
					</div>
					<div>
						<i class="fa fa-envelope"></i>
						<p style="color:#626468;"><a href="mailto:contato@thersistemas.com.br" role="button" style="color:#0098ff;">contato@thersistemas.com.br</a></p>
					</div>
				</div>
				
				<div class="footer-right">
					<!--
					<p class="footer-company-about">
						<span>About the company</span>
						Lorem ipsum dolor sit amet, consectateur adispicing elit. Fusce euismod convallis velit, eu auctor lacus vehicula sit amet.
					</p>
					-->
					<div class="footer-icons">
						<a href="https://www.facebook.com/thersistemas"><i class="fa fa-facebook"></i></a>
						<a href="https://plus.google.com/b/111627314477530389545/+TheriontecSistemasInovadoresVi%C3%A7osa"><i class="fa fa-google-plus"></i></a>
					</div>
				</div>
			</footer>
		</div>
		<!-- /footer_panel -->
	</body>
</html>
<?php } ?>