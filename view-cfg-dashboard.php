<?php
#################################################################################
## Projeto: SEGVIDA
##  Modulo: view-cfg-dashboard.php
##  Funcao: VIEW - Select, Insert, Update, Delete, Upload, Datatable Refresh;
##   Resp.: Rodrigo Gomide (rlgomide@gmail.com)
## Criacao: 00/00/2017 12:23:19
#################################################################################
	
	## System configuration
	include_once "lib-bin/includes/config.php";
	include_once "lib-bin/includes/aux_lib.php";
	
	####
	# HTML HEAD
	####
	function mostra_pg_html_head(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
	<!-- Datatables -->
	<link rel="stylesheet" href="vendors/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">
	<link rel="stylesheet" href="vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css">
	
<!-- CSS -->
<style>
	@charset "UTF-8";
	@import url(http://fonts.googleapis.com/css?family=Roboto);
	
	* {
	    font-family: 'Roboto', sans-serif;
	}
	
	#cad-modal .modal-dialog {
	    width: 450px;
	}
	
	#cad-modal input[type=text], input[type=password] {
		margin-top: 10px;
	}
	
	.form-control-select {
		margin-top: 10px;
	}
	
	#div-reg-msg {
	    border: 1px solid #dadfe1;
	    height: 30px;
	    line-height: 28px;
	    transition: all ease-in-out 500ms;
	}
	
	#div-reg-msg.success {
	    border: 1px solid #68c3a3;
	    background-color: #c8f7c5;
	}
	
	#div-reg-msg.error {
	    border: 1px solid #eb575b;
	    background-color: #ffcad1;
	}
	
	#icon-reg-msg {
	    width: 30px;
	    float: left;
	    line-height: 28px;
	    text-align: center;
	    background-color: #dadfe1;
	    margin-right: 5px;
	    transition: all ease-in-out 500ms;
	}
	
	#icon-reg-msg.success {
	    background-color: #68c3a3 !important;
	}
	
	#icon-reg-msg.error {
	    background-color: #eb575b !important;
	}
	
	#img_logo {
	    max-height: 100px;
	    max-width: 100px;
	}
	
	/*##################
	  # confirm-dialog #
	  ################## */
	.confirm-dialog .modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.confirm-dialog .modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.confirm-dialog .modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.confirm-dialog .modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.confirm-dialog .modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	/*#########################################
	  #    override the bootstrap configs     #
	  ######################################### */
	
	.modal-backdrop.in {
	    filter: alpha(opacity=50);
	    opacity: .8;
	}
	
	.modal-content {
	    background-color: #ececec;
	    border: 1px solid #bdc3c7;
	    border-radius: 0px;
	    outline: 0;
	}
	
	.modal-header {
	    min-height: 16.43px;
	    padding: 15px 15px 15px 15px;
	    border-bottom: 0px;
	}
	
	.modal-body {
	    position: relative;
	    padding: 5px 15px 5px 15px;
	}
	
	.modal-footer {
	    padding: 15px 15px 15px 15px;
	    text-align: right;
	    border-top: 0px;
	}
	
	.checkbox {
	    margin-bottom: 0px;
	}
	
	.btn {
	    border-radius: 0px;
	}
	
	.btn:focus,
	.btn:active:focus,
	.btn.active:focus,
	.btn.focus,
	.btn:active.focus,
	.btn.active.focus {
	    outline: none;
	}
	
	.btn-lg, .btn-group-lg>.btn {
	    border-radius: 0px;
	}
	
	.btn-link {
	    padding: 5px 10px 0px 0px;
	    color: #95a5a6;
	}
	
	.btn-link:hover, .btn-link:focus {
	    color: #2c3e50;
	    text-decoration: none;
	}
	
	.glyphicon {
	    top: 0px;
	}
	
	.form-control {
	  border-radius: 0px;
	}
	.input-upper {
	  text-transform: uppercase;
	}
	.input-lower {
	  text-transform: lowercase;
	}
	
	#div-label-type 
	{
		border: 1px solid #003a1f;
		height: 30px;
		line-height: 28px;
		transition: all ease-in-out 500ms;
		text-align: center;
		font-size: 90%;
		font-weight: bold;
		color: #003a1f;
		margin-bottom: 10px;
	}
	
	* {
	    box-sizing: border-box;
	}
	
	.columns {
	    float: left;
	    width: 33.3%;
	    padding: 8px;
	}
	
	.button {
	    background-color: #4CAF50;
	    border: none;
	    color: white;
	    padding: 10px 35px;
	    text-align: center;
	    text-decoration: none;
	    font-size: 18px;
	    border-radius: 4px;
	}
	
	@media only screen and (max-width: 600px) {
	    .columns {
	        width: 100%;
	    }
	}
	
	.input-group-addon {
	     min-width:150px;
	         width:150px;
	    text-align:right;
	}
	
	.input-group { width: 100%; }
	
	select {
		border-radius: 0px;
		margin-top: 0px;
	}
	
	select:not([multiple]) {
	    -webkit-appearance: none;
	    -moz-appearance: none;
	    background-position: right 50%;
	    background-repeat: no-repeat;
	    background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAMCAYAAABSgIzaAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBNYWNpbnRvc2giIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NDZFNDEwNjlGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NDZFNDEwNkFGNzFEMTFFMkJEQ0VDRTM1N0RCMzMyMkIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo0NkU0MTA2N0Y3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo0NkU0MTA2OEY3MUQxMUUyQkRDRUNFMzU3REIzMzIyQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PuGsgwQAAAA5SURBVHjaYvz//z8DOYCJgUxAf42MQIzTk0D/M+KzkRGPoQSdykiKJrBGpOhgJFYTWNEIiEeAAAMAzNENEOH+do8AAAAASUVORK5CYII=);
	    padding: .5em;
	    padding-right: 1.5em;
	}
	
	.no-border {
	    border-radius: 0;
	}
	
	div.dataTables_wrapper div.dataTables_length select {
	    width: 75px;
	    display: inline-block;
	    padding-top: 0;
	}
	
	.dataTables_filter {
	    width: 40%;
	    float: right;
	    text-align: right;
	}
	
	.form-control2 {
		margin-top: 0px;
		margin-bottom: 0px;
	}
	
	//Dashboard
	.dashboard-table {
		with:100%;
	}
	
	.dashboard-table th {
		width: 200px;
		font-size: 130%;
		text-align: center;
		vertical-align: middle;
		padding: 2px;
		color:black;
		white-space: nowrap;
	}
	.dashboard-table td {
		width: 100px;
		text-align: right;
		vertical-align: middle;
		padding: 2px;
		color:#0000ff;
	}
	.datepicker {
			z-index:1051 !important;
		}
	
</style>
<?php
	}
	
	####
	# HTML BODY
	####
	function mostra_pg_html_body(SessionCTRL $SID_CTRL, AcessoCTRL $ACESSO_MODULO)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
		
		####
		# Carrega dados do modulo
		####
		{
			##Conecta ao Banco de Dados
			$mysqli = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
			if (mysqli_connect_errno()) { die("0|".TXT_NAO_POSSIVEL_CONNECTAR." ".mysqli_connect_error()."|error|"); exit; }
			
			##UTF-8
			if ($stmt = $mysqli->prepare("SET NAMES utf8")) { $stmt->execute(); }
			
			##Carrega Indicadores do Dashboard
			
			##Escapes
			$sql_idSYSTEM_CLIENTE = $mysqli->escape_String($SID_CTRL->getUSER_IDSYSTEM_CLIENTE());
			
			##Prepara query
			if ($stmt = $mysqli->prepare(
			"SELECT lower(AA.`codigo`) as codigo,
	            AA.`ordem` as ordem,
	            AA.`ind_exibir` as ind_exibir
	       FROM `SYSTEM_CFG_DASHBOARD` AA
	 INNER JOIN `SYSTEM_CLIENTE` C
	         ON C.`idSYSTEM_CLIENTE` = AA.`idSYSTEM_CLIENTE`
	      WHERE AA.`idSYSTEM_CLIENTE` = ?
	   ORDER BY AA.`ordem`"
			)) 
			{
				$stmt->bind_param('s', $sql_idSYSTEM_CLIENTE);
				$stmt->execute();
				$stmt->store_result();
				//
				// obtém variáveis a partir dos resultados. 
				$stmt->bind_result($o_CODIGO,$o_ORDEM,$o_IND_EXIBIR);
				
				##Se nao encontrou dados, retorna
				if ($stmt->num_rows == 0) 
				{
					$RET = 0;
				}
				else
				{
					$C=0;
					while($stmt->fetch())
					{
						## Seta Titulo
						switch($o_CODIGO)
						{
							case 'cliente':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_CLIENTES;
								break;
							case 'colaborador':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_COLABORADORES;
								break;
							case 'modelo':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_MODELOS;
								break;
							case 'ld_bio':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_BIO;
								break;
							case 'ld_calor':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_CALOR;
								break;
							case 'ld_eletr':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_ELETR;
								break;
							case 'ld_expl':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_EXPL;
								break;
							case 'ld_infl':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_INFL;
								break;
							case 'ld_part':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_PART;
								break;
							case 'ld_poei':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_POEI;
								break;
							case 'ld_rad':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_RAD;
								break;
							case 'ld_rui':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_RUI;
								break;
							case 'ld_vap':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VAP;
								break;
							case 'ld_vbrvci':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VBRVCI;
								break;
							case 'ld_vbrvmb':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_LAUDOS_VBRVMB;
								break;
							case 'contrato':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_CONTRATOS;
								break;
							case 'dispositivo':
								$o_TITLE = TXT_PAINEL_DASHBOARD_TOTAL_DE_DISPOSITIVOS;
								break;
							default:
								break;
						}
						
						## Seta Select
						switch($o_IND_EXIBIR)
						{
							case '0':
								$o_SET_0 = "selected";
								$o_SET_1 = "";
								break;
							case '1':
							default:
								$o_SET_0 = "";
								$o_SET_1 = "selected";
								break;
						}
						
						$TBODY_LIST .= '<tr><td style="width:300px;"><font size="+1">'.$o_TITLE.'&nbsp;<i class="fa fa-arrow-right"></i>&nbsp;</font></td>';
						$TBODY_LIST .= '<td><input id="codigo_'.$C.'" type="hidden" value="'.$o_CODIGO.'"><input value="'.$o_ORDEM.'" id="ordem_'.$C.'" type="text" size="3" maxlength="2" class="form-control numeric"></td>';
						$TBODY_LIST .= '<td style="width:60px;"><select id="ind_exibir_'.$C.'" class="form-control"><option value="1" '.$o_SET_1.'>'.TXT_CFG_DASHBOARD_SIM.'</option><option value="0" '.$o_SET_0.'>'.TXT_CFG_DASHBOARD_NAO.'</option></select></td></tr>';
						$C++;
					}
					$C--;
				}
				
			}
			
			## Verifica Nivel de Acesso
			if($ACESSO_MODULO->editar == 0)
			{
				$save_btn_link = '<button type="button" class="btn btn-gray btn-lg btn-block" style=" color:#404141; cursor: not-allowed;">'.TXT_BT_SAVE.'</button>';
			}
			else
			{
				$save_btn_link = '<button id="bt_salvar" type="button" class="btn btn-yellow btn-lg btn-block">'.TXT_BT_SAVE.'</button>';
			}
			
		}# /carrega dados #
		
		#<li id="search_button_link"><a href="#" onclick="javascript:$(this).trigger('f_show_advsearch');" role="button" aria-expanded="false"><i class="fa fa-lg fa-search btn_color_blue2"></i></a></li>
		
?>
<!-- page content -->
<div class="center_col" role="main">
	<div class="">
		<div class="clearfix"></div>
		<div class="row">
			
			<!-- main_table -->
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="x_panel">
					<div class="x_title">
						<h2><ol class="breadcrumb breadcrumb-arrow">
								<li><a><?php echo TXT_CFG_DASHBOARD_CONFIGURACAO; ?></a></li>
								<li class="active"><span><?php echo TXT_CFG_DASHBOARD_DASHBOARD; ?></span></li>
							</ol></h2>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<form>
						<input id="regs" type="hidden" value="<?php echo $C; ?>">
						<table class="dashboard-table">
							<thead>
								<tr>
									<th><?php echo TXT_CFG_DASHBOARD_PAINEL; ?></th>
									<th><?php echo TXT_CFG_DASHBOARD_ORDEM; ?></th>
									<th><?php echo TXT_CFG_DASHBOARD_EXIBIR; ?></th>
								</tr>
							</thead>
							
							<tbody id="datatable_tbody">
								<?php echo $TBODY_LIST; ?>
								<tr>
									<td colspan="3">
										<?php echo $save_btn_link; ?>
									</td>
								</tr>
							</tbody>
							
						</table>
					</form>
					</div>
				</div>
			</div>
			<!-- /main_tabe -->
			
		</div>
	</div>
</div>
<!-- /page content -->

<?php
	}
	
	####
	# HTML MODAL
	####
	function mostra_pg_html_modal(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		//carrega_idioma($_LANG);
?>

<?php
	}
	
	####
	# HTML JAVASCRIPT
	####
	function mostra_pg_html_javascript(SessionCTRL $SID_CTRL)
	{
		## Carrega Idioma
		carrega_idioma($SID_CTRL->getIDIOMA());
?>
		<script>$(document).ready(function(){ $('[data-toggle="tooltip"]').tooltip(); });</script>
		<script src="js/view-cfg-dashboard.js" charset="UTF-8"></script>
		

<?php
	} 
?>
